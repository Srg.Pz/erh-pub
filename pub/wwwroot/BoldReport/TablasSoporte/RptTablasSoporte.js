﻿var RptTablasSoporte = {
    NombreDelControlador:"TablasSoporte",
    EnvioBoletasCorreoElectronico: async function (MyOptions) {
        var htmlCustom = '';
        await UIAjax.invokeMethodAsync('ajax/planillas/reportes', 'GetClaseProceso').then(async function (r) {
            var AFull = r.data;
            var AFilter = AFull.filter((x) => x.cod_proceso == "P");
            await UIFunctions.setSelect2(AFilter, null, 'codigo', 'descripcion').then(function (rhtml) {
                htmlCustom = $("<div class='col-md-4'>")
                    .append(`<label>Proceso</label>`)
                    .append(`<select id="selectProcesoCustomReporte" class="form-control">` + rhtml + `</select>` +
                        `</div>`).outerHTML();
            });
        });
        let Options = {
            Titulo: 'Reporte de Envio de Boletas por Correo Electrónico',
            Custom: htmlCustom,
            Planilla: {
                Operador: "=",
                Filtro: ["-1"],
                Accion: "E",
            },
            Formato: {
                Source: [
                    { codigo: 'F3', nombre: 'Cargos de Entrega' },
                ]
            },
            Periodo1: {
                Show: true,
            },
            Callback: function () {
                $('#selectProcesoCustomReporte').val('B');
            }
        };
        $.extend(true, Options, MyOptions);
        await UISSRS.Modal(Options).then(async function (result) {
            if (!UIEmpty(result)) {
                var jsonParam = {
                    NombreDeReporte: null,
                    CodigoDeTipoPlanilla: result.CodigoDeTipoPlanilla,
                    FlgProceso: 'P',
                    FlgClaseProceso: $('#selectProcesoCustomReporte').val(),
                    Numero: result.Numero1,
                    Periodo: result.Periodo1,
                    CodigoDeProyecto: result.CodigoDeProyecto,
                    CodigoDeUN: result.CodigoDeUN,
                    CodigoDeCC: result.CodigoDeCC,
                    CodigoDeArea: result.CodigoDeArea,
                    CodigoDeLocalidad: result.CodigoDeLocalidad,
                    Estado: 'T',
                    NombreDelControlador: RptTablasSoporte.NombreDelControlador,
                };
                switch (result.Formato) {
                    case 'F3':
                        jsonParam.NombreDeReporte = 'RptAuditoriaEnvioBoletasCorreoElectronicoF3';
                        break;
                }
                UIBoldReport.Load(jsonParam);
            }
        });
    }
};
Object.freeze(RptTablasSoporte);