﻿//"use strict";

//import { signalR } from "./signalr/dist/browser/signalr";

var connection = new signalR.HubConnectionBuilder().withUrl("/chatHub").build();
//var connection = new signalR.HubConnectionBuilder().withUrl("/chatHub", signalR.HttpTransportType.ServerSentEvents).build();
//console.log(signalR.HttpTransportType);
//Disable send button until connection is established
document.getElementById("sendButton").disabled = true;

var urlParams = new URLSearchParams(window.location.search);
const group = urlParams.get('group') || 'Chat_Home';

document.getElementById('titulo-sala').innerText = group;

//const _s = document.querySelectorAll.bind(document);
//console.log(_s('#messagesList'));
//console.log(_s('#messagesList')[0].innerHTML);
connection.on("ReceiveMessage", function (user, message) {
    var msg = message.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
    var fecha = new Date().toLocaleTimeString();
    var encodedMsg = '<i>' + fecha + "</i> Usuario <strong class='text-uppercase'>" + user + "</strong> dice: " + msg;
    var li = document.createElement("li");
    //li.textContent = encodedMsg;
    li.innerHTML = encodedMsg;
    document.getElementById("messagesList").appendChild(li);
});

connection.start().then(function () {
    connection.invoke('AddToGroup', group).catch(err => console.log(err.toString()));
    document.getElementById("sendButton").disabled = false;
}).catch(function (err) {
    return console.error(err.toString());
});

document.getElementById("sendButton").addEventListener("click", function (event) {
    //1 conectado
    //console.log(connection.connection);
    //console.log(connection.connection.connectionState);
    if (connection.connection.connectionState != "Connected") {
        alert('usted no está conectado con el servicio');
        return;
    }

    var user = document.getElementById("userInput").value;
    var message = document.getElementById("messageInput").value;
    connection.invoke("SendMessage", user, message, group).catch(function (err) {
        return console.error(err.toString());
    });
    event.preventDefault();
});