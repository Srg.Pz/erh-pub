// let editor;

// pus = DecoupledEditor.create(document.querySelector('#editor'))
// .then( newEditor => {
//     editor = newEditor;
//     const toolbarContainer = document.querySelector('#toolbar-container');
//     toolbarContainer.appendChild( editor.ui.view.toolbar.element );
// })
// .catch(error => {
//     console.error(error);
// });

// GetFieldsDocument();

// document.addEventListener('dragstart', function (event) {
//     var field = '{' + event.target.innerHTML + '}'
//     event.dataTransfer.setData('Text', field.toUpperCase());
// });

function MyCustomUploadAdapterPlugin(editor) {
    editor.plugins.get('FileRepository').createUploadAdapter = (loader) => {
        return new MyUploadAdapter(loader);
    };
}

async function GetContratoRtf(personal, numPeriodo, replace, aplicacion, reporte, fecha){
    var obj = {
        fecha: fecha,
        aplicacion: aplicacion,
        reporte: reporte,
        replaceData: replace,
        personal: personal,
        contrato: numPeriodo
    }
    await $.getJSON(GetUrl("AjaxContratos/GetBodyDocument"), obj, function (data) {
        editor.setData(data.mensaje);
        if(!replace) editor.isReadOnly = false;
    });

    $('#btnPDFContrato').attr('href',`${GetUrl("AjaxContratos/GetConvertPDF")}?fecha=${obj.fecha}&aplicacion=${obj.aplicacion}&reporte=${obj.reporte}&personal=${obj.personal}&contrato=${obj.contrato}&_=${new Date().getTime()}`)
}

function GetFieldsDocument(){
    $.getJSON(GetUrl("AjaxContratos/GetFieldsReporteConfigurable"), {}, function (data) {
        $('#fields').empty()
        $('#fields').append('<span class="font-weight-bold">* Seleccione y arrastre.</span>')
        data.map(item => {
            $('#fields').append('<p draggable="true" class="d-block">'+ item +'</p>')
        })
    });
}

async function PostDocConfigurable(aplicacion, reporte, dataDoc){
    var obj = { 
        aplicacion: aplicacion,
        reporte: reporte,
        document: dataDoc 
    }
    await UIAjax.runFunctionAsync('AjaxContratos', 'PostBodyDocument', obj, function () {
        $('#btnSaveContrato').addClass('d-none');
        GetContratoRtf(true);
    });
}