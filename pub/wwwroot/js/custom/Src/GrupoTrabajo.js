﻿
function Parametros_Grupo_Listar(tipo) {

    $('#dtGrupo_Trabajo_Trabajadores').DataTable().clear().draw();

    dtGrupos_Trabajo = $('#dtGrupos_Trabajo').DataTable({
        "ajax": portal + '/TablasSoporte/GetGruposTrabajo?tipo=' + tipo
        , "initComplete": function (settings, json) {
            //$(chbTodos).appendTo("#tbTrabajadores_filter label");
        }

        , bDestroy: true
        , dom: 'Bfrtip'
        , buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5'
        ]
        , "language": {
            "url": '/js/language.js'
        }
        , "columns": [

            { "data": "cod_grupo" },
            { "data": "nombre" },
            { "data": "estado" },
        ]

    });
}




function GrupoTrabajoLoad(cod_grupo) {
    $.ajax({
        type: 'GET',
        url: GetUrl("/TablasSoporte/GetGrupoTrabajo?cod_grupo=" + cod_grupo),
        dataType: 'json',
        async: false,
        success: function (data) {

            var item = data.data[0];

            $('#txtCodigo').val(item.cod_grupo);
            $('#txtGrupo').val(item.nombre);
            $('#selectEstado').val(item.estado);
            $('#txtObjetivo').val(item.objetivo);
            $('#txtResponsabilidad').val(item.responsabilidad);
        }
    });
};



function GrupoTrabajoSave() {

    var msgError = GrupodeTrabajoValidar();

    if (msgError != '') {
        MsgBox(msgError, "warning", false);
        return;
    }

    var jsonData = {
        Accion: accion,
        tipo: $('#selectParametros_Grupo').val(),
        cod_grupo: $('#txtCodigo').val(),
        nombre: $('#txtGrupo').val(),
        objetivo: $('#txtObjetivo').val(),
        responsabilidad: $('#txtResponsabilidad').val(),
        estado: $('#selectEstado').val(),
    };
    UIAjax.runFunctionAsync('TablasSoporte', 'SetGrupoTrabajo', jsonData, function () {
        $('#modalGrupoTrabajo').modal('hide');
        Parametros_Grupo_Listar($("#selectParametros_Grupo").val());
    });
}


function GrupodeTrabajoValidar() {

    var codigo = $('#txtCodigo').val();
    var Nombre = $('#txtGrupo').val();

    if (codigo.length == 0) {

        $('#txtCodigo').focus();
        return 'Código de grupo de trabajo inválido';
    }

    if (Nombre == '') {

        $('#txtGrupo').focus();
        return 'Nombre de grupo de trabajo vacío';
    }

    return '';
}


function GrupodeTrabajoCleanControls() {

    $('#txtCodigo').val('');
    $('#txtGrupo').val('');
    $('#txtObjetivo').val('');
    $('#txtResponsabilidad').val('');
    $('#selectEstado').val('A');
}






//////////////////////////////////////////////////////////////////////////////////////////////

function GrupodeTrabajoTrabajadoresListar(cod_grupo) {

    dtGrupo_Trabajo_Trabajadores = $('#dtGrupo_Trabajo_Trabajadores').DataTable({
        "ajax": portal + '/TablasSoporte/GetGrupoTrabajoPersonales?parametro_grupo=' + $('#selectParametros_Grupo').val() + '&cod_grupo=' + cod_grupo
        , "initComplete": function (settings, json) {
            //$(chbTodos).appendTo("#tbTrabajadores_filter label");
        }

        , bDestroy: true
        , dom: 'Bfrtip'
        , buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5'
        ]
        , "language": {
            "url": '/js/language.js'
        }
        , "columns": [

            {
                data: "cod_personal",
                render: function (data, type, row) {
                    if (type === 'display') {
                        return '<input type="checkbox" class="editor-active" name="chbCTA">';
                    }
                    return data;
                },
                className: "dt-body-center"
            },
            { "data": "cod_trabajador" },
            { "data": "trabajador" },
            { "data": "cargo" },
        ]

    });
}


function GrupodeTrabajoTrabajadores_Libres_Listar(cod_grupo) {

    dtTrabajadores = $('#dtTrabajadores').DataTable({
        "ajax": portal + '/TablasSoporte/GetGrupoTrabajoPersonalesLibres?parametro_grupo=' + $('#selectParametros_Grupo').val()
        , "initComplete": function (settings, json) {
            //$(chbTodos).appendTo("#tbTrabajadores_filter label");
        }

        , bDestroy: true
        , dom: 'Bfrtip'
        , buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5'
        ]
        , "language": {
            "url": '/js/language.js'
        }
        , "columns": [

            {
                data: "cod_personal",
                render: function (data, type, row) {
                    if (type === 'display') {
                        return '<input type="checkbox" class="editor-active" name="chbCC1">';
                    }
                    return data;
                },
                className: "dt-body-center"
            },
            { "data": "cod_trabajador" },
            { "data": "trabajador" },
            { "data": "cargo" },

        ]

    });
}


$('#btnTrabajadoresNuevo').on('click', function (e) {
    e.preventDefault();

    $("#modalTrabajadores").on("shown.bs.modal", function () {

        GrupodeTrabajoTrabajadores_Libres_Listar(cod_grupo_Seleccionado);

    }).modal('show');

});


function GrupoTrabajoTrabajadores_Libres_Save(accion, cod_grupo, cod_trabajador) {

    var jsonData = {
        accion: accion,
        cod_grupo: cod_grupo,
        cod_trabajador: cod_trabajador
    };
    UIAjax.runFunctionAsync('TablasSoporte', 'SetPersonalEnGrupoTrabajo', jsonData, function () {
        $('#modalTrabajadores').modal('hide');
        GrupodeTrabajoTrabajadoresListar(cod_grupo);
    });
}



$('#btnTrabajadoresEliminar').on('click', function (e) {
    e.preventDefault();

    $("#dtGrupo_Trabajo_Trabajadores input[type=checkbox]").each(function (i) {

        if ($("input:checkbox[name=chbCTA]")[i].checked == true) {
            var row = $(this).closest("tr")[0];

            var cod_trabajador = row.cells[1].innerText;
            GrupoTrabajoTrabajadores_Libres_Save('D', cod_grupo_Seleccionado, cod_trabajador);
        }
    });

    GrupodeTrabajoTrabajadoresListar(cod_grupo_Seleccionado);

});