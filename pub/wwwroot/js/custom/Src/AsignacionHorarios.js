﻿
function fnListTrabajadores() {

    var CodPlanilla = $("#selectTipoPlanilla").val();
    var CodProyecto = $("#selectProyecto").val();
    var CodCentroCosto = $("#selectCentrosCosto").val();
    var CodUnidadNegocio = $("#selectUnidadesNegocio").val();
    var CodLocalidad = $("#selectLocalidad").val();
    var CodArea = $("#selectArea").val();
    //var Filtro = $("#textFiltro").val();

    var Parameters = 'CodTipoPlanilla=' + CodPlanilla.trim()
        + '&CodProyecto=' + CodProyecto.trim()
        + '&CodCentroCosto=' + CodCentroCosto.trim()
        + '&CodLocalidad=' + CodLocalidad.trim()
        + '&CodUnidadNegocio=' + CodUnidadNegocio.trim()
        + '&CodArea=' + CodArea.trim();
    //+ '&Filtro=' + Filtro.trim();

    //var chbTodos = '<div class="checkbox checkbox-primary d-flex mr-auto">' +
    //    '<input type="checkbox" id="chkTodos" style="display:none;" >' +
    //    '<label for="chkTodos" class="cr pt-1">Todos</label>' +
    //    '</div>';

    tableTrabajadores = $('#tbTrabajadores').DataTable({
        ajax: portal + '/ControlAsistencia/GetColaboradores/?' + Parameters,
        initComplete: function (settings, json) {
            $('#tbTrabajadores_filter').addClass('mt-2');
            UIDataTable.Buttons.Todos(this);
        },
        ordering: false,
        //dom: '<"divFilterColaboradores">Bfrtip',
        dom: 'Bfrtip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5'
        ],
        columns: [
            { data: "codigo" },
            { data: "trabajador" },
            { data: "numero_doc" },
            { data: "fotocheck" }
        ]
    });

}

//// -------------- >  GUARDAR  <--------------
function SaveDiario(_CodPersonal, _CodTurno, _Fecha, tr) {

    ////COLORES
    //var prcolor = $("#txtPaletColors").val();
    //var fn = prcolor.slice(4, parseInt(prcolor.length - 1));
    //var parts = fn.toString().split(",");

    var saveObj = {
        CodPersonal: _CodPersonal,
        CodTurno: _CodTurno,
        Fecha: _Fecha,
        Turno: {
            hei3: $('#lblphei3_reg', tr).val(),
            hei2: $('#lblphei2_reg', tr).val(),
            hei1: $('#lblphei1_reg', tr).val(),
            hi: $('#lblhi', tr).val(),
            bs: $('#lblpbs_reg', tr).val(),
            bi: $('#lblpbi_reg', tr).val(),
            hs: $('#lblhs', tr).val(),
            hes1: $('#lblphes1_reg', tr).val(),
            hes2: $('#lblphes2_reg', tr).val(),
            hes3: $('#lblphes3_reg', tr).val(),
            thi: $('#lblthi', tr).val(),
            //tbs: $('#txt_tbsMPC').val(),
            //tbi: $('#txt_tbiMPC').val(),
            ths: $('#lblths', tr).val(),
        }
    };

    $.post(portal + '/ControlAsistencia/PostAsignacionDiarioHorario', saveObj, function (data) {

        if (data.respuesta == 1) {
            MsgBox("success");
        }
        else {
            MsgBox(data.mensaje, "warning", false);
        }

    });
}

////---------------------------- > PROGRAMACION POR SEMANA  < -----------------------------

function fnListProgramacion() {
    var count = 0;

    var checkB = '<div class="checkbox checkbox-fill d-inline">'
        + '<input id="@@Id_" type="checkbox" name="@@Name_" value="@@Val_">'
        + '<label for="@@Id_" class="cr">&nbsp;</label>'
        + '</div>';

    tableProgramacion = $('#tbProgramacion').DataTable({
        "ajax": portal + '/ControlAsistencia/GetProgramacionXUsuario?NotIn=' + (!$('#chkHorarioEspecial').is(':checked') ? '-2' : "")
        , "initComplete": function (settings, json) {
        }
        , bDestroy: true
        , paging: false
        , searching: false
        , "columns": [
            { "data": "cod_turno" },
            { "data": "nombre" },
            {
                "data": "lunes"
                , "render": function (data, type, row) {
                    count++;
                    var nombre = checkB.replace(/@@Id_/g, "chbLunes" + count).replace(/@@Name_/g, "diaLunes").replace(/@@Val_/g, row.cod_turno);
                    return nombre;
                }
            },
            {
                "data": "martes"
                , "render": function (data, type, row) {
                    count++;
                    var nombre = checkB.replace(/@@Id_/g, "chbMartes" + count).replace(/@@Name_/g, "diaMartes").replace(/@@Val_/g, row.cod_turno);
                    return nombre;
                }
            },
            {
                "data": "miercoles"
                , "render": function (data, type, row) {
                    count++;
                    var nombre = checkB.replace(/@@Id_/g, "chbMiercoles" + count).replace(/@@Name_/g, "diaMiercoles").replace(/@@Val_/g, row.cod_turno);
                    return nombre;
                }
            },
            {
                "data": "jueves"
                , "render": function (data, type, row) {
                    count++;
                    var nombre = checkB.replace(/@@Id_/g, "chbJueves" + count).replace(/@@Name_/g, "diaJueves").replace(/@@Val_/g, row.cod_turno);
                    return nombre;
                }
            },
            {
                "data": "viernes"
                , "render": function (data, type, row) {
                    count++;
                    var nombre = checkB.replace(/@@Id_/g, "chbViernes" + count).replace(/@@Name_/g, "diaViernes").replace(/@@Val_/g, row.cod_turno);
                    return nombre;
                }
            },
            {
                "data": "sabado"
                , "render": function (data, type, row) {
                    count++;
                    var nombre = checkB.replace(/@@Id_/g, "chbSabado" + count).replace(/@@Name_/g, "diaSabado").replace(/@@Val_/g, row.cod_turno);
                    return nombre;
                }
            }
            ,
            {
                "data": "domingo"
                , "render": function (data, type, row) {
                    count++;
                    var nombre = checkB.replace(/@@Id_/g, "chbDomingo" + count).replace(/@@Name_/g, "diaDomingo").replace(/@@Val_/g, row.cod_turno);
                    return nombre;
                }
            }

        ]
    });
}


// -------------- GUARDAR 
async function SaveProgramacionSemanal() {

    var _FechaDesde = $("#textFechaDesdeProgSeman").val();
    var _FechaHasta = $("#textFechaHastaProgSeman").val();

    var _Lunes = jsonProgramacion["diaLunes"];
    var _Martes = jsonProgramacion["diaMartes"];
    var _Miercoles = jsonProgramacion["diaMiercoles"];
    var _Jueves = jsonProgramacion["diaJueves"];
    var _Viernes = jsonProgramacion["diaViernes"];
    var _Sabado = jsonProgramacion["diaSabado"];
    var _Domingo = jsonProgramacion["diaDomingo"];

    // ----> VALIDACIONES <------

    var slp1 = _FechaDesde.toString().split("/");
    var slp2 = _FechaHasta.toString().split("/");

    var _FechaDesdeFin = new Date(slp1[1] + '-' + slp1[0] + '-' + slp1[2]);
    var _FechaHastaFin = new Date(slp2[1] + '-' + slp2[0] + '-' + slp2[2]);

    if (_FechaDesdeFin > _FechaHastaFin) {
        $("#textFechaHastaProgSeman").addClass("validation_empty").focus();
        MsgBox("Fecha hasta debe ser mayor a Fecha Desde.", "warning", false);
        return;
    }

    var Rows = tableTrabajadores.$('tr.selected');
    if (Rows.length == 0) {
        UIMensaje.COLABORADORM();
        $('#mdProgramacion,html').animate({ scrollTop: $('#tbTrabajadores').height() }, 500);
        return;
    }

    var CodPersonal = [];
    $.each(Rows, function (index, value) {
        var jsonRow = tableTrabajadores.row(value).data();
        CodPersonal.push({});
        CodPersonal[index] = jsonRow.personal;
    });

    if ($("#textFechaDesdeProgSeman").val() == undefined || $("#textFechaDesdeProgSeman").val() == null || $("#textFechaDesdeProgSeman").val().length < 10) {
        $("#textFechaDesdeProgSeman").addClass("validation_empty").focus();
        MsgBox("Formato de fecha inicio inválido.", "warning", true);
        return;
    }

    if ($("#textFechaHastaProgSeman").val() == undefined || $("#textFechaHastaProgSeman").val() == null || $("#textFechaHastaProgSeman").val().length < 10) {
        $("#textFechaHastaProgSeman").addClass("validation_empty").focus();
        MsgBox("Formato de fecha fin inválido.", "warning", true);
        return;
    }

    if (_Lunes == undefined || _Lunes == "") {
        MsgBox("Seleccione un Horario para el día Lunes.", "warning", true);
        return;
    }

    if (_Martes == undefined || _Martes == "") {
        MsgBox("Seleccione un Horario para el día Martes.", "warning", true);
        return;
    }

    if (_Miercoles == undefined || _Miercoles == "") {
        MsgBox("Seleccione un Horario para el día Miércoles.", "warning", true);
        return;
    }

    if (_Jueves == undefined || _Jueves == "") {
        MsgBox("Seleccione un Horario para el día Jueves.", "warning", true);
        return;
    }

    if (_Viernes == undefined || _Viernes == "") {
        MsgBox("Seleccione un Horario para el día Viernes.", "warning", true);
        return;
    }

    if (_Sabado == undefined || _Sabado == "") {
        MsgBox("Seleccione un Horario para el día Sábado.", "warning", true);
        return;
    }

    if (_Domingo == undefined || _Domingo == "") {
        MsgBox("Seleccione un Horario para el día Domingo.", "warning", true);
        return;
    }

    idsCodPersonal = CodPersonal.join(',');

    if ($('#chkActPersonal').is(':checked')) {
        SaveProgramacionSemanalActualizarPersonal();
    }


    var saveObj = {
        CodPersonal: idsCodPersonal,
        FechaDesde: _FechaDesde,
        FechaHasta: _FechaHasta,
        Lunes: _Lunes,
        Martes: _Martes,
        Miercoles: _Miercoles,
        Jueves: _Jueves,
        Viernes: _Viernes,
        Sabado: _Sabado,
        Domingo: _Domingo
    };

    if ($('#chkHorarioEspecial').is(':checked')) {
        if (fnValidacionesIngresoDatosHoraEspecial()) {
            SaveProgramacionSemanalHorarioEspecial();
        }
        else {
            return;
        }
    }

    await UILoadingDefault("show");
    await UIAjax.runFunctionAsync('ControlAsistencia', 'PostProgramacionSemanalAsignacionHorario', saveObj);
    UILoadingDefault("hide");
}


//// ----------------------> ACTUALIZAR PERSONAL <-------------------

function SaveProgramacionSemanalActualizarPersonal() {

    var _Lunes_ = jsonProgramacion["diaLunes"];
    var _Martes_ = jsonProgramacion["diaMartes"];
    var _Miercoles_ = jsonProgramacion["diaMiercoles"];
    var _Jueves_ = jsonProgramacion["diaJueves"];
    var _Viernes_ = jsonProgramacion["diaViernes"];
    var _Sabado_ = jsonProgramacion["diaSabado"];
    var _Domingo_ = jsonProgramacion["diaDomingo"];

    //// ----> VALIDACIONES <------
    var saveObj = {
        CodPersonal: idsCodPersonal,
        Lunes: _Lunes_,
        Martes: _Martes_,
        Miercoles: _Miercoles_,
        Jueves: _Jueves_,
        Viernes: _Viernes_,
        Sabado: _Sabado_,
        Domingo: _Domingo_
    };

    $.post(portal + '/ControlAsistencia/PostProgramacionSemanalActPersonalAsignacionHorario', saveObj, function (data) {
    });
}


//// ------------------->>> SAVE / HORARIO ESPECIAL <<< --------------------
function SaveProgramacionSemanalHorarioEspecial() {

    var saveObj = {
        Cod_Turno: '-2',

        //A
        Himin: $('#txtSalidaMinHora').val(),
        Hei3: $('#txtHorasExtTrm3Hora').val(),
        Hei2: $('#txtHorasExtTrm2Hora').val(),
        Hei1: $('#txtHorasExtTrm1Hora').val(),
        Hi: $('#txtHoraIngHora').val(),
        Bs: $('#txtSalidaBreakHora').val(),
        Bi: $('#txtRegresoBreakHora').val(),
        Hs: $('#txtHoraSalidaHora').val(),
        Hes1: $('#txtHoraExtraTrm1Hora').val(),
        Hes2: $('#txtHoraExtraTrm2Hora').val(),
        Hes3: $('#txtHoraExtraTrm3Hora').val(),
        Hsmax: $('#txtSalidaMax').val(),
        Th: $('#txtTiempoTotLab').val(),
        Tb: $('#txtTiempoBreak').val(),
        Hmin_asist: $('#txtMinAsistencia').val(),

        //B
        Thei3: $('#txtHorasExtTrm3Toleran').val(),
        Thei2: $('#txtHorasExtTrm2Toleran').val(),
        Thei1: $('#txtHorasExtTrm1Toleran').val(),
        Thi: $('#txtHoraIngToleran').val(),
        Tbs: $('#txtSalidaBreakToleran').val(),
        Tbi: $('#txtRegresoBreakTolera').val(),
        Ths: $('#txtHoraSalidaTolera').val(),
        Thes1: $('#txtHoraExtraTrm1Tolera').val(),
        Thes2: $('#txtHoraExtraTrm2Tolera').val(),
        Thes3: $('#txtHoraExtraTrm3Tolera').val(),

        //C
        Tht: $('#txtHoraIngFree').val(),

        Flg_Nocturno: $("#chbHoraNoct").is(':checked') ? "S" : "N",
        Flg_Extraordinario: $("#chbHoraExtr").is(':checked') ? "S" : "N",
        Flg_Bono_extra: $("#chbHoraBonExt").is(':checked') ? "S" : "N"
    };

    $.post(portal + '/ControlAsistencia/PostProgramacionSemanalHorarioEspecial', saveObj, function (data) {

        if (data.respuesta == 1) {
            MsgBox("success");
        }
        else {
            MsgBox(data.mensaje, "warning", false);
        }

    });
}

function fnValidacionesIngresoDatosHoraEspecial() {

    var Status = true;

    $('input[name=horario]').each(function () {
        var Crtl = $(this)[0].id;
        Status = ValidarFormatoHoraMinGuardar(Crtl);
        if (Status == false) {
            return Status;
        }
    });

    return Status;
}