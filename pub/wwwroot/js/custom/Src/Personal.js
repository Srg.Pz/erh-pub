﻿//*********************************************************************************
var edadMayoria = 18;
var minRuc = 11;


//#region REPORTE CAMBIO DE SUELDO
function CambiodeSueldoInicializar() {




    $('#btnCambioSueldoAceptar').on('click', function (e) {
        e.preventDefault();

        CambiodeSueldoGenerarReporte();
    });
}

function CambiodeSueldoGenerarReporte() {

    var jsonParam = 'codProyecto=' + $('#selRptCambioSueldoCodProyecto').val().trim() +
        '&codCentroCosto=' + $('#selRptCambioSueldoCodCentroCosto').val().trim() +
        '&codUnidadNegocio=' + $('#selRptCambioSueldoCodUnidadNegocio').val().trim() +
        '&codArea=' + $('#selRptCambioSueldoCodArea').val().trim() +
        '&codLocalidad=' + $('#selRptCambioSueldoCodLocalidad').val().trim() +
        '&codPlanilla=' + $('#selRptCambioSueldoCodPlanilla').val().trim();

    containerPage.maskContainer("Cargando");

    $('#dtCambioSueldo').DataTable({
        "ajax": GetUrl("Ajax/Personal/GetReportePersonalCambiodeSueldo?" + jsonParam),
        "initComplete": function (settings, json) {
            //$('#gridContainerRptCambioSueldo').removeClass('d-none');
            //$("html, body").animate({
            //    scrollTop: $('#gridContainerRptCambioSueldo').offset().top
            //}, 600, "linear", function () {
            //    containerPage.unmaskContainer();
            //});
            UIMaskContainer.Load('#gridContainerRptCambioSueldo');
        },
        //"bFilter": true,
        "paging": false,
        "bInfo": false,
        "scrollY": "400px",
        //responsive:false
        //, bDestroy: true
        //, dom: 'Bfrtip'        
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5'
        ],
        columns: [

            { "data": "codigo" },
            { "data": "trabajador" },
            {
                "targets": 2,
                "data": 'periodo',
                "render": function (data, type, row) {
                    return row.anio + '-' + row.mes + '-' + row.numero;
                }
            },
            { "data": "sueldo", render: $.fn.dataTable.render.number(',', '.', 2, '') },
            { "data": "cargo" },
            { "data": "nom_proyecto" },
            { "data": "nom_ccosto" },
            { "data": "nom_unegocio" },
            { "data": "nom_area" },
            { "data": "nom_localidad" },
        ]
    });
}

//#endregion

//#region REPORTE FAMILIARES
function FamiliaresInicializar() {

    $('#txtFAMILIARESFechaDesde, #txtFAMILIARESFechaHasta').datepicker({
        autoclose: true,
        format: "dd/mm/yyyy"
    });

    $('#txtFAMILIARESEdadDesde').mask('00');
    $('#txtFAMILIARESEdadHasta').mask('00');

    $("#rbFAMILIARESFecha").change();
    $('#txtFAMILIARESFechaDesde').attr('disabled', false);
    $('#txtFAMILIARESFechaHasta').attr('disabled', false);
    $('#txtFAMILIARESEdadDesde').attr('disabled', true);
    $('#txtFAMILIARESEdadHasta').attr('disabled', true);

    FamiliaresParentescoCargar();

    $("#rbFAMILIARESFecha").change(function () {
        $('#txtFAMILIARESFechaDesde').attr('disabled', false);
        $('#txtFAMILIARESFechaHasta').attr('disabled', false);

        $('#txtFAMILIARESEdadDesde').attr('disabled', true);
        $('#txtFAMILIARESEdadHasta').attr('disabled', true);
    });

    $("#rbFAMILIARESEdad").change(function () {
        $('#txtFAMILIARESFechaDesde').attr('disabled', true);
        $('#txtFAMILIARESFechaHasta').attr('disabled', true);

        $('#txtFAMILIARESEdadDesde').attr('disabled', false);
        $('#txtFAMILIARESEdadHasta').attr('disabled', false);

        $('#txtFAMILIARESEdadDesde').mask('00');
        $('#txtFAMILIARESEdadHasta').mask('00');
    });



    $('#btnFAMILIARESGenerarReporte').on('click', function (e) {
        e.preventDefault();

        FamiliaresGenerarReporte();
    });
}

function FamiliaresGenerarReporte() {

    //1. Detectar los parentescos seleccionados
    var codParentescos = '';
    $('#dtRptFAMILIARESParentesco_Body tr.selected').each(function (index, value) {
        if (codParentescos.length > 0)
            codParentescos += ",";
        codParentescos += $(this).data('codigo');
    });

    //2. Validar
    var msg = FamiliaresValidarDatos(codParentescos);
    if (msg != '') {
        MsgBox(msg, "warning", false);
        return;
    }

    containerPage.maskContainer("Cargando");

    var ParametersDt =
        'codProyecto=' + $('#selRptFAMILIARESCodProyecto').val().trim() +
        '&codCentroCosto=' + $('#selFAMILIARESCodCentroCosto').val().trim() +
        '&codUnidadNegocio=' + $('#selRptFAMILIARESCodUnidadNegocio').val().trim() +
        '&codArea=' + $('#selRptFAMILIARESCodArea').val().trim() +
        '&codLocalidad=' + $('#selRptFAMILIARESCodLocalidad').val().trim() +
        '&codPlanilla=' + $('#selRptFAMILIARESCodPlanilla').val().trim() +
        '&modo=' + ($('input:radio[id=rbFAMILIARESFecha]:radio').is(':checked') ? '01' : '02') +
        '&fechaDesde=' + $('#txtFAMILIARESFechaDesde').val().trim() +
        '&fechaHasta=' + $('#txtFAMILIARESFechaHasta').val().trim() +
        '&edadDesde=' + $('#txtFAMILIARESEdadDesde').val().trim() +
        '&edadHasta=' + $('#txtFAMILIARESEdadHasta').val().trim() +
        '&personalCesado=' + ($('#chkPersonalCesado').is(':checked') ? 'T' : 'A') +
        '&codParentescos=' + codParentescos.trim();

    $('#dtFamiliares').DataTable({
        "ajax": GetUrl("Ajax/Personal/GetReportePersonalFamiliares?" + ParametersDt)
        , "initComplete": function (settings, json) {

            $('#gridContainerRptFamiliares').removeClass('d-none');
            $("html, body").animate({
                scrollTop: $('#gridContainerRptFamiliares').offset().top
            }, 600, "linear", function () {
                containerPage.unmaskContainer();
            });
        }

        , "bFilter": true
        , "paging": false
        , "bInfo": false
        //, "scrollY": "200px"
        , bDestroy: true
        , dom: 'Bfrtip'
        , buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5'
        ]
        , "language": {
            "url": '/js/language.js'
        }
        , "columns": [

            { "data": "codigo" },
            { "data": "trabajador" },
            { "data": "trab_sexo" },
            { "data": "nro_parientes" },
            { "data": "nombre_familiar" },
            { "data": "flg_genero" },
            { "data": "parentesco" },
            { "data": "fecha_nacimiento2" },
            { "data": "edad" },
            { "data": "meses" },
        ]

    });


}

function FamiliaresValidarDatos(codParentescos) {

    if (codParentescos.trim() == '') {
        return 'Debe seleccionar al menos un parentesco';
    }

    if ($('input:radio[id=rbFAMILIARESFecha]:radio').is(':checked')) {

        if ($('#txtFAMILIARESFechaDesde').val().trim() == '') {
            return 'Campo fecha desde esta vacío';
        }

        if ($('#txtFAMILIARESFechaHasta').val().trim() == '') {
            return 'Campo fecha hasta esta vacío';
        }
    }
    else {
        if ($('#txtFAMILIARESEdadDesde').val().trim() == '') {
            return 'Campo edad desde esta vacío';
        }

        if ($('#txtFAMILIARESEdadHasta').val().trim() == '') {
            return 'Campo edad hasta esta vacío';
        }
    }

    return '';
}

function FamiliaresParentescoCargar() {

    $.ajax({
        type: "GET",
        url: GetUrl("Ajax/Personal/GetParentesco"),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function success(data) {
            var container = $("#dtRptFAMILIARESParentesco_Body");
            container.empty();
            $.each(data.data, function (index, value) {
                var html = '<tr data-codigo="' + value.codigo + '" onclick="FamiliaresParentescoOnClick(this);">' +
                    '<td class="text-center" style="display:none">' + (index + 1) + '</td>' +
                    '<td class="text-center">' + value.codigo + '</td>' +
                    '<td class="text-center">' + value.nombre + '</td>' +
                    '</tr>';
                container.append(html);
            });
        },
        error: function error(response) {
            console.log(response);
            MsgBox("warning");
        }
    });
}

function FamiliaresParentescoOnClick(ths) {
    var Row = $(ths);
    if (Row.hasClass('selected')) {
        Row.removeClass('selected');
    }
    else {
        Row.addClass('selected');
    }
}

//#endregion

//#region REPORTE LISTADO OFICIAL PERSONAL
function LOficialPersonalInicializar() {

    $('#btnLOficialPersonalAceptar').on('click', function (e) {
        e.preventDefault();

        LOficialPersonalGenerarReporte();
    });
}

function LOficialPersonalGenerarReporte() {


    var ParametersDt = 'codProyecto=' + $('#selLOficialPersonalCodProyecto').val().trim() +
        '&codCentroCosto=' + $('#selLOficialPersonalCodCentroCosto').val().trim() +
        '&codUnidadNegocio=' + $('#selLOficialPersonalCodUnidadNegocio').val().trim() +
        '&codArea=' + $('#selLOficialPersonalCodArea').val().trim() +
        '&codLocalidad=' + $('#selLOficialPersonalCodLocalidad').val().trim() +
        '&codPlanilla=' + $('#selLOficialPersonalCodPlanilla').val().trim();

    containerPage.maskContainer("Cargando");

    $('#dtLOficialPersonal').DataTable({
        "ajax": GetUrl("Ajax/Personal/GetReportePersonalLOficialPersonal?" + ParametersDt)
        , "initComplete": function (settings, json) {

            $('#gridContainerLOficialPersonal').removeClass('d-none');
            $("html, body").animate({
                scrollTop: $('#gridContainerLOficialPersonal').offset().top
            }, 600, "linear", function () {
                containerPage.unmaskContainer();
            });
        }
        //, data: JSON.stringify(jsonData)
        , "bFilter": true
        , "paging": false
        , "bInfo": false
        , "scrollY": "400px"
        , bDestroy: true
        , dom: 'Bfrtip'
        , buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5'
        ]
        , "language": {
            "url": '/js/language.js'
        }
        , "columns": [

            { "data": "codigo" },
            { "data": "trabajador" },
            { "data": "direccion" },
            { "data": "nacionalidad" },
            { "data": "tipodoc" },
            { "data": "nrodoc" },

            {
                "data": "fecha_ingreso",
                render: function (data, type, row) {
                    if (data == null || data.length < 10) return "";
                    var items = data.substring(0, 10).split('-');
                    return items[2] + "/" + items[1] + "/" + items[0];
                }
            },

            {
                "data": "fecha_nacimiento",
                render: function (data, type, row) {
                    if (data == null || data.length < 10) return "";
                    var items = data.substring(0, 10).split('-');
                    return items[2] + "/" + items[1] + "/" + items[0];
                }
            },

            { "data": "edad" },
            { "data": "sexo" },
            { "data": "sistema_pension" },
        ]
    });
}

//#endregion

//#region REPORTE SANCIONES AMONESTACIONES
function SancionesAmonestacionesInicializar() {

    $('#btnSancionesAmonestacionesListarPersonal').on('click', function (e) {
        e.preventDefault();

        SancionesAmonestacionesListarPersonal();
    });

    $('#dtSancionesAmonestacionesPersonal').on('click', 'tr', function () {
        $('#dtSancionesAmonestacionesPersonal tr').removeClass('selected');
        var ths = $(this);
        ths.addClass('selected');

        var table = $('#dtSancionesAmonestacionesPersonal').DataTable();
        var data = table.row(this).data();

        SancionesAmonestacionesGenerarReporte(data.cod_personal);
    });

}

function SancionesAmonestacionesListarPersonal() {

    var ParametersDt =
        'codProyecto=' + $('#selSancionesAmonestacionesProyecto').val().trim() +
        '&codCentroCosto=' + $('#selSancionesAmonestacionesCentroCosto').val().trim() +
        '&codUnidadNegocio=' + $('#selSancionesAmonestacionesUnidadNegocio').val().trim() +
        '&codArea=' + $('#selSancionesAmonestacionesArea').val().trim() +
        '&codLocalidad=' + $('#selSancionesAmonestacionesLocalidad').val().trim() +
        '&codPlanilla=' + $('#selSancionesAmonestacionesPlanilla').val().trim() +
        '&Tipo=' + $('#selSancionesAmonestacionesTipo').val().trim();

    containerPage.maskContainer("Cargando");

    $('#dtSancionesAmonestacionesPersonal').DataTable({
        "ajax": GetUrl("Ajax/Personal/GetPersonalSancionesyAmonestaciones?" + ParametersDt)
        , "initComplete": function (settings, json) {

            $('#gridContainerSancionesAmonestaciones').removeClass('d-none');
            $('#gridContainerSancionesAmonestacionesFinal').addClass('d-none');

            $("html, body").animate({
                scrollTop: $('#gridContainerSancionesAmonestaciones').offset().top
            }, 600, "linear", function () {
                containerPage.unmaskContainer();
            });
        }
        //, data: JSON.stringify(jsonData)
        , "bFilter": true
        , "paging": false
        , "bInfo": false
        //, "scrollY": "400px"
        , bDestroy: true
        , dom: 'Bfrtip'
        , buttons: [
            //'copyHtml5',
            //'excelHtml5',
            //'csvHtml5'
        ]
        , "language": {
            "url": '/js/language.js'
        }
        , "columns": [

            { "data": "tipo_doc" },
            { "data": "numero_docid" },
            { "data": "trabajador" },
            { "data": "cod_personal" },
            { "data": "estado" },
        ]
    });
}

function SancionesAmonestacionesGenerarReporte(cod_personal) {

    var ParametersDt = 'codProyecto=' + $('#selSancionesAmonestacionesProyecto').val().trim() +
        '&codCentroCosto=' + $('#selSancionesAmonestacionesCentroCosto').val().trim() +
        '&codUnidadNegocio=' + $('#selSancionesAmonestacionesUnidadNegocio').val().trim() +
        '&codArea=' + $('#selSancionesAmonestacionesArea').val().trim() +
        '&codLocalidad=' + $('#selSancionesAmonestacionesLocalidad').val().trim() +
        '&codPlanilla=' + $('#selSancionesAmonestacionesPlanilla').val().trim() +
        '&Tipo=' + $('#selSancionesAmonestacionesTipo').val().trim() +
        '&cod_personal=' + cod_personal;

    //containerPage.maskContainer("Cargando");

    $('#dtSancionesAmonestaciones').DataTable({
        "ajax": GetUrl("Ajax/Personal/GetReportePersonalSancionesyAmonestaciones?" + ParametersDt)
        , "initComplete": function (settings, json) {

            $('#gridContainerSancionesAmonestacionesFinal').removeClass('d-none');
            $("html, body").animate({
                scrollTop: $('#gridContainerSancionesAmonestacionesFinal').offset().top
            }, 600, "linear", function () {
                containerPage.unmaskContainer();
            });
            //$('#gridContainerSancionesAmonestacionesFinal').removeClass('d-none');
        }
        //, data: JSON.stringify(jsonData)
        , "bFilter": true
        , "paging": false
        , "bInfo": false
        //, "scrollY": "400px"
        , bDestroy: true
        , dom: 'Bfrtip'
        , buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5'
        ]
        , "language": {
            "url": '/js/language.js'
        }
        , "columns": [

            { "data": "item" },
            { "data": "tipo" },
            {
                "data": "fecha",
                render: function (data, type, row) {
                    if (data == null || data.length < 10) return "";
                    var items = data.substring(0, 10).split('-');
                    return items[2] + "/" + items[1] + "/" + items[0];
                }

            },
            { "data": "descripcion" },
            { "data": "reportado" },
        ]
    });
}


//#endregion

//#region MAESTRO TRABAJADOR
function MaestroTrabajadorInicializar() {

    $('#btnMaestroTrabajadorAceptar').on('click', function (e) {
        e.preventDefault();

        MaestroTrabajadorListarPersonal();
        //MaestroTrabajadorsGenerarReporte();
    });

    $('#dtMaestroTrabajador').on('click', 'tr', function () {

        var Row = $(this);

        if (Row.hasClass('selected')) {
            Row.removeClass('selected');
        }
        else {
            Row.addClass('selected');
        }


        var codPersonales = '';
        $('#dtMaestroTrabajador tr.selected').each(function (index, value) {

            try {
                if (codPersonales.length > 0)
                    codPersonales += ",";

                var table = $('#dtMaestroTrabajador').DataTable();
                if (table != null) {
                    var data = table.row(this).data();
                    codPersonales += data.cod_personal;
                }
            }
            catch (e) {
                // sentencias para manejar cualquier excepción
                //console.log(e); // pasa el objeto de la excepción al manejador de errores
            }
        });

        MaestroTrabajadorsGenerarReporte(codPersonales);
    });
}


function MaestroTrabajadorListarPersonal() {

    var ParametersDt = 'codProyecto=' + $('#selMaestroTrabajadorCodProyecto').val().trim() +
        '&codCentroCosto=' + $('#selMaestroTrabajadorCodCentroCosto').val().trim() +
        '&codUnidadNegocio=' + $('#selMaestroTrabajadorCodUnidadNegocio').val().trim() +
        '&codArea=' + $('#selMaestroTrabajadorCodArea').val().trim() +
        '&codLocalidad=' + $('#selMaestroTrabajadorCodLocalidad').val().trim() +
        '&codPlanilla=' + $('#selMaestroTrabajadorCodPlanilla').val().trim() +
        '&estado=' + $('#selMaestroTrabajadorEstado').val().trim();

    containerPage.maskContainer("Cargando");

    $('#dtMaestroTrabajador').DataTable({
        "ajax": GetUrl("Ajax/Personal/GetReportePersonalTrabajadoresMaestroPersonal?" + ParametersDt)
        , timeout: 300000 // sets timeout to 3 seconds
        , "initComplete": function (settings, json) {

            $('#gridContainerMaestroTrabajador').removeClass('d-none');
            $("html, body").animate({
                scrollTop: $('#gridContainerMaestroTrabajador').offset().top
            }, 600, "linear", function () {
                containerPage.unmaskContainer();
            });
        }
        //, data: JSON.stringify(jsonData)
        , "bFilter": true
        , "paging": false
        , "bInfo": false
        , "scrollY": "400px"
        , bDestroy: true
        , dom: 'Bfrtip'
        , buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5'
        ]
        , "language": {
            "url": '/js/language.js'
        }
        , "columns": [

            { "data": "cod_personal" },
            { "data": "trabajador" },
            { "data": "tipodoc" },
            { "data": "nrodoc" },
            {
                "data": "fecha_ingreso",
                render: function (data, type, row) {
                    if (data == null || data.length < 10) return "";
                    var items = data.substring(0, 10).split('-');
                    return items[2] + "/" + items[1] + "/" + items[0];
                }
            },
            { "data": "estado_trabajador" },
            { "data": "sueldo", render: $.fn.dataTable.render.number(',', '.', 2, '') },

            { "data": "banco_pago" },
            { "data": "banco_cts" },
            { "data": "afp" },
            { "data": "flg_asignacion_familiar" },
            { "data": "cargo" },
            { "data": "area" },
            { "data": "planilla" },
        ]
    });
}


function MaestroTrabajadorsGenerarReporte(codPersonales) {

    var ParametersDt = 'codProyecto=' + $('#selMaestroTrabajadorCodProyecto').val().trim() +
        '&codCentroCosto=' + $('#selMaestroTrabajadorCodCentroCosto').val().trim() +
        '&codUnidadNegocio=' + $('#selMaestroTrabajadorCodUnidadNegocio').val().trim() +
        '&codArea=' + $('#selMaestroTrabajadorCodArea').val().trim() +
        '&codLocalidad=' + $('#selMaestroTrabajadorCodLocalidad').val().trim() +
        '&codPlanilla=' + $('#selMaestroTrabajadorCodPlanilla').val().trim() +
        '&codPersonales=' + codPersonales;

    containerPage.maskContainer("Cargando");

    $('#dtMaestroTrabajadorDetalle').DataTable({
        "ajax": GetUrl("Ajax/Personal/GetReportePersonalTrabajadoresMaestro?" + ParametersDt)
        , timeout: 300000 // sets timeout to 3 seconds
        , "initComplete": function (settings, json) {
            $('#gridContainerMaestroTrabajador').removeClass('d-none');
            $("html, body").animate({
                scrollTop: $('#gridContainerMaestroTrabajador').offset().top
            }, 600, "linear", function () {
                containerPage.unmaskContainer();
            });
        }
        //, data: JSON.stringify(jsonData)
        , "bFilter": true
        , "paging": true
        , "iDisplayLength": 50
        , "bInfo": false
        //, "scrollY": "400px"
        , bDestroy: true
        , dom: 'Bfrtip'
        , buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5'
        ]
        , "language": {
            "url": '/js/language.js'
        }
        , "columns": [
            { "data": "cod_personal" },
            { "data": "trabajador" },
            { "data": "tipo_doc" },
            { "data": "numero_docid" },
            //{ "data": "estado_trabajador" },
            { "data": "sexo" },
            { "data": "estado_civil" },
            { "data": "nacionalidad" },
            { "data": "cod_ubigeo_nac" },
            { "data": "cod_afp" },
            { "data": "cuispp" },
            { "data": "fecha_afiliacion" },
            { "data": "flg_domicilio" },
            { "data": "discapacidad" },
            { "data": "nivel_academico" },
            { "data": "flg_madre_trabajadora" },
            { "data": "planilla" },
            { "data": "proyecto" },
            { "data": "unidadnegocio" },
            { "data": "ccosto" },
            { "data": "area" },
            { "data": "localidad" },
            { "data": "cargo" },
            { "data": "fecha_ingreso" },
            { "data": "motivo_ingreso" },
            { "data": "sueldo", render: $.fn.dataTable.render.number(',', '.', 2, '') },
            { "data": "num_cuenta_pago" },
            { "data": "banco" },
            { "data": "pago_cuenta" },
            { "data": "moneda_pago" },
            { "data": "tipo_trabajador" },
            { "data": "estado_laboral" },
            { "data": "essalud_vida" },
            { "data": "afiliacion_eps" },
            { "data": "sctr_salud" },
            { "data": "scrt_pension" },
            { "data": "fecha_sctr_inicio" },
            { "data": "fecha_sctr_fin" },
            { "data": "cod_vida_ley" },
            { "data": "tasa_vida_ley" },
            { "data": "sindicato" },
            { "data": "regimen_alternativo" },
            { "data": "jornada_maxima" },
            { "data": "horario_nocturno" },
            { "data": "alcance_cargo" },
            { "data": "asignacion_familiar" },
            { "data": "cod_vida_ley" },
            { "data": "flg_reloj" },
            { "data": "fotocheck" },
            { "data": "contrato" },
            { "data": "fecha_ingreso" },
            { "data": "fecha_liquidacion" },
            { "data": "cod_via" },
            { "data": "direccion" },
            { "data": "numero" },
            { "data": "interior" },
            { "data": "zona" },
            { "data": "departamento" },
            { "data": "provincia" },
            { "data": "distrito" },
            { "data": "cod_ubigeo" },
            { "data": "referencia" },
            { "data": "telefonos" },
            { "data": "telefono2" },
            { "data": "telefono3" },
            { "data": "fax" },
            { "data": "numero_hijos" },
            { "data": "fecha_cese" },
            { "data": "motivo_cese" },
            { "data": "tipo_comision_afp" },
            { "data": "flg_adelanto" },
            { "data": "flg_reloj" },
            { "data": "porcen_adelanto" },
            { "data": "monto_adelanto" },
        ]
    });
}

//#endregion




//#region CUMPLEAÑOS DEL MES
function PersonalCumpleDelMesInicializar() {

    $('#txtRptPersonalCumpleDelMesAnio').mask('0000');

    $('#btnRptPersonalCumpleDelMesAceptar').on('click', function (e) {
        e.preventDefault();

        PersonalCumpleDelMesGenerarReporte();
    });
}

function PersonalCumpleDelMesGenerarReporte() {

    var ParametersDt = 'codProyecto=' + $('#selRptPersonalCumpleDelMesProyecto').val().trim() +
        '&codUnidadNegocio=' + $('#selRptPersonalCumpleDelMesCodUnidadNegocio').val().trim() +
        '&codCentroCosto=' + $('#selRptPersonalCumpleDelMesCodCentroCosto').val().trim() +
        '&codArea=' + $('#selRptPersonalCumpleDelMesCodArea').val().trim() +
        '&codLocalidad=' + $('#selRptPersonalCumpleDelMesCodLocalidad').val().trim() +
        '&codPlanilla=' + $('#selRptPersonalCumpleDelMesCodPlanilla').val().trim() +
        '&mes=' + $('#selRptPersonalCumpleDelMesMes').val().trim() +
        '&estado=' + $('#selRptPersonalCumpleDelMesEstado').val().trim();



    containerPage.maskContainer("Cargando");

    $('#dtRptPersonalCumpleDelMes').DataTable({
        "ajax": GetUrl("Ajax/Personal/GetReportePersonalCumpleDelMes?" + ParametersDt)
        , "initComplete": function (settings, json) {

            $('#gridcontainerRptPersonalCumpleDelMes').removeClass('d-none');
            $("html, body").animate({
                scrollTop: $('#gridcontainerRptPersonalCumpleDelMes').offset().top
            }, 600, "linear", function () {
                containerPage.unmaskContainer();
            });
        }
        //, data: JSON.stringify(jsonData)
        , "bFilter": true
        , "paging": false
        , "bInfo": false
        , "scrollY": "400px"
        , bDestroy: true
        , dom: 'Bfrtip'
        , buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5'
        ]
        , "language": {
            "url": '/js/language.js'
        }
        , "columns": [

            {
                "data": "fecha_nacimiento",
                render: function (data, type, row) {
                    if (data == null || data.length < 10) return "";
                    var items = data.substring(0, 10).split('-');
                    return items[2] + "-" + items[1];
                }
            },
            { "data": "codigo" },
            { "data": "trabajador" },
            { "data": "estado" },
            {
                "data": "fecha_ingreso",
                render: function (data, type, row) {
                    if (data == null || data.length < 10) return "";
                    var items = data.substring(0, 10).split('-');
                    return items[2] + "/" + items[1] + "/" + items[0];
                }
            },
            {
                "data": "fecha_nacimiento",
                render: function (data, type, row) {
                    if (data == null || data.length < 10) return "";
                    var items = data.substring(0, 10).split('-');
                    return items[2] + "/" + items[1] + "/" + items[0];
                }
            },

            { "data": "cargo" },
        ]
    });
}

//#endregion

//#region PERSONAL POR GRUPO SANGUINEO
function PersonalPorGrupoSanguineoInicializar() {

    $('#txtRptPersonalPorGrupoSanguineoAnio').mask('0000');

    $('#btnRptPersonalPorGrupoSanguineoAceptar').on('click', function (e) {
        e.preventDefault();

        PersonalPorGrupoSanguineoGenerarReporte();
    });
}

function PersonalPorGrupoSanguineoGenerarReporte() {

    var ParametersDt = 'codProyecto=' + $('#selRptPersonalPorGrupoSanguineoProyecto').val().trim() +
        '&codCentroCosto=' + $('#selRptPersonalPorGrupoSanguineoCodCentroCosto').val().trim() +
        '&codUnidadNegocio=' + $('#selRptPersonalPorGrupoSanguineoCodUnidadNegocio').val().trim() +
        '&codArea=' + $('#selRptPersonalPorGrupoSanguineoCodArea').val().trim() +
        '&codLocalidad=' + $('#selRptPersonalPorGrupoSanguineoCodLocalidad').val().trim() +
        '&codPlanilla=' + $('#selRptPersonalPorGrupoSanguineoCodPlanilla').val().trim() +
        '&GrupoSanguineo=' + $('#selRptPersonalPorGrupoSanguineoGrupoS').val().trim();


    containerPage.maskContainer("Cargando");

    $('#dtRptPersonalPorGrupoSanguineo').DataTable({
        "ajax": GetUrl("Ajax/Personal/GetReportePersonalPorGrupoSanguineo?" + ParametersDt)
        , "initComplete": function (settings, json) {

            $('#gridcontainerRptPersonalPorGrupoSanguineo').removeClass('d-none');
            $("html, body").animate({
                scrollTop: $('#gridcontainerRptPersonalPorGrupoSanguineo').offset().top
            }, 600, "linear", function () {
                containerPage.unmaskContainer();
            });
        }
        //, data: JSON.stringify(jsonData)
        , "bFilter": true
        , "paging": false
        , "bInfo": false
        , "scrollY": "400px"
        , bDestroy: true
        , dom: 'Bfrtip'
        , buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5'
        ]
        , "columns": [

            { "data": "codigo" },
            { "data": "trabajador" },
            { "data": "tipodoc" },
            { "data": "nrodoc" },
            { "data": "grupo_sanguineo" },
        ]
    });
}

//#endregion

//#region PERSONAL POR AFILIACION AFP
function PersonalPorAfiliacionAfpInicializar() {

    $('#txtRptPersonalPorAfiliacionAfpFechaDesde, #txtRptPersonalPorAfiliacionAfpFechaHasta').datepicker({
        autoclose: true,
        format: "dd/mm/yyyy"
    });
    $('#txtRptPersonalPorAfiliacionAfpFechaDesde').mask('00');
    $('#txtRptPersonalPorAfiliacionAfpFechaHasta').mask('00');

    $('#txtRptPersonalPorAfiliacionAfpAnio').mask('0000');

    $('#btnRptPersonalPorAfiliacionAfpAceptar').on('click', function (e) {
        e.preventDefault();

        PersonalPorAfiliacionAfpGenerarReporte();
    });


    $("#selRptPersonalPorAfiliacionAfpCesados").change(function () {

        var val = $('#selRptPersonalPorAfiliacionAfpCesados').val() == 'A';

        $('#txtRptPersonalPorAfiliacionAfpFechaDesde').attr('disabled', val);
        $('#txtRptPersonalPorAfiliacionAfpFechaHasta').attr('disabled', val);
    });
}

function PersonalPorAfiliacionAfpGenerarReporte() {

    var ParametersDt = 'codProyecto=' + $('#selRptPersonalPorAfiliacionAfpProyecto').val().trim() +
        '&codCentroCosto=' + $('#selRptPersonalPorAfiliacionAfpCodCentroCosto').val().trim() +
        '&codUnidadNegocio=' + $('#selRptPersonalPorAfiliacionAfpCodUnidadNegocio').val().trim() +
        '&codArea=' + $('#selRptPersonalPorAfiliacionAfpCodArea').val().trim() +
        '&codLocalidad=' + $('#selRptPersonalPorAfiliacionAfpCodLocalidad').val().trim() +
        '&codPlanilla=' + $('#selRptPersonalPorAfiliacionAfpCodPlanilla').val().trim() +
        '&estado=' + $('#selRptPersonalPorAfiliacionAfpCesados').val().trim() +
        '&cesadosDesde=' + $('#txtRptPersonalPorAfiliacionAfpFechaDesde').val().trim() +
        '&cesadosHasta=' + $('#txtRptPersonalPorAfiliacionAfpFechaHasta').val().trim();

    containerPage.maskContainer("Cargando");

    $('#dtRptPersonalPorAfiliacionAfp').DataTable({
        "ajax": GetUrl("Ajax/Personal/GetReportePersonalPorAfiliacionAfp?" + ParametersDt)
        , "initComplete": function (settings, json) {

            $('#gridcontainerRptPersonalPorAfiliacionAfp').removeClass('d-none');
            $("html, body").animate({
                scrollTop: $('#gridcontainerRptPersonalPorAfiliacionAfp').offset().top
            }, 600, "linear", function () {
                containerPage.unmaskContainer();
            });
        }
        //, data: JSON.stringify(jsonData)
        , "bFilter": true
        , "paging": false
        , "bInfo": false
        , "scrollY": "400px"
        , bDestroy: true
        , dom: 'Bfrtip'
        , buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5'
        ]
        , "columns": [

            //BD
            { "data": "codigo_afp" },
            { "data": "afp" },
            { "data": "codigo" },
            { "data": "trabajador" },
            { "data": "cuspp" },
            { "data": "mixta" },
            {
                "data": "fecha_afiliacion",
                render: function (data, type, row) {
                    if (data == null || data.length < 10) return "";
                    var items = data.substring(0, 10).split('-');
                    return items[2] + "/" + items[1] + "/" + items[0];
                }
            },
            {
                "data": "fecha_Ingreso",
                render: function (data, type, row) {
                    if (data == null || data.length < 10) return "";
                    var items = data.substring(0, 10).split('-');
                    return items[2] + "/" + items[1] + "/" + items[0];
                }
            },
        ]
    });
}

//#endregion

//#region PERSONAL POR CUENTAS DE PAGOS 
function PersonalPorCtasPagosFinDeMesInicializar() {

    $('#btnRptPersonalPorCtasPagosFinDeMesAceptar').on('click', function (e) {
        e.preventDefault();

        PersonalPorCtasPagosFinDeMesGenerarReporte();
    });
}

function PersonalPorCtasPagosFinDeMesGenerarReporte() {

    var ParametersDt = 'codProyecto=' + $('#selRptPersonalPorCtasPagosFinDeMesProyecto').val().trim() +
        '&codCentroCosto=' + $('#selRptPersonalPorCtasPagosFinDeMesCodCentroCosto').val().trim() +
        '&codUnidadNegocio=' + $('#selRptPersonalPorCtasPagosFinDeMesCodUnidadNegocio').val().trim() +
        '&codArea=' + $('#selRptPersonalPorCtasPagosFinDeMesCodArea').val().trim() +
        '&codLocalidad=' + $('#selRptPersonalPorCtasPagosFinDeMesCodLocalidad').val().trim() +
        '&codPlanilla=' + $('#selRptPersonalPorCtasPagosFinDeMesCodPlanilla').val().trim() +
        '&Consolidado=' + ($('#chbRptPersonalPorCtasPagosFinDeMesConsolidado').is(':checked') == true ? 'C' : 'T');


    containerPage.maskContainer("Cargando");

    $('#dtRptPersonalPorCtasPagosFinDeMes').DataTable({
        "ajax": GetUrl("Ajax/Personal/GetReportePersonalPorCtasPagosFinDeMes?" + ParametersDt)
        , "initComplete": function (settings, json) {

            $('#gridcontainerRptPersonalPorCtasPagosFinDeMes').removeClass('d-none');
            $("html, body").animate({
                scrollTop: $('#gridcontainerRptPersonalPorCtasPagosFinDeMes').offset().top
            }, 600, "linear", function () {
                containerPage.unmaskContainer();
            });
        }
        //, data: JSON.stringify(jsonData)
        , "bFilter": true
        , "paging": false
        , "bInfo": false
        , "scrollY": "400px"
        , bDestroy: true
        , dom: 'Bfrtip'
        , buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5'
        ]
        , "language": {
            "url": '/js/language.js'
        }
        , "columns": [

            { "data": "codigo_banco" },
            { "data": "banco" },
            { "data": "codigo" },
            { "data": "trabajador" },
            { "data": "numero_cuenta" },
            { "data": "moneda" },
        ]
    });
}

//#endregion

//#region PERSONAL POR CUENTAS DE CTS 
function PersonalPorCtasPagosCtsInicializar() {

    $('#txtRptPersonalPorCtasPagosCtsAnio').mask('0000');

    $('#btnRptPersonalPorCtasPagosCtsAceptar').on('click', function (e) {
        e.preventDefault();

        PersonalPorCtasPagosCtsGenerarReporte();
    });
}

function PersonalPorCtasPagosCtsGenerarReporte() {

    var ParametersDt = 'codProyecto=' + $('#selRptPersonalPorCtasPagosCtsProyecto').val().trim() +
        '&codCentroCosto=' + $('#selRptPersonalPorCtasPagosCtsCodCentroCosto').val().trim() +
        '&codUnidadNegocio=' + $('#selRptPersonalPorCtasPagosCtsCodUnidadNegocio').val().trim() +
        '&codArea=' + $('#selRptPersonalPorCtasPagosCtsCodArea').val().trim() +
        '&codLocalidad=' + $('#selRptPersonalPorCtasPagosCtsCodLocalidad').val().trim() +
        '&codPlanilla=' + $('#selRptPersonalPorCtasPagosCtsCodPlanilla').val().trim() +
        '&Consolidado=' + ($('#chbRptPersonalPorCtasPagosCtsConsolidado').is(':checked') == true ? 'C' : 'T');

    containerPage.maskContainer("Cargando");

    $('#dtRptPersonalPorCtasPagosCts').DataTable({
        "ajax": GetUrl("Ajax/Personal/GetReportePersonalPorCtasPagosCts?" + ParametersDt)
        , "initComplete": function (settings, json) {

            $('#gridcontainerRptPersonalPorCtasPagosCts').removeClass('d-none');
            $("html, body").animate({
                scrollTop: $('#gridcontainerRptPersonalPorCtasPagosCts').offset().top
            }, 600, "linear", function () {
                containerPage.unmaskContainer();
            });
        }
        //, data: JSON.stringify(jsonData)
        , "bFilter": false
        , "paging": false
        , "bInfo": false
        , "scrollY": "400px"
        , bDestroy: true
        , dom: 'Bfrtip'
        , buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5'
        ]
        , "language": {
            "url": '/js/language.js'
        }
        , "columns": [

            { "data": "codigo_banco" },
            { "data": "banco" },
            { "data": "codigo" },
            { "data": "trabajador" },
            { "data": "numero_cuenta" },
            { "data": "moneda" },
        ]
    });
}

//#endregion

//#region PERSONAL POR TRASLADO ENTRE EMPRESAS 
function PersonalPorTrasladoEmpresaInicializar() {

    $('#btnRptPersonalPorTrasladoEmpresaAceptar').on('click', function (e) {
        e.preventDefault();

        PersonalPorTrasladoEmpresaGenerarReporte();
    });
}

function PersonalPorTrasladoEmpresaGenerarReporte() {

    var ParametersDt = 'codProyecto=' + $('#selRptPersonalPorTrasladoEmpresaProyecto').val() +
        '&codCentroCosto=' + $('#selRptPersonalPorTrasladoEmpresaCodCentroCosto').val() +
        '&codUnidadNegocio=' + $('#selRptPersonalPorTrasladoEmpresaCodUnidadNegocio').val() +
        '&codArea=' + $('#selRptPersonalPorTrasladoEmpresaCodArea').val() +
        '&codLocalidad=' + $('#selRptPersonalPorTrasladoEmpresaCodLocalidad').val() +
        '&codPlanilla=' + $('#selRptPersonalPorTrasladoEmpresaCodPlanilla').val();
    //'&mes=' + $('#selRptPersonalPorAfiliacionAfpMes').val().trim() +
    //'&anio=' + $('#txtRptPersonalPorAfiliacionAfpAnio').val().trim();

    containerPage.maskContainer("Cargando");

    var jsonParam = {
        codProyecto: $('#selRptPersonalPorTrasladoEmpresaProyecto').val(),
        codCentroCosto: $('#selRptPersonalPorTrasladoEmpresaCodCentroCosto').val(),
        codUnidadNegocio: $('#selRptPersonalPorTrasladoEmpresaCodUnidadNegocio').val(),
        codArea: $('#selRptPersonalPorTrasladoEmpresaCodArea').val(),
        codLocalidad: $('#selRptPersonalPorTrasladoEmpresaCodLocalidad').val(),
        codPlanilla: $('#selRptPersonalPorTrasladoEmpresaCodPlanilla').val(),
    };

    $('#dtRptPersonalPorTrasladoEmpresa').DataTable({
        ajax: portal + "/ajax/personal/GetReportePersonalPorTrasladoEmpresa?" + $.param(jsonParam),
        initComplete: function (settings, json) {

            $('#gridcontainerRptPersonalPorTrasladoEmpresa').removeClass('d-none');
            $("html, body").animate({
                scrollTop: $('#gridcontainerRptPersonalPorTrasladoEmpresa').offset().top
            }, 500, "linear", function () {
                containerPage.unmaskContainer();
            });
        },
        //, data: JSON.stringify(jsonData),
        //bFilter: false,
        //paging: false,
        //bInfo: false,
        //scrollY: "400px",
        dom: 'Bfrtip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5'
        ],
        columns: [
            { data: "cod_trabajador" },
            //{ data: "dni" },
            { data: "paterno" },
            { data: "materno" },
            { data: "nombre" },
            { data: "cargo_a" },
            { data: "unidad_negocio_a" },
            { data: "centro_costo_a" },
            { data: null, defaultContent: '' },
            { data: null, defaultContent: '' },
            { data: "cargo_n" },
            { data: "unidad_negocio_n" },
            { data: "centro_costo_n" },
            { data: null, defaultContent: '' },
            { data: null, defaultContent: '' },
        ]
    });
}

//#endregion

//#region LISTADO DE VIENES ACTIVOS Y PENDIENTES 
function LBienesActivosPendientesInicializar() {

    $('#btnRptLBienesActivosPendientesAceptar').on('click', function (e) {
        e.preventDefault();

        LBienesActivosPendientesGenerarReporte();
    });
}

function LBienesActivosPendientesGenerarReporte() {

    var ParametersDt = 'codProyecto=' + $('#selRptPersonalPorTrasladoEmpresaProyecto').val().trim() +
        '&codCentroCosto=' + $('#selRptPersonalPorTrasladoEmpresaCodCentroCosto').val().trim() +
        '&codUnidadNegocio=' + $('#selRptPersonalPorTrasladoEmpresaCodUnidadNegocio').val().trim() +
        '&codArea=' + $('#selRptPersonalPorTrasladoEmpresaCodArea').val().trim() +
        '&codLocalidad=' + $('#selRptPersonalPorTrasladoEmpresaCodLocalidad').val().trim() +
        '&codPlanilla=' + $('#selRptPersonalPorTrasladoEmpresaCodPlanilla').val().trim() +
        '&estado=' + $('#selRptLBienesActivosPendientesEstado').val().trim();



    containerPage.maskContainer("Cargando");

    $('#dtRptLBienesActivosPendientes').DataTable({
        "ajax": GetUrl("Ajax/Personal/GetReporteLBienesActivosPendientes?" + ParametersDt)
        , "initComplete": function (settings, json) {

            $('#gridcontainerRptLBienesActivosPendientes').removeClass('d-none');
            $("html, body").animate({
                scrollTop: $('#gridcontainerRptLBienesActivosPendientes').offset().top
            }, 600, "linear", function () {
                containerPage.unmaskContainer();
            });
        }
        //, data: JSON.stringify(jsonData)
        , "bFilter": true
        , "paging": false
        , "bInfo": false
        , "scrollY": "400px"
        , bDestroy: true
        , dom: 'Bfrtip'
        , buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5'
        ]
        , "language": {
            "url": '/js/language.js'
        }
        , "columns": [

            { "data": "bien_item" },
            { "data": "bien_nombre" },
            { "data": "bien_estado" },
            { "data": "cod_trabajador" },
            { "data": "trabajador" },
            { "data": "cargo" },
        ]
    });
}

//#endregion

//#region LISTADO DE PERSONAL POR TALLAS DE VESTIR 
function LTallasDeVestirPersonalInicializar() {

    $('#btnRptLTallasDeVestirPersonalAceptar').on('click', function (e) {
        e.preventDefault();

        LTallasDeVestirPersonalGenerarReporte();
    });

    $('#dtRptLTallasDeVestirPersonalFiltro').on('click', 'tr', function () {
        var ths = $(this);
        ths.addClass('selected');
    });


    $("#selRptLTallasDeVestirPersonalProyecto,#selRptLTallasDeVestirPersonalCodCentroCosto,#selRptLTallasDeVestirPersonalCodUnidadNegocio," +
        "#selRptLTallasDeVestirPersonalCodArea,#selRptLTallasDeVestirPersonalCodLocalidad,#selRptLTallasDeVestirPersonalCodPlanilla").change(function () {

            LTallasDeVestirPersonalFiltro();
        });
}


function LTallasDeVestirPersonalFiltro() {

    var ParametersDt = 'codProyecto=' + $('#selRptLTallasDeVestirPersonalProyecto').val().trim() +
        '&codCentroCosto=' + $('#selRptLTallasDeVestirPersonalCodCentroCosto').val().trim() +
        '&codUnidadNegocio=' + $('#selRptLTallasDeVestirPersonalCodUnidadNegocio').val().trim() +
        '&codArea=' + $('#selRptLTallasDeVestirPersonalCodArea').val().trim() +
        '&codLocalidad=' + $('#selRptLTallasDeVestirPersonalCodLocalidad').val().trim() +
        '&codPlanilla=' + $('#selRptLTallasDeVestirPersonalCodPlanilla').val().trim();


    $('#dtRptLTallasDeVestirPersonalFiltro').DataTable({
        "ajax": GetUrl("Ajax/Personal/GetReporteLTallasDeVestirPersonalFiltro?" + ParametersDt)
        , "initComplete": function (settings, json) {

            $('#chkRptLTallasDeVestirPersonalFiltro').prop('checked', false);
            $('#chkRptLTallasDeVestirPersonalFiltro').click();

        }
        //, data: JSON.stringify(jsonData)
        , "bFilter": true
        , "paging": false
        , "bInfo": false
        , "scrollY": "400px"
        , bDestroy: true
        , dom: 'Bfrtip'
        , buttons: [
            //'copyHtml5',
            //'excelHtml5',
            //'csvHtml5'
        ]
        , "language": {
            "url": '/js/language.js'
        }
        , "columns": [

            { "data": "cod_trabajador" },
            { "data": "trabajador" },

        ]
    });
}


function LTallasDeVestirPersonalGenerarReporte() {

    var listCodTrabajadores = '';
    if ($("#chkRptLTallasDeVestirPersonalFiltro").is(':checked') == false) {
        $('#dtRptLTallasDeVestirPersonalFiltro tr.selected').each(function (index, value) {

            try {
                if (listCodTrabajadores.length > 0)
                    listCodTrabajadores += ",";

                var table = $('#dtRptLTallasDeVestirPersonalFiltro').DataTable();
                if (table != null) {
                    var data = table.row(this).data();
                    listCodTrabajadores += data.cod_trabajador;
                }
            }
            catch (e) {
            }

        });

        if (listCodTrabajadores == '') {
            UIMensaje.COLABORADORM();
            return;
        }
    }


    var ParametersDt = 'codProyecto=' + $('#selRptLTallasDeVestirPersonalProyecto').val().trim() +
        '&codCentroCosto=' + $('#selRptLTallasDeVestirPersonalCodCentroCosto').val().trim() +
        '&codUnidadNegocio=' + $('#selRptLTallasDeVestirPersonalCodUnidadNegocio').val().trim() +
        '&codArea=' + $('#selRptLTallasDeVestirPersonalCodArea').val().trim() +
        '&codLocalidad=' + $('#selRptLTallasDeVestirPersonalCodLocalidad').val().trim() +
        '&codPlanilla=' + $('#selRptLTallasDeVestirPersonalCodPlanilla').val().trim() +
        '&polo=' + $('#selRptLTallasDeVestirPersonalPolo').val().trim() +
        '&camisa=' + $('#selRptLTallasDeVestirPersonalCamisa').val().trim() +
        '&casaca=' + $('#selRptLTallasDeVestirPersonalCasaca').val().trim() +
        '&pantalon=' + $('#selRptLTallasDeVestirPersonalPantalon').val().trim() +
        '&zapato=' + $('#selRptLTallasDeVestirPersonalZapato').val().trim() +
        '&zapatilla=' + $('#selRptLTallasDeVestirPersonalZapatilla').val().trim() +
        '&CodTrabajadores=' + listCodTrabajadores;


    containerPage.maskContainer("Cargando");

    $('#dtRptLTallasDeVestirPersonal').DataTable({
        "ajax": GetUrl("Ajax/Personal/GetReporteLTallasDeVestirPersonal?" + ParametersDt)
        , "initComplete": function (settings, json) {

            $('#gridcontainerRptLTallasDeVestirPersonal').removeClass('d-none');
            $("html, body").animate({
                scrollTop: $('#gridcontainerRptLTallasDeVestirPersonal').offset().top
            }, 600, "linear", function () {
                containerPage.unmaskContainer();
            });
        }
        //, data: JSON.stringify(jsonData)
        , "bFilter": true
        , "paging": false
        , "bInfo": false
        , "scrollY": "400px"
        , bDestroy: true
        , dom: 'Bfrtip'
        , buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5'
        ]
        , "language": {
            "url": '/js/language.js'
        }
        , "columns": [

            { "data": "codigo" },
            { "data": "trabajador" },
            { "data": "sexo" },
            { "data": "casaca" },
            { "data": "camisa" },
            { "data": "polo" },
            { "data": "pantalon" },
            { "data": "zapato" },
            { "data": "zapatilla" },


        ]
    });
}

//#endregion




//****************************************************************************************************************************************************




//PERSONALES
var GrabarDatosPersonalesAdmin = function GrabarDatosPersonalesAdmin() {
    var v_sexo;
    if ($('#rbMasculino').prop('checked')) v_sexo = 1; else if ($('#rbFemenino').prop('checked')) v_sexo = 2; else {
        MsgBox("Seleccione el sexo.", "warning", true);
        return;
    }
    if ($('#txtFechaNac').val().length > 0) {
        var edad = ValidateFechaEdad('txtFechaNac');
        if (edad < edadMayoria) {
            MsgBox("No se puede ingresar a un menor de edad, el trabajador tiene " + edad + " años de edad.", "warning", true);
            $('#txtFechaNac_new').focus();
            return;
        }
    }
    if ($('#selAFP').val() != null) {
        if ($("#selAFP option:selected").text().indexOf('AFP') > -1) {
            if ($('#rbRemuneracion').prop('checked')) tipoComision = "1"; else if ($('#rbMixta').prop('checked')) tipoComision = "2"; else {
                MsgBox("Seleccione el tipo de comisión AFP.", "warning", true);
                return;
            }
        }
    }

    var obj = {
        codEntidad: g_codEntidad,
        fechaNacimiento: $('#txtFechaNac').val(),
        nroHijos: $('#txtNroHijos').val(),
        codEstadoCivil: $('#selEstadoCivil').val(),
        codUbigeoNac: $('#selDistrito_Personal').val(),
        tipoDocIdentidad: $('#selTipoDocIdent').val(),
        nroDocIdentidad: $('#txtNroDocIdent').val(),
        csse: $('#txtCsse').val(),
        codAFP: $('#selAFP').val(),
        cussp: $('#txtCussp').val(),
        fechaAfiliación: $('#txtFechaAfiliacion').val(),
        tipoComisionAFP: $('input[name="groupTipoComisionAFP"]:checked').val(), //$('input[name="groupTipoComisionAFP"]:checked').val();
        flgDiscapacidad: $('#chkDiscapacitado').prop('checked') ? 'S' : 'N',
        flgDomiciliado: $('#chkDomiciliado').prop('checked') ? 'S' : 'N',
        flgMadreResponsable: $('#chkMadreRespFamiliar').prop('checked') ? 'S' : 'N',
        codNacionalidad: $('#selNacionalidad').val(),
        codNivelAcademico: $('#selNivelAcademico').val(),
        codProfesion: $('#selProfesion').val(),
        codBrevete: $('#selTipoBrevete').val(),
        nroBrevete: $('#txtNroBrevete').val(),
        sexo: v_sexo,
        talla: toInt($('#txtTalla').val()),
        colorPiel: $('#txtColorPiel').val(),
        peso: toInt($('#txtPeso').val()),
        ojos: $('#txtOjos').val(),
        pelo: $('#txtPelo').val(),
        alergias: $('#txtAlergias').val(),
        ojoIz: $('#txtOjoIzq').val(),
        ojoDe: $('#txtOjoDer').val(),
        oidoIz: $('#txtOidosIzq').val(),
        oidoDe: $('#txtOidosDer').val(),
        polo: $('#selTallaPolo').val(),
        camisa: $('#selTallaCamisa').val(),
        pantalon: $('#selTallaPantalon').val(),
        casaca: $('#selTallaCasaca').val(),
        zapato: $('#selTallaZapato').val(),
        zapatilla: $('#selTallaZapatilla').val(),
        tipoSangre: $('#selGrupoSanguineo').val()
    };
    UIAjax.runFunctionAsync('Personal', 'PostDatosPersonalesLegajo', obj);
}; //PERSONALES
//*********************************************************************************
//*********************************************************************************
//BIENES


var Save_Bienes = function Save_Bienes(accion, item) {
    var costo_mn = 0;
    var costo_me = 0;
    if ($('#selectCostoMonedaB').val() == 'N') {
        costo_mn = $('#txt_BIENES_CostoMn').val();
    }
    else {
        costo_me = $('#txt_BIENES_CostoMn').val();
    }

    var obj = {
        accion: accion,
        cod_personal: g_codPersonal,
        item: $('#txt_BIENES_Item').val(),
        nombre: $('#txt_BIENES_Nombre').val(),
        estado: $('#sel_BIENES_Estado').val(),
        fecha_entrega: $('#txt_BIENES_FechaEntrega').val(),
        fecha_devolucion: $('#txt_BIENES_FechaDevolucion').val(),
        costo_mn: costo_mn,
        costo_me: costo_me,
        //costo_me: $('#txt_BIENES_CostoMe').val(),
        cod_doc: $('#sel_BIENES_CodDocEntrega').val(),
        fecha_doc: $('#txt_BIENES_FechaDocEntrega').val(),
        numero_doc: $('#txt_BIENES_NroDocEntrega').val(),
        costo_repo_mn: $('#txt_BIENES_CostoReposMn').val(),
        costo_repo_me: $('#txt_BIENES_CostoReposMe').val(),
        descripcion: $('#txt_BIENES_Descripcion').val()
    };
    UIAjax.runFunctionAsync('Personal', 'PostBienesLegajo', obj, function () {
        ClearInput('Bi');
        GetBienes_Admin(g_codPersonal);
    }, null, function () {
        $('#btnSaveBienes').removeAttr('disabled');
    });
};

var GetBienes_Admin = function GetBienes_Admin(pCodPersonal) {
    //var jsonData = {
    //    cod_personal: pCodPersonal
    //};
    $.ajax({
        type: "GET",
        url: portal + "/Personal/GetBienesPersonal?codPersonal=" + pCodPersonal/*.concat(pCodPersonal)*/,
        //data: JSON.stringify(jsonData),
        cache: false,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function success(data) {
            var obj = data;
            var container = $("#tbodyBienes");
            container.empty();
            $.each(obj, function (index, value) {
                var html = "<tr onclick=\"GetDetalleBienes_Admin(this);\" data-item=\"".concat(value.item, "\">\n                                        <td class=\"text-center\">").concat(value.item, "</td>\n                                        <td class=\"text-center\">").concat(value.nombre, "</td>\n                                        <td class=\"text-center\">").concat(value.desc_estado, "</td>\n                                  </tr>");
                container.append(html);
            });
        },
        error: function error(response) {
            console.log(response);
        }
    });
};

var GetDetalleBienes_Admin = function GetDetalleBienes_Admin(p_tr) {
    g_accion_bienes = 'E';
    g_num_bien = $(p_tr).data('item');
    $('#txtItem_Bienes').prop('disabled', true);
    $("#tbodyBienes tr").removeClass('selected');
    $(p_tr).addClass('selected');
    var jsonData = {
        cod_personal: g_codPersonal,
        numero: $(p_tr).data('item')
    };
    $.ajax({
        type: "GET",
        url: portal + "/Personal/GetDetalleBienesPersonal?codPersonal=".concat(g_codPersonal, "&numero=").concat(g_num_bien),
        data: JSON.stringify(jsonData),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function success(data) {
            var obj = data;

            if (obj[0].estado == 'E') {
                $('#btnViewConstancia').text('Constancia Entrega');
                $('#btnViewConstancia').attr('on-state', obj[0].Estado);
                $('#btnViewConstancia').attr('on-item', obj[0].item);
                if ($('#btnViewConstancia').hasClass('d-none')) $('#btnViewConstancia').removeClass('d-none');
            } else if (obj[0].estado == 'D') {
                $('#btnViewConstancia').text('Constancia Devolución');
                $('#btnViewConstancia').attr('on-state', obj[0].Estado);
                $('#btnViewConstancia').attr('on-item', obj[0].item);
                if ($('#btnViewConstancia').hasClass('d-none')) $('#btnViewConstancia').removeClass('d-none');
            } else {
                if (!$('#btnViewConstancia').hasClass('d-none')) $('#btnViewConstancia').addClass('d-none');
            }

            $('#txt_BIENES_Item').val(obj[0].item);
            $('#txt_BIENES_Nombre').val(obj[0].nombre);
            $('#sel_BIENES_Estado').val(obj[0].estado);
            $('#txt_BIENES_FechaEntrega').val(obj[0].fecha_entrega);
            $('#txt_BIENES_FechaDevolucion').val(obj[0].fecha_devolucion);

            var costo_mn = obj[0].costo_mn;
            var costo_me = obj[0].costo_me;
            if (costo_mn > 0) {
                $('#txt_BIENES_CostoMn').val(costo_mn);
                $('#selectCostoMonedaB').val('N');
            }
            else {
                $('#txt_BIENES_CostoMn').val(costo_me);
                $('#selectCostoMonedaB').val('E');
            }
            //$('#txt_BIENES_CostoMn').val(obj[0].costo_mn);
            //$('#txt_BIENES_CostoMe').val(obj[0].costo_me);

            $('#sel_BIENES_CodDocEntrega').val(obj[0].cod_documento);
            $('#txt_BIENES_FechaDocEntrega').val(obj[0].fecha_documento);
            $('#txt_BIENES_NroDocEntrega').val(obj[0].numero_documento);
            $('#txt_BIENES_CostoReposMn').val(obj[0].costo_reposicion_mn);
            $('#txt_BIENES_CostoReposMe').val(obj[0].costo_reposicion_me);
            $('#txt_BIENES_Descripcion').val(obj[0].descripcion);
        },
        error: function error(response) {
            console.log(response);
        }
    });
};

var EliminarBienes_Admin = function EliminarBienes_Admin(pItem) {
    MsgBox('question', async function () {
        var jsonParam = {
            codPersonal: g_codPersonal,
            item: pItem
        };
        await UILoadingDefault('show');
        UIAjax.runFunctionAsync('Personal', 'DeleteBienesLegajo', jsonParam, function () {
            GetBienes_Admin(g_codPersonal);
            ClearInput('Bi');
            g_num_bien = '';
            rpta = 1;
        }, null, function () {
            rpta = -1;
        });
        UILoadingDefault('hide');
    });    
}; //BIENES
//*********************************************************************************
//*********************************************************************************
//MERITOS Y DEMERITOS


$('#btnAddMeritosDemeritos').on('click', function (e) {
    e.preventDefault();
    ClearInput('Me');
    $('#txtItem_MeritosDemeritos').prop('disabled', true);
    $("#tbodyMeritosDemeritos tr").removeClass('selected');
    g_accion_merito = 'N';
    g_item_merito = 0;
});
$('#btnRemoveMeritosDemeritos').on('click', function (e) {
    e.preventDefault();
    if (g_item_merito == 0) {
        UIMensaje.REGISTRO();
        return;
    }
    EliminarMeritoDemerito(g_item_merito);
});
$('#btnSaveMeritosDemeritos').on('click', function (e) {
    event.preventDefault();
    if (validaDatosTabMeritFalt() == 1) {
        return;
    }
    GrabarMeritosDemeritos();
}); //MERITOS Y DEMERITOS
//*********************************************************************************
