﻿const UISleep = m => new Promise(r => setTimeout(r, m));
const FFecha1 = "dd/mm/yyyy";
const FFecha2 = "dd/MM/yyyy";
const FFecha3 = "DD/MM/YYYY";
const jsonButtonClass = { glow: 'erp-glowing-red-30 text-white', default: 'btn-primary' };
const jsonBorderClass = { glow: 'erp-border-dance-blue', default: '' };

var Entity = {
	UI: {
		Periodo: typeof getPeriodo === "function" ? getPeriodo() : null,
		Author: "https://corp-goldtech.com/",
		CodigoDeEmpresa: null,
		CodigoDePersonal: null,
		Portal: null,
		Usuario: null,
		TipoDeUsuario: null,
		Fecha: null,
		Ano: null,
		initial_catalog: null,
		CodigoDeTipoPlanilla: null,
	},
	SSRS: {
		URLService: null,
		URLPortal: null,
		ControlAsistencia: null,
	}
};

const UI = {
	////MAL
	//type: function (data) {
	//	var __type = typeof data;
	//	if (!isNaN(data)) {
	//		__type = 'number';
	//	}
	//	else if (data.constructor === Array) {
	//		__type = 'array';
	//	}
	//	else if (data.constructor === Date) {
	//		__type = 'date';
	//	}
	//	else if (data == null) {
	//		__type = 'null';
	//	}
	//	else if (data instanceof RegExp) {
	//		__type = 'regexp';
	//	}
	//	return __type;
	//},
	typeCA: function (data) {
		//console.log(data);
		var __string = new String(data);
		var __value = __string.trim();
		var __type = new String({}.toString.call(data).slice(8, -1)).toLowerCase().toString();
		//console.log(__type);
		if (data != null && __value.length > 0) {
			if (__type == 'string') {
				var AStringBool = [
					"S", "SÍ", "SI", "Sí", "Si", "sí", "si", "1", "YES", "Yes", "yes", "ON", "On", "on", "true", "TRUE", "True",
					"N", "NO", "No", "no", "0", "NOT", "Not", "not", "OFF", "Off", "off", "false", "FALSE", "False",
				];
				if (AStringBool.includes(data)) {
					__type = 'boolean';
				}
			}
			if (!isNaN(data) && __value.left(1) != '0') {
				__type = 'number';
			}
			//if (__type == 'string') {
			//}
		}
		else {
			__type = 'null';
		}
		//console.log(__type);
		return __type;
	},
	isEmptyObject: function (data) {
		return this.isObject(data) && Object.keys(data).length;
	},
	//getType: function (val) {
	//	return Object.prototype.toString.call(val);
	//},
	//getTypeName: function (val) {
	//	return {}.toString.call(val);
	//	//return {}.toString.call(val).slice(8, -1);
	//},
	isDefined: function (val) {
		return val !== void 0 || typeof val !== 'undefined';
	},
	hasOwnProp: function (obj, val) {
		return Object.prototype.hasOwnProperty.call(obj, val);
	},
	isJSON: function (data) {
		//var tuple = [false, ""];
		//let tuple = ["Bob", 24]
		let valido = false, objeto = null;
		if (!data) valido = false;
		//var [a, b] = getTuple();
		try {
			objeto = JSON.parse(data);
			if (objeto && typeof objeto === `object`) {
				valido = true;
			}
			//console.log(data);
			//console.log(objeto);
			//console.log(valido);
		} catch (e) {
			//console.log(e);
			valido = false;
		}
		return [valido, objeto];
	}
}

const UIClipboard = {
	setText: function (text) {
		if (window.clipboardData && window.clipboardData.setData) {
			// Internet Explorer-specific code path to prevent textarea being shown while dialog is visible.
			return window.clipboardData.setData("Text", text);
		}
		else if (document.queryCommandSupported && document.queryCommandSupported("copy")) {
			var textarea = document.createElement("textarea");
			textarea.textContent = text;
			textarea.style.position = "fixed";  // Prevent scrolling to bottom of page in Microsoft Edge.
			document.body.appendChild(textarea);
			textarea.select();
			try {
				return document.execCommand("copy");  // Security exception may be thrown by some browsers.
			}
			catch (ex) {
				console.warn("Copy to clipboard failed.", ex);
				return false;
			}
			finally {
				document.body.removeChild(textarea);
			}
		}
	}
};

var UIEmpty = function (p, v) {
	var result = false;

	if (p === "" || p === null || p === undefined) {
		result = true;
	}

	if ($.isPlainObject(p)) {
		if ($.isEmptyObject(p)) {
			result = true;
		}
	}
	if ($.type(p) == "string") {
		//console.log('es string');
	}

	if (result && v != null && v != undefined) {
		result = v;
	}
	if (!result) {
		if (v != null && v != undefined) {
			result = p;
		}
	}

	return result;
};

var UIFnEmpty = function (p) {
	var result = true;
	if (!UIEmpty(p)) {
		if (typeof p === "function") {
			result = false;
		}
	}
	return result;
}

var UIParam = function (jsonObj, KeyChar) {
	var STRParam = "";
	KeyChar = UIEmpty($.trim(KeyChar), '&');
	STRParam = $.param(jsonObj);
	if (KeyChar != "&") {
		STRParam = STRParam.replaceAll('&', KeyChar);
	}
	return STRParam;
};

var UIString = {
	Replace: function (Str, Key, SReplace) {
		var STRValue = UIEmpty($.trim(Str), "");
		Key = UIEmpty(Key, "");
		SReplace = UIEmpty(SReplace, "");
		STRValue = STRValue.replaceAll(Key, SReplace);
		return STRValue;
	}
}

var UIURLParameters = {
	fromJson: function (jsonParam) {
		var url = Object.keys(jsonParam).map(function (k) {
			return encodeURIComponent(k) + '=' + encodeURIComponent(jsonParam[k])
		}).join('&');
		return url;
	}
}
//|| jQuery.isEmptyObject(p)

//AÚN NO FUNCIONA, NO USARLO
var UIXMLHttpRequest = {
	invokeFunctionAsync: function (Class, Method, Params) {
		return new Promise(function (resolve, reject) {
			try {
				//let xhr = new XMLHttpRequest();
				//xhr.open('POST', portal + `/${Class}/${Method}`);
				//xhr.setRequestHeader('Content-Type', 'application/json');
				//xhr.responseType = "json";
				//xhr.onload = function () {
				//    if (xhr.status === 200) {
				//        resolve(xhr);
				//        //resolve(JSON.parse(xhr.response));
				//        //Do something
				//    }
				//    else {
				//        reject(null)
				//        //alert('Request failed.  Returned status of ' + xhr.status);
				//    }
				//};
				//xhr.send(Params);
				const xhr = new XMLHttpRequest();
				//xhr.responseType = 'json';
				// listen for `load` event
				//xhr.onload = () => {
				//    // print JSON response
				//    if (xhr.status >= 200 && xhr.status < 300) {
				//        // parse JSON
				//        const response = JSON.parse(xhr.responseText);
				//        //console.log(response);
				//        resolve(response);
				//    }
				//    else {
				//        resolve(null);
				//    }
				//};
				// open request
				xhr.open('POST', portal + `/${Class}/${Method}`, true);
				//xhr.setRequestHeader('dataType', 'json');
				xhr.setRequestHeader("Content-Type", "text/plain")
				xhr.onreadystatechange = function () {
					console.log(xhr);
					if (xhr.readyState === 4 && xhr.status === 200) {
						//var res = JSON.parse(xhr.response);
						resolve({ respuesta: 1 });
					}
					else {
						resolve(null);
					}
				};
				xhr.send(JSON.stringify(Params));
				//xhr.send(Params);
				// send rquest with JSON payload
				//console.log(JSON.stringify(Params));
				//xhr.send(JSON.stringify(Params));

				//fetch(portal + `/${Class}/${Method}`, {
				//    method: 'POST',
				//    body: JSON.stringify(Params),
				//    mode: 'cors', // no-cors, *cors, same-origin
				//    cache: 'no-cache',
				//    headers: {
				//        'Content-Type': 'application/json'
				//        // 'Content-Type': 'application/x-www-form-urlencoded',
				//    },
				//    redirect: 'follow',
				//    referrerPolicy: 'no-referrer',
				//}).then(function (response) {
				//    //if (response.ok) {
				//    //    return response.text()
				//    //} else {
				//    //    throw "Error en la llamada Ajax";
				//    //}
				//    return response.json();
				//}).then(function (myJson) {
				//    resolve(myJson);
				//    //console.log(JSON.stringify(myJson));
				//}).catch(function (err) {
				//    reject(null);
				//    //console.log(err);
				//});

				//fetch('http://example.com/movies.json')
				//    .then(function (response) {
				//        return response.json();
				//    })
				//    .then(function (myJson) {
				//        console.log(JSON.stringify(myJson));
				//    });
			} catch (e) {
				reject(e);
			}
		});
		//return new Promise(function (resolve, reject) {
		//    try {
		//        let xhr = new XMLHttpRequest();
		//        xhr.open('POST', portal + `/${Class}/${Method}`);
		//        xhr.setRequestHeader('Content-Type', 'application/json');
		//        xhr.responseType = "json";
		//        xhr.onload = function () {
		//            if (xhr.status === 200) {
		//                resolve(xhr);
		//                //resolve(JSON.parse(xhr.response));
		//                //Do something
		//            }
		//            else {
		//                reject(null)
		//                //alert('Request failed.  Returned status of ' + xhr.status);
		//            }
		//        };
		//        xhr.send(Params);
		//    } catch (e) {
		//        reject(e);
		//    }
		//});
	},
	runFunctionAsync: async function (Class, Method, Params, ATrue, AFalse, AAlways) {
		await UIXMLHttpRequest.invokeFunctionAsync(Class, Method, Params).then(function (r) {
			if (r.respuesta > 0) {
				if (!UIEmpty(ATrue)) {
					ATrue();
				}
				if (!Params.HideSuccess) {
					MsgBox('success');
				}
			}
			else {
				if (!UIEmpty(AFalse)) {
					AFalse();
				}
				if (!Params.HideWarning) {
					MsgBox(r.mensaje, "warning", false);
				}
			}
		}).finally(function () {
			if (!UIEmpty(AAlways)) {
				AAlways();
			}
		});
	},
};

var UIAjax = {
	get: function (Class, Method, Params, Debug) {
		try {
			$.ajax({
				type: "GET",
				url: portal + `/${Class}/${Method}`,
				data: Params,
				dataType: "json",
				contentType: "application/json;charset=utf-8;",
				async: false,
				success: (r) => {
					if (app_config.debugger.state || !UIEmpty(Debug)) {
						app_config.console.success(`Ajax-get (/${Class}/${Method})`, r);
					}
					return r;
				},
				error: (r) => {
					if (app_config.debugger.state) {
						console.log(`%c✔ Error Ajax-get (/${Class}/${Method})`, "color: #148f32");
					}
					return null;
				}
			});
		} catch (e) {
			if (app_config.debugger.state) {
				console.log(`%c✔ Try-Catch Ajax-get (/${Class}/${Method})`, "color: #148f32");
				console.log(e);
			}
			return null;
		}
	},
	getAsync: function (Class, Method, Params, Debug) {
		try {
			$.ajax({
				type: "GET",
				url: portal + `/${Class}/${Method}`,
				data: Params,
				dataType: "json",
				contentType: "application/json;charset=utf-8;",
				success: (r) => {
					if (app_config.debugger.state || !UIEmpty(Debug)) {
						app_config.console.success(`Ajax-getAsync (/${Class}/${Method})`, r);
					}
					return r;
				},
				error: (r) => {
					if (app_config.debugger.state) {
						console.log(`%c✔ Error Ajax-getAsync (/${Class}/${Method})`, "color: #148f32");
					}
					return null;
				}
			});
		} catch (e) {
			if (app_config.debugger.state) {
				console.log(`%c✔ Try-Catch Ajax-getAsync (/${Class}/${Method})`, "color: #148f32");
				console.log(e);
			}
			return null;
		}
	},
	invokeMethod: function (Class, Method, Params) {
		return new Promise(function (resolve, reject) {
			try {
				$.ajax({
					type: "GET",
					url: portal + `/${Class}/${Method}`,
					data: Params,
					dataType: "json",
					contentType: "application/json;charset=utf-8;",
					async: false,
					success: (r) => {
						if (app_config.debugger.state) {
							app_config.console.success(`Ajax-invokeMethodAsync (/${Class}/${Method})`, r);
						}
						resolve(r);
					},
					error: (r) => {
						if (app_config.debugger.state) {
							console.log(`%c✔ Error Ajax-invokeMethodAsync (/${Class}/${Method})`, "color: #148f32");
						}
						reject(r);
					}
				});
			} catch (e) {
				if (app_config.debugger.state) {
					console.log(`%c✔ Try-Catch Ajax-invokeMethodAsync (/${Class}/${Method})`, "color: #148f32");
				}
				reject(e);
			}
		});
	},
	invokeMethodAsync: function (Class, Method, Params, Debug) {
		return new Promise(function (resolve, reject) {
			try {
				$.ajax({
					type: "GET",
					url: portal + `/${Class}/${Method}`,
					data: Params,
					dataType: "json",
					contentType: "application/json;charset=utf-8;",
					success: (r) => {
						if (app_config.debugger.state || Debug) {
							app_config.console.success(`Ajax-invokeMethodAsync (/${Class}/${Method})`, r);
						}
						resolve(r);
					},
					error: (r) => {
						if (app_config.debugger.state) {
							console.log(`%c✔ Error Ajax-invokeMethodAsync (/${Class}/${Method})`, "color: #148f32");
						}
						reject(r);
					}
				});
			} catch (e) {
				if (app_config.debugger.state) {
					console.log(`%c✔ Try-Catch Ajax-invokeMethodAsync (/${Class}/${Method})`, "color: #148f32");
				}
				reject(e);
			}
		});
	},
	invokeMethodTextAsync: function (Class, Method, Params, Debug) {
		return new Promise(function (resolve, reject) {
			try {
				$.ajax({
					type: "GET",
					url: portal + `/${Class}/${Method}`,
					data: Params,
					dataType: "text",
					contentType: "application/json;charset=utf-8;",
					success: (r) => {
						if (app_config.debugger.state || Debug) {
							app_config.console.success(`Ajax-invokeMethodAsync (/${Class}/${Method})`, r);
						}
						resolve(r);
					},
					error: (r) => {
						if (app_config.debugger.state) {
							console.log(`%c✔ Error Ajax-invokeMethodAsync (/${Class}/${Method})`, "color: #148f32");
						}
						reject(r);
					}
				});
			} catch (e) {
				if (app_config.debugger.state) {
					console.log(`%c✔ Try-Catch Ajax-invokeMethodAsync (/${Class}/${Method})`, "color: #148f32");
				}
				reject(e);
			}
		});
	},
	invokeMethodByteTextAsync: async function (Class, Method, Params) {
		Params = Params || {};
		return new Promise(function (resolve, reject) {
			URL = `${portal}/${Class}/${Method}?${UIURLParameters.fromJson(Params)}`;
			var xhr = new XMLHttpRequest();
			//xhr.responseType = 'json';
			xhr.responseType = "arraybuffer";
			xhr.open("GET", URL, true);
			xhr.onload = function () {
				if (this.status >= 200 && this.status < 300 && !this.responseURL.toLowerCase().endsWith(portal + '/login')) {
					var arrayBuffer = xhr.response; // Note: not oReq.responseText
					if (arrayBuffer) {
						var b = new Uint8Array(arrayBuffer);
						var str64 = new TextDecoder("utf-8").decode(b);
						resolve(UIConvert.Base64Lite.decode(str64));
					}
					else {
						reject(null);
					}
				} else {
					reject({
						status: this.status,
						statusText: xhr.statusText
					});
				}
			};
			xhr.onerror = function () {
				reject({
					status: this.status,
					statusText: xhr.statusText
				});
			};
			xhr.send();
		});
	},
	invokeFunctionAsync: function (Class, Method, Params) {
		return new Promise(function (resolve, reject) {
			try {
				$.ajax({
					type: "POST",
					url: portal + `/${Class}/${Method}`,
					data: Params,
					dataType: "json",
					//contentType: "application/json;charset=utf-8;",
					success: function (r) { resolve(r) },
					error: function (r) { reject(r) }
				});
			} catch (e) {
				reject(e);
			}
		});
	},
	runFunctionAsync: async function (Class, Method, Params, ATrue, AFalse, AAlways) {
		await UIAjax.invokeFunctionAsync(Class, Method, Params).then(function (r) {
			if (r.respuesta > 0) {
				if (!UIEmpty(ATrue)) {
					ATrue();
				}
				if (!Params.HideSuccess) {
					if (Params.ShowToast) {
						ShowToast({ message: "Cambios guardados correctamente." });
					}
					else {
						MsgBox('success');
					}
				}
			}
			else {
				if (!UIEmpty(AFalse)) {
					AFalse();
				}
				if (!Params.HideWarning) {
					if (Params.ShowToast) {
						ShowToast({ message: r.mensaje, type: 'warning' });
					}
					else {
						MsgBox(r.mensaje, "warning", false);
					}
				}
			}
		}).finally(function () {
			if (!UIEmpty(AAlways)) {
				AAlways();
			}
		});
	},
	invokeBlobAsync: function (Class, Method, Params, URLResult) {
		return new Promise(function (resolve, reject) {
			try {
				$.ajax({
					type: 'GET',
					url: portal + `/${Class}/${Method}`,
					data: Params,
					xhrFields: {
						responseType: 'blob'
					},
					success: function (r) {
						if (app_config.debugger.state) {
							app_config.console.success(`Ajax-invokeBlobAsync (/${Class}/${Method})`, r);
						}
						if (!UIEmpty(URLResult) && URLResult) {
							var url = window.URL || window.webkitURL;
							var src = url.createObjectURL(r);
							resolve(src);
						}
						else {
							resolve(r);
						}
					},
					error: (r) => {
						if (app_config.debugger.state) {
							console.log(`%c✔ Error Ajax-invokeBlobAsync (/${Class}/${Method})`, "color: #148f32");
						}
						reject(r);
					}
				});
			} catch (e) {
				if (app_config.debugger.state) {
					console.log(`%c✔ Try-Catch Ajax-invokeBlobAsync (/${Class}/${Method})`, "color: #148f32");
				}
				reject(e);
			}
		});
	},
	invokeBlobPostAsync: function (Class, Method, Params, URLResult) {
		return new Promise(function (resolve, reject) {
			try {
				$.ajax({
					type: 'POST',
					url: portal + `/${Class}/${Method}`,
					data: Params,
					xhrFields: {
						responseType: 'blob'
					},
					success: function (r) {
						if (app_config.debugger.state) {
							app_config.console.success(`Ajax-invokeBlobPostAsync (/${Class}/${Method})`, r);
						}
						if (!UIEmpty(URLResult) && URLResult) {
							var url = window.URL || window.webkitURL;
							var src = url.createObjectURL(r);
							resolve(src);
						}
						else {
							resolve(r);
						}
					},
					error: (r) => {
						if (app_config.debugger.state) {
							console.log(`%c✔ Error Ajax-invokeBlobPostAsync (/${Class}/${Method})`, "color: #148f32");
						}
						reject(r);
					}
				});
			} catch (e) {
				if (app_config.debugger.state) {
					console.log(`%c✔ Try-Catch Ajax-invokeBlobPostAsync (/${Class}/${Method})`, "color: #148f32");
				}
				reject(e);
			}
		});
	},
	invokeFormAsync: function (Class, Method, Params, Debug) {
		return new Promise(function (resolve, reject) {
			try {
				var formData = new FormData();
				if (!UIEmpty(Params)) {
					$.each(Params, function (i, v) {
						formData.append(i, v);
					});
				}
				$.ajax({
					type: "POST",
					url: portal + `/${Class}/${Method}`,
					data: formData,
					contentType: false,
					processData: false,
					success: (r) => {
						if (app_config.debugger.state || !UIEmpty(Debug)) {
							app_config.console.success(`Ajax-invokeFormAsync (/${Class}/${Method})`, r);
						}
						resolve(r);
					},
					error: (r) => {
						if (app_config.debugger.state) {
							console.log(`%c✔ Error Ajax-invokeFormAsync (/${Class}/${Method})`, "color: #148f32");
						}
						reject(r);
					}
				});
			} catch (e) {
				if (app_config.debugger.state) {
					console.log(`%c✔ Try-Catch Ajax-invokeFormAsync (/${Class}/${Method})`, "color: #148f32");
				}
				reject(e);
			}
		});
	},
	runFormAsync: async function (Class, Method, Params, ATrue, AFalse, AAlways) {
		await UIAjax.invokeFormAsync(Class, Method, Params).then(function (r) {
			if (r.respuesta > 0) {
				if (!UIEmpty(ATrue)) {
					ATrue();
				}
				if (UIEmpty(r.mensaje) || r.mensaje == "Ok") {
					MsgBox('success');
				}
				else {
					MsgBox(r.mensaje, "success", false);
				}
			}
			else {
				if (!UIEmpty(AFalse)) {
					AFalse();
				}
				MsgBox(r.mensaje, "warning", false);
			}
		}).finally(function () {
			if (!UIEmpty(AAlways)) {
				AAlways();
			}
		});
	},
};

var UIBlob = {
	Save: function (file, filename, fAlways) { // content: blob, name: string
		//if (navigator.msSaveBlob) { // For ie and Edge
		//    return navigator.msSaveBlob(file, filename);
		//    //return navigator.msSaveBlob(file.content, file.name);
		//}
		//else {
		//    let link = document.createElement('a');
		//    link.href = window.URL.createObjectURL(file);
		//    //link.href = window.URL.createObjectURL(file.content);
		//    link.download = filename;
		//    document.body.appendChild(link);
		//    link.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));
		//    link.remove();
		//    window.URL.revokeObjectURL(link.href);
		//}
		let link = document.createElement('a');
		link.href = window.URL.createObjectURL(file);
		//link.href = window.URL.createObjectURL(file.content);
		link.download = filename;
		document.body.appendChild(link);
		link.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));
		link.remove();
		window.URL.revokeObjectURL(link.href);
		if (!UIEmpty(fAlways)) {
			fAlways();
		}
	},
	CreateURLFrom64: function (value64, mimetype) {
		var _blob = UIConvert.Base64toBlob(value64, mimetype);
		var urlCreator = typeof window.URL == "string" ? window.webkitURL : window.URL;
		return urlCreator.createObjectURL(_blob);
	},
};

var UIXLSX = {
	Json: function (json, filename, header) {
		//const header = Object.keys(json[0]); // columns name
		//var wscols = [];
		//for (var i = 0; i < header.length; i++) {  // columns length added
		//    //wscols.push({ wch: header[i].length + 5 });
		//    wscols.push({ wch: header[i].length });
		//}
		const ws = XLSX.utils.json_to_sheet(json);
		UIXLSX._AutoFitColumns(ws, json, header);
		//function autofitColumns(worksheet) {
		//    let objectMaxLength = [];
		//    const [startLetter, endLetter] = worksheet['!ref']?.replace(/\d/, '').split(':');
		//    const ranges = range(startLetter.charCodeAt(0), endLetter.charCodeAt(0) + 1);
		//    ranges.forEach((c) => {
		//        const cell = String.fromCharCode(c);
		//        const cellLength = worksheet[`${cell}1`].v.length + 1;
		//        objectMaxLength.push({ width: cellLength });
		//    });
		//    worksheet['!cols'] = objectMaxLength;
		//}
		//autofitColumns(ws);
		const wb = XLSX.utils.book_new();
		wb.Props = {
			//Title: "foo",
			//Subject: "bar",
			Author: Entity.UI.Author,
			CreatedDate: new Date
		};
		XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
		XLSX.write(wb, { bookType: 'xlsx', bookSST: true, type: 'binary' });
		XLSX.writeFile(wb, filename);
		//function s2ab(s) {
		//    var buf = new ArrayBuffer(s.length);
		//    var view = new Uint8Array(buf);
		//    for (var i = 0; i < s.length; i++) view[i] = s.charCodeAt(i) & 0xFF;
		//    return buf;
		//}
		//new Blob([s2ab(wbout)]);

		//var wb = XLSX.utils.book_new();
		//wb.Props = {
		//    Title: "foo",
		//    Subject: "bar",
		//    Author: "foo",
		//    CreatedDate: date
		//};
		//wb.SheetNames.push("export_1");
		//var ws = XLSX.utils.aoa_to_sheet(ws_data);
		//var wscols = [
		//    { wch: 50 },
		//    { wch: 15 },
		//    { wch: 15 },
		//    { wch: 15 },
		//    { wch: 15 },
		//    { wch: 15 },
		//    { wch: 15 },
		//    { wch: 15 },
		//    { wch: 15 },
		//    { wch: 15 },
		//];
		//ws['!cols'] = wscols;
		//wb.Sheets["export_1"] = ws;
		//var wbout = XLSX.write(wb, { bookType: 'xlsx', type: 'binary' });
		//FileSaver.saveAs(new Blob([s2ab(wbout)], { type: "application/octet-stream" }), 'pm-missing-' + date + '.xlsx');

	},
	DOMTable: function (id, filename) {
		//Ejm: tableDemo, NO usar el selector de jQuery ($)
		var wb = XLSX.utils.table_to_book(document.getElementById(id), { sheet: "Sheet1" });
		var wbout = XLSX.write(wb, { bookType: 'xlsx', bookSST: true, type: 'binary' });
		XLSX.writeFile(wbout, filename);
	},
	_AutoFitColumns: function (ws, json, header) {
		const jsonKeys = header ? header : Object.keys(json[0]);
		let objectMaxLength = [];
		for (let i = 0; i < json.length; i++) {
			let value = json[i];
			for (let j = 0; j < jsonKeys.length; j++) {
				if (typeof value[jsonKeys[j]] == "number") {
					objectMaxLength[j] = 10;
				} else {
					const l = value[jsonKeys[j]] ? value[jsonKeys[j]].length : 0;
					objectMaxLength[j] = objectMaxLength[j] >= l ? objectMaxLength[j] : l;
				}
			}
			let key = jsonKeys;
			for (let j = 0; j < key.length; j++) {
				objectMaxLength[j] = objectMaxLength[j] >= key[j].length ? objectMaxLength[j] : key[j].length;
			}
		}
		const wscols = objectMaxLength.map(w => { return { width: w } });
		ws["!cols"] = wscols;
	}
};

var UISave = {
	URL: function (url, filename) {
		let link = document.createElement('a');
		link.href = url;
		link.target = "_blank";
		if (!UIEmpty(filename)) {
			link.download = filename;
		}
		document.body.appendChild(link);
		link.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));
		link.remove();
	},
	Base64: function (data, filename) {
		var url = "";
		if (filename.endsWith('.txt')) {
			//url = `data:text/plain;base64,${data}`;
			//url = 'data:text/plain;base64,' + encodeURIComponent(data);
			var _blob = UIConvert.Base64toBlob(data, 'text/plain');
			UIBlob.Save(_blob, filename);
		}
		//if (!UIEmpty(url)) {
		//    UISave.URL(url, filename);
		//}
	}
}

var UIFixedText = function (cadena) {
	var internalFunction = function (_cadena) {
		//var chars = {
		//    "á": "a", "é": "e", "í": "i", "ó": "o", "ú": "u",
		//    "à": "a", "è": "e", "ì": "i", "ò": "o", "ù": "u", "ñ": "n",
		//    "Á": "A", "É": "E", "Í": "I", "Ó": "O", "Ú": "U",
		//    "À": "A", "È": "E", "Ì": "I", "Ò": "O", "Ù": "U", "Ñ": "N"
		//}
		var chars = {
			"á": "\u00E1", "é": "\u00E9", "í": "\u00ED", "ó": "\u00F3", "ú": "\u00FA",
			"à": "\u00E0", "è": "\u00E8", "ì": "\u00EC", "ò": "\u00F2", "ù": "\u00F9", "ñ": "\u00F1",
			"Á": "\u00C1", "É": "\u00C9", "Í": "\u00CD", "Ó": "\u00D3", "Ú": "\u00DA",
			"À": "\u00C0", "È": "\u00C8", "Ì": "\u00CC", "Ò": "\u00D2", "Ù": "\u00D9", "Ñ": "\u00D1"
		}
		var expr = /[áàéèíìóòúùñ]/ig;
		var res = _cadena.replace(expr, function (e) { return chars[e] });
		return res;
	}
	//var str = "Óscar qué tal estásMÉ?¡!";
	//console.log(internalFunction(str));
	return internalFunction(cadena);
	//const removeAccents = (str) => {
	//    return str.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
	//}
	//var cadena = removeAccents('La uña se me rompió después de beber cachaça');
	//console.log(cadena); // esto muestra en la consola el texto "La una se me rompio despues de beber cachaca"
}

var UIFloat = function (Number, Digits) {
	if (UIEmpty(Number)) {
		Number = 0;
	}
	var result = parseFloat(Number);
	if (UIEmpty(Digits)) {
		Digits = 2;
	}
	if (isNaN(result)) {
		result = 0;
	}
	result = result.toFixed(Digits);
	return result;
};

var UIFloat2 = function (Number, Digits) {
	if (UIEmpty(Number)) {
		Number = 0;
	}
	var result = parseFloat(Number);
	if (UIEmpty(Digits)) {
		Digits = 2;
	}
	if (isNaN(result)) {
		result = 0;
	}
	result = result.toFixed(Digits);
	var resultFinal = parseFloat(result);
	return resultFinal;
};

var UINumber = function (Number, Digits) {
	var result;
	if (UIEmpty(Digits)) {
		Digits = 2;
	}
	result = Number.toLocaleString('es-PE', { minimumIntegerDigits: Digits, useGrouping: false });
	return result;
};

var UIAutoNumeric = {
	Init: function (Id, Options) {
		var optionsDefault = {
			aSep: '',
			aDec: '.',
			vMin: '0.00',
			vMax: '9999999.99'
		};
		$.extend(true, optionsDefault, Options);
		$(Id).autoNumeric('destroy').autoNumeric('init', optionsDefault);
	},
	Destroy: function (Id) {
		$(Id).autoNumeric('destroy');
	},
	setValue: function (Id, value) {
		$(Id).autoNumeric('set', UIFloat2(value));
	}
};

var UIDatePicker = {
	Init: function (Id, Options) {
		var optionsDefault = {
			autoclose: true,
			language: 'es',
			//format: "dd/mm/yyyy",
			//gotoCurrent: true,
		};
		$.extend(true, optionsDefault, Options);
		$(Id).datepicker('destroy').datepicker(optionsDefault);
		if (!UIEmpty(optionsDefault.defaultDate)) {
			var $Id = $(Id);
			$Id.datepicker('setDate', optionsDefault.defaultDate);
			if ($Id.is('div')) {
				$('input:text', $Id).datepicker('destroy').datepicker(optionsDefault).datepicker('setDate', optionsDefault.defaultDate);
			}
		}
	},
	Destroy: function (Id) {
		$(Id).datepicker('destroy');
	}
};

var UIMultipleSelect = {
	Init: function (Id, Options) {
		var optionsDefault = {
			filter: true,
			showClear: false,
			displayDelimiter: ' | ',
			textAll: '[Seleccionar todo]',
			selectAll: true, //Para mostrar el Seleccionar todos
			locale: 'es-ES',
			hideOptgroupCheckboxes: true,
			minimumCountSelected: 3,
			checkAll: false,
			setSelects: null,
			removeOptions: []
		};
		$.extend(true, optionsDefault, Options);
		var ele = $(Id);
		if (optionsDefault.removeOptions != null && optionsDefault.removeOptions.length > 0) {
			$.each(optionsDefault.removeOptions, function (i, e) {
				$("option[value = '" + e + "']", ele).remove();
			});
		}
		$.fn.multipleSelect.locales['es-ES'].formatSelectAll = function () { return optionsDefault.textAll };
		//$.fn.multipleSelect.locales['es-ES'] = {
		//	formatSelectAll: function formatSelectAll() {
		//		return '[Seleccionar todo]';
		//	},
		//	formatAllSelected: function formatAllSelected() {
		//		return 'Todos seleccionados';
		//	},
		//	formatCountSelected: function formatCountSelected(count, total) {
		//		return count + ' de ' + total + ' seleccionado';
		//	},
		//	formatNoMatchesFound: function formatNoMatchesFound() {
		//		return 'No se encontraron coincidencias';
		//	}
		//};
		ele.multipleSelect('destroy').multipleSelect(optionsDefault);
		if (optionsDefault.checkAll) {
			ele.multipleSelect('checkAll');
		}
		else {
			if (!UIEmpty(optionsDefault.setSelects)) {
				ele.multipleSelect('setSelects', optionsDefault.setSelects);
			}
		}
	}
};

var UIInput = {
	Alfanumerics: function (Id, WithSpace) {
		var _Reg = "^[a-zA-Z0-9 ]+$";
		if (!UIEmpty(WithSpace)) {
			if (!WithSpace) {
				_Reg = "^[a-zA-Z0-9]+$";
			}
		}
		UIInput.setRegExp(Id, _Reg);
	},
	FullAlfanumerics: function (Id, WithSpace) {
		var _Reg = "^[a-zA-Z0-9 áéíóúàèìòùÀÈÌÒÙÁÉÍÓÚñÑüÜ/\\s]+$";
		if (!UIEmpty(WithSpace)) {
			if (!WithSpace) {
				_Reg = "^[a-zA-Z0-9áéíóúàèìòùÀÈÌÒÙÁÉÍÓÚñÑüÜ/\\s]+$";
			}
		}
		UIInput.setRegExp(Id, _Reg);
	},
	Letters: function (Id, WithSpace) {
		var _Reg = "^[a-zA-Z ]+$";
		if (!UIEmpty(WithSpace)) {
			if (!WithSpace) {
				_Reg = "^[a-zA-Z]+$";
			}
		}
		UIInput.setRegExp(Id, _Reg);
	},
	Numerics: function (Id) {
		var _Reg = "^[0-9]+$";
		UIInput.setRegExp(Id, _Reg);
	},
	setRegExp: function (Id, _Reg) {
		$(Id).off('keypress').on('keypress', function (e) {
			var regex = new RegExp(_Reg);
			var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
			if (regex.test(str)) {
				return true;
			}
			e.preventDefault();
			return false;
		});
	},
};

var UICheckbox = {
	setValue: function (Id, Value) {
		var AString = ["S", "SÍ", "SI", "Sí", "Si", "sí", "si", "1", "YES", "Yes", "yes", "ON", "On", "on", "true", "TRUE", "True", true];
		//var fnBoolean = function (key) {
		//    var valor = $.trim(jsonSource[key]);
		//    //if (AString.includes(valor)) {
		//    //}
		//    //return jsonSource[key] == "S" ? true : false;
		//    return AString.includes(valor);
		//};
		$(Id).prop('checked', AString.includes($.trim(Value)))
	},
	getValue: function (Id, vTrue, vFalse) {
		var elem = $(Id);
		if (elem.length == 1) {
			if (elem.is('input[type="checkbox"]')) {
				vTrue = vTrue || 'S';
				vFalse = vFalse || 'N';
				return elem.is(':checked') ? vTrue : vFalse;
			}
			else {
				vTrue = vTrue || elem;
				vFalse = vFalse || elem;
				return elem.val() == vTrue ? vTrue : vFalse;
			}
		}
		else {
			return elem;
		}
	}
};

var UIMoney = function (n, decPlaces, thouSeparator, decSeparator) {
	//var n = this;
	n = isNaN(n) ? 0 : n;
	var decPlaces = isNaN(decPlaces = Math.abs(decPlaces)) ? 2 : decPlaces;
	var decSeparator = decSeparator == undefined ? "." : decSeparator;
	var thouSeparator = thouSeparator == undefined ? "," : thouSeparator;
	var sign = n < 0 ? "-" : "";
	var i = parseInt(n = Math.abs(+n || 0).toFixed(decPlaces)) + "";
	var j = (j = i.length) > 3 ? j % 3 : 0;
	return sign + (j ? i.substr(0, j) + thouSeparator : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thouSeparator) + (decPlaces ? decSeparator + Math.abs(n - i).toFixed(decPlaces).slice(2) : "");

	//(12562).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')
};

var UINumberToFormat = function (Number, Digits) {
	var result;
	if (UIEmpty(Digits)) {
		Digits = 2;
	}
	result = Number.toLocaleString('es-PE', { minimumIntegerDigits: Digits, useGrouping: false });
	return result;
};

var UIHH_mm = {
	Change: function (selector) {
		$(selector).on('change', function () {
			var ths = $(this);
			var valor = ths.val();
			if (valor.length != 5) {
				var valorInt = parseFloat(valor);
				if (!isNaN(valorInt) && valorInt < 24) {
					if (valor.includes(':')) {
						if (valor.length == 4) {
							var AHora = valor.split(':');
							if (AHora[1] > 5) {
								ths.val('00:00');
								MsgBox('El valor ingresado no es correcto.', 'warning', true);
								return;
							}
							else {
								ths.val(`${valor}0`);
							}
						}
						else {
							ths.val(`${valor}00`);
						}
					}
					else {
						ths.val(`${UINumberToFormat(valorInt)}:00`);
					}
				}
				else {
					if (valor.length == 4 && !isNaN(valorInt)) {
						var hh = valor.substring(0, 2);
						var mm = valor.substring(2, 4);
						if (hh > 23 || mm > 59) {
							ths.val('00:00');
							MsgBox('El valor ingresado no es correcto.', 'warning', true);
						}
						else {
							ths.val(hh + ':' + mm);
						}
					}
					else {
						ths.val('00:00');
						if (valor.length == 0) {
							return;
						}
						MsgBox('El valor ingresado no es correcto.', 'warning', true);
						return;
					}
				}
			}
			else {
				var Arreglo = valor.split(':');
				var hh = parseInt(Arreglo[0]);
				var mm = parseInt(Arreglo[1]);
				if (hh > 23 || mm > 59) {
					ths.val('00:00');
					MsgBox('La valor ingresada no es correcta.', 'warning', true);
					return;
				}
			}
			return true;
		});
	}
};

var UIDate = function (now, frmt, add) {
	var formats = {
		"ar-SA": "dd/MM/yy",
		"bg-BG": "dd.M.yyyy",
		"ca-ES": "dd/MM/yyyy",
		"zh-TW": "yyyy/M/d",
		"cs-CZ": "d.M.yyyy",
		"da-DK": "dd-MM-yyyy",
		"de-DE": "dd.MM.yyyy",
		"el-GR": "d/M/yyyy",
		"en-US": "M/d/yyyy",
		"fi-FI": "d.M.yyyy",
		"fr-FR": "dd/MM/yyyy",
		"he-IL": "dd/MM/yyyy",
		"hu-HU": "yyyy. MM. dd.",
		"is-IS": "d.M.yyyy",
		"it-IT": "dd/MM/yyyy",
		"ja-JP": "yyyy/MM/dd",
		"ko-KR": "yyyy-MM-dd",
		"nl-NL": "d-M-yyyy",
		"nb-NO": "dd.MM.yyyy",
		"pl-PL": "yyyy-MM-dd",
		"pt-BR": "d/M/yyyy",
		"ro-RO": "dd.MM.yyyy",
		"ru-RU": "dd.MM.yyyy",
		"hr-HR": "d.M.yyyy",
		"sk-SK": "d. M. yyyy",
		"sq-AL": "yyyy-MM-dd",
		"sv-SE": "yyyy-MM-dd",
		"th-TH": "d/M/yyyy",
		"tr-TR": "dd.MM.yyyy",
		"ur-PK": "dd/MM/yyyy",
		"id-ID": "dd/MM/yyyy",
		"uk-UA": "dd.MM.yyyy",
		"be-BY": "dd.MM.yyyy",
		"sl-SI": "d.M.yyyy",
		"et-EE": "d.MM.yyyy",
		"lv-LV": "yyyy.MM.dd.",
		"lt-LT": "yyyy.MM.dd",
		"fa-IR": "MM/dd/yyyy",
		"vi-VN": "dd/MM/yyyy",
		"hy-AM": "dd.MM.yyyy",
		"az-Latn-AZ": "dd.MM.yyyy",
		"eu-ES": "yyyy/MM/dd",
		"mk-MK": "dd.MM.yyyy",
		"af-ZA": "yyyy/MM/dd",
		"ka-GE": "dd.MM.yyyy",
		"fo-FO": "dd-MM-yyyy",
		"hi-IN": "dd-MM-yyyy",
		"ms-MY": "dd/MM/yyyy",
		"kk-KZ": "dd.MM.yyyy",
		"ky-KG": "dd.MM.yy",
		"sw-KE": "M/d/yyyy",
		"uz-Latn-UZ": "dd/MM yyyy",
		"tt-RU": "dd.MM.yyyy",
		"pa-IN": "dd-MM-yy",
		"gu-IN": "dd-MM-yy",
		"ta-IN": "dd-MM-yyyy",
		"te-IN": "dd-MM-yy",
		"kn-IN": "dd-MM-yy",
		"mr-IN": "dd-MM-yyyy",
		"sa-IN": "dd-MM-yyyy",
		"mn-MN": "yy.MM.dd",
		"gl-ES": "dd/MM/yy",
		"kok-IN": "dd-MM-yyyy",
		"syr-SY": "dd/MM/yyyy",
		"dv-MV": "dd/MM/yy",
		"ar-IQ": "dd/MM/yyyy",
		"zh-CN": "yyyy/M/d",
		"de-CH": "dd.MM.yyyy",
		"en-GB": "dd/MM/yyyy",
		"es-MX": "dd/MM/yyyy",
		"fr-BE": "d/MM/yyyy",
		"it-CH": "dd.MM.yyyy",
		"nl-BE": "d/MM/yyyy",
		"nn-NO": "dd.MM.yyyy",
		"pt-PT": "dd-MM-yyyy",
		"sr-Latn-CS": "d.M.yyyy",
		"sv-FI": "d.M.yyyy",
		"az-Cyrl-AZ": "dd.MM.yyyy",
		"ms-BN": "dd/MM/yyyy",
		"uz-Cyrl-UZ": "dd.MM.yyyy",
		"ar-EG": "dd/MM/yyyy",
		"zh-HK": "d/M/yyyy",
		"de-AT": "dd.MM.yyyy",
		"en-AU": "d/MM/yyyy",
		"es-ES": "dd/MM/yyyy",
		"fr-CA": "yyyy-MM-dd",
		"sr-Cyrl-CS": "d.M.yyyy",
		"ar-LY": "dd/MM/yyyy",
		"zh-SG": "d/M/yyyy",
		"de-LU": "dd.MM.yyyy",
		"en-CA": "dd/MM/yyyy",
		"es-GT": "dd/MM/yyyy",
		"fr-CH": "dd.MM.yyyy",
		"ar-DZ": "dd-MM-yyyy",
		"zh-MO": "d/M/yyyy",
		"de-LI": "dd.MM.yyyy",
		"en-NZ": "d/MM/yyyy",
		"es-CR": "dd/MM/yyyy",
		"fr-LU": "dd/MM/yyyy",
		"ar-MA": "dd-MM-yyyy",
		"en-IE": "dd/MM/yyyy",
		"es-PA": "MM/dd/yyyy",
		"fr-MC": "dd/MM/yyyy",
		"ar-TN": "dd-MM-yyyy",
		"en-ZA": "yyyy/MM/dd",
		"es-DO": "dd/MM/yyyy",
		"ar-OM": "dd/MM/yyyy",
		"en-JM": "dd/MM/yyyy",
		"es-VE": "dd/MM/yyyy",
		"ar-YE": "dd/MM/yyyy",
		"en-029": "MM/dd/yyyy",
		"es-CO": "dd/MM/yyyy",
		"ar-SY": "dd/MM/yyyy",
		"en-BZ": "dd/MM/yyyy",
		"es-PE": "dd/MM/yyyy",
		"ar-JO": "dd/MM/yyyy",
		"en-TT": "dd/MM/yyyy",
		"es-AR": "dd/MM/yyyy",
		"ar-LB": "dd/MM/yyyy",
		"en-ZW": "M/d/yyyy",
		"es-EC": "dd/MM/yyyy",
		"ar-KW": "dd/MM/yyyy",
		"en-PH": "M/d/yyyy",
		"es-CL": "dd-MM-yyyy",
		"ar-AE": "dd/MM/yyyy",
		"es-UY": "dd/MM/yyyy",
		"ar-BH": "dd/MM/yyyy",
		"es-PY": "dd/MM/yyyy",
		"ar-QA": "dd/MM/yyyy",
		"es-BO": "dd/MM/yyyy",
		"es-SV": "dd/MM/yyyy",
		"es-HN": "dd/MM/yyyy",
		"es-NI": "dd/MM/yyyy",
		"es-PR": "dd/MM/yyyy",
		"am-ET": "d/M/yyyy",
		"tzm-Latn-DZ": "dd-MM-yyyy",
		"iu-Latn-CA": "d/MM/yyyy",
		"sma-NO": "dd.MM.yyyy",
		"mn-Mong-CN": "yyyy/M/d",
		"gd-GB": "dd/MM/yyyy",
		"en-MY": "d/M/yyyy",
		"prs-AF": "dd/MM/yy",
		"bn-BD": "dd-MM-yy",
		"wo-SN": "dd/MM/yyyy",
		"rw-RW": "M/d/yyyy",
		"qut-GT": "dd/MM/yyyy",
		"sah-RU": "MM.dd.yyyy",
		"gsw-FR": "dd/MM/yyyy",
		"co-FR": "dd/MM/yyyy",
		"oc-FR": "dd/MM/yyyy",
		"mi-NZ": "dd/MM/yyyy",
		"ga-IE": "dd/MM/yyyy",
		"se-SE": "yyyy-MM-dd",
		"br-FR": "dd/MM/yyyy",
		"smn-FI": "d.M.yyyy",
		"moh-CA": "M/d/yyyy",
		"arn-CL": "dd-MM-yyyy",
		"ii-CN": "yyyy/M/d",
		"dsb-DE": "d. M. yyyy",
		"ig-NG": "d/M/yyyy",
		"kl-GL": "dd-MM-yyyy",
		"lb-LU": "dd/MM/yyyy",
		"ba-RU": "dd.MM.yy",
		"nso-ZA": "yyyy/MM/dd",
		"quz-BO": "dd/MM/yyyy",
		"yo-NG": "d/M/yyyy",
		"ha-Latn-NG": "d/M/yyyy",
		"fil-PH": "M/d/yyyy",
		"ps-AF": "dd/MM/yy",
		"fy-NL": "d-M-yyyy",
		"ne-NP": "M/d/yyyy",
		"se-NO": "dd.MM.yyyy",
		"iu-Cans-CA": "d/M/yyyy",
		"sr-Latn-RS": "d.M.yyyy",
		"si-LK": "yyyy-MM-dd",
		"sr-Cyrl-RS": "d.M.yyyy",
		"lo-LA": "dd/MM/yyyy",
		"km-KH": "yyyy-MM-dd",
		"cy-GB": "dd/MM/yyyy",
		"bo-CN": "yyyy/M/d",
		"sms-FI": "d.M.yyyy",
		"as-IN": "dd-MM-yyyy",
		"ml-IN": "dd-MM-yy",
		"en-IN": "dd-MM-yyyy",
		"or-IN": "dd-MM-yy",
		"bn-IN": "dd-MM-yy",
		"tk-TM": "dd.MM.yy",
		"bs-Latn-BA": "d.M.yyyy",
		"mt-MT": "dd/MM/yyyy",
		"sr-Cyrl-ME": "d.M.yyyy",
		"se-FI": "d.M.yyyy",
		"zu-ZA": "yyyy/MM/dd",
		"xh-ZA": "yyyy/MM/dd",
		"tn-ZA": "yyyy/MM/dd",
		"hsb-DE": "d. M. yyyy",
		"bs-Cyrl-BA": "d.M.yyyy",
		"tg-Cyrl-TJ": "dd.MM.yy",
		"sr-Latn-BA": "d.M.yyyy",
		"smj-NO": "dd.MM.yyyy",
		"rm-CH": "dd/MM/yyyy",
		"smj-SE": "yyyy-MM-dd",
		"quz-EC": "dd/MM/yyyy",
		"quz-PE": "dd/MM/yyyy",
		"hr-BA": "d.M.yyyy.",
		"sr-Latn-ME": "d.M.yyyy",
		"sma-SE": "yyyy-MM-dd",
		"en-SG": "d/M/yyyy",
		"ug-CN": "yyyy-M-d",
		"sr-Cyrl-BA": "d.M.yyyy",
		"es-US": "M/d/yyyy"
	};

	if (UIEmpty(frmt)) {
		frmt = "es-ES";
	}

	//Options key examples:
	//day:
	//The representation of the day.
	//Possible values are "numeric", "2-digit".
	//    weekday:
	//The representation of the weekday.
	//Possible values are "narrow", "short", "long".
	//    year:
	//The representation of the year.
	//Possible values are "numeric", "2-digit".
	//    month:
	//The representation of the month.
	//Possible values are "numeric", "2-digit", "narrow", "short", "long".
	//    hour:
	//The representation of the hour.
	//Possible values are "numeric", "2-digit".
	//    minute: The representation of the minute.
	//Possible values are "numeric", "2-digit".
	//    second:
	//The representation of the second.
	//Possible values are "numeric", 2 - digit".

	//var options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
	var options = { year: 'numeric', month: '2-digit', day: '2-digit' };
	if (now == null || now == undefined) {
		now = new Date();
		//return now.toLocaleDateString(frmt, options);
	}
	var today = new Date(now);

	if (add == null || add == undefined) {
		add = 0;
	}
	today.setDate(now.getDate() + add);

	//today.setDate(now.getDate() + 1);    

	var result = today.toLocaleDateString(frmt, options);
	//console.log(today.toLocaleDateString("en-US")); // 9/17/2016
	//console.log(today.toLocaleDateString("en-US", options)); // Saturday, September 17, 2016
	//console.log(today.toLocaleDateString("hi-IN", options)); // शनिवार, 17 सितंबर 2016
	return result;
	//console.log(new Date(Date.parse("05/02/2021")));
};

var UITime = {
	Parse: function (hh, mm, ss) {
		var horas = parseFloat(hh || 0);
		var minutos = parseFloat(mm || 0);
		var segundos = parseFloat(ss || 0);

		var horas_Segundos = horas * 3600;
		var minutos_Segundos = minutos * 60;
		var segundos = segundos + minutos_Segundos + horas_Segundos;
		var hours = Math.floor(segundos / 3600);
		var minutes = Math.floor((segundos % 3600) / 60);
		var seconds = segundos % 60;
		hours = hours < 10 ? '0' + hours : hours;
		//Anteponiendo un 0 a los minutos si son menos de 10
		minutes = minutes < 10 ? '0' + minutes : minutes;
		//Anteponiendo un 0 a los segundos si son menos de 10
		seconds = seconds < 10 ? '0' + seconds : seconds;
		var result = hours + ":" + minutes + ":" + seconds;
		return result;
	},
	ParseToHHmm: function (hh, mm, ss) {
		var result = UITime.Parse(hh, mm, ss);
		var AResult = result.split(':');
		return AResult[0] + ":" + AResult[1];
	},
	inRange: function (timeTest, timeIni, timeEnd) {
		var result = false;
		try {
			if (UIRegex.isHHmm(timeIni) && UIRegex.isHHmm(timeEnd)) {
				var nowDate = new Date();
				if (timeTest == "now") {
					var dTimeTest = new Date();
				}
				else {
					var ATimeTest = timeTest.split(':');
					var dTimeTest = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), ATimeTest[0], ATimeTest[1]);
				}
				var ATimeIni = timeIni.split(':');
				var ATimeEnd = timeEnd.split(':');
				var dTimeIni = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), ATimeIni[0], ATimeIni[1]);
				var dTimeEnd = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), ATimeEnd[0], ATimeEnd[1]);
				result = (dTimeTest >= dTimeIni && dTimeTest <= dTimeEnd);
			}
		} catch (e) {
			result = false;
		}
		return result;
	},
}

var UIDateTime = {
	firstDayMonth: function (month) {
		var date = new Date();
		if (UIEmpty(month)) {
			month = parseInt(date.getMonth());
		}
		return new Date(date.getFullYear(), month, 1);
	},
	lastDayMonth: function (month) {
		var date = new Date();
		if (UIEmpty(month)) {
			month = parseInt(date.getMonth());
		}
		return new Date(date.getFullYear(), month + 1, 0);
	},
};

var UIMath = {
	Random: function () {
		return new Date().getTime() + Math.floor(Math.random() * (15000 - 1)) + 1;
	},
};

var UIFunctions = {
	Checkbox: {
		HTML: function (id, check, name) {
			console.log(check);
			var str_chk = "";
			if (check == true || check == "ok" || check == "Ok" || check == "yes" || check == "Yes" || check == "true" || check == "True" || check == "S") {
				str_chk = "checked";
			}
			if (UIEmpty(id)) {
				id = UIMath.Random();
			}
			return `<div class="custom-control custom-checkbox" without-label>
						<input type="checkbox" name="${name}" class="custom-control-input" id="${id}" ${str_chk} @change="e => eventChanged(e)">
						<label class="custom-control-label" for="${id}"></label>
					</div>`;
		},
		HTML2: function (id, check, name, disabled) {
			var str_chk = "";
			var str_disabled = "";
			if (check == true || check == "ok" || check == "Ok" || check == "yes" || check == "Yes" || check == "true" || check == "True" || check == "S") {
				str_chk = "checked";
			}
			if (disabled == true) {
				str_disabled = "disabled";
			}
			if (UIEmpty(id)) {
				id = UIMath.Random();
			}
			return `<div class="checkbox checkbox-primary d-inline mr-0" without-label cursor-pointer>
						<input type="checkbox" name="${name}" class="custom-control-input" id="${id}" ${str_chk} @change="e => eventChanged(e)" ${str_disabled}>
						<label class="cr m-0" for="${id}"></label>
					</div>`;
		},
		Vue: function (id, check, name, model) {
			var str_chk = check ? "checked" : "";
			return `<div class="custom-control custom-checkbox">
						<input type="checkbox" name="${name}" class="custom-control-input" ref="input" id="${id}" ${str_chk} :value="${model}" v-model="${model}" v-on:change="eventChanged">
						<label class="custom-control-label" for="${id}"></label>
					</div>`;
		},
	},
	getValue: {
		HTML: function (id, vTrue, vFalse) {
			var elem = $(id);
			//vTrue = UIValue(vTrue, elem);
			//vFalse = UIValue(vFalse, elem);
			if (elem.length == 1) {
				if (elem.is('input[type="checkbox"]')) {
					vTrue = vTrue || 'S';
					vFalse = vFalse || 'N';
					return elem.is(':checked') ? vTrue : vFalse;
				}
				else {
					vTrue = vTrue || elem;
					vFalse = vFalse || elem;
					return elem.val() == vTrue ? vTrue : vFalse;
				}
			}
			else {
				return elem;
			}
		}
	},
	invokeJS: function (functionName) {
		var namespaces, func, context;

		if (typeof functionName === 'undefined') { throw 'No se ha especificado el nombre de la función.'; }

		if (typeof eval(functionName) !== 'function') { throw functionName + ' no es una función.'; }

		context = window;

		namespaces = functionName.split(".");
		func = namespaces.pop();

		for (var i = 0; i < namespaces.length; i++) {
			context = context[namespaces[i]];
		}

		var params = [];
		if (arguments.length > 1) {
			for (var i = 1; i < arguments.length; i++) {
				params.push(arguments[i]);
			}
		}

		return context[func].apply(context, params);
	},
	invokeJSAsync: async function (functionName, ...params) {
		await UIFunctions.invokeJS(functionName, ...params);
	},
	setMeses: async function (id, valor) {
		var sb = new StringBuilder();
		sb.append(`<option value="01">Enero</option>`);
		sb.append(`<option value="02">Febrero</option>`);
		sb.append(`<option value="03">Marzo</option>`);
		sb.append(`<option value="04">Abril</option>`);
		sb.append(`<option value="05">Mayo</option>`);
		sb.append(`<option value="06">Junio</option>`);
		sb.append(`<option value="07">Julio</option>`);
		sb.append(`<option value="08">Agosto</option>`);
		sb.append(`<option value="09">Septiembre</option>`);
		sb.append(`<option value="10">Octubre</option>`);
		sb.append(`<option value="11">Noviembre</option>`);
		sb.append(`<option value="12">Diciembre</option>`);
		let _html = sb.toString();
		if (!UIEmpty(valor)) {
			valor = valor == true ? new Date().getMonths() : valor;
			_html = _html.replace(`value="${valor}">`, `value="${valor}" selected>`);
		}
		$(id).html(_html);
	},
	setProvincia: async function (id, cod_departamento, value, texto) {
		return await UIFunctions.setSelectCore('Ajax', 'getTypeProvince', { IdDepartment: cod_departamento }, id, value, texto);
	},
	setDistrito: async function (id, cod_provincia, value, texto) {
		//await UIFunctions._setPrivateSelect('Ajax', 'getTypeDistrict', { IdProvince: cod_provincia }, false, id, value, texto);
		return await UIFunctions.setSelectCore('Ajax', 'getTypeDistrict', { IdProvince: cod_provincia }, id, value, texto);
	},
	_setPrivateSelect: async function (Class, Method, Params, Debug, id, value, texto) {
		if (UIEmpty(value)) {
			value = "codigo";
		}
		if (UIEmpty(texto)) {
			texto = "nombre";
		}
		var html = '';
		var ParamsDefault = {
			Options: {
				Operador: null,
				Filtro: [],
				Accion: null,
			},
		};
		$.extend(true, ParamsDefault, Params);
		await UIAjax.invokeMethodAsync(Class, Method, ParamsDefault, Debug).then(function (r) {
			var container = $(id);
			container.empty();
			if (!UIEmpty(r.draw)) {
				if (value == "__stringify") {
					$.each(r.data, function (i, v) {
						html += `<option value='${JSON.stringify(v)}'>${v[texto]}</option>`;
					});
				}
				else {
					$.each(r.data, function (i, v) {
						html += `<option value='${v[value]}'>${v[texto]}</option>`;
					});
				}
			}
			else {
				if (value == "__stringify") {
					$.each(r, function (i, v) {
						html += `<option value='${JSON.stringify(v)}'>${v[texto]}</option>`;
					});
				}
				else {
					$.each(r, function (i, v) {
						html += `<option value='${v[value]}'>${v[texto]}</option>`;
					});
				}
			}
			if (!UIEmpty(ParamsDefault)) {
				var _source = ParamsDefault.source;
				if (!UIEmpty(_source) && _source.length > 0) {
					var _html = "";
					$.each(_source, function (i, v) {
						_html += `<option value='${v.codigo}'>${v.nombre}</option>`;
					});
					//'A' Antes
					//'D' Después
					if (ParamsDefault.source_direction == "D") {
						html = html + _html;
					}
					else {
						html = _html + html;
					}
				}
			}
			container.append(html);
			var fnOperador = function (key) {
				if (!UIEmpty(ParamsDefault[key].Operador)) {
					switch (ParamsDefault[key].Operador) {
						case "=": case "!=":
							if (["E", "C"].indexOf(ParamsDefault[key].Accion) > -1) {
								let fOptionsIN = _.filter(container.find('option'), (ths) => ParamsDefault[key].Filtro.indexOf($.trim(ths.value)) > -1);
								let fOptionsNOTIN = _.filter(container.find('option'), (ths) => ParamsDefault[key].Filtro.indexOf($.trim(ths.value)) == -1);
								if (ParamsDefault[key].Operador == "=") {
									if (ParamsDefault[key].Accion == "E") {
										$(fOptionsIN).remove();
									}
									else {
										$(fOptionsNOTIN).remove();
									}
								}
								else {
									if (ParamsDefault[key].Accion == "E") {
										$(fOptionsNOTIN).remove();
									}
									else {
										$(fOptionsIN).remove();
									}
								}
								html = container.html();
							}
							else {
								console.warn(`Error para la acción: ${ParamsDefault[key].Accion} Valor esperado : [E (Eliminar), C (Conservar)]`);
							}
							break;
						default:
							console.warn(`Error para el operador: ${ParamsDefault[key].Operador} Valor esperado: [=, !=]`);
							break;
					}
				}
			};
			if (!UIEmpty(ParamsDefault.Options.Operador)) {
				fnOperador("Options");
			}
		});
		return html;
	},
	setSelect: async function (Class, Method, Params, Debug, id, value, texto) {
		return await UIFunctions._setPrivateSelect(Class, Method, Params, Debug, id, value, texto);
	},
	setSelect2: async function (Source, id, value, texto) {
		if (UIEmpty(value)) {
			value = "codigo";
		}
		if (UIEmpty(texto)) {
			texto = "nombre";
		}
		var container = $(id);
		container.empty();
		var html = '';
		if (!UIEmpty(Source.draw)) {
			if (value == "__stringify") {
				$.each(Source.data, function (i, v) {
					html += `<option value='${JSON.stringify(v)}'>${v[texto]}</option>`;
				});
			}
			else {
				$.each(r.data, function (i, v) {
					html += `<option value='${v[value]}'>${v[texto]}</option>`;
				});
			}
		}
		else {
			if (value == "__stringify") {
				$.each(Source, function (i, v) {
					html += `<option value='${JSON.stringify(v)}'>${v[texto]}</option>`;
				});
			}
			else {
				$.each(Source, function (i, v) {
					html += `<option value='${v[value]}'>${v[texto]}</option>`;
				});
			}
		}
		container.append(html);
		return html;
	},
	setSelectCore: async function (Class, Method, Params, id, value, texto) {
		if (UIEmpty(value)) {
			value = "__stringify";
		}
		if (UIEmpty(texto)) {
			texto = "{nombre}";
		}
		var html = '';
		var ParamsDefault = {
			Debug: false,
			Options: {
				Operador: null,
				Filtro: [],
				Accion: null,
			},
		};
		$.extend(true, ParamsDefault, Params);

		var jsonResponse = {
			success: false,
			r: null,
			html: '',
		}

		await UIAjax.invokeMethodAsync(Class, Method, ParamsDefault, ParamsDefault.Debug).then(function (r) {
			jsonResponse.success = true;
			jsonResponse.r = r;
			var container = $(id);
			container.empty();
			var _source = [];
			if (!UIEmpty(r.draw)) {
				_source = r.data;
			}
			else {
				_source = r;
			}
			if (value == "__stringify") {
				$.each(_source, function (i, v) {
					html += `<option value='${JSON.stringify(v)}'>${texto.formatUnicorn(v)}</option>`;
				});
			}
			else if (value.includes('{') && value.includes('}')) {
				$.each(_source, function (i, v) {
					html += `<option value='${value.formatUnicorn(v)}'>${texto.formatUnicorn(v)}</option>`;
				});
			}
			else {
				$.each(_source, function (i, v) {
					html += `<option value='${v[value]}'>${texto.formatUnicorn(v)}</option>`;
				});
			}

			if (!UIEmpty(ParamsDefault)) {
				var _source = ParamsDefault.source;
				if (!UIEmpty(_source) && _source.length > 0) {
					var _html = "";
					$.each(_source, function (i, v) {
						_html += `<option value='${v.codigo}'>${v.nombre}</option>`;
					});
					//'A' Antes
					//'D' Después
					if (ParamsDefault.source_direction == "D") {
						html = html + _html;
					}
					else {
						html = _html + html;
					}
				}
			}
			container.append(html);
			var fnOperador = function (key) {
				if (!UIEmpty(ParamsDefault[key].Operador)) {
					switch (ParamsDefault[key].Operador) {
						case "=": case "!=":
							if (["E", "C"].indexOf(ParamsDefault[key].Accion) > -1) {
								let fOptionsIN = _.filter(container.find('option'), (ths) => ParamsDefault[key].Filtro.indexOf($.trim(ths.value)) > -1);
								let fOptionsNOTIN = _.filter(container.find('option'), (ths) => ParamsDefault[key].Filtro.indexOf($.trim(ths.value)) == -1);
								if (ParamsDefault[key].Operador == "=") {
									if (ParamsDefault[key].Accion == "E") {
										$(fOptionsIN).remove();
									}
									else {
										$(fOptionsNOTIN).remove();
									}
								}
								else {
									if (ParamsDefault[key].Accion == "E") {
										$(fOptionsNOTIN).remove();
									}
									else {
										$(fOptionsIN).remove();
									}
								}
								html = container.html();
							}
							else {
								console.warn(`Error para la acción: ${ParamsDefault[key].Accion} Valor esperado : [E (Eliminar), C (Conservar)]`);
							}
							break;
						default:
							console.warn(`Error para el operador: ${ParamsDefault[key].Operador} Valor esperado: [=, !=]`);
							break;
					}
				}
			};
			if (!UIEmpty(ParamsDefault.Options.Operador)) {
				fnOperador("Options");
			}
		});
		jsonResponse.html = html;
		return jsonResponse;
	},
	getPARAM: function (idParent) {
		var jsonResult = {};
		var padre = $(idParent);
		var hijos = $('.PARAM', padre);
		var name;
		$.each(hijos, function (i, v) {
			name = v.getAttribute('data-name');
			if (name != null) {
				jsonResult[name] = v.value;
			}
		});
		return jsonResult;
	}
};

var UIConvert = {
	Base64toBlob: function (base64Data, contentType) {
		contentType = contentType || '';
		var sliceSize = 1024;
		var byteCharacters = atob(base64Data);
		var bytesLength = byteCharacters.length;
		var slicesCount = Math.ceil(bytesLength / sliceSize);
		var byteArrays = new Array(slicesCount);

		for (var sliceIndex = 0; sliceIndex < slicesCount; ++sliceIndex) {
			var begin = sliceIndex * sliceSize;
			var end = Math.min(begin + sliceSize, bytesLength);

			var bytes = new Array(end - begin);
			for (var offset = begin, i = 0; offset < end; ++i, ++offset) {
				bytes[i] = byteCharacters[offset].charCodeAt(0);
			}
			byteArrays[sliceIndex] = new Uint8Array(bytes);
		}
		return new Blob(byteArrays, { type: contentType });
	},
	isDate: function (Valor) {
		//Sólo funciona para dd/mm/yyyy
		var regFecha = /^(?:(?:(?:0?[1-9]|1\d|2[0-8])[/](?:0?[1-9]|1[0-2])|(?:29|30)[/](?:0?[13-9]|1[0-2])|31[/](?:0?[13578]|1[02]))[/](?:0{2,3}[1-9]|0{1,2}[1-9]\d|0?[1-9]\d{2}|[1-9]\d{3})|29[/]0?2[/](?:\d{1,2}(?:0[48]|[2468][048]|[13579][26])|(?:0?[48]|[13579][26]|[2468][048])00))$/;
		return regFecha.test(Valor);
		//let regExpHora = /^(?:[01][0-9]|2[0-3])[-:h][0-5][0-9]$/;
	},
	Base64Lite: { _keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=", encode: function (e) { var t = ""; var n, r, i, s, o, u, a; var f = 0; e = UIConvert.Base64Lite._utf8_encode(e); while (f < e.length) { n = e.charCodeAt(f++); r = e.charCodeAt(f++); i = e.charCodeAt(f++); s = n >> 2; o = (n & 3) << 4 | r >> 4; u = (r & 15) << 2 | i >> 6; a = i & 63; if (isNaN(r)) { u = a = 64 } else if (isNaN(i)) { a = 64 } t = t + this._keyStr.charAt(s) + this._keyStr.charAt(o) + this._keyStr.charAt(u) + this._keyStr.charAt(a) } return t }, decode: function (e) { var t = ""; var n, r, i; var s, o, u, a; var f = 0; e = e.replace(/[^A-Za-z0-9\+\/\=]/g, ""); while (f < e.length) { s = this._keyStr.indexOf(e.charAt(f++)); o = this._keyStr.indexOf(e.charAt(f++)); u = this._keyStr.indexOf(e.charAt(f++)); a = this._keyStr.indexOf(e.charAt(f++)); n = s << 2 | o >> 4; r = (o & 15) << 4 | u >> 2; i = (u & 3) << 6 | a; t = t + String.fromCharCode(n); if (u != 64) { t = t + String.fromCharCode(r) } if (a != 64) { t = t + String.fromCharCode(i) } } t = UIConvert.Base64Lite._utf8_decode(t); return t }, _utf8_encode: function (e) { e = e.replace(/\r\n/g, "\n"); var t = ""; for (var n = 0; n < e.length; n++) { var r = e.charCodeAt(n); if (r < 128) { t += String.fromCharCode(r) } else if (r > 127 && r < 2048) { t += String.fromCharCode(r >> 6 | 192); t += String.fromCharCode(r & 63 | 128) } else { t += String.fromCharCode(r >> 12 | 224); t += String.fromCharCode(r >> 6 & 63 | 128); t += String.fromCharCode(r & 63 | 128) } } return t }, _utf8_decode: function (e) { var t = ""; var n = 0; var r = c1 = c2 = 0; while (n < e.length) { r = e.charCodeAt(n); if (r < 128) { t += String.fromCharCode(r); n++ } else if (r > 191 && r < 224) { c2 = e.charCodeAt(n + 1); t += String.fromCharCode((r & 31) << 6 | c2 & 63); n += 2 } else { c2 = e.charCodeAt(n + 1); c3 = e.charCodeAt(n + 2); t += String.fromCharCode((r & 15) << 12 | (c2 & 63) << 6 | c3 & 63); n += 3 } } return t } },
	Base64: {

		// private property
		_keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",

		// public method for encoding
		encode: function (input) {
			var output = "";
			var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
			var i = 0;

			input = UIConvert.Base64._utf8_encode(input);

			while (i < input.length) {

				chr1 = input.charCodeAt(i++);
				chr2 = input.charCodeAt(i++);
				chr3 = input.charCodeAt(i++);

				enc1 = chr1 >> 2;
				enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
				enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
				enc4 = chr3 & 63;

				if (isNaN(chr2)) {
					enc3 = enc4 = 64;
				} else if (isNaN(chr3)) {
					enc4 = 64;
				}

				output = output +
					this._keyStr.charAt(enc1) + this._keyStr.charAt(enc2) +
					this._keyStr.charAt(enc3) + this._keyStr.charAt(enc4);
			}
			return output;
		},

		// public method for decoding
		decode: function (input) {
			var output = "";
			var chr1, chr2, chr3;
			var enc1, enc2, enc3, enc4;
			var i = 0;

			input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

			while (i < input.length) {

				enc1 = this._keyStr.indexOf(input.charAt(i++));
				enc2 = this._keyStr.indexOf(input.charAt(i++));
				enc3 = this._keyStr.indexOf(input.charAt(i++));
				enc4 = this._keyStr.indexOf(input.charAt(i++));

				chr1 = (enc1 << 2) | (enc2 >> 4);
				chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
				chr3 = ((enc3 & 3) << 6) | enc4;

				output = output + String.fromCharCode(chr1);

				if (enc3 != 64) {
					output = output + String.fromCharCode(chr2);
				}
				if (enc4 != 64) {
					output = output + String.fromCharCode(chr3);
				}
			}

			output = UIConvert.Base64._utf8_decode(output);

			return output;
		},

		// private method for UTF-8 encoding
		_utf8_encode: function (string) {
			string = string.replace(/\r\n/g, "\n");
			var utftext = "";

			for (var n = 0; n < string.length; n++) {

				var c = string.charCodeAt(n);

				if (c < 128) {
					utftext += String.fromCharCode(c);
				}
				else if ((c > 127) && (c < 2048)) {
					utftext += String.fromCharCode((c >> 6) | 192);
					utftext += String.fromCharCode((c & 63) | 128);
				}
				else {
					utftext += String.fromCharCode((c >> 12) | 224);
					utftext += String.fromCharCode(((c >> 6) & 63) | 128);
					utftext += String.fromCharCode((c & 63) | 128);
				}
			}
			return utftext;
		},

		// private method for UTF-8 decoding
		_utf8_decode: function (utftext) {
			var string = "";
			var i = 0;
			var c = c1 = c2 = 0;

			while (i < utftext.length) {

				c = utftext.charCodeAt(i);

				if (c < 128) {
					string += String.fromCharCode(c);
					i++;
				}
				else if ((c > 191) && (c < 224)) {
					c2 = utftext.charCodeAt(i + 1);
					string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
					i += 2;
				}
				else {
					c2 = utftext.charCodeAt(i + 1);
					c3 = utftext.charCodeAt(i + 2);
					string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
					i += 3;
				}
			}
			return string;
		}
	},
	CSVtoJSON: function (csv, splitLine, splitColumn) {
		try {
			splitLine = splitLine ?? '¬';
			splitColumn = splitColumn ?? '|';
			var lines = csv.split(splitLine);
			var result = [];

			// NOTE: If your columns contain commas in their values, you'll need
			// to deal with those before doing the next step 
			// (you might convert them to &&& or something, then covert them back later)
			// jsfiddle showing the issue https://jsfiddle.net/
			var headers = lines[0].split(splitColumn);

			for (var i = 1; i < lines.length; i++) {

				var obj = {};
				var currentline = lines[i].split(splitColumn);

				for (var j = 0; j < headers.length; j++) {
					obj[headers[j]] = currentline[j];
				}

				result.push(obj);

			}

			//return result; //JavaScript object
			return JSON.stringify(result); //JSON
		} catch (e) {

		}
	},
};

var UIHtmlDecode = function (text) {
	var entities = [
		['amp', '&'],
		['apos', '\''],
		['#x27', '\''],
		['#x2F', '/'],
		['#39', '\''],
		['#47', '/'],
		['lt', '<'],
		['gt', '>'],
		['nbsp', ' '],
		['quot', '"'],
		["#xAC", "¬"],
	];
	for (var i = 0, max = entities.length; i < max; ++i) {
		text = text.replace(new RegExp('&' + entities[i][0] + ';', 'g'), entities[i][1]);
	}
	return text;
}

var UIEvent = {
	Disabled: {
		Change: function (IdSource, ValueSource, IdTarget, TriggerChange) {
			$(IdSource).off('change').on('change', function (e) {
				var ths = $(this);
				var disabled = false;
				if (ths.is('input[type=checkbox]')) {
					disabled = (ths.is(':checked') == ValueSource);
				}
				else {
					disabled = (ths.val() == ValueSource);
				}
				if (disabled) {
					$(IdTarget).attr('disabled', 'disabled');
				}
				else {
					$(IdTarget).removeAttr('disabled');
				}
			});
			TriggerChange = TriggerChange || false;
			if (TriggerChange) {
				$(IdTarget).trigger('change');
			}
		}
	},
	FileChange: function (IdButton, IdFile, Action, IsDev) {
		$(IdFile).off('click').on('click', function () {
			var ths = $(this);
			ths.val(null);
		});
		//$(IdFile).on('change', async function (e) {
		//    //e.preventDefault();
		//    if (!UIFnEmpty(Action)) {
		//        await Action(this, e);
		//    }
		//});
		$(IdFile).off('change').on('change', Action);
		$(IdButton).off('click').on('click', function () {
			if (IsDev) {
				UIMensaje.DESARROLLO();
				return;
			}
			$(IdFile).trigger('click');
		});
	}
};

var UIModal = {
	TryRpte: async function (Class, Method, Params) {
		try {
			await UIModal.Rpte(Class, Method, Params);
		} catch (e) {
			UILoadingDefault("hide");
			UIMensaje.ERRORINESPERADO();
		}
	},
	Rpte: async function (Class, Method, Params) {
		var options = {
			id: "myFullEmbed",
			height: "80vh",
			pdfOpenParams: {
				pagemode: "thumbs",
				page: 1
			},
			forcePDFJS: true, //default false
		};
		await UILoadingDefault("show");
		await UIAjax.invokeBlobAsync(Class, Method, Params, true).then(function (r) {
			var em = PDFObject.embed(r, "#showFullDoc", options);
			$('#modalFullRpte').modal('show');
		}).finally(function () {
			UILoadingDefault("hide");
		});
	},
	RptePost: async function (Class, Method, Params, Options) {
		var options = {
			id: "myFullEmbed",
			height: "80vh",
			pdfOpenParams: {
				pagemode: "thumbs",
				page: 1
			},
			forcePDFJS: true, //default false
		};
		$.extend(true, options, Options);
		await UILoadingDefault("show");
		await UIAjax.invokeBlobPostAsync(Class, Method, Params, true).then(function (r) {
			if (!UIEmpty(Params.renderIn)) {
				var em = PDFObject.embed(r, Params.renderIn, options);
			}
			else {
				var em = PDFObject.embed(r, "#showFullDoc", options);
				$('#modalFullRpte').modal('show');
			}
		}).finally(function () {
			UILoadingDefault("hide");
		});
	},
	RptePostNone: async function (Class, Method, Params) {
		return await UIModal.RptePost(Class, Method, Params, { options: { pagemode: 'none' } });
	},
	DataTable: async function (Class, Method, Params, Options) {
		var optionsDefault = {
			TituloDeModal: '',
			//iDisplayLength:13,
			initComplete: function (settings, json) { },
			dom: 'Bfrtip',
			searching: false,
			info: false,
			paging: false,
			buttons: [],
			columns: [],
			createdRow: function (row, data, dataIndex) {
			}
		};
		$.extend(true, optionsDefault, Options);
		await UILoadingDefault("show");
		$('#h4TitleMFULLDT').html(optionsDefault.TituloDeModal);
		await UIAjax.invokeMethodAsync(Class, Method, Params, Options.Debug).then(function (r) {
			$('#tableMFULLDT').DataTable({
				data: r.data,
				initComplete: function (settings, json) {
					optionsDefault.initComplete(settings, json);
					$('#modalFullDataTable').modal('show');
				},
				dom: optionsDefault.dom,
				searching: optionsDefault.searching,
				info: optionsDefault.info,
				paging: optionsDefault.paging,
				buttons: optionsDefault.buttons,
				columns: optionsDefault.columns,
				createdRow: optionsDefault.createdRow,
			});
		}).finally(function () {
			UILoadingDefault("hide");
		});
	},
	Inconsistencia: async function (Class, Method, Params, Options) {
		var optionsDefault = {
			Title: "Inconsistencias",
			Debug: false,
			keytitle: 'codigo',
			valuetitle: 'codigo',
			valuebody: 'nombre',
			XLSX: {
				show: false,
				action: async function () {
					UIMensaje.DESARROLLO();
				}
			}
		};
		$.extend(true, optionsDefault, Options);
		await UILoadingDefault("show");
		var container = $('#containerUIBody');
		var btnExportarXLSXMUII = $('#btnExportarXLSXMUII');
		btnExportarXLSXMUII.addClass('d-none');
		container.empty();
		await UIAjax.invokeMethodAsync(Class, Method, Params, optionsDefault.Debug).then(function (r) {
			var html = '';
			var head = _.uniq(r, x => x[optionsDefault.keytitle]);
			for (var i = 0; i < head.length; i++) {
				let jsonSingle = head[i];
				let body = _.where(r, { [optionsDefault.keytitle]: jsonSingle[optionsDefault.keytitle] });
				let iddiv = `collapseMUII${jsonSingle[optionsDefault.keytitle]}`;
				var htmlAux = `<a href='#${iddiv}' class='btn btn-primary btn-block text-left py-2 my-1 d-flex justify-content-between align-items-center' data-toggle='collapse' data-length='${body.length}'>
								${jsonSingle[optionsDefault.valuetitle]}
							   <span class='badge badge-light my-auto'>${body.length}</span></a>`; //badge-pill

				var htmlUL = `<ul class='list-group'>`;
				for (var j = 0; j < body.length; j++) {
					let jsonChild = body[j];
					htmlUL += `<li class='list-group-item list-group-item-action list-group-item-light py-1' data-nombre='${jsonChild[optionsDefault.valuebody]}'>${jsonChild[optionsDefault.valuebody]}</li>`
				}
				htmlUL += '</ul>'
				htmlAux += `<div id='${iddiv}' class='collapse'>${htmlUL}</div>`;
				html += htmlAux;
			}
			//container.append('<input class="form-control" id="txtBusquedaMUII" type="text" placeholder="Search.." />');
			container.append(html);
			//$('#txtBusquedaMUII').keyup(function () {
			//    var matches = $('#containerUIBody').find('li:contains(' + $(this).val() + ') ');
			//    $('li', '#containerUIBody').not(matches).slideUp();
			//    matches.slideDown();
			//});
			$('#txtBusquedaMUII').off('keyup').on('keyup', function (e) {
				var Aa = $("a", container);
				var Adiv = $("div.collapse", container);
				var value = $(this).val().toLowerCase();
				Aa.removeClass('hide');
				Adiv.removeClass('show');
				$("li", container).html(function (i, v) {
					return $(this).attr('data-nombre');
				});
				$("li", container).filter(function (i, v) {
					$(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
				});

				$.each(Aa, function (i, v) {
					var divCollapse = $(v.getAttribute('href'));
					var Ali = $('li:not([style="display: none;"])', divCollapse);
					divCollapse.addClass('show');
					if (Ali.length == 0) {
						$(v).addClass('hide');
					}
					else {
						if (!UIEmpty(value)) {
							$.each(Ali, function (i, v) {
								var re = new RegExp("(" + value + ")", "gi");
								$(v).html((i, html) => html.replace(re, "<span class='highlight'>$1</span>"));
							});
						}
						$('span', v).html(Ali.length);
					}
				});
				if (UIEmpty(value)) {
					Adiv.removeClass('show');
					$.each(Aa, function (i, v) {
						$('span', v).html(v.getAttribute('data-length'));
					});
					//$('span.highlight', container).remove();
				}
			});
			btnExportarXLSXMUII.off('click').on('click', optionsDefault.XLSX.action);
			if (optionsDefault.XLSX.show) {
				btnExportarXLSXMUII.removeClass('d-none');
			}
		}).finally(function () {
			UILoadingDefault("hide");
		});

		$('#h4titleMUII').html(optionsDefault.Title);
		$('#modalUIInconsistencia').modal('show');
	},
	Inconsistencia2: async function (Class, Method, Params, Options) {
		var optionsDefault = {
			Title: "Inconsistencias",
			Debug: false,
			id: 'codigo',//id de cabecera
			idpadre: 'codigo_padre',//id del padre que se hará match con la cabecera
			valuetitle: 'codigo', //el título que se mostrará
			//valuefilter: 'nombre', //lo que se va filtrar
			valuebody: 'nombre', //lo que se va mostrar
			XLSX: {
				show: false,
				action: async function () {
					UIMensaje.DESARROLLO();
				}
			}
		};
		$.extend(true, optionsDefault, Options);
		await UILoadingDefault("show");
		$('#txtBusquedaMUII').val('');
		$('#divAlterMUII').addClass('d-none');
		var container = $('#containerUIBody');
		var btnExportarXLSXMUII = $('#btnExportarXLSXMUII');
		btnExportarXLSXMUII.addClass('d-none');
		container.empty();
		var GET_VALUE = async function (valor, json) {
			if (typeof valor == "function") {
				return await valor(json);
			}
			else {
				return json[valor];
			}
		};
		var RETURN_VALUE = async function (valor, json) {
			if (typeof valor == "function") {
				return await valor(json);
			}
			else {
				return valor;
			}
		};
		var Titulo;
		await UIAjax.invokeMethodAsync(Class, Method, Params, optionsDefault.Debug).then(async function (r) {
			Titulo = await RETURN_VALUE(optionsDefault.Title, r);
			var thead = r.table;
			var tbody = r.table1;
			var html = '';
			var head = thead;
			for (var i = 0; i < head.length; i++) {
				let jsonSingle = head[i];
				let body = _.where(tbody, { [optionsDefault.idpadre]: jsonSingle[optionsDefault.id] });
				let iddiv = `collapseMUII${jsonSingle[optionsDefault.id]}`;
				//var htmlAux = `<a href='#${iddiv}' class='btn btn-primary btn-block text-left py-2 my-1 d-flex justify-content-between align-items-center' data-toggle='collapse' data-length='${body.length}'>`;
				var htmlAux = `<a href='#${iddiv}' class='btn btn-primary a-inconsistencia-primary' data-toggle='collapse' data-length='${body.length}'>`;
				htmlAux += `${await GET_VALUE(optionsDefault.valuetitle, jsonSingle)}`;
				htmlAux += `<span class='badge badge-light my-auto'>${body.length}</span></a>`;
				var htmlUL = `<ul class='list-group'>`;
				for (var j = 0; j < body.length; j++) {
					let jsonChild = body[j];
					let descripcion = await GET_VALUE(optionsDefault.valuebody, jsonChild);
					//console.log(descripcion);
					htmlUL += `<li class='list-group-item list-group-item-action list-group-item-light py-1' data-nombre="${descripcion}">`;
					htmlUL += `${descripcion}`;
					htmlUL += `</li>`;
				}
				htmlUL += `</ul>`;
				htmlAux += `<div id='${iddiv}' class='collapse'>${htmlUL}</div>`;
				html += htmlAux;
			}
			container.append(html);

			$('#txtBusquedaMUII').off('keyup').on('keyup', function (e) {
				var Aa = $("a", container);
				var Adiv = $("div.collapse", container);
				var value = $(this).val().toLowerCase();
				Aa.removeClass('hide');
				Adiv.removeClass('show');
				$("li", container).html(function (i, v) {
					return $(this).attr('data-nombre');
				});
				$("li", container).filter(function (i, v) {
					$(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
				});
				$.each(Aa, function (i, v) {
					var divCollapse = $(v.getAttribute('href'));
					var Ali = $('li:not([style="display: none;"])', divCollapse);
					divCollapse.addClass('show');
					if (Ali.length == 0) {
						$(v).addClass('hide');
					}
					else {
						if (!UIEmpty(value)) {
							$.each(Ali, function (i, v) {
								var re = new RegExp("(?<!<\/?[^>]*|&[^;]*)(" + value + ")", "gi");
								$(v).html((i, html) => html.replace(re, "<span class='highlight'>$1</span>"));
							});
						}
						$('span', v).html(Ali.length);
					}
				});
				if (UIEmpty(value)) {
					Adiv.removeClass('show');
					$.each(Aa, function (i, v) {
						$('span', v).html(v.getAttribute('data-length'));
					});
				}
				if (Aa.length == Aa.filter((i, x) => $(x).hasClass('hide')).length) {
					$('#divAlterMUII').removeClass('d-none').html('Sin resultados para: <b>' + this.value + '</b>');
				}
				else {
					$('#divAlterMUII').addClass('d-none');
				}
			});
			btnExportarXLSXMUII.off('click').on('click', optionsDefault.XLSX.action);
			if (optionsDefault.XLSX.show) {
				btnExportarXLSXMUII.removeClass('d-none');
			}

		}).finally(function () {
			UILoadingDefault("hide");
		});
		$('#row2MUII').attr('style', `max-height: 60vh;overflow-y: scroll;`);
		$('#h4titleMUII').html(Titulo);
		$('#modalUIInconsistencia').modal('show');

		$('#modalUIInconsistencia').on('hidden.bs.modal', function () {
			$('#row2MUII').removeAttr('style');
		})
	},
	Table: async function (Options) {
		var optionsDefault = {
			TituloDeModal: '',
			//iDisplayLength:13,
			modalSize: 'modal-lg',
			multipleSelect: false,
			source: [],
			initComplete: function (settings, json) { },
			dom: 'Bfrtip',
			searching: true,
			scrollY: '50vh',
			scrollCollapse: true,
			info: false,
			paging: false,
			buttons: [],
			columns: [],
			createdRow: function (row, data, dataIndex) { }
		};
		$.extend(true, optionsDefault, Options);
		/*await UILoadingDefault("show");*/
		var nr = UIMath.Random();
		var ID_MODAL = `modal${nr}`;
		var ID_TITLE = `h4Title${nr}`;
		var ID_TABLE = `table${nr}`;
		var ID_ACEPTAR = `btnAceptar${nr}`;
		var ID_CANCELAR = `btnCancelar${nr}`;

		var AHTML = new StringBuilder();
		AHTML.append(`<div id="${ID_MODAL}" class="modal fade control-compress" role="dialog">`);
		AHTML.append(`	<div class="modal-dialog ${optionsDefault.modalSize} modal-dialog-centered">`);
		AHTML.append(`		<div class="modal-content">`);
		AHTML.append(`			<div class="modal-header-full-width modal-header">`);
		AHTML.append(`				<div class="container">`);
		AHTML.append(`					<div class="row justify-content-between">`);
		AHTML.append(`						<div class="col my-auto pl-0">`);
		AHTML.append(`							<label id="${ID_TITLE}" class="h4 mb-0 text-dark"></label>`);
		AHTML.append(`						</div>`);
		AHTML.append(`						<div class="col-auto">`);
		AHTML.append(`							<label data-dismiss="modal" class="m-0 f-24 cursor-pointer "><span aria-hidden="true">×</span></label>`);
		AHTML.append(`						</div>`);
		AHTML.append(`					</div>`);
		AHTML.append(`				</div>`);
		AHTML.append(`			</div>`);
		AHTML.append(`			<div class="modal-body">`);
		AHTML.append(`				<div class="row mt-3">`);
		AHTML.append(`					<div class="col">`);
		AHTML.append(`						<div class="table-responsive">`);
		AHTML.append(`							<table id="${ID_TABLE}" class="w-100 table table-compress table-bordered"></table>`);
		AHTML.append(`						</div>`);
		AHTML.append(`					</div>`);
		AHTML.append(`				</div>`);
		AHTML.append(`			</div>`);
		AHTML.append(`			<div class="modal-footer">`);
		AHTML.append(`				<div class="row">`);
		AHTML.append(`					<div class="col-auto">`);
		AHTML.append(`						<button id="${ID_ACEPTAR}" class="btn btn-primary">ACEPTAR</button>`);
		AHTML.append(`						<button id="${ID_CANCELAR}" class="btn btn-danger">CANCELAR</button>`);
		AHTML.append(`					</div>`);
		AHTML.append(`				</div>`);
		AHTML.append(`			</div>`);
		AHTML.append(`		</div>`);
		AHTML.append(`	</div>`);
		AHTML.append(`</div>`);

		$(document.body).append(AHTML.toString());

		document.getElementById(ID_TITLE).innerHTML = optionsDefault.TituloDeModal;

		$(`#${ID_TITLE}`).html(optionsDefault.TituloDeModal);

		var jTableRND = $(`#${ID_TABLE}`);
		var jModal = $(`#${ID_MODAL}`);

		var tableRND = jTableRND.DataTable({
			data: optionsDefault.source,
			initComplete: function (settings, json) {
				optionsDefault.initComplete(settings, json);
				//$.fn.dataTable.tables({ visible: true, api: true }).columns.adjust();
				//console.log($('thead', jTableRND));
				$(tableRND.table().header()).addClass('thead-sky thead-sticky');
				$(tableRND.table().body()).addClass('cursor-pointer');
				$(`#${ID_MODAL}`).modal('show');
			},
			dom: optionsDefault.dom,
			searching: optionsDefault.searching,
			info: optionsDefault.info,
			paging: optionsDefault.paging,
			scrollY: '50vh',
			scrollCollapse: true,
			buttons: optionsDefault.buttons,
			columns: optionsDefault.columns,
			createdRow: optionsDefault.createdRow,
		});

		jTableRND.on('click', 'tr', function (e) {
			if (optionsDefault.multipleSelect == true) {
				$(this).toggleClass('selected');
			}
			else {
				tableRND.$('tr').removeClass('selected');
				$(this).addClass('selected');
			}
		});

		jModal.on('shown.bs.modal', function () {
			$.fn.dataTable.tables({ visible: true, api: true }).columns.adjust();
			//$.fn.dataTable.tables({ visible: true, api: true }).columns.adjust().draw();
			//document.getElementById(ID_TABLE).focus();
		});

		jModal.on("hidden.bs.modal", function (e) {
			$(this).remove();
		});

		var jsonResponse = {
			SelectedRows: [],
			SingleRow: null,
		};

		return new Promise(async function (resolve, reject) {
			$(`#${ID_ACEPTAR}`).on('click', function (e) {
				jsonResponse.SelectedRows = Array.from(tableRND.rows('.selected').data());
				jsonResponse.SingleRow = jsonResponse.SelectedRows[0] ?? null;
				jModal.modal('hide');
				resolve(jsonResponse);
			});
			$(`#${ID_CANCELAR}`).on('click', function (e) {
				jModal.modal('hide');
				resolve(jsonResponse);
			});

		});
	},
	Dynamic: async function (Options) {
		var optionsDefault = {
			TituloDeModal: '',
			modalSize: 'modal-lg',
		};
		$.extend(true, optionsDefault, Options);
		/*await UILoadingDefault("show");*/
		var nr = UIMath.Random();
		var ID_MODAL = `modal${nr}`;
		var ID_TITLE = `h4Title${nr}`;
		var ID_BODY = `modalBody${nr}`;
		var ID_FOOTER = `modalFooter${nr}`;
		var ID_ACEPTAR = `btnAceptar${nr}`;
		var ID_CANCELAR = `btnCancelar${nr}`;

		var AHTML = new StringBuilder();
		AHTML.append(`<div id="${ID_MODAL}" class="modal fade control-compress" role="dialog">`);
		AHTML.append(`	<div class="modal-dialog ${optionsDefault.modalSize} modal-dialog-centered">`);
		AHTML.append(`		<div class="modal-content">`);
		AHTML.append(`			<div class="modal-header-full-width modal-header">`);
		AHTML.append(`				<div class="container">`);
		AHTML.append(`					<div class="row justify-content-between">`);
		AHTML.append(`						<div class="col my-auto pl-0">`);
		AHTML.append(`							<label id="${ID_TITLE}" class="h4 mb-0 text-dark"></label>`);
		AHTML.append(`						</div>`);
		AHTML.append(`						<div class="col-auto">`);
		AHTML.append(`							<label data-dismiss="modal" class="m-0 f-24 cursor-pointer "><span aria-hidden="true">×</span></label>`);
		AHTML.append(`						</div>`);
		AHTML.append(`					</div>`);
		AHTML.append(`				</div>`);
		AHTML.append(`			</div>`);
		AHTML.append(`			<div id="${ID_BODY}" class="modal-body">`);
		//AHTML.append(`				<div class="row mt-3">`);
		//AHTML.append(`					<div class="col">`);
		//AHTML.append(`						<div class="table-responsive">`);
		//AHTML.append(`							<table id="${ID_TABLE}" class="w-100 table table-compress table-bordered"></table>`);
		//AHTML.append(`						</div>`);
		//AHTML.append(`					</div>`);
		//AHTML.append(`				</div>`);
		AHTML.append(`			</div>`);
		AHTML.append(`			<div id="${ID_FOOTER}" class="modal-footer">`);
		AHTML.append(`				<div class="row">`);
		AHTML.append(`					<div class="col-auto">`);
		AHTML.append(`						<button id="${ID_ACEPTAR}" class="btn btn-primary">ACEPTAR</button>`);
		AHTML.append(`						<button id="${ID_CANCELAR}" class="btn btn-danger">CANCELAR</button>`);
		AHTML.append(`					</div>`);
		AHTML.append(`				</div>`);
		AHTML.append(`			</div>`);
		AHTML.append(`		</div>`);
		AHTML.append(`	</div>`);
		AHTML.append(`</div>`);

		$(document.body).append(AHTML.toString());

		document.getElementById(ID_TITLE).innerHTML = optionsDefault.TituloDeModal;

		//$(`#${ID_TITLE}`).html(optionsDefault.TituloDeModal);

		var jModal = $(`#${ID_MODAL}`);
		var jTitle = $(`#${ID_TITLE}`);
		var jBody = $(`#${ID_BODY}`);
		var jFooter = $(`#${ID_FOOTER}`);
		var jAceptar = $(`#${ID_ACEPTAR}`);
		var jCancelar = $(`#${ID_CANCELAR}`);

		jModal.on('shown.bs.modal', function () { });

		jModal.on("hidden.bs.modal", function (e) {
			$(this).remove();
		});

		var jsonResponse = {
			jModal: jModal,
			jTitle: jTitle,
			jBody: jBody,
			jFooter: jFooter,
			jAceptar: jAceptar,
			jCancelar: jCancelar,
		};

		return new Promise(async function (resolve, reject) {
			//$(`#${ID_ACEPTAR}`).on('click', function (e) {
			//	jModal.modal('hide');
			//	resolve(jsonResponse);
			//});
			//$(`#${ID_CANCELAR}`).on('click', function (e) {
			//	jModal.modal('hide');
			//	resolve(jsonResponse);
			//});
			resolve(jsonResponse);
		});

		//return jsonResponse;
	},
};

var UIHTMLReporte = {
	Get: async function (Class, Method, Params) {
		Params = Params || {};
		return new Promise(function (resolve, reject) {
			URL = `${portal}/${Class}/${Method}?${UIURLParameters.fromJson(Params)}`;
			var xhr = new XMLHttpRequest();
			xhr.responseType = 'json';
			xhr.responseType = "arraybuffer";
			xhr.open("GET", URL, true);
			xhr.onload = function () {
				if (this.status >= 200 && this.status < 300 && !this.responseURL.toLowerCase().endsWith(portal + '/login')) {
					var arrayBuffer = xhr.response; // Note: not oReq.responseText
					if (arrayBuffer) {
						var b = new Uint8Array(arrayBuffer);
						var str64 = new TextDecoder("utf-8").decode(b);
						resolve(UIConvert.Base64Lite.decode(str64));
					}
					else {
						//resolve(null);
						reject(null);
					}
					/*resolve(xhr.response);*/
				} else {
					reject({
						status: this.status,
						statusText: xhr.statusText
					});
				}
			};
			xhr.onerror = function () {
				reject({
					status: this.status,
					statusText: xhr.statusText
				});
			};
			xhr.send();
		});
	},
	TryGet: async function (Class, Method, Params, Action) {
		try {
			await UIHTMLReporte.Get(Class, Method, Params).then(async function (r) {
				//console.log(r);
				//console.log(Action);
				//r = r.replace(/[^/\"_+-<>=a-zA-Z 0-9]+/g, '');
				r = $.trim(r);
				document.body.insertAdjacentHTML('beforeend', r);
				//$('body').append(r);
				if (!UIFnEmpty(Action)) {
					await Action(r);
				}
			});
			UILoadingDefault('hide');
		} catch (e) {
			if (app_config?.debugger.state) {
				console.log(e);
			}
			UILoadingDefault('hide');
			UIMensaje.ERRORINESPERADO();
		}
	},
	TryGetJS: async function (Class, Method, Params, Action) {
		try {
			await UIHTMLReporte.Get(Class, Method, Params).then(async function (r) {
				r = $.trim(r);
				var scriptElm = document.createElement('script');
				scriptElm.innerHTML = r;
				document.body.appendChild(scriptElm);
				if (!UIFnEmpty(Action)) {
					await Action(r);
				}
			});
			UILoadingDefault('hide');
		} catch (e) {
			if (app_config?.debugger.state) {
				console.log(e);
			}
			UILoadingDefault('hide');
			UIMensaje.ERRORINESPERADO();
		}
	}
};

const UIAjaxCore = {
	GetPartial: async function (valor) {
		Params = { valor: valor };
		return new Promise(function (resolve, reject) {
			URL = `${portal}/ajax/core/GetPartial?${UIURLParameters.fromJson(Params)}`;
			var xhr = new XMLHttpRequest();
			xhr.responseType = "arraybuffer";
			xhr.open("GET", URL, true);
			xhr.onload = function () {
				if (this.status >= 200 && this.status < 300 && !this.responseURL.toLowerCase().endsWith(portal + '/login')) {
					var arrayBuffer = xhr.response;
					if (arrayBuffer) {
						var b = new Uint8Array(arrayBuffer);
						var str64 = new TextDecoder("utf-8").decode(b);
						resolve(UIConvert.Base64Lite.decode(str64));
					}
					else {
						reject(null);
					}
				} else {
					reject({
						status: this.status,
						statusText: xhr.statusText
					});
				}
			};
			xhr.onerror = function () {
				reject({
					status: this.status,
					statusText: xhr.statusText
				});
			};
			xhr.send();
		});
	},
	TryGetPartial: async function (valor, Action) {
		try {
			await UIAjaxCore.GetPartial(valor).then(async function (r) {
				r = $.trim(r);
				document.body.insertAdjacentHTML('beforeend', r);
				if (!UIFnEmpty(Action)) {
					await Action(r);
				}
			});
			UILoadingDefault('hide');
		} catch (e) {
			if (app_config?.debugger.state) {
				console.log(e);
			}
			UILoadingDefault('hide');
			UIMensaje.ERRORINESPERADO();
		}
	},
};

var UITree = {
	//Falta modificar, actualmente sólo trabajo de 1 nivel
	//Cortaron el desarrollo por apurados como siempre......
	Create: async function (divTree, Options) {
		var optionsDefault = {
			data: [],
			id: 'codigo',//id de cabecera
			idpadre: 'codigo_padre',//id del padre que se hará match con la cabecera
			valuetitle: 'codigo', //el título que se mostrará
			//valuefilter: 'nombre', //lo que se va filtrar
			valuebody: 'nombre', //lo que se va mostrar
		};
		$.extend(true, optionsDefault, Options);
		var GET_VALUE = async function (valor, json) {
			if (typeof valor == "function") {
				return await valor(json);
			}
			else {
				return json[valor];
			}
		};

		check = true;
		var idUL = `ulTree${UIMath.Random()}`;
		var html = `<ul id='${idUL}' style='cursor:pointer' class='us-n'>`;
		html += "<li>";
		//if (check) html += "<input type='checkbox' class='check'/>";
		var fnCreateInputCheckbox = function () {
			var id = UIMath.Random();
			return `<div class="checkbox checkbox-primary d-inline mr-0" without-label cursor-pointer>
						<input type="checkbox" class="custom-control-input" id="${id}">
						<label class="cr" for="${id}"></label>
					</div>`;
		}

		if (check) html += fnCreateInputCheckbox();
		html += "ABCDADSAD";
		html += "<ul>";
		var nregistros = optionsDefault.data.length;
		for (var i = 0; i < nregistros; i++) {
			var jsonSingle = optionsDefault.data[i];
			if (jsonSingle[optionsDefault.idpadre] == "") {
				html += `<li data-id='${jsonSingle[optionsDefault.id]}' data-action=''>`;
				if (check) html += fnCreateInputCheckbox();
				//if (check) html += "<input type='checkbox' class='check'/>";
				let descripcion = await GET_VALUE(optionsDefault.valuebody, jsonSingle);
				html += descripcion;
				html += await crearSubmenu(optionsDefault.data, jsonSingle[optionsDefault.id], check);
				html += "</li>";
			}
		}
		html += "</ul>";
		html += "</li>";
		html += "</ul>";
		divTree.innerHTML = html;
		configurarTreeViewChecks(check);

		async function crearSubmenu(lista, idPadre, check) {
			check = check | false;
			var html = "<ul>";
			var nregistros = lista.length;
			for (var i = 0; i < nregistros; i++) {
				var jsonChild = lista[i];
				if (jsonChild[optionsDefault.idpadre] == idPadre) {
					html += "<li data-id='";
					html += jsonChild[optionsDefault.id];
					html += "' data-accion='";
					html += "";
					html += "'>";
					//if (check) html += "<input type='checkbox' class='check'/>";
					if (check) html += fnCreateInputCheckbox();
					let descripcion = await GET_VALUE(optionsDefault.valuebody, jsonChild);
					html += descripcion;
					html += await crearSubmenu(lista, jsonChild[optionsDefault.id], check);
					html += "</li>";
				}
			}
			html += "</ul>";
			return html;
		}

		function configurarTreeViewChecks(check) {
			var ulMenu = document.getElementById(idUL);
			ulMenu.onclick = function (event) {
				var liMenu = event.target;
				console.log('configurarTreeViewChecks');
				console.log(liMenu);
				if (liMenu.childNodes.length > 1) {
					var ulSubMenu = liMenu.childNodes[check ? 2 : 1];
					if (ulSubMenu.hasChildNodes()) {
						if (ulSubMenu.style.display == "" || ulSubMenu.style.display == "block") {
							ulSubMenu.style.display = "none";
						}
						else {
							ulSubMenu.style.display = "block";
						}
					}
					else {
						var opcion = liMenu.innerText;
						var accion = liMenu.getAttribute("data-accion");
						seleccionarNodoTreeView(liMenu, opcion, accion);
					}
				}
			}

			var checks = document.getElementsByClassName("check");
			var nchecks = checks.length;
			for (var i = 0; i < nchecks; i++) {
				var check = checks[i];
				check.onclick = function () {
					seleccionarCheckTreeView(this);
				}
			}
		}

		function seleccionarCheckTreeView(chkPadre) {
			var seleccion = chkPadre.checked;
			var liPadre = chkPadre.parentNode;
			var ulPadre = liPadre.childNodes[2];
			if (ulPadre.hasChildNodes()) {
				var nHijos = ulPadre.childNodes.length;
				var liHijos = ulPadre.childNodes;
				var liHijo;
				var chkHijo;
				for (var i = 0; i < nHijos; i++) {
					liHijo = liHijos[i];
					console.log(liHijo);
					chkHijo = liHijo.firstChild;
					chkHijo.checked = seleccion;
					seleccionarCheckTreeView(chkHijo);
				}
			}
		}
		//container.append(html);
	},
};

var UIMInconsistencia = {
	Personal: async function (jsonParam) {
		var Options = {
			keytitle: 'cod_incon',
			valuetitle: 'inconsistencia',
			XLSX: {
				show: true,
				action: async function () {
					UISave.URL(`${portal}/ajax/personal/GetInconsistenciasPersonalXLSX/?` + $.param(jsonParam));
				}
			},
		};
		await UIModal.Inconsistencia('ajax/personal', 'GetInconsistenciasPersonal', jsonParam, Options);
	},
}

var UIDataTable = {
	fn: {
		Colaborador: async function (id, params, options) {
			try {
				var optionsDefault = {
					showLoading: false,

					columns: [
						{ data: "cod_trabajador", name: "cod_trabajador" },
						{ data: "trabajador", name: "trabajador" },
						{ data: "nombre_estado", name: "nombre_estado" },
						{ data: "fecha_ingreso", name: "fecha_ingreso" },
						{ data: "fecha_cese", name: "fecha_cese" },
					],
					initComplete: function (settings, json) { },
					dom: 'Bfrtip',
					//searching: false,
					//info: false,
					paging: false,
					scrollY: "300px",
					scrollX: true,
					scrollCollapse: true,
					buttons: [],
					//columnDefs: [
					//	{ "sortable": false, "searchable": false, "targets": "_all" },
					//],
					createdRow: function (row, data, dataIndex) { }
				};
				var jsonParam = {
					CodigoDeProyecto: '-1',
					CodigoDeCC: '-1',
					CodigoDeUN: '-1',
					CodigoDeLocalidad: '-1',
					CodigoDeArea: '-1',
					CodigoDeTipoPlanilla: '-1',
					Estado: '%',
					Filtro: '',
				};

				$.extend(true, optionsDefault, options);
				$.extend(true, jsonParam, params);

				UIDataTable.setEmpty(id);
				var table;
				return new Promise(async function (resolve, reject) {
					try {
						if (optionsDefault.showLoading) {
							await UILoadingDefault("show");
						}
						await UIAjax.invokeMethodAsync('ajax/core', 'GetColaboradores', jsonParam).then(function (r) {
							table = $(id).DataTable({
								data: r.data,
								initComplete: async function (settings, json) {
									optionsDefault.initComplete.call(this, settings, json);
									if (optionsDefault.showLoading) {
										UILoadingDefault("hide");
									}
									var dttable = table.table();
									var wrapper = $(dttable.container());

									table.jcontainer = $(wrapper);
									table.jheader = $(dttable.header());
									table.jtrs = dttable.$('tr');
									table.isuitable = true;
									table.uiapi = this.api();

									table.jheader.remove();
									//console.log($('div.dataTables_sizing', wrapper));

									$('div.dataTables_sizing', wrapper).css('height', '');

									var iCantidad = 0;
									//var idTask = setInterval(function () {
									//	table.columns.adjust();
									//	iCantidad++;
									//	if (iCantidad == 50) {
									//		clearInterval(idTask);
									//	}
									//}, 60);
									resolve(table);
								},
								dom: optionsDefault.dom,
								searching: optionsDefault.searching,
								//info: optionsDefault.info,
								paging: optionsDefault.paging,
								scrollY: optionsDefault.scrollY,
								scrollX: optionsDefault.scrollX,
								scrollCollapse: optionsDefault.scrollCollapse,
								buttons: optionsDefault.buttons,
								columns: optionsDefault.columns,
								createdRow: optionsDefault.createdRow,
								infoCallback: function (settings, start, end, max, total, pre) {
									//var api = this.api();
									//var pageInfo = api.page.info();
									//return 'Page ' + (pageInfo.page + 1) + ' of ' + pageInfo.pages;
									return `<b>Cantidad de Registros: ${r.data.length}</b>`;
								}
							});
							table.on('search.dt', function () {
								//console.log(this);
								//console.log($('div.dataTables_sizing', this));
								//document
								//console.log(this.querySelectorAll('tr'));
								//document.getElementById('').querySelectorAll('wd').forEach(function (x) {
								//this.querySelectorAll('thead tr .dataTables_sizing').forEach(function (x) {
								//	x.classList.add('oo');
								//	console.log(x);
								//	x.style.height = '';
								//});
								//$('div.dataTables_sizing', this).css('height', '');
							});
							table.on('draw.dt', function () {
								//console.log('3');
								//console.log(this);
								$('div.dataTables_sizing', this).css('height', '');
							});
							//table.on('datachange.dt', function () {
							//	console.log('2');
							//	//updateGraph( GraphData )
							//});
						});
					} catch (e) {
						//reject(e);
						reject(e);
					}
				});
			} catch (e) {
				UILoadingDefault('hide');
				MsgBox(e.message, "warning", false);
			}
		}
	},
	setEmpty: function (id) {
		var tables = $(id);
		if (tables.length > 0) {
			$.each(tables, function (i, v) {
				if ($.fn.dataTable.isDataTable(v)) {
					$(v).DataTable().clear().draw().destroy();
				}
				else {
					$(v).find('tbody').empty();
				}
			});
		}
	},
	setFullEmpty: function (id) {
		var tables = $(id);
		if (tables.length > 0) {
			$.each(tables, function (i, v) {
				var __v = $(v);
				if ($.fn.dataTable.isDataTable(v)) {
					__v.DataTable().clear().draw().destroy();
				}
				__v.empty();
			});
		}
	},
	CreateSource: async function (Options) {
		var optionsDefault = {
			jQuery: '',
			showLoading: false,
			//iDisplayLength:13,
			data: [],
			initComplete: function (settings, json) { },
			dom: 'Bfrtip',
			searching: false,
			info: false,
			select: {
				style: 'single'
			},
			paging: false,
			scrollY: "500px",
			scrollX: true,
			scrollCollapse: true,
			buttons: [],
			columns: [],
			columnDefs: [
				{ "sortable": false, "searchable": false, "targets": "_all" },
			],
			createdRow: function (row, data, dataIndex) {
			}
		};
		$.extend(true, optionsDefault, Options);
		var table;
		if (optionsDefault.showLoading) {
			await UILoadingDefault("show");
		}
		return new Promise(function (resolve, reject) {
			try {
				table = $(optionsDefault.jQuery).DataTable({
					data: optionsDefault.data,
					initComplete: function (settings, json) {
						optionsDefault.initComplete(settings, json);
						if (optionsDefault.showLoading) {
							UILoadingDefault("hide");
						}
						var dttable = table.table();
						var wrapper = $(dttable.container());

						table.jcontainer = $(wrapper);
						table.jheader = $(dttable.header());
						table.jtrs = dttable.$('tr');
						table.isuitable = true;
						table.uiapi = this.api();
						//return table;
						resolve(table);
					},
					dom: optionsDefault.dom,
					searching: optionsDefault.searching,
					info: optionsDefault.info,
					select: optionsDefault.select,
					paging: optionsDefault.paging,
					scrollY: optionsDefault.scrollY,
					scrollX: optionsDefault.scrollX,
					scrollCollapse: optionsDefault.scrollCollapse,
					buttons: optionsDefault.buttons,
					columns: optionsDefault.columns,
					columnDefs: optionsDefault.columnDefs,
					createdRow: optionsDefault.createdRow,
				});
			} catch (e) {
				reject(e);
			}
		});
	},
	CreateColumnCHK: function (key, addclass, disabled) {
		disabled = disabled ?? true;
		return {
			data: key, render: function (data, type, row, meta) {
				return UIFunctions.Checkbox.HTML2(null, data, null, disabled);
			}, className: 'text-center align-middle ' + $.trim(addclass)
		};
	},
	//CreateEnumerator: function (table, index, string_format, ...params) {
	CreateEnumerator: function (table, index, string_format, params) {

		//console.log(table);
		//console.log(index);
		//console.log(string_format);
		//console.log(params);

		string_format = UIEmpty(string_format, '{__}');
		if (UIEmpty(params)) {
			params = {
				__: '',
			};
		}
		//params.splice(0, 0, "-");
		//Si no tiene el search no funciona, buscar una solución
		table.on('order.dt search.dt', function () {
			table.column(index, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
				//params[0] = i + 1;
				params.__ = i + 1;
				//cell.innerHTML = String.format(string_format, params);
				cell.innerHTML = string_format.formatUnicorn(params);
				//console.log(String.format("{0} hola", params));
			});
		}).draw();
	},
	Buttons: {
		Todos: function (ths, fTrue, fFalse, selectAll) {
			var api = ths.api();
			var wrapper = $(api.table().container());
			var trs = api.$('tr');
			trs.on('click', function (e) {
				let ths = $(this);
				ths.toggleClass('selected');
			});
			$('div[id$="_filter"]', wrapper).append('<button type="button" class="float-left btn btn-outline-secondary btnTodosDataTable">Todos</button>');
			wrapper.on('click', 'button.btnTodosDataTable', function (e) {
				var ths = $(this);
				var boolChk = ths.toggleClass('btn-secondary btn-outline-secondary').hasClass('btn-secondary');
				trs.toggleClass('selected', boolChk);
				if (boolChk) {
					if (!UIEmpty(fTrue)) {
						fTrue();
					}
				}
				else {
					if (!UIEmpty(fFalse)) {
						fFalse();
					}
				}
			});
			selectAll = selectAll || false;
			if (selectAll) {
				$('button.btnTodosDataTable', wrapper).trigger('click');
			}
		}
	},
	CButtons: {
		excel: function (options) {
			var optionsDefault = {
				extend: 'excelHtml5',
				text: '<i class="fas fa-file-excel mr-2"></i>Excel',
				className: 'btn-outline-success text-dark',
				filename: 'unknown'
			};
			$.extend(true, optionsDefault, options);
			return {
				extend: optionsDefault.extend,
				text: optionsDefault.text,
				className: optionsDefault.className,
				filename: optionsDefault.filename
			}
		},
	},
	SelectRows: function (ths, multiple, debug, fSelect, pSelect, clickFirstRow) {
		var api = ths.api();
		var trs = api.$('tr');
		//var wrapper = $(api.table().container());
		multiple = multiple || false;
		clickFirstRow = clickFirstRow || false;
		trs.on('click', function (e) {
			let ths = $(this);
			if (!multiple) {
				trs.removeClass('selected');
			}
			if (multiple) {
				ths.toggleClass('selected');
			}
			else {
				ths.addClass('selected');
			}
			if (multiple) {
				var trsSelected = api.$('tr.selected');
				var jsonRows = api.rows(trsSelected).data();
				if (debug) {
					console.log(jsonRows);
				}
				if (!UIEmpty(fSelect)) {
					fSelect(jsonRows, trsSelected, e);
				}
			}
			else {
				var jsonRow = Object.assign({}, api.row(this).data());
				if (debug) {
					console.log(jsonRow);
				}
				if (!UIEmpty(fSelect)) {
					$.extend(true, jsonRow, pSelect);
					fSelect(jsonRow, ths, e);
				}
			}
		});
		if (trs.length > 0 && clickFirstRow) {
			//$('tr:first', trs).trigger('click');
			trs.first().trigger('click');
		}
	},
	SelectRows2: function (table, multiple, debug, fSelect, pSelect) {
		var trs = table.$('tr')
		multiple = multiple || false;
		trs.on('click', function (e) {
			let ths = $(this);
			if (!multiple) {
				trs.removeClass('selected');
			}
			if (multiple) {
				ths.toggleClass('selected');
			}
			else {
				ths.addClass('selected');
			}
			if (multiple) {
				var trsSelected = table.$('tr.selected');
				var jsonRows = table.rows(trsSelected).data();
				if (debug) {
					console.log(jsonRows);
				}
				if (!UIEmpty(fSelect)) {
					fSelect(jsonRows, trsSelected, e);
				}
			}
			else {
				var jsonRow = Object.assign({}, table.row(this).data());
				if (debug) {
					console.log(jsonRow);
				}
				if (!UIEmpty(fSelect)) {
					$.extend(true, jsonRow, pSelect);
					fSelect(jsonRow, ths, e);
				}
			}
		});
	},
	DoubleSelectRow: function (ths, debug, fSelect, pSelect) {
		var api = ths.api();
		var trs = api.$('tr');
		//var wrapper = $(api.table().container());
		trs.on('dblclick', function (e) {
			let ths = $(this);
			trs.removeClass('selected');
			ths.addClass('selected');
			var jsonRow = Object.assign({}, api.row(this).data());
			$.extend(true, jsonRow, pSelect);
			if (debug) {
				console.log(jsonRow);
			}
			if (!UIFnEmpty(fSelect)) {
				fSelect(jsonRow, ths, e);
			}
		});
	},
	AJAXDelete: function (ths, id, key, Class, Method, Parametros, ATrue, AFalse, AAlways) {
		var api = ths.api();
		$(id).off('click').on('click', function (e) {
			var jsonRow = api.row(api.$('tr.selected')).data();
			if (UIEmpty(jsonRow)) {
				UIMensaje.REGISTRO();
				return;
			}
			let registro = UIEmpty(jsonRow[key], '[SELECCIONADO]');
			MsgBox(`Se eliminará el registro: ${registro}.`, 'question', false, true, async function () {
				var jsonParam = {};
				$.each(Parametros, function (i, v) {
					if (i != "__merge") {
						jsonParam[i] = jsonRow[v];
					}
				});
				if (!UIEmpty(Parametros["__merge"])) {
					$.extend(true, jsonParam, Parametros["__merge"]);
				}
				await UIAjax.runFunctionAsync(Class, Method, jsonParam, ATrue, AFalse, AAlways);
			});
		});
	},
};

var UIMaskContainer = {
	Load: function (id, pBefore, pAfter) {
		if (!UIEmpty(containerPage)) {
			containerPage.maskContainer('Cargando');
			if (!UIEmpty(pBefore)) {
				pBefore();
			}
			var container = $(id);
			container.removeClass('d-none');
			$("html, body").animate({
				scrollTop: container.offset().top
			}, 600, "linear", function () {
				if (!UIEmpty(pAfter)) {
					pAfter();
				}
				containerPage.unmaskContainer();
			});
		}
	}
};

var UIMask = {
	Time: function (id, options) {
		var optionsDefault = {
			value: '00:00',
			focus: true,
		};
		$.extend(true, optionsDefault, options);
		var mask = function (val) {
			val = val.split(":");
			return (parseInt(val[0]) > 19) ? "HZ:M0" : "H0:M0";
		}
		var pattern = {
			onKeyPress: function (val, e, field, options) {
				field.mask(mask.apply({}, arguments), options);
			},
			placeholder: '00:00',
			translation: {
				'H': { pattern: /[0-2]/, optional: false },
				'Z': { pattern: /[0-3]/, optional: false },
				'M': { pattern: /[0-5]/, optional: false }
			},
		};
		$(id).unmask().mask(mask, pattern);

		if (!UIEmpty(optionsDefault.value)) {
			$(id).unmask().val(optionsDefault.value).mask(mask, pattern);
			//$(id).masked(optionsDefault.value);
			//$(id).val(optionsDefault.value);
		}
		else {
			$(id).unmask().mask(mask, pattern);
		}
		if (optionsDefault.focus) {
			$(id).focus(function () {
				var $this = $(this);
				$this.select();
				// Work around Chrome's little problem
				$this.mouseup(function () {
					// Prevent further mouseup intervention
					$this.unbind("mouseup");
					return false;
				});
			});
		}
		UIHH_mm.Change(id);
	}
};

var UILoadingOverlay = {
	Load: async function (id, pBefore, pAfter) {
		let _loading = $('div.loadingoverlay');
		if (_loading.length == 0) {
			await UILoadingDefault("show");
		}
		if (!UIEmpty(pBefore)) {
			pBefore();
		}
		var container = $(id);
		container.removeClass('d-none');
		$("html, body").animate({
			scrollTop: container.offset().top
		}, 600, "linear", function () {
			if (!UIEmpty(pAfter)) {
				pAfter();
			}
			UILoadingDefault("hide");
		});
	}
};

var UILoadingDefaultConfigure = async function (options) {
	var optionsConfigure = options
	if (!UIEmpty($.LoadingOverlaySetup)) {
		if (UIEmpty(optionsConfigure)) {
			optionsConfigure = {
				text: "Por favor espere...",
				textClass: "text-white us-n",
				background: "rgba(0, 0, 0, 0.5)",
				image: "",
				fontawesome: "fal fa-sync fa-spin text-warning",
				fontawesomeAnimation: "0.75s fadein",
				fontawesomeColor: "#ffcc00",
			};
		}
		$.LoadingOverlaySetup(optionsConfigure);
	}
}

var UILoadingDefault = async function (action) {
	//await UISleep(500);
	if (document.body.classList.contains('setbody')) {
		if (UIEmpty(action)) {
			let _loading = $('div.loadingoverlay');
			if (_loading.length > 0) {
				action = "hide";
			}
			else {
				action = "show";
			}
		}
		//$.LoadingOverlay(action);
		await $.LoadingOverlay(action);
		await UISleep(200);
	}
}

var UIBootbox = async function (options) {
	var optionsDefault = {
		message: '<p class="text-center mb-0 f-16"><i class="fa fa-spin fa-cog"></i> Procesando su solicitud, por favor espere...</p>',
		centerVertical: true,
		closeButton: false
	};
	$.extend(true, optionsDefault, options);
	var dialog = await bootbox.dialog(optionsDefault);
	return dialog;
}

var UIMonth = function (now) {
	now = now || new Date();
	now.setMonth(now.getMonth() + 1);
	return this.NumberToFormat(now.getMonth());
};

var UIYear = function (now) {
	now = now || new Date();
	return now.getFullYear();
};

var UIRegex = {
	isHHmm: function (timeString) {
		var result = false;
		try {
			/*
			 * 5:30
			 * 05:03
			 */
			var rxHHmm = /^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/;
			result = rxHHmm.test(timeString);
		} catch (e) {

		}
		return result;
	},
	isHHmmss: function (timeString) {
		var result = false;
		try {
			/*
			 * 5:30
			 * 05:03
			 * 5:30:00
			 * 05:30:15
			 */
			var rxHHmm = /^([0-1]?[0-9]|[2][0-3]):([0-5][0-9])(:[0-5][0-9])?$/;
			result = rxHHmm.test(timeString);
		} catch (e) {

		}
		return result;
	},
}

var UIValue = function (value, defaultValue) {
	var result = value;
	if (defaultValue === undefined) {
		defaultValue = "";
	}

	if (value === null || value === undefined || value === "") {
		result = defaultValue;
	}

	return result;
};

var UIValidate = {
	IsBase64: function (str) {
		var bool_64 = str && str.length % 4 == 0 && /^[A-Za-z0-9+/]+[=]{0,3}$/.test(str) && !/^([0-9])*$/.test(str) && !/^[A-Z]+$/i.test(str);
		var string_result = "";
		if (bool_64) {
			let signatures = {
				JVBERi0: "application/pdf",
				R0lGODdh: "image/gif",
				R0lGODlh: "image/gif",
				iVBORw0KGgo: "image/png",
				"/": "image/jpeg",
				"Q": "image/jpeg",
				U: "image/webp",
			};
			for (var s in signatures) {
				if (str.indexOf(s) === 0) {
					string_result = signatures[s];
					break;
				}
			}
		}
		return string_result;
	}
};

var UIJson = {
	FromURL: function (href) {
		var vars = {};
		var parts = href.replace(/[?&]+([^=&]+)=([^&]*)/gi,
			function (m, key, value) {
				vars[key] = value;
			});
		return vars;
		//let url = new URL('http://www.test.com/t.html?a=1&b=3&c=m2-m3-m4-m5');
		//let searchParams = new URLSearchParams(url.search);
		//console.log(searchParams.get('c'));  // outputs "m2-m3-m4-m5"
	}
};

var UISSRS = {
	Load: async function (ReportName, Params, Options) {
		if (UIEmpty(NombreDelControlador)) {
			console.log('No se ha declarado la variable NombreDelControlador');
			return;
		}
		var optionsDefault = {
			"rc:Parameters": false,
			"rs:Command": "Render",
			"rs:Embed": true,
		};
		$.extend(true, optionsDefault, Options);
		let _href = `${Entity.SSRS.URLService}/Pages/ReportViewer.aspx?/RR.HH/${NombreDelControlador}/${ReportName}&${$.param(Params)}&${$.param(optionsDefault)}`;
		$('#iframeMFRSSRS').attr('src', _href);
		$('#modalFullRpteSSRS').modal('show');
	},
	LoadExternal: async function (ReportName, Params, Options) {
		if (UIEmpty(NombreDelControlador)) {
			console.log('No se ha declarado la variable NombreDelControlador');
			return;
		}
		var optionsDefault = {
			"rc:Parameters": false,
			"rs:Command": "Render",
			"rs:Embed": true,
		};
		$.extend(true, optionsDefault, Options);
		let link = document.createElement('a');
		link.href = `${Entity.SSRS.URLService}/Pages/ReportViewer.aspx?/RR.HH/${NombreDelControlador}/${ReportName}&${$.param(Params)}&${$.param(optionsDefault)}"`;
		link.target = "_blank";
		document.body.appendChild(link);
		link.click();
		link.remove();
	},
	Modal: async function (Options) {
		var optionsDefault = {
			Titulo: 'Reporte',
			CustomPrev: null,
			Custom: null,
			CustomNext: null,
			Proyecto: {
				Params: {}
			},
			Planilla: {
				MultiSelect: false,
				Operador: null,
				Filtro: [],
				Accion: null,
			},
			Estado: {
				Show: false,
				Source: [
					{ codigo: '%', nombre: 'Todos' },
					{ codigo: 'A', nombre: 'Activos' },
					{ codigo: 'C', nombre: 'Cesados' },
				]
			},
			Fecha1: {
				Show: false,
				Texto: 'Fecha1',
				Value: null,
			},
			Fecha2: {
				Show: false,
				Texto: 'Fecha2',
				Value: null,
			},
			Periodo1: {
				Show: false,
				Texto: 'Periodo1',
			},
			Periodo2: {
				Show: false,
				Texto: 'Periodo2',
			},
			Formato: {
				Show: true,
				Source: [
					{ codigo: 'F1', nombre: 'Formato 1' },
				]
			},
			Banco: {
				Show: false,
				Source: null,
				Class: null,
				Method: null,
				Params: {},
				Callback: null,
			},
			Moneda: {
				Show: false,
				Source: null,
				Class: null,
				Method: null,
				Params: {},
				Callback: null,
			},
			Ano: {
				Show: false,
				Value: Entity.UI.Ano,
			},
			Mes: {
				Show: false,
				Class: 'ajax/core',
				Method: 'GetMeses',
				Params: {}
			},
			Colaborador: {
				Show: false,
				AutoSelectAll: false,
				Colums: [
					{ data: "cod_trabajador", title: "Código" },
					{ data: "trabajador", title: "Trabajador" },
					{ data: "estado_texto", title: "Estado" },
					{ data: "fecha_ingreso", title: "F. Ingreso" },
					{ data: "fecha_cese", title: "F. Cese" },
				],
			},
			CodigoDeProceso: 'P',
			CodigoClaseDeProceso: 'B',
			Callback: null
		};
		$.extend(true, optionsDefault, Options);

		//set variable FORCED
		if (!UIEmpty(Options.Estado) && !UIEmpty(Options.Estado.Source) && Options.Estado.Source.length > 0) {
			optionsDefault.Estado.Source = Options.Estado.Source;
		}

		UILoadingDefault("show");
		$('#h4TitleMSSRS').html(optionsDefault.Titulo);
		//UIDataTable.setEmpty('#tableColaboradorMSSRS');

		$('#colProyectoMSSRS, #colCCMSSRS, #colUNMSSRS, #colAreaMSSRS, #colLocalidadMSSRS, #colPlanillaMSSRS').removeClass('d-none');

		$('#selectProyectoMSSRS, #selectCCMSSRS, #selectUNMSSRS').empty();
		$('#selectAreaMSSRS, #selectLocalidadMSSRS, #selectPlanillaMSSRS').empty();
		$('#selectFormatoMSSRS').empty();
		$('#selectBancoMSSRS, #selectMonedaMSSRS').empty();

		$('#colEstadoMSSRS, #colFecha1MSSRS, #colFecha2MSSRS').addClass('d-none');
		$('#colPeriodo1MSSRS, #colPeriodo2MSSRS').addClass('d-none');
		$('#colBancoMSSRS, #colMonedaMSSRS, #colMesMSSRS').addClass('d-none');
		$('#colAnoMSSRS').addClass('d-none');
		$('#rowColaboradorMSSRS').addClass('d-none');
		let jsonParam = {
			CodigoDePerfil: 'AD',
			CodigoDeProyecto: '-1',
		};
		await UIFunctions.setSelect('ajax/ssrs', 'GetProyectos', jsonParam, false, '#selectProyectoMSSRS');
		await UIFunctions.setSelect('ajax/ssrs', 'GetCentrosCosto', jsonParam, false, '#selectCCMSSRS');
		await UIFunctions.setSelect('ajax/ssrs', 'GetUnidadesNegocio', jsonParam, false, '#selectUNMSSRS');
		await UIFunctions.setSelect('ajax/ssrs', 'GetAreas', jsonParam, false, '#selectAreaMSSRS');
		await UIFunctions.setSelect('ajax/ssrs', 'GetLocalidades', jsonParam, false, '#selectLocalidadMSSRS');
		await UIFunctions.setSelect('ajax/ssrs', 'GetTipoPlanillas', jsonParam, false, '#selectPlanillaMSSRS');
		await UIFunctions.setSelect2(optionsDefault.Formato.Source, '#selectFormatoMSSRS');
		await UIFunctions.setSelect2(optionsDefault.Estado.Source, '#selectEstadoMSSRS');

		var countColaborador = 0;
		var tableColaboradorMSSRS;
		var fnOperador = function (key) {
			if (!UIEmpty(optionsDefault[key].Operador)) {
				switch (optionsDefault[key].Operador) {
					case "=": case "!=":
						if (["E", "C"].indexOf(optionsDefault[key].Accion) > -1) {
							let fOptionsIN = _.filter(selPlanilla.find('option'), (ths) => optionsDefault[key].Filtro.indexOf($.trim(ths.value)) > -1);
							let fOptionsNOTIN = _.filter(selPlanilla.find('option'), (ths) => optionsDefault[key].Filtro.indexOf($.trim(ths.value)) == -1);
							if (optionsDefault[key].Operador == "=") {
								if (optionsDefault[key].Accion == "E") {
									$(fOptionsIN).remove();
								}
								else {
									$(fOptionsNOTIN).remove();
								}
							}
							else {
								if (optionsDefault[key].Accion == "E") {
									$(fOptionsNOTIN).remove();
								}
								else {
									$(fOptionsIN).remove();
								}
							}
						}
						else {
							console.warn(`Error para la acción: ${optionsDefault[key].Accion} Valor esperado : [E (Eliminar), C (Conservar)]`);
						}
						break;
					default:
						console.warn(`Error para el operador: ${optionsDefault[key].Operador} Valor esperado: [=, !=]`);
						break;
				}
			}
		};
		var fnColaborador = async function () {
			let jsonParam = {
				CodigoDeProyecto: $('#selectProyectoMSSRS').val(),
				CodigoDeUN: $('#selectUNMSSRS').val(),
				CodigoDeCC: $('#selectCCMSSRS').val(),
				CodigoDeArea: $('#selectAreaMSSRS').val(),
				CodigoDeLocalidad: $('#selectLocalidadMSSRS').val(),
				CodigoDeTipoPlanilla: $('#selectPlanillaMSSRS').val(),
				Estado: $('#selectEstadoMSSRS').val(),
				FechaDeInicio: $('#txtFecha1MSSRS').val(),
				FechaDeFin: $('#txtFecha2MSSRS').val(),
				Formato: $('#selectFormatoMSSRS').val(),
			};
			await UIAjax.invokeMethodAsync('ajax/ssrs', 'GetColaboradores', jsonParam).then(function (r) {
				tableColaboradorMSSRS = $('#tableColaboradorMSSRS').DataTable({
					data: r.data,
					initComplete: function (settings, json) {
						UIDataTable.Buttons.Todos(this);
						var api = this.api();
						var wrapper = $(api.table().container());
						$(tableColaboradorMSSRS.table().header()).addClass('thead-sky thead-sticky');
						if (optionsDefault.Colaborador.AutoSelectAll) {
							$('button.btnTodosDataTable', wrapper).click();
						}
						countColaborador = r.data.length;
					},
					scrollCollapse: true,
					scrollY: "300px",
					paging: false,
					columns: optionsDefault.Colaborador.Colums,
					drawCallback: function (settings) {
						var api = this.api();
					}
				});
			});
		};
		var selPlanilla = $('#selectPlanillaMSSRS');
		selPlanilla.removeAttr('multiple');
		if (optionsDefault.Planilla.MultiSelect) {
			selPlanilla.attr('multiple', 'multiple');
			//selPlanilla.multipleSelect('destroy').multipleSelect({ locale: 'es-ES', filter: true }).multipleSelect('checkAll');
		}
		if (!UIEmpty(optionsDefault.Planilla.Operador)) {
			fnOperador("Planilla");
		}

		$('#lblFecha1MSSRS').html(optionsDefault.Fecha1.Texto);
		$('#lblFecha2MSSRS').html(optionsDefault.Fecha2.Texto);
		$('#lblPeriodo1MSSRS').html(optionsDefault.Periodo1.Texto);
		$('#lblPeriodo2MSSRS').html(optionsDefault.Periodo2.Texto);

		$('#txtFecha1MSSRS, #txtFecha2MSSRS').datepicker("destroy").datepicker({ language: 'es' });
		$('#txtFecha1MSSRS').datepicker('setDate', optionsDefault.Fecha1.Value || UIDateTime.firstDayMonth());
		$('#txtFecha2MSSRS').datepicker('setDate', optionsDefault.Fecha2.Value || UIDateTime.lastDayMonth());
		UIEvent.Disabled.Change('#selectEstadoMSSRS', 'A', "#txtFecha1MSSRS, #txtFecha2MSSRS");

		$('#selectEstadoMSSRS').trigger('change');

		if (optionsDefault.Estado.Show) {
			$('#colEstadoMSSRS').removeClass('d-none');
		}
		if (optionsDefault.Fecha1.Show) {
			$('#colFecha1MSSRS').removeClass('d-none');
		}
		if (optionsDefault.Fecha2.Show) {
			$('#colFecha2MSSRS').removeClass('d-none');
		}
		if (optionsDefault.Banco.Show) {
			if (!UIEmpty(optionsDefault.Banco.Source)) {
				await UIFunctions.setSelect2(optionsDefault.Banco.Source, '#selectBancoMSSRS');
				if (!UIFnEmpty(optionsDefault.Banco.Callback)) {
					await optionsDefault.Banco.Callback();
				}
			}
			else {
				if (UIEmpty(optionsDefault.Banco.Class)) {
					console.warn(`El valor de [optionsDefault.Banco.Class] no puede ser vacío.`);
				}
				else if (UIEmpty(optionsDefault.Banco.Method)) {
					console.warn(`El valor de [optionsDefault.Banco.Method] no puede ser vacío.`);
				}
				else {
					await UIFunctions.setSelect(optionsDefault.Banco.Class, optionsDefault.Banco.Method, optionsDefault.Banco.Params, false, '#selectBancoMSSRS');
					if (!UIFnEmpty(optionsDefault.Banco.Callback)) {
						await optionsDefault.Banco.Callback();
					}
				}
			}
			$('#colBancoMSSRS').removeClass('d-none');
		}
		if (optionsDefault.Moneda.Show) {
			if (!UIEmpty(optionsDefault.Moneda.Source)) {
				await UIFunctions.setSelect2(optionsDefault.Moneda.Source, '#selectMonedaMSSRS');
				if (!UIFnEmpty(optionsDefault.Moneda.Callback)) {
					await optionsDefault.Moneda.Callback();
				}
			}
			else {
				if (UIEmpty(optionsDefault.Moneda.Class)) {
					console.warn(`El valor de [optionsDefault.Moneda.Class] no puede ser vacío.`);
				}
				else if (UIEmpty(optionsDefault.Moneda.Method)) {
					console.warn(`El valor de [optionsDefault.Moneda.Method] no puede ser vacío.`);
				}
				else {
					await UIFunctions.setSelect(optionsDefault.Moneda.Class, optionsDefault.Moneda.Method, optionsDefault.Moneda.Params, false, '#selectMonedaMSSRS');
					if (!UIFnEmpty(optionsDefault.Moneda.Callback)) {
						await optionsDefault.Moneda.Callback();
					}
				}
			}
			$('#colMonedaMSSRS').removeClass('d-none');
		}

		$('#txtAnoMSSRS').val(optionsDefault.Ano.Value);
		if (optionsDefault.Ano.Show) {
			$('#colAnoMSSRS').removeClass('d-none');
		}

		if (optionsDefault.Mes.Show) {
			$('#colMesMSSRS').removeClass('d-none');
			await UIFunctions.setSelect(optionsDefault.Mes.Class, optionsDefault.Mes.Method, optionsDefault.Mes.Params, false, '#selectMesMSSRS');
		}

		$('#divCustomPrevMSSRS').html(optionsDefault.CustomPrev);
		$('#divCustomMSSRS').html(optionsDefault.Custom);
		$('#divCustomNextMSSRS').html(optionsDefault.CustomNext);

		if (optionsDefault.Colaborador.Show) {
			$('#colEstadoMSSRS, #colFecha1MSSRS, #colFecha2MSSRS').removeClass('d-none');
			$("#txtFecha1MSSRS, #txtFecha2MSSRS").removeAttr('disabled');
			$('#rowColaboradorMSSRS').removeClass('d-none');
			$('.selChangeMSSRS').off('change').on('change', fnColaborador);
			$('#selectProyectoMSSRS').trigger('change');
		}
		if (optionsDefault.Periodo1.Show) {
			$('#colPeriodo1MSSRS').removeClass('d-none');
			$('#txtNumeroPeriodo1MSSRS').off('change').on('change', async function () {
				let _jsonNew = { FlgClaseProceso: optionsDefault.CodigoClaseDeProceso };
				await LoadMSSRS.Periodo1(_jsonNew);
				$('#txtNumeroPeriodo2MSSRS').val(this.value);
				await LoadMSSRS.Periodo2(_jsonNew);
			});
			$('#selectPlanillaMSSRS').off('change').on('change', async function () {
				if (optionsDefault.Colaborador.Show) {
					await fnColaborador();
				}
				let _jsonNew = { FlgClaseProceso: optionsDefault.CodigoClaseDeProceso };
				await LoadMSSRS.Periodo1(_jsonNew);
				$('#txtNumeroPeriodo2MSSRS').val($('#txtNumeroPeriodo1MSSRS').val());
				await LoadMSSRS.Periodo2(_jsonNew);
			});
		}
		if (optionsDefault.Periodo2.Show) {
			if (!optionsDefault.Periodo1.Show) {
				console.warn(`Si sólo muestra el Periodo2, traerá errores.`);
			}
			$('#colPeriodo2MSSRS').removeClass('d-none');
			let _jsonNew = { FlgClaseProceso: optionsDefault.CodigoClaseDeProceso };
			$('#txtNumeroPeriodo2MSSRS').off('change').on('change', function () {
				LoadMSSRS.Periodo2(_jsonNew)
			});
		}
		if (optionsDefault.Periodo1.Show) {
			$('#txtNumeroPeriodo1MSSRS').trigger('change');
		}

		if (!UIFnEmpty(optionsDefault.Callback)) {
			await optionsDefault.Callback();
		}

		$('#modalSSRS').modal('show');
		UILoadingDefault("hide");

		return new Promise(async function (resolve, reject) {
			$('#btnAceptarMSSRS').off('click').on('click', function (e) {
				let jsonResponse = LoadMSSRS.Filtro();
				if (!UIEmpty(tableColaboradorMSSRS) && !$('#rowColaboradorMSSRS').hasClass('d-none')) {
					jsonResponse.SelectedRows = Array.from(tableColaboradorMSSRS.rows('.selected').data());
					if (jsonResponse.SelectedRows.length == 0) {
						//MsgBox("Debe seleccionar un registro como mínimo.", "warning", true);
						UIMensaje.COLABORADORM();
						return;
					}
					jsonResponse.ACodigoDePersonal = jsonResponse.SelectedRows.map(function (v) { return v.cod_personal; });
					if (jsonResponse.SelectedRows.length == countColaborador) {
						jsonResponse.IsSelectedAllRows = true;
					}
				}
				$('#modalSSRS').modal('hide');
				resolve(jsonResponse);
			});
			$('#btnCancelarMSSRS').off('click').on('click', function (e) {
				$('#modalSSRS').modal('hide');
				resolve(null);
			});

		});
	}
};

var UIBoldReport = {
	Load: async function (Params, Options) {
		if (UIEmpty(Params.NombreDelControlador)) {
			if (UIEmpty(NombreDelControlador)) {
				console.log('No se ha declarado la variable NombreDelControlador');
				return;
			}
			else {
				Params.NombreDelControlador = NombreDelControlador;
			}
		}
		var optionsDefault = {
			height: screen.height,
			width: screen.width,
			resizable: 'yes',
			scrollbars: 'yes',
			toolbar: 'yes',
			menubar: 'yes',
			location: 'yes',
		};
		$.extend(true, optionsDefault, Options);
		Params.HideSuccess = true;
		UIAjax.runFunctionAsync('Reportes', 'PostParametrosReporte', Params, function () {
			window.open(portal + "/Reportes", '_reportes', UIParam(optionsDefault, ','));
		});
	},
	TryModal: async function (Params, Options) {
		try {
			//UIBoldReport.Load(Params, Options);
			if (UIEmpty(Params.NombreDelControlador)) {
				if (UIEmpty(NombreDelControlador)) {
					console.log('No se ha declarado la variable NombreDelControlador');
					return;
				}
				else {
					Params.NombreDelControlador = NombreDelControlador;
				}
			}
			var optionsDefault = {
				height: screen.height,
				width: screen.width,
				resizable: 'yes',
				scrollbars: 'yes',
				toolbar: 'yes',
				menubar: 'yes',
				location: 'yes',
			};
			$.extend(true, optionsDefault, Options);
			Params.HideSuccess = true;
			UIAjax.runFunctionAsync('Reportes', 'PostParametrosReporte', Params, function () {
				var sb = new StringBuilder();
				var titulo = Params._title ?? 'Reporte';
				sb.append(`<div id="modalBoldReportFULL" class="modal modal-fullscreen-xl fade" role="dialog" style="z-index: 9999; padding-right: 17px;" aria-modal="false">`);
				sb.append(`	<div class="modal-dialog modal-xl modal-dialog-centered">`);
				sb.append(`		<div class="modal-content">`);
				sb.append(`			<div class="modal-header">`);
				sb.append(`				<div class="container" style="max-width: 100%;">`);
				sb.append(`					<div class="row justify-content-between">`);
				sb.append(`						<div class="col my-auto pl-0">`);
				sb.append(`							<label class="h4 mb-0 text-dark">${titulo}</label>`);
				sb.append(`						</div>`);
				sb.append(`						<div class="col-auto">`);
				sb.append(`							<label data-dismiss="modal" class="m-0 f-24 cursor-pointer"><span aria-hidden="true">×</span></label>`);
				sb.append(`						</div>`);
				sb.append(`					</div>`);
				sb.append(`				</div>`);
				sb.append(`			</div>`);
				sb.append(`			<div class="modal-body">`);
				sb.append(`				<div class="row"><div class="col"><iframe id="iframeMBOREFU" name="theFrame" src="${portal}/Reportes?isembed=true" width='100%' height='${window.innerHeight - 108}px' allowfullscreen frameborder='0'></iframe></div></div>`);
				//sb.append(`				<div class="row"><div class="col"><iframe src="${portal}/Reportes?isembed=true&resizable=yes&scrollbars=yes&toolbar=yes&menubar=yes&location=yes&height=${screen.height}&width=${screen.width}" width='100%' height='${screen.height}px' allowfullscreen frameborder='0'></iframe></div></div>`);
				sb.append(`			</div>`);
				sb.append(`		</div>`);
				sb.append(`	</div>`);
				sb.append(`</div>`);
				document.body.insertAdjacentHTML('afterbegin', sb.toString());
				$('#modalBoldReportFULL').on('hidden.bs.modal', function () {
					this.remove();
				});

				//window.onresize = function () {
				//	var iframe = window.parent.document.getElementById('iframeMBOREFU');
				//	iframe.style.width = window.style.width;
				//	iframe.style.height = window.style.height;
				//}

				//console.log($('#modalBoldReportFULL .modal-body').height());
				//console.log(document.querySelector('#modalBoldReportFULL .modal-body').clientHeight);
				//$('#iframeMBOREFU').attr('height', $('#modalBoldReportFULL .modal-body').height());
				//window.open(portal + "/Reportes", '_reportes', UIParam(optionsDefault, ','));
				//console.log(UIParam(optionsDefault, ','));
				//window.open(portal + "/Reportes", "_reportes", UIParam(optionsDefault, ','));
				$('#modalBoldReportFULL').modal('show');
			});
		} catch (e) {
			UILoadingDefault('hide');
			UIMensaje.ERRORINESPERADO();
		}
	},
};

var MsgBox = function (mensaje, tipo, autoclose, dangerMode, funcTrue, funcFalse) {
	switch (mensaje) {
		case "success":
			exit = true;
			MsgBox("Cambios guardados con éxito.", "success", true);
			break;
		case "warning": case "error":
			exit = true;
			MsgBox("No se pudo completar el envio.", mensaje, true);
			break;
		case "question":
			exit = true;
			MsgBox("Se eliminará el registro seleccionado.", mensaje, false, true, tipo);
			break;
	}
	var title = "";
	switch (tipo) {
		case "success":
			title = "Buen Trabajo!";
			break;
		case "warning":
			title = "Ups!";
			break;
		case "info":
			title = "Información";
			break;
		case "error":
			title = "Sucedio un error!";
			break;
		case "question":
			title = "¿Está seguro?";
			break;
	}
	if (title != "") {
		if (tipo == 'question') {
			var btnColor = "#04A9F5";
			if (dangerMode) {
				btnColor = "#E44343";
			}
			Swal.fire({
				title: title,
				text: mensaje,
				icon: tipo,
				//buttons: { confirm: true, cancel: true },
				showCancelButton: true,
				confirmButtonColor: btnColor,
				//cancelButtonColor: '#d33',
				confirmButtonText: 'Aceptar',
				cancelButtonText: 'Cancelar',
			}).then((result) => {
				if (result.value) {
					if (!UIEmpty(funcTrue)) {
						funcTrue();
					}
				}
				else {
					if (!UIEmpty(funcFalse)) {
						funcFalse();
					}
				}
			});
		}
		else {
			if (autoclose) {
				if (tipo == "success") {
					Swal.fire({ icon: tipo, title: title, text: mensaje, showConfirmButton: false, timer: 1000 });
				}
				else {
					Swal.fire({ icon: tipo, title: title, text: mensaje, showConfirmButton: false, timer: 1350 });
				}
			}
			else {
				Swal.fire({ icon: tipo, title: title, text: mensaje });
			}
		}
	}
};

var MsgToast = function (mensaje, tipo, timer) {

	if (timer == null) {
		timer = 2500;
	}

	switch (mensaje) {
		case "success":
			exit = true;
			MsgToast("Cambios guardados con éxito.", "success", timer);
			break;
		case "warning": case "error":
			exit = true;
			MsgToast("No se pudo completar el envio.", mensaje, timer);
			break;
	}
	var title = "";
	switch (tipo) {
		case "success":
			title = "Buen Trabajo!";
			break;
		case "warning":
			title = "Ups!";
			break;
		case "info":
			title = "Información";
			break;
		case "error":
			title = "Sucedio un error!";
			break;
	}

	var divNotification = $('#divNotification');
	var icon = '<i class="fal fa-check-circle f-58 text-success"></i>';
	switch (tipo) {
		case 'warning': case 'info':
			icon = '<i class="fal fa-exclamation-circle f-58 text-' + tipo + '"></i>';
			break;
		case 'danger':
			icon = '<i class="fal fa-ban f-58 text-danger"></i>';
			break;
	}
	var timeId = new Date().getTime();
	var html = '<div id = "toast-' + timeId + '" class="customToast rounded alert-' + tipo + '" role="alert" aria-live="assertive" data-animation="true" data-delay="' + timer + '" aria-atomic="true" >' +
		'<div class="container-fluid py-2 us-n">' +
		'<div class="row">' +
		'<div class="col-md-4 d-flex justify-content-center">' +
		icon +
		'</div>' +
		'<div class="col-md-8 pl-0 m-auto">' +
		'<div class="row">' +
		'<div class="col-auto">' +
		'<span class="h5">' + title + '</span>' +
		'</div>' +
		'</div>' +
		'<div class="row">' +
		'<div class="col-12">' +
		'<span>' + mensaje + '</span>' +
		'</div>' +
		'</div>' +
		'</div>' +
		'</div>' +
		'</div>' +
		'</div >';
	divNotification.append(html);
	//var containerBody = $('body');
	//containerBody.append(html);
	$('#toast-' + timeId).toast('show');
	setInterval(function () {
		$('#toast-' + timeId).remove();
	}, timer);
	//$('#toast-123').toast('show');
};

var UIMensaje = {
	_get_autoclose: function (valor) {
		var result = false;
		if ($.type(valor) == 'boolean') {
			result = valor || false;
		}
		return result;
	},
	DESARROLLO(autoclose) {
		MsgBox("Esta opción está en desarrollo.", "warning", UIMensaje._get_autoclose(autoclose));
	},
	MANTENIMIENTO(autoclose) {
		MsgBox("Esta opción está en mantenimiento.", "warning", UIMensaje._get_autoclose(autoclose));
	},
	REGISTROM(autoclose) {
		MsgBox("Debe seleccionar un registro como mínimo.", "warning", UIMensaje._get_autoclose(autoclose));
	},
	COLABORADORM(autoclose) {
		MsgBox("Debe seleccionar un colaborar como mínimo.", "warning", UIMensaje._get_autoclose(autoclose));
	},
	REGISTRO(autoclose) {
		MsgBox("Debe seleccionar un registro.", "warning", UIMensaje._get_autoclose(autoclose));
	},
	COLABORADOR(autoclose) {
		MsgBox("Debe seleccionar un colaborador.", "warning", UIMensaje._get_autoclose(autoclose));
	},
	PERIODO(autoclose) {
		MsgBox("Debe seleccionar un periodo.", "warning", UIMensaje._get_autoclose(autoclose));
	},
	EMPRESA(autoclose) {
		MsgBox("Debe seleccionar una empresa.", "warning", UIMensaje._get_autoclose(autoclose));
	},
	CODIGO(autoclose) {
		MsgBox("El código no puede ser vacío.", "warning", UIMensaje._get_autoclose(autoclose));
	},
	NOMBRE(autoclose) {
		MsgBox("El nombre no puede ser vacío.", "warning", UIMensaje._get_autoclose(autoclose));
	},
	DESCRIPCION(autoclose) {
		MsgBox("La descripción no puede ser vacía.", "warning", UIMensaje._get_autoclose(autoclose));
	},
	SINREGISTROFECHA(autoclose) {
		MsgBox("No hay registros a mostrar en la fecha.", "warning", UIMensaje._get_autoclose(autoclose));
	},
	SINREGISTROFECHAS(autoclose) {
		MsgBox("No hay registros a mostrar en el rango de fechas.", "warning", UIMensaje._get_autoclose(autoclose));
	},
	ERROR404() {
		Swal.fire({ imageUrl: '/images/404.png', onOpen: function (ele) { }, });
	},
	ERRORINESPERADO() {
		var sb = new StringBuilder();
		let overflowY = document.body.style.overflowY;
		document.body.style.overflowY = "hidden";
		sb.append(`<div id="divErrorInesperadoDynamic" class="swal2-container swal2-center swal2-backdrop-show" style="overflow-y: auto;background: rgba(0, 0, 0, 0.7); user-select:none;">`);
		sb.append(`	<div aria-labelledby="swal2-title" aria-describedby="swal2-content" class="swal2-popup swal2-modal swal2-icon-info swal2-show" style="display: flex; background: rgb(0, 120, 215);">`);
		sb.append(`		<div class="swal2-header">`);
		sb.append(`			<img class="img-fluid" src="/images/sad.png" style="height: 100px; margin-top: 20px; margin-bottom: 20px;" />`);
		sb.append(`			<h2 class="swal2-title" style="display: flex;color:white;">Error inesperado, por favor si es esto persiste comuníquese con el área de soporte.</h2>`);
		sb.append(`		</div>`);
		sb.append(`		<div class="swal2-actions"><button type="button" class="btn btn-sm btn-outline-light" onclick="document.getElementById('divErrorInesperadoDynamic').remove();document.body.style.overflowY = '${overflowY}';">OK</button></div>`);
		sb.append(`	</div>`);
		sb.append(`</div >`);
		document.body.insertAdjacentHTML('afterend', sb.toString());
	},
	DEVELOPER: async function () {
		return new Promise(function (resolve, reject) {
			try {
				var sb = new StringBuilder();
				let overflowY = document.body.style.overflowY;
				document.body.style.overflowY = "hidden";
				sb.append(`<div id="divQuestionDynamic" class="swal2-container swal2-center swal2-backdrop-show" style="overflow-y: auto;background: rgba(0, 0, 0, 1); user-select:none;z-index:99999;">`);
				sb.append(`	<div aria-labelledby="swal2-title" aria-describedby="swal2-content" class="swal2-popup swal2-modal swal2-icon-info swal2-show" style="display: flex; background: rgb(0, 120, 215);">`);
				sb.append(`		<div class="swal2-header">`);
				sb.append(`			<img class="img-fluid" src="/images/happy2.png" style="height: 100px; margin-top: 20px; margin-bottom: 20px;" />`);
				sb.append(`			<h2 class="swal2-title" style="display: flex;color:white;">Hola Sergio, ingresa tu clave.</h2>`);
				sb.append(`			<input id="txtPasswordDeveloper" class="form-control" type="text" style="text-align: center;font-size: 20px;padding: 0px 5px;-webkit-text-security: disc;" />`);
				sb.append(`		</div>`);
				sb.append(`		<div class="swal2-actions"><button id="btnOkQuestionDynamic" type="button" class="btn btn-sm btn-outline-light">OK</button></div>`);
				sb.append(`	</div>`);
				sb.append(`</div >`);
				document.body.insertAdjacentHTML('afterend', sb.toString());
				document.getElementById('txtPasswordDeveloper').focus();
				let fnTrigger = function () {
					resolve(document.getElementById('txtPasswordDeveloper').value == "elias-654");
					document.getElementById('divQuestionDynamic').remove();
				}
				document.getElementById('txtPasswordDeveloper').onkeypress = function (e) {
					if (e.charCode == 13) {
						fnTrigger();
					}
				}
				document.getElementById('btnOkQuestionDynamic').onclick = fnTrigger;
			} catch (e) {
				$('#divQuestionDynamic').remove();
				reject(e);
			}
		});
	},
};

var MsgBoxCustom = async function (mensaje, tipo, options) {
	var optionsDefailt = {
		icon: tipo,
		text: mensaje,
		showCancelButton: true,
		confirmButtonText: "Aceptar",
		cancelButtonText: "Cancelar",
	};
	$.extend(true, optionsDefailt, options);
	switch (tipo) {
		case "success":
			optionsDefailt.title = "Buen Trabajo!";
			break;
		case "warning":
			optionsDefailt.title = "Ups!";
			break;
		case "info":
			optionsDefailt.title = "Información";
			break;
		case "error":
			optionsDefailt.title = "Sucedio un error!";
			break;
		case "question":
			optionsDefailt.title = "¿Está seguro?";
			break;
	}
	return await Swal.fire(optionsDefailt).then(function (result) { return result; });
};

var UIGeolocation = {
	Init: async function () {
		var jsonResponde = {
			success: false,
			mensaje: null,
			position: null,
		};
		return new Promise((resolve, reject) => {
			if (navigator.geolocation) {
				var fnSuccess = function (position) {
					jsonResponde.success = true;
					jsonResponde.position = position;
					resolve(jsonResponde);
				};
				var fnError = function (error) {
					var mensaje = "";
					switch (error.code) {
						case error.PERMISSION_DENIED:
							mensaje = "El usuario denegó la solicitud de geolocalización.";
							break;
						case error.POSITION_UNAVAILABLE:
							mensaje = "La información de ubicación no está disponible.";
							break;
						case error.TIMEOUT:
							mensaje = "Se agotó el tiempo de espera de la solicitud para obtener la ubicación del usuario.";
							break;
						case error.UNKNOWN_ERROR: default:
							mensaje = "Un error desconocido ocurrió.";
							break;
					}
					jsonResponde.mensaje = mensaje;
					resolve(jsonResponde);
				};
				navigator.geolocation.getCurrentPosition(fnSuccess, fnError);
			} else {
				jsonResponde.mensaje = 'Este navegador no soporta Geolocalización!';
				resolve(resolve);
			}
		});
	}
}

var DESARROLLO = function () {
	MsgBox("Esta opción está en desarrollo.", "warning", true);
};

(function ($) {
	$.extend({
		splitAttrString: function (theStr) {
			var attrs = [];

			var RefString = function (s) {
				this.value = s + '~';
			};
			RefString.prototype.toString = function () {
				return this.value;
			};
			RefString.prototype.charAt = String.prototype.charAt;
			var data = new RefString(theStr);

			var getBlock = function (endChr, restString) {
				var block = '';
				var currChr = '';
				while ((currChr != endChr) && (restString.value !== '')) {
					if (/'|"/.test(currChr)) {
						block = $.trim(block) + getBlock(currChr, restString);
					}
					else if (/\{/.test(currChr)) {
						block = $.trim(block) + getBlock('}', restString);
					}
					else if (/\[/.test(currChr)) {
						block = $.trim(block) + getBlock(']', restString);
					}
					else {
						block += currChr;
					}
					currChr = restString.charAt(0);
					restString.value = restString.value.slice(1);
				}
				return $.trim(block);
			};

			do {
				var attr = getBlock(',', data);
				attrs.push(attr);
			}
			while (data.value !== '');
			return attrs;
		}
	});
})(jQuery);

//(function ($) {
//    $.myfunction = function () {
//        console.log('hola');
//    };
//}(jQuery));

var ThrowEmpty = function ThrowEmpty() {
	var Inputs = $('.not-allow-empty');
	Inputs.each(function () { $(this).val($.trim($(this).val())).change(); });
	var boolExit = false;
	Inputs.each(function (index, value) {
		var ths = $(value);
		if (ths.val() == '' || ths.val() == null || ths.val() == undefined) {
			//console.log(ths);
			//console.log(value);
			ths.attr('class', ths.attr('class') + ' input_empty');
			//console.log(ths.attr('class'));
			boolExit = true;
			ths.addClass('input_empty');
			ths.on('click', function (e) {
				ths.removeClass('input_empty');
			});
			MsgBox("No se puede asignar valores nulos.", "warning", true);
			return false;
		}
	});
	return boolExit;
};

$.fn.serializeObject = function () {
	var o = {};
	var a = this.serializeArray();
	$.each(a, function () {
		if (o[this.name]) {
			if (!o[this.name].push) {
				o[this.name] = [o[this.name]];
			}
			o[this.name].push(this.value || '');
		} else {
			o[this.name] = this.value || '';
		}
	});
	return o;
};

function beginHTML(jsonHTML) {
	var html = '';
	var jsonDefault = {
		type: "",
		id: "",
		class: "",
		selected: "",
		content: [],
		attributes: []
	};
	$.extend(true, jsonDefault, jsonHTML);
	if (jsonDefault.type != "") {
		var vId = '', vClass = '';
		if (jsonDefault.id != "") {
			vId = ' id="' + jsonDefault.id + '" ';
		}
		if (jsonDefault.class != "") {
			vClass = ' class="' + jsonDefault.class + '" ';
		}
		var vData = ' ';
		$.each(jsonDefault.attributes, function (index, value) {
			var child = { key: "", value: "" };
			$.extend(true, child, value);
			vData += child.key + '="' + child.value + '" '
		})
		html = html + '<' + jsonDefault.type + vId + vClass + vData + '>';
		html = html.replace('  ', ' ');
		$.each(jsonDefault.content, function (index, value) {
			var child = { type: "", id: "", class: "", value: "", html: "" };
			$.extend(true, child, value);
			var vSelect = '';
			vId = '';
			vClass = '';
			if (child.id != "") {
				vId = ' id="' + child.id + '" ';
			}
			if (child.class != "") {
				vClass = ' class="' + child.class + '" ';
			}
			if (child.value != "") {
				vSelect = ' value="' + child.value + '" ';
			}
			if (jsonDefault.selected != "" && jsonDefault.selected == child.value) {
				vSelect += 'selected ';
			}
			html = html + '<' + child.type + vId + vClass + vSelect + '>{xxx}</' + child.type + '>'
			html = html.replace('  ', ' ');
			html = html.replace('{xxx}', child.html);
		});

		html = html + '</' + jsonDefault.type + '>';
	}

	return html;
};

$.fn.appendVal = function (TextToAppend) {
	return $(this).val(
		$(this).val() + TextToAppend
	);
};

//function dataURLtoFile(dataurl, filename) {
//    var arr = dataurl.split(',');
//    var mime = arr[0].match(/:(.*?);/)[1];
//    var bstr = atob(arr[1]);
//    var n = bstr.length;
//    var u8arr = new Uint8Array(n);
//    while (n--) {
//        u8arr[n] = bstr.charCodeAt(n);
//    }
//    return new File([u8arr], filename, { type: mime });
//}
//var file = dataURLtoFile(`data:text/plain;base64,${r.fileBytes}`, 'error_envio_boleta.txt');
//console.log(file);

UIAjax.invokeMethodAsync('ajax/core', 'GetUI').then(function (r) {
	//console.log(r);
	Entity.SSRS.URLService = r.ssrs.urlService;
	Entity.SSRS.URLPortal = r.ssrs.urlPortal;
	Entity.UI.CodigoDeEmpresa = r.codigoDeEmpresa;
	Entity.UI.CodigoDePersonal = r.codigoDePersonal;
	Entity.UI.TipoDeUsuario = r.tipoDeUsuario;
	Entity.UI.Usuario = r.usuario;
	Entity.UI.Ano = r.ano;
	Entity.UI.initial_catalog = r.initial_catalog;
	Entity.UI.CodigoDeTipoPlanilla = r.cod_tipo_planilla;
	//Entity.SSRS.ControlAsistencia=`${r.urlService}/Pages/ReportViewer.aspx?/RR.HH/ControlAsistencia(AD)/`
});

var UIRUC = {
	Validar: function (RUC) {
		var result = false;
		function InternalValidation(ruc) {
			//11 dígitos y empieza en 10,15,16,17 o 20
			if (!(ruc >= 1e10 && ruc < 11e9
				|| ruc >= 15e9 && ruc < 18e9
				|| ruc >= 2e10 && ruc < 21e9))
				return false;

			for (var suma = -(ruc % 10 < 2), i = 0; i < 11; i++, ruc = ruc / 10 | 0)
				suma += (ruc % 10) * (i % 7 + (i / 7 | 0) + 1);
			return suma % 11 === 0;
		}
		result = InternalValidation(RUC);
		return result;
	},
	Consultar: async function (RUC) {
		return await UIAjax.invokeMethodAsync('ajax/core', 'GetClienteSUNATByRUC', { Ruc: RUC });
	}
};

var UIFNCustom = {
	Ubigeo: async function (id_departamento, id_provincia, id_distrito) {
		$(id_departamento).on('change', async function (e) {
			$(`${id_provincia}, ${id_distrito}`).empty();
			if (!UIEmpty(this.value) && this.value != '-1') {
				var rProvincia = await UIFunctions.setProvincia(id_provincia, this.value, "codigo");
				if (rProvincia.success && rProvincia.r.length > 0) {
					await UIFunctions.setDistrito(id_distrito, rProvincia.r[0].codigo, "codigo");
				}
			}
		});
		$(id_provincia).on('change', async function (e) {
			if (!UIEmpty(this.value)) {
				await UIFunctions.setDistrito(id_distrito, this.value, "codigo");
			}
		});
	},
	CSVtoJSON: function (stringHead, stringBody, splitColumn) {
		splitColumn = splitColumn ?? '|';
		var headers = stringHead.split(splitColumn);
		var obj = {};
		var currentline = stringBody.split(splitColumn);
		for (var j = 0; j < headers.length; j++) {
			obj[headers[j]] = currentline[j];
		}
		return obj;
	},
};

var UIFNAjax = {
	GetInfoExtra: async function (CodigoDeEntidad, CodigoDePersonal) {
		var jsonResult = {};
		var jsonParam = {
			CodigoDeTipoEntidad: 'PE',
			CodigoDeEntidad: CodigoDeEntidad,
			CodigoDePersonal: CodigoDePersonal
		};
		await UIAjax.invokeMethodAsync('ajax/personal', 'GetLegajoInfoExtra', jsonParam).then(function (r) {
			jsonResult = r.data[0];
		});
		return jsonResult;
	}
}