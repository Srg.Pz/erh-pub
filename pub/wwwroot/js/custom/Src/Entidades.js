﻿

function OnLoadEvents() {

    $('#btnNuevo').on('click', function (e) {
        e.preventDefault();

        accion = 'N';

        LimpiarControles();


        $("#modalEntidad").on("shown.bs.modal", function () {

            if (accion == 'N') {
                $("#txtCodigo").prop("disabled", false);
                $('#txtEntidad').focus();
            }
            else {
                $("#txtCodigo").prop("disabled", true);
                $('#txtEntidad').focus();
            }

        }).modal('show');

    });

    $('#selectDepartamento').on('change', function (e) {
        e.preventDefault();
        GetProvincias('selectProvincia', e.target.value);
    });

    $('#selectProvincia').on('change', function (e) {
        e.preventDefault();
        GetDistritos('selectDistrito', e.target.value);
    });


    $('#selectTipoEntidades').on('change', function (e) {
        e.preventDefault();
        EntidadList(e.target.value);
    });

    $('#dtEntidades').on('click', 'tr', function () {

        $('#dtEntidades tr').removeClass('selected');

        accion = '';
        var ths = $(this);
        ths.addClass('selected');

        $("#modalEntidad").on("shown.bs.modal", function () {

            if (accion == 'N') {
                $("#txtCodigo").prop("disabled", false);
                $('#txtEntidad').focus();
            }
            else {
                $("#txtCodigo").prop("disabled", true);
                $('#txtEntidad').focus();
            }

        }).modal('show');

        var table = $('#dtEntidades').DataTable();
        var data = table.row(this).data();

        var TipoEntidad = $('#selectTipoEntidades').val();
        EntidadLoad(TipoEntidad, data.cod_entidad);

    });

    $('#btnGuardar').on('click', function (e) {
        e.preventDefault();
        EntidadSave();
    });

}

//#region CARGA DE COMBOS

function CargarTipoEntidades() {

    $.ajax({
        type: "GET",
        url: portal + '/TablasSoporte/GetTipoEntidades',
        data: JSON.stringify({}),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function success(data) {

            var obj = data.data;
            dtPeriodos = obj;
            var container = $("#selectTipoEntidades");
            container.empty();


            $.each(obj, function (index, value) {
                var html = "<option value='" + value.cod_tipo_entidad + "'>" + value.nombre + "</option>";
                container.append(html);
            });

            if (obj.length > 0) {
                EntidadList(obj[0].cod_tipo_entidad);
            }


        },
        error: function error(response) { }
    });


}

function CargarVias() {

    $.ajax({
        type: "GET",
        url: portal + '/TablasSoporte/GetEntidadesVias',
        data: JSON.stringify({}),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function success(data) {

            var obj = data.data;
            dtPeriodos = obj;
            var container = $("#selectVia");
            container.empty();


            $.each(obj, function (index, value) {
                var html = "<option value='" + value.cod_via + "'>" + value.nombre + "</option>";
                container.append(html);
            });

        },
        error: function error(response) { }
    });
}

function CargarZona() {

    $.ajax({
        type: "GET",
        url: portal + '/TablasSoporte/GetEntidadesZonaUrbana',
        data: JSON.stringify({}),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function success(data) {

            var obj = data.data;
            dtPeriodos = obj;
            var container = $("#selectZona");
            container.empty();

            $.each(obj, function (index, value) {
                var html = "<option value='" + value.cod_zona + "'>" + value.nombre + "</option>";
                container.append(html);
            });

        },
        error: function error(response) { }
    });
}

//#endregion

function EntidadList(TipoEntidad) {

    dtEntidades = $('#dtEntidades').DataTable({
        "ajax": portal + '/TablasSoporte/GetEntidades?TipoEntidad=' + TipoEntidad
        , "initComplete": function (settings, json) {
            //$(chbTodos).appendTo("#tbTrabajadores_filter label");
        }
        , bDestroy: true
        , "scrollY": "300px"
        , dom: 'Bfrtip'
        , buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5'
        ]
        , "language": {
            "url": '/js/language.js'
        }
        , "columns": [

            { "data": "cod_entidad", "width": "20%" },
            { "data": "nombre", "width": "50%" },
            { "data": "siglas", "width": "20%" },
        ]

    });
}

function EntidadLoad(TipoEntidad, cod_entidad) {

    LimpiarControles();

    var jsonParam = {
        TipoEntidad: TipoEntidad,
        cod_entidad: cod_entidad
    };
    $.ajax({
        type: 'GET',
        url: GetUrl("/TablasSoporte/GetEntidad"),
        dataType: 'json',
        data: jsonParam,
        async: false,
        success: function (data) {

            var item = data.data[0];

            $('#txtEntidad').val(item.nombre);
            $('#txtCodigo').val(item.cod_entidad);
            $('#txtRuc').val(item.ruc);
            $('#chkAgRetencion').prop('checked', item.flg_Agente == 'S');
            $('#txtSiglas').val(item.siglas);
            $('#selectVia').val(item.cod_via);
            $('#txtDireccion').val(item.direccion);
            $('#txtNumero').val(item.numero);
            $('#txtInterior').val(item.interior);
            $('#selectZona').val(item.cod_zona);
            $('#txtZona').val(item.zona);
            $('#txtReferencia').val(item.referencia);

            if (item.cod_ubigeo != null) {
                $('#selectDepartamento').val(item.cod_ubigeo.substring(0, 2));
                $('#selectDepartamento').change();
                $('#selectProvincia').val(item.cod_ubigeo.substring(0, 4));
                $('#selectProvincia').change();
                $('#selectDistrito').val(item.cod_ubigeo.trim());
            }
            
            $('#txtTelefonos').val(item.telefonos);
            $('#txtEmail').val(item.email);
            $('#txtWWW').val(item.www);
            $('#txtNomCheque').val(item.nom_cheque);
            $('#txtNotas1').val(item.notas1);
            $('#txtNotas2').val(item.notas2);
            $('#txtCadena1').val(item.cadena1);
        }
    });
};

function EntidadSave() {

    var msgError = EntidadValidar();

    if (msgError != '') {
        MsgBox(msgError, "warning", false);
        return;
    }

    var jsonData = {
        Accion: accion,

        entidad: $('#txtEntidad').val(),
        cod_entidad: $('#txtCodigo').val(),
        cod_tipo_entidad: $('#selectTipoEntidades').val(),
        ruc: $('#txtRuc').val(),
        ag_retencion: $('#chkAgRetencion').prop('checked') ? 'S' : 'N',
        siglas: $('#txtSiglas').val(),
        cod_via: $('#selectVia').val(),
        interior: $('#txtInterior').val(),
        numero: $('#txtNumero').val(),
        cod_zona: $('#selectZona').val(),
        zona: $('#txtZona').val(),
        referencia: $('#txtReferencia').val(),
        direccion: $('#txtDireccion').val(),
        cod_ubigeo: $('#selectDistrito').val(),
        telefonos: $('#txtTelefonos').val(),
        email: $('#txtEmail').val(),
        www: $('#txtWWW').val(),
        nom_cheque: $('#txtNomCheque').val(),
        notas1: $('#txtNotas1').val(),
        notas2: $('#txtNotas2').val(),
        cadena1: $('#txtCadena1').val(),
    };
    UIAjax.runFunctionAsync('TablasSoporte', 'SetClasificadoresProyecto', jsonData, function () {
        $('#modalEntidad').modal('hide');
        EntidadList($("#selectTipoEntidades").val());
    });
}

function EntidadValidar() {


    var entidad = $('#txtEntidad').val();
    if (entidad == '') {

        $('#txtEntidad').focus();
        return 'Nombre de Entidad vacía';
    }

    var codigo = $('#txtCodigo').val();
    if (codigo == '') {

        $('#txtCodigo').focus();
        return 'Codigo de Entidad vacía';
    }

    var via = parseInt($('#selectVia').val());
    if (isNaN(via) || via <= 0) {

        $('#selectVia').focus();
        return 'Código de via inválido';
    }

    var zona = parseInt($('#selectZona').val());
    if (isNaN(zona) || zona <= 0) {

        $('#selectZona').focus();
        return 'Código de zona inválida';
    }



    return '';
}

function LimpiarControles() {

    $('#txtEntidad').val('');
    $('#txtCodigo').val('');
    $('#txtRuc').val('');
    $('#chkAgRetencion').prop('checked', false);
    $('#txtSiglas').val('');
    $('#selectVia').val('');
    $('#txtDireccion').val('');
    $('#txtNumero').val('');
    $('#txtInterior').val('');
    $('#selectZona').val('');
    $('#txtZona').val('');
    $('#txtReferencia').val('');
    $('#selectDepartamento').val('');
    $('#selectDepartamento').change();
    $('#selectProvincia').val('');
    $('#selectProvincia').change();
    $('#selectDistrito').val('');
    $('#txtTelefonos').val('');
    $('#txtEmail').val('');
    $('#txtWWW').val('');
    $('#txtNomCheque').val('');
    $('#txtNotas1').val('');
    $('#txtNotas2').val('');
    $('#txtCadena1').val('');
}
