﻿var g_proyecto = "",
	g_ccosto = "",
	g_unegocio = "",
	g_area = "",
	g_localidad = "",
	g_tipoplanilla = "",
	g_estadotrab = "",
	g_filtro = "",
	g_codPersonal = "",
	g_nomPersonal = "",
	g_codEntidad = "",

	g_fechaInicio = "",
	g_fechaFin = "",
	g_excInicio = "",
	g_excFin = "",
	g_codTurno = "",

	g_excepcion = "",
	g_anual = "";

var regExpFecha = /^(?:(?:(?:0?[1-9]|1\d|2[0-8])[/](?:0?[1-9]|1[0-2])|(?:29|30)[/](?:0?[13-9]|1[0-2])|31[/](?:0?[13578]|1[02]))[/](?:0{2,3}[1-9]|0{1,2}[1-9]\d|0?[1-9]\d{2}|[1-9]\d{3})|29[/]0?2[/](?:\d{1,2}(?:0[48]|[2468][048]|[13579][26])|(?:0?[48]|[13579][26]|[2468][048])00))$/;
var regExpHora = /^(?:[01][0-9]|2[0-3])[-:h][0-5][0-9]$/;

function GetFiltrosCompensacion() {
	g_proyecto = $('#selectProyecto').val();
	g_ccosto = $('#selectCentrosCosto').val();
	g_unegocio = $('#selectUnidadesNegocio').val();
	g_area = $('#selectArea').val();
	g_localidad = $('#selectLocalidad').val();
	g_tipoplanilla = $('#selectTipoPlanilla').val();
	g_estadotrab = $('#selectEstado').val();
	g_filtro = $('#textFiltro').val();
}

function GetColaboradoresCompensacion() {
	GetFiltrosCompensacion();
	var jsonParam = {
		tipoPlanilla: g_tipoplanilla,
		proyecto: g_proyecto,
		centroCosto: g_ccosto,
		localidad: g_localidad,
		unidadNegocio: g_unegocio,
		area: g_area,
		estado: g_estadotrab,
		trabajador: g_filtro
	};
	$.getJSON(GetUrl("/ControlAsistencia/GetListPersonalCompensaciones"), jsonParam, function (data) {
		var container = $("#tbodyColaboradores");
		container.empty();
		$.each(data, function (index, row) {
			_estado = (row.estado === 'C') ? '<a href="#!" class="label theme-bg2 text-white f-12">' + row.desc_estado + '</a>' : '<a href="#!" class="label theme-bg text-white f-12">' + row.desc_estado + '</a>';
			var html = "<tr onclick=\"GetDetallePersonal('" + row.entidad + "','" + row.personal + "','" + row.trabajador + "');\" class=\"unread\">"
				+ "<td class=\"text-center\"><div class='custom-control custom-checkbox'>"
				+ "<input type='checkbox' class='custom-control-input' data-personal='" + row.personal + "' id='chk" + row.personal + "'>"
				+ "<label class='custom-control-label' for='chk" + row.personal + "'></label>"
				+ "</div></td>"
				+ "<td class=\"text-center\">" + row.cod_trabajador + "</td>"
				+ "<td>"
				+ "    <h6 class=\"mb-1\">" + row.trabajador + "</h6>"
				+ "</td>"
				+ "<td>"
				+ "    " + _estado
				+ "</td>"
				+ "</tr>";
			container.append(html);
		});
		$('#tbodyColaboradores .custom-checkbox').on('click', function (e) {
			e.stopPropagation();
		});
	});
}

function GetBolsaHorasPersonal() {
	var jsonParam = {
		personal: g_codPersonal
	};

	$.getJSON(GetUrl("ControlAsistencia/GetBolsaHorasPersonal"), jsonParam, function (data) {
		var container = $("#tbodyBolsaHorasPersonal");
		container.empty();
		var sum_saldo = 0;
		$.each(data, function (index, row) {
			sum_saldo += row.saldo_comp;
			var html = "<tr onclick=\"GetDetalleBolsaPersonal(this);\" data-json='" + JSON.stringify(row) + "'>"
				+ "<td class=\"text-center\">" + row.motivo + "</td>"
				+ "<td class=\"text-center\">" + row.nombre + "</td>"
				+ "<td class=\"text-center\">" + row.inicio + "</td>"
				+ "<td class=\"text-center\">" + row.saldo_comp + "</td>"
				+ "<td class=\"text-center\">" + row.compensado + "</td>"
				+ "</tr>";
			container.append(html);
		});
		$('#sumSaldo').text(UIFloat(sum_saldo));
		$("#tbodyDiasDisponibles").empty();
		$("#tbodyDiasProcesados").empty();
	});
}

function GetDiasDisponibles_BolsaHorasPersonal() {
	GetFiltrosCompensacion();
	jsonParam = {
		personal: g_codPersonal,
		flg: $('input[name=filtro]:checked').val()
	};

	$.getJSON(GetUrl("ControlAsistencia/GetDiasDisponiblesPersonal"), jsonParam, function (data) {
		var container = $("#tbodyDiasDisponibles");
		container.empty();
		$.each(data, function (index, row) {
			var html = "<tr data-json='" + JSON.stringify(row) + "'>"
				+ "<td class=\"text-center\">" + row.fecha + "</td>"
				+ "<td class=\"text-center\">" + row.tardanzas + "</td>"
				+ "<td class=\"text-center\">" + row.faltantes + "</td>"
				+ "<td class=\"text-center\">" + row.total + "</td>"
				+ "<td class=\"text-center\">" + row.porAsignar + "</td>"
				+ "</tr>";
			container.append(html);
		});
		$('#countDiasDisponibles').text($('#tbodyDiasDisponibles tr').length);
		$('#tbodyDiasDisponibles tr').on('click', function (e) {
			e.preventDefault();
			$('#tbodyDiasDisponibles tr').removeClass('selected');
			$(this).addClass('selected');
		});
	});
}

function GetDiasProcesados_BolsaHorasPersonal(pCodAnual, pNumero) {
	GetFiltrosCompensacion();
	jsonParam = {
		personal: g_codPersonal,
		anual: pCodAnual,
		excepcion: '',
		nroExcepcion: pNumero
	};

	$.getJSON(GetUrl("ControlAsistencia/GetDiasProcesadosPersonal"), jsonParam, function (data) {
		var container = $("#tbodyDiasProcesados");
		container.empty();
		$.each(data, function (index, row) {
			var html = "<tr data-json='" + JSON.stringify(row) + "'>"
				+ "<td class=\"text-center\">" + row.fecha + "</td>"
				+ "<td class=\"text-center\">" + row.descripcion + "</td>"
				+ "<td class=\"text-center\">" + row.num_Horas_Cargadas + "</td>"
				+ "<td class=\"text-center\">" + row.horas_Pendientes + "</td>"
				+ "</tr>";
			container.append(html);
		});
		$('#countDiasProcesados').text($('#tbodyDiasProcesados tr').length);
		$('#tbodyDiasProcesados tr').on('click', function (e) {
			e.preventDefault();
			$('#tbodyDiasProcesados tr').removeClass('selected');
			$(this).addClass('selected');
		});
	});
}

function GetDetallePersonal(pCodEntidad, pCodPersonal, pTrabajador) {
	g_codPersonal = pCodPersonal;
	g_codEntidad = pCodEntidad;
	g_nomPersonal = pTrabajador;
	$('#nombreTrabajador').text(g_nomPersonal);
	$('#countDiasDisponibles, #countDiasProcesados').text('-');
	$('#fechaBolsa').text('--/--/----');
	GetBolsaHorasPersonal();
	$('#modalDetalle').modal('show');
};

function GetDetalleBolsaPersonal(pThis) {
	$('#' + ($(pThis).parent())[0].id + ' tr').removeClass('selected');
	$(pThis).addClass('selected');
	var row = $(pThis).data('json');
	$('input[name=filtro]').prop('checked', false);

	if (row.saldo_comp > 0) $('#rbAI').prop('checked', true);
	else if (row.saldo_comp < 0) $('#rbHT').prop('checked', true);
	else $('#rbT').prop('checked', true);

	$('#fechaBolsa').text(row.inicio);

	GetDiasDisponibles_BolsaHorasPersonal();
	GetDiasProcesados_BolsaHorasPersonal(row.cod_Anual, row.numero);
}

function CargarDia(codAnual, codExcepcion, codPersonal, numExcepcion, fechaAsis, numHorasSaldoRegu, numHorasDiaDispo, tipoExcep) {
	var jsonParam = {
		personal: codPersonal,
		anual: codAnual,
		excepcion: codExcepcion,
		nroExcepcion: numExcepcion,
		fechaAsistencia: fechaAsis,
		horasRegularizar: numHorasSaldoRegu,
		horasDiaDisponible: numHorasDiaDispo,
		tipoExcepcion: tipoExcep
	};
	$.post(GetUrl("ControlAsistencia/PostCargarDiaPersonal"), jsonParam).done(function (data) {
		if (data.respuesta != 1) {
			MsgBox(data.mensaje, "warning", false);
			return;
		}
		
		GetBolsaHorasPersonal();
		GetDiasDisponibles_BolsaHorasPersonal();
		GetDiasProcesados_BolsaHorasPersonal(codAnual, numExcepcion);

	}).fail(function (data) {
		MsgBox("error");
	});
};

function RegresarDia(codAnual, codExcepcion, codPersonal, numExcepcion, fechaAsis, fechaActualiza) {
	var jsonParam = {
		personal: codPersonal,
		anual: codAnual,
		excepcion: codExcepcion,
		nroExcepcion: numExcepcion,
		fechaAsistencia: fechaAsis,
		fechaActualizacion: fechaActualiza
	};
	$.post(GetUrl("ControlAsistencia/PostRegresarDiaPersonal"), jsonParam).done(function (data) {
		if (data.respuesta != 1) {
			MsgBox(data.mensaje, "warning", false);
			return;
		}
		GetBolsaHorasPersonal();
		GetDiasDisponibles_BolsaHorasPersonal();
		GetDiasProcesados_BolsaHorasPersonal(codAnual, numExcepcion);
	}).fail(function (data) {
		MsgBox("error");
	});
};

function SaldoCero() {
	var rows = $("#tbodyBolsaHorasPersonal tr");
	for (i = 0; i < rows.length; i++) {
		var databolsa = $(rows[i]).data('json');
		if (parseInt(databolsa.saldo_comp) < 0) {
			var codAnual = databolsa.cod_Anual;
			var numExcepcion = databolsa.numero;
			var CodExcepcion = databolsa.cod_excepcion;
			var FechaAsis = ""; //databolsa.Inicio;
			var NumHorasSaldoRegu = databolsa.saldo_comp;
			var NumHorasDiaDispo = databolsa.saldo_comp;
			var tipo = $("input[name=filtro]:checked").val();
			var TipoExcep = "01";
			CargarDia(codAnual, CodExcepcion, g_codPersonal, numExcepcion, FechaAsis, NumHorasSaldoRegu, NumHorasDiaDispo, TipoExcep);
		}
	}
}

function procesarProgramacionHoras() {
	var _proceso = $('input[name=filtro]:checked').val();
	var _motivo = $('#textMotivoExcepcion').val().trim();
	var _excepcion;
	if (_proceso == "C") _excepcion = "35";
	else _excepcion = $('#selectExcepcion').val();

	var array = [];
	var k = 0;
	$('#tbodyColaboradoresHorasExtras tr input[type=checkbox]:checked').each(function (e) {
		var row = $(this).parent().parent().parent();
		var tiempo = $(row).find('input[type=text]').val();
		tiempo = new Date('01/01/1970 ' + tiempo);
		var generar = formatTime(tiempo);
		var fecha = $(row).find('.fecha').text();
		var nombre = $(row).find('.nombre').text();
		if (generar == '00:00') {
			MsgBox("El # de horas debe ser mayor a 0 | " + nombre + " | " + fecha, "warning", false);
			return;
		}
		array.push({});
		array[k].fila = k + 1;
		array[k].codigoPersonal = $(row).data('personal');
		array[k].fecha = $(row).find('.fecha').text();
		array[k].generar = generar;
		array[k].existe = '';
		array[k].numero = '';
		array[k].numero_hhee = $(row).find('.numero_hhee').text();
		array[k].numero_hhcc = $(row).find('.numero_hhcc').text();
		k++;
	});

	var params = {
		excepcion: _excepcion,
		motivo: _motivo,
		proceso: _proceso,
		lista: array,
	};

	$.post(GetUrl("ControlAsistencia/PostProgramacionCompensaciones"), params).done(function (data) {
		if (data.respuesta < 1) {
			MsgBox(data.mensaje, "warning", false);
			return;
		} else {
			MsgBox("success");
			GetColaboradoresHorasExtras();

		}
	}).fail(function (data) {
		MsgBox("error");
	});

}

function GetValidaPeriodoAbierto(pFecha) {
	jsonParam = {
		personal: g_codPersonal,
		fecha: pFecha
	};
	var cantidad = 0;
	var dataAjax = __Ajax('GET', GetUrl("ControlAsistencia/GetValidaPeriodoAbierto"), jsonParam, false);
	if (dataAjax.length > 0) {
		cantidad = dataAjax[0].count;
	}
	return cantidad;
}

//HORAS EXTRAS
function checkTime(i) {
	return (i < 10) ? "0" + i : i;
}

function formatDate(date) {
	var d = new Date(date),
		month = '' + (d.getMonth() + 1),
		day = '' + d.getDate(),
		year = d.getFullYear();

	if (month.length < 2) month = '0' + month;
	if (day.length < 2) day = '0' + day;

	return [day, month, year].join('/');
}

function formatTime(date) {
	var h = checkTime(date.getHours());
	var m = checkTime(date.getMinutes());
	return h + ":" + m;
}

function getTimeDifference(date1, date2, tipoRpta) {
	var rpta = "00:00";

	var time1 = formatTime(date1);
	var time2 = formatTime(date2);
	if (time1 == time2) {
		if (tipoRpta == "S")
			rpta = "00:00";
		else {
			var today = new Date();
			rpta = new Date(today.getFullYear(), today.getMonth(), today.getDate(), 0, 0);
		}
		return rpta;
	}
	var seconds;
	if (time1 > time2) {
		seconds = 24 * 60 * 60 - (date1.getHours() * 60 * 60 + date1.getMinutes() * 60);
		seconds += date2.getHours() * 60 * 60;
		seconds += date2.getMinutes() * 60;
	}
	else {
		seconds = date2.getHours() * 60 * 60 + date2.getMinutes() * 60;
		seconds -= date1.getHours() * 60 * 60;
		seconds -= date1.getMinutes() * 60;
	}
	var hour = Math.floor(seconds / (60 * 60));
	var minutes = Math.floor((seconds - hour * 60 * 60) / 60);
	if (tipoRpta == "S")
		rpta = checkTime(hour) + ":" + checkTime(minutes);
	else {
		var today = new Date();
		rpta = new Date(today.getFullYear(), today.getMonth(), today.getDate(), hour, minutes);
	}

	return rpta;
}

function validaHora(hora) {
	if (hora == '') return;

	if (hora.length > 5) {
		MsgBox("El formato correcto es 00:00", "warning", false);
		return -1;
	}
	if (hora.length != 5) {
		MsgBox("El formato correcto es 00:00", "warning", false);
		return -1;
	}
	a = hora.charAt(0);
	b = hora.charAt(1);
	c = hora.charAt(2);
	d = hora.charAt(3);
	if (((a == 2 && b > 3) || a > 2) || d > 5 || c != ':') {
		MsgBox("Valor ingresado incorrecto.", "warning", false);
		return -1;
	}
	return 1;
}

function GetFiltrosHorasExtras() {
	g_proyecto = $('#selectProyecto').val();
	g_ccosto = $('#selectCentrosCosto').val();
	g_unegocio = $('#selectUnidadesNegocio').val();
	g_area = $('#selectArea').val();
	g_localidad = $('#selectLocalidad').val();
	g_tipoplanilla = $('#selectTipoPlanilla').val();
	g_fechaInicio = $('#textFechaInicio').val();
	g_fechaFin = $('#textFechaFin').val();
	g_excInicio = $('#textExcesoInicio').val();
	g_excFin = $('#textExcesoFin').val();
	g_codTurno = $('#selectHorario').val();
	g_filtro = ($('#textFiltro').val() == '') ? '%' : $('#textFiltro').val();
}

async function GetColaboradoresHorasExtras() {
	GetFiltrosHorasExtras();
	var jsonParam = {
		tipoPlanilla: g_tipoplanilla,
		proyecto: g_proyecto,
		centroCosto: g_ccosto,
		localidad: g_localidad,
		unidadNegocio: g_unegocio,
		area: g_area,
		fecInicio: g_fechaInicio,
		fecFin: g_fechaFin,
		excInicio: g_excInicio,
		excFin: g_excFin,
		codTurno: g_codTurno,
		personal: g_filtro
	};
	await UILoadingDefault('show');
	await UIAjax.invokeMethodAsync('ControlAsistencia', 'GetListPersonalHorasExtras', jsonParam).then(function (data) {
		console.log(data);
		var container = $("#tbodyColaboradoresHorasExtras");
		container.empty();
		var html = '';
		var i = 0;
		$.each(data, function (index, row) {
			i++;
			
			html = html + "<tr class=\"unread\" data-personal='" + row.cod_personal + "'>"
				+ "<td class=\"text-center\">" + row.cod_trabajador + "</td>"
				+ "<td class=\"text-center\">"
				+ "<label class='nombre'>" + row.nombres + "</label>"
				//+ "<p class='m-0'>" + row.area+"</p>"
				+ "</td>"
				+ "<td class=\"text-center\"><label class='fecha'>" + row.fecha + "</label></td>"
				+ "<td class=\"text-center\">" + row.ingreso + "</td>"
				+ "<td class=\"text-center\">" + row.salida + "</td>"
				+ "<td class=\"text-center\"><label class='sobretiempoIngreso'>" + row.sobretiempoIngreso + "</label></td>"
				+ "<td class=\"text-center\"><label class='sobretiempoSalida'>" + row.sobretiempoSalida + "</label></td>"
				+ "<td class=\"text-center\"><label class='sobretiempo text-success font-weight-bold'>" + row.sobretiempo + "</label></td>"
				+ "<td class=\"text-center\">" + row.prog_hhee + "</td>"
				+ "<td class=\"text-center\">" + row.prog_hhcc + "</td>"
				+ "<td class=\"text-center text-success font-weight-bold\">" + row.programado + "</td>"
				+ "<td class=\"text-center\"><div class='custom-control custom-checkbox'>"
				+ "<input type='checkbox' class='custom-control-input' id='chk" + i + row.cod_trabajador + "'>"
				+ "<label class='custom-control-label' for='chk" + i + row.cod_trabajador + "'></label>"
				+ "</div></td>"
				+ "<td class=\"text-center bg-default\"><input type='text' class='form-control' value='00:00' disabled id='text" + i + row.cod_trabajador + "'></td>"
				+ "<td class=\"text-center bg-success\"><label class='diferencia text-white'>" + row.diferencia + "</label></td>"
				+ "<td class=\"text-center\"><label class='negativo'>" + row.negativo + "</label></td>"
				+ "<td class=\"text-center\">" + row.horario + "</td>"
				+ "<td class=\"d-none text-center\"><label class='numero_hhee'>" + row.numero_hhee + "</label></td>"
				+ "<td class=\"d-none text-center\"><label class='numero_hhcc'>" + row.numero_hhcc + "</label></td>"
				+ "</tr>";
			//container.append(html);
		});

		container.append(html);

		$('#tbodyColaboradoresHorasExtras input[type=text]').mask("00:00", { placeholder: "00:00" });
		$('#tbodyColaboradoresHorasExtras .custom-checkbox').on('change', function (e) {
			var generar = $(this).parent().parent().find('input[type=text]');
			generar.prop('disabled', !e.target.checked);
			e.stopPropagation();
		});
		$('#tbodyColaboradoresHorasExtras input[type=text]').on('change', function (e) {
			var generar = e.target.value;
			if (validaHora(generar) < 0) {
				$(this).val('00:00');
				return;
			}
			var sobretiempo = $(this).parent().parent().find('.sobretiempo').text();
			sobretiempo = new Date('01/01/1970 ' + sobretiempo);
			generar = new Date('01/01/1970 ' + generar);
			//(this).parent().parent().find('.diferencia').text(sobretiempo - generar);
			var diferencia;
			var time1 = formatTime(sobretiempo), time2 = formatTime(generar);
			n = time1.localeCompare(time2);
			if (n == -1) {
				$(this).parent().parent().find('.negativo').text("+");
				diferencia = getTimeDifference(sobretiempo, generar);
			}
			else if (n == 0) {
				$(this).parent().parent().find('.negativo').text("=");
				diferencia = getTimeDifference(sobretiempo, generar);
			}
			else {
				$(this).parent().parent().find('.negativo').text("-");
				diferencia = getTimeDifference(generar, sobretiempo);
			}
			$(this).parent().parent().find('.diferencia').text(formatTime(diferencia));
			e.stopPropagation();
		});
	});
	UILoadingDefault('hide');
}

//SUSPENSIONES
const formatDate2 = (date) => {

	var from = date.split("/")
	date = new Date(from[2], from[1] - 1, from[0])
	var d = new Date(date),
		month = '' + (d.getMonth() + 1),
		day = '' + d.getDate(),
		year = d.getFullYear();

	if (month.length < 2) month = '0' + month;
	if (day.length < 2) day = '0' + day;

	return [year, month, day].join('');
}

function DateFormatJS(date) {
	var arr = date.split('/');
	return arr[1] + '/' + arr[0] + '/' + arr[2];
}

function GetFiltrosSuspension() {
	g_proyecto = $('#selectProyecto').val();
	g_ccosto = $('#selectCentrosCosto').val();
	g_unegocio = $('#selectUnidadesNegocio').val();
	g_area = $('#selectArea').val();
	g_localidad = $('#selectLocalidad').val();
	g_tipoplanilla = ($('#selectTipoPlanilla').val() == '-1') ? '%' : $('#selectTipoPlanilla').val();
	g_estadotrab = $('#selectEstado').val();
	g_filtro = ($('#txtBusqueda').val() == '') ? '%' : $('#txtBusqueda').val();
	g_excepcion = $('#selectTipoExcepcion').val().split('-')[0];
	g_anual = $('#selectAnhio').val();
}

function GetPermisosUsuario() {
	var jsonParam = {};
	$.getJSON(GetUrl("/ControlAsistencia/GetPermisosUsuario"), jsonParam, function (data) {
		if (data.length > 0) {
			if (data[0].flg_autoriza == 'S') {
				$('#btnGuardarExcepcion').addClass('d-none');
				$('#btnAprobarExcepcion').addClass('d-none');
				$('#btnAutorizarExcepcion').removeClass('d-none');
			} else if (data[0].flg_aprueba == 'S') {
				$('#btnGuardarExcepcion').addClass('d-none');
				$('#btnAutorizarExcepcion').addClass('d-none');
				$('#btnAprobarExcepcion').removeClass('d-none');
			}
			else if (data[0].flg_registra == 'S') {
				$('#btnGuardarExcepcion').removeClass('d-none');
				$('#btnAutorizarExcepcion').addClass('d-none');
				$('#btnAprobarExcepcion').addClass('d-none');
			}
			else {
				$('#btnGuardarExcepcion').addClass('d-none');
				$('#btnAutorizarExcepcion').addClass('d-none');
				$('#btnAprobarExcepcion').addClass('d-none');
			}

			if (data[0].flg_autoriza != 'S') {
				$('#btnAMAutorizar').addClass('d-none');
			}
			if (data[0].flg_aprueba != 'S') {
				$('#btnAMAprobar').addClass('d-none');
			}


			//Esta variable está en el _Layout
			if (!jsonAcceso.aprueba) {
				$('#rowRBAPruebaMAUMA').remove();
			}
			if (!jsonAcceso.autoriza) {
				$('#rowRBAutorizaMAUMA').remove();
			}
		}
	});
}

async function GetColaboradoresSuspension() {
	GetFiltrosSuspension();
	await UILoadingDefault("show");
	var jsonParam = {
		tipoPlanilla: g_tipoplanilla,
		proyecto: g_proyecto,
		centroCosto: g_ccosto,
		localidad: g_localidad,
		unidadNegocio: g_unegocio,
		area: g_area,
		excepcion: g_excepcion,
		anual: g_anual,
		estado: g_estadotrab,
		filtro: g_filtro
	};
	await UIAjax.invokeMethodAsync('ControlAsistencia', 'GetListPersonalExcepciones', jsonParam).then(function (r) {
		var container = $("#tbodyColaboradoresSuspensiones");
		container.empty();
		$.each(r, function (index, row) {
			var html = "<tr onclick=\"GetDetallePersonalSuspensiones('" + row.entidad + "','" + row.personal + "','" + row.trabajador + "');\" class=\"unread\">"
				+ "<td class=\"text-center\">" + row.row_number + "</td>"
				+ "<td class=\"text-center\">" + row.cod_trabajador + "</td>"
				+ "<td>"
				+ "    <h6 class=\"mb-1\">" + row.trabajador + "</h6>"
				+ "</td>"
				+ "<td>" + row.ene + "</td>"
				+ "<td>" + row.feb + "</td>"
				+ "<td>" + row.mar + "</td>"
				+ "<td>" + row.abr + "</td>"
				+ "<td>" + row.may + "</td>"
				+ "<td>" + row.jun + "</td>"
				+ "<td>" + row.jul + "</td>"
				+ "<td>" + row.ago + "</td>"
				+ "<td>" + row.set + "</td>"
				+ "<td>" + row.oct + "</td>"
				+ "<td>" + row.nov + "</td>"
				+ "<td>" + row.dic + "</td>"
				+ "<td>" + row.nom_cargo + "</td>"
				+ "<td>" + row.fecha_ingreso + "</td>"
				+ "<td>" + row.fecha_cese + "</td>"
				+ "<td>" + row.nom_doc + "</td>"
				+ "<td>" + row.nro_doc + "</td>"
				+ "</tr>";
			container.append(html);
		});
	});
	UILoadingDefault("hide");
}

function GetCabeceraResumenExcepciones() {
	var jsonParam = {
		anual: g_anual,
		excepcion: g_excepcion,
		personal: g_codPersonal
	};
	$.getJSON(GetUrl("/ControlAsistencia/GetCabeceraResumenExcepciones"), jsonParam, function (data) {
		if (data.length > 0) {
			$('#lblTipoDeExcepcion').text(data[0].cod_excepcion + " - " + data[0].nombre);
			$('#lblAnhio').text(data[0].cod_anual);
			$('#lblProgramado').text(data[0].programado);
			$('#lblGozado').text(data[0].gozado);
			$('#lblPagado').text(data[0].pagado);
		}
	});
}

async function GetListPeriodoAfectoExcepciones() {
	var jsonParam = {
		excepcion: g_excepcion,
		personal: g_codPersonal
	};
	await UIFunctions._setPrivateSelect('ControlAsistencia', 'GetListPeriodoAfectoExcepciones', jsonParam, false, '#selectRSPeriodoAfecto', 'cod_anual', 'anual_afecto');
	//$.getJSON(GetUrl("/ControlAsistencia/GetListPeriodoAfectoExcepciones"), jsonParam, function (data) {
	//    var container = $("#selectRSPeriodoAfecto");
	//    container.empty();
	//    $.each(data, function (index, row) {
	//        var html = "<option value='" + row.cod_anual + "'>" + row.anual_afecto + "</option>";
	//        container.append(html);
	//    });
	//});
}

function GetExcepcionesxPersonal(pEstado) {
	var jsonParam = {
		anual: g_anual,
		excepcion: g_excepcion,
		personal: g_codPersonal,
		estado: pEstado
	};
	$.getJSON(GetUrl("/ControlAsistencia/GetExcepcionesxPersonal"), jsonParam, function (data) {
		var container = $("#tbodyRelacionDeSolicitudes");
		container.empty();
		$.each(data, function (index, row) {
			var html = "<tr data-numero='" + row.numero + "' data-estado='" + row.cod_estado + "' class=\"unread\">"
				+ "<td class='text-center'>" + row.numero + "</td>"
				+ "<td class='text-center'>" + row.fecha + "</td>"
				+ "<td class='text-center'>" + row.inicio + "</td>"
				+ "<td class='text-center'>" + row.fin + "</td>"
				+ "<td class='text-center'>" + row.unidades + "</td>"
				+ "<td class='text-center'>" + row.estado + "</td>"
				+ "<td class='text-center'>" + ((row.flg_sustento != '') ? "<a href=" + GetUrl('ControlAsistencia/VerSustento?codPersonal=' + g_codPersonal + '&codAnual=' + g_anual + '&codExcepcion=' + g_excepcion + '&numero=' + row.numero) + " target='_blank'><i class='fas fa-eye fontSize30'></i></a>" : "") + "</td>"
				+ "<td>" + row.calculo_aplicado + "</td>"
				+ "</tr>";
			container.append(html);
		});
		$('#tbodyRelacionDeSolicitudes tr').on('click', function (e) {
			e.preventDefault();
			$('#tbodyRelacionDeSolicitudes tr').removeClass('selected');
			$(this).addClass('selected');
			var numExcepcion = $('#tbodyRelacionDeSolicitudes tr.selected').data('numero');
			GetDetalleExcepcion(numExcepcion);
			$('#btnGuardarExcepcion').data('tipo', 'MODIFICAR');
			e.stopPropagation();
		});
		$('#tbodyRelacionDeSolicitudes tr td a').on('click', function (e) {

			e.stopPropagation();
		});
	});
}

function GetDetalleExcepcion(pNumero) {
	var jsonParam = {
		anual: g_anual,
		excepcion: g_excepcion,
		personal: g_codPersonal,
		numero: pNumero
	};
	$.getJSON(GetUrl("/ControlAsistencia/GetExcepcionPersonalDetalle"), jsonParam, function (data) {
		if (data.length > 0) {
			var objE = data[0];
			//console.log(objE);
			$('#txtRSNumero').val(data[0].numero);
			$('#txtRSFecha').val(data[0].fecha);
			$('#selectRSPeriodoAfecto').val(data[0].cod_anual_afecto);
			$('#txtRSEstado').val(data[0].estado_descripcion);
			if (data[0].flg_unidad == 'D') {
				$('#txtRSInicio').val(data[0].inicio);
				$('#txtRSFin').val(data[0].fin);
			}
			else {
				$('#txtRSInicioHoras').val(data[0].inicio);
				$('#txtRSFinHoras').val(data[0].fin);
			}

			$('#txtRSUnidades').val(data[0].unidades);
			$('#txtRSMotivo').val(data[0].motivo);
			$('#selectRSTipo').val(data[0].tipo_solicitud);

			if (data[0].flg_adelanto == 'S')
				$('#chkPagoAdelantado').prop('checked', true);
			else
				$('#chkPagoAdelantado').prop('checked', false);

			$('#selectRSDocumento').val(data[0].cod_documento);
			$('#txtNDocumento').val(data[0].numero_documento);
			$('#txtRSFDocumento').val(data[0].fecha_documento);
			$('#txtRSUsuario').val(data[0].cod_usuario);
			$('#txtRSFRegistro').val(data[0].fecha_registro);
			$('#txtRSModificado').val(data[0].cod_usuario_modifica);
			$('#txtRSFModificado').val(data[0].fecha_modifica);
			$('#txtRSAprobador').val(data[0].cod_aprobador);
			$('#txtRSFAprobacion').val(data[0].fecha_aprobacion);
			$('#txtRSAutorizador').val(data[0].cod_autorizador);
			$('#txtRSFAutorizacion').val(data[0].fecha_autorizacion);
			$('#txtRSRechazo').val(data[0].cod_usuario_rechazo);
			$('#txtRSFRechazo').val(data[0].fecha_rechazo);
		}
	});
}

async function GetExcepcionesAutorizacionMasiva() {
	var jsonParam = {
		anual: g_anual,
		estadoExc: $('input[name=radioEstado]:checked').val(),
		unidad: $('input[name=radioUnidad]:checked').val(),
		tipoPlanilla: g_tipoplanilla,
		mes: $('#selectAMMes').val(),
		estadoPer: g_estadotrab,
		filtro: $('#txtAMFiltro').val()
	};
	await UILoadingDefault('show');
	await UIAjax.invokeMethodAsync('ControlAsistencia', 'GetExcepcionesAutorizacionMasiva', jsonParam).then(function (data) {
		var container = $("#tbodyAMExcepciones");
		container.empty();
		$.each(data, function (index, row) {
			let _id = UIMath.Random();
			var __id = "chkGM" + row.numero + row.cod_personal + _id;
			var html = "<tr data-numero='" + row.numero + "' data-personal='" + row.cod_personal + "'  data-excepcion='" + row.cod_excepcion + "' class=\"unread\">"
				+ "<td class='text-center'><div class='custom-control custom-checkbox'>"
				+ "<input type='checkbox' class='custom-control-input' id='" + __id + "'>"
				+ "<label class='custom-control-label' for='" + __id + "'></label>"
				+ "</div></td>"
				+ "<td class='text-center'>" + row.cod_trabajador + "</td>"
				+ "<td class='text-left'>" + row.trabajador + "</td>"
				+ "<td class='text-center'>" + row.excepcion + "</td>"
				+ "<td class='text-center'>" + row.fecha + "</td>"
				+ "<td class='text-center'>" + row.inicio + "</td>"
				+ "<td class='text-center'>" + row.fin + "</td>"
				+ "<td class='text-center'>" + row.unid + "</td>"
				+ "</tr>";
			container.append(html);
		});
		$('#tbodyAMExcepciones tr').on('click', function (e) {
			e.preventDefault();
			$('#tbodyAMExcepciones tr').removeClass('selected');
			$(this).addClass('selected');
			var tr = $('#tbodyAMExcepciones tr.selected');
			g_codPersonal = tr.data('personal');
			var numExcepcion = tr.data('numero');
			var codExcepcion = tr.data('excepcion');
			GetDetalleExcepcionMasiva(numExcepcion, codExcepcion);
		});
		$('#tbodyAMExcepciones .custom-checkbox').on('click', function (e) {
			e.stopPropagation();
		});
	});
	UILoadingDefault('hide');
	//ControlAsistencia/VerSustento?codPersonal=' + g_codPersonal + '&codAnual=' + g_anual + '&codExcepcion=' + g_excepcion + '&numero=' + row.numero
}

function GetDetalleExcepcionMasiva(pNumero, pExcepcion) {
	var jsonParam = {
		anual: g_anual,
		excepcion: pExcepcion,
		personal: g_codPersonal,
		numero: pNumero
	};
	$.getJSON(GetUrl("/ControlAsistencia/GetExcepcionPersonalDetalle"), jsonParam, function (data) {
		if (data.length > 0) {
			var objSingle = data[0];
			console.log(objSingle);
			$('#tdSustentoMAM').html('');
			$('#selectAMDocumento').val(objSingle.cod_documento);
			$('#txtAMNroDocumento').val(objSingle.numero_documento);
			$('#txtAMFechaDocumento').val(objSingle.fecha_documento);
			$('#txtAMUsuarioModifica').val(objSingle.cod_usuario_modifica);
			$('#txtAMFechaModifica').val(objSingle.fecha_modifica);
			$('#txtAMRechazoExcepcion').val(objSingle.motivo_rechazo);

			$('#lblAMEstado').text(objSingle.estado_descripcion);
			$('#txtAMMotivo').val(objSingle.motivo);
			$('#lblAMFechaRegistra').text(objSingle.fecha_registro);
			$('#lblAMUsuarioRegistra').text(objSingle.cod_usuario);
			$('#lblAMFechaAprueba').text(objSingle.fecha_aprobacion);
			$('#lblAMUsuarioAprueba').text(objSingle.cod_aprobador);
			$('#lblAMFechaAutoriza').text(objSingle.fecha_autorizacion);
			$('#lblAMUsuarioAutoriza').text(objSingle.cod_autorizador);
			if (!UIEmpty(objSingle.flg_sustento)) {
				var jsonParamSustento = {
					codPersonal: g_codPersonal,
					codAnual: g_anual,
					codExcepcion: pExcepcion,
					numero: pNumero
				}
				$('#tdSustentoMAM').html(`<a class="btn btn-sm btn-info" href="${portal}/ControlAsistencia/VerSustento/?${$.param(jsonParamSustento)}" target="_blank" >Ver Sustento</a>`);
			}
		}
	});
}

function GetPeriodoAfectoPersonalExcepciones() {
	var jsonParam = {
		excepcion: "01",
		personal: g_codPersonal
	};
	$.getJSON(GetUrl("/ControlAsistencia/GetPeriodoAfectoPersonalExcepciones"), jsonParam, function (data) {
		if (data.length > 0) {
			$('#selectRSPeriodoAfecto').val(data[0].cod_anual);
		}
	});
}

async function GetDetallePersonalSuspensiones(entidad, personal, trabajador) {
	g_codPersonal = personal;
	g_codEntidad = entidad;
	$('#lblTrabajador').text(trabajador);

	$('#modalRelacionDeSolicitudes').find('input[type=text], textarea').val('');
	$('#modalRelacionDeSolicitudes').find('select').prop('selectedIndex', -1);
	$('#chkPagoAdelantado').prop('checked', false);
	
	GetFiltrosSuspension();

	var jsonParam = {
		CodigoDePersonal: g_codPersonal,
		CodigoDeExcepcion: g_excepcion,
		IndicaGozado: $('#selectGozado').val(),
		IndicaSeleccion: $('#selectSeleccion').val()
	};
		
	tableSaldoExcepciones = $('#tableSaldoExcepciones').DataTable({
		ajax: portal + "/ajax/controlasistencia/GetSaldoExcepciones/?" + $.param(jsonParam),
		// initComplete: function (settings, json) {
		//     //console.log(json);
		// },
		bDestroy: true,
		searching: false,
		paging: false,
		order: false,
		info: false,
		language: {
			url: '/js/language.js'
		},
		columns: [
			{ data: "anio_desde_hasta" },
			{
				data: "porcentaje", render: function (data, type, row, meta) {
					return `<div class="progress">
						<div class="progress-bar bg-primary" role="progressbar" style="width: ${parseInt(data)}%" aria-valuenow="${data}" aria-valuemin="0" aria-valuemax="100"></div>
					</div>`;
				}, className:"py-0 align-middle minw-120px"
			},
			{ data: "porcentaje_text" },
			{ data: "unidades" },
			{ data: "fecha_ini" },
			{ data: "fecha_fin" },
		]
	});

	GetCabeceraResumenExcepciones();
	await GetListPeriodoAfectoExcepciones();
	//console.log(getPeriodo());
	$('#selectRSPeriodoAfecto').val(Entity.UI.Periodo);
	GetExcepcionesxPersonal('%');
	var _option_texto = $('#selectTipoExcepcion option:selected').text();
	$('#h4TitleMRS').text(`RELACIÓN DE ${_option_texto}`);
	$('#labelSubTitleMRS').text(`REGISTRO DE ${_option_texto}`);
	
	$('#modalRelacionDeSolicitudes').modal('show');
}

async function DeleteExcepcion(numExcepcion, estado, Confirmar) {
	//containerColaboradores.maskContainer("Cargando ....");
	await UILoadingDefault("show");
	var jsonParam = {
		CodigoDeExcepcion: g_excepcion,
		CodigoDePersonal: g_codPersonal,
		CodigoAnual: g_anual,
		Numero: numExcepcion,
		Confirmar: Confirmar,
	};
	//await UIAjax.invokeFunctionAsync('ControlAsistencia', 'DeleteExcepcion', jsonParam).then(function (r) {
	//    if (r.respuesta != 1) {
	//        MsgBox(r.mensaje, "warning", false);
	//        return;
	//    }
	//    else {
	//        if (estado != 'A')
	//            MsgBox("Anulación exitosa.", "success", false);
	//        else
	//            MsgBox("Eliminación exitosa.", "success", false);

	//        GetCabeceraResumenExcepciones();
	//        if ($('#chkRegistrados').prop('checked')) GetExcepcionesxPersonal('R');
	//        else GetExcepcionesxPersonal('%');
	//        NewExcepcion();
	//    }
	//});
	await UIAjax.runFunctionAsync('ControlAsistencia', 'DeleteExcepcion', jsonParam, function () {
		GetCabeceraResumenExcepciones();
		if ($('#chkRegistrados').prop('checked')) GetExcepcionesxPersonal('R');
		else GetExcepcionesxPersonal('%');
		NewExcepcion();
	});
	UILoadingDefault("hide");
};

function PostExcepcion(pTipoGrabacion, pNumero) {
	containerColaboradores = $('#containerInputExcepcion');
	containerColaboradores.maskContainer("Cargando ....");
	let year = g_anual;
	let unit = $('#selectTipoExcepcion').val().split('-')[1];
	let typeException = g_excepcion;
	let textMotivo = $('#txtRSMotivo').val();
	let dateStart = (unit == 'D') ? $('#txtRSInicio').val() : $('#txtRSInicioHoras').val();
	let dateEnd = (unit == 'D') ? $('#txtRSFin').val() : $('#txtRSFinHoras').val();
	let TypeDocument = $('#selectRSDocumento').val();
	let dateException = (unit == 'D') ? $('#txtRSInicio').val() : $('#txtRSFecha').val();
	let TypeRequest = $('#selectRSTipo').val();
	let NroDocument = $('#txtNDocumento').val();
	let dateDocument = $('#txtRSFDocumento').val();
	let anualAfecto = $('#selectRSPeriodoAfecto').val();

	let vflgAdelanto = ($("#chkPagoAdelantado").prop('checked')) ? 'S' : 'N';
	dateException = formatDate2(dateException)
	if (unit == "D") {
		dateStart = formatDate2(dateStart);
		dateEnd = formatDate2(dateEnd);
	} else {
		dateStart = dateStart;
		dateEnd = dateEnd;
	}

	var input = document.getElementById("inputFileSustento");
	var files = input.files;
	var formData = new FormData();

	formData.append("tipoGrabacion", pTipoGrabacion);
	formData.append("codAnual", year);
	formData.append("codExcepcion", typeException);
	formData.append("codPersonal", g_codPersonal);
	formData.append("tipoSolicitud", TypeRequest);
	formData.append("motivo", textMotivo);
	formData.append("inicio", dateStart);
	formData.append("fin", dateEnd);
	formData.append("numero", pNumero);
	formData.append("flgUnidad", unit);
	formData.append("fecha", dateException);
	formData.append("codDocumento", TypeDocument);
	formData.append("numeroDocumento", NroDocument);
	formData.append("fechaDocumento", dateDocument);
	formData.append("codAnualAfecto", anualAfecto);
	formData.append("flgAdelanto", vflgAdelanto);
	formData.append("codTipoPlanilla", g_tipoplanilla);
	formData.append("file", files[0]);

	$.ajax({
		url: GetUrl("ControlAsistencia/PostException"),
		data: formData,
		processData: false,
		contentType: false,
		type: "POST",
		success: function (data) {
			console.log(data);
			if (data.respuesta == 1) {
				MsgBox("success");
				GetCabeceraResumenExcepciones();

				if ($(this).prop('checked')) GetExcepcionesxPersonal('R');
				else GetExcepcionesxPersonal('%');

				NewExcepcion();
				GetColaboradoresSuspension();
			}
			else {
				MsgBox(data.mensaje, "warning", false);
				console.log(data.mensaje);
			}
		},
		error: function (result) {
			MsgBox("error");
		}
	});
	containerColaboradores.unmaskContainer();

}

function PostAutorizacionMasivaExcepcion(pEstadoDestino, pMotivoRechazo) {
	if ($('#tbodyAMExcepciones tr td input[type=checkbox]:checked').length < 1) {
		MsgBox("Seleccionar las excepciones a procesar.", "warning", false);
		return;
	}
	containerColaboradores.maskContainer("Cargando ....");

	var plista = [];
	$('#tbodyAMExcepciones tr td input[type=checkbox]:checked').each(function (e) {
		var row = $(this).parent().parent().parent();
		var obj = {
			cod_anual: g_anual,
			cod_excepcion: $(row).data('excepcion'),
			cod_personal: $(row).data('personal'),
			numero: $(row).data('numero')
		};
		plista.push(obj);
	});
	var saveObj = {
		tipoPlanilla: g_tipoplanilla,
		estadoOriginal: $('input[name=radioEstado]:checked').val(),
		estadoDestino: pEstadoDestino,
		motivoRechazo: pMotivoRechazo,
		lista: plista
	};

	$.post(GetUrl("ControlAsistencia/PostExcepcionesAutorizacionMasiva"), saveObj)
		.done(function (data) {
			if (data.respuesta == 1) {
				MsgBox("success");
				GetExcepcionesAutorizacionMasiva();
				$('#detalle1 label').text('');
				$('#detalle2 input, #detalle1 textarea, #detalle2 select').val('');
			}
			else {
				MsgBox(data.mensaje, "warning", false);
				console.log(data.mensaje);
			}

		})
		.fail(function (data) {
			MsgBox("error");
			console.log(data);
		});
	containerColaboradores.unmaskContainer();
}

async function PostGeneracionMasivaExcepcion() {
	if ($('#tbodyGMExcepciones tr td input[type=checkbox]:checked').length < 1) {
		MsgBox("Seleccionar al personal.", "warning", true);
		return;
	}    
	var pflgUnidad = $('input[name=radioGMUnidad]:checked').val();
	//var pfecha = (pflgUnidad == 'D') ? $('#txtGMFechaInicioExc').val() : $('#txtGMFechaExc').val()
	var pfecha = (pflgUnidad == 'D') ? $('#txtGMFechaExc').val() : $('#txtGMFechaExc').val()
	var pInicio = (pflgUnidad == 'D') ? $('#txtGMFechaInicioExc').val() : $('#txtGMHoraInicioExc').val();
	var pFin = (pflgUnidad == 'D') ? $('#txtGMFechaFinExc').val() : $('#txtGMHoraFinExc').val();
	if (UIEmpty(pfecha)) {
		MsgBox("La fecha no puede ser vacía.", "warning", true);
		return;
	}
	if (UIEmpty(pInicio)) {
		MsgBox("El inicio no puede ser vacío.", "warning", true);
		return;
	}
	if (UIEmpty(pFin)) {
		MsgBox("El fin no puede ser vacío.", "warning", true);
		return;
	}
	pfecha = formatDate2(pfecha);
	if (pflgUnidad == "D") {
		pInicio = formatDate2(pInicio);
		pFin = formatDate2(pFin);
	} else {
		pInicio = pInicio;
		pFin = pFin;
	}
	var jsonParam = {
		Accion: "ADICIONAR",
		FlgUnidad: pflgUnidad,
		CodigoDeExcepcion: $('#selectGMExcepcion').val(),
		Fecha: pfecha,
		FechaDeInicio: pInicio,
		FechaDeFin: pFin,
		Json: [],
	};
	$('#tbodyGMExcepciones tr td input[type=checkbox]:checked').each(function (e) {
		var ths = $(this);        
		var tr = ths.parents('tr');
		//console.log(tr);
		var jsonRow = JSON.parse(tr.attr('data-json'));
		jsonParam.Json.push({
			NombreDePersonal: jsonRow.trabajador,
			NumeroDeDocumento: jsonRow.numero_doc,
			CodigoDePersonal: jsonRow.personal,
		});
	});
	await UILoadingDefault("show");
	await UIAjax.runFunctionAsync('ControlAsistencia', 'PostExcepcionesGeneracionMasiva', jsonParam, null, null, UILoadingDefault);
}

function GetPersonalGMExcepciones() {
	jsonParam = {
		tipoPlanilla: $('#selectGMTipoPlanilla').val(),
		proyecto: $('#selectGMProyecto').val(),
		centroCosto: $('#selectGMCC').val(),
		localidad: $('#selectGMLocalidad').val(),
		unidadNegocio: $('#selectGMUN').val(),
		area: $('#selectGMArea').val(),
		estado: $('input[name=radioGMEstado]:checked').val(),
		filtro: $('#txtGMFiltro').val(),
	};
	$.getJSON(GetUrl('/Personal/GetColaboradoresLegajo'), jsonParam, function (data) {
		var container = $("#tbodyGMExcepciones");
		container.empty();
		$.each(data, function (index, row) {
			var html = "<tr data-personal='" + row.personal + "' data-json='" + JSON.stringify(row) + "' class=\"unread\">"
				+ "<td class='text-center'><div class='custom-control custom-checkbox'>"
				+ "<input type='checkbox' class='custom-control-input' id='chkGM" + row.numero_doc + row.personal + "'>"
				+ "<label class='custom-control-label' for='chkGM" + row.numero_doc + row.personal + "'></label>"
				+ "</div></td>"
				+ "<td class=\"text-center\">" + row.numero_doc + "</td>"
				+ "<td><h6 class=\"mb-1\">" + row.trabajador + "</h6></td>"
				+ "<td>" + row.fotocheck + "</td>"
				+ "<td>" + row.desc_estado + "</td>"
				+ "</tr>";
			container.append(html);
		});
	});

}

function GetGMException() {
	var valor = $('input[name=radioGMUnidad]:checked').val();
	var jsonParam = {
		unidad: valor
	};
	$.getJSON(GetUrl("ControlAsistencia/GetTipoExcepcionxUnidad"), jsonParam, function (data) {
		var container = $("#selectGMExcepcion");
		container.empty();
		$.each(data, function (index, row) {
			var html = "<option value='" + row.codigo + "'>" + row.nombre + "</option>";
			container.append(html);
		});
	});
}

function responseImport(data, id) {
	var container = $(id);
	container.empty();

	if (id === '#tbodyError') {
		$.each(data, function (index, value) {
			var html = "<tr>"
				+ "<td class='text-center'>" + value.fila + "</td>"
				+ "<td class='text-center'>" + value.texto + "</td>"
				+ "<td class='text-center'>" + value.dato + "</td>"
				+ "</tr>";
			container.append(html);
		});
	} else {
		$.each(data, function (index, value) {
			var html = "<tr>"
				+ "<td class='text-center'>" + value.cod_tipo_excepcion + "</td>"
				+ "<td class='text-center'>" + value.nro_doc + "</td>"
				+ "<td class='text-center'>" + value.trabajador + "</td>"
				+ "<td class='text-center'>" + value.mensaje + "</td>"
				+ "</tr>";
			container.append(html);
		});
	}
};

function NewExcepcion(date) {
	$('#containerInputExcepcion input[type=text], #containerInputExcepcion textarea').val('');
	$('#containerInputExcepcion input[type=checkbox]').prop('checked', false);
	$('#containerInputExcepcion select').val('-1');
	$('#txtRSFecha').val(date);

	$('#txtRSInicio').val(date);
	$('#txtRSFin').val(date);
	$('#txtRSInicioHoras').val('00:00');
	$('#txtRSFinHoras').val('00:00');
	$('#txtRSUnidades').val('1');

	$("#txtRSEstado").val("Registrado");
	if (g_excepcion == '01') GetPeriodoAfectoPersonalExcepciones();
	else $('#selectRSPeriodoAfecto').val(g_anual);
	$('#tbodyRelacionDeSolicitudes tr').removeClass('selected');
	$('#btnGuardarExcepcion').data('tipo', 'ADICIONAR');
};

function validaHorasInicioFin(baseInicio, baseFin, inicio, fin, nombreHorario) {
	var _inicio = formatTime(inicio);
	var _fin = formatTime(fin);
	var _baseInicio = formatTime(baseInicio);
	var _baseFin = formatTime(baseFin);
	if (_baseInicio <= _baseFin) {
		if (_inicio < _baseInicio) {
			MsgBox("Hora inicio no puede ser menor a la hora de ingreso del horario\nHorario " + nombreHorario + " de " + _baseInicio + " a " + _baseFin, "warning", true);
			return false;
		}
		if (_fin > _baseFin) {
			MsgBox("Hora fin no puede ser mayor a la hora de salida del horario\nHorario " + nombreHorario + " de " + _baseInicio + " a " + _baseFin, "warning", true);
			return false;
		}
	}
	else {
		if ((_inicio >= _baseInicio) || (_inicio < _baseFin)) {}
		else {
			MsgBox("Error en la hora de Inicio:" + _inicio + "\nHorario " + nombreHorario + " de " + _baseInicio + " a " + _baseFin, "warning", true);
			return;
		}
		if ((_fin > _inicio) || (_fin <= _baseFin)) {}
		else {
			MsgBox("Error en la hora de Fin:" + _fin + "\nHorario " + nombreHorario + " de " + _baseInicio + " a " + _baseFin, "warning", true);
			return false;
		}
	}
	return true;
}

function validacionEspecialHoras(fecha, inicio, fin) {
	var horarioTrabFecha = GetHorarioPersonalxFecha(formatDate(fecha));
	var horarioTrab = GetHorarioPersonal();
	if (horarioTrabFecha.length > 0) {
		if (horarioTrabFecha[0].flg_hhee == "S") return true;
		var hi = new Date(horarioTrabFecha[0].hi);
		var hs = new Date(horarioTrabFecha[0].hs);
		if (!validaHorasInicioFin(hi, hs, inicio, fin, horarioTrabFecha[0].nombre))
			return false;
		return true;
	}

	if (horarioTrab.length == 0) return true;
	if (horarioTrab[0].flg_hhee == "S") return true;

	var day = fecha.getDay() + 1;
	for (i = 0; i < horarioTrab.length; i++) {
		if (horarioTrab[i].dia == day) {
			var hi = new Date(horarioTrab[0].hi);
			var hs = new Date(horarioTrab[0].hs);
			if (!validaHorasInicioFin(hi, hs, inicio, fin, horarioTrab[i].nombre))
				return false;
			break;
		}
	}
	return true;
}

function GetHorarioPersonalxFecha(pFecha) {
	var response = null;
	$.ajax({
		type: "GET",
		url: GetUrl("ControlAsistencia/GetHorarioPersonalxFecha?fecha=" + pFecha + "&personal=" + g_codPersonal + "&excepcion=" + g_excepcion)/*.concat(p_codEntidad, "&codPersonal=").concat(p_codPersonal)*/,
		//data: JSON.stringify(jsonParam),
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		async: false,
		success: function success(data) {
			response = data;
		},
		error: function error(response) {
			response = null;
		}
	});
	return response;
}

function GetHorarioPersonal() {
	var response = null;
	$.ajax({
		type: "GET",
		url: GetUrl("ControlAsistencia/GetHorarioPersonal?personal=" + g_codPersonal + "&excepcion=" + g_excepcion)/*.concat(p_codEntidad, "&codPersonal=").concat(p_codPersonal)*/,
		//data: JSON.stringify(jsonParam),
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		async: false,
		success: function success(data) {
			response = data;
		},
		error: function error(response) {
			response = null;
		}
	});
	return response;
}