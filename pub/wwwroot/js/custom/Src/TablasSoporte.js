﻿function HexToRgb() {
	let input = $("#inputColorMensaje").val();
	let value = input.match(/[A-Za-z0-9]{2}/g);
	value = value.map(function (v) { return parseInt(v, 16) });
	$('#numRojo').val(value[0]);
	$('#numVerde').val(value[1]);
	$('#numAzul').val(value[2]);
	$('#spanColor').css("background-color", input);
}

function RgbToHex(rgb) {
	var hex = Number(rgb).toString(16);
	if (hex.length < 2) {
		hex = "0" + hex;
	}
	return hex;
};

function fullColorHex() {
	var red = RgbToHex($('#numRojo').val());
	var green = RgbToHex($('#numVerde').val());
	var blue = RgbToHex($('#numAzul').val());
	var color = '#' + red + green + blue;
	$("#inputColorMensaje").val(color);
	$('#spanColor').css("background-color", color);
}

function cleanInputModal(idModal) {
	$('#' + idModal).find('input[type=text], input[type=file], input[type=hidden], textarea').val('')
	$('#' + idModal).find('select').prop('selectedIndex', 0);
	$('#' + idModal).find('img').attr('src', '~/images/custom/imgUndefinedCompany.png');
}

function FillEmpresas() {
	var table = $("#tblEmpresas");
	if ($.fn.dataTable.isDataTable(table)) {
		table.DataTable().clear();
		table.DataTable().destroy();
	}
	table.empty();

	$.getJSON(GetUrl("TablasSoporte/GetEmpresas"), {}, function (sourceTable) {
		tblEmpresas = table.DataTable({
			data: sourceTable.data,
			scrollCollapse: true,
			fixedHeader: true,
			select: {
				style: "single",
			},
			language: { url: "/js/language.js" },
			paging: false,
			columns: [
				{
					title: "Código",
					data: "codigo",
					className: "text-center",
				},
				{
					title: "Nombre",
					data: "nombre",
					className: "text-center",
				},
				{
					title: "Siglas",
					data: "siglas",
					className: "text-left",
				},
			],
			columnDefs: [{ targets: "no-sort", orderable: false }],
			info: false,
			searching: false,
		});
		$("#tblEmpresas thead").addClass("thead-sky");
	});
}

function FillEmpresasClonar() {
	var table = $("#tblEmpresasClonar");
	if ($.fn.dataTable.isDataTable(table)) {
		table.DataTable().clear();
		table.DataTable().destroy();
	}
	table.empty();

	$.getJSON(GetUrl("TablasSoporte/GetEmpresas"), { flgClonar: 'S' }, function (sourceTable) {
		tblEmpresasClonar = table.DataTable({
			data: sourceTable.data,
			scrollCollapse: true,
			fixedHeader: true,
			select: {
				style: "single",
			},
			language: { url: "/js/language.js" },
			paging: false,
			columns: [
				{
					title: "Código",
					data: "codigo",
					className: "text-center",
				},
				{
					title: "Empresa",
					data: "nombre",
					className: "text-center",
				},
			],
			columnDefs: [{ targets: "no-sort", orderable: false }],
			info: false,
			searching: false,
		});
		$("#tblEmpresasClonar thead").addClass("thead-sky");
		tblEmpresasClonar.on('click', 'tbody tr', function () {
			$('#tblEmpresasClonar tbody tr').removeClass('selected');
			$(this).addClass('selected');
		})
	});
}

async function FillDatosGenerales() {
	$.getJSON(GetUrl("TablasSoporte/GetEmpresaDataTab"), { empresa: gEmpresa, flg: "generales" }, async function (source) {
		if (source.length < 1) {
			return;
		}
		var data = source.data[0];
		//console.log(data);
		$("#txtCodigo").val(data.cod_empresa);
		$("#selectEstado").val(data.flg_activa);
		$("#txtEmpresa").val(data.nombre);
		$("#txtRazonSocial").val(data.razon_social);
		$("#txtSiglas").val(data.siglas);
		$("#txtRUC").val(data.ruc);
		$("#selectVia").val(data.cod_via);
		$("#txtDirecion").val(data.direccion);
		$("#txtNDireccion").val(data.numero);
		$("#txtIntDireccion").val(data.interior);
		$("#selectZona").val(data.cod_zona);
		$("#txtZona").val(data.zona);
		if (data.cod_ubigeo != '') {
			$('#selectDepartamento').val(data.departamento);
			await UIFunctions.setProvincia('#selectProvincia', data.departamento, "codigo");
			$('#selectProvincia').val(data.provincia);
			await UIFunctions.setDistrito('#selectDistrito', data.provincia, "codigo");
			$('#selectDistrito').val(data.cod_ubigeo);
		}
		$("#txtReferencia").val(data.referencia);
		$("#txtRepresentante").val(data.representante);
		$("#txtDocRepresentante").val(data.doc_representante);
		$("#selectSexo").val(data.representante_sexo);
		$("#selectCargoRepresentante").val(data.cod_cargo);
		$("#txtJefeDeRRHH").val(data.jefe_rrhh);
		$("#txtDocJefeDeRRHH").val(data.jefe_rrhh_doc);
		$("#selectCargojefeDeRRHH").val(data.jefe_rrhh_cargo);
		$("#txtNroParRegistral").val(data.partida_registral);
		$("#txtCiuParRegistral").val(data.ciudad_partida);
		$("#txtRegPatr").val(data.registro_patronal);
		$("#txtTelefonos").val(data.telefonos);
		$("#txtFax").val(data.fax);
		$("#selectRegimen").val(data.flg_regimen);
		$("#txtEmail").val(data.email);
		$("#txtWeb").val(data.www);
		$("#txtSlogan").val(data.slogan);
		$("#selectMonedaBase").val(data.cod_moneda_base);
		$("#selectMonedaExtranjera").val(data.cod_moneda_extranjera);
		$("#selectPTipoDeCambio").val(data.flg_tipo_cambio);
		$("#selectTipoPersonal").val(data.tipo_pers);
		$("#selectBoleta").val(data.flg_boleta);
		$("#selectCTS").val(data.flg_cts);
		$("#selectUtilidades").val(data.flg_utilidad);
		$("#selectLiquidacion").val(data.flg_liquidacion);
		$("#selectExcepciones").val(data.flg_papeleta_excepciones);
		$("#selectCtaCte").val(data.flg_cta_cte);
		$("#selectQuintaAnual").val(data.flg_quinta);
		$("#selectAFPSNP").val(data.flg_afp);
		$("#txtPapeletaAuditoria").val(data.papeleta_vac_auditoria_area);
		$("#txtVersion").val(data.papeleta_vac_auditoria_version);
		$("#txtCadena1").val(data.cadena1);
		$("#selectPlanillaOf").val(data.flg_planilla_oficial_gral);
		$("#selectSistemaContabilidad").val(data.cod_sist_contable);
		$("#selectNucleoEmpresarial").val(data.cod_empresa_nucleo),
			$("#selectEmpresaNucleoEmpresarial").val(data.adsad),
			$("#chkBoletaCopia").prop(
				"checked",
				data.flg_boleta_una_copia == "S" ? true : false
			);
		$("#chkCTSCopia").prop(
			"checked",
			data.flg_cts_una_copia == "S" ? true : false
		);
		$("#chkUtilidadesCopia").prop(
			"checked",
			data.flg_utilidad_una_copia == "S" ? true : false
		);
		//$('#chkLiquidacionCopia').prop('checked', (data.flg_boleta_una_copia == 'S' ? true : false))
		$("#chkBoletaDatosEmpresa").prop(
			"checked",
			data.flg_boleta_datos_empresa == "S" ? true : false
		);
		$("#chkLiquidacionDatosEmpresa").prop(
			"checked",
			data.flg_liquidacion_datos_empresa == "S" ? true : false
		);
		$("#chkAFPSNP").prop(
			"checked",
			data.flg_quinta_mostrar_creditos == "S" ? true : false
		);
		$("#chkAgente").prop("checked", data.flg_agente == "S" ? true : false);
	});
}

function viewDetailCuenta(pthis) {
	event.preventDefault()
	var table = $(pthis).closest('table');
	var tr = $(pthis).closest('tr');
	table.find('tbody tr').removeClass('selected');
	tr.addClass('selected');
	var xRows = tblCuentas.rows('.selected').data();
	if (xRows.length != 0x0001) {
		MsgBox("Seleccione una cuenta.", "warning", true);
		return;
	}
	$('#txtCodCuentaNew').val(xRows[0].cod_cuenta_bancaria);
	$('#selectBancoNew').val(xRows[0].cod_entidad);
	$('#selectMonedaNew').val(xRows[0].cod_moneda);
	$('#txtNroCuentaNew').val(xRows[0].num_cuenta_bancaria);
	$('#modalCuentaBancaria').modal({ show: true });
	event.stopPropagation()
}
function viewDetailDocumento(pthis) {
	event.preventDefault()
	var table = $(pthis).closest('table');
	var tr = $(pthis).closest('tr');
	table.find('tbody tr').removeClass('selected');
	tr.addClass('selected');
	var xRows = tblDocumentos.rows('.selected').data();
	if (xRows.length != 0x0001) {
		MsgBox("Seleccione una cuenta.", "warning", true);
		return;
	}
	$('#txtCodDocInstitucional').val(xRows[0].cod_documento);
	$('#txtDescripcionDocNew').val(xRows[0].descripcion);
	$('#txtFechaDocNew').val(xRows[0].fecha_publicacion);
	$('#chkVistaDocNew').prop('checked', xRows[0].flg_estado == 'S' ? true : false);
	$('#txtMensajeDocNew').val(xRows[0].mensaje);
	$('#fileDocNew').val('');
	$('#modalDocInstitucional').modal({ show: true });
	event.stopPropagation()
}
function viewDetailNoticia(pthis) {
	event.preventDefault()
	var table = $(pthis).closest('table');
	var tr = $(pthis).closest('tr');
	table.find('tbody tr').removeClass('selected');
	tr.addClass('selected');
	var xRows = tblNoticias.rows('.selected').data();
	if (xRows.length != 0x0001) {
		MsgBox("Seleccione una cuenta.", "warning", true);
		return;
	}
	$('#txtCodNoticiaNew').val(xRows[0].num_correlativo);
	$('#txtTituloNoticiaNew').val(xRows[0].titulo);
	$('#txtFechaNoticiaNew').val(xRows[0].fecha);
	$('#chkVistaNoticiaNew').prop('checked', xRows[0].flg_activo == 'SI' ? true : false);
	$('#txtDetalleNoticiaNew').val(xRows[0].detalle);
	$('#fileNoticiaNew').val('');
	FillImgNoticia(xRows[0].num_correlativo);
	$('#modalNoticia').modal({ show: true });
	event.stopPropagation()
}

// function viewDetailVideo(){
//   event.preventDefault()

//   event.stopPropagation()
// }

function FillDatosAdicionales() {
  $.getJSON(
    GetUrl("TablasSoporte/GetEmpresaDataTab"),
    { empresa: gEmpresa, flg: "adicionales" },
    function (source) {
      if (source.length < 1) {
        return;
      }
      var data = source.data[0];
      $("#txtPDF").val(data.dir_pdf);
      $("#txtColorFondoWeb").val(data.color_fondo_menu_web);
      $("#txtArchivoFirmaBoleta").val(data.dir_firma_boleta),
      $("#txtPasswordFirmaBoleta").val(data.password_firma_boleta),
      $("#txtPosicionXFirmaBoleta").val(data.posicion_x_firma_boleta),
      $("#txtPosicionYFirmaBoleta").val(data.posicion_y_firma_boleta),
      $("#txtArchivoFirmaLiquidacion").val(data.dir_firma_liquidacion),
      $("#txtPasswordFirmaLiquidacion").val(data.password_firma_liquidacion),
      $("#txtPosicionXFirmaLiquidacion").val(data.posicion_x_firma_liquidacion),
      $("#txtPosicionYFirmaLiquidacion").val(data.posicion_y_firma_liquidacion),
      $("#txtArchivoFirmaCTS").val(data.dir_firma_cts),
      $("#txtPasswordFirmaCTS").val(data.password_firma_cts),
      $("#txtPosicionXFirmaCTS").val(data.posicion_x_firma_cts),
      $("#txtPosicionYFirmaCTS").val(data.posicion_y_firma_cts),
      $("#txtArchivoFirmaUtilidad").val(data.dir_firma_utilidad),
      $("#txtPasswordFirmaUtilidad").val(data.password_firma_utilidad),
      $("#txtPosicionXFirmaUtilidad").val(data.posicion_x_firma_utilidad),
      $("#txtPosicionYFirmaUtilidad").val(data.posicion_y_firma_utilidad),
      $("#txtArchivoFirmaQuinta").val(data.dir_firma_quinta),
      $("#txtPasswordFirmaQuinta").val(data.password_firma_quinta),
      $("#txtPosicionXFirmaQuinta").val(data.posicion_x_firma_quinta),
      $("#txtPosicionYFirmaQuinta").val(data.posicion_y_firma_quinta),
      $("#txtArchivoFirmaContrato").val(data.dir_firma_contrato),
      $("#txtPasswordFirmaContrato").val(data.password_firma_contrato),
      $("#txtPosicionXFirmaContrato").val(data.posicion_x_firma_contrato),
      $("#txtPosicionYFirmaContrato").val(data.posicion_y_firma_contrato),
      $("#txtArchivoFirmaDocsConfig").val(data.dir_firma_docs_config),
      $("#txtPasswordFirmaDocsConfig").val(data.password_firma_docs_config),
      $("#txtPosicionXFirmaDocsConfig").val(data.posicion_x_firma_docs_config),
      $("#txtPosicionYFirmaDocsConfig").val(data.posicion_y_firma_docs_config),
      $("#txtServidor").val(data.smtp_servidor);
      $("#txtPuerto").val(data.smtp_puerto);
      $("#selectTipoSMTP").val(data.smtp_encript);
      $("#txtEmailDA").val(data.smtp_usuario);
      $("#txtPassword").val(data.smtp_password);
      $("#txtNombreDeRemitente").val(data.email_remitente);
      $("#selectCorreoAEnviar").val(data.flg_envio_mail);
      $("#txtCabeceraEmail").val(data.cabecera_email);
      //$('#colorColorMensaje').val(data.cod_zona);
      $("#numRojo").val(data.red_mail);
      $("#numVerde").val(data.green_mail);
      $("#numAzul").val(data.blue_mail);
      $("#txtEmailCuerpo").val(data.email_cuerpo);

      $("#chkPDF").prop(
        "checked",
        data.flg_encripta_boleta == "S" ? true : false
      );
      $("#chkReqUsuPass").prop(
        "checked",
        data.email_passreq == "S" ? true : false
      );
      $("#chkContenidoHTML").prop(
        "checked",
        data.flg_html == "S" ? true : false
      );
    }
  );
}

async function FillCuentasBancarias() {

	var table = $("#tblCuentasBancarias");
	if ($.fn.dataTable.isDataTable(table)) {
		table.DataTable().clear();
		table.DataTable().destroy();
	}
	table.empty();

	$.getJSON(GetUrl("TablasSoporte/GetEmpresaDataTab"), { empresa: gEmpresa, flg: "cuentas" }, function (sourceTable) {
		tblCuentas = table.DataTable({
			data: sourceTable.data,
			scrollCollapse: true,
			fixedHeader: true,
			select: {
				style: "single",
			},
			language: { url: "/js/language.js" },
			paging: false,
			columns: [
				{
					title: "Banco",
					data: "banco",
					className: "text-center",
				},
				{
					title: "Moneda",
					data: "moneda",
					className: "text-center",
				},
				{
					title: "Nro. Cuenta Bancaria",
					data: "num_cuenta_bancaria",
					className: "text-left",
				},
			],
			columnDefs: [{ targets: "no-sort", orderable: false }],
			info: false,
			searching: false,
		});
		$("#tblCuentasBancarias thead").addClass("thead-sky");
		tblCuentas.on('click', 'tbody tr', function () {
			$('#tblCuentasBancarias tbody tr').removeClass('selected');
			$(this).addClass('selected');
		})
		tblCuentas.on('dblclick', 'tbody tr', function (e) {
			viewDetailCuenta()
		})
		tblCuentas.on('doubleTap', 'tbody tr', function (e) {
			viewDetailCuenta()
		})
	}
	);
}

function FillDocumentos() {
	var table = $("#tblDocumentos");
	if ($.fn.dataTable.isDataTable(table)) {
		table.DataTable().clear();
		table.DataTable().destroy();
	}
	table.empty();

	$.getJSON(GetUrl("TablasSoporte/GetEmpresaDataTab"), { empresa: gEmpresa, flg: "documentos" }, function (sourceTable) {
		tblDocumentos = table.DataTable({
			data: sourceTable.data,
			scrollCollapse: true,
			fixedHeader: true,
			select: {
				style: "single",
			},
			language: { url: "/js/language.js" },
			paging: false,
			columns: [
				{
					title: "Sel",
					data: "cod_documento",
					className: "text-center",
					render: function (data, type, row) {
						return (
							'<div class="custom-control custom-checkbox"><input type="checkbox" value="' + row.ruta + '" data-codigo="' + data + '" data-nombre="' + row.descripcion + '" data-mensaje="' + row.mensaje + '" class="custom-control-input chkSel" id="chkSel' + row.cod_documento + '"><label class="custom-control-label" for="chkSel' + row.cod_documento + '"></label></div>'
						);
					},
				},
				{
					title: "Código",
					data: "cod_documento",
					className: "text-center",
				},
				{
					title: "Descripción",
					data: "descripcion",
					className: "text-left",
				},
				{
					title: "Vista",
					data: "flg_estado",
					className: "text-left",
					render: function (data, type, row) {
						return (
							'<div class="custom-control custom-checkbox"><input disabled type="checkbox" ' +
							(data == "S" ? "checked" : "") +
							' value="' +
							data +
							'" class="custom-control-input" id="chkVista' +
							row.cod_documento +
							'"><label class="custom-control-label" for="chkVista' +
							row.cod_documento +
							'"></label></div>'
						);
					},
				},
				{
					title: "Fecha Publicación",
					data: "fecha_publicacion",
					className: "text-left",
				},
				{
					title: "...",
					data: "cod_documento",
					className: "text-left",
				},
				{
					title: "Documento",
					data: "ruta",
					className: "text-left",
				},
				{
					title: "Mensaje",
					data: "mensaje",
					className: "text-left",
				},
			],
			columnDefs: [{ targets: "no-sort", orderable: false }],
			info: false,
			searching: false,
		});
		$("#tblDocumentos thead").addClass("thead-sky");
		tblDocumentos.on('click', 'tbody tr', function () {
			$('#tblDocumentos tbody tr').removeClass('selected');
			$(this).addClass('selected');
		})
		tblDocumentos.on('dblclick', 'tbody tr', function () {
			viewDetailDocumento();
		})
		tblDocumentos.on('doubleTap', 'tbody tr', function () {
			viewDetailDocumento();
		})
	}
	);
}

function FillNoticias() {
	var table = $("#tblNoticias");
	if ($.fn.dataTable.isDataTable(table)) {
		table.DataTable().clear();
		table.DataTable().destroy();
	}
	table.empty();

	$.getJSON(GetUrl("TablasSoporte/GetEmpresaDataTab"), { empresa: gEmpresa, flg: "noticias" }, function (sourceTable) {
		tblNoticias = table.DataTable({
			data: sourceTable.data,
			scrollCollapse: true,
			fixedHeader: true,
			select: {
				style: "single",
			},
			language: { url: "/js/language.js" },
			paging: false,
			columns: [
				{
					title: "Activo",
					data: "flg_activo",
					className: "text-left",
					render: function (data, type, row) {
						return (
							'<div class="custom-control custom-checkbox"><input disabled type="checkbox" ' +
							(data == "SI" ? "checked" : "") +
							' value="' +
							data +
							'" class="custom-control-input" id="chkActivo' +
							row.num_correlativo +
							'"><label class="custom-control-label" for="chkActivo' +
							row.num_correlativo +
							'"></label></div>'
						);
					},
				},
				{
					title: "Fecha",
					data: "fecha",
					className: "text-left",
				},
				{
					title: "Titulo",
					data: "titulo",
					className: "text-left",
				},
				{
					title: "Detalle",
					data: "detalle",
					className: "text-left d-none",
				},
			],
			columnDefs: [{ targets: "no-sort", orderable: false }],
			info: false,
			searching: false,
		});
		$("#tblNoticias thead").addClass("thead-sky");
		tblNoticias.on('click', 'tbody tr', function () {
			$('#tblNoticias tbody tr').removeClass('selected');
			$(this).addClass('selected');
		})
		tblNoticias.on('dblclick', 'tbody tr', function () {
			viewDetailNoticia()
		})
		tblNoticias.on('doubleTap', 'tbody tr', function () {
			viewDetailNoticia()
		})
	});
}

function FillImg(flg = "logo") {
	var imagen = GetUrl(
		"TablasSoporte/GetEmpresaImg?empresa=" + gEmpresa + "&flg=" + flg + "&timestamp=" + new Date().getTime()
	);
	if (flg == "firma") $("#imgFirma").attr("src", imagen);
	else if (flg == "quinta") $("#imgQuinta").attr("src", imagen);
	else if (flg == "agua") $("#imgSello").attr("src", imagen);
	else if (flg == "logo") $("#imgLogo").attr("src", imagen);
	else if (flg == "fdigital") $("#imgFirmaDigital").attr("src", imagen);
	else MsgBox("Dato inválido.", "warning", true);
}

function FillSectorPublico() {
	$.getJSON(
		GetUrl("TablasSoporte/GetEmpresaDataTab"),
		{ empresa: gEmpresa, flg: "sector" },
		function (source) {
			$("#containerSectorPublico input").val("");
			if (source.length < 1) {
				return;
			}
			var data = source.data[0];
			$("#txtSeccion").val(data.seccion);
			$("#txtSector").val(data.sector);
			$("#txtPliego").val(data.pliego);
			$("#txtUnidadEjecutora").val(data.unidad_ejecutora);
			$("#txtFuncion").val(data.funcion);
			$("#txtPrograma").val(data.programa);
			$("#txtSubPrograma").val(data.subprograma);
		}
	);
}

function FillEncuestas() {
	var table = $("#tblEncuestas");
	if ($.fn.dataTable.isDataTable(table)) {
		table.DataTable().clear();
		table.DataTable().destroy();
	}
	table.empty();

	$.getJSON(
		GetUrl("TablasSoporte/GetEmpresaDataTab"),
		{ empresa: '01', flg: "encuesta" },
		function (sourceTable) {
			tblEncuestas = table.DataTable({
				data: sourceTable.data,
				scrollCollapse: true,
				fixedHeader: true,
				select: {
					style: "single",
				},
				language: { url: "/js/language.js" },
				paging: false,
				columns: [
					{
						title: "Cód.",
						data: "cod_titulo",
						className: "text-left",
					},
					{
						title: "Titulo",
						data: "titulo",
						className: "text-left",
					},
					{
						title: "Estado",
						data: "estado",
						className: "text-left",
					},
				],
				columnDefs: [{ targets: "no-sort", orderable: false }],
				info: false,
				searching: false,
			});
			$("#tblEncuestas thead").addClass("thead-sky");
		}
	);
}

function FillCalificaciones() {
	var table = $("#tblCalificaciones");
	if ($.fn.dataTable.isDataTable(table)) {
		table.DataTable().clear();
		table.DataTable().destroy();
	}
	table.empty();

	$.getJSON(
		GetUrl("TablasSoporte/GetEmpresaEncuestaCalificacion"),
		{ empresa: gEmpresa },
		function (sourceTable) {
			tblCalificacion = table.DataTable({
				data: sourceTable.data,
				scrollCollapse: true,
				fixedHeader: true,
				select: {
					style: "single",
				},
				language: { url: "/js/language.js" },
				paging: false,
				columns: [
					{
						title: "Código",
						data: "cod_calificacion",
						className: "text-left",
					},
					{
						title: "Descripción Corta",
						data: "desc_corta_calificacion",
						className: "text-left",
					},
					{
						title: "Descripción Larga",
						data: "desc_larga_calificacion",
						className: "text-left",
					},
				],
				columnDefs: [{ targets: "no-sort", orderable: false }],
				info: false,
				searching: false,
			});
			$("#tblCalificaciones thead").addClass("thead-sky");
			tblCalificacion.on('click', 'tbody tr', function () {
				$('#tblCalificaciones tbody tr').removeClass('selected');
				$(this).addClass('selected');
			})
		}
	);
}

function FillPreguntas(pTitulo) {
	var table = $("#tblPreguntas");
	if ($.fn.dataTable.isDataTable(table)) {
		table.DataTable().clear();
		table.DataTable().destroy();
	}
	table.empty();

	$.getJSON(
		GetUrl("TablasSoporte/GetEmpresaEncuestaPreguntas"),
		{ empresa: '01', titulo: pTitulo },
		function (sourceTable) {
			tblPreguntas = table.DataTable({
				data: sourceTable.data,
				scrollCollapse: true,
				fixedHeader: true,
				select: {
					style: "single",
				},
				language: { url: "/js/language.js" },
				paging: false,
				columns: [
					{
						title: "Item",
						data: "cod_item",
						className: "text-left",
					},
					{
						title: "Descripción",
						data: "item",
						className: "text-left",
					},
					{
						title: "Estado",
						data: "estado",
						className: "text-left",
					},
				],
				columnDefs: [{ targets: "no-sort", orderable: false }],
				info: false,
				searching: false,
			});
			$("#tblPreguntas thead").addClass("thead-sky");
			tblPreguntas.on('click', 'tbody tr', function () {
				$('#tblPreguntas tbody tr').removeClass('selected');
				$(this).addClass('selected');
			})
		}
	);
}

function FillVideos() {
	var table = $("#tblVideos");
	if ($.fn.dataTable.isDataTable(table)) {
		table.DataTable().clear();
		table.DataTable().destroy();
	}
	table.empty();

	$.getJSON(GetUrl("TablasSoporte/GetEmpresaVideos"), { empresa: gEmpresa, tipoEnlace: $('#chkTipoEnlace').val() }, function (sourceTable) {
		tblVideos = table.DataTable({
			data: sourceTable.data,
			scrollCollapse: true,
			fixedHeader: true,
			select: {
				style: "single",
			},
			language: { url: "/js/language.js" },
			paging: false,
			columns: [
				// {
				//   title: "",
				//   data: "num_correlativo",
				//   className: "text-center d-inline d-md-inline d-lg-none",
				//   render: function (data, type, row) {
				//     return (
				//       '<button onclick="viewDetailVideo()" class="btn btn-sm btn-primary">Ver</button>'
				//     );
				//   },
				// },
				{
					title: "Item",
					data: "num_correlativo",
					className: "text-left",
				},
				{
					title: "Titulo",
					data: "titulo",
					className: "text-left",
				},
				{
					title: "Fecha",
					data: "fecha",
					className: "text-left",
				},
				{
					title: "Encuesta",
					data: "encuesta",
					className: "text-left",
				},
			],
			columnDefs: [{ targets: "no-sort", orderable: false }],
			info: false,
			searching: false,
		});
		$("#tblVideos thead").addClass("thead-sky");
	}
	);
}

function FillVideosEncuesta(corr) {
	var table = $("#tblEncuestaVideo");
	if ($.fn.dataTable.isDataTable(table)) {
		table.DataTable().clear();
		table.DataTable().destroy();
	}
	table.empty();

	$.getJSON(GetUrl("TablasSoporte/GetEmpresaVideosEncuesta"), { empresa: gEmpresa, corr: corr }, function (sourceTable) {
		tblEncuestaVideo = table.DataTable({
			data: sourceTable.data,
			scrollCollapse: true,
			fixedHeader: true,
			select: {
				style: "single",
			},
			language: { url: "/js/language.js" },
			paging: false,
			columns: [
				{
					title: "Cód. Video",
					data: "num_correlativo",
					className: "text-left",
				},
				{
					title: "Cód. Pregunta",
					data: "cod_pregunta",
					className: "text-left",
				},
				{
					title: "Pregunta",
					data: "pregunta",
					className: "text-left",
				},
			],
			columnDefs: [{ targets: "no-sort", orderable: false }],
			info: false,
			searching: false,
		});
		$("#tblEncuestaVideo thead").addClass("thead-sky");
		tblEncuestaVideo.on('click', 'tbody tr', function () {
			$('#tblEncuestaVideo tbody tr').removeClass('selected');
			$(this).addClass('selected');
		})
	}
	);
}

function FillImgNoticia(id) {
	var imagen = GetUrl("TablasSoporte/GetImageNoticia?empresa=" + gEmpresa + "&id=" + id + "&timestamp=" + new Date().getTime());
	$("#imgNoticia").attr("src", imagen);
}

function FillPersonalEnvioEmail() {
	containerEmail.maskContainer('cargando ...');
	var table = $("#tblEnvioEmail");
	if ($.fn.dataTable.isDataTable(table)) {
		table.DataTable().clear();
		table.DataTable().destroy();
	}
	table.empty();

	$.getJSON(
		GetUrl("TablasSoporte/GetPersonalEnvioEmail"),
		{
			empresa: gEmpresa,
			planilla: $('#selectPlanilla').val(),
			proyecto: $('#selectProyecto').val(),
			cc: $('#selectCC').val(),
			localidad: $('#selectLocalidad').val(),
			un: $('#selectUN').val(),
			area: $('#selectArea').val(),
			estado: 'A'
		},
		function (sourceTable) {
			tblEnvioEmail = table.DataTable({
				data: sourceTable.data,
				scrollCollapse: true,
				fixedHeader: true,
				select: {
					style: "single",
				},
				language: { url: "/js/language.js" },
				paging: false,
				columns: [
					{
						title: "Código",
						data: "codigo",
						className: "text-left",
					},
					{
						title: "Trabajador",
						data: "trabajador",
						className: "text-left",
					},
					{
						title: "",
						data: "cod_personal",
						className: "text-left",
						render: function (data, type, row) {
							return (
								'<div class="custom-control custom-checkbox"><input type="checkbox" ' + (row.email.length <= 0 ? 'disabled' : '') + ' value="' + row.email + '" data-personal="' + data + '" class="custom-control-input chkSel" id="chkEmail' + row.codigo + '"><label class="custom-control-label" for="chkEmail' + row.codigo + '"></label></div>'
							);
						},
					},
					{
						title: "E-Mail",
						data: "email",
						className: "text-left",
					},
				],
				columnDefs: [{ targets: "no-sort", orderable: false }],
				info: false,
				searching: true,
			});
			$("#tblEnvioEmail thead").addClass("thead-sky");
		}
	);
	containerEmail.unmaskContainer();
}

function PostDatosGenerales() {

	let vflg = 'U'
	if ($('#txtCodigo').val() == '' && !$('#tblEmpresas tr').hasClass('selected')) {
		vflg = 'I';
	}

	var obj = {
		flg: vflg,

		flgAgente: $("#chkAgente").prop('checked') ? 'S' : 'N',
		flgBoletaCopia: $("#chkBoletaCopia").prop('checked') ? 'S' : 'N',
		flgCtsCopia: $("#chkCTSCopia").prop('checked') ? 'S' : 'N',
		flgUtilidadCopia: $("#chkUtilidadesCopia").prop('checked') ? 'S' : 'N',
		flgQuintaMostrarCreditos: $("#chkAFPSNP").prop('checked') ? 'S' : 'N',
		flgBoletaDatosEmpresa: $("#chkBoletaDatosEmpresa").prop('checked') ? 'S' : 'N',
		flgLiquidacionDatosEmpresa: $("#chkLiquidacionDatosEmpresa").prop('checked') ? 'S' : 'N',

		codEmpresa: $("#txtCodigo").val(),
		flgActiva: $("#selectEstado").val(),
		nombre: $("#txtEmpresa").val(),
		siglas: $("#txtSiglas").val(),
		razonSocial: $("#txtRazonSocial").val(),
		ruc: $("#txtRUC").val(),
		codVia: $("#selectVia").val(),
		direccion: $("#txtDirecion").val(),
		numero: $("#txtNDireccion").val(),
		interior: $("#txtIntDireccion").val(),
		codZona: $("#selectZona").val(),
		zona: $("#txtZona").val(),
		codUbigeo: $("#selectDistrito").val(),
		referencia: $("#txtReferencia").val(),
		representante: $("#txtRepresentante").val(),
		docRepresentante: $("#txtDocRepresentante").val(),
		reprensentanteSexo: $("#selectSexo").val(),
		codCargo: $("#selectCargoRepresentante").val(),
		jefeRrhh: $("#txtJefeDeRRHH").val(),
		jefeRrhhDoc: $("#txtDocJefeDeRRHH").val(),
		jefeRrhhCargo: $("#selectCargojefeDeRRHH").val(),
		partidaRegistral: $("#txtNroParRegistral").val(),
		ciudadPartida: $("#txtCiuParRegistral").val(),
		registroPatronal: $("#txtRegPatr").val(),
		telefonos: $("#txtTelefonos").val(),
		fax: $("#txtFax").val(),
		flgRegimen: $("#selectRegimen").val(),
		email: $("#txtEmail").val(),
		www: $("#txtWeb").val(),
		slogan: $("#txtSlogan").val(),
		codMonedaBase: $("#selectMonedaBase").val(),
		codMonedaExtranjera: $("#selectMonedaExtranjera").val(),
		flgTipoCambio: $("#selectPTipoDeCambio").val(),
		tipoPers: $("#selectTipoPersonal").val(),
		flgBoleta: $("#selectBoleta").val(),
		flgCts: $("#selectCTS").val(),
		flgUtilidad: $("#selectUtilidades").val(),
		flgLiquidacion: $("#selectLiquidacion").val(),
		flgPapeletaExcepciones: $("#selectExcepciones").val(),
		flgCtaCte: $("#selectCtaCte").val(),
		flgQuinta: $("#selectQuintaAnual").val(),
		flgAfp: $("#selectAFPSNP").val(),
		papeletaVacAuditoriaArea: $("#txtPapeletaAuditoria").val(),
		papeletaVacAuditoriaVersion: $("#txtVersion").val(),
		cadena1: $("#txtCadena1").val(),
		flgPlanillaOficialGral: $("#selectPlanillaOf").val(),
		codSistContable: $("#selectSistemaContabilidad").val(),
		flgNucleoEmpresarial: $("#selectNucleoEmpresarial").val(),
		codEmpresaNucleo: $("#selectEmpresaNucleoEmpresarial").val(),
	};
	UIAjax.invokeFunctionAsync('TablasSoporte', 'PostEmpresaDatosGenerales', { data: obj }).then(function (data) {
		if (data.respuesta == 1) {
			FillEmpresas();
			if (vflg == 'I') {
				$('#txtCodEmpresaOrigen').val(data.mensaje);
				FillEmpresasClonar();
				$('#modalInicializarEmpresa').modal({ show: true });
			} else {
				MsgBox("success");
			}
		} else {
			MsgBox(data.mensaje, "warning", false);
		}
	});
}

function PostDatosAdicionales() {
    var obj = {
        flgEncriptaBoleta: $("#chkPDF").prop('checked') ? 'S' : 'N',
        emailPassReq: $("#chkReqUsuPass").prop('checked') ? 'S' : 'N',
        flgHtml: $("#chkContenidoHTML").prop('checked') ? 'S' : 'N',

        codEmpresa: gEmpresa,
        dirPDF: $("#txtPDF").val(),
        colorFondoWeb: $("#txtColorFondoWeb").val(),
        dirFirmaBoleta: $("#txtArchivoFirmaBoleta").val(),
        passwordFirmaBoleta: $("#txtPasswordFirmaBoleta").val(),
        posicionXFirmaBoleta: $("#txtPosicionXFirmaBoleta").val(),
        posicionYFirmaBoleta: $("#txtPosicionYFirmaBoleta").val(),
        dirFirmaLiquidacion: $("#txtArchivoFirmaLiquidacion").val(),
        passwordFirmaLiquidacion: $("#txtPasswordFirmaLiquidacion").val(),
        posicionXFirmaLiquidacion: $("#txtPosicionXFirmaLiquidacion").val(),
        posicionYFirmaLiquidacion: $("#txtPosicionYFirmaLiquidacion").val(),
        dirFirmaCts: $("#txtArchivoFirmaCTS").val(),
        passwordFirmaCts: $("#txtPasswordFirmaCTS").val(),
        posicionXFirmaCts: $("#txtPosicionXFirmaCTS").val(),
        posicionYFirmaCts: $("#txtPosicionYFirmaCTS").val(),
        dirFirmaUtilidad: $("#txtArchivoFirmaUtilidad").val(),
        passwordFirmaUtilidad: $("#txtPasswordFirmaUtilidad").val(),
        posicionXFirmaUtilidad: $("#txtPosicionXFirmaUtilidad").val(),
        posicionYFirmaUtilidad: $("#txtPosicionYFirmaUtilidad").val(),
        dirFirmaQuinta: $("#txtArchivoFirmaQuinta").val(),
        passwordFirmaQuinta: $("#txtPasswordFirmaQuinta").val(),
        posicionXFirmaQuinta: $("#txtPosicionXFirmaQuinta").val(),
        posicionYFirmaQuinta: $("#txtPosicionYFirmaQuinta").val(),
        dirFirmaContrato: $("#txtArchivoFirmaContrato").val(),
        passwordFirmaContrato: $("#txtPasswordFirmaContrato").val(),
        posicionXFirmaContrato: $("#txtPosicionXFirmaContrato").val(),
        posicionYFirmaContrato: $("#txtPosicionYFirmaContrato").val(),
        dirFirmaDocsConfig: $("#txtArchivoFirmaDocsConfig").val(),
        passwordFirmaDocsConfig: $("#txtPasswordFirmaDocsConfig").val(),
        posicionXFirmaDocsConfig: $("#txtPosicionXFirmaDocsConfig").val(),
        posicionYFirmaDocsConfig: $("#txtPosicionYFirmaDocsConfig").val(),
        smtpServidor: $("#txtServidor").val(),
        smtpPuerto: $("#txtPuerto").val(),
        smtpEncript: $("#selectTipoSMTP").val(),
        smtpUsuario: $("#txtEmailDA").val(),
        smtpPassword: $("#txtPassword").val(),
        emailRemitente: $("#txtNombreDeRemitente").val(),
        flgEnvioMail: $("#selectCorreoAEnviar").val(),
        cabeceraMail: $("#txtCabeceraEmail").val(),
        redMail: $("#numRojo").val(),
        greenMail: $("#numVerde").val(),
        blueMail: $("#numAzul").val(),
        emailCuerpo: $("#txtEmailCuerpo").val(),
    };
    UIAjax.runFunctionAsync('TablasSoporte', 'PostEmpresaDatosAdicionales', { data: obj });
}

function PostSectorPublico() {
	var obj = {
		codEmpresa: gEmpresa,
		seccion: $("#txtSeccion").val(),
		sector: $("#txtSector").val(),
		pliego: $("#txtPliego").val(),
		unidadEjecutora: $("#txtUnidadEjecutora").val(),
		funcion: $("#txtFuncion").val(),
		programa: $("#txtPrograma").val(),
		subPrograma: $("#txtSubPrograma").val()
	};
	UIAjax.runFunctionAsync('TablasSoporte', 'PostEmpresaSectorPublico', { data: obj });
}

function PostCuentaBancaria() {
	var obj = {
		codEmpresa: gEmpresa,
		codBanco: $("#selectBancoNew").val(),
		codCuentaBanco: $("#txtCodCuentaNew").val(),
		nroCuenta: $("#txtNroCuentaNew").val(),
		codMoneda: $("#selectMonedaNew").val(),
	};

	UIAjax.runFunctionAsync('TablasSoporte', 'PostEmpresaCuentaBancaria', { data: obj }, function () {
		FillCuentasBancarias();
		$('#modalCuentaBancaria').modal('hide');
	});
}

function DeleteDocInstitucional(items) {
	var obj = {
		codEmpresa: gEmpresa,
		items: items,
	};
	UIAjax.runFunctionAsync('TablasSoporte', 'DeleteEmpresaDocInstitucional', obj, function () {
		FillDocumentos();
	});
};

function DeleteNoticias(corr) {
	var obj = {
		codEmpresa: gEmpresa,
		numCorrelativo: corr,
	};
	UIAjax.runFunctionAsync('TablasSoporte', 'DeleteEmpresaNoticias', { data: obj }, function () {
		FillNoticias();
	});
}

function PostInicializarEmpresa(flg) {

	var xRows = tblEmpresasClonar.rows('.selected').data();
	if (xRows.length != 0x0001) {
		MsgBox("Seleccione la empresa a clonar.", "warning", true);
		return;
	}

	let empresaDestino = xRows[0].codigo;
	let empresaOrigen = $('#txtCodEmpresaOrigen').val()

	let vtitle = 'No clonar empresa';
	let vmessage = '¿Está seguro que no desea clonar conceptos y parametros de alguna empresa?';
	if (flg == 'A') {
		vtitle = 'Clonar empresa';
		vmessage = '¿Está seguro que desea clonar conceptos y parametros de la empresa seleccionada?';
	}

	MsgBox(vmessage, "question", false, (flg == 'A' ? false : true), function () {
		var params = {
			origen: empresaOrigen,
			destino: empresaDestino,
			flg: flg
		}
		UIAjax.runFunctionAsync('TablasSoporte', 'PostInicializarEmpresa', params, null, null, function () {
			$('#modalInicializarEmpresa').modal('hide');
		});
	});
}

function PostEmpresaLogos() {
	var input = document.getElementById("filelogoEmpresa");
	var files = input.files;
	var flg = $('#filelogoEmpresa').data('flg')
	var formData = new FormData();
	formData.append("codEmpresa", gEmpresa);
	formData.append("flg", flg);
	formData.append("file", (files.length > 0 ? files[0] : null));

	$.ajax({
		url: GetUrl('TablasSoporte/PostEmpresaLogos'),
		data: formData,
		processData: false,
		contentType: false,
		type: "POST",
		success: function (data) {
			if (data.respuesta == 1) {
				MsgBox("success");
				$('#filelogoEmpresa').val('')
				FillImg(flg);
			} else {
				MsgBox(data.mensaje, "warning", false);
			}
		},
		error: function (data) {
			MsgBox("error");
			console.log(data);
		}
	});
}

function SendMail(emails) {
	containerEmail.maskContainer('cargando ...');
	var params = {
		codEmpresa: gEmpresa,
		list: emails
	}
	UIAjax.runFunctionAsync('TablasSoporte', 'PostSendMail', params, null, null, function () {
		$('#modalInicializarEmpresa').modal('hide');
		containerEmail.unmaskContainer();
	});
}

function SaveNewRowEncuestaDoc(pthis) {
	if (tblEmpresas.rows('.selected').data().length != 0x0001) {
		MsgBox("Seleccione una empresa.", "warning", true);
		return;
	}

	let table = $(pthis).closest('table')[0].id
	let tr = $(pthis).closest('tr')
	let valor1 = tr.find('#input1').val()
	let valor2 = tr.find('#input2').val()
	let nameMethod = "";
	var params;
	var xRows = tblEncuestas.rows('.selected').data()

	if (table == "tblEncuestas") {
		nameMethod = "PostEmpresaEncuesta"
		valor2 = tr.find('#input2').prop('checked') ? 'S' : 'N';
		params = {
			codEmpresa: gEmpresa,
			titulo: valor1,
			estado: valor2
		}
	}
	else if (table == "tblCalificaciones") {
		nameMethod = "PostEmpresaCalificaciones"
		params = {
			codEmpresa: gEmpresa,
			descCorta: valor1,
			descLarga: valor2
		}
	}
	else if (table == "tblPreguntas") {
		nameMethod = "PostEmpresaPregunta"
		if (xRows.length != 0x0001) {
			MsgBox("Seleccione una encuesta.", "warning", true);
			return;
		}
		valor2 = tr.find('#input2').prop('checked') ? 'S' : 'N';
		params = {
			codEmpresa: gEmpresa,
			titulo: xRows[0].cod_titulo,
			item: valor1,
			estado: valor2
		}
	}
	UIAjax.runFunctionAsync('TablasSoporte', nameMethod, params, function () {
		if (table == 'tblEncuestas') FillEncuestas()
		else if (table == 'tblCalificaciones') FillCalificaciones()
		else if (table == 'tblPreguntas') FillPreguntas(xRows[0].cod_titulo)
	});
}

function SaveNewRowEncuestaVideo(pthis) {
	let tr = $(pthis).closest('tr')
	let valor1 = tr.find('#input1').val()
	let xRows = tblVideos.rows('.selected').data();
	let params = {
		flg: "I",
		codEmpresa: gEmpresa,
		corr: xRows[0].num_correlativo,
		pregunta: valor1
	}
	UIAjax.runFunctionAsync('TablasSoporte', 'PostEmpresaVideoEncuesta', params, function () {
		FillVideosEncuesta(xRows[0].num_correlativo)
	});
}

function DeleteEncuesta(flg) {

	var vText;
	if (flg == "encuesta") {
		var rows = tblEncuestas.rows('.selected').data();
		if (rows.length <= 0) {
			MsgBox("Seleccione una encuesta.", "warning", true);
			return;
		}
		var params = {
			codEmpresa: '01',
			codTitulo: rows[0].cod_titulo,
			item: '',
			flg: flg
		}
		vText = 'También se eliminarán las preguntas asignadas a está encuesta.'
	}
	else if (flg == "calificacion") {
		var rows = tblCalificacion.rows('.selected').data();
		if (rows.length <= 0) {
			MsgBox("Seleccione una calificación.", "warning", true);
			return;
		}
		var params = {
			codEmpresa: '01',
			codTitulo: rows[0].cod_calificacion,
			item: '',
			flg: flg
		}
		vText = 'Se eliminarán la calificación.'
	}
	else if (flg == "pregunta") {
		var rowsEncuesta = tblEncuestas.rows('.selected').data();
		if (rowsEncuesta.length <= 0) {
			MsgBox("Seleccione una encuesta.", "warning", true);
			return;
		}
		var rowsItem = tblPreguntas.rows('.selected').data();
		if (rowsItem.length <= 0) {
			MsgBox("Seleccione una pregunta.", "warning", true);
			return;
		}
		var params = {
			codEmpresa: '01',
			codTitulo: rowsEncuesta[0].cod_titulo,
			item: rowsItem[0].cod_item,
			flg: flg
		}
		vText = 'Se eliminarán la pregunta.'
	}

	MsgBox(vText, "question", false, true, function () {
		UIAjax.runFunctionAsync('TablasSoporte', 'DeleteEmpresaEncuesta', params, function () {
			if (flg == 'encuesta') {
				var xRows = tblEncuestas.rows('.selected').data()
				FillEncuestas()
				FillPreguntas(xRows[0].cod_titulo)
			}
			else if (flg == 'calificacion') FillCalificaciones()
			else if (flg == 'pregunta') {
				var xRows = tblEncuestas.rows('.selected').data()
				FillPreguntas(xRows[0].cod_titulo)
			}
		});
	});
}

function PostVideo() {
	let urlNew = '';
	if ($('#chkTipoEnlace').val() == 'V') {
		var urlOrigin = $('#txtUrlVideoNew').val();
		if ($('#txtTituloVideoNew').val() == '') {
			MsgBox("Ingrese un título.", "warning", true);
			return;
		}
		if ($('#txtDetalleVideoNew').val() == '') {
			MsgBox("Ingrese un detalle.", "warning", true);
			return;
		}
		if ($('#txtFechaVideoNew').val() == '') {
			MsgBox("Ingrese una fecha.", "warning", true);
			return;
		}
		if (urlOrigin.lastIndexOf("https://www.youtube.com/") < 0 && (urlOrigin.lastIndexOf("embed/") < 0 && urlOrigin.lastIndexOf("watch?v=") < 0)) {
			MsgBox("URL de video incorrecta.", "warning", true);
			return;
		}

		urlNew = urlOrigin;
		if (urlOrigin.lastIndexOf("watch?v=") > 0) {
			urlNew = urlOrigin.replace("watch?v=", "embed/");
		}
	}
	else {
		urlNew = $('#txtUrlVideoNew').val();
	}

	var params = {
		flg: 'I',
		codEmpresa: gEmpresa,
		corr: 0,
		titulo: $('#txtTituloVideoNew').val(),
		detalle: $('#txtDetalleVideoNew').val(),
		url: urlNew,
		fecha: $('#txtFechaVideoNew').val(),
		tipoEnlace: $('#chkTipoEnlace').val()
	}
	UIAjax.runFunctionAsync('TablasSoporte', 'PostEmpresaVideo', params, function () {
		FillVideos();
		$('#modalVideoInstitucional').modal('hide')
	});
}

function DeleteVideo() {
	var xRows = tblVideos.rows('.selected').data()
	if (xRows.length <= 0) {
		UIMensaje.REGISTRO();
		return;
	}
	var params = {
		flg: 'I',
		codEmpresa: gEmpresa,
		corr: xRows[0].num_correlativo,
		tipoEnlace: $('#chkTipoEnlace').val()
	}
	MsgBox("Se eliminará el registro.", "question", false, true, function () {
		UIAjax.runFunctionAsync('TablasSoporte', 'DeleteEmpresaVideo', params, function () {
			FillVideos();
			FillVideosEncuesta(-1);
		});
	});
}

function DeleteVideoEncuesta() {
	var xRows = tblEncuestaVideo.rows('.selected').data()
	if (xRows.length <= 0) {
		MsgBox("Seleccione una pregunta.", "warning", true);
		return;
	}
	var params = {
		codEmpresa: gEmpresa,
		corr: xRows[0].num_correlativo,
		codPregunta: xRows[0].cod_pregunta
	}
	UIAjax.runFunctionAsync('TablasSoporte', 'DeleteEmpresaVideoEncuesta', params, function () {
		FillVideos();
	});
}

//Tabla Usuarios
function FillUsuarios() {
	var table = $("#tblUsuarios");
	if ($.fn.dataTable.isDataTable(table)) {
		table.DataTable().clear();
		table.DataTable().destroy();
	}
	table.empty();

	$.getJSON(GetUrl("TablasSoporte/GetUsuarios"), { estado: 'AIR' }, function (sourceTable) {
		tblEmpresas = table.DataTable({
			data: sourceTable.data,
			scrollCollapse: true,
			select: {
				style: "single",
			},
			language: { url: "/js/language.js" },
			paging: false,
			columns: [
				{
					title: "Código",
					data: "codigo",
					className: "text-center",
				},
				{
					title: "Usuario",
					data: "nombre",
					className: "text-center",
				},
				{
					title: "Prefijo Active Directory",
					data: "usuario_ad",
					className: "text-center",
				},
				{
					title: "Empresas",
					data: "flg_empresa",
					className: "text-center",
					render: function (data, type, row) {
						return (
							'<div class="custom-control custom-checkbox"><input type="checkbox" ' + (data == "S" ? "checked" : "") + ' class="custom-control-input" id="chkEmpresa' + row.codigo + '"><label class="custom-control-label" for="chkEmpresa' + row.codigo + '"></label></div>'
						);
					},
				},
				{
					title: "Tipos Planillas",
					data: "flg_tipo_planilla",
					className: "text-center",
					render: function (data, type, row) {
						return (
							'<div class="custom-control custom-checkbox"><input type="checkbox" ' + (data == "S" ? "checked" : "") + '  class="custom-control-input" id="chkPlanilla' + row.codigo + '"><label class="custom-control-label" for="chkPlanilla' + row.codigo + '"></label></div>'
						);
					},
				},
				{
					title: "Localidades",
					data: "flg_localidad",
					className: "text-center",
					render: function (data, type, row) {
						return (
							'<div class="custom-control custom-checkbox"><input type="checkbox" ' + (data == "S" ? "checked" : "") + '  class="custom-control-input" id="chkLocalidad' + row.codigo + '"><label class="custom-control-label" for="chkLocalidad' + row.codigo + '"></label></div>'
						);
					},
				},
				{
					title: "Centros Costo",
					data: "flg_centro_costo",
					className: "text-center",
					render: function (data, type, row) {
						return (
							'<div class="custom-control custom-checkbox"><input type="checkbox" ' + (data == "S" ? "checked" : "") + '  class="custom-control-input" id="chkCC' + row.codigo + '"><label class="custom-control-label" for="chkCC' + row.codigo + '"></label></div>'
						);
					},
				},
				{
					title: "Áreas",
					data: "flg_area",
					className: "text-center",
					render: function (data, type, row) {
						return (
							'<div class="custom-control custom-checkbox"><input type="checkbox" ' + (data == "S" ? "checked" : "") + '  class="custom-control-input" id="chkArea' + row.codigo + '"><label class="custom-control-label" for="chkArea' + row.codigo + '"></label></div>'
						);
					},
				},
				{
					title: "Unidades Negocio",
					data: "flg_unidad_negocio",
					className: "text-center",
					render: function (data, type, row) {
						return (
							'<div class="custom-control custom-checkbox"><input type="checkbox" ' + (data == "S" ? "checked" : "") + '  class="custom-control-input" id="chkUN' + row.codigo + '"><label class="custom-control-label" for="chkUN' + row.codigo + '"></label></div>'
						);
					},
				},
				{
					title: "Proyectos",
					data: "flg_proyecto",
					className: "text-center",
					render: function (data, type, row) {
						return (
							'<div class="custom-control custom-checkbox"><input type="checkbox" ' + (data == "S" ? "checked" : "") + '  class="custom-control-input" id="chkProyecto' + row.codigo + '"><label class="custom-control-label" for="chkProyecto' + row.codigo + '"></label></div>'
						);
					},
				},
				{
					title: "Grupo Trabajo",
					data: "flg_grupo_trabajo",
					className: "text-center",
					render: function (data, type, row) {
						return (
							'<div class="custom-control custom-checkbox"><input type="checkbox" ' + (data == "S" ? "checked" : "") + ' class="custom-control-input" id="chkGrupo' + row.codigo + '"><label class="custom-control-label" for="chkGrupo' + row.codigo + '"></label></div>'
						);
					},
				},
				{
					title: "Alertas",
					data: "flg_alertas",
					className: "text-center",
					render: function (data, type, row) {
						return (
							'<div class="custom-control custom-checkbox"><input type="checkbox" ' + (data == "S" ? "checked" : "") + ' class="custom-control-input" id="chkAlerta' + row.codigo + '"><label class="custom-control-label" for="chkAlerta' + row.codigo + '"></label></div>'
						);
					},
				},
				{
					title: "Email",
					data: "email",
					className: "text-left",
				},
				{
					title: "Estado",
					data: "estado",
					className: "text-center",
				},
			],
			columnDefs: [{ targets: "no-sort", orderable: false }],
			info: false,
			searching: false,
		});
		$("#tblUsuarios thead").addClass("thead-sky");
	});
}

async function createDataTable(obj, customDatatable = { scrollCollapse: true, paging: false, info: false, searching: false }) {
	var table = $("#" + obj.tableName);
	if ($.fn.dataTable.isDataTable(table)) {
		table.DataTable().clear();
		table.DataTable().destroy();
	}
	table.empty();

	await $.getJSON(GetUrl(obj.urlApi), obj.params, function (sourceTable) {
		if (obj.flg == 'usuarios') {
			sourceTable.data = sourceTable.data.filter(x => x.codigo != obj.usuario)
		}
		table.DataTable({
			data: sourceTable.data,
			scrollCollapse: (customDatatable.scrollCollapse === undefined ? true : customDatatable.scrollCollapse),
			select: {
				style: "single",
			},
			language: { url: "/js/language.js" },
			paging: (customDatatable.paging === undefined ? false : customDatatable.paging),
			columns: obj.columns,
			columnDefs: [{ targets: "no-sort", orderable: false }],
			info: (customDatatable.info === undefined ? false : customDatatable.info),
			searching: (customDatatable.searching === undefined ? false : customDatatable.searching),
		});
		$("#" + obj.tableName + " thead").addClass("thead-sky");
		$("#" + obj.tableName).addClass("table-compress");
	});
}
