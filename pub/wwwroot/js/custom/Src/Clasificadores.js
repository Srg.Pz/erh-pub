﻿
//#region PROYECTOS

function ProyectosList() {

    tableProyectos = $('#tbProyectos').DataTable({
        "ajax": portal + '/TablasSoporte/GetClasificadoresProyectos/'
        , "initComplete": function (settings, json) {
            //$(chbTodos).appendTo("#tbTrabajadores_filter label");
        }
        , bDestroy: true
        , dom: 'Bfrtip'
        , buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5'
        ]
        , "columns": [
            { "data": "cod_proyecto" },
            { "data": "nombre" },
            { "data": "tipo_proyecto" },
            { "data": "estado" }
        ]
    });
}

function ProyectosLoad(CodProyecto) {
    var jsonParam = {
        Cod_proyecto: CodProyecto,
    };
    $.ajax({
        type: 'GET',
        url: GetUrl("/TablasSoporte/GetClasificadoresProyecto"),
        dataType: 'json',
        data: jsonParam,
        async: false,
        success: function (data) {

            var item = data.data[0];

            var cp = parseInt(item.cod_proyecto);
            if (cp == -1) {
                $('#btnProyectoGuardar').hide();
            }
            else {
                $('#btnProyectoGuardar').show();
            }
            
            $('#txtProyectoCodigo').val(item.cod_proyecto);
            $('#txtProyectoNombre').val(item.nombre);
            $('#selectProyectoTipo').val(item.tipo_proyecto);
            $('#txtProyectoDireccion').val(item.direccion);
            $('#selProyectoDepartamento').val(item.cod_ubigeo.substring(0, 2));
            $('#selProyectoDepartamento').change();
            $('#selectProyectoProvincia').val(item.cod_ubigeo.substring(0, 4));
            $('#selectProyectoProvincia').change();
            $('#selectProyectoDistrito').val(item.cod_ubigeo.trim());
            $('#txtProyectoTelefono').val(item.telefonos);
            $('#txtProyectoFax').val(item.fax);
            $('#txtProyectoNota').val(item.nota);
            $('#txtProyectoGasto').val(item.gasto_adm);
            $('#chkProyectoProvCTS').prop('checked', item.flg_prov_cts == 'S');
            $('#chkProyectoProvVacaciones').prop('checked', item.flg_prov_vac == 'S');
            $('#chkProyectoProvGratificacion').prop('checked', item.flg_prov_gra == 'S');
            $('#chkProyectoProvEsSalud').prop('checked', item.flg_prov_ess == 'S');
            $('#txtProyectoSCTRPension').val(item.sctr_pension);
            $('#txtProyectoSCTRSalud').val(item.sctr_salud);
            $('#txtProyectoFechaInicio').val(item.fecha_inicio != '01/01/1900' ? item.fecha_inicio : '');
            $('#txtProyectoFechaFinal').val(item.fecha_final != '01/01/1900' ? item.fecha_final : '');
            $('#txtProyectoDescripcion').val(item.descripcion);
        }
    });
};

function ProyectosValidar() {

    var Cod_proyecto = parseInt($('#txtProyectoCodigo').val());
    var Nombre = $('#txtProyectoNombre').val();

    if (isNaN(Cod_proyecto) || Cod_proyecto <= 0) {

        $('#txtProyectoCodigo').focus();
        return 'Código de proyecto inválido';
    }

    if (Nombre == '') {

        $('#txtProyectoNombre').focus();
        return 'Nombre de proyecto vacío';
    }

    return '';
}

function ProyectosSave() {

    var msgError = ProyectosValidar();

    if (msgError != '')
    {
        MsgBox(msgError, "warning", false);
        return;
    }

    var jsonData = {
        Accion: accion,
        Cod_proyecto: $('#txtProyectoCodigo').val(),
        Nombre: $('#txtProyectoNombre').val(),
        Tipo_proyecto: $('#selectProyectoTipo').val(),
        Estado: $('#selectProyectoEstado').val(),
        Direccion: $('#txtProyectoDireccion').val(),
        cod_ubigeo: $('#selectProyectoDistrito').val(),
        Telefonos: $('#txtProyectoTelefono').val(),
        Fax: $('#txtProyectoFax').val(),
        Nota: $('#txtProyectoNota').val(),
        gasto_adm: $('#txtProyectoGasto').val().replace(',', ''),
        flg_prov_cts: $('#chkProyectoProvCTS').prop('checked') ? 'S' : 'N',
        flg_prov_vac: $('#chkProyectoProvVacaciones').prop('checked') ? 'S' : 'N',
        flg_prov_gra: $('#chkProyectoProvGratificacion').prop('checked') ? 'S' : 'N',
        flg_prov_ess: $('#chkProyectoProvEsSalud').prop('checked') ? 'S' : 'N',
        sctr_pension: $('#txtProyectoSCTRPension').val().replace(',', ''),
        sctr_salud: $('#txtProyectoSCTRSalud').val().replace(',', ''),
        Fecha_inicio: $('#txtProyectoFechaInicio').val(),
        Fecha_final: $('#txtProyectoFechaFinal').val(),
        Descripcion: $('#txtProyectoDescripcion').val()
    };
    UIAjax.runFunctionAsync('TablasSoporte', 'SetClasificadoresProyecto', jsonData, function () {
        $('#modalProyectoDetalle').modal('hide');
        ProyectosList();
    });
}

function ProyectosCleanControls() {

    $('#txtProyectoCodigo').val('');
    $('#txtProyectoNombre').val('');
    $('#selectProyectoTipo').val('1');
    $('#selectProyectoEstado').val('E');
    $('#txtProyectoDireccion').val('');
    $('#txtProyectoTelefono').val('');
    $('#txtProyectoFax').val('');
    $('#txtProyectoNota').val('');
    $('#txtProyectoGasto').val('');

    $('#chkProyectoProvCTS').prop('checked', false);
    $('#chkProyectoProvVacaciones').prop('checked', false);
    $('#chkProyectoProvGratificacion').prop('checked', false);
    $('#chkProyectoProvEsSalud').prop('checked', false);

    $('#txtProyectoSCTRPension').val('');
    $('#txtProyectoSCTRSalud').val('');
    $('#txtProyectoFechaInicio').val('');
    $('#txtProyectoFechaFinal').val('');
    $('#txtProyectoDescripcion').val('');

    $('#selProyectoDepartamento').val('14');
    $('#selProyectoDepartamento').change();
    $('#selectProyectoProvincia').val('1401');
    $('#selectProyectoProvincia').change();
    $('#selectProyectoDistrito').val('140101');
}

//#endregion


//#region CENTROS DE COSTOS

var CentrosdeCosto_ProyectosListar = function CentrosdeCosto_ProyectosListar() {
    
    $.ajax({
        type: "GET",
        url: portal + '/TablasSoporte/GetClasificadoresProyectos',
        data: JSON.stringify({}),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function success(data) {

            var obj = data.data;
            dtPeriodos = obj;
            var container = $("#selectCentrosdeCostoProyecto");
            container.empty();
            container.append("<option value='-999'>[Master]</option>");

            $.each(obj, function (index, value) {
                var html = "<option value='" + value.cod_proyecto + "'>" + value.nombre + "</option>";
                container.append(html);
            });

        },
        error: function error(response) { }
    });
};

function CentrosdeCostoMasterListar() {

    dtCentrosdeCostos1 = $('#dtCentrosdeCostos1').DataTable({
        "ajax": portal + '/TablasSoporte/GetClasificadoresCentrosdeCostosMaster/'
        , "initComplete": function (settings, json) {
            //$(chbTodos).appendTo("#tbTrabajadores_filter label");
        }
        , bDestroy: true
        , dom: 'Bfrtip'
        , buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5'
        ]
        , "language": {
            "url": '/js/language.js'
        }
        , "columns": [
            
            { "data": "cod_centro_costo" },
            { "data": "descripcion" },
            { "data": "cadena1" },
            { "data": "cadena2" },
            {
                data: "cod_empresa",
                render: function (data, type, row) {
                    if (type === 'display') {
                        return '<input type="checkbox" class="editor-active">';
                    }
                    return data;
                },
                className: "dt-body-center"
            },
        ]

    });
}

function CentrosdeCostoxProyectoListar(Cod_Proyecto) {

    dtCentrosdeCostos1 = $('#dtCentrosdeCostos1').DataTable({
        "ajax": portal + '/TablasSoporte/GetClasificadoresCentrosdeCostosxProyecto?Cod_Proyecto=' + Cod_Proyecto
        , "initComplete": function (settings, json) {
            //$(chbTodos).appendTo("#tbTrabajadores_filter label");
        }
        , bDestroy: true
        , dom: 'Bfrtip'
        , buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5'
        ]
        , "language": {
            "url": '/js/language.js'
        }
        , "columns": [

            { "data": "cod_centro_costo" },
            { "data": "descripcion" },
            { "data": "cadena1" },
            { "data": "cadena2" },
            {
                data: "cod_empresa",
                render: function (data, type, row) {
                    if (type === 'display') {
                        return '<input type="checkbox" class="editor-active" name="chbCC1">';
                    }
                    return data;
                },
                className: "dt-body-center"
            },
        ]

    });
}

function CentrosdeCostoxNoEnProyectoListar(Cod_Proyecto) {

    dtCentrosdeCostos2 = $('#dtCentrosdeCostos2').DataTable({
        "ajax": portal + '/TablasSoporte/GetClasificadoresCentrosdeCostosNoEnProyecto?Cod_Proyecto=' + Cod_Proyecto
        , "initComplete": function (settings, json) {
            //$(chbTodos).appendTo("#tbTrabajadores_filter label");
        }
        , bDestroy: true
        , dom: 'Bfrtip'
        , buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5'
        ]
        , "language": {
            "url": '/js/language.js'
        }
        , "columns": [

            { "data": "cod_centro_costo" },
            { "data": "descripcion" },
            {
                data: "sel",
                render: function (data, type, row) {
                    if (type === 'display') {
                        return '<input type="checkbox" class="editor-active" name="chbCC2">';
                    }
                    return data;
                },
                className: "dt-body-center"
            },
        ]

    });
}

function CentrosdeCostoSave(_Cod_proyecto, _Cod_centro_costo) {
    var jsonData = {
        Cod_proyecto: _Cod_proyecto,
        Cod_centro_costo: _Cod_centro_costo
    };
    UIAjax.runFunctionAsync('TablasSoporte', 'SetCentrodeCostoEnProyecto', jsonData);
}

function CentrosdeCostoDel(_Cod_proyecto, _Cod_centro_costo) {

    var jsonData = {
        Cod_proyecto: _Cod_proyecto,
        Cod_centro_costo: _Cod_centro_costo
    };
    UIAjax.runFunctionAsync('TablasSoporte', 'DelCentrodeCostoEnProyecto', jsonData);
}

//function CentrodeCostoMasterComboProyectosOnChange(CodProyecto) {
//    if (CodProyecto == "-999") {
//        CentrosdeCostoMasterListar();
//        dtCentrosdeCostos2.clear().draw();
//        $("#btnCentroCostosProyectoIns").hide();
//    }
//    else {
//        CentrosdeCostoxProyectoListar(CodProyecto);
//        CentrosdeCostoxNoEnProyectoListar(CodProyecto);
//        $("#btnCentroCostosProyectoIns").show();
//    }
//}


function CentrodeCostoMasterValidar() {

    var Codigo = parseInt($('#txtCentrodeCostoMasterCodigo').val());
    var Nombre = $('#txtCentrodeCostoMasterDescripcion').val();

    if (isNaN(Codigo) || Codigo <= 0) {

        $('#txtProyectoCodigo').focus();
        return 'Código de centro de costo inválido';
    }

    if (Nombre == '') {

        $('#txtProyectoNombre').focus();
        return 'Nombre de centro de costo vacío';
    }

    return '';
}

function CentrodeCostoMasterSave() {

    var msgError = CentrodeCostoMasterValidar();

    if (msgError != '') {
        MsgBox(msgError, "warning", false);
        return;
    }
    
    var jsonData = {
        Accion: accion,
        Cod_centro_costo: $('#txtCentrodeCostoMasterCodigo').val(),
        Descripcion: $('#txtCentrodeCostoMasterDescripcion').val(),
        Cadena1: $('#txtCentrodeCostoMasterCadena1').val(),
        Cadena2: $('#txtCentrodeCostoMasterCadena2').val(),
        Estado: $('#selectCentrodeCostoMasterEstado').val()
    };
    UIAjax.runFunctionAsync('TablasSoporte', 'SetCentrodeCostoMaster', jsonData, function () {
        $('#modalCentrodeCostoMasterDetalle').modal('hide');
        $('#selectCentrosdeCostoProyecto').change();
    });
}

function CentrodeCostoMasterLoad(CodCentrodeCosto) {
    var jsonParam = {
        cod_centro_costo: CodCentrodeCosto,
    };
    $.ajax({
        type: 'GET',
        url: GetUrl("/TablasSoporte/GetClasificadoresCentrodeCostoMaster"),
        dataType: 'json',
        data: jsonParam,
        async: false,
        success: function (data) {

            var item = data.data[0];

            var cp = parseInt(item.cod_centro_costo);
            if (cp == -1) {
                $('#btnCentrodeCostoMasterGuardar').hide();
            }
            else {
                $('#btnCentrodeCostoMasterGuardar').show();
            }

            $('#txtCentrodeCostoMasterCodigo').val(item.cod_centro_costo);
            $('#selectCentrodeCostoMasterEstado').val(item.estado);
            $('#txtCentrodeCostoMasterDescripcion').val(item.descripcion);
            $('#txtCentrodeCostoMasterCadena1').val(item.cadena1);
            $('#txtCentrodeCostoMasterCadena2').val(item.cadena2);
        }
    });
};

function CentrodeCostoMasterCleanControls() {

    $('#txtCentrodeCostoMasterCodigo').val('');
    $('#selectCentrodeCostoMasterEstado').val('A');
    $('#txtCentrodeCostoMasterDescripcion').val('');
    $('#txtCentrodeCostoMasterCadena1').val('');
    $('#txtCentrodeCostoMasterCadena2').val('');
}


function CentrodeCostoImportar() {

    tbProyectosImportar = $('#tbProyectosImportar').DataTable({
        //"ajax": portal + '/TablasSoporte/GetClasificadoresProyectos/',
        "initComplete": function (settings, json) {
            //$(chbTodos).appendTo("#tbTrabajadores_filter label");
        }
        , bDestroy: true
        , dom: 'Bfrtip'
        , buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5'
        ]
        , "language": {
            "url": '/js/language.js'
        }
        , "columns": [
            { "data": "proyecto" },
            { "data": "centro_costo" },
            { "data": "descripcion" },
            { "data": "cadena1" }
        ]
    });
}



//#endregion

//#region  LOCALIDADES
function LocalidadesList() {

    tableLocalidades = $('#tbLocalidades').DataTable({
        "ajax": portal + '/TablasSoporte/GetLocalidadesClasificadores/'
        , "initComplete": function (settings, json) {
            //$(chbTodos).appendTo("#tbTrabajadores_filter label");
        }
        , bDestroy: true
        , dom: 'Bfrtip'
        , buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5'
        ]
        , "language": {
            "url": '/js/language.js'
        }
        , "columns": [
            { "data": "cod_localidad" },
            { "data": "descripcion" },
            { "data": "estado" },
            { "data": "flg_centro_riesgo" },
            { "data": "cadena1" }
        ]
    });
}

function LocalidadesLoad(Cod_Localidad) {

    $("input[type=text]").val("");

    var jsonParam = {
        CodLocalidad: Cod_Localidad
    };

    $.ajax({
        type: 'GET',
        url: portal + '/TablasSoporte/GetLoadLocalidadClasificadores',
        data: jsonParam,
        dataType: 'json',
        cache: false,
        async: false,
        success: function (data) {

            var item = data[0];

            $('#txtLocalidadesCodigo').val(item.cod_localidad);
            $('#selectLocalidadesEstado').val(item.estado);
            $('#txtLocalidadesDescripcion').val(item.descripcion);
            $('#txtLocalidadesFichaRuc').val(item.ficha_ruc);

            $('#chkLocalidadesCentroRiesgo').prop('checked', item.flg_centro_riesgo == 'S');
            $('#txtLocalidadesTasa').val(item.tasa_centro_riesgo);
            $('#txtLocalidadesDireccion').val(item.direccion);
            $('#txtLocalidadesNDirNumero').val(item.numero);
            $('#txtLocalidadesIntDirInterior').val(item.interior);
            $('#selectLocalidadesDepartamento').val(item.cod_ubigeo.substring(0, 2));
            $('#selectLocalidadesDepartamento').change();
            $('#selectLocalidadesProvincia').val(item.cod_ubigeo.substring(0, 4));
            $('#selectLocalidadesProvincia').change();
            $('#selectLocalidadesDistrito').val(item.cod_ubigeo.trim());

            $('#txtLocalidadesReferencia').val(item.referencia);
            $('#txtLocalidadesTelefonos').val(item.telefonos);
            $('#txtLocalidadesFax').val(item.fax);
            $('#selectLocalidadesJefeDeLocalidad').val(item.cod_personal);
            $('#txtLocalidadesCadena1').val(item.cadena1);
        }
    });
}


function LocalidadesValidar() {

    var Cod_localidad = parseInt($('#txtLocalidadesCodigo').val());
    var Nombre = $('#txtLocalidadesDescripcion').val();

    if (isNaN(Cod_localidad) || Cod_localidad <= 0) {

        $('#txtLocalidadesCodigo').focus();
        return 'Código de localidad inválida';
    }

    if (Nombre == '') {

        $('#txtLocalidadesDescripcion').focus();
        return 'Nombre de localidad vacía';
    }

    return '';
}


function LocalidadesSave() {

    var msgError = LocalidadesValidar();

    if (msgError != '') {
        MsgBox(msgError, "warning", false);
        return;
    }

    var jsonData = {
        Accion: accion,
        cod_localidad: $('#txtLocalidadesCodigo').val(),
        estado: $('#selectLocalidadesEstado').val(),
        descripcion: $('#txtLocalidadesDescripcion').val(),
        ficha_ruc: $('#txtLocalidadesFichaRuc').val(),
        flg_centro_riesgo: $('#chkLocalidadesCentroRiesgo').prop('checked') ? 'S' : 'N',
        tasa_centro_riesgo: $('#txtLocalidadesTasa').val().replace(',', ''),
        direccion: $('#txtLocalidadesDireccion').val(),
        numero: $('#txtLocalidadesNDirNumero').val(),
        interior: $('#txtLocalidadesIntDirInterior').val(),
        cod_ubigeo: $('#selectLocalidadesDistrito').val(),
        referencia: $('#txtLocalidadesReferencia').val(),
        telefonos: $('#txtLocalidadesTelefonos').val(),
        fax: $('#txtLocalidadesFax').val(),
        cod_personal: $('#selectLocalidadesJefeDeLocalidad').val(),
        cadena1: $('#txtLocalidadesCadena1').val()
    };
    UIAjax.runFunctionAsync('TablasSoporte', 'SetLocalidadesClasificadores', jsonData, function () {
        $('#modalLocalidadesDetalle').modal('hide');
        LocalidadesList();
    });
}

function LocalidadesCleanControls() {

    $('#txtLocalidadesCodigo').val('');
    $('#txtLocalidadesDescripcion').val('');
    $('#txtLocalidadesFichaRuc').val('');
    $('#selectLocalidadesEstado').val('A');
    $('#txtLocalidadesTasa').val('');
    $('#txtLocalidadesDireccion').val('');
    $('#txtLocalidadesNDirNumero').val('');
    $('#txtLocalidadesIntDirInterior').val('');
    $('#txtLocalidadesReferencia').val('');
    $('#txtLocalidadesTelefonos').val('');
    $('#txtLocalidadesFax').val('');
    $('#txtLocalidadesCadena1').val('');
    $('#chkLocalidadesCentroRiesgo').prop('checked', false);
    $('#selectLocalidadesDepartamento').val('14');
    $('#selectLocalidadesDepartamento').change();
    $('#selectLocalidadesProvincia').val('1401');
    $('#selectLocalidadesProvincia').change();
    $('#selectLocalidadesDistrito').val('140101');
}


//#endregion



//#region UNIDADES DE NEGOCIO

function UnidadNegocioList() {

    tableUnidadesNegocio = $('#tbUnidadesNegocio').DataTable({
        "ajax": portal + '/TablasSoporte/GetUnidadesNegocioClasificadores/'
        , "initComplete": function (settings, json) {
            //$(chbTodos).appendTo("#tbTrabajadores_filter label");
        }
        , bDestroy: true
        , dom: 'Bfrtip'
        , buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5'
        ]
        , "language": {
            "url": '/js/language.js'
        }
        , "columns": [
            { "data": "cod_unidad_negocio" },
            { "data": "descripcion" },
            { "data": "estado" },
            { "data": "cadena1" }
        ]
    });
}

function UnidadNegocioLoad(Cod_Unidad_Negocio) {

    $("input[type=text]").val("");

    var jsonParam = {
        CodUnidadNegocio: Cod_Unidad_Negocio
    };

    $.ajax({
        type: 'GET',
        url: portal + '/TablasSoporte/GetLoaUnidadNegocioClasificadores',
        data: jsonParam,
        dataType: 'json',
        cache: false,
        async: false,
        success: function (data) {

            $('#txtUnidadesNegocioCodigo').val(data[0].cod_unidad_negocio.trim());
            $('#selectUnidadesNegocioEstado').val(data[0].estado);
            $('#txtUnidadesNegocioDescripcion').val(data[0].descripcion.trim());
            $('#txtUnidadesNegocioCadena1').val(data[0].cadena1.trim());

        }
    });
}

function UnidadNegocioSave() {

    var jsonData = {
        Accion: accion,
        cod_unidad_negocio: $('#txtUnidadesNegocioCodigo').val().trim(),
        estado: $('#selectUnidadesNegocioEstado').val().trim(),
        descripcion: $('#txtUnidadesNegocioDescripcion').val().trim(),
        cadena1: $('#txtUnidadesNegocioCadena1').val(),
    };
    UIAjax.runFunctionAsync('TablasSoporte', 'SetClasificadoresUnidadesNegocio', jsonData, function () {
        $('#modalUnidadesNegocioDetalle').modal('hide');
        UnidadNegocioList();
    });
}

function UnidadNegocioCleanControls() {

    $('#txtUnidadesNegocioCodigo').val('');
    $('#selectUnidadesNegocioEstado').val('A');
    $('#txtUnidadesNegocioDescripcion').val('');
    $('#txtUnidadesNegocioCadena1').val('');
}


//#endregion



//#region ÁREAS

async function AreaLoad(Cod_Area) {

    $("input[type=text]").val("");

    var jsonParam = {
        CodArea: Cod_Area
    };
    await Load.Area.Gerencia('#selectAreaGerencia');

    $.ajax({
        type: 'GET',
        //url: '@Url.Action("GetLoadAreasClasificadores")',
        url: portal + '/TablasSoporte/GetLoadAreaClasificadores',
        data: jsonParam,
        dataType: 'json',
        cache: false,
        async: false,
        success: function (data) {
            var jsonObj = data[0];
            $('#txtAreaCodigo').val(data[0].cod_area);
            $('#txtAreaArea').val(data[0].descripcion);
            $('#selectAreaEstado').val(data[0].estado);
            $('#selectAreaGerencia').val(jsonObj.cod_gerencia);
            $('#txtAreaCadena1').val(data[0].cadena1);
            $('#selectAreaJefeDeArea').val(data[0].cod_personal);
            $('#txtAreaDescripcionLarga').val(data[0].descripcion_larga);

        }
    });
}


function AreaValidar() {

    var Cod_area = parseInt($('#txtAreaCodigo').val());
    var Nombre = $('#txtAreaArea').val();

    if (isNaN(Cod_area) || Cod_area <= 0) {

        $('#txtAreaCodigo').focus();
        return 'Código de area inválido';
    }

    if (Nombre == '') {

        $('#txtAreaArea').focus();
        return 'Nombre de area vacía';
    }

    return '';
}


function AreaSave() {
    var msgError = AreaValidar();
    if (msgError != '') {
        MsgBox(msgError, "warning", true);
        return;
    }
    var jsonParam = {
        Accion: accion,
        CodigoDeArea: $('#txtAreaCodigo').val().trim(),
        CodigoDeGerencia: $('#selectAreaGerencia').val(),
        Descripcion: $('#txtAreaArea').val().trim(),
        Estado: $('#selectAreaEstado').val(),
        Cadena1: $('#txtAreaCadena1').val().trim(),
        CodigoDePersonal: $('#selectAreaJefeDeArea').val(),
        DescripcionLarga: $('#txtAreaDescripcionLarga').val().trim(),
    };
    UIAjax.runFunctionAsync('TablasSoporte', 'SetClasificadorArea', jsonParam, function () {
        $('#modalAreasDetalle').modal('hide');
        Load.Area.Ready();
    });
}


function AreaCleanControls() {

    $('#txtAreaCodigo').val('');
    $('#txtAreaArea').val('');
    $('#selectAreaEstado').val('A');
    $('#txtAreaCadena1').val('');
    $('#selectAreaJefeDeArea').val('');
    $('#txtAreaDescripcionLarga').val('');
}


//#endregion