﻿

//#region CERTIFICADO DE QUINTA
function CertificadodeQuintaInicializar() {

	dtCertQuintaPeriodos = $('#dtCertQuintaPeriodos');


	var thePeriodoDP = getPeriodo();
	var _dateDP = new Date();
	if (!thePeriodoDP) {
		thePeriodoDP = _dateDP.getFullYear();
	}
	var minPeriodoDP = getMinPeriodo();
	if (!minPeriodoDP) {
		$.ajax(portal + '/ajax/core/GetMinPeriodo').done(function (data) {
			minPeriodoDP = data[0].minPeriodo;
		});
	}

	$("#txtCertQuintaAnyho").val(thePeriodoDP);
	$("#txtCertQuintaAnyho").attr('min', minPeriodoDP);
	$("#txtCertQuintaAnyho").attr('max', _dateDP.getFullYear() + 1);


	$('#txtCertQuintaAnyho').on('change', function () {
		_newAnyo = $(this).val();
		CertificadodeQuintaGetPeriodoDP(_newAnyo);
	});

	$('#selCertQuintaPlanilla').on('change', function () {
		_newAnyo = $('#txtCertQuintaAnyho').val();
		CertificadodeQuintaGetPeriodoDP(_newAnyo);
	});

	CertificadodeQuintaGetPeriodoDP();


	dtCertQuintaPeriodos.on('click', 'tr', function () {

		$('#dtCertQuintaPeriodos tr').removeClass('selected');

		var ths = $(this);
		ths.addClass('selected');

		var table = $('#dtCertQuintaPeriodos').DataTable();
		var data = table.row(this).data();

		periodoDesde = data.desde2;
		periodoNumero = data.numero;
		periodoMes = data.mes;

		CertificadodeQuintaListarTrabajadoresRefrescar();
	});

	dtCertQuintaTrabajadores.on('click', 'tr', function () {

		var Row = $(this);

		if (Row.hasClass('selected')) {
			Row.removeClass('selected');
		}
		else {
			Row.addClass('selected');
		}

	});

	$("#selCertQuintaEstado,#selCertQuintaFiltroRemuneraciones,#selCertQuintaProyecto,#selCertQuintaUnidadNegocio," +
		"#selCertQuintaCentrodeCosto,#selCertQuintaArea,#selCertQuintaLocalidad,#selCertQuintaPlanilla").change(function () {
			CertificadodeQuintaListarTrabajadoresRefrescar();
		});


	$('#btnCertQuintaSeleccionarTodos').on('click', function (e) {
		e.preventDefault();

		$('#dtCertQuintaTrabajadores tr').each(function (index, value) {

			var Row = $(this);
			Row.addClass('selected');
		});
	});


	$('#btnCertQuintaGenerarReporte').on('click', function (e) {
		e.preventDefault();

		var codPersonales = '';
		$('#dtCertQuintaTrabajadores tr.selected').each(function (index, value) {

			try {
				if (codPersonales.length > 0)
					codPersonales += ",";

				var table = $('#dtCertQuintaTrabajadores').DataTable();
				if (table != null) {
					var data = table.row(this).data();
					codPersonales += data.cod_personal;
				}
			}
			catch (e) {
				// sentencias para manejar cualquier excepción
				//console.log(e); // pasa el objeto de la excepción al manejador de errores
			}
		});

		if (codPersonales == '') {
			UIMensaje.COLABORADOR();
			return;
		}

		let periodoNumero = ('000' + periodoMes).substr(-3);

		let jsonParam = {
			NombreDeReporte: "RptRentaQuintaMaster",
			CodigoDeTipoPlanilla: $("#selCertQuintaPlanilla").val(),
			CodigoAnual: $("#txtCertQuintaAnyho").val(),
			CodigoMes: periodoMes,
			Numero: periodoNumero,
			CodigoDeProceso: 'P',
			CodigoClaseDeProceso: 'B',
			CodigoDeProyecto: $("#selCertQuintaProyecto").val(),
			CodigoDeUN: $("#selCertQuintaUnidadNegocio").val(),
			CodigoDeCC: $("#selCertQuintaCentrodeCosto").val(),
			CodigoDeArea: $("#selCertQuintaArea").val(),
			CodigoDeLocalidad: $("#selCertQuintaLocalidad").val(),
			Todos: 'N',
			ListaPersonal: codPersonales,
			CodigoDeTipoConcepto: '2'
		}

		UIBoldReport.Load(jsonParam)

	});
}


function CertificadodeQuintaGetPeriodoDP(anyo) {

	periodoDesde = periodoNumero = '';

	_CodTipoPlanilla = $('#selCertQuintaPlanilla').val();
	var jsonParam = {
		FlgClaseProceso: 'B',
		CodTipoPlanilla: _CodTipoPlanilla,
		CodAnual: (anyo) ? anyo : $('#txtCertQuintaAnyho').val(),
	};

	dtCertQuintaPeriodos.DataTable({
		"ajax": portal + '/ajax/planillas/GetPeriodosAll2/?' + $.param(jsonParam),
		searching: false,
		paging: false,
		info: false,
		columns: [
			{ "data": "periodo" },
			{ "data": "mes" },
			{ "data": "numero" },
			{ "data": "desde2" },
			{ "data": "hasta2" },
		]
	});

	CertificadodeQuintaListarTrabajadoresRefrescar();
}


function CertificadodeQuintaListarTrabajadoresRefrescar() {

	if (periodoDesde == '' || periodoNumero == '') {

		CertificadodeQuintaListarTrabajadoresCrear();
		return;
	}


	var codProyecto = $('#selCertQuintaProyecto').val();
	var codCCosto = $('#selCertQuintaCentrodeCosto').val();
	var codUnidadNegocio = $('#selCertQuintaUnidadNegocio').val();
	var codArea = $('#selCertQuintaArea').val();
	var codLocalidad = $('#selCertQuintaLocalidad').val();
	var codTipoPlanilla = $('#selCertQuintaPlanilla').val();
	var codEstado = $('#selCertQuintaEstado').val();
	var filtroRemuneracion = $('#selCertQuintaFiltroRemuneraciones').val();


	//containerPage.maskContainer("Cargando ....");
	//$("html, body").animate({
	//    scrollTop: $('#dtCertQuintaTrabajadores').offset().top
	//}, 600, "linear", function () {
	//    containerPage.unmaskContainer();
	//});

	CertificadodeQuintaListarTrabajadores(codProyecto, codCCosto, codUnidadNegocio, codArea, codLocalidad, codTipoPlanilla, periodoDesde, periodoNumero, codEstado, filtroRemuneracion);
}

function CertificadodeQuintaListarTrabajadoresCrear() {

	dtCertQuintaTrabajadores = $('#dtCertQuintaTrabajadores').DataTable({
		//bDestroy: true,
		//"language": {
		//    "url": '/js/language.js'
		//}
		//, "bFilter": false
		//, "paging": false
		//, "bInfo": false
		//, "scrollY": "400px"
		//, dom: 'Bfrtip'
		//, buttons: [
		//    'excelHtml5',
		//]
		//, searching: true,
		//paging: false,
		columns: [
			{ "data": "cod_trabajador" },
			{ "data": "num_reingreso" },
			{ "data": "trabajador" },
			{ "data": "tipo_doc" },
			{ "data": "num_doc" },
			{ "data": "fecha_ingreso" },
			{ "data": "fecha_cese" },
			{ "data": "neto" },
		]
	});

	dtCertQuintaTrabajadores.clear().draw();
}


async function CertificadodeQuintaListarTrabajadores(codProyecto, codCCosto, codUnidadNegocio, codArea, codLocalidad, codTipoPlanilla, periodo, numero, codEstado, filtroRemuneracion) {
	var jsonParam = {
		codProyecto: codProyecto,
		codCCosto: codCCosto,
		codUnidadNegocio: codUnidadNegocio,
		codArea: codArea,
		codLocalidad: codLocalidad,
		codTipoPlanilla: codTipoPlanilla,
		periodo: periodo,
		numero: numero,
		codEstado: codEstado,
		filtroRemuneracion: filtroRemuneracion
	};
	await UILoadingDefault('show');
	await UIAjax.invokeMethodAsync('ajax/planillas', 'GetPlanillaCertificadoQuintaTrabajadores', jsonParam).then(function (r) {
		dtCertQuintaTrabajadores = $('#dtCertQuintaTrabajadores').DataTable({
			data: r,
			//bDestroy: true,
			//"language": {
			//    "url": '/js/language.js'
			//}
			//"bFilter": false,
			//"paging": false,
			//"bInfo": false,
			//, "scrollY": "400px"
			//dom: 'Bfrtip',
			buttons: [
				//'copyHtml5',
				'excelHtml5',
				//'csvHtml5'
			],
			columns: [
				{ "data": "cod_trabajador" },
				{ "data": "num_reingreso" },
				{ "data": "trabajador" },
				{ "data": "tipo_doc" },
				{ "data": "num_doc" },
				//{ "data": "cargo" },
				{ "data": "fecha_ingreso" },
				{ "data": "fecha_cese" },
				{ "data": "neto" }
			],
			//"order": [[3, 'asc']],
		});
	});
	UILoadingDefault('hide');
}



//#endregion

//#region 6. DECLARACION DE AFP
function DeclaracionDeAfpInicializar() {

	$('#txtRptDeclaracionDeAfpAnio').mask('0000');

	$('#btnRptDeclaracionDeAfpAceptar').on('click', function (e) {
		e.preventDefault();

		DeclaracionDeAfpGenerarReporte();
	});
}

function DeclaracionDeAfpGenerarReporte() {
	$('#gridcontainerRptDeclaracionDeAfp, #rowBodyRptDecAfpResumen').addClass('d-none');
	UIDataTable.setEmpty('#dtRptDeclaracionDeAfp, #tableRptDecAfpResumen');

	var jsonParam = {
		CodigoDeTipoPlanilla: $('#selRptDeclaracionDeAfpCodPlanilla').val(),
		CodigoDeProyecto: $('#selRptDeclaracionDeAfpProyecto').val(),
		CodigoDeUN: $('#selRptDeclaracionDeAfpCodUnidadNegocio').val(),
		CodigoDeCC: $('#selRptDeclaracionDeAfpCodCentroCosto').val(),
		CodigoDeArea: $('#selRptDeclaracionDeAfpCodArea').val(),
		CodigoDeLocalidad: $('#selRptDeclaracionDeAfpCodLocalidad').val(),
		CodigoAnual: $('#txtRptDeclaracionDeAfpAnio').val(),
		CodigoMes: $('#selRptDeclaracionDeAfpMes').val(),
		NombreDeReporte: 'RptDeclaraciónAFPResumen'
	};

	$.LoadingOverlay("show");
	if ($('#selTipoDeReporteRptDecAfp').val() === 'D') {
		jsonParam = {
			...jsonParam,
			CodigoDeAfp: $('#selRptDeclaracionDeAfpCodAfp').val(),
			NombreDeReporte: 'RptDeclaraciónAFPDetallado'
		}
	}
	//$.LoadingOverlay("hide");
	UIBoldReport.Load(jsonParam);
	$.LoadingOverlay("hide");
}

//#endregion

//#region PLANILLA CON CUENTAS CONTABLES
function PlanillaCCContablesInicializar() {

	//$('#btnRptPlanillaCCContablesAceptar').on('click', function (e) {
	//    e.preventDefault();

	//    PlanillaCCContablesGenerarReporte();
	//});


	var thePeriodoDP = getPeriodo();
	var _dateDP = new Date();
	if (!thePeriodoDP) {
		thePeriodoDP = _dateDP.getFullYear();
	}
	var minPeriodoDP = getMinPeriodo();
	if (!minPeriodoDP) {
		$.ajax(portal + '/ajax/core/GetMinPeriodo').done(function (data) {
			minPeriodoDP = data[0].minPeriodo;
		});
	}

	$('#selRptPlanillaCCContablesProceso').on('change', function () {

		PlanillaCCContablesGetClaseProceso($(this).val());
	});

	$('#selRptPlanillaCCContablesClase_Proceso').on('change', function () {

		PlanillaCCContablesListarPeriodos($("#txtRptPlanillaCCContablesAnyho").val());
	});


	$("#txtRptPlanillaCCContablesAnyho").val(thePeriodoDP);
	$("#txtRptPlanillaCCContablesAnyho").attr('min', minPeriodoDP);
	$("#txtRptPlanillaCCContablesAnyho").attr('max', _dateDP.getFullYear() + 1);
	$('#txtRptPlanillaCCContablesAnyho').on('change', function () {
		_newAnyo = $(this).val();
		PlanillaCCContablesListarPeriodos(_newAnyo);
	});

	$("#dtRptPlanillaCCContablesPeriodos").on('click', 'tr', function () {
		$('#dtRptPlanillaCCContablesPeriodos tr').removeClass('selected');
		var ths = $(this);
		ths.addClass('selected');
	});

}

function PlanillaCCContablesGetClaseProceso(Cod_Proceso) {
	var jsonParam = {
		'Cod_Proceso': Cod_Proceso
	};
	$.ajax({
		type: 'GET',
		url: GetUrl("Ajax/Planillas/GetReportePlanillaRptCContablesClaseProceso"),
		dataType: 'json',
		data: jsonParam,
		async: false,
		success: function (data) {

			var container = $("#selRptPlanillaCCContablesClase_Proceso");
			container.empty();

			var container2 = $("#selRptPlanillaCCContablesAdicionarClaseProceso");
			container2.empty();

			$.each(data.data, function (index, row) {
				var html = '<option value="' + row.cod_Clase_Proceso + '">' + row.nombre + '</option>';
				container.append(html);
				container2.append(html);
			});

			PlanillaCCContablesListarPeriodos($("#txtRptPlanillaCCContablesAnyho").val());
		}
	});
}



function PlanillaCCContablesListarPeriodos(anyo) {

	periodoDesde = periodoNumero = '';

	_CodTipoPlanilla = $('#selRptPlanillaCCContablesCodPlanilla').val();
	var jsonParam = {
		FlgClaseProceso: $("#selRptPlanillaCCContablesClase_Proceso").val(), //'B',
		CodTipoPlanilla: _CodTipoPlanilla,
		CodAnual: (anyo) ? anyo : $('#txtCertQuintaAnyho').val(),
	};
	$("#dtRptPlanillaCCContablesPeriodos").DataTable({
		"ajax": portal + '/ajax/planillas/GetPeriodosAll2/?' + $.param(jsonParam),
		searching: false,
		//"scrollY": "200px",
		paging: false,
		info: false,
		columns: [
			{ "data": "periodo" },
			{ "data": "mes" },
			{ "data": "numero" },
			{ "data": "desde2" },
			{ "data": "hasta2" },
		]
	});
}

//#endregion

//#region REPORTE DE PROVISIONES
function ReporteDeProvisionesInicializar() {

	$('#btnRptReporteDeProvisionesAceptar').on('click', function (e) {
		e.preventDefault();


		ReporteDeProvisionesGenerarReporte();
	});
}

function ReporteDeProvisionesGenerarReporte() {

	var ParametersDt = 'codProyecto=' + $('#selRptReporteDeProvisionesProyecto').val().trim() +
		'&codCentroCosto=' + $('#selRptReporteDeProvisionesCodCentroCosto').val().trim() +
		'&codUnidadNegocio=' + $('#selRptReporteDeProvisionesCodUnidadNegocio').val().trim() +
		'&codArea=' + $('#selRptReporteDeProvisionesCodArea').val().trim() +
		'&codLocalidad=' + $('#selRptReporteDeProvisionesCodLocalidad').val().trim() +
		'&codPlanilla=' + $('#selRptReporteDeProvisionesCodPlanilla').val().trim();
	//'&mes=' + $('#selRptReporteDeProvisionesMes').val().trim() +
	//'&anio=' + $('#txtRptReporteDeProvisionesAnio').val().trim();

	containerPage.maskContainer("Cargando");

	$('#dtRptReporteDeProvisiones').DataTable({
		"ajax": GetUrl("Ajax/Planillas/GetReportePlanillaRptReporteDeProvisiones?" + ParametersDt)
		, "initComplete": function (settings, json) {

			$('#gridcontainerRptReporteDeProvisiones').removeClass('d-none');
			$("html, body").animate({
				scrollTop: $('#gridcontainerRptReporteDeProvisiones').offset().top
			}, 600, "linear", function () {
				containerPage.unmaskContainer();
			});
		}
		//, data: JSON.stringify(jsonData)
		, "bFilter": true
		, "paging": false
		, "bInfo": false
		, "scrollY": "400px"
		, dom: 'Bfrtip'
		, buttons: [
			'copyHtml5',
			'excelHtml5',
			'csvHtml5'
		]
		, "columns": [

			//Basede datos
			{ "data": "cod_trabajador" },
			{ "data": "apellido_p" },
			{ "data": "apellido_m" },
			{ "data": "nombres" },
			{ "data": "afectoAnt" },
			{ "data": "rentaAnt" },
			{ "data": "afectoAnt" },
			{ "data": "rentaAnt" },
			//{ "data": "nom_localidad" },
		]
	});
}

//#endregion


//#region CALCULO DE CTS
function CalculoDeCtsInicializar() {

	$('#btnRptCalculoDeCtsAceptar').on('click', function (e) {
		e.preventDefault();


		CalculoDeCtsGenerarReporte();
	});
}

function CalculoDeCtsGenerarReporte() {

	var ParametersDt = 'codProyecto=' + $('#selRptCalculoDeCtsProyecto').val().trim() +
		'&codCentroCosto=' + $('#selRptCalculoDeCtsCodCentroCosto').val().trim() +
		'&codUnidadNegocio=' + $('#selRptCalculoDeCtsCodUnidadNegocio').val().trim() +
		'&codArea=' + $('#selRptCalculoDeCtsCodArea').val().trim() +
		'&codLocalidad=' + $('#selRptCalculoDeCtsCodLocalidad').val().trim() +
		'&codPlanilla=' + $('#selRptCalculoDeCtsCodPlanilla').val().trim();
	//'&mes=' + $('#selRptCalculoDeCtsMes').val().trim() +
	//'&anio=' + $('#txtRptCalculoDeCtsAnio').val().trim();

	containerPage.maskContainer("Cargando");

	$('#dtRptCalculoDeCts').DataTable({
		"ajax": GetUrl("Ajax/Planillas/GetReportePlanillaRptCalculoDeCts?" + ParametersDt)
		, "initComplete": function (settings, json) {

			$('#gridcontainerRptCalculoDeCts').removeClass('d-none');
			$("html, body").animate({
				scrollTop: $('#gridcontainerRptCalculoDeCts').offset().top
			}, 600, "linear", function () {
				containerPage.unmaskContainer();
			});
		}
		//, data: JSON.stringify(jsonData)
		, "bFilter": true
		, "paging": false
		, "bInfo": false
		, "scrollY": "400px"
		, bDestroy: true
		, dom: 'Bfrtip'
		, buttons: [
			'copyHtml5',
			'excelHtml5',
			'csvHtml5'
		]
		, "language": {
			"url": '/js/language.js'
		}
		, "columns": [

			//Basede datos
			{ "data": "cod_trabajador" },
			{ "data": "apellido_p" },
			{ "data": "apellido_m" },
			{ "data": "nombres" },
			{ "data": "afectoAnt" },
			{ "data": "rentaAnt" },
			{ "data": "afectoAnt" },
			{ "data": "rentaAnt" },
			//{ "data": "nom_localidad" },
		]
	});
}

//#endregion
//******************************************************************************************************************


var g_codPersonal;
var g_numIngreso,
	g_miembro_comite = "",
	g_miembro_comite_desde = "",
	g_miembro_comite_hasta = "",
	g_estadotrab = "",
	g_tipoplanilla = "",
	g_proyecto = "",
	g_ccosto = "",
	g_localidad = "",
	g_unegocio = "",
	g_area = "",
	g_filtro = "",
	g_busqueda = "",
	g_proceso = "",
	g_claseProceso = "",
	g_periodo = "",
	g_mes = "",
	g_numero = "",
	g_hasta = "",
	g_desde = "";

function GetFiltros() {
	g_proceso = ($('#TabPagos').hasClass('active')) ? 'P' : 'V';
	g_claseProceso = ($('#TabPagos').hasClass('active'))
		? $('input[name=groupPagos]:checked').val()
		: $('input[name=groupProvisiones]:checked').val();
	g_tipoplanilla = $('#selectTipoPlanilla').val();
	g_proyecto = $('#selectProyecto').val();
	g_ccosto = $('#selectCentrosCosto').val();
	g_localidad = $('#selectLocalidad').val();
	g_unegocio = $('#selectUnidadesNegocio').val();
	g_area = $('#selectArea').val();
	g_filtro = $('#selectFiltrar').val();
	g_busqueda = $('#txtBusqueda').val();

	var periodoSTR = $('#selectPeriodo').val();
	var array = periodoSTR.split('-');
	g_periodo = array[3];
	g_mes = array[4];
	g_numero = array[5];
	g_desde = array[1];
	g_hasta = array[2];
}

async function GetPeriodosCalculo() {
	$("#tbodyColaboradores").empty();
	await UILoadingDefault('show');
	var jsonParam = {
		cod_tipo_planilla: $('#selectTipoPlanilla').val(),
		flg_proceso: ($('#TabPagos').hasClass('active')) ? 'P' : 'V',
		flg_clase_proceso: ($('#TabPagos').hasClass('active'))
			? $('input[name=groupPagos]:checked').val()
			: $('input[name=groupProvisiones]:checked').val(),
		anual: getPeriodo()
	};
	await UIAjax.invokeMethodAsync('Planillas', 'GetPeriodosCalculo', jsonParam).then(async function (data) {
		var container = $("#selectPeriodo");
		container.empty();
		$.each(data, function (index, row) {
			let valOption = row.periodo + '-' + row.desde + '-' + row.hasta + '-' + row.periodo_real + '-' + row.mes + '-' + row.numero;
			var html = '<option value="' + valOption + '" data-json=\'' + JSON.stringify(row) + '\'>' + row.mes + ' - ' + row.numero + ' - ' + row.desde_hasta + '</option>';
			container.append(html);
		});
		if (data != null && data.length > 0) {
			GetPersonalCalculo();
		}
	});
	UILoadingDefault('hide');
};

async function GetPersonalCalculo() {
	await UILoadingDefault('show');
	GetFiltros();

	var jsonPeriodo = JSON.parse($('#selectPeriodo option:selected').attr('data-json'));

	var jsonParam = {
		FlgClaseProceso: g_claseProceso,
		Periodo: jsonPeriodo.periodo_real,
		Numero: jsonPeriodo.numero,
		CodigoDeTipoPlanilla: g_tipoplanilla,
		CodigoDeProyecto: g_proyecto,
		CodigoDeCC: g_ccosto,
		CodigoDeLocalidad: g_localidad,
		CodigoDeUN: g_unegocio,
		CodigoDeArea: g_area,
		Desde: jsonPeriodo.desde_ddmmyyyy,
		Hasta: jsonPeriodo.hasta_ddmmyyyy,
		Filtro: g_filtro,
		Buscar: g_busqueda,
	};

	$.ajax({
		type: 'GET',
		url: GetUrl("Planillas/GetPersonalCalculo"),
		dataType: 'json',
		data: jsonParam,
		async: false,
		success: function (data) {
			var container = $("#tbodyColaboradores");
			container.empty();

			$.each(data, function (index, row) {
				var html = "<tr class=\"unread\" data-personal='" + row.cod_Personal + "' data-ingreso='" + row.num_ingreso + "' data-estado='" + row.estado + "' data-fingreso='" + row.fecha_ingreso + "' data-fcese='" + row.fecha_cese + "'>"
					+ "<td class='text-center'><div class='custom-control custom-checkbox'>"
					+ "<input type='checkbox' class='custom-control-input' value='" + row.cod_Personal + "' id='chk" + row.cod_Personal + row.cod_trabajador + "' />"
					+ "<label class='custom-control-label' for='chk" + row.cod_Personal + row.cod_trabajador + "'> </label>"
					+ "</div></td>"
					+ "<td><input type=\"button\" value=\"Ver\" class=\"btn btn-sm btn-primary\" onclick=\"GetModalDetalle('" + row.cod_Personal + "','" + row.num_ingreso + "','" + row.flg_miembro_comite_sst + "','" + row.miembro_comite_sst_desde + "','" + row.miembro_comite_sst_hasta + "','" + row.estado + "');\"/></td>"
					+ "<td>" + row.cod_trabajador + "</td>"
					+ "<td><h6 class=\"mb-1\">" + row.trabajador + "</h6><p class=\"m-0\">" + row.cargo + "</p></td>"
					+ "<td>" + row.estado + "</td>"
					+ "<td><label id='lblEstadoCalculo'>" + row.estado_calculo + "</label></td>"
					+ "<td>" + (row.fecha_calculo == null ? "" : row.fecha_calculo) + "</td>"
					+ "</tr>";
				container.append(html);
			});

			$('#tbodyColaboradores .custom-control-input').on('change', (e) => {
				e.preventDefault();
				if (!e.target.checked) {
					$('#chkSel').prop('checked', false);
				}
			});
			$('#chkSel').prop('checked', false);
		}
	});
	UILoadingDefault('hide');
};

function GetModalDetalle(codPersonal, numIngreso, miembro_comite, miembro_comite_desde, miembro_comite_hasta, estado) {
	GetFiltros();
	g_codPersonal = codPersonal;
	g_numIngreso = numIngreso;
	g_miembro_comite = miembro_comite;
	g_miembro_comite_desde = miembro_comite_desde;
	g_miembro_comite_hasta = miembro_comite_hasta;
	g_estadotrab = estado;
	GetDetalleCalculoPersonal();
	$('#modalDetalle').modal('show');
};

function GetDetalleCalculoPersonal() {
	containerPage.maskContainer("Cargando ....");
	GetFiltros();
	var jsonParam = {
		personal: g_codPersonal,
		anual: getPeriodo(),
		periodoSTR: $('#selectPeriodo').val(),
		proceso: g_claseProceso,
		tipoConcepto: '%',
		tipoPlanilla: g_tipoplanilla,
		numIngreso: g_numIngreso,
	};
	$.ajax({
		type: 'GET',
		url: GetUrl("Planillas/GetDetalleCalculoPersonal"),
		dataType: 'json',
		data: jsonParam,
		async: false,
		success: function (data) {
			var dtConceptosCalculados = data.table;
			var dtConceptosVariables = data.table1;
			var dtContratos = data.table2;
			var dtPlanEPS = data.table3;
			var dtExcepciones = data.table4;
			var dtDepositos = data.table5;
			var dtAsistenciaTotal = data.table6;
			var dtAsistencia = data.table7;
			var dtCuentasCorrientes = data.table8;
			var dtRetencionJudicial = data.table9;
			var dtSaldoDeudor = data.table10;
			var dtCeseTrabajador = data.table11;

			if (dtConceptosCalculados.length > 0) {
				var container = $("#tbodyConceptos");
				container.empty();
				$.each(dtConceptosCalculados, function (index, row) {
					var html = "<tr class=\"unread\">"
						+ "<td class='text-center'>" + row.tipo + "</td>"
						+ "<td class='text-center'>" + row.concepto + "</td>"
						+ "<td class='text-center'>" + row.cod_plame + "</td>"
						+ "<td class='text-center'>" + row.nombre + "</td>"
						+ "<td class='text-center'>" + row.monto + "</td>"
						+ "<td class='text-center'>" + row.num_orden_calculo + "</td>"
						+ "<td class='text-center'>" + row.formula + "</td>"
						+ "<td class='text-center'>" + row.resolucion_formula + "</td>"
						+ "</tr>";
					container.append(html);
				})
			}

			if (dtConceptosVariables.length > 0) {
				var container = $("#tbodyConceptosVariables");
				container.empty();
				$.each(dtConceptosVariables, function (index, row) {
					var html = "<tr class=\"unread\">"
						+ "<td class='text-center'>" + row.tipo_concepto + "</td>"
						+ "<td class='text-center'>" + row.flg_permanente + "</td>"
						+ "<td class='text-center'>" + row.concepto + "</td>"
						+ "<td class='text-center'>" + row.nombre + "</td>"
						+ "<td class='text-right'>" + row.monto + "</td>"
						+ "</tr>";
					container.append(html);
				})
			}

			if (dtContratos.length > 0) {
				var container = $("#tbodyContratos");
				container.empty();
				$.each(dtContratos, function (index, row) {
					var html = "<tr class=\"unread\">"
						+ "<td class='text-center'>" + row.num_ingreso + "</td>"
						+ "<td class='text-center'>" + row.num_periodo + "</td>"
						+ "<td class='text-center'>" + row.correlativo_empresa + "</td>"
						+ "<td class='text-center'>" + row.fecha_contrato + "</td>"
						+ "<td class='text-center'>" + row.fecha_ingreso + "</td>"
						+ "<td class='text-center'>" + row.fecha_cese + "</td>"
						+ "<td class='text-center'>" + row.fin_contrato + "</td>"
						+ "<td class='text-center'>" + row.fecha_prorroga + "</td>"
						+ "<td class='text-center'>" + row.fin_prorroga + "</td>"
						+ "<td class='text-center'>" + row.tipo_contrato + "</td>"
						+ "<td class='text-center'>" + row.estado2 + "</td>"
						+ "<td class='text-center'>" + row.contrato + "</td>"
						+ "<td class='text-center'>" + row.existe_pdf + "</td>"
						+ "<td class='text-center'>" + row.fecha_pruebai + "</td>"
						+ "<td class='text-center'>" + row.fecha_pruebaf + "</td>"
						+ "<td class='text-center'>" + row.numero_mintra + "</td>"
						+ "<td class='text-center'>" + row.fecha_mintra + "</td>"
						+ "<td class='text-center'>" + row.observaciones + "</td>"
						+ "<td class='text-center'>" + row.tipo_planilla + "</td>"
						+ "<td class='text-center'>" + "" + "</td>"
						+ "<td class='text-right'>" + row.monto1 + "</td>"
						+ "<td class='text-right'>" + row.monto2 + "</td>"
						+ "<td class='text-center'>" + row.cod_usuario_modifica + "</td>"
						+ "<td class='text-center'>" + row.fecha_modifica + "</td>"
						+ "<td class='text-center'>" + row.cod_aprobador + "</td>"
						+ "<td class='text-center'>" + row.fecha_aprobador + "</td>"
						+ "<td class='text-center'>" + row.cod_autorizador + "</td>"
						+ "<td class='text-center'>" + row.fecha_autorizador + "</td>"
						+ "<td class='text-center'>" + row.cod_usuario_rechazo + "</td>"
						+ "<td class='text-center'>" + row.fecha_usuario_rechazo + "</td>"
						+ "<td class='text-center'>" + row.nom_estado_aprobacion + "</td>"
						+ "</tr>";
					container.append(html);
				})
			}

			if (dtPlanEPS.length > 0) {
				$('#lblEntidadContratante').text(dtPlanEPS[0].nombre);
				$('#lblPlan').text(dtPlanEPS[0].cod_plan);
				$('#textPlan').val(dtPlanEPS[0].cod_plan);
				$('#textPorcenEmpresa').val(dtPlanEPS[0].porcen_empresa);
				$('#textPorcenTrabajador').val(dtPlanEPS[0].porcen_trabajador);

				if (dtPlanEPS[0].flg_titular == 'S') $('#chkTitular').prop('checked', true);
				else $('#chkTitular').prop('checked', false);

				if (dtPlanEPS[0].flg_conyuge == 'S') $('#chkConyuge').prop('checked', true);
				else $('#chkConyuge').prop('checked', false);

				if (dtPlanEPS[0].flg_hijos_menores == 'S') $('#chkMenores').prop('checked', true);
				else $('#chkMenores').prop('checked', false);

				//$('#textNroHijosMenores').val(obj[0].hijos_menores);
				$('#textNroHijosMenores').val(dtPlanEPS[0].hijos_menores);

				if (dtPlanEPS[0].flg_hijos_mayores == 'S') $('#chkHijosMayores').prop('checked', true);
				else $('#chkHijosMayores').prop('checked', false);

				$('#textNroHijosMayores').val(dtPlanEPS[0].hijos_mayores);

				if (dtPlanEPS[0].flg_Padres == 'S') $('#chkPadres').prop('checked', true);
				else $('#chkPadres').prop('checked', false);

				$('#textNroPadres').val(dtPlanEPS[0].padres);

				//$('#lblMontoPlan').text(obj[0].monto_plan_total);
				//$('#lblMontoHijosMayores').text(obj[0].monto_hijos_mayores_total);
				//$('#lblMontoPadres').text(obj[0].monto_padres_total);
				$('#lblMontoPlan').text(dtPlanEPS[0].monto_plan_total);
				$('#lblMontoHijosMayores').text(dtPlanEPS[0].monto_hijos_mayores_total);
				$('#lblMontoPadres').text(dtPlanEPS[0].monto_padres_total);
			}

			if (dtExcepciones.length > 0) {
				var container = $("#tbodyExcepciones");
				container.empty();
				$.each(dtExcepciones, function (index, row) {
					var html = "<tr class=\"unread\">"
						+ "<td class='text-center'>" + row.cod_excepcion + "</td>"
						+ "<td class='text-center'>" + row.nombre + "</td>"
						+ "<td class='text-center'>" + row.motivo + "</td>"
						+ "<td class='text-center'>" + row.fecha + "</td>"
						+ "<td class='text-center'>" + row.inicio + "</td>"
						+ "<td class='text-center'>" + row.fin + "</td>"
						+ "<td class='text-center'>" + row.numero + "</td>"
						+ "<td class='text-center'>" + row.nom_unidad + "</td>"
						+ "<td class='text-right'>" + row.unidades + "</td>"
						+ "<td class='text-center'>" + row.cod_anual_afecto + "</td>"
						+ "</tr>";
					container.append(html);
				})
			}

			if (dtDepositos.length > 0) {
				var container = $("#tbodyDepositoCuenta");
				container.empty();
				$.each(dtDepositos, function (index, row) {
					var html = "<tr class=\"unread\">"
						+ "<td class='text-center'>" + row.periodo + "</td>"
						+ "<td class='text-center'>" + row.numero + "</td>"
						+ "<td class='text-center'>" + row.tipo_cuenta + "</td>"
						+ "<td class='text-center'>" + row.cod_banco + "</td>"
						+ "<td class='text-center'>" + row.banco + "</td>"
						+ "<td class='text-center'>" + row.numero_cuenta + "</td>"
						+ "<td class='text-center'>" + row.moneda + "</td>"
						+ "<td class='text-right'>" + row.monto + "</td>"
						+ "<td class='text-center'>" + row.concepto + "</td>"
						+ "</tr>";
					container.append(html);
				})
			}

			if (dtAsistenciaTotal.length > 0) {
				var container = $("#tbodyAsistenciaTotales");
				container.empty();
				$.each(dtAsistenciaTotal, function (index, row) {
					var html = "<tr class=\"unread\">"
						+ "<td class='text-center'>" + row.nombre + "</td>"
						+ "<td class='text-center'>" + ((row.th != null) ? UIMoney(row.th) : "") + "</td>"
						+ "<td class='text-center'>" + ((row.htar != null) ? UIMoney(row.htar) : "") + "</td>"
						+ "<td class='text-center'>" + ((row.the1 != null) ? row.the1 : "") + "</td>"
						+ "<td class='text-center'>" + ((row.the2 != null) ? row.the2 : "") + "</td>"
						+ "<td class='text-center'>" + ((row.the3 != null) ? row.the3 : "") + "</td>"
						+ "<td class='text-center'>" + ((row.the7 != null) ? row.the7 : "") + "</td>"
						+ "<td class='text-center'>" + ((row.the8 != null) ? row.the8 : "") + "</td>"
						+ "<td class='text-center'>" + (row.the1 + row.the2 + row.the3 + row.the7 + row.the8) + "</td>"
						+ "<td class='text-center'>" + (row.the1 + row.the2 + row.the3) + "</td>"
						+ "<td class='text-center'>" + ((row.the4 != null) ? row.the4 : "") + "</td>"
						+ "<td class='text-center'>" + ((row.the5 != null) ? row.the5 : "") + "</td>"
						+ "<td class='text-center'>" + ((row.the6 != null) ? row.the6 : "") + "</td>"
						+ "<td class='text-center'>" + (row.the5 + row.the6) + "</td>"
						+ "<td class='text-center'>" + ((row.hai != null) ? row.hai : "") + "</td>"
						+ "<td class='text-center'>" + ((row.fi != null) ? row.fi : "") + "</td>"
						+ "<td class='text-center'>" + ((row.fm != null) ? row.fm : "") + "</td>"
						+ "<td class='text-center'>" + ((row.si != null) ? row.si : "") + "</td>"
						+ "</tr>";
					container.append(html);
				});
			}

			if (dtAsistencia.length > 0) {
				var container = $("#tbodyAsistencia");
				container.empty();
				$.each(dtAsistencia, function (index, row) {
					var html = "<tr class=\"unread\">"
						+ "<td class='text-center'>" + row.dia_semana + "</td>"
						+ "<td class='text-center'>" + row.fecha + "</td>"
						+ "<td class='text-center'><div class='forma-indicador ok " + row.flg_ok == 1 ? "" : "d-none" + "'></div></td>"
							+ "<td class='text-center'><div class='forma-indicador fm " + row.flg_fm == 1 ? "" : "d-none" + "'></div></td>"
								+ "<td class='text-center'><div class='forma-indicador ta " + row.flg_ta == 1 ? "" : "d-none" + "'></div></td>"
									+ "<td class='text-center'><div class='forma-indicador fi " + row.flg_fi == 1 ? "" : "d-none" + "'></div></td>"
										+ "<td class='text-center'><div class='forma-indicador ai " + row.flg_ai == 1 ? "" : "d-none" + "'></div></td>"
											+ "<td class='text-center'><div class='forma-indicador si " + row.flg_si == 1 ? "" : "d-none" + "'></div></td>"
												+ "<td class='text-center'><div class='forma-indicador rf " + row.flg_rf == 1 ? "" : "d-none" + "'></div></td>"
													+ "<td class='text-center'><div class='forma-indicador rs " + row.flg_rs == 1 ? "" : "d-none" + "'></div></td>"
														+ "<td class='text-center'><div class='forma-indicador rr " + row.flg_rr == 1 ? "" : "d-none" + "'></div></td>"
															+ "<td class='text-center'><div class='forma-indicador rn " + row.flg_rn == 1 ? "" : "d-none" + "'></div></td>"
																+ "<td class='text-center'><div class='forma-indicador rs " + row.flg_rs == 1 ? "" : "d-none" + "'></div></td>"
																+ "<td class='text-center'>" + row.excepcion + "</td>"
																+ "<td class='text-center'>" + row.hi + "</td>"
																+ "<td class='text-center'>" + row.hs + "</td>"
																+ "<td class='text-center'>" + row.th_hhmm + "</td>"
																+ "<td class='text-center'>" + row.tb_calculado + "</td>"
																+ "<td class='text-center'>" + row.htar + "</td>"
																+ "<td class='text-center'>" + row.the1 + "</td>"
																+ "<td class='text-center'>" + row.the2 + "</td>"
																+ "<td class='text-center'>" + row.the3 + "</td>"
																+ "<td class='text-center'>" + row.the7 + "</td>"
																+ "<td class='text-center'>" + row.the8 + "</td>"
																+ "<td class='text-center'>" + row.the4 + "</td>"
																+ "<td class='text-center'>" + row.the5 + "</td>"
																+ "<td class='text-center'>" + row.hai + "</td>"
																+ "<td class='text-center'>" + row.hsii + "</td>"
																+ "<td class='text-center'>" + row.hsis + "</td>"
																+ "<td class='text-center'>" + row.hsi_hhmm + "</td>"
																+ "<td class='text-center'>" + row.thec + "</td>"
																+ "<td class='text-center'>" + row.hrsa + "</td>"
																+ "<td class='text-center'>" + row.hrrd + "</td>"
																+ "<td class='text-center'>" + row.hrsi + "</td>"
																+ "<td class='text-center'>" + row.the6 + "</td>"
																+ "<td class='text-center'>" + row.cod_turno + "</td>"
																+ "<td class='text-center'>" + row.phi + "</td>"
																+ "<td class='text-center'>" + row.phs + "</td>"
																+ "<td class='text-center'>" + row.hei1 + "</td>"
																+ "<td class='text-center'>" + row.hei2 + "</td>"
																+ "<td class='text-center'>" + row.hes1 + "</td>"
																+ "<td class='text-center'>" + row.hes2 + "</td>"
																+ "<td class='text-center'>" + row.hpi + "</td>"
																+ "<td class='text-center'>" + row.hps + "</td>"
																+ "<td class='text-center'>" + row.bs + "</td>"
																+ "<td class='text-center'>" + row.bi + "</td>"
																+ "<td class='text-center'>" + row.ptb + "</td>"
																+ "<td class='text-center'>" + row.fecha_ingreso + "</td>"
																+ "<td class='text-center'>" + row.fecha_cese + "</td>"
																+ "<td class='text-center'>" + row.himax + "</td>"
																+ "<td class='text-center'>" + row.hsmax + "</td>"
																+ "<td class='text-center'>" + row.htarj + "</td>"
																+ "<td class='text-center'>" + row.th2 + "</td>"
																+ "<td class='text-center'>" + row.th3 + "</td>"
																+ "<td class='text-center'>" + row.th4 + "</td>"
																+ "<td class='text-center'>" + row.hei3 + "</td>"
																+ "<td class='text-center'>" + row.hes3 + "</td>"
																+ "<td class='text-center'>" + row.hina + "</td>"
																+ "<td class='text-center'>" + row.har + "</td>"
																+ "<td class='text-center'>" + row.cod_empresa + "</td>"
																+ "<td class='text-center'>" + row.cod_personal + "</td>"
																+ "<td class='text-center'>" + row.flg_origen + "</td>"
																+ "<td class='text-center'>" + row.phei3 + "</td>"
																+ "<td class='text-center'>" + row.phei2 + "</td>"
																+ "<td class='text-center'>" + row.phei1 + "</td>"
																+ "<td class='text-center'>" + row.pbs + "</td>"
																+ "<td class='text-center'>" + row.pbi + "</td>"
																+ "<td class='text-center'>" + row.phes1 + "</td>"
																+ "<td class='text-center'>" + row.phes2 + "</td>"
																+ "<td class='text-center'>" + row.phes3 + "</td>"
																+ "<td class='text-center'>" + row.pth + "</td>"
																+ "<td class='text-center'>" + row.cod_excepcion + "</td>"
					+ "</tr>";
					container.append(html);
				});
			}

			if (dtCuentasCorrientes.length > 0) {
				var container = $("#tbodyCuentasCorrientes");
				container.empty();
				$.each(dtCuentasCorrientes, function (index, row) {
					var html = "<tr class=\"unread\">"
						+ "<td class='text-center'>" + row.tipo_concepto + "</td>"
						+ "<td class='text-center'>" + row.cod_concepto + "</td>"
						+ "<td class='text-center'>" + row.nombre + "</td>"
						+ "<td class='text-center'>" + row.numero + "</td>"
						+ "<td class='text-center'>" + row.moneda + "</td>"
						+ "<td class='text-right'>" + row.monto + "</td>"
						+ "<td class='text-center'>" + row.cuota + "</td>"
						+ "<td class='text-center'>" + row.fecha + "</td>"
						+ "<td class='text-right'>" + row.monto_prog + "</td>"
						+ "</tr>";
					container.append(html);
				})
			}

			if (dtSaldoDeudor.length > 0) {
				$('#lblPrestamoDeudor').text(dtSaldoDeudor[0].prestamo_deudor);
				$('#lblSaldoDeudor').text(dtSaldoDeudor[0].saldo_deudor);

			}

			if (dtRetencionJudicial.length > 0) {
				var container = $("#tbodyRetencionJudicial");
				container.empty();
				$.each(dtRetencionJudicial, function (index, row) {
					var html = "<tr class=\"unread\">"
						+ "<td class='text-center'>" + row.beneficiario + "</td>"
						+ "<td class='text-right'>" + row.valor_porcen + "</td>"
						+ "<td class='text-right'>" + row.monto + "</td>"
						+ "<td class='text-center'>" + row.afecto + "</td>"
						+ "<td class='text-center'>" + row.distrito_judicial + "</td>"
						+ "<td class='text-center'>" + row.juzgado + "</td>"
						+ "<td class='text-center'>" + row.expediente + "</td>"
						+ "<td class='text-center'>" + row.secretario + "</td>"
						+ "</tr>";
					container.append(html);
				});
			}

			if (dtCeseTrabajador.length > 0) {
				$('#lblFechaIngreso').text(dtCeseTrabajador[0].fecha_ingreso);
				$('#lblMotivoIngreso').text(dtCeseTrabajador[0].nom_motivo_ingreso);

				if (dtCeseTrabajador[0].estado == 'A') $('#rbActivo').prop('checked', true);
				else $('#rbCesado').prop('checked', true);

				$('#textFechaCese').val(dtCeseTrabajador[0].fecha_cese);
				$('#textTipoCambio').val((dtCeseTrabajador[0].tipo_cambio_cese == '' ? 0 : dtCeseTrabajador[0].tipo_cambio_cese));
				$('#selectMotivoCese').val(dtCeseTrabajador[0].cod_motivo_cese);
				//$('#textFechaLiquidacion').val(dtCeseTrabajador[0].fecha_liquidacion);
				$('#selectSaldoCtaCte').val(dtCeseTrabajador[0].flg_saldo_ctacte_boleta);
				$('#textMotivoCese').val(dtCeseTrabajador[0].motivo_cese);
				$('#textMotivoNoContra').val(dtCeseTrabajador[0].motivo_nocontr);
				$('#chkNoContra').prop('checked', (dtCeseTrabajador[0].flg_nocontratable == 'S') ? true : false)
				if (dtCeseTrabajador[0].flg_mostrar_calcular == 0) {
					$('#chkCalcular').prop('checked', false);
					$('#chkCalcular').parent().addClass('d-none');
				}
				else {
					$('#chkCalcular').prop('checked', true);
					$('#chkCalcular').parent().removeClass('d-none');
				}
			}
			containerPage.unmaskContainer();
		}
	});
};

async function AnularCese() {
	GetFiltros();
	var jsonData = {
		personal: g_codPersonal,
		planilla: g_tipoplanilla,
		anual: getPeriodo(),
		mes: g_mes,
		numero: g_numero,
		proceso: g_proceso,
		claseProceso: g_claseProceso,
		desde: g_desde,
		hasta: g_hasta
	};
	await UIAjax.runFunctionAsync('Planillas', 'AnularCese', jsonData, function () {
		GetModalDetalle(g_codPersonal, g_numIngreso, g_miembro_comite, g_miembro_comite_desde, g_miembro_comite_hasta, g_estadotrab);
		GetPersonalCalculo();
	}, null, function () {
		UILoadingDefault("hide");
	});
}

async function CeseTrabajador(p_flg_calcular, p_flg_no_contra, p_motivo_no_contratable, p_fecha_cese, p_cod_motivo_cese, p_motivo_cese, p_flgSaldo, p_tipoCambio) {
	GetFiltros();
	var jsonData = {
		personal: g_codPersonal,
		planilla: g_tipoplanilla,
		anual: getPeriodo(),
		mes: g_mes,
		numero: g_numero,
		proceso: g_proceso,
		claseProceso: g_claseProceso,
		desde: g_desde,
		hasta: g_hasta,
		flgCalcular: p_flg_calcular,
		flgNoContra: p_flg_no_contra,
		motivoNoContra: p_motivo_no_contratable,
		fechaCese: p_fecha_cese,
		codMotivoCese: p_cod_motivo_cese,
		motivoCese: p_motivo_cese,
		flgSaldo: p_flgSaldo,
		tipoCambio: p_tipoCambio
	};
	await UILoadingDefault('show');
	await UIAjax.runFunctionAsync('Planillas', 'CeseTrabajador', jsonData, function () {
		GetModalDetalle(g_codPersonal, g_numIngreso, g_miembro_comite, g_miembro_comite_desde, g_miembro_comite_hasta, g_estadotrab);
		GetPersonalCalculo();
	}, null, function () {
		containerPage.unmaskContainer();
	});
	UILoadingDefault('hide');
}

async function GenerarCalculo(p_fecha_cese, p_cod_motivo, p_motivo_cese, p_lista) {
	GetFiltros();
	var params = {
		planilla: g_tipoplanilla,
		anual: getPeriodo(),
		mes: g_mes,
		numero: g_numero,
		proceso: g_proceso,
		claseProceso: g_claseProceso,
		desde: g_desde,
		hasta: g_hasta,
		fechaCese: p_fecha_cese,
		codMotivoCese: p_cod_motivo,
		motivoCese: p_motivo_cese,
		listaPersonal: p_lista
	};
	await UIAjax.runFunctionAsync('Planillas', 'GenerarCalculo', params, function () {
		GetPersonalCalculo();
	}, null, function () {
		UILoadingDefault("hide");
	});
}