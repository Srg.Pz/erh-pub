﻿//$(document).ready(function () {
//    console.log("hola");
//});

var tableParametrosGI;

$('#btnItemCI').on('click', function (e) {
    $('#aTabSQL1').click();
    //ChangeProceso();
    $('#selectProcesoCI').prop('selectedIndex', 1).trigger('change');
    $('#modalCI').modal('show');
});

$('#btnItemGI').on('click', function (e) {
    var _selectPeriodo = $('#selectPeriodo');
    if (_selectPeriodo.length == 1) {
        var json = JSON.parse(_selectPeriodo.val());
        if (UIEmpty(json)) {
            MsgBox("Seleccione un periodo primero.", "warning", true);
            return;
        }
    }
    var _selectProceso = $('#selectProceso');
    if (_selectProceso.length == 1) {
        if (isEmpty(_selectProceso.val())) {
            MsgBox("Seleccione un proceso primero.", "warning", true);
            return;
        }
    }
    $('#aTabIngreseValores').click();
    $('#modalGI').modal('show');
});

$('#btnItemGenerarAsiento').on('click', async function (e) {
    $.LoadingOverlay("show");
    var TabPagos = $('#aTabPagos');
    var valProceso = '';
    var valClaseDeProceso = '';
    if (TabPagos.length == 1) {
        if (TabPagos.hasClass('active')) {
            valProceso = 'P';
            valClaseDeProceso = $('input[name="gpPagos"]:checked').val();
        }
        else {
            //aTabProvisiones
            valProceso = 'V';
            valClaseDeProceso = $('input[name="gpProvisiones"]:checked').val();
        }
    }
    else {
        var ClaseProceso = $('input[name="gpPagos"]');
        if (ClaseProceso.length > 0) {
            valProceso = 'P';
            valClaseDeProceso = ClaseProceso.filter(':checked').val();
        }
    }

    var CodigoAnual = '', CodigoMes = '', Numero = '';
    var Periodos = $('#selectPeriodo');
    if (Periodos.length == 1) {
        var jsonRow = JSON.parse(Periodos.val());
        if (!isEmpty(jsonRow)) {
            CodigoAnual = jsonRow.cod_anual;
            CodigoMes = jsonRow.mes;
            Numero = jsonRow.numero;
        }
    }

    var jsonParam = {
        CodigoDeTipoPlanilla: $('#selectTipoPlanilla').val(),
        CodigoAnual: CodigoAnual,
        CodigoMes: CodigoMes,
        CodigoDeProceso: valProceso,
        CodigoClaseDeProceso: valClaseDeProceso,
        Numero: Numero,
        Tipo: 'D',
    };
    let r = await UIAjax.invokeFunctionAsync('AjaxInterfases', 'PostGenerarAsiento', jsonParam)
    $('#txtCuerpoEnvio').val(r.strenvio)
    if (r.strmsje != "OK") {
        MsgBox(r.strmsje, 'warning');
        $('#h4TitleAsiento').text("Error al generar el asiento.")
        $('#h4MessageAsiento').text(r.strmsje)
    } else {
        let rpta = JSON.parse(r.strrpta)
        MsgBox("Se generó el asiento correctamente. " + rpta.data.MESSAGE, 'success', true);
        $('#h4TitleAsiento').text("Se generó el asiento correctamente.")
        $('#h4MessageAsiento').text(rpta.data.MESSAGE)
    }
    $.LoadingOverlay("hide");
    $('#modalResponseAsiento').modal('show')
});

$('#selectProcesoCI').on('change', function (e) {
    ChangeProceso();
});

$('#tableParametrosCI').on('click', 'tbody tr', function (e) {
    $('#tableParametrosCI tbody tr').removeClass('selected');
    var ths = $(this);
    ths.addClass('selected');
}).on('change', 'tbody tr td select', function (e) {
    var ths = $(this);
    var cell = ths.closest('td');
    var row = ths.closest('tr');
    //tableConceptosVariables.cell($(cell)).invalidate().draw();
    tableParametrosCI.cell($(cell)).data(ths.val());
    //tableParametrosCI.rows().invalidate().draw();
    tableParametrosCI.row(row).invalidate().draw();
}).on('click', 'tbody tr td input', function (e) {
    var ths = $(this);
    ths.removeClass('input_empty');
}).on('change', 'tbody tr td input', function (e) {
    var ths = $(this);
    var cell = ths.closest('td');
    var row = ths.closest('tr');
    var cellIndex = tableParametrosCI.cell(cell).index();
    if (cellIndex.column == 4) {
        var jsonRow = tableParametrosCI.row(row).data();
        switch (jsonRow.tipo_dato) {
            case "E":
                tableParametrosCI.row(row).data().valor_defecto_entero = parseInt(ths.val());
                break;
            case "I":
                tableParametrosCI.row(row).data().valor_defecto_importe = parseFloat(ths.val());
                break;
            case "F":
                tableParametrosCI.row(row).data().valor_defecto_fecha = ths.val();
                break;
            default:
                tableParametrosCI.row(row).data().valor_defecto_cadena = ths.val();
                break;
        }
    }
    else {
        tableParametrosCI.cell($(cell)).data(ths.val());
    }
    //ths.blur();
    //console.log(tableParametrosCI.cell(cell).index());
    //tableParametrosCI.row(row).invalidate().draw();
    //tableParametrosCI.rows().invalidate().draw();
});

$('#tableValidacionesCI').on('click', 'tbody tr', function (e) {
    $('#tableValidacionesCI tbody tr').removeClass('selected');
    var ths = $(this);
    //ths.addClass('selected');
}).on('click', 'tbody tr td input[type="text"]', function (e) {
    //e.stopPropagation();
    //$('#tableParametrosCI tbody tr').removeClass('selected');
    var ths = $(this);
    var Col = ths.closest('td');
    var Row = ths.closest('tr');
    //console.log(tableParametrosCI.cell(Col).index().columnVisible);
    //console.log(tableParametrosCI.cell(Col).index());
    ths.removeClass('input_empty');
    Row.attr('data-input', tableValidacionesCI.cell(Col).index().columnVisible);
}).on('change', 'tbody tr td input', function (e) {
    var ths = $(this);
    var cell = ths.closest('td');
    var row = ths.closest('tr');
    var cellIndex = tableValidacionesCI.cell(cell).index();
    tableValidacionesCI.cell($(cell)).data(ths.val());
});

$('#tableParametrosGI').on('click', 'tbody tr td input', function (e) {
    var ths = $(this);
    ths.removeClass('input_empty');
}).on('change', 'tbody tr td input', function (e) {
    var ths = $(this);
    var cell = ths.closest('td');
    var row = ths.closest('tr');
    var cellIndex = tableParametrosGI.cell(cell).index();
    var jsonRow = tableParametrosGI.row(row).data();
    switch (jsonRow.tipo_dato) {
        case "E":
            tableParametrosGI.row(row).data().valor_defecto_entero = parseInt(ths.val());
            break;
        case "I":
            tableParametrosGI.row(row).data().valor_defecto_importe = parseFloat(ths.val());
            break;
        case "F":
            tableParametrosGI.row(row).data().valor_defecto_fecha = ths.val();
            break;
        default:
            tableParametrosGI.row(row).data().valor_defecto_cadena = ths.val();
            break;
    }
    //console.log(tableParametrosGI.row(row).data());
    //ths.blur();
    //console.log(tableParametrosCI.cell(cell).index());
    //tableParametrosCI.row(row).invalidate().draw();
    //tableParametrosCI.rows().invalidate().draw();
});

$('#selectSQLCI').on('click', function (e) {
    var ths = $(this);
    var valor = ths.val();
    if (!UIEmpty(valor)) {
        var inputArea = $('#tabContentSQL div.active.show textarea');
        if (inputArea.length == 1) {
            inputArea.appendVal(':' + valor);
        }
    }
});

$('#selectValidacionesCI').on('click', function (e) {
    var ths = $(this);
    var Row = $('#tableValidacionesCI tbody tr.selected');
    if (Row.length == 0) {
        Row = $('#tableValidacionesCI tbody tr:last-child');
    }
    var indexInput = parseInt(Row.attr('data-input')) + 1;
    var input = Row.find('td:nth-child(' + indexInput + ') > input:first-child');
    //console.log(indexInput);
    input.appendVal(ths.val());
    //$(''+indexInput)
});

$('body').on('click', '#btnTodosColaboradores', function (e) {
    var ths = $(this);
    var boolChk = ths.toggleClass('btn-secondary btn-outline-secondary').hasClass('btn-secondary');
    tableColaboradores.rows().every(function (rowIdx, tableLoop, rowLoop) {
        var node = this.node();
        $(node).toggleClass('selected', boolChk);
    });
    var neto = '0', a_pagar = '0';
    if (boolChk) {
        var jsonAjax = tableColaboradores.ajax.json();
        neto = jsonAjax.data.sum("neto")
        a_pagar = jsonAjax.data.sum("a_pagar");
    }
    UpdateTotalColaboradores(neto, a_pagar);
});

$('#btnAdicionarCI').on('click', function (e) {
    $('#selectProcesoCI').val('00').trigger('change');
});

$('#btnEliminarCI').on('click', function (e) {
    var valor = $('#selectProcesoCI').val();
    if (valor == '00') {
        MsgBox("No se puede eliminar un registro no guardado", "warning", true);
        return;
    }
    MsgBox("question", function () {
        var jsonParam = {
            Tipo: TipoDeInterface,
            CodigoDeInterface: valor
        };
        $.ajax({
            type: "DELETE",
            url: portal + "/ajax/interfases/DeleteInterfaceProceso",
            data: jsonParam,
            dataType: "json",
            success: function success(data) {
                if (data.respuesta == 1) {
                    LoadSelectProcesoCI();
                    var container = $('#selectProcesoCI');
                    container.val(container.children('option:nth-child(2)').val()).change();
                    MsgBox("success");
                }
                else {
                    MsgBox("warning");
                }
            },
            error: function error(data) {
                MsgBox("error");
            }
        });
    });
});

$('#btnRefrescarCI').on('click', function (e) {
    LoadSelectProcesoCI();
    ChangeProceso();
});

$('#btnGuardarCI').on('click', function (e) {
    if (ValidarTableParametrosCI()) {
        return;
    }
    if (ValidarTableValidacionesCI()) {
        return;
    }
    var jsonProceso = {
        TipoDeInterface: TipoDeInterface,
        CodigoDeInterface: $('#selectProcesoCI').val(),
        Descripcion: $('#txtInterfaceCI').val(),
        Extension: $('#txtEntensionCI').val(),
        Formato: $('#selectTipoCI').val(),
        Query1: $('#txtSQL1CI').val(),
        Query2: $('#txtSQL2CI').val(),
        Query3: $('#txtSQL3CI').val(),
        Query4: $('#txtSQL4CI').val(),
        RutaDeArchivo: $('#txtRutaCI').val(),
        NombreDeArchivo: $('#txtArchivoCI').val(),
        Orden: "",
        FlgFormato: $('#selectFormatoCI').val(),
        IProcesoParam: [],
        IProcesoValidaciones: []
    };
    var AProcesoParam = Array.from(tableParametrosCI.rows().data(), function (x) {
        return {
            TipoDeInterface: jsonProceso.TipoDeInterface,
            CodigoDeInterface: x.cod_interface,
            Item: x.item,
            Nombre: x.nombre,
            NombreDeVariable: x.nombre_variable,
            TipoDeDato: x.tipo_dato,
            ValorDefectoCadena: x.valor_defecto_cadena,
            ValorDefectoEntero: x.valor_defecto_entero,
            ValorDefectoImporte: x.valor_defecto_importe,
            ValorDefectoNumero: x.valor_defecto_numero,
            ValorDefectoFecha: x.valor_defecto_fecha,
            Orden: x.orden,
        };
    });
    jsonProceso.IProcesoParam = AProcesoParam;

    var AProcesoValidaciones = Array.from(tableValidacionesCI.rows().data(), function (x) {
        return {
            TipoDeInterface: jsonProceso.TipoDeInterface,
            CodigoDeInterface: x.cod_interface,
            Secuencia: x.secuencia,
            Descripcion: x.descripcion,
            Validacion: x.validacion,
            Mensaje: x.mensaje,
        };
    });

    jsonProceso.IProcesoValidaciones = AProcesoValidaciones;
    //var abc = $('#txtRutaCI').val();
    //console.log(abc);
    ////console.log(Object.isString($('#txtRutaCI').val()));
    //console.log(typeof abc);
    //console.log(jsonProceso);
    //return;
    $.ajax({
        type: "POST",
        url: portal + "/ajax/interfases/PostInterfaceProcesoWithDetails",
        data: jsonProceso,
        dataType: "json",
        success: function success(data) {
            if (data.respuesta == 1) {
                LoadSelectProcesoCI();
                MsgBox("success");
            }
            else {
                MsgBox("warning");
            }
        },
        error: function error(data) {
            MsgBox("error");
        }
    });

});

$('#btnAdicionarValidaciones').on('click', function (e) {
    var jsonData = tableValidacionesCI.rows().data();
    var MaxNumero = jsonData.reduce(function (max, x) { return (x.secuencia > max) ? x.secuencia : max; }, 0);
    tableValidacionesCI.row.add({
        secuencia: MaxNumero + 1,
        tipo_interface: TipoDeInterface,
        cod_interface: $('#selectProcesoCI').val(),
        validacion: "",
        mensaje: "",
        descripcion: ""
    }).draw();
    //console.log(tableValidacionesCI.rows().data());
});

$('#btnEliminarValidaciones').on('click', function (e) {
    var rowDeleted = $('#tableValidacionesCI tbody tr:last-child');
    //tableParametrosCI.row(rowDeleted).remove().draw();
    var jsonRow = tableValidacionesCI.row(rowDeleted).data();
    if (!isEmpty(jsonRow)) {
        var jsonParam = {
            Tipo: jsonRow.tipo_interface,
            CodigoDeInterface: jsonRow.cod_interface,
            Secuencia: jsonRow.secuencia
        };
        $.ajax({
            type: "DELETE",
            url: portal + "/ajax/interfases/DeleteInterfaceProcesoValidaciones",
            data: jsonParam,
            dataType: "json",
            success: function success(data) {
                if (data.respuesta == 1) {
                    tableValidacionesCI.row(rowDeleted).remove().draw();
                    MsgBox("success");
                }
                else {
                    MsgBox("warning");
                }
            },
            error: function error(data) {
                MsgBox("error");
            }
        });
    }
    else {
        MsgBox("No hay registros disponibles para eliminación", "warning", true);
    }
});

$('#btnAdicionarTabSQL4').on('click', function (e) {
    var jsonData = tableParametrosCI.rows().data();
    var MaxNumero = jsonData.reduce(function (max, x) { return (x.item > max) ? x.item : max; }, 0);
    ////nombre//nombre_variable//tipo_dato//valor_defecto_cadena
    tableParametrosCI.row.add({
        tipo_interface: TipoDeInterface,
        cod_interface: $('#selectProcesoCI').val(),
        item: MaxNumero + 1,
        nombre: "",
        nombre_variable: "",
        tipo_dato: "T",
        valor_defecto_cadena: "",
        valor_defecto_entero: 0,
        valor_defecto_importe: 0,
        valor_defecto_numero: 0,
        valor_defecto_fecha: null,
        orden: MaxNumero + 1
    }).draw();
});

$('#btnEliminarTabSQL4').on('click', function (e) {
    var rowDeleted = $('#tableParametrosCI tbody tr:last-child');
    //tableParametrosCI.row(rowDeleted).remove().draw();
    var jsonRow = tableParametrosCI.row(rowDeleted).data();
    if (!isEmpty(jsonRow)) {
        var jsonParam = {
            Tipo: jsonRow.tipo_interface,
            CodigoDeInterface: jsonRow.cod_interface,
            Item: jsonRow.item
        };
        $.ajax({
            type: "DELETE",
            url: portal + "/ajax/interfases/DeleteInterfaceProcesoParam",
            data: jsonParam,
            dataType: "json",
            success: function success(data) {
                if (data.respuesta == 1) {
                    tableParametrosCI.row(rowDeleted).remove().draw();
                    MsgBox("success");
                }
                else {
                    MsgBox("warning");
                }
            },
            error: function error(data) {
                MsgBox("error");
            }
        });
    }
    else {
        MsgBox("No hay registros disponibles para eliminación", "warning", true);
    }
});

$('#modalGI').on('show.bs.modal', async function (e) {
    await LoadModalGI();
});

$('#btnProcesarGI').on('click', function (e) {

    $('#aTabResultados').click();

    //var Proyecto = $('#selectProyecto');
    //var valProyecto = '';
    //if (Proyecto.length == 1) {
    //    valProyecto = Proyecto.val();
    //}

    //var CentrosCosto = $('#selectCentrosCosto');
    //var valCentrosCosto = '';
    //if (CentrosCosto.length == 1) {
    //    valCentrosCosto = CentrosCosto.val();
    //}

    //var UnidadesNegocio = $('#selectUnidadesNegocio');
    //var valUnidadesNegocio = '';
    //if (UnidadesNegocio.length == 1) {
    //    valUnidadesNegocio = UnidadesNegocio.val();
    //}

    //var Area = $('#selectArea');
    //var valArea = '';
    //if (Area.length == 1) {
    //    valArea = Area.val();
    //}

    //var Localidad = $('#selectLocalidad');
    //var valLocalidad = '';
    //if (Localidad.length == 1) {
    //    valLocalidad = Localidad.val();
    //}

    //var TipoPlanilla = $('#selectTipoPlanilla');
    //var valTipoPlanilla = '';
    //if (TipoPlanilla.length == 1) {
    //    if (TipoPlanilla.prop("multiple")) {
    //        valTipoPlanilla = TipoPlanilla.val().join(",");
    //    }
    //    else {
    //        valTipoPlanilla = TipoPlanilla.val();
    //    }
    //}

    //var TabPagos = $('#aTabPagos');
    //var valProceso = '';
    //var valClaseDeProceso = '';
    //if (TabPagos.length == 1) {
    //    if (TabPagos.hasClass('active')) {
    //        valProceso = 'P';
    //        valClaseDeProceso = $('input[name="gpPagos"]:checked').val();
    //    }
    //    else {
    //        //aTabProvisiones
    //        valProceso = 'V';
    //        valClaseDeProceso = $('input[name="gpProvisiones"]:checked').val();
    //    }
    //}
    //else {
    //    var ClaseProceso = $('input[name="gpPagos"]');
    //    if (ClaseProceso.length > 0) {
    //        valProceso = 'P';
    //        valClaseDeProceso = ClaseProceso.filter(':checked').val();
    //    }
    //}

    //var FechaPeriodoDesde = $('#txtFechaPeriodoDesde');
    //var valFechaPeriodoDesde = '';
    //if (FechaPeriodoDesde.length == 1) {
    //    valFechaPeriodoDesde = FechaPeriodoDesde.val();
    //}

    //var FechaPeriodoHasta = $('#txtFechaPeriodoHasta');
    //var valFechaPeriodoHasta = '';
    //if (FechaPeriodoHasta.length == 1) {
    //    valFechaPeriodoHasta = FechaPeriodoHasta.val();
    //}

    //var CodigoAnual = '', CodigoMes = '';
    //var Periodos = $('#selectPeriodo');
    //if (Periodos.length == 1) {
    //    var jsonRow = JSON.parse(Periodos.val());
    //    if (!isEmpty(jsonRow)) {
    //        CodigoAnual = jsonRow.cod_anual;
    //        CodigoMes = jsonRow.mes;
    //        if (valFechaPeriodoDesde == '') {
    //            valFechaPeriodoDesde = jsonRow.desde;
    //        }
    //        if (valFechaPeriodoHasta == '') {
    //            valFechaPeriodoHasta = jsonRow.hasta;
    //        }
    //    }
    //}

    //var valPersonal = '';
    //var Colaboradores = $('#tableColaboradores');
    //if (Colaboradores.length == 1) {
    //    var TodosColaboradores = $('#btnTodosColaboradores');
    //    if (TodosColaboradores.length == 1 && TodosColaboradores.hasClass('btn-secondary')) {
    //        valPersonal = '%';
    //    }
    //    else {
    //        var Rows = tableColaboradores.rows('.selected').data();
    //        var ACodigo = Array.from(Rows, function (p) { return p.cod_personal });
    //        valPersonal = ACodigo.join(",");
    //    }
    //}

    //var FiltroTipo = $('input[name="gpFiltroTipo"]');
    //var valFiltroTipo = '';
    //if (FiltroTipo.length > 0) {
    //    valFiltroTipo = FiltroTipo.filter(':checked').val();
    //}

    //var Estado = $('#selectEstado');
    //var valEstado = '';
    //if (Estado.length == 1) {
    //    valEstado = Estado.val();
    //}

    //var FechaDeposito = $('#txtFechaDeposito');
    //var valFechaDeposito = '';
    //if (FechaDeposito.length == 1) {
    //    valFechaDeposito = FechaDeposito.val();
    //}

    //var Porcentaje = $('#txtPorcentaje');
    //var valPorcentaje = '';
    //if (Porcentaje.length == 1) {
    //    valPorcentaje = Porcentaje.val();
    //}

    //var Moneda = $('#selectMoneda');
    //var valMoneda = '';
    //if (Moneda.length == 1) {
    //    valMoneda = Moneda.val();
    //}

    //var EntidadDeProceso = $('#selectProceso');
    //var valEntidadDeProceso = '';
    //if (EntidadDeProceso.length == 1) {
    //    var jsonOption = $('option:selected', EntidadDeProceso).attr('data-json');
    //    if (jsonOption != undefined || jsonOption != null) {
    //        valEntidadDeProceso = JSON.parse(jsonOption).cod_entidad;
    //    }
    //}

    //var NombreDeArchivo = $('#txtNombreDeArchivo');
    //var valNombreDeArchivo = '';
    //if (NombreDeArchivo.length == 1) {
    //    valNombreDeArchivo = NombreDeArchivo.val();
    //}

    //var jsonParam = {
    //    Tipo: TipoDeInterface,
    //    CodigoDeInterface: $('#selectProceso').val(),
    //    CodigoDeProyecto: valProyecto,
    //    CodigoDeCC: valCentrosCosto,
    //    CodigoDeUN: valUnidadesNegocio,
    //    CodigoDeArea: valArea,
    //    CodigoDeLocalidad: valLocalidad,
    //    CodigoAnual: CodigoAnual,
    //    CodigoMes: CodigoMes,
    //    CodigoDeAfp: '%',
    //    CodigoDeTipoPlanilla: valTipoPlanilla,
    //    CodigoClaseDeProceso: UIValue(valClaseDeProceso, 'B'),
    //    CodigoDeProceso: UIValue(valProceso, 'P'),
    //    FechaDeInicio: valFechaPeriodoDesde,
    //    FechaDeFin: valFechaPeriodoHasta,
    //    Filtro: valFiltroTipo,
    //    Estado: valEstado,
    //    FechaDeDeposito: valFechaDeposito,
    //    CodigoDePersonal: valPersonal,
    //    PorcetajeDeDeposito: valPorcentaje,
    //    CodigoDeMoneda: valMoneda,
    //    CodigoEntidadDeProceso: valEntidadDeProceso,
    //    NombreDeArchivo: valNombreDeArchivo,
    //    Table: []
    //};

    //if ($.fn.dataTable.isDataTable('#tableResultadosGI')) {
    //    $('#tableResultadosGI').DataTable().clear().destroy();
    //    $('#tableResultadosGI').empty();
    //}
    //if ($.fn.dataTable.isDataTable('#tableParametrosGI')) {
    //    tableParametrosGI.rows().invalidate().draw();
    //    var AValidaciones = Array.from(tableParametrosGI.rows().data(), function (x) {
    //        var val = '';
    //        switch (x.tipo_dato) {
    //            case "E":
    //                val = x.valor_defecto_entero;
    //                break;
    //            case "I":
    //                val = x.valor_defecto_importe;
    //                break;
    //            case "F":
    //                val = x.valor_defecto_fecha;
    //                break;
    //            default:
    //                val = x.valor_defecto_cadena;
    //                break;
    //        }
    //        return {
    //            id: 1,
    //            Nombre: x.nombre_variable,
    //            Valor: val
    //        };
    //    });
    //    jsonParam.Table = AValidaciones;
    //}

    var jsonParam = LoadFiltro();

    $.ajax({
        type: "POST",
        url: portal + "/ajax/interfases/GetGenerarInterface",
        data: jsonParam,
        dataType: "json",
        async: false,
        success: function success(jsonAjax) {
            var textSelected = $('#selectProceso').find('option:selected').html();
            var columns = [];
            $.each(jsonAjax.keys, function (index, value) {
                columns.push({ title: jsonAjax.title[index].replace(/_/g, " "), data: value });
            });
            $('#tableResultadosGI').DataTable({
                data: jsonAjax.data,
                initComplete: function (settings, json) {
                    $('#tableResultadosGI thead').addClass('thead-sky thead-sticky');
                    $('#txtResultadoGI').val(`Generando ${textSelected}\r\nSe recuperaron ${jsonAjax.recordsTotal} filas`);
                    if (!isEmpty(jsonAjax.mensaje)) {
                        $('#txtResultadoGI').appendVal(`\r\n${jsonAjax.mensaje}`);
                    }
                    if (!jsonAjax.success) {
                        $('#tableResultadosGI').empty();
                    }
                    if (jsonAjax.success) {
                        var jsonParamDownload = {
                            TableStr: JSON.stringify(jsonParam.Table)
                        };
                        $.extend(true, jsonParamDownload, jsonParam);
                        UISave.URL(portal + '/Interfases/DownloadGenerarInterface/?' + $.param(jsonParamDownload));
                    }
                    else {
                        MsgBox("No se puede procesar, ya que existen errores.", "warning", false);
                    }
                },
                searching: false,
                paging: false,
                columns: columns,
                order: false,
                info: false
            });
        },
        error: function error(jsonAjax) { }
    });
});

function LoadSelectProcesoCI() {
    var container = $('#selectProcesoCI');
    var valSelect = container.val();
    container.empty();
    $.ajax({
        type: "GET",
        url: portal + "/ajax/interfases/GetInterfaceProceso",
        data: { Tipo: TipoDeInterface },
        dataType: "json",
        contentType: "application/json;charset=utf-8;",
        async: false,
        success: function success(data) {
            var AProcesoParam = Array.from(data, function (x) { return $('<option>', { value: x.cod_interface, text: x.descripcion }) });
            container.append(AProcesoParam);
        },
        error: function error(data) {
        }
    });
    container.children('option:first-child').addClass('font-weight-bold');
    if (valSelect == '00') {
        valSelect = container.children('option:last-child').val();
    }
    container.val(valSelect);
};

function ChangeProceso() {

    $('#txtArchivoCI').val('');
    $('#txtRutaCI').val('');
    $('#txtSQL1CI').val('');
    $('#txtSQL2CI').val('');
    $('#txtSQL3CI').val('');
    $('#txtSQL4CI').val('');

    var select = $('#selectProcesoCI');
    var valueSelect = select.val();
    var textSelect = select.find('option:selected').html();
    if (valueSelect == '00') {
        valueSelect = '';
        textSelect = '';
        $('#txtEntensionCI').val('');
        $('#selectTipoCI').val('');
        $('#selectFormatoCI').val('');

        if (!$.fn.dataTable.isDataTable('#tableParametrosCI')) {
            $('#tableParametrosCI').DataTable();
        }
        $('#tableParametrosCI').DataTable().clear().destroy();
        $('#tableParametrosCI tbody').empty();

        if (!$.fn.dataTable.isDataTable('#tableValidacionesCI')) {
            $('#tableValidacionesCI').DataTable();
        }
        $('#tableValidacionesCI').DataTable().clear().destroy();
        $('#tableValidacionesCI tbody').empty();
    }
    else {

        var jsonParam = {
            Tipo: TipoDeInterface,
            CodigoDeInterface: valueSelect
        };
        $.ajax({
            type: "GET",
            url: portal + "/ajax/interfases/GetInterfaceProceso",
            data: jsonParam,
            dataType: "json",
            contentType: "application/json;charset=uft-8;",
            success: function success(data) {
                var jsonSingle = data[1];
                $('#txtEntensionCI').val(jsonSingle.extension);
                $('#selectFormatoCI').val(jsonSingle.flg_formato);
                $('#selectTipoCI').val(jsonSingle.formato);
                $('#txtArchivoCI').val(jsonSingle.nombre_archivo);
                $('#txtRutaCI').val(jsonSingle.ruta_archivo);

                $('#txtSQL1CI').val(jsonSingle.query1);
                $('#txtSQL2CI').val(jsonSingle.query2);
                $('#txtSQL3CI').val(jsonSingle.query3);
                $('#txtSQL4CI').val(jsonSingle.query4);

            },
            error: function error(data) {
            }
        });
        tableParametrosCI = $('#tableParametrosCI').on("draw.dt", function () {
            $(this).find(".dataTables_empty").parents('tbody').empty();
        }).DataTable({
            ajax: portal + "/ajax/interfases/GetInterfaceProcesoParamSingle/?" + $.param(jsonParam),
            initComplete: function () {
            },
            language: {
                url: '/js/language.js'
            },
            bSort: false,
            bDestroy: true,
            order: false,
            info: false,
            paging: false,
            searching: false,
            //"drawCallback": function (settings) {
            //    var api = this.api();
            //    console.log(settings);
            //    console.log($(this));
            //    console.log(api);
            //    // Output the data for the visible rows to the browser's console
            //    console.log(api.rows({ page: 'current' }).data());
            //},
            //select: {
            //    style: 'single'
            //},
            //columnDefs: [
            //    { orderable: false, targets: '_all' }
            //],
            columns: [
                //nombre//nombre_variable//tipo_dato//valor_defecto_cadena
                { data: "item" },
                {
                    data: "nombre", render: function (data, type, row) {
                        return '<input type="text" value="' + row.nombre + '" class="form-control form-control-sm" spellcheck="false" autocomplete="off" />';
                    }
                },
                {
                    data: "nombre_variable", render: function (data, type, row) {
                        return '<input type="text" value="' + row.nombre_variable + '" class="form-control form-control-sm" spellcheck="false" autocomplete="off" />';
                    }
                },
                {
                    data: "tipo_dato", render: function (data, type, row) {

                        var selectDynamic = beginHTML({
                            type: "select",
                            class: "form-control form-control-sm mr-5",
                            id: "ejemploselect",
                            selected: row.tipo_dato,
                            content: [
                                { type: "option", value: "T", html: "Texto" },
                                { type: "option", value: "E", html: "Número Entero" },
                                { type: "option", value: "I", html: "Importe" },
                                { type: "option", value: "F", html: "Fecha" }
                            ]
                        });

                        return selectDynamic;

                    }
                },
                {
                    data: "tipo_dato", render: function (data, type, row, settings) {
                        var html;
                        var typeInput, valInput;
                        switch (data) {
                            case "E":
                                typeInput = "number";
                                valInput = row.valor_defecto_entero;
                                break;
                            case "I":
                                typeInput = "number";
                                valInput = row.valor_defecto_importe;
                                break;
                            case "F":
                                typeInput = "date";
                                valInput = row.valor_defecto_fecha;
                                break;
                            default:
                                typeInput = "text";
                                valInput = row.valor_defecto_cadena;
                                break;
                        }
                        //html = '<input type="'+ typeInput +'" value="' + valInput + '" class="form-control form-control-sm" />';
                        html = `<input type="${typeInput}" value="${UIValue(valInput)}" class="form-control form-control-sm" />`;
                        return html;
                    }
                }
            ]
        });
        tableValidacionesCI = $('#tableValidacionesCI').on("draw.dt", function () {
            $(this).find(".dataTables_empty").parents('tbody').empty();
        }).DataTable({
            ajax: portal + "/ajax/interfases/GetInterfaceProcesoValidacionesSingle/?" + $.param(jsonParam),
            initComplete: function () {
            },
            language: {
                url: '/js/language.js'
            },
            bDestroy: true,
            order: false,
            info: false,
            paging: false,
            searching: false,
            columns: [
                { data: "secuencia" },
                {
                    data: "descripcion", render: function (data, type, row) {
                        return '<input type="text" value="' + row.descripcion + '" class="form-control form-control-sm" spellcheck="false" autocomplete="off" />';
                    }
                },
                {
                    data: "validacion", render: function (data, type, row) {
                        return '<input type="text" value="' + row.validacion + '" class="form-control form-control-sm" spellcheck="false" autocomplete="off" />';
                    }
                },
                {
                    data: "mensaje", render: function (data, type, row, settings) {
                        //return '<input type="text" value="' + row.mensaje + '" class="form-control form-control-sm" spellcheck="false" autocomplete="off" />';
                        var ele = $('<input type="text" value="" class="form-control form-control-sm" spellcheck="false" autocomplete="off" />');
                        ele.attr('value', row.mensaje);
                        return ele[0].outerHTML;
                    }
                },
            ],
            createdRow: function (row, data, dataIndex) {
                //$(row).find('td:eq(2)').attr('data-validate', '1');
                $(row).attr('data-input', '1');
                //var rRow = tableValidacionesCI.row($(row));
                //rRow.child('<input type="text" value="ABC" class="form-control form-control-sm" />').show();
            }
        });
    }
    $('#spanInterfaceCI').html(valueSelect);
    $('#txtInterfaceCI').val(textSelect);

};

function ValidarTableParametrosCI() {
    tableParametrosCI.rows().invalidate().draw();
    $('#tableParametrosCI tbody tr td input').each(function () { $(this).val($.trim($(this).val())).change(); });
    if (ThrowEmpty()) {
        return true;
    }
    var inputEmpty = $('#tableParametrosCI tbody tr td').find('input[value=""], input[value="NaN"]');
    if (inputEmpty.length > 0) {
        inputEmpty.first().addClass('input_empty');
        MsgBox("No se puede asignar valores nulos.", "warning", true);
        return true;
    }
    inputEmpty = $('#tableParametrosCI tbody tr td:nth-child(3)').find('input[value*=" "]');
    if (inputEmpty.length > 0) {
        inputEmpty.first().addClass('input_empty');
        MsgBox("El nombre de la variable no debe contener espacios.", "warning", true);
        return true;
    }
    var error = false;
    $.each($('#tableParametrosCI tbody tr td:nth-child(3) > input'), function (index, value) {
        var input = $(value);
        var val = UIValue(input.val()).charAt(0);
        if (isNumeric(val)) {
            input.addClass('input_empty');
            error = true;
            MsgBox("El nombre de la variable debe empezar en una letra y solo puede incluir luego letras, números y guión bajo.", "warning");
        }
    });
    return error;
}

function ValidarTableValidacionesCI() {
    tableValidacionesCI.rows().invalidate().draw();
    $('#tableValidacionesCI tbody tr td input').each(function () { $(this).val($.trim($(this).val())).change(); });
    if (ThrowEmpty()) {
        return true;
    }
    var inputEmpty = $('#tableValidacionesCI tbody tr td').find('input[value=""], input[value="NaN"]');
    if (inputEmpty.length > 0) {
        inputEmpty.first().addClass('input_empty');
        MsgBox("No se puede asignar valores nulos.", "warning", true);
        return true;
    }
    //var error = false;
    //$.each($('#tableParametrosCI tbody tr td:nth-child(3) > input'), function (index, value) {
    //    var input = $(value);
    //    var val = getValue(input.val()).charAt(0);
    //    if (isNumeric(val)) {
    //        input.addClass('input_empty');
    //        error = true;
    //        MsgBox("El nombre de la variable debe empezar en una letra y solo puede incluir luego letras, números y guión bajo.", "warning");
    //    }
    //});
    //return error;
}

function RemoveTabGI(name) {
    var a;
    switch (name) {
        case 'aTabIngreseValores':
            a = $('#aTabResultados');
            a.click();
            $('#aTabIngreseValores').toggleClass('d-none');
            $('#containerIngreseValores').toggleClass('d-none');
            break;
        case 'aTabResultados':
            a = $('#aTabIngreseValores');
            a.click();
            $('#aTabResultados').toggleClass('d-none');
            $('#containerResultados').toggleClass('d-none');
            break;
    }
};

async function LoadModalGI(CodigoDeInterface) {
    $('#aTabIngreseValores').click();
    $('#aTabIngreseValores').removeClass('d-none');
    $('#containerIngreseValores').removeClass('d-none');
    $('#txtResultadoGI').val('');

    if ($.fn.dataTable.isDataTable('#tableParametrosGI')) {
        $('#tableParametrosGI').DataTable().clear().destroy();
        $('#tableParametrosGI').empty();
    }
    var jsonParam = {
        Tipo: TipoDeInterface,
        CodigoDeInterface: CodigoDeInterface || $('#selectProceso').val(),
    };
    await UIAjax.invokeMethodAsync('ajax/interfases', 'GetInterfaceProcesoParamSingle', jsonParam).then(function (r) {
        tableParametrosGI = $('#tableParametrosGI').DataTable({
            data: r.data,
            initComplete: function (settings, json) {
                $('#tableParametrosGI thead').addClass('thead-sky thead-sticky');
                if ($.fn.dataTable.isDataTable('#tableResultadosGI')) {
                    $('#tableResultadosGI').DataTable().clear().destroy();
                    $('#tableResultadosGI').empty();
                }
                if (r.recordsTotal == 0) {
                    $('#tableParametrosGI').empty();
                    RemoveTabGI('aTabIngreseValores');
                    $('#btnProcesarGI').click();
                }
                //UILoadingDefault("hide");
            },
            searching: false,
            paging: false,
            columns: [
                { title: "Parámetros", data: "nombre" },
                {
                    title: "Valor", data: "tipo_dato", render: function (data, type, row, settings) {
                        var html;
                        var typeInput, valInput;
                        switch (data) {
                            case "E":
                                typeInput = "number";
                                valInput = row.valor_defecto_entero;
                                break;
                            case "I":
                                typeInput = "number";
                                valInput = row.valor_defecto_importe;
                                break;
                            case "F":
                                typeInput = "date";
                                valInput = row.valor_defecto_fecha;
                                break;
                            default:
                                typeInput = "text";
                                valInput = row.valor_defecto_cadena;
                                break;
                        }
                        //html = '<input type="'+ typeInput +'" value="' + valInput + '" class="form-control form-control-sm" />';
                        html = `<input type="${typeInput}" value="${UIValue(valInput)}" class="form-control form-control-sm" />`;
                        return html;
                    }
                }
            ],
            order: false,
            info: false
        });
    });
    //$.ajax({
    //    type: "GET",
    //    url: portal + "/ajax/interfases/GetInterfaceProcesoParamSingle",
    //    data: jsonParam,
    //    contentType: "application/json; charset=utf-8",
    //    dataType: "json",
    //    async: false,
    //    success: function success(jsonAjax) {
    //        tableParametrosGI = $('#tableParametrosGI').DataTable({
    //            data: jsonAjax.data,
    //            initComplete: function (settings, json) {
    //                $('#tableParametrosGI thead').addClass('thead-sky thead-sticky');
    //                if ($.fn.dataTable.isDataTable('#tableResultadosGI')) {
    //                    $('#tableResultadosGI').DataTable().clear().destroy();
    //                    $('#tableResultadosGI').empty();
    //                }
    //                if (jsonAjax.recordsTotal == 0) {
    //                    $('#tableParametrosGI').empty();
    //                    RemoveTabGI('aTabIngreseValores');
    //                    $('#btnProcesarGI').click();
    //                }
    //                //UILoadingDefault("hide");
    //            },
    //            searching: false,
    //            paging: false,
    //            columns: [
    //                { title: "Parámetros", data: "nombre" },
    //                {
    //                    title: "Valor", data: "tipo_dato", render: function (data, type, row, settings) {
    //                        var html;
    //                        var typeInput, valInput;
    //                        switch (data) {
    //                            case "E":
    //                                typeInput = "number";
    //                                valInput = row.valor_defecto_entero;
    //                                break;
    //                            case "I":
    //                                typeInput = "number";
    //                                valInput = row.valor_defecto_importe;
    //                                break;
    //                            case "F":
    //                                typeInput = "date";
    //                                valInput = row.valor_defecto_fecha;
    //                                break;
    //                            default:
    //                                typeInput = "text";
    //                                valInput = row.valor_defecto_cadena;
    //                                break;
    //                        }
    //                        //html = '<input type="'+ typeInput +'" value="' + valInput + '" class="form-control form-control-sm" />';
    //                        html = `<input type="${typeInput}" value="${UIValue(valInput)}" class="form-control form-control-sm" />`;
    //                        return html;
    //                    }
    //                }
    //            ],
    //            order: false,
    //            info: false
    //        });
    //    },
    //    error: function error(jsonAjax) {
    //    }
    //});
}

function LoadFiltro() {
    var Proyecto = $('#selectProyecto');
    var valProyecto = '';
    if (Proyecto.length == 1) {
        valProyecto = Proyecto.val();
    }

    var CentrosCosto = $('#selectCentrosCosto');
    var valCentrosCosto = '';
    if (CentrosCosto.length == 1) {
        valCentrosCosto = CentrosCosto.val();
    }

    var UnidadesNegocio = $('#selectUnidadesNegocio');
    var valUnidadesNegocio = '';
    if (UnidadesNegocio.length == 1) {
        valUnidadesNegocio = UnidadesNegocio.val();
    }

    var Area = $('#selectArea');
    var valArea = '';
    if (Area.length == 1) {
        valArea = Area.val();
    }

    var Localidad = $('#selectLocalidad');
    var valLocalidad = '';
    if (Localidad.length == 1) {
        valLocalidad = Localidad.val();
    }

    var TipoPlanilla = $('#selectTipoPlanilla');
    var valTipoPlanilla = '';
    if (TipoPlanilla.length == 1) {
        if (TipoPlanilla.prop("multiple")) {
            valTipoPlanilla = TipoPlanilla.val().join(",");
        }
        else {
            valTipoPlanilla = TipoPlanilla.val();
        }
    }

    var TabPagos = $('#aTabPagos');
    var valProceso = '';
    var valClaseDeProceso = '';
    if (TabPagos.length == 1) {
        if (TabPagos.hasClass('active')) {
            valProceso = 'P';
            valClaseDeProceso = $('input[name="gpPagos"]:checked').val();
        }
        else {
            //aTabProvisiones
            valProceso = 'V';
            valClaseDeProceso = $('input[name="gpProvisiones"]:checked').val();
        }
    }
    else {
        var ClaseProceso = $('input[name="gpPagos"]');
        if (ClaseProceso.length > 0) {
            valProceso = 'P';
            valClaseDeProceso = ClaseProceso.filter(':checked').val();
        }
    }

    var FechaPeriodoDesde = $('#txtFechaPeriodoDesde');
    var valFechaPeriodoDesde = '';
    if (FechaPeriodoDesde.length == 1) {
        valFechaPeriodoDesde = FechaPeriodoDesde.val();
    }

    var FechaPeriodoHasta = $('#txtFechaPeriodoHasta');
    var valFechaPeriodoHasta = '';
    if (FechaPeriodoHasta.length == 1) {
        valFechaPeriodoHasta = FechaPeriodoHasta.val();
    }

    var CodigoAnual = '', CodigoMes = '';
    var Periodos = $('#selectPeriodo');
    if (Periodos.length == 1) {
        var jsonRow = JSON.parse(Periodos.val());
        if (!isEmpty(jsonRow)) {
            CodigoAnual = jsonRow.cod_anual;
            CodigoMes = jsonRow.mes;
            if (valFechaPeriodoDesde == '') {
                valFechaPeriodoDesde = jsonRow.desde;
            }
            if (valFechaPeriodoHasta == '') {
                valFechaPeriodoHasta = jsonRow.hasta;
            }
        }
    }

    var valPersonal = '';
    var Colaboradores = $('#tableColaboradores');
    if (Colaboradores.length == 1) {
        var TodosColaboradores = $('#btnTodosColaboradores');
        if (TodosColaboradores.length == 1 && TodosColaboradores.hasClass('btn-secondary')) {
            valPersonal = '%';
        }
        else {
            var Rows = tableColaboradores.rows('.selected').data();
            var ACodigo = Array.from(Rows, function (p) { return p.cod_personal });
            valPersonal = ACodigo.join(",");
        }
    }

    var FiltroTipo = $('input[name="gpFiltroTipo"]');
    var valFiltroTipo = '';
    if (FiltroTipo.length > 0) {
        valFiltroTipo = FiltroTipo.filter(':checked').val();
    }

    var Estado = $('#selectEstado');
    var valEstado = '';
    if (Estado.length == 1) {
        valEstado = Estado.val();
    }

    var FechaDeposito = $('#txtFechaDeposito');
    var valFechaDeposito = '';
    if (FechaDeposito.length == 1) {
        valFechaDeposito = FechaDeposito.val();
    }

    var Porcentaje = $('#txtPorcentaje');
    var valPorcentaje = '';
    if (Porcentaje.length == 1) {
        valPorcentaje = Porcentaje.val();
    }

    var Moneda = $('#selectMoneda');
    var valMoneda = '';
    if (Moneda.length == 1) {
        valMoneda = Moneda.val();
    }

    var EntidadDeProceso = $('#selectProceso');
    var valEntidadDeProceso = '';
    if (EntidadDeProceso.length == 1) {
        var jsonOption = $('option:selected', EntidadDeProceso).attr('data-json');
        if (jsonOption != undefined || jsonOption != null) {
            valEntidadDeProceso = JSON.parse(jsonOption).cod_entidad;
        }
    }

    var NombreDeArchivo = $('#txtNombreDeArchivo');
    var valNombreDeArchivo = '';
    if (NombreDeArchivo.length == 1) {
        valNombreDeArchivo = NombreDeArchivo.val();
    }

    var jsonParam = {
        Tipo: TipoDeInterface,
        CodigoDeInterface: $('#selectProceso').val(),
        CodigoDeProyecto: valProyecto,
        CodigoDeCC: valCentrosCosto,
        CodigoDeUN: valUnidadesNegocio,
        CodigoDeArea: valArea,
        CodigoDeLocalidad: valLocalidad,
        CodigoAnual: CodigoAnual,
        CodigoMes: CodigoMes,
        CodigoDeAfp: '%',
        CodigoDeTipoPlanilla: valTipoPlanilla,
        CodigoClaseDeProceso: UIValue(valClaseDeProceso, 'B'),
        CodigoDeProceso: UIValue(valProceso, 'P'),
        FechaDeInicio: valFechaPeriodoDesde,
        FechaDeFin: valFechaPeriodoHasta,
        Filtro: valFiltroTipo,
        Estado: valEstado,
        FechaDeDeposito: valFechaDeposito,
        CodigoDePersonal: valPersonal,
        PorcetajeDeDeposito: valPorcentaje,
        CodigoDeMoneda: valMoneda,
        CodigoEntidadDeProceso: valEntidadDeProceso,
        NombreDeArchivo: valNombreDeArchivo,
        Table: []
    };

    if ($.fn.dataTable.isDataTable('#tableResultadosGI')) {
        $('#tableResultadosGI').DataTable().clear().destroy();
        $('#tableResultadosGI').empty();
    }
    if ($.fn.dataTable.isDataTable('#tableParametrosGI')) {
        tableParametrosGI.rows().invalidate().draw();
        var AValidaciones = Array.from(tableParametrosGI.rows().data(), function (x) {
            var val = '';
            switch (x.tipo_dato) {
                case "E":
                    val = x.valor_defecto_entero;
                    break;
                case "I":
                    val = x.valor_defecto_importe;
                    break;
                case "F":
                    val = x.valor_defecto_fecha;
                    break;
                default:
                    val = x.valor_defecto_cadena;
                    break;
            }
            return {
                id: 1,
                Nombre: x.nombre_variable,
                Valor: val
            };
        });
        jsonParam.Table = AValidaciones;
    }
    return jsonParam;
}