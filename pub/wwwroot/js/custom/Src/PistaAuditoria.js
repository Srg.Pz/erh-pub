﻿
function SettingControls() {
    $('#txtInicio, #txtFin').datepicker({
        autoclose: true,
        format: "dd/mm/yyyy"
    });

    FiltroActivacion(true);
}

var Cod_Empresa_Seleccionada = '';
var Ventana_Seleccionada = '';
var Proceso_Seleccionado = '';
var Usuario_Seleccionado = '';

function OnLoadEvents() {

    $("#chkFiltrar").change(function () {

        FiltroActivacion(!this.checked);
    });

    $('#btnConsultar').on('click', function (e) {
        e.preventDefault();

        EmpresasListar(GetFechas());
    });


    $('#dtEmpresas').on('click', 'tr', function () {

        $('#dtEmpresas tr').removeClass('selected');

        var ths = $(this);
        ths.addClass('selected');

        var table = $('#dtEmpresas').DataTable();
        var data = table.row(this).data();
        Cod_Empresa_Seleccionada = data.cod_Empresa;
        VentanasListar(GetFechas(), Cod_Empresa_Seleccionada);
    });

    $('#dtVentanas').on('click', 'tr', function () {

        $('#dtVentanas tr').removeClass('selected');

        var ths = $(this);
        ths.addClass('selected');

        var table = $('#dtVentanas').DataTable();
        var data = table.row(this).data();
        Ventana_Seleccionada = data.ventana;
        ProcesosListar(GetFechas(), Cod_Empresa_Seleccionada, Ventana_Seleccionada);
    });

    $('#dtProcesos').on('click', 'tr', function () {

        $('#dtProcesos tr').removeClass('selected');

        var ths = $(this);
        ths.addClass('selected');

        var table = $('#dtProcesos').DataTable();
        var data = table.row(this).data();
        Proceso_Seleccionado = data.tabla_proceso;
        UsuariosListar(GetFechas(), Cod_Empresa_Seleccionada, Ventana_Seleccionada, Proceso_Seleccionado)
    });

    $('#dtUsuarios').on('click', 'tr', function () {

        $('#dtUsuarios tr').removeClass('selected');

        var ths = $(this);
        ths.addClass('selected');

        var table = $('#dtUsuarios').DataTable();
        var data = table.row(this).data();
        Usuario_Seleccionado = data.cod_usuario;
        ReporteListar(GetFechas(), Cod_Empresa_Seleccionada, Ventana_Seleccionada, Proceso_Seleccionado, Usuario_Seleccionado);
    });

    $('#dtFiltroTrabajadores').on('click', 'tr', function () {

        $('#dtFiltroTrabajadores tr').removeClass('selected');
        var ths = $(this);
        ths.addClass('selected');
    });

    $('#dtFiltroTrabajadores').on('dblclick', 'tr', function () {

        var table = $('#dtFiltroTrabajadores').DataTable();
        var data = table.row(this).data();

        var rbTCodigo = $('input:radio[id=rbTCodigo]:radio').is(':checked');
        var rbTEntidad = $('input:radio[id=rbTEntidad]:radio').is(':checked');
        var rbTFotocheck = $('input:radio[id=rbTFotocheck]:radio').is(':checked');

        var valor = '';
        if (rbTCodigo) valor = data.cod_personal;
        else if (rbTEntidad) valor = data.cod_entidad;
        else if (rbTFotocheck) valor = data.fotocheck;

        $("#txtFiltro").val(valor);
        $('#modalTrabajadores').modal('hide');
    });

    $('#dtDataFinal').on('click', 'tr', function () {

        $('#dtDataFinal tr').removeClass('selected');

        var ths = $(this);
        ths.addClass('selected');

        var table = $('#dtDataFinal').DataTable();
        var data = table.row(this).data();
        //alert(data.pista);
        RestriccionListar(data.pista);
        ValoresListar(data.pista);
    });

    EmpresasCrear();
    VentanasCrear();
    ProcesosCrear();
    UsuariosCrear();
    ReporteCrear();
    RestriccionCrear();
    ValoresCrear();
}

function FiltroActivacion(value) {
    $('#chkTrabajador').prop('disabled', value);
    $('#rbRestriccion').prop('disabled', value);
    $('#rbValores').prop('disabled', value);
    $('#txtFiltro').prop('disabled', value);
}


//#region FUNCIONES DE FILTROS VENTANAS

function GetFechas() {

    return "Fecha_Inicio=" + $("#txtInicio").val() + "&Fecha_Fin=" + $("#txtFin").val();
}

function EmpresasCrear() {

    dtEmpresas = $('#dtEmpresas').DataTable({
        "bFilter": false
        , "paging": false
        , "bInfo": false
        , "scrollY": "200px"
        , buttons: [
            //'copyHtml5',
            //'excelHtml5',
            //'csvHtml5'
        ]
        , "columns": [

            {
                "data": "cod_Empresa",
                "visible": false
            },
            { "data": "nombre" },
        ]

    });
}
async function EmpresasListar(Fechas) {
    $('#dtVentanas,#dtProcesos,#dtUsuarios,#dtDataFinal,#dtRestriccion,#dtValores').DataTable().rows().remove().draw();

    var Columna_Busca = $('input:radio[id=rbRestriccion]:radio').is(':checked') ? 'restriccion' : 'Valores';
    var Valor_Busca = $('#txtFiltro').val();
    var jsonParam = {
        Fecha_Inicio: $("#txtInicio").val(),
        Fecha_Fin: $("#txtFin").val(),
        Columna_Busca: Columna_Busca,
        Valor_Busca: Valor_Busca,
    };
    await UILoadingDefault("show");
    await UIAjax.invokeMethodAsync('Ajax/TablasSoporte', 'GetPistaAuditoriaEmpresas', jsonParam).then(function (r) {
        dtEmpresas = $('#dtEmpresas').DataTable({
            data: r.data,
            "initComplete": function (settings, json) {
                //$(chbTodos).appendTo("#tbTrabajadores_filter label");
                $('#dtEmpresas tbody tr:eq(0)').click();
            },
            "bFilter": false,
            "paging": false,
            "bInfo": false,
            scrollY: "200px",
            buttons: [],
            columns: [
                {
                    "data": "cod_Empresa",
                    "visible": false
                },
                { "data": "nombre" },
            ]
        });
    });
    UILoadingDefault("hide");
}

function VentanasCrear() {

    dtVentanas = $('#dtVentanas').DataTable({
        "bFilter": false,
        "paging": false,
        "bInfo": false,
        "scrollY": "200px",
        dom: 'Bfrtip',
        buttons: [
            //'copyHtml5',
            //'excelHtml5',
            //'csvHtml5'
        ],
        "columns": [

            { "data": "ventana" },
        ]
    });
}

function VentanasListar(Fechas, Cod_Empresa) {

    $('#dtProcesos,#dtUsuarios,#dtDataFinal,#dtRestriccion,#dtValores').DataTable().rows().remove().draw();

    dtVentanas = $('#dtVentanas').DataTable({
        "ajax": portal + '/Ajax/TablasSoporte/GetPistaAuditoriaVentanas?Cod_Empresa=' + Cod_Empresa + '&' + Fechas + getColumnaValor()
        , "initComplete": function (settings, json) {
            $('#dtVentanas tbody tr:eq(0)').click();
        }

        , "bFilter": false
        , "paging": false
        , "bInfo": false
        , "scrollY": "200px"
        , bDestroy: true
        , dom: 'Bfrtip'
        , buttons: [
            //'copyHtml5',
            //'excelHtml5',
            //'csvHtml5'
        ]
        , "language": {
            "url": '/js/language.js'
        }
        , "columns": [

            { "data": "ventana" },
        ]

    });
}

function ProcesosCrear() {

    dtProcesos = $('#dtProcesos').DataTable({
        "initComplete": function (settings, json) {
            $('#dtProcesos').DataTable().rows().remove().draw();
        }
        , "bFilter": false
        , "paging": false
        , "bInfo": false
        , "scrollY": "200px"
        , bDestroy: true
        , dom: 'Bfrtip'
        , buttons: [
            //'copyHtml5',
            //'excelHtml5',
            //'csvHtml5'
        ]
        , "language": {
            "url": '/js/language.js'
        }
        , "columns": [

            { "data": "tabla_proceso" },
        ]

    });
}
function ProcesosListar(Fechas, Cod_Empresa, Ventana) {

    $('#dtUsuarios,#dtDataFinal,#dtRestriccion,#dtValores').DataTable().rows().remove().draw();

    dtProcesos = $('#dtProcesos').DataTable({
        "ajax": portal + '/Ajax/TablasSoporte/GetPistaAuditoriaTablaProcesos?Cod_Empresa=' + Cod_Empresa + '&Ventana=' + Ventana + '&' + Fechas + getColumnaValor()
        , "initComplete": function (settings, json) {
            $('#dtProcesos tbody tr:eq(0)').click();
        }

        , "bFilter": false
        , "paging": false
        , "bInfo": false
        , "scrollY": "200px"
        , bDestroy: true
        , dom: 'Bfrtip'
        , buttons: [
            //'copyHtml5',
            //'excelHtml5',
            //'csvHtml5'
        ]
        , "language": {
            "url": '/js/language.js'
        }
        , "columns": [

            { "data": "tabla_proceso" },
        ]

    });
}

function UsuariosCrear() {

    //$('#dtGrupo_Trabajo_Trabajadores').DataTable().clear().draw();

    dtUsuarios = $('#dtUsuarios').DataTable({
        "initComplete": function (settings, json) {
            //$(chbTodos).appendTo("#tbTrabajadores_filter label");
        }

        , "bFilter": false
        , "paging": false
        , "bInfo": false
        , "scrollY": "200px"
        , bDestroy: true
        , dom: 'Bfrtip'
        , buttons: [
            //'copyHtml5',
            //'excelHtml5',
            //'csvHtml5'
        ]
        , "language": {
            "url": '/js/language.js'
        }
        , "columns": [

            { "data": "nombre" },
            {
                "data": "cod_usuario",
                "visible": false
            },
        ]

    });
}
function UsuariosListar(Fechas, Cod_Empresa, Ventana, Tabla_Proceso) {

    $('#dtDataFinal,#dtRestriccion,#dtValores').DataTable().rows().remove().draw();

    dtUsuarios = $('#dtUsuarios').DataTable({
        "ajax": portal + '/Ajax/TablasSoporte/GetPistaAuditoriaUsuarios?Cod_Empresa=' + Cod_Empresa + '&Ventana=' + Ventana + '&Tabla_Proceso=' + Tabla_Proceso + '&' + Fechas + getColumnaValor()
        , "initComplete": function (settings, json) {
            $('#dtUsuarios tbody tr:eq(0)').click();
        }

        , "bFilter": false
        , "paging": false
        , "bInfo": false
        , "scrollY": "200px"
        , bDestroy: true
        , dom: 'Bfrtip'
        , buttons: [
            //'copyHtml5',
            //'excelHtml5',
            //'csvHtml5'
        ]
        , "language": {
            "url": '/js/language.js'
        }
        , "columns": [

            { "data": "nombre" },
            {
                "data": "cod_usuario",
                "visible": false
            },
        ]

    });
}


function getDateString(date) {
    var dateObj = new Date(parseInt(date.substr(6)));
    return dateObj.toDateString();
}

function getColumnaValor() {
    var Columna_Busca = $('input:radio[id=rbRestriccion]:radio').is(':checked') ? 'restriccion' : 'Valores';
    var Valor_Busca = $('#txtFiltro').val();

    return '&Columna_Busca=' + Columna_Busca + '&Valor_Busca=' + Valor_Busca;
}

function ReporteCrear() {

    dtDataFinal = $('#dtDataFinal').DataTable({
        "bFilter": false
        , "paging": false
        , "bInfo": false
        , "scrollY": "300px"
        , bDestroy: true
        //, dom: 'Bfrtip'
        , buttons: []
        , "columns": [
            { "data": "pista" },
            { "data": "fecha" },
            { "data": "equipo" },
            { "data": "modulo" },
            { "data": "accion" },
            { "data": "columnas" },
            { "data": "valores" },
            { "data": "restriccion" },
        ]

    });
}
function ReporteListar(Fechas, Cod_Empresa, Ventana, Tabla_Proceso, Usuario) {

    dtDataFinal = $('#dtDataFinal').DataTable({
        "ajax": portal + '/Ajax/TablasSoporte/GetPistaAuditoriaReporte?Cod_Empresa=' + Cod_Empresa + '&Ventana=' + Ventana + '&Tabla_Proceso=' + Tabla_Proceso + '&' + Fechas + '&Usuario=' + Usuario + getColumnaValor()
        , "initComplete": function (settings, json) {
            //$(chbTodos).appendTo("#tbTrabajadores_filter label");
        }

        , "bFilter": false
        , "paging": false
        , "bInfo": false
        //, "scrollY": "300px"
        , bDestroy: true
        , dom: 'Bfrtip'
        , buttons: [
            //'copyHtml5',
            'excelHtml5',
            //'csvHtml5'
        ]
        , "language": {
            "url": '/js/language.js'
        }
        , "columns": [
            { "data": "pista" },
            {
                "data": "fecha2",
                //"render": function (data) { return getDateString(data); }
            },
            { "data": "equipo" },
            { "data": "modulo" },
            { "data": "accion" },
            { "data": "columnas" },
            { "data": "valores" },
            { "data": "restriccion" },
        ]

    });
}

function FiltroTrabajadores() {

    dtFiltroTrabajadores = $('#dtFiltroTrabajadores').DataTable({
        "ajax": portal + '/Ajax/TablasSoporte/GetPistaAuditoriaTrabajadores'
        , "initComplete": function (settings, json) {
            //$(chbTodos).appendTo("#tbTrabajadores_filter label");
        }

        , "bFilter": true
        , "paging": false
        , "bInfo": false
        //, "scrollY": "300px"
        , bDestroy: true
        , dom: 'Bfrtip'
        , buttons: [
            //'copyHtml5',
            //'excelHtml5',
            //'csvHtml5'
        ]
        , "columns": [
            { "data": "cod_trabajador" },
            { "data": "doc_Ident" },
            { "data": "apellidos_Nombres" },
            { "data": "proyecto" },
            { "data": "unidad_Negocio" },
            { "data": "centro_Costo" },
            { "data": "area" },
            { "data": "localidad" },
            { "data": "cargo" },
            { "data": "planilla" },
            { "data": "cod_entidad" },
            { "data": "cod_personal" },
            { "data": "fotocheck" },
            { "data": "estado" },
            { "data": "fecha_ingreso2" },
            { "data": "fecha_cese2" },
        ]

    });
}


function RestriccionCrear() {

    $('#dtRestriccion').DataTable({
        "initComplete": function (settings, json) {
            //$(chbTodos).appendTo("#tbTrabajadores_filter label");
        }

        , "bFilter": false
        , "paging": false
        , "bInfo": false
        , "scrollY": "200px"
        , bDestroy: true
        , dom: 'Bfrtip'
        , buttons: [
            //'copyHtml5',
            //'excelHtml5',
            //'csvHtml5'
        ]
        , "language": {
            "url": '/js/language.js'
        }
        , "columns": [

            { "data": "restriccion" },
        ]

    });
}
function RestriccionListar(Pista) {

    $('#dtRestriccion').DataTable({
        "ajax": portal + '/Ajax/TablasSoporte/GetPistaAuditoriaRestricciones?Pista=' + Pista
        , "initComplete": function (settings, json) {
            //$(chbTodos).appendTo("#tbTrabajadores_filter label");
        }

        , "bFilter": false
        , "paging": false
        , "bInfo": false
        , "scrollY": "200px"
        , bDestroy: true
        , dom: 'Bfrtip'
        , buttons: [
            //'copyHtml5',
            //'excelHtml5',
            //'csvHtml5'
        ]
        , "language": {
            "url": '/js/language.js'
        }
        , "columns": [

            { "data": "restriccion" },
        ]

    });
}

function ValoresCrear() {

    $('#dtValores').DataTable({
        "initComplete": function (settings, json) {
            //$(chbTodos).appendTo("#tbTrabajadores_filter label");
        }

        , "bFilter": false
        , "paging": false
        , "bInfo": false
        , "scrollY": "200px"
        , bDestroy: true
        , dom: 'Bfrtip'
        , buttons: [
            //'copyHtml5',
            //'excelHtml5',
            //'csvHtml5'
        ]
        , "language": {
            "url": '/js/language.js'
        }
        , "columns": [

            { "data": "valores" },
        ]

    });
}
function ValoresListar(Pista) {

    $('#dtValores').DataTable({
        "ajax": portal + '/Ajax/TablasSoporte/GetPistaAuditoriaValores?Pista=' + Pista
        , "initComplete": function (settings, json) {
            //$(chbTodos).appendTo("#tbTrabajadores_filter label");
        }

        , "bFilter": false
        , "paging": false
        , "bInfo": false
        , "scrollY": "200px"
        , bDestroy: true
        , dom: 'Bfrtip'
        , buttons: [
            //'copyHtml5',
            //'excelHtml5',
            //'csvHtml5'
        ]
        , "language": {
            "url": '/js/language.js'
        }
        , "columns": [

            { "data": "valores" },
        ]

    });
}

//#endregion

var Load = {
    fnAceptarMRUERH: async function (e) {
        var jsonParam = {
            NombreDeReporte: "RptRankingUsoEmpresarialRHF3",
            CodigoAnual: $('#txtAnhoMRUERH').val(),
            Top: $('#txtTopMRUERH').val(),
        };
        if (jsonParam.Top < 1) {
            MsgBox("El valor ingresado en TOP no es válido.", "warning", false);
            return;
        }
        if (UIEmpty(jsonParam.CodigoAnual)) {
            MsgBox("El valor ingresado en Año no es válido.", "warning", false);
            return;
        }
        UIBoldReport.Load(jsonParam);
        $('#modalRankingUsoEmpresarialRH').modal('hide');
    },
}