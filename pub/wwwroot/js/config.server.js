﻿class AppConfiguration {
    constructor(bool, options) {
        if (bool == null | bool == undefined) {
            bool = false;
        }

        this.debugger = {
            state: bool,
            success: {
                icon: '%c✔',
                color: '#148f32'
            },
            error: {
                icon: '%c❌',
                color: '#FF0000'
            },
            warning: {
                icon: '%c⚠️',
                color: '#FCE100'
            },
            datatables: {
                state: true,
                ajax: true,
                params: true,
                response: true
            }
        }

        $.extend(true, this.debugger, options);

        this.utility = {
            _json(url) {
                var vars = {};
                var parts = url.replace(/[?&]+([^=&]+)=([^&]*)/gi,
                    function (m, key, value) {
                        vars[key] = value;
                    });
                return vars;
            }
        }

        this.console = {
            _log(prop, name, mensaje, obj) {
                //if (obj == null || obj == undefined) {
                //    console.log(`${app_config.debugger[prop].icon} ${name} (${mensaje})`, `color: ${app_config.debugger[prop].color}`);
                //}
                //else {
                //    console.log(`${ app_config.debugger[prop].icon } ${name} (${mensaje}) Response: \n%o`, `color: ${app_config.debugger[prop].color}`, obj);
                //}
                var _icon = app_config.debugger[prop].icon;
                var _color = app_config.debugger[prop].color;
                switch (mensaje) {
                    case 'datatables':
                        if (app_config.debugger.datatables.state) {
                            var options = [];
                            options.push(obj.ajax);
                            options.push(app_config.utility._json(obj.ajax));
                            options.push(obj.json);
                            //console.log(`${_icon} ${name} ('DataTables : ${obj.sTableId}')\nAjax:\n%o\nParams:\n%o\nResponse:\n%o`, `color: ${_color}`, options[0], options[1], options[2]);
                            //console.log(`${_icon} ${name} ('DataTables : ${obj.sTableId}')`, `color: ${_color}`);
                            console.groupCollapsed(`${_icon} ${name} ('DataTables : ${obj.sTableId}')`, `color: ${_color}`);
                            //console.group(`${_icon} ${name} ('DataTables : ${obj.sTableId}')`, `color: ${_color}`);                        
                            if (app_config.debugger.datatables.ajax) {
                                console.log(`%cAjax:\n%o`, `color: ${_color}`, options[0]);
                            }
                            if (app_config.debugger.datatables.params) {
                                console.log(`%cParams:\n%o`, `color: ${_color}`, options[1]);
                            }
                            if (app_config.debugger.datatables.response) {
                                console.log(`%cResponse:\n%o`, `color: ${_color}`, options[2]);
                            }
                            console.groupEnd();
                            //console.log(obj);
                        }
                        break;
                    default:
                        console.log(`${app_config.debugger[prop].icon} ${name} (${mensaje}) Response: \n%o`, `color: ${app_config.debugger[prop].color}`, obj);
                        break;
                }
            },
            success(mensaje, obj) {
                this._log('success', 'Success', mensaje, obj);
            },
            error(mensaje) {
                this._log('error', 'Error', mensaje, obj);
            },
            warning(mensaje) {
                this._log('warning', 'Warning', mensaje, obj);
            }
        }
    };

    Set(value) {
        this.debugger.state = value;
    }

}
//export { AppConfiguration };

var app_config = new AppConfiguration(false);
