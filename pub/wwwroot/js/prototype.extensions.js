﻿Array.prototype.sum = function (prop) {
	var total = 0;
	for (var i = 0, _len = this.length; i < _len; i++) {
		var value = this[i][prop];
		if (value === "" || value === null || value === undefined) {
			value = 0;
		}
		total += value;
	}
	return total;
};

Array.prototype.HHmm = function (prop) {
	var horas = 0;
	var minutos = 0;
	for (var i = 0, _len = this.length; i < _len; i++) {
		var value = this[i][prop];
		if (value === "" || value === null || value === undefined) {
			value = "00:00";
		}
		let AHHmm = value.split(":");
		horas += parseInt(AHHmm[0]);
		minutos += parseInt(AHHmm[1]);
	}
	return UITime.ParseToHHmm(horas, minutos);
};

//// First, checks if it isn't implemented yet.
//if (!String.prototype.format) {
//  String.prototype.format = function() {
//    var args = arguments;
//    return this.replace(/{(\d+)}/g, function(match, number) { 
//      return typeof args[number] != 'undefined'
//        ? args[number]
//        : match
//      ;
//    });
//  };
//}
//"{0} is dead, but {1} is alive! {0} {2}".format("ASP", "ASP.NET")


Date.prototype.getString = function () {
	var options = { year: 'numeric', month: '2-digit', day: '2-digit' };
	return this.toLocaleDateString("es-ES", options);
}

Date.prototype.padMonth = function () {
	return ("0" + (this.getMonth() + 1)).slice(-2);
}

//function XDate() {
//	var x = new (Function.prototype.bind.apply(Date, [Date].concat(Array.prototype.slice.call(arguments))))
//	x.__proto__ = XDate.prototype;
//	return x;
//}

//XDate.prototype.__proto__ = Date.prototype;

//XDate.prototype.foo = function () {
//	return 'bar';
//};

if (!String.format) {
	String.format = function (format) {
		var args = Array.prototype.slice.call(arguments, 1);
		return format.replace(/{(\d+)}/g, function (match, number) {
			return typeof args[number] != 'undefined' ? args[number] : match;
		});
	};
}

String.prototype.formatUnicorn = String.prototype.formatUnicorn || function () {
	"use strict";
	var str = this.toString();
	if (arguments.length) {
		var t = typeof arguments[0];
		var key;
		var args = ("string" === t || "number" === t) ?
			Array.prototype.slice.call(arguments)
			: arguments[0];

		for (key in args) {
			str = str.replace(new RegExp("\\{" + key + "\\}", "gi"), args[key]);
		}
	}

	return str;
};

String.prototype.toDate = function (format) {
	var result = null;
	try {
		var normalized = this.replace(/[^a-zA-Z0-9]/g, '-');
		if (normalized) {
			var normalizedFormat = format.toLowerCase().replace(/[^a-zA-Z0-9]/g, '-');
			var formatItems = normalizedFormat.split('-');
			var dateItems = normalized.split('-');

			var monthIndex = formatItems.indexOf("mm");
			var dayIndex = formatItems.indexOf("dd");
			var yearIndex = formatItems.indexOf("yyyy");
			var hourIndex = formatItems.indexOf("hh");
			var minutesIndex = formatItems.indexOf("ii");
			var secondsIndex = formatItems.indexOf("ss");

			var today = new Date();

			var year = yearIndex > -1 ? dateItems[yearIndex] : today.getFullYear();
			var month = monthIndex > -1 ? dateItems[monthIndex] - 1 : today.getMonth() - 1;
			var day = dayIndex > -1 ? dateItems[dayIndex] : today.getDate();

			var hour = hourIndex > -1 ? dateItems[hourIndex] : today.getHours();
			var minute = minutesIndex > -1 ? dateItems[minutesIndex] : today.getMinutes();
			var second = secondsIndex > -1 ? dateItems[secondsIndex] : today.getSeconds();

			result = new Date(year, month, day, hour, minute, second);
		}
	} catch (e) {
		result = null;
	}
	return result;

};
String.prototype.left = function (n) {
	return this.substring(0, n);
};
String.prototype.right = function (n) {
	//return this.substring(this.length - n, n);
	return this.slice(this.length - n, this.length);
};

function StringBuilder(value) {
	this.strings = new Array("");
	this.append(value);
}
StringBuilder.prototype.append = function (value) {
	if (value) {
		this.strings.push(value);
	}
}
StringBuilder.prototype.clear = function () {
	this.strings.length = 1;
}
StringBuilder.prototype.toString = function () {
	return this.strings.join("");
}

//console.log("Hello, {name}, are you feeling {adjective}?".formatUnicorn({ name: "Gabriel", adjective: "OK" }));


//PENDIENTE DE IMPLEMENTAR
//Date.prototype.addDays = function (days) {
//	var dat = new Date(this.valueOf())
//	dat.setDate(dat.getDate() + days);
//	return dat;
//}

//function getDates(startDate, stopDate) {
//	var dateArray = new Array();
//	var currentDate = startDate;
//	while (currentDate <= stopDate) {
//		dateArray.push(currentDate)
//		currentDate = currentDate.addDays(1);
//	}
//	return dateArray;
//}

//var dateArray = getDates(new Date(), (new Date()).addDays(2));
//for (i = 0; i < dateArray.length; i++) {
//	alert(dateArray[i]);
//}
