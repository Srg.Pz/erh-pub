﻿var tableColaboradores;
var tableMeritosDemeritos;

var tableColaboradorADC;
var tableListaADC;

var tableVariacionSueldoVS;
var tableVariacionClasificadoresCVARCLA;

var g_proyecto = "",
	g_ccosto = "",
	g_unegocio = "",
	g_area = "",
	g_localidad = "",
	g_tipoplanilla = "",
	g_estadotrab = "",
	g_filtro = "",
	g_codPersonal = "",
	g_codEntidad = "",
	g_reingreso = 'N';
var containerPage;
var containerColaboradores;

var regExpFecha = /^(?:(?:(?:0?[1-9]|1\d|2[0-8])[/](?:0?[1-9]|1[0-2])|(?:29|30)[/](?:0?[13-9]|1[0-2])|31[/](?:0?[13578]|1[02]))[/](?:0{2,3}[1-9]|0{1,2}[1-9]\d|0?[1-9]\d{2}|[1-9]\d{3})|29[/]0?2[/](?:\d{1,2}(?:0[48]|[2468][048]|[13579][26])|(?:0?[48]|[13579][26]|[2468][048])00))$/;
//Parametros Planilla
g_porcenAvance = 0,
	g_codTrabajadorCorr = 'N',
	g_flgSueldoCero = 'N';

ActionsReactionsLoadRegex();

var validaDatos = function validaDatos() {

	//Clasificadores
	if ($('#selTipoPlanilla_new').val() == -1) {
		MsgBox("Falta asignar el tipo de Planilla.", "warning", true);
		$('#selTipoPlanilla_new').focus();
		return 1;
	}

	//Datos Personales
	if ($('#txtNumDoc_new').val().trim().length == 0) {
		MsgBox("Falta _Ingresar el numero de Documento.", "warning", true);
		$('#txtNumDoc_new').focus();
		return 1;
	}

	if ($('#txtRuc_new').val().trim().length > 0) {
		if ($('#txtRuc_new').val().trim().length < minRuc) {
			MsgBox("Ingreso minimo 11 digitos para el RUC.", "warning", true);
			$('#txtRuc_new').focus();
			return 1;
		}
	}

	if ($('#selBrevete_new').val() != -1) {
		if ($('#txtNumBrevete_new').val().trim().length <= 0) {
			MsgBox("Ingrese el numero de Brevete.", "warning", true);
			$('#txtNumBrevete_new').focus();
			return 1;
		}
	}
	if ($('#txtApellidoP_new').val() == '') {
		MsgBox("Falta registrar el Apellido Paterno.", "warning", true);
		$('#txtApellidoP_new').focus();
		return 1;
	}

	if ($('#txtApellidoM_new').val() == '') {
		MsgBox("Falta registrar el Apellido Materno.", "warning", true);
		$('#txtApellidoM_new').focus();
		return 1;
	}

	if ($('#txtNombres_new').val() == '') {
		MsgBox("Falta registrar el nombre.", "warning", true);
		$('#txtNombres_new').focus();
		return 1;
	}

	if (!regExpFecha.test($('#txtFechaNac_new').val())) {
		MsgBox("La fecha de nacimiento no es correcta.", "warning", true);
		$('#txtFechaNac_new').focus();
		return 1;
	}

	if ($('#selAFP_new').val() == null) {
		MsgBox("La AFP no ha sido seleccionada.", "warning", true);
		$('#selAFP_new').focus();
		return 1;
	}

	if ($('#selPais_new').val() == 0) {
		MsgBox("El Pais no ha sido seleccionado.", "warning", true);
		$('#selPais_new').focus();
		return 1;
	}

	if ($('#selVia_new').val() != -1) {
		if ($('#txtVia_new').val().trim().length == 0) {
			MsgBox("Ingreso una descripcion  de la Via.", "warning", true);
			$('#txtVia_new').focus();
			return 1;
		}
	}

	if ($('#selZona_new').val() != -1) {
		if ($('#txtZona_new').val().trim().length == 0) {
			MsgBox("Ingreso una descripcion  de la Zona.", "warning", true);
			$('#txtZona_new').focus();
			return 1;
		}
	}

	//Datos Laborales
	if ($('#selCategoria_new').val() == -1) {
		MsgBox("Falta asignar la categoria Laboral.", "warning", true);
		$('#selCategoria_new').focus();
		return 1;
	}

	if ($('#txtCodTrab_new').val() == '') {
		MsgBox("Falta registrar código de trabajador.", "warning", true);
		$('#txtCodTrab_new').focus();
		return 1;
	}

	if ($('#selCargo_new').val() == -1) {
		MsgBox("Falta asignar el cargo Laboral.", "warning", true);
		$('#selCargo_new').focus();
		return 1;
	}

	if ($('#selHorarioLuVi_new').val() == -1) {
		MsgBox("Falta asignar el horario Laboral.", "warning", true);
		$('#selHorarioLuVi_new').focus();
		return 1;
	}

	if (!regExpFecha.test($('#txtFechaIngreso_new').val())) {
		MsgBox("La fecha de ingreso no es correcta.", "warning", true);
		$('#txtFechaIngreso_new').focus();
		return 1;
	}

	if ($('#selMotivoIngreso_new').val() == -1) {
		MsgBox("Falta asignar el motivo de ingreso.", "warning", true);
		$('#selMotivoIngreso_new').focus();
		return 1;
	}

	if ($('#selTipoContrato_new').val() == -1) {
		MsgBox("Falta asignar el tipo de contrato.", "warning", true);
		$('#selTipoContrato_new').focus();
		return 1;
	}

}

var validaDatosTabGenerales = function validaDatosTabGenerales() {

	if ($('#txtApePat').val() == '') {
		MsgBox("Falta registrar el Apellido Paterno.", "warning", true);
		$('#txtApePat').focus();
		return 1;
	}

	if ($('#txtApeMat').val() == '') {
		MsgBox("Falta registrar el Apellido Materno.", "warning", true);
		$('#txtApeMat').focus();
		return 1;
	}

	if ($('#txtNombres').val() == '') {
		MsgBox("Falta registrar el nombre.", "warning", true);
		$('#txtNombres').focus();
		return 1;
	}
	if ($('#txtRuc_data').val().length < minRuc && $('#txtRuc_data').val() != "") {
		MsgBox("El numero  de RUC debe ser minimo 11 digitos.", "warning", true);
		$('#txtRuc_data').focus();
		return 1;
	}

}

var validaDatosTabFamiliar = function validaDatosTabFamiliar() {

	if ($('#txtApellidoP_Familiar').val().trim() == '') {
		MsgBox("Falta registrar el Apellido Paterno del Familiar.", "warning", true);
		$('#txtApellidoP_Familiar').focus();
		return 1;
	}

	if ($('#txtApellidoM_Familiar').val().trim() == '') {
		MsgBox("Falta registrar el Apellido Materno del Familiar.", "warning", true);
		$('#txtApellidoM_Familiar').focus();
		return 1;
	}

	if ($('#txtNombres_Familiar').val().trim() == '') {
		MsgBox("Falta registrar el nombre del Familiar.", "warning", true);
		$('#txtNombres_Familiar').focus();
		return 1;
	}

	if ($('#txtNroDocVinculo').val().trim() == '') {
		MsgBox("Falta registrar el Nro. Doc. Vínculo.", "warning", true);
		$('#txtNroDocVinculo').focus();
		return 1;
	}

	if ($('#txtNroDocVinculo').val().trim() == '') {
		MsgBox("Falta registrar el Nro. Doc. Identidad.", "warning", true);
		$('#txtNroDocVinculo').focus();
		return 1;
	}
}

var validaDatosTabRetencion = function validaDatosTabRetencion() {

	if ($('#txtApellidoP_Familiar').val().trim() == '') {
		MsgBox("Falta registrar el Apellido Paterno del Familiar.", "warning", true);
		$('#txtApellidoP_Familiar').focus();
		return 1;
	}

	if ($('#txtApellidoM_Familiar').val().trim() == '') {
		MsgBox("Falta registrar el Apellido Materno del Familiar", "warning", true);
		$('#txtApellidoM_Familiar').focus();
		return 1;
	}

	if ($('#txtNombres_Familiar').val().trim() == '') {
		MsgBox("Falta registrar el nombre del Familiar.", "warning", true);
		$('#txtNombres_Familiar').focus();
		return 1;
	}

	if ($('#txtNroDocVinculo').val().trim() == '') {
		MsgBox("Falta registrar el Nro. Doc. Vínculo.", "warning", true);
		$('#txtNroDocVinculo').focus();
		return 1;
	}

	if ($('#txtNroDocVinculo').val().trim() == '') {
		MsgBox("Falta registrar el Nro. Doc. Identidad.", "warning", true);
		$('#txtNroDocVinculo').focus();
		return 1;
	}
}

var validaDatosTabBienes = function validaDatosTabBienes() {

	if ($('#txt_BIENES_Nombre').val().trim() == '') {
		MsgBox("Falta definir el nombre de un bien.", "warning", true);
		$('#txt_BIENES_Nombre').focus();
		return 1;
	}

	if (!regExpFecha.test($('#txt_BIENES_FechaEntrega').val())) {
		MsgBox("La fecha de entrega del bien no es correcta.", "warning", true);
		$('#txt_BIENES_FechaEntrega').focus();
		return 1;
	}
}

var validaDatosTabDocuEsc = function validaDatosTabDocuEsc() {

	if ($('#txtDescripcion_Doc').val().trim() == '') {
		MsgBox("Falta registrar la descripcion del Documento.", "warning", true);
		$('#txtDescripcion_Doc').focus();
		return 1;
	}
	if ($('#fileDocEscan').val().length == 0) {
		MsgBox("Falta seleccionar un Documento.", "warning", true);
		$('#fileDocEscan').focus();
		return 1;
	}
}

var validaDatosTabOtrosDatos = function validaDatosTabOtrosDatos() {
	if ($('#txtDescripcion_OtrosDatos').val().trim() == '') {
		MsgBox("Falta registrar la descripcion.", "warning", true);
		$('#txtDescripcion_OtrosDatos').focus();
		return 1;
	}
	if ($('#txtAbrv_OtrosDatos').val().trim() == '') {
		MsgBox("Falta registrar la Abreviatura.", "warning", true);
		$('#txtAbrv_OtrosDatos').focus();
		return 1;
	}
}

var validaDatosTabFormacion = function validaDatosTabFormacion() {
	if ($('#txtTitulo_Formacion').val().trim() == '') {
		MsgBox("Complete el Título de la formación.", "warning", true);
		return 1;
	}
	if ($('#txtAnio_Formacion').val().trim() == '') {
		MsgBox("Complete el año de la formación.", "warning", true);
		return 1;
	}
	if ($('#txtMeses_Formacion').val().trim() == '') {
		MsgBox("Complete los meses de la formación.", "warning", true);
		return 1;
	}
	if ($('#txtDias_Formacion').val().trim() == '') {
		MsgBox("Complete los días de la formación.", "warning", true);
		return 1;
	}
	if ($('#txtAnioEgreso_Formacion').val().trim() == '') {
		MsgBox("Complete año de egreso de la formación.", "warning", true);
		return 1;
	}
	if ($('#txtDesde_Formacion').val().trim() == '') {
		MsgBox("Complete el campo desde de la formación.", "warning", true);
		return 1;
	}
	if ($('#txtHasta_Formacion').val().trim() == '') {
		MsgBox("Complete el campo hasta de la formación.", "warning", true);
		return 1;
	}
	//if ($('#selCarrera_Formacion').prop('selectedIndex') == -1) {
	//    alert('No se a seleccionado una carrera.')
	//    return;
	//}
}

var validaDatosTabControles = function validaDatosTabControles() {
	if (!regExpFecha.test($('#txtFechaEmision').val())) {
		MsgBox("La fecha de emision del control no es correcta.", "warning", true);
		$('#txtFechaEmision').focus();
		return 1;
	}
	if (!regExpFecha.test($('#txtFechaCaducidad').val())) {
		MsgBox("La fecha de caducidad del control no es correcta.", "warning", true);
		$('#txtFechaCaducidad').focus();
		return 1;
	}
}

var validaDatosTabMeritFalt = function validaDatosTabMeritFalt() {
	if ($('#selTipo_MeritosDemeritos').val() == null) {
		MsgBox("Falta registrar el tipo de merito/falta.", "warning", true);
		$('#selTipo_MeritosDemeritos').focus();
		return 1;
	}
	if (!regExpFecha.test($('#txtFecha_MeritosDemeritos').val())) {
		MsgBox("La fecha reportada no es correcta.", "warning", true);
		$('#txtFecha_MeritosDemeritos').focus();
		return 1;
	}
	if ($('#txtDescripcion_MeritosDemeritos').val().trim() == '') {
		MsgBox("Falta registrar la descripcion del merito/falta.", "warning", true);
		$('#txtDescripcion_MeritosDemeritos').focus();
		return 1;
	}
}

var validaInicializaDefault = function validaInicializaDefault() {
	$('#selDocid_Familiar').trigger("change");
	$("#selTipoDocIdent").trigger("change");
	//$('#selInstEducativa_Formacion').trigger('change');
}

function GetFiltros() {
	g_proyecto = $('#selectProyecto').val();
	g_ccosto = $('#selectCentrosCosto').val();
	g_unegocio = $('#selectUnidadesNegocio').val();
	g_area = $('#selectArea').val();
	g_localidad = $('#selectLocalidad').val();
	g_tipoplanilla = $('#selectTipoPlanilla').val();
	g_estadotrab = $('#selectEstado').val();
	g_filtro = '';
}

var GetParametrosPlanilla = function GetParametrosPlanilla() {
	var data = __Ajax('GET', portal + '/Personal/GetParametrosPlanilla', {}, false);

	if (data.length > 0) {
		g_porcenAvance = data[0].porcen_adelanto;
		g_codTrabajadorCorr = data[0].ind_codigo_corr;
		g_flgSueldoCero = data[0].flg_sueldo_cero;

		if (g_codTrabajadorCorr == 'N') {
			$('#txtCodTrabajador').prop('disabled', 'disabled');
		}
	}
};

$(document).ready(function () {

	UIInput.Letters('#txtColorPiel');
	//UIMultipleSelect.Init('#selCargo_Org');

	$('#pills-tab a').on('shown.bs.tab', function (e) {
		let ths = $(this);
		let li = ths.parents('li');
		var jsonRow = tableColaboradores.row('.clicked').data();
		if (!UIEmpty(jsonRow)) {
			let pCodEntidad = jsonRow.entidad;
			let pCodPersonal = jsonRow.personal;
			ClearInput('Bi');
			ClearInput('Me');
			g_accion_bienes = 'N';
			g_accion_merito = 'N';
			g_codPersonal = pCodPersonal;
			g_codEntidad = pCodEntidad;
			GetFiltros();
			switch (li.prop('id')) {
				case 'tabGenerales':
					GetDatosGenerales(pCodEntidad, pCodPersonal);
					break;
				case 'tabLaborales':
					GetDatosLaborales(pCodPersonal);
					break;
				case 'tabPersonales':
					GetDatosPersonales(pCodEntidad, "AD");
					break;
				case 'tabOrganizacion':
					GetOrganizacion(pCodPersonal);
					GetPersonalDistribucionCC(pCodPersonal, g_proyecto);
					GetPersonalDistribucionLocalidad(pCodPersonal);
					GetPersonalDistribucionUnidadDeNegocio();
					break;
				case 'tabHorarios':
					GetHorario(pCodPersonal);
					break;
				case 'tabHistorial':
					GetPersonalHistorial(pCodEntidad);
					break;
				case 'tabFamiliares':
					GetFamiliares(pCodEntidad);
					break;
				case 'tabRetenciones':
					GetRetenciones(pCodPersonal);
					break;
				case 'tabBienes':
					GetBienes_Admin(pCodPersonal);
					break;
				case 'tabDocsEscaneados':
					GetDocsEscaneados(pCodPersonal);
					break;
				case 'tabVariacionSueldo':
					GetVariacionSueldo(pCodPersonal);
					break;
				case 'tabFormacion':
					GetFormacion(pCodPersonal);
					break;
				case 'tabControles':
					GetPersonalcontrol(pCodPersonal);
					break;
				case 'tabMeritosFaltas':
					GetMeritosDemeritos(pCodPersonal);
					break;
				case 'tabSBS':
					break;
				case 'tabOtrosDatos':
					GetOtrosDatos(pCodPersonal);
					break;
				case 'tabReloj':
					GetReloj(pCodPersonal);
					break;
				case 'tabVariacionClasificadores':
					Load.VariacionClasificadores.Ready(jsonRow);
					break;
			}
			$('#codPersonal').val(g_codPersonal);
			$('#modalPersonal').modal('show');
			validaInicializaDefault();
		}
	});

	containerPage = $('#modalPersonal');
	containerColaboradores = $('#divColaboradores');
	var currentIndex;

	UIDatePicker.Init('input.txtFechaDDMMYYYY');

	$('#tableColaboradores').on('click', 'tbody tr', function (e) {
		var ths = $(this);
		//$('#tableColaboradores tbody tr.clicked').removeClass('clicked');
		var trs = tableColaboradores.$('tr');
		trs.removeClass('clicked');
		$('input[name="chkTableColaboradores"]', trs).prop('checked', false);
		ths.addClass('clicked');
		var jsonRow = tableColaboradores.row(ths).data();
		if (!UIEmpty(jsonRow)) {
			$('input[name="chkTableColaboradores"]').prop('checked', false);
			$(`#customCheck${jsonRow.personal}`).prop('checked', true);
			$.LoadingOverlay("show");
			GetDetallePersonal(jsonRow.entidad, jsonRow.personal);
			$.LoadingOverlay("hide");
		}
	}).on('click', 'tbody tr td:first-child', function (e) {
		e.stopPropagation();
	}).on('click', 'tbody tr td:first-child input', function (e) {
		e.stopPropagation();
		var ths = $(this);
		var Row = ths.closest("tr");
		//Row.addClass('selected');
		var trs = tableColaboradores.$('tr');
		trs.removeClass('clicked');
		$('input[name="chkTableColaboradores"]', trs).prop('checked', false);
		ths.closest('tr').addClass('clicked');
		var jsonRow = tableColaboradores.row(Row).data();
		if (!isEmpty(jsonRow)) {
			
			$('input[name="chkTableColaboradores"]').prop('checked', false);
			ths.prop('checked', true);
		}
	});

	//Evento Blur y Focus  en Fecha para mantener el valor  inicial si no  es asignada ninguna fecha del datepicker
	$('#txtIngreso,#txtFecIngreso2,#txtFechaAfilEPS,#txtSctrDesde,#txtSctrHasta,#txtDelAsig, #txtAlAsig,#txtFechaNac,#txtFechaAfiliacion,'
		+ '#txtFechaAlta_Familiar,#txtFechaNac_Familiar, #txtFechaBaja_Familiar,#txtFechaAfilEPS_Familiar, #txtDesde_Retencion,'
		+ '#txtHasta_Retencion,#txt_BIENES_FechaEntrega, #txt_BIENES_FechaDevolucion, #txt_BIENES_FechaDocEntrega, #txtFechaEmision,'
		+ '#txtFechaCaducidad,#txtFecha_MeritosDemeritos').on("blur", function (e) {
			e.preventDefault();
			if ($(this).val() == "") {
				$(this).val(currentIndex);
				currentIndex = "";
			}
		});
	$('#txtIngreso,#txtFecIngreso2,#txtFechaAfilEPS,#txtSctrDesde,#txtSctrHasta,#txtDelAsig, #txtAlAsig,#txtFechaNac,#txtFechaAfiliacion,'
		+ '#txtFechaAlta_Familiar,#txtFechaNac_Familiar, #txtFechaBaja_Familiar,#txtFechaAfilEPS_Familiar, #txtDesde_Retencion,'
		+ '#txtHasta_Retencion,#txt_BIENES_FechaEntrega, #txt_BIENES_FechaDevolucion, #txt_BIENES_FechaDocEntrega, #txtFechaEmision,'
		+ '#txtFechaCaducidad,#txtFecha_MeritosDemeritos').on("focus", function (e) {
			e.preventDefault();
			currentIndex = $(this).val();
		});

	$('#selTipoDoc_consulta').on('change', function (e) {
		e.preventDefault();
		ValidateTipoDocPersonal($(this).prop('id'), 'txtNroDoc_consulta', 1);
		$('#selTipoDoc_new').val($(this).val()).trigger("change");
		$('#txtNumDoc_new').val('');
		$('#txtNroDoc_consulta').focus();
	});

	$('#selTipoDoc_new').on('change', function (e) {
		e.preventDefault();
		ValidateTipoDocPersonal($(this).prop('id'), 'txtNumDoc_new', 1);
	});

	$('#selTipoDocIdent').on('change', function (e) {
		e.preventDefault();
		ValidateTipoDocPersonal($(this).prop('id'), 'txtNroDocIdent', 3);
	});

	$('#selAFP').on('change', function (e) {
		e.preventDefault();
		if ($("#selAFP option:selected").text().indexOf('AFP') == -1) {
			$('#rbRemuneracion').prop('checked', false);
			$('#rbMixta').prop('checked', false);
		}
	});

	$('#selAFP_new').on('change', function (e) {
		e.preventDefault();
		if ($("#selAFP_new option:selected").text().indexOf('AFP') == -1) {
			$('#rbComisionRemu_new').prop('checked', false);
			$('#rbComisionMixta_new').prop('checked', false);
		}
	});
	$('#selDocid_Familiar').on('change', function (e) {
		e.preventDefault();
		ValidateTipoDocPersonal($(this).prop('id'), 'txtNroDocId_Familiar', 3);
	});

	$('#txtMontoAdelanto_new').on('keypress', function (e) {
		if (isNaN($(this).val()) == false) {
			$('#txtPorcenAdelanto_new').val(0)
		}
	});

	$('#txtPorcenAdelanto_new').on('keypress', function (e) {
		if (isNaN($(this).val()) == false) {
			$('#txtMontoAdelanto_new').val(0);
		}
	});

	$('#txtMontoAdelanto').on('keypress', function (e) {
		if (isNaN($(this).val()) == false) {
			$('#txtPorcentajeAdelanto').val(0)
		}
	});

	$('#txtPorcentajeAdelanto').on('keypress', function (e) {
		if (isNaN($(this).val()) == false) {
			$('#txtMontoAdelanto').val(0);
		}
	});

	$('#txtValorPorcen_Retencion').on('keypress', function (e) {
		if (isNaN($(this).val()) == false) {
			$('#txtMonto_Retencion').val(0)
		}
	});

	$('#txtMonto_Retencion').on('keypress', function (e) {
		if (isNaN($(this).val()) == false) {
			$('#txtValorPorcen_Retencion').val(0);
		}
	});

	//$('#selTipoDoc_consulta').prop('selectedIndex', 3);
	$('#txtFechaNac_new, #txtFechaAfil_new, #txtFechaIngreso_new, #txtFinContrato, #txtInicioPeriodoPrueba, #txtFinPeriodoPrueba').mask('00/00/0000', { placeholder: "__/__/____" });

	GetParametrosPlanilla();

	inputFoto.onchange = function (e) {

		e.preventDefault();
		var input = document.getElementById('inputFoto');
		var files = input.files;
		var formData = new FormData();
		formData.append("file", files[0]);
		formData.append("codPersonal", g_codPersonal);

		$.ajax({
			url: portal + "/User/CambioFotoPersonal",
			data: formData,
			processData: false,
			contentType: false,
			type: "POST",
			success: function (data) {
				if (data.respuesta == 1) {
					$('#form input[type=file]').val('');
					$('#imgPersonal').attr('src', '');
					var imagen = portal + "/Noticias/ShowImagePersonal?codPersonal=" + g_codPersonal + "&timestamp=" + new Date().getTime();
					$('#imgPersonal').attr('src', imagen);
					MsgBox("success");
					//GetColaboradores();
				}
				else {
					MsgBox(data.mensaje, "warning", false);
				}
			}
		});

	}

	//PERSONALES
	UIDatePicker.Init('#txtFechaNac, #txtFechaAfiliacion');

	$('#txtNroHijos,#txtTalla,#txtOjoIzq,#txtOjoDer,#txtOidosIzq,#txtOidosDer,#txtPeso,#txtNro_Familiar').autoNumeric('init');

	//BIENES
	UIDatePicker.Init('#txt_BIENES_FechaEntrega, #txt_BIENES_FechaDevolucion, #txt_BIENES_FechaDocEntrega');
	$('#txt_BIENES_CostoMn').autoNumeric('init');
	$('#txt_BIENES_CostoMe').autoNumeric('init');
	$('#txt_BIENES_CostoReposMn').autoNumeric('init');
	$('#txt_BIENES_CostoReposMe').autoNumeric('init');

	//VARIACIÓN SUELDO
	UIDatePicker.Init('#txtFeachaDesdeVS');
	$('#txtFeachaDesdeVS').datepicker('setDate', new Date());

	//MERITOS Y DEMERITOS
	$('#txtFecha_MeritosDemeritos').datepicker({
		autoclose: true,
		format: "dd/mm/yyyy"
	});

	//MERITOS Y DEMERITOS
	//**************************************************

	//**************************************************
	//ADD PERSONAL
	$('#txtFechaNac_new, #txtFechaAfil_new, #txtFechaIngreso_new, #txtFinContrato, #txtInicioPeriodoPrueba, #txtFinPeriodoPrueba').datepicker({
		autoclose: true,
		format: "dd/mm/yyyy",
		language: "es",
	});
	$('#txtMontoBasico_new, #txtTasa_new, #txtMontoAdelanto_new, #txtPorcenAdelanto_new, #txtValorPorcen_Retencion, #txtMonto_Retencion').autoNumeric('init');

	UIEvent.Disabled.Change('#chkDomiciliado', false, '#selDepartamento_Personal, #selProvincia_Personal, #selDistrito_Personal');

	//#region TAB ORGANIZACIÓN
	$('#txtCargo_Org').on('dblclick', async function (e) {
		try {
			var jsonCargos = JSON.parse(strCargos.replaceAll('&quot;', '"'));
			var jsonOptions = {
				TituloDeModal: `Cargo actual: <span class="badge badge-warning">${$('#selCargo_Org option:selected').text()}</span>`,
				//multipleSelect:true,
				source: jsonCargos,
				columns: [
					{ data: 'codigo', title: 'Código' },
					{ data: 'cadena1', title: 'Cadena1' },
					{ data: 'nombre', title: 'Nombre' },
				]
			};
			await UIModal.Table(jsonOptions).then(function (r) {
				console.log(r);
				if (r != null && r.SelectedRows.length > 0) {
					var jsonRow = r.SelectedRows[0];
					$('#selCargo_Org').val(jsonRow.codigo);
				}
			});
		} catch (e) {
			console.log(e);
		}
	});
	//#endregion

	//ADD PERSONAL
	//**************************************************

	//**************************************************
	//LABORALES
	$('#txtFecIngreso2,#txtFechaAfilEPS, #txtSctrDesde, #txtSctrHasta,#txtDelAsig,#txtAlAsig, #txtDelAsig,#txtAlAsig').datepicker({
		autoclose: true,
		format: "dd/mm/yyyy",
	});

	$('#txtIngreso,#txtFecIngreso2,#txtFechaAfilEPS, #txtSctrDesde, #txtSctrHasta, #txtFechaAfiliacion').mask('00/00/0000', { placeholder: "__/__/____" });

	$('#txtTasaVidaLey,#txtMontoAdelanto,#txtPorcentajeAdelanto,#txtBasico,#txtBono,#txtOidosDer,#txtPeso').autoNumeric('init');

	//LABORALES
	//**************************************************

	$('#selDepartamento, #selDepartamento_Personal, #selDepartamento_Familiar, #selDepartamentoNac_new, #selDepartamento_new').on('change', function (e) {
		e.preventDefault();
		var idProv = $(this).data('prov');
		var idDist = $(this).data('dist');
		$('#' + idProv).empty();
		$('#' + idDist).empty();
		$('#' + idDist).attr('disabled', 'disabled');
		GetProvincias(idProv, e.target.value);
	});

	getListClaseProceso();
	//$('#selectClaseProceso_Retencion').multipleSelect({ seleccioneAllText: 'Seleccionar todo' });

	$('#txtDesde_Retencion').datepicker({ autoclose: true, format: 'dd/mm/yyyy' });
	$('#txtHasta_Retencion').datepicker({ autoclose: true, format: 'dd/mm/yyyy' });
	$('#txtFechaAlta_Familiar').datepicker({ autoclose: true, format: 'dd/mm/yyyy' });
	$('#txtFechaNac_Familiar').datepicker({ autoclose: true, format: 'dd/mm/yyyy' });
	$('#txtFechaBaja_Familiar').datepicker({ autoclose: true, format: 'dd/mm/yyyy' });
	$('#txtFechaAfilEPS_Familiar').datepicker({ autoclose: true, format: 'dd/mm/yyyy' });
	$('#txtFechaEmision').datepicker({ autoclose: true, format: 'dd/mm/yyyy' });
	$('#txtFechaCaducidad').datepicker({ autoclose: true, format: 'dd/mm/yyyy' });

	$('.selClasificador').on('change', function (e) {
		e.preventDefault();
		GetColaboradores();
	});

	$('.clasificadorNew').on('change', function (e) {
		e.preventDefault();
		$('#' + $(this).data('label')).text($(this).val().trim());
	});

	$('#selProvincia, #selProvincia_Personal, #selProvincia_Familiar, #selProvinciaNac_new, #selProvincia_new').on('change', function (e) {
		e.preventDefault();
		let idDist = $(this).data('dist');
		document.getElementById(idDist).removeAttribute("disabled");
		GetDistritos(idDist, e.target.value);
	});

	$('.selClasificador').on('change', function (e) {
		e.preventDefault();
		GetColaboradores();
	});

	$('#btnInconsistencias').on('click', async function (e) {
		e.preventDefault();
		GetFiltros();
		var jsonParam = {
			CodigoDeTipoPlanilla: g_tipoplanilla == '-1' ? '%' : g_tipoplanilla,
			CodigoDeProyecto: g_proyecto,
			CodigoDeCC: g_ccosto,
			CodigoDeLocalidad: g_localidad,
			CodigoDeUN: g_unegocio,
			CodigoDeArea: g_area,
			Estado: g_estadotrab,
		};
		await UIMInconsistencia.Personal(jsonParam);
		//var Options = {
		//    keytitle: 'cod_incon',
		//    valuetitle: 'inconsistencia',
		//    XLSX: {
		//        show: true,
		//        action: async function () {
		//            UISave.URL(`${portal}/ajax/personal/GetInconsistenciasPersonalXLSX/?` + $.param(jsonParam));
		//        }
		//    },
		//};
		//await UIModal.Inconsistencia('ajax/personal', 'GetInconsistenciasPersonal', jsonParam, Options);
	});

	$('#chkDomiciliado_new').on('change', function (e) {
		var ths = $(this);
		//if (!ths.is(':checked')) {
		//    $('#selDepartamento_new').val('-1').trigger('change');
		//}
		if (!ths.is(':checked')) {
			$('#selDepartamento_new').val('-1').change();
			$('#selDepartamento_new, #selProvincia_new, #selDistrito_new').attr('disabled', 'disabled');
		}
		else {
			$('#selDepartamento_new, #selProvincia_new, #selDistrito_new').removeAttr('disabled');
		}
	});

	$('#modalNuevoPersonal .selectClasificadoresNEW').on('change', async function () {
		var jsonParam = {
			CodigoDeProyecto: $('#selProyecto_new').val(),
			CodigoDeCC: $('#selCC_new').val(),
			CodigoDeUN: $('#selUN_new').val(),
			CodigoDeArea: $('#selArea_new').val(),
			CodigoDeLocalidad: $('#selLocalidad_new').val(),
			CodigoDeTipoPlanilla: $('#selTipoPlanilla_new').val(),
		};
		var printTemp = function (_jquery, _value) {
			return;//Quitar esto si estará en pruebas
			if (UIEmpty(_value)) {
				console.log(`%cSelector: ${_jquery} ,Valor: ${_value}, IsEmpty: ${true}`, `color: orange`);
			}
			else {
				console.log(`%cSelector: ${_jquery} ,Valor: ${_value}, IsEmpty: ${false}`, `color: #148f32`);
			}
		}
		//Comprobar que el valor no sea vacio o nulo, y luego asignarlo
		var setIfIsNotEmpty = function (_jquery, _value) {
			printTemp(_jquery, _value);
			if (!UIEmpty(_value)) {
				$(_jquery).val(_value);
			}
		}
		var setIfIsNotEmptyCK = function (_jquery, _value, _trigger) {
			printTemp(_jquery, _value);
			if (!UIEmpty(_value)) {
				UICheckbox.setValue(_jquery, _value);
				if (_trigger) {
					$(_jquery).trigger('change');
				}
			}
		}
		var setIfIsNotEmptyRB = function (_jquery, _value) {
			printTemp(_jquery, _value);
			if (!UIEmpty(_value)) {
				$(`input[name="${_jquery}"][value="${_value}"]`).prop('checked', true);
			}
		}
		await UIAjax.invokeMethodAsync('ajax/personal', 'GetRegistroPersonalDatosDfltSingle', jsonParam).then(function (r) {
			if (r.length > 0) {
				var objSingle = r[0];
				//console.log(objSingle);
				//El orden está como la configuración del PB (Aplicativo Desktop)
				setIfIsNotEmpty('#selTipoContrato_new', objSingle.cod_tipo_contrato);
				//Falta el puesto, no se encontró en la parte de la web
				setIfIsNotEmpty('#selCargo_new', objSingle.cod_cargo);
				setIfIsNotEmpty('#selCategoria_new', objSingle.cod_categoria);
				setIfIsNotEmpty('#selMotivoIngreso_new', objSingle.cod_motivo_ingreso);
				setIfIsNotEmpty('#selTipoTrab_new', objSingle.cod_tipo_trabajador);
				setIfIsNotEmpty('#selClase_new', objSingle.tipo_trabajador);
				setIfIsNotEmpty('#selAlcanceCargo_new', objSingle.cod_alcance);
				setIfIsNotEmptyCK('#chkAfilEPS_new', objSingle.afiliacion_eps, true);
				setIfIsNotEmpty('#selAfilEPS_new', objSingle.cod_eps);
				setIfIsNotEmptyCK('#chkSCTRPension_new', objSingle.sctr_pension > 0, true);//Como no hay un flg se creó este artificio
				setIfIsNotEmpty('#selSCTRPension_new', objSingle.sctr_pension);
				setIfIsNotEmptyCK('#chkSCTRSalud_new', objSingle.sctr_salud > 0, true);
				setIfIsNotEmpty('#selSCTRSalud_new', objSingle.sctr_salud);
				setIfIsNotEmpty('#selRegistroLaboral_new', objSingle.tipo_monto_periodo);
				setIfIsNotEmptyCK('#chkAsigFam_new', objSingle.flg_asignacion_familiar);
				setIfIsNotEmptyCK('#chkEsSaludVida_new', objSingle.essalud_vida);
				setIfIsNotEmpty('#selMonedaBasico_new', objSingle.cod_moneda_basico);
				setIfIsNotEmpty('#txtMontoBasico_new', objSingle.monto);
				setIfIsNotEmptyRB('grPago', objSingle.flg_pago_cuenta);
				setIfIsNotEmptyRB('grPagoCTS', objSingle.flg_boleta_reciboh);
				setIfIsNotEmpty('#selBancoPago_new', objSingle.cod_banco_pago);
				setIfIsNotEmpty('#selMonedaPago_new', objSingle.cod_moneda_pago);
				setIfIsNotEmpty('#txtNumCuentaPago_new', objSingle.num_cuenta_pago);
				setIfIsNotEmpty('#selBancoCTS_new', objSingle.cod_banco_cts);
				setIfIsNotEmpty('#selMonedaCTS_new', objSingle.cod_moneda_cts);
				setIfIsNotEmpty('#txtNumCuentaCTS_new', objSingle.num_cuenta_cts);
				setIfIsNotEmptyCK('#chkAdelanto_new', objSingle.flg_adelanto, true);
				setIfIsNotEmpty('#txtMontoAdelanto_new', objSingle.monto_adelanto);
				setIfIsNotEmpty('#txtPorcenAdelanto_new', objSingle.porcen_adelanto);
				setIfIsNotEmptyCK('#chkVidaLey_new', objSingle.flg_vida_ley, true);
				setIfIsNotEmpty('#selSeguro_new', objSingle.codigo_vida_ley);
				setIfIsNotEmpty('#txtTasa_new', objSingle.tasa_vida_ley);
				setIfIsNotEmpty('#selHorarioLuVi_new', objSingle.horario);
				setIfIsNotEmptyCK('#chkSenati_new', objSingle.flg_senati, false);
			}
		});
	});

	$('#btnAddPersonal').on('click', function (e) {
		e.preventDefault();

		//UIMultipleSelect.Init('#selUN_new, #selCC_new, #selArea_new', { selectAll: false });
		UIMultipleSelect.Init('#modalNuevoPersonal .selectClasificadoresNEW', { selectAll: false });


		$("#txtNumDoc_new,#txtRuc_new,#txtApellidoP_new,#txtApellidoM_new,#txtNombres_new,#txtVia_new," +
			"#txtNro_new,#txtInterior_new,#txtZona_new,#txtReferencia_new,#txtEmailLab_new,#txtFechaNac_new," +
			"#txtCodTelefono,#telefono1,#telefono2,#telefono3,#txtFax,#txtEmailPer_new,#txtCodTrab_new," +
			"#txtFinContrato,#txtInicioPeriodoPrueba,#txtFinPeriodoPrueba").val('');

		$("#selTipoPlanilla_new,#selProyecto_new,#selCC_new,#selArea_new,#selBrevete_new," +
			"#selAlcanceCargo_new,#selHorarioLuVi_new").val(-1);

		$('#txtNroHijos_new').val(0);
		$('#txtMontoBasico_new').val(0);
		$('#txtTasa_new').val(0);
		$('#txtMontoAdelanto_new').val(0);
		$('#txtPorcenAdelanto_new').val(0);

		$('#selPais_new').val(189);//Peruano

		$('#selDepartamentoNac_new').val('-1');
		$('#selDepartamento_new').val('-1');

		$('#chkDomiciliado_new').prop('checked', false).trigger('change');

		$('#lblTipoPlanilla_new').text($('#selTipoPlanilla_new').val());
		$('#lblLocalidad_new').text($('#selLocalidad_new').val());
		$('#lblProyecto_new').text($('#selProyecto_new').val());
		$('#lblUN_new').text($('#selUN_new').val());
		$('#lblCC_new').text($('#selCC_new').val());
		$('#lblArea_new').text($('#selArea_new').val());

		$('#selAFP_new').val(-1);//Sin Fondeo de Pensiones
		$('#rbComisionMixta_new').prop('checked', false);
		$('#rbComisionRemu_new').prop('checked', false);
		$('#chkAfilEPS_new').prop('checked', false);
		$('#selAfilEPS_new').prop('disabled', true);
		$('#chkSCTRPension_new').prop('checked', false);
		$('#chkSCTRSalud_new').prop('checked', false);
		$('#chkAsigFam_new').prop('checked', false);
		$('#chkEsSaludVida_new').prop('checked', false);
		$('#chkSenati_new').prop('checked', false);
		$('#chkVidaLey_new').prop('checked', false);
		$('#chkAdelanto_new').prop('checked', false);


		$('#selClase_new').val('P').trigger('change');


		$('#modalNuevoPersonal').modal({ show: true });
		$("#selTipoDoc_consulta").trigger("change");
		$('#txtNroDoc_consulta').focus();
	});

	$('#btnIngresoMasivoPersonal').on('click', function (e) {
		e.preventDefault();
		$("#tbodyError").empty();
		$("#tbodyOk").empty();
		$('#modalIngresoMasivo').modal({ show: true });
	});

	$('#btnSaveDatosGenerales').on('click', function (e) {
		event.preventDefault();
		if (validaDatosTabGenerales() == 1) {
			return;
		};
		GrabarDatosGenerales();
	});

	$('#btnSaveDatosLaborales').on('click', function (e) {
		event.preventDefault();
		GrabarDatosLaborales();
	});

	$('#btnSaveDatosPersonales').on('click', function (e) {
		event.preventDefault();
		GrabarDatosPersonalesAdmin();
	});

	$('#btnSaveOrganizacion').on('click', function (e) {
		e.preventDefault();
		GrabarOrganizacion();
	});

	$('#btnSaveHorarios').on('click', function (e) {
		e.preventDefault();
		var d1 = '', d2 = '', d3 = '', d4 = '', d5 = '', d6 = '', d7 = '';

		$('#tbodyHorarios tr').each(function () {
			var cod_turno = $(this).data('cod');

			var lun = $(this).find('#chkLunes');
			var mar = $(this).find('#chkMartes');
			var mie = $(this).find('#chkMiercoles');
			var jue = $(this).find('#chkJueves');
			var vie = $(this).find('#chkViernes');
			var sab = $(this).find('#chkSabado');
			var dom = $(this).find('#chkDomingo');

			if ($(lun).prop('checked')) d2 = cod_turno;
			if ($(mar).prop('checked')) d3 = cod_turno;
			if ($(mie).prop('checked')) d4 = cod_turno;
			if ($(jue).prop('checked')) d5 = cod_turno;
			if ($(vie).prop('checked')) d6 = cod_turno;
			if ($(sab).prop('checked')) d7 = cod_turno;
			if ($(dom).prop('checked')) d1 = cod_turno;
		});

		GrabarHorarios(d1, d2, d3, d4, d5, d6, d7);
	});

	//OTROS DATOS
	$('#btnAddOtrosDatos').on('click', function (e) {
		e.preventDefault();
		$('#selTipoDato_OtrosDatos').prop('selectedIndex', 0);
		$('#txtDescripcion_OtrosDatos, #txtAbrv_OtrosDatos').val('');
		$("#addOtrosDatos").collapse('show'); //toggle
	});

	$('#btnCancelOtrosDatos').on('click', function (e) {
		e.preventDefault();
		$("#addOtrosDatos").collapse('hide'); //toggle
	});

	$('#btnSaveOtrosDatos').on('click', function (e) {
		e.preventDefault();
		let desc = $('#txtDescripcion_OtrosDatos').val().trim();
		let abrv = $('#txtAbrv_OtrosDatos').val().trim();
		let tipoDato = $('#selTipoDato_OtrosDatos').val();
		if (validaDatosTabOtrosDatos() == 1) {
			return;
		};
		if (tipoDato == null || tipoDato == '') {
			tipoDato = -1;
		}

		GrabarOtroDato(desc, abrv, parseInt(tipoDato));
	});

	$('#btnSaveRptaOtrosDatos').on('click', function (e) {
		e.preventDefault();
		var rpta = -1;
		$('#tbodyOtrosDatos tr').each(function () {
			let codigo = parseInt($(this).data('numero'));
			let valor = $($(this).find('input[type=text]')).val();
			rpta = GrabarRptaOtrosDatos(codigo, valor);
		});

		if (rpta > 0) {
			MsgBox("success");
		}
	});

	$('#btnRemoveOtrosDatos').on('click', function (e) {
		e.preventDefault();
		let tr = $('#tbodyOtrosDatos tr.selected');
		if (tr.length != 1) {
			UIMensaje.REGISTRO();
			return;
		}

		EliminarOtroDato(tr.data('numero'));
	});

	//FAMILIARES
	$('#btnAddFamiliares').on('click', async function (e) {
		e.preventDefault();
		ClearInput('Fa');
		$('#tbodyFamiliares tr').removeClass('selected');
		var jsonFiltro = Load.Filtro();
		var jsonInfo = await UIFNAjax.GetInfoExtra(jsonFiltro.CodigoDeEntidad, jsonFiltro.CodigoDePersonal);
		//console.log(jsonInfo);
		$('#selDocid_Familiar').val(jsonInfo.cod_docid);
		$('#sel_Nacionalidad_Familiar').val(jsonInfo.nacion);
		$('#selVia_Familiar').val(jsonInfo.cod_via);
		$('#txtDescVia_Familiar').val(jsonInfo.tregistro_nombre_via);
		if (!UIEmpty(jsonInfo.distrito)) {
			$('#selDepartamento_Familiar').val(jsonInfo.departamento).change();
			$('#selProvincia_Familiar').val(jsonInfo.provincia).change();
			$('#selDistrito_Familiar').val(jsonInfo.distrito);
		}
		$('#selZona_Familiar').val(jsonInfo.cod_zona);
		$('#txtDescZona').val(jsonInfo.zona);
		$('#txtReferencia_Familiar').val(jsonInfo.referencia);
		$('#txtNro_Familiar').val(jsonInfo.numero);
		$('#txtInterior_Familiar').val(jsonInfo.interior);
		$('#txtTel_Familiar').val(jsonInfo.tregistro_telefono);
	});

	$('#btnRemoveFamiliares').on('click', function (e) {
		e.preventDefault();
		let tr = $('#tbodyFamiliares tr.selected');
		if (tr.length != 1) {
			UIMensaje.REGISTRO();
			return;
		}
		EliminarFamiliar(tr.data('numero'));
	});

	$('#btnSaveFamiliares').on('click', function (e) {
		e.preventDefault();
		if (validaDatosTabFamiliar() == 1) {
			return;
		}

		let accion = '';
		let numFamiliar = '';
		let tr = $('#tbodyFamiliares tr.selected');
		if (tr.length == 0)
			accion = 'N';
		else {
			accion = 'E';
			numFamiliar = tr.data('numero');
		}

		GrabarFamilares(accion, numFamiliar);

	});

	$('#selParentesco_Familiar').on('change', function () {
		var ths = $(this);
		var valor = ths.val();
		if (valor == '02') {
			$('#pnEstudiosSup').removeClass('d-none');
		}
		else {
			$('#pnEstudiosSup').addClass('d-none');
		}
	});

	$('#selEstado_Familiar').on('change', function () {
		var ths = $(this);
		if (ths.val() == 'A') {
			$('#selMotivoBaja_Familiar').val('');
			$('#selMotivoBaja_Familiar').attr('disabled', true);
			$('#txtFechaBaja_Familiar').val('');
			$('#txtFechaBaja_Familiar').attr('disabled', true);
		}
		else {
			$('#selMotivoBaja_Familiar').removeAttr('disabled');
			$('#txtFechaBaja_Familiar').removeAttr('disabled');
		}
	});

	//RETENCIONES
	$('#btnAddRetencion').on('click', function (e) {
		e.preventDefault();
		ClearInput('Re');
		$('#tbodyRetenciones tr').removeClass('selected');
	});

	$('#btnRemoveRetencion').on('click', function (e) {
		e.preventDefault();
		let tr = $('#tbodyRetenciones tr.selected');

		if (tr.length != 1) {
			UIMensaje.REGISTRO();
			return;
		}
		DeleteRetencion(tr);
	});

	$('#btnSaveRetencion').on('click', function (e) {
		e.preventDefault();
		let tr = $('#tbodyRetenciones tr.selected');
		PostRetencionPersonal();
	});

	//BIENES
	$('#btnAddBienes').on('click', function (e) {
		e.preventDefault();
		ClearInput('Bi');
		$('#tbodyBienes tr').removeClass('selected');
	});

	$('#btnRemoveBienes').on('click', function (e) {
		e.preventDefault();
		let tr = $('#tbodyBienes tr.selected');
		if (tr.length != 1) {
			UIMensaje.REGISTRO();
			return;
		}

		EliminarBienes_Admin(tr.data('item'));
	});

	$('#btnSaveBienes').on('click', function (e) {
		e.preventDefault();
		if (validaDatosTabBienes() == 1) {
			return;
		}
		$('#btnSaveBienes').attr('disabled', 'disabled');
		let accion = 'N';
		let item = 0;
		let tr = $('#tbodyBienes tr.selected');

		if (tr.length == 0) {
			accion = 'N';
			item = 0;
		}
		else {
			accion = 'E';
			item = tr.data('item');
		}
		Save_Bienes(accion, item);
	});

	$('#btnViewConstancia').on('click', function (e) {
		e.preventDefault();
		var tr = $('#tbodyBienes tr.selected');
		var item = 0;

		if (tr.length == 0) {
			MsgBox("Seleccione un bien para generar la Acta de Entrega.", "warning", true);
			$(this).attr("href", "");
		} else {
			item = tr.data('item');
			var jsonParam = {
				CodPersonal: g_codPersonal,
				CodBien: item
			};
			$.ajax({
				type: "GET",
				url: portal + '/ajax/personal/GetLegajoPersonalBienes',
				data: jsonParam,
				contentType: "application/json; charset=utf-8",
				dataType: "json",
				async: false,
				success: function success(data) {
					if (data.length > 0) {
						var value = data[0];

						$('#spanTitlePdfConstancia').text(item);

						var costo_valor = value.costo_mn;
						if (!costo_valor > 0) {
							costo_valor = value.costo_me;
						}

						var PDFDynamic = {
							info: {
								title: 'Constancia_Bienes_' + item,
							},
							//footer: {
							//    columns: [
							//        'Left part',
							//        { text: 'Right part', alignment: 'right' }
							//    ]
							//},
							//pageMargins: [40, top, 40, 40],
							content: [
								{ text: 'CONSTANCIA DE ENTREGA DE BIENES', style: 'header', alignment: 'center', decoration: 'underline' },
								//{ text: 'Mediante el presente documento se hace testimonio que al Sr(ta).:', style: 'line' },
								'Mediante el presente documento se hace testimonio que al Sr(ta).:',
								{ text: value.trabajador, style: 'line', bold: true },
								{
									text: [
										'Identificado con ',
										{ text: value.nombre_documento_identidad, bold: true },
										' Nro. ',
										{ text: value.numero_documento_identidad, bold: true },
										', se le hizo entrega del siguiente bien:'
									]
								},
								{
									//layout: 'lightHorizontalLines', // optional
									layout: 'noBorders', // optional
									table: {
										// headers are automatically repeated if the table spans over multiple pages
										// you can declare how many rows should be treated as headers
										headerRows: 1,
										widths: ['*', '*', '*', '*'],
										body: [
											['', ''],
											['Nombre del Bien', ': ' + value.nombre_bien],
											['Costo del Bien', ': ' + costo_valor],
											['Fecha de Entrega', ': ' + value.fecha_entrega],
											//[{ text: 'Bold value', bold: true }, 'Val 2']
										]
									}
								},
								{ text: 'A continuacion, el detalle del bien entregado', style: 'line' },
								{ text: value.descripcion, style: 'line' },
								{ text: '', style: 'spaceBody' },
								{ text: 'En caso de daño, pérdida, robo y/o no devolución del bien, el costo del mismo será asumido por usuario(a) autorizando el descuento correspondiente', style: 'line' },
								{ text: '__________________________', style: 'line' },
								{ text: 'RECIBI CONFORME' },
								//{ text: 'Subtítulo', style: 'subheader' },
								//{ text: 'Defining column widths', style: 'subheader' },
								//'Tables support the same width definitions as standard columns:',
								//{
								//    bold: true,
								//    ul: [
								//        'auto',
								//        'star',
								//        'fixed value'
								//    ]
								//}
							],
							styles: {
								header: {
									fontSize: 18,
									bold: true,
									margin: [0, 0, 0, 40]
								},
								line: {
									margin: [0, 10, 0, 10]
								},
								spaceBody: {
									margin: [0, 0, 0, 150]
								},
								subheader: {
									fontSize: 16,
									bold: true,
									margin: [0, 10, 0, 5]
								},
								tableExample: {
									margin: [0, 5, 0, 15]
								},
								tableHeader: {
									bold: true,
									fontSize: 13,
									color: 'black'
								}
							},
							defaultStyle: {
								// alignment: 'justify'
							}
						};
						pdfMake.createPdf(PDFDynamic).getDataUrl().then(function (value) {
							$('#pdfV').prop('src', value);
						});
						$('#modalPdfConstancia').modal('show');
					}
				},
				error: function error(response) { }
			});
			//url = portal + '/Personal/LegajoTabBienesConstanciaEntrega?CodPersonal='+g_codPersonal+'&CodBien=' + item;
			//window.open(url, '_top');
		}

	});

	//FORMACION
	$('#btnAddFormacion').on('click', function (e) {
		e.preventDefault();
		ClearInput('Fo');
		$('#tbodyFormacion tr').removeClass('selected');
	});

	$('#btnRemoveFormacion').on('click', function (e) {
		e.preventDefault();
		let tr = $('#tbodyFormacion tr.selected');
		if (tr.length != 1) {
			UIMensaje.REGISTRO();
			return;
		}
		EliminarFormacion(tr.data('item'));
	});

	$('#btnSaveFormacion').on('click', function (e) {
		e.preventDefault();

		if (validaDatosTabFormacion() == 1) {
			return;
		}

		let accion = '';
		let item = '';
		let tr = $('#tbodyFormacion tr.selected');
		if (tr.length == 0) {
			accion = 'N';
			item = 0;
		}
		else {
			accion = 'E';
			item = tr.data('item');
		}

		GrabarFormacion(accion, item);
	});

	//DOCS ESCANEADOS
	$('#btnAddDocEscan').on('click', function (e) {
		e.preventDefault();
		//$('#selTipoArchivo').prop('selectedIndex', 0);
		$('#txtDescripcion_Doc').val('');
		$('#fileDocEscan').val('');
		$("#addDocEscan").collapse('show'); //toggle
	});

	$('#btnRemoveDocEscan').on('click', function (e) {
		e.preventDefault();
		let tr = $('#tbodyDocsEscaneados tr.selected');
		if (tr.length != 1) {
			UIMensaje.REGISTRO();
			return;
		}
		EliminarDocEscan(tr);
		$("#addDocEscan").collapse('hide'); //toggle
	});

	$('#btnSaveDocEscan').on('click', function (e) {
		e.preventDefault();
		if (validaDatosTabDocuEsc() == 1) {
			return;
		}
		GuardarDocEscan();
		$("#addDocEscan").collapse('hide'); //toggle
	});

	$('#btnCancelDocEscan').on('click', function (e) {
		e.preventDefault();
		$("#addDocEscan").collapse('hide'); //toggle
	});



	//CONTROLES
	$('#btnAddControl').on('click', function (e) {
		e.preventDefault();
		$('#selControles').prop('selectedIndex', 0);
		//$('#selControles').removeProp('disabled');
		$('#selControles').removeAttr('disabled');
		$('#txtFechaEmision').val('');
		$('#txtFechaCaducidad').val('');
		$("#addControl").collapse('show'); //toggle
		$('#tbodyControl tr.selected').removeClass('selected');
		//$('#tbodyControl tr.selected').each(function (a) { $(this).removeClass('selected'); });
	});
	$('#btnRemoveControl').on('click', function (e) {
		e.preventDefault();
		let tr = $('#tbodyControl tr.selected');
		if (tr.length != 1) {
			UIMensaje.REGISTRO();
			return;
		}
		DeletePersonalcontrol(tr);
	});

	$('#btnSaveControl').on('click', function (e) {
		e.preventDefault();

		if (validaDatosTabControles() == 1) {
			return;
		}

		let tr = $('#tbodyControl tr.selected');
		if (tr.length != 1) {
			let ExisteControl = false;
			let SelValue = $('#selControles').val();
			$('#tbodyControl tr').each(function (e) {
				//if (($(this).find('td input[type=checkbox]')).prop('checked')) {
				let Row = $(this);
				if (SelValue == Row.data('codigo')) {
					ExisteControl = true;
					return false;
				}
			});
			if (ExisteControl) {
				MsgBox("error");
				return;
			}
		}
		PostPersonalcontrol();
	});

	$('#btnCancelControl').on('click', function (e) {
		e.preventDefault();
		$("#addControl").collapse('hide'); //toggle
	});

	//VARIACIÓN FORMACIÓN

	$('#btnAgregarCVARCLA').on('click', function () {
		Load.VariacionClasificadores.Add({ isNew: true });
		$('#addVariacionClasificadoresCVARCLA').collapse('show');
	});


	$('#chkBancoPago').on('change', function () {
		if ($('#chkBancoPago').prop('checked')) {
			$('#contBancoOrigenPago').removeClass('d-none');
			$('#lblBancoPago').text('Interbancario Pago:');
		} else {
			$('#contBancoOrigenPago').addClass('d-none');
			$('#lblBancoPago').text('Banco Pago:');
		}
	});

	$('#chkBancoCTS').on('change', function () {
		if ($('#chkBancoCTS').prop('checked')) {
			$('#contBancoOrigenCTS').removeClass('d-none');
			$('#lblBancoCTS').text('Interbancario CTS:');
		} else {
			$('#contBancoOrigenCTS').addClass('d-none');
			$('#lblBancoCTS').text('Banco CTS:');
		}
	});

	$('#chkAfiliacionEPS').on('change', function (e) {
		if ($('#txtEstado').data('cod') == 'A') {
			if ($('#chkAfiliacionEPS').prop('checked')) $('#selEstadoLaboral').val('10');
			else $('#selEstadoLaboral').val('11');
		} else {
			if ($('#chkAfiliacionEPS').prop('checked')) $('#selEstadoLaboral').val('12');
			else $('#selEstadoLaboral').val('13');
		}

		if (e.target.checked)
			$('#selAfiliacionEPS, #txtCodEPS, #txtFechaAfilEPS').attr('disabled', false);
		else
			$('#selAfiliacionEPS, #txtCodEPS, #txtFechaAfilEPS').attr('disabled', true);
	});

	$('#chkSctrPension').on('change', function (e) {
		if (e.target.checked) {
			$('#selSctrPension').attr('disabled', false);
			$('#selSctrPensionRiesgo').attr('disabled', false);
			$('#txtSctrDesde, #txtSctrHasta').attr('disabled', false);
		}
		else {
			$('#selSctrPensionRiesgo').val('');
			$('#selSctrPension').val('0');

			$('#selSctrPension').attr('disabled', true);
			$('#selSctrPensionRiesgo').attr('disabled', true);
			$('#txtSctrDesde, #txtSctrHasta').attr('disabled', true);
		}
	});
	$('#chkSctrSalud').on('change', function (e) {
		if (e.target.checked) {
			$('#selSctrSalud, #selSctrSaludRiesgo').attr('disabled', false);
		}
		else {
			$('#selSctrSaludRiesgo').val('');
			$('#selSctrSalud').val('0');

			$('#selSctrSalud, #selSctrSaludRiesgo').attr('disabled', true);
		}
	});

	$('#chkVidaLey').on('change', function (e) {
		if (e.target.checked)
			$('#selVidaLey, #txtCodVidaLey, #txtTasaVidaLey').attr('disabled', false);
		else
			$('#selVidaLey, #txtCodVidaLey, #txtTasaVidaLey').attr('disabled', true);
	});

	$('#chkMiembroComite').on('change', function (e) {
		if (e.target.checked)
			$('#txtDelAsig, #txtAlAsig').attr('disabled', false);
		else
			$('#txtDelAsig, #txtAlAsig').attr('disabled', true);
	});

	$('#chkAdelanto').on('change', function (e) {
		if (e.target.checked)
			$('#txtMontoAdelanto, #txtPorcentajeAdelanto').attr('disabled', false);
		else
			$('#txtMontoAdelanto, #txtPorcentajeAdelanto').attr('disabled', true);
	});

	//$('#lblUploadFilePersonal, #btnPlantillaPersonal').on('click', function () {
	//	UIMensaje.MANTENIMIENTO();
	//	return;
	//});

	$('#btnUploadFilePersonal').on('change', function (e) {
		e.preventDefault();
		/*
		var file = $("#btnUploadFilePersonal").get(0).files[0];
		var r = new FileReader();
		r.onload = function () {
			var binimage = r.result;
			$.ajax({
				type: "POST",
				contentType: "application/json; charset=utf-8",
				url: "/TempData["portal"]/Personal/SaveFilePersonal",
				data: { 'Based64BinaryString' : binimage },
				dataType: "json",
				async: false,
				success: function (data) {
					//Volver a recuperar
					if (data.Respuesta == 1) {
						GetListOk();
						GetListError();
						alert("Importación exitosa");
						console.log(data);
					}
					else {
						console.log(data);
					}
					//limpiar
					document.getElementById("btnUploadFilePersonal").value = "";
					gCancelaUpload = false;

				},
				error: function (result) {
					console.log(result);
					document.getElementById("btnUploadFilePersonal").value = "";
					gCancelaUpload = false;
				}
			});
		};
		r.readAsDataURL(file);
		*/
		var input = document.getElementById("btnUploadFilePersonal");
		var files = input.files;
		var formData = new FormData();

		formData.append("file", files[0]);

		$.ajax({
			url: portal + "/Personal/SaveFilePersonal",
			data: formData,
			processData: false,
			contentType: false,
			type: "POST",
			success: function (data) {
				if (data.Respuesta == 1) {
					GetListResponse(data.ok, '#tbodyOk');
					GetListResponse(data.error, '#tbodyError');
					GetColaboradores();
				}
				else {
					MsgBox("warning");
				}
				$("#btnUploadFilePersonal").val('');
			},
			error: function (result) {
				MsgBox("error");
				$("#btnUploadFilePersonal").val('');
			}
		});

	});

	$('#selTipoTrabajador, #selTipoTrab_new').on('change', function (e) {
		if (e.target.value == '27') {
			let id = $(this).data('control');
			let data = __Ajax('GET', portal + '/Personal/GetMontoCategoria', { codPersonal: g_codPersonal }, false);
			if (data.length < 1) {
				$(id).val(0)
				return;
			}
			$(id).val(data[0].bono1)
		}
	});

	$('#txtCodTrabajador').on('change', function (e) {
		if (e.target.value != '' && $('#txtNroDocIdent').val() != '') {
			var data = __Ajax('GET', portal + '/Personal/GetValidaCodigoTrabajador', { codTrabajador: e.target.value, numeroDoc: $('#txtNroDocIdent').val() }, false);
			if (data) {
				MsgBox("El código de trabajador ingresado ya existe. Registre otro.", "warning", false);
				$('#txtCodTrabajador').val('');
			}
		}
	});

	$('#txtMontoAdelanto').on('change', function (e) {
		if (e.target.value != '') {
			let montoAdelanto = e.target.value;
			if (montoAdelanto == 0) return;
			let montoBasico = $('#txtBasico').val();
			if (montoBasico <= montoAdelanto) {
				MsgBox("El importe del adelanto no debería ser mayor o igual al sueldo. ¿Seguro de continuar?", "question", false, true, null, function () {
					$('#txtMontoAdelanto').val('0');
				});
			}
		}
	});

	$('#txtPorcentajeAdelanto').on('change', function (e) {
		if (e.target.value != '') {
			let montoParcenAdelanto = e.target.value;
			if (g_porcenAvance == 0) return;
			if (montoParcenAdelanto > g_porcenAvance) {
				MsgBox("El porcentaje del adelanto no puede ser mayor que " + g_porcenAvance, "warning", true);
				$('#txtPorcentajeAdelanto').val('');
			}
		}
	});

	$('#txtFotocheck').on('change', function (e) {
		let fotocheck = e.target.value;
		let codTrabajador = $('#txtCodTrabajador').val();
		if (fotocheck.trim() == '') return;

		var data = __Ajax('GET', portal + '/Personal/GetValidaFotocheck', { fotocheck: e.target.value }, false);
		let row = data[0];
		if (row.existe > 0 && codTrabajador != row.cod_trabajador) {
			MsgBox("El fotocheck: " + fotocheck + ' ya ha sido asignado a ' + row.trabajador + ' ... Verifique', "warning", true);
			$('#txtFotocheck').val('');
		}
	});

	$('#txtBasico').on('change', function (e) {
		e.preventDefault();

		//if (monto == '') {
		//if (typeof monto !== 'undefined') {
		//if (typeof monto == "undefined" || monto == null) {
		//    $('#txtBasico').val(0);
		//}

		let monto = e.target.value;
		if (g_flgSueldoCero == 'N' && monto == 0) {
			MsgBox("No es posible asignar sueldo cero.", "warning", true);
			return;
		}

		let montoAdelanto = $('#txtMontoAdelanto').val();
		if (montoAdelanto == '' || montoAdelanto == 0) return;


		if ($('#txtBasico').val() <= montoAdelanto) {
			MsgBox("El monto del adelanto no puede ser mayor que el sueldo.", "warning", true);
			//$('#txtBasico').val('');
		}
	});

	$('#btnHistorialBancos').on('click', function (e) {
		e.preventDefault();
		var data = __Ajax('GET', portal + '/Personal/GetHistorialBancosPersonal', { codPersonal: g_codPersonal, tipoProceso: 'B' }, false);
		if (data.length <= 0) {
			$('#modalHistorialBancos').modal('hide');
			return;
		}

		var container = $("#tbodyHistorialBancos");
		container.empty();
		$.each(data, function (index, value) {
			var html = "<tr ondblclick=\"GetDetalleBanco(this)\" data-json='" + JSON.stringify(value) + "'>"
				+ "<td class=\"text-center\">" + value.banco + "</td>"
				+ "<td class=\"text-center\">" + value.moneda + "</td>"
				+ "<td class=\"text-center\">" + value.nombre_Tipo_Cuenta + "</td>"
				+ "<td class=\"text-center\">" + value.numero_cuenta + "</td>"
				+ "<td class=\"text-center\">" + value.flg_Interbancario + "</td>"
				+ "<td class=\"text-center\">" + value.banco_Origen + "</td>"
				+ "</tr>";
			container.append(html);
		});
		$('#modalHistorialBancos').modal({ show: true });

	});

	$('#txtNumero_Familiar').prop('disabled', true);
	GetColaboradores();
	GetHorariosUsuario('AD');

	$('#btnBuscar_new').on('click', async function (e) {
		e.preventDefault();
		var limpiar = false;

		if (ValidateTipoDocPersonal('selTipoDoc_consulta', 'txtNroDoc_consulta', 2) == 1) {
			return
		}

		//if (ValidaDatosVacios() == 'N') {
		//    limpiar = confirm('Existe Data Cargada, Desea Limpiar lo ingresado ?');
		//}

		var param = { codTipoEntidad: 'PE', codDocId: $('#selTipoDoc_consulta').val(), nroDocId: $('#txtNroDoc_consulta').val() }
		await UIAjax.invokeMethodAsync('Personal', 'GetValidaExisteTrabajador', param).then(function (data) {
			var obj = data[0];
			g_reingreso = obj?.flg_reingreso;
			if (obj.rpta < 0) {
				MsgBox(obj.mensaje, "warning", false);
				return;
			}
			else if (obj.rpta == 0) {
				MsgBox(obj.mensaje, "warning", false);
				$('#selDepartamentoNac_new').val(obj.depa_per);
				GetProvincias('selProvinciaNac_new', obj.depa_per);
				$('#selProvinciaNac_new').val(obj.prov_per);
				GetDistritos('selDistritoNac_new', obj.prov_per);
				$('#txtCodTrab_new').val(obj.cod_trabajador);

				$('#selDepartamento_new').val(obj.depa_gen);
				GetProvincias('selProvincia_new', obj.depa_gen);
				$('#selProvincia_new').val(obj.prov_gen);
				GetDistritos('selDistrito_new', obj.prov_gen);

				switch (obj.sexo) {
					case "1":
						$('#rb_Masculino_new').prop('checked', true);
						break;
					case "2":
						$('#rb_Femenino_new').prop('checked', true);
						break;
				}
				$('#txtNumDoc_new').val(param.nroDocId);
				$('#selEstadoCivil_new').val(obj.cod_estado_civil);
				$('#txtNombres_new').val(obj.nombres);
				$('#txtApellidoM_new').val(obj.apellido_m);
				$('#txtApellidoP_new').val(obj.apellido_p);
				$('#txtFechaNac_new').datepicker('setDate', obj.fecha_nacimiento);
				$('#selPais_new').val(obj.nacion);
				$('#selAFP_new').val(obj.cod_afp);
				$('#txtCUSPP_new').val(obj.cuispp);
				switch (obj.tipo_comision_afp) {
					case "1":
						$('#rbComisionRemu_new').prop('checked', true);
						break;
					case "2":
						$('#rbComisionMixta_new').prop('checked', true);
						break;
				}
			}
			else {
				if (obj.rpta == 1) {
					MsgBox(obj.mensaje + "\nDesea Agregar el  numero de Documento", "question", false, false, function () {
						$('#txtNumDoc_new').val($('#txtNroDoc_consulta').val());
						let nro_doc = $('#txtNumDoc_new').val();

						$('#selTipoDoc_new').val($('#selTipoDoc_consulta').val());
						$('#txtCodTrab_new').val(nro_doc);
					}, function () {
						$('#txtNroDoc_consulta').focus();
					});
				} else {
					MsgBox(obj.mensaje, "warning", false);
				}
				if (limpiar) {
					//limpiar controles
					$('#selDepartamentoNac_new').val('14');
					GetProvincias('selProvinciaNac_new', '14');
					$('#selProvinciaNac_new').val('1401');
					GetDistritos('selDistritoNac_new', '1401');

					$('#selDepartamento_new').val('14');
					GetProvincias('selProvincia_new', '14');
					$('#selProvincia_new').val('1401');
					GetDistritos('selDistrito_new', '1401');

					$("#tbPer input[type=text], #tbLab input[type=text]").val('');
				}
			}
		});
		//var data = __Ajax('GET', portal + 'Personal/GetValidaExisteTrabajador', param, false);		
	});

	$('#btnCancel_new').on('click', function (e) {
		e.preventDefault();

		$('#modalNuevoPersonal').modal('hide');
	});

	$('#btnSave_new').on('click', function (e) {
		e.preventDefault();
		if (validaDatos() == 1) {
			return;
		}
		GrabarPersonal();
	});

	$('#txtNumDoc_new').on('change', function (e) {
		let nro_doc = $('#txtNumDoc_new').val();
		$('#txtCodTrab_new').val(nro_doc); 1
	});

	$('#txtRuc_new').on('change', function (e) {
		if ($('#txtNumDoc_new').val() == "") {
			let ruc = $('#txtRuc_new').val();
			$('#txtCodTrab_new').val(ruc);
		}
	});

	$('#rbComisionRemu_new').on('change', function (e) {
		if (e.target.checked) {
			var fechaAfil = convertDateFormat2($('#txtFechaAfil_new').val());
			var fecha = new Date(fechaAfil);

			if (validarFechaAfiliacion(fecha)) {
				MsgBox('Todos los afiliados a partir del 01/02/2013 están afectos a la Comisión Mixta. Cambie la fecha de afiliación para seleccionar la comisión afecta.', "warning", false);
				$('#rbComisionMixta_new').prop('checked', true);
				return;
			}
		}
	});

	$('#txtFechaAfil_new').on('change', function (e) {
		var fecha = $('#txtFechaAfil_new').val();
		if (!regExpFecha.test(fecha)) {
			return;
		}
		var fechaAfil = convertDateFormat2(fecha);
		var fecha = new Date(fechaAfil);
		if (validarFechaAfiliacion(fecha) && $('#rbComisionRemu_new').prop('checked')) {
			MsgBox('Todos los afiliados a partir del 01/02/2013 están afectos a la Comisión Mixta. Cambie la fecha de afiliación para seleccionar la comisión afecta.', "warning", false);
			$('#rbComisionMixta_new').prop('checked', true);
			return;
		}
	});

	$('#selClase_new').on('change', function (e) {
		e.preventDefault();
		let select = $('#selClase_new').val();
		if (select == "C" || select == "R")
			$('#txtFinContrato, #txtInicioPeriodoPrueba, #txtFinPeriodoPrueba').attr('disabled', false);
		else
			$('#txtFinContrato, #txtInicioPeriodoPrueba, #txtFinPeriodoPrueba').attr('disabled', true);

		if (select == "P")
			//$('#txtFinContrato').attr('disabled', true);
			$('#txtFinContrato, #txtInicioPeriodoPrueba, #txtFinPeriodoPrueba').val('');

	});

	$('#chkAfilEPS_new').on('change', function (e) {
		if (!e.target.checked) {
			$('#selAfilEPS_new').val('-1');
		}
		$('#selAfilEPS_new').attr('disabled', !e.target.checked);
	});
	$('#chkSCTRPension_new').on('change', function (e) {
		if (!e.target.checked) {
			$('#selSCTRPension_new').val('0');
		}
		$('#selSCTRPension_new').attr('disabled', !e.target.checked);
	});
	$('#chkSCTRSalud_new').on('change', function (e) {
		if (!e.target.checked) {
			$('#selSCTRSalud_new').val('0');
		}
		$('#selSCTRSalud_new').attr('disabled', !e.target.checked);
	});

	$('#chkVidaLey_new').on('change', function (e) {
		$('#selSeguro_new, #txtTasa_new').attr('disabled', !e.target.checked);
	});

	$('#chkAdelanto_new').on('change', function (e) {
		$('#txtMontoAdelanto_new, #txtPorcenAdelanto_new').attr('disabled', !e.target.checked);
	});

	$('#chkPagoInter_new').on('change', function (e) {
		if (e.target.checked)
			$('#trBancoOrigenPago').removeClass('d-none');
		else
			$('#trBancoOrigenPago').addClass('d-none');
	});

	$('#chkCTSInter_new').on('change', function (e) {
		if (e.target.checked)
			$('#trBancoOrigenCTS').removeClass('d-none');
		else
			$('#trBancoOrigenCTS').addClass('d-none');
	});

	$(document).on('click', '#tableMeritosDemeritos tbody tr', function (e) {

		$('#tableMeritosDemeritos tbody tr').removeClass('selected');

		var ths = $(this);

		ths.addClass('selected');

		var jsonRow = tableMeritosDemeritos.row(ths).data();

		if (!isEmpty(jsonRow)) {
			$('#file').val(null);
			g_accion_merito = 'E';
			g_item_merito = jsonRow.item;
			$('#txtItem_MeritosDemeritos').val(jsonRow.item);
			$('#selTipo_MeritosDemeritos').val(jsonRow.tipo);
			$('#txtFecha_MeritosDemeritos').val(jsonRow.fecha);
			$('#selReportado_MeritosDemeritos').val(jsonRow.cod_entidad);
			$('#txtDescripcion_MeritosDemeritos').val(jsonRow.descripcion);
			$('#hdExisteArchivo_MeritosDemeritos').val(jsonRow.existeArchivo == "S" ? "Contiene archivo" : "Sin archivo");
			if (jsonRow.existeArchivo == "S") {
				$('#lblFileMeritosFaltas').text('Actualizar');
			}
			else {
				$('#lblFileMeritosFaltas').text('Archivo');
			}

		}

	});

	$('body').on('click', '#tableMeritosDemeritos tbody tr td i', function (e) {
		e.stopImmediatePropagation();
		//e.stopPropagation();
		//modalVerDocumento

		var ths = $(this);

		var Rows = ths.closest('tr');

		$('#tableMeritosDemeritos tbody tr').removeClass('selected');

		Rows.addClass('selected');

		var jsonRow = tableMeritosDemeritos.row(Rows).data();

		//console.log(jsonRow);

		//return;

		var jsonParam = {
			CodigoDePersonal: g_codPersonal,
			Filtro: jsonRow.item,
			Tipo: jsonRow.flg_tipo_archivo
		};
		var html;
		var url = portal + "/ajax/personal/GetPersonalMeritosDemeritosdocumento/?" + $.param(jsonParam);
		var container = $('#divVerDocumento');
		//var modaldialog = '#modaldialogVerDocumento';
		container.empty();

		if (jsonParam.Tipo == 1) {
			html = '<img class="w-100" src="' + url + '" >';
		} else if (jsonParam.Tipo == 2) {
			html = '<div class="text-center">' + '<a href="' + url + '">Descargar el documento</a>' + '</div>';
		}

		container.append(html);
		$('#modalVerDocumento').modal('show');
	});

	$('#selVinculoFamiliar').on('change', function (e) {
		GetVinculoSustentoFamiliar('selVinculoSustentoFam', $(this).val());
	});

	$('#selTipoInst_Formacion').on('change', function (e) {

		FormacionReloadInstitucionEducativa($('input[name="gpRegimen"]:checked').val() + $(this).val());
		//container.trigger('change');

	});

	$('#selInstEducativa_Formacion').on('change', function (e) {
		var val = $(this).val();
		if (!isEmpty(val)) {
			GetCarreras('selCarrera_Formacion', val);
		}
		else {
			$('#selCarrera_Formacion').empty();
		}
	});
	$('input[name="gpRegimen"]').on('change', function (e) {

		FormacionReloadInstitucionEducativa($(this).val() + $('#selTipoInst_Formacion').val());
	});

	$('#btnExportarXLSXHistorial').on('click', function (e) {
		e.preventDefault();
		UISave.URL(portal + '/Personal/DownloadXLSXHistorial/?cod_entidad=' + g_codEntidad);
	});

	$('#btnItemFotochekPersonal').on('click', async function () {
		var jsonFiltro = Load.Filtro();
		if (UIEmpty(jsonFiltro.jsonRow)) {
			UIMensaje.COLABORADOR();
			return;
		}
		var jsonOptions = {
			TituloDeModal: 'Reporte de Fotocheks de Personal',
			modalSize: 'modal-md',
		};

		var AHTML = new StringBuilder();
		AHTML.append(`<div class="row mt-3">`);
		AHTML.append(`	<div class="col">`);
		AHTML.append(`		<label>Formato</label>`);
		AHTML.append(`		<select id="selectFormatoFotochekPersonal" class="form-control form-control-sm">`);
		AHTML.append(`		<option value="F1">Formato 1</option>`);
		AHTML.append(`		<option value="F2">Formato 2</option>`);
		AHTML.append(`		<option value="F3">Formato 3</option>`);
		AHTML.append(`		</select>`);
		AHTML.append(`	</div>`);
		AHTML.append(`</div>`);
		await UIModal.Dynamic(jsonOptions).then(function (r) {
			r.jBody.html(AHTML.toString());
			r.jAceptar.on('click', function () {
				console.log('das');
				var jsonParam = {
					Formato: $('#selectFormatoFotochekPersonal').val(),
					CodigoDePersonal: jsonFiltro.jsonRow.personal,
					CodigoDeTipoPlanilla: jsonFiltro.jsonRow.planilla,
					NombreDeReporte: null,
				};
				switch (jsonParam.Formato) {
					case 'F1':
						jsonParam.NombreDeReporte = 'RptFotocheckPersonalF1';
						break;
					case 'F2':
						jsonParam.NombreDeReporte = 'RptFotocheckPersonalF2';
						break;
					case 'F3':
						jsonParam.NombreDeReporte = 'RptFotocheckPersonalF3';
						break;
				}
				UIBoldReport.Load(jsonParam);
			});
			r.jCancelar.on('click', function () {
				r.jModal.modal('hide');
			});
			r.jModal.modal('show');
		});
	});

	$('#btnItemConfiguracionRegistroDefecto').on('click', async function () {
		//UIMensaje.DESARROLLO();
		//return;
		$('#selectTipoPlanillaMCOREDE option[value="-1"]').remove();
		var ___load = {
			Filtro: function () {
				return {
					CodigoDeTipoPlanilla: $('#selectTipoPlanillaMCOREDE').val(),
					CodigoDeProyecto: $('#selectProyectoMCOREDE').val(),
					CodigoDeCC: $('#selectCentrosCostoMCOREDE').val(),
					CodigoDeUN: $('#selectUnidadesNegocioMCOREDE').val(),
					CodigoDeArea: $('#selectAreaMCOREDE').val(),
					CodigoDeLocalidad: $('#selectLocalidadMCOREDE').val(),
				};
			},
			DELEGATE: async function () {
				var tabSelected = $('#ulTabPrincipalMCOREDE a.active');
				var action = tabSelected.attr('data-action');
				if (!UIEmpty(action)) {
					await this[action](this.Filtro());
				}
			},
			DatosPersonales: async function (jsonFiltro) {
				await UILoadingDefault('show');
				try {
					await UIAjax.invokeMethodAsync('ajax/personal', 'GetLegajoConfiguracionDefaultDatosPersonalesByFiltro', jsonFiltro).then(function (r) {
						var jsonResult = {};
						if (r.length > 0) {
							jsonResult = r[0];
						}
						UICheckbox.setValue('#chkPaisDPMCOREDE', jsonResult.flg_nacion_req);
						$('#selectPaisDPMCOREDE').val(jsonResult.nacion);
						UICheckbox.setValue('#chkEstadoCivilDPMCOREDE', jsonResult.flg_cod_estcivil_req);
						$('#selectEstadoCivilDPMCOREDE').val(jsonResult.cod_estado_civil);
						UICheckbox.setValue('#chkTipoDocIdentidadDPMCOREDE', jsonResult.flg_cod_docid_req);
						$('#selectTipoDocIdentidadDPMCOREDE').val(jsonResult.cod_docid);
						UICheckbox.setValue('#chkCodigoAFPDPMCOREDE', jsonResult.flg_cod_afp_req);
						$('#selectCodigoAFPDPMCOREDE').val(jsonResult.cod_afp);
						UICheckbox.setValue('#chkTipoComisionAFPDPMCOREDE', jsonResult.flg_tipo_com_afp_req);
						$('#selectTipoComisionAFPDPMCOREDE').val(jsonResult.tipo_comision_afp);
						UICheckbox.setValue('#chkSexoDPMCOREDE', jsonResult.flg_sexo_req);
						$('#selectSexoDPMCOREDE').val(jsonResult.sexo);
						$('#btnGuardarDPMCOREDE').off('click').on('click', async function (e) {
							var jsonParam = {
								RegistroPersonalDatosDflt: [],
							};
							$.extend(true, jsonParam, ___load.Filtro());
							jsonParam.RegistroPersonalDatosDflt.push({
								nacion: $('#selectPaisDPMCOREDE').val(),
								flg_nacion_req: UICheckbox.getValue('#chkPaisDPMCOREDE'),
								cod_estado_civil: $('#selectEstadoCivilDPMCOREDE').val(),
								flg_cod_estcivil_req: UICheckbox.getValue('#chkEstadoCivilDPMCOREDE'),
								cod_docid: $('#selectTipoDocIdentidadDPMCOREDE').val(),
								flg_cod_docid_req: UICheckbox.getValue('#chkTipoDocIdentidadDPMCOREDE'),
								cod_afp: $('#selectCodigoAFPDPMCOREDE').val(),
								flg_cod_afp_req: UICheckbox.getValue('#chkCodigoAFPDPMCOREDE'),
								tipo_comision_afp: $('#selectTipoComisionAFPDPMCOREDE').val(),
								flg_tipo_com_afp_req: UICheckbox.getValue('#chkTipoComisionAFPDPMCOREDE'),
								sexo: $('#selectSexoDPMCOREDE').val(),
								flg_sexo_req: UICheckbox.getValue('#chkSexoDPMCOREDE'),
							});
							await UILoadingDefault('show');
							await UIAjax.runFunctionAsync('ajax/personal', 'PostLegajoConfiguracionDefaultDatosPersonales', jsonParam);
							UILoadingDefault('hide');
						});
					});
				} catch (e) {
					console.log(e);
				}
				finally {
					UILoadingDefault('hide');
				}
			},
			DatosGenerales: async function (jsonFiltro) {
				await UILoadingDefault('show');
				try {
					await UIAjax.invokeMethodAsync('ajax/personal', 'GetLegajoConfiguracionDefaultDatosGeneralesByFiltro', jsonFiltro).then(function (r) {
						$('#txtBusquedaDGMCOREDE').off('keyup').on('keyup', function () {
							let valor = this.value;
							let cols = $('#aDGMCOREDE div.search-col-datos-generales');
							if (!UIEmpty(valor)) {
								valor = valor.toLowerCase();
								cols.filter(function (i, v) {
									let valor_label = $('label[for^="chk"]', v).text().toLowerCase();
									if (valor_label.indexOf(valor) !== -1) {
										$(v).removeClass('d-none');
										return true;
									}
									else {
										$(v).addClass('d-none');
										return false;
									}
								});
							}
							else {
								cols.removeClass('d-none');
							}
						});
						UIAutoNumeric.Init('#txtMontoDGMCOREDE, #txtMontoAdelantoDGMCOREDE', { vMax: '999999.99' });
						UIAutoNumeric.Init('#txtTasaVidaLeyDGMCOREDE', { vMax: '999.99' });
						UIAutoNumeric.Init('#txtPorcentajeAdelantoDGMCOREDE', { vMax: '99.99' });
						var jsonResult = {};
						if (r.length > 0) {
							jsonResult = r[0];
						}
						UICheckbox.setValue('#chkTipoContratoDGMCOREDE', jsonResult.flg_cod_tipo_contrato_req);
						$('#selectTipoContratoDGMCOREDE').val(jsonResult.cod_tipo_contrato);
						UICheckbox.setValue('#chkPuestoDGMCOREDE', jsonResult.flg_cod_puesto_req);
						$('#selectPuestoDGMCOREDE').val(jsonResult.cod_puesto);
						UICheckbox.setValue('#chkCategoriaDGMCOREDE', jsonResult.flg_cod_cate_req);
						$('#selectCategoriaDGMCOREDE').val(jsonResult.cod_categoria);
						UICheckbox.setValue('#chkCargoDGMCOREDE', jsonResult.flg_cod_cargo_req);
						$('#selectCargoDGMCOREDE').val(jsonResult.cod_cargo);
						UICheckbox.setValue('#chkMotivoIngresoDGMCOREDE', jsonResult.flg_cod_motivo_ing_req);
						$('#selectMotivoIngresoDGMCOREDE').val(jsonResult.cod_motivo_ingreso);
						UICheckbox.setValue('#chkTipoTrabajadorDGMCOREDE', jsonResult.flg_cod_tipo_trabaj_req);
						$('#selectTipoTrabajadorDGMCOREDE').val(jsonResult.cod_tipo_trabajador);
						UICheckbox.setValue('#chkCondicionLaboralDGMCOREDE', jsonResult.flg_tipo_trabajador_req);
						$('#selectCondicionLaboralDGMCOREDE').val(jsonResult.tipo_trabajador);
						UICheckbox.setValue('#chkAlcanceCargoDGMCOREDE', jsonResult.flg_cod_alcance_req);
						$('#selectAlcanceCargoDGMCOREDE').val(jsonResult.cod_alcance);
						UICheckbox.setValue('#chkAfiliacionEPSDGMCOREDE', jsonResult.flg_afiliacion_eps_req);
						$('#selectAfiliacionEPSDGMCOREDE').val(jsonResult.afiliacion_eps);
						UICheckbox.setValue('#chkEPSDGMCOREDE', jsonResult.flg_cod_eps_req);
						$('#selectEPSDGMCOREDE').val(jsonResult.cod_eps);
						UICheckbox.setValue('#chkSCTRPensionDGMCOREDE', jsonResult.flg_sctr_pension_req);
						$('#selectSCTRPensionDGMCOREDE').val(jsonResult.sctr_pension);
						UICheckbox.setValue('#chkSCTRSaludDGMCOREDE', jsonResult.flg_sctr_salud_req);
						$('#selectSCTRSaludDGMCOREDE').val(jsonResult.sctr_salud);
						UICheckbox.setValue('#chkTipoMontoPeriodoDGMCOREDE', jsonResult.flg_tipo_monto_per_req);
						$('#selectTipoMontoPeriodoDGMCOREDE').val(jsonResult.tipo_monto_periodo);
						UICheckbox.setValue('#chkAsignacionFamiliarDGMCOREDE', jsonResult.flg_asig_fam_req);
						$('#selectAsignacionFamiliarDGMCOREDE').val(jsonResult.flg_asignacion_familiar);
						UICheckbox.setValue('#chkEsSaludVidaDGMCOREDE', jsonResult.flg_essalud_vida_req);
						$('#selectEsSaludVidaDGMCOREDE').val(jsonResult.essalud_vida);
						UICheckbox.setValue('#chkMonedaSueldoDGMCOREDE', jsonResult.flg_cod_moneda_basico_req);
						$('#selectMonedaSueldoDGMCOREDE').val(jsonResult.cod_moneda_basico);
						UICheckbox.setValue('#chkMontoDGMCOREDE', jsonResult.flg_monto_req);
						UIAutoNumeric.setValue('#txtMontoDGMCOREDE', jsonResult.monto);
						UICheckbox.setValue('#chkPagoCuentaDGMCOREDE', jsonResult.flg_pago_cuenta_req);
						$('#selectPagoCuentaDGMCOREDE').val(jsonResult.flg_pago_cuenta);
						UICheckbox.setValue('#chkBoletaReciboDGMCOREDE', jsonResult.flg_boleta_reciboh_req);
						$('#selectBoletaReciboDGMCOREDE').val(jsonResult.flg_boleta_reciboh);
						UICheckbox.setValue('#chkBancoPagoDGMCOREDE', jsonResult.flg_cod_banco_pago_req);
						$('#selectBancoPagoDGMCOREDE').val(jsonResult.cod_banco_pago);
						UICheckbox.setValue('#chkMonedaPagoDGMCOREDE', jsonResult.flg_cod_moneda_pago_req);
						$('#selectMonedaPagoDGMCOREDE').val(jsonResult.cod_moneda_pago);
						UICheckbox.setValue('#chkNumeroCuentaPagoDGMCOREDE', jsonResult.flg_num_cuenta_pago_req);
						$('#txtNumeroCuentaPagoDGMCOREDE').val(jsonResult.num_cuenta_pago);
						UICheckbox.setValue('#chkBancoCTSDGMCOREDE', jsonResult.flg_cod_banco_cts_req);
						$('#selectBancoCTSDGMCOREDE').val(jsonResult.cod_banco_cts);
						UICheckbox.setValue('#chkMonedaCTSDGMCOREDE', jsonResult.flg_cod_moneda_cts_req);
						$('#selectMonedaCTSDGMCOREDE').val(jsonResult.cod_moneda_cts);
						UICheckbox.setValue('#chkNumeroCuentaCTSDGMCOREDE', jsonResult.flg_num_cuenta_cts_req);
						$('#txtNumeroCuentaCTSDGMCOREDE').val(jsonResult.num_cuenta_cts);
						UICheckbox.setValue('#chkAdelantoDGMCOREDE', jsonResult.flg_adelanto_req);
						$('#selectAdelantoDGMCOREDE').val(jsonResult.flg_adelanto);
						UICheckbox.setValue('#chkMontoAdelantoDGMCOREDE', jsonResult.flg_monto_adelanto_req);
						UIAutoNumeric.setValue('#txtMontoAdelantoDGMCOREDE', jsonResult.monto_adelanto);
						UICheckbox.setValue('#chkPorcentajeAdelantoDGMCOREDE', jsonResult.flg_porcen_adelanto_req);
						UIAutoNumeric.setValue('#txtPorcentajeAdelantoDGMCOREDE', jsonResult.porcen_adelanto);
						UICheckbox.setValue('#chkVidaLeyDGMCOREDE', jsonResult.flg_vida_ley_req);
						$('#selectVidaLeyDGMCOREDE').val(jsonResult.flg_vida_ley);
						UICheckbox.setValue('#chkCodigoVidaLeyDGMCOREDE', jsonResult.flg_codigo_vida_ley_req);
						$('#selectCodigoVidaLeyDGMCOREDE').val(jsonResult.codigo_vida_ley);
						UICheckbox.setValue('#chkTasaVidaLeyDGMCOREDE', jsonResult.flg_tasa_vida_ley_req);
						UIAutoNumeric.setValue('#txtTasaVidaLeyDGMCOREDE', jsonResult.tasa_vida_ley);
						UICheckbox.setValue('#chkHorarioDGMCOREDE', jsonResult.flg_horario_req);
						$('#selectHorarioDGMCOREDE').val(jsonResult.horario);
						UICheckbox.setValue('#chkSenatiDGMCOREDE', jsonResult.flg_senati_req);
						$('#selectSenatiDGMCOREDE').val(jsonResult.flg_senati);
						$('#btnGuardarDGMCOREDE').off('click').on('click', async function (e) {
							var jsonParam = {
								RegistroPersonalDatosDflt: [],
							};
							$.extend(true, jsonParam, ___load.Filtro());
							jsonParam.RegistroPersonalDatosDflt.push({
								cod_tipo_contrato: $('#selectTipoContratoDGMCOREDE').val(),
								flg_cod_tipo_contrato_req: UICheckbox.getValue('#chkTipoContratoDGMCOREDE'),
								cod_puesto: $('#selectPuestoDGMCOREDE').val(),
								flg_cod_puesto_req: UICheckbox.getValue('#chkPuestoDGMCOREDE'),
								cod_categoria: $('#selectCategoriaDGMCOREDE').val(),
								flg_cod_cate_req: UICheckbox.getValue('#chkCategoriaDGMCOREDE'),
								cod_cargo: $('#selectCargoDGMCOREDE').val(),
								flg_cod_cargo_req: UICheckbox.getValue('#chkCargoDGMCOREDE'),
								cod_motivo_ingreso: $('#selectMotivoIngresoDGMCOREDE').val(),
								flg_cod_motivo_ing_req: UICheckbox.getValue('#chkMotivoIngresoDGMCOREDE'),
								cod_tipo_trabajador: $('#selectTipoTrabajadorDGMCOREDE').val(),
								flg_cod_tipo_trabaj_req: UICheckbox.getValue('#chkTipoTrabajadorDGMCOREDE'),
								tipo_trabajador: $('#selectCondicionLaboralDGMCOREDE').val(),
								flg_tipo_trabajador_req: UICheckbox.getValue('#chkCondicionLaboralDGMCOREDE'),
								cod_alcance: $('#selectAlcanceCargoDGMCOREDE').val(),
								flg_cod_alcance_req: UICheckbox.getValue('#chkAlcanceCargoDGMCOREDE'),
								afiliacion_eps: $('#selectAfiliacionEPSDGMCOREDE').val(),
								flg_afiliacion_eps_req: UICheckbox.getValue('#chkAfiliacionEPSDGMCOREDE'),
								cod_eps: $('#selectEPSDGMCOREDE').val(),
								flg_cod_eps_req: UICheckbox.getValue('#chkEPSDGMCOREDE'),
								sctr_pension: $('#selectSCTRPensionDGMCOREDE').val(),
								flg_sctr_pension_req: UICheckbox.getValue('#chkSCTRPensionDGMCOREDE'),
								sctr_salud: $('#selectSCTRSaludDGMCOREDE').val(),
								flg_sctr_salud_req: UICheckbox.getValue('#chkSCTRSaludDGMCOREDE'),
								tipo_monto_periodo: $('#selectTipoMontoPeriodoDGMCOREDE').val(),
								flg_tipo_monto_per_req: UICheckbox.getValue('#chkTipoMontoPeriodoDGMCOREDE'),
								flg_asignacion_familiar: $('#selectAsignacionFamiliarDGMCOREDE').val(),
								flg_asig_fam_req: UICheckbox.getValue('#chkAsignacionFamiliarDGMCOREDE'),
								essalud_vida: $('#selectEsSaludVidaDGMCOREDE').val(),
								flg_essalud_vida_req: UICheckbox.getValue('#chkEsSaludVidaDGMCOREDE'),
								cod_moneda_basico: $('#selectMonedaSueldoDGMCOREDE').val(),
								flg_cod_moneda_basico_req: UICheckbox.getValue('#chkMonedaSueldoDGMCOREDE'),
								monto: $('#txtMontoDGMCOREDE').val(),
								flg_monto_req: UICheckbox.getValue('#chkMontoDGMCOREDE'),
								flg_pago_cuenta: $('#selectPagoCuentaDGMCOREDE').val(),
								flg_pago_cuenta_req: UICheckbox.getValue('#chkPagoCuentaDGMCOREDE'),
								flg_boleta_reciboh: $('#selectBoletaReciboDGMCOREDE').val(),
								flg_boleta_reciboh_req: UICheckbox.getValue('#chkBoletaReciboDGMCOREDE'),
								cod_banco_pago: $('#selectBancoPagoDGMCOREDE').val(),
								flg_cod_banco_pago_req: UICheckbox.getValue('#chkBancoPagoDGMCOREDE'),
								cod_moneda_pago: $('#selectMonedaPagoDGMCOREDE').val(),
								flg_cod_moneda_pago_req: UICheckbox.getValue('#chkMonedaPagoDGMCOREDE'),
								num_cuenta_pago: $('#txtNumeroCuentaPagoDGMCOREDE').val(),
								flg_num_cuenta_pago_req: UICheckbox.getValue('#chkNumeroCuentaPagoDGMCOREDE'),
								cod_banco_cts: $('#selectBancoCTSDGMCOREDE').val(),
								flg_cod_banco_cts_req: UICheckbox.getValue('#chkBancoCTSDGMCOREDE'),
								cod_moneda_cts: $('#selectMonedaCTSDGMCOREDE').val(),
								flg_cod_moneda_cts_req: UICheckbox.getValue('#chkMonedaCTSDGMCOREDE'),
								num_cuenta_cts: $('#txtNumeroCuentaCTSDGMCOREDE').val(),
								flg_num_cuenta_cts_req: UICheckbox.getValue('#chkNumeroCuentaCTSDGMCOREDE'),
								flg_adelanto: $('#selectAdelantoDGMCOREDE').val(),
								flg_adelanto_req: UICheckbox.getValue('#chkAdelantoDGMCOREDE'),
								monto_adelanto: $('#txtMontoAdelantoDGMCOREDE').val(),
								flg_monto_adelanto_req: UICheckbox.getValue('#chkMontoAdelantoDGMCOREDE'),
								porcen_adelanto: $('#txtPorcentajeAdelantoDGMCOREDE').val(),
								flg_porcen_adelanto_req: UICheckbox.getValue('#chkPorcentajeAdelantoDGMCOREDE'),
								flg_vida_ley: $('#selectVidaLeyDGMCOREDE').val(),
								flg_vida_ley_req: UICheckbox.getValue('#chkVidaLeyDGMCOREDE'),
								codigo_vida_ley: $('#selectCodigoVidaLeyDGMCOREDE').val(),
								flg_codigo_vida_ley_req: UICheckbox.getValue('#chkCodigoVidaLeyDGMCOREDE'),
								tasa_vida_ley: $('#txtTasaVidaLeyDGMCOREDE').val(),
								flg_tasa_vida_ley_req: UICheckbox.getValue('#chkTasaVidaLeyDGMCOREDE'),
								horario: $('#selectHorarioDGMCOREDE').val(),
								flg_horario_req: UICheckbox.getValue('#chkHorarioDGMCOREDE'),
								flg_senati: $('#selectSenatiDGMCOREDE').val(),
								flg_senati_req: UICheckbox.getValue('#chkSenatiDGMCOREDE'),
							});
							await UILoadingDefault('show');
							await UIAjax.runFunctionAsync('ajax/personal', 'PostLegajoConfiguracionDefaultDatosGenerales', jsonParam);
							UILoadingDefault('hide');
						});
					});
				} catch (e) {
					console.log(e);
				}
				finally {
					UILoadingDefault('hide');
				}
			},
			CargoSueldo: async function (jsonFiltro) {
				await UILoadingDefault('show');
				await UIAjax.invokeMethodAsync('ajax/personal', 'GetAllRegistroPersonalDatosDfltCargosByFiltro', jsonFiltro).then(async function (r) {
					var tableCargoSueldoCSMCOREDE = $('#tableCargoSueldoCSMCOREDE').DataTable({
						data: r,
						initComplete: function (settings, json) {
							//var api = this.api();
							//$('.filterhead', api.table().header()).each(function () {
							//	var title = $(this).text();
							//	console.log(title);
							//	$(this).html('<input type="text" placeholder="Buscar ' + title + '" class="form-control column_search" />');
							//});
							//txtMontoTCSCSMCOREDE
							UIAutoNumeric.Init('input.txtMontoTCSCSMCOREDE', { vMax: '99999.99' });
							$('input.txtMontoTCSCSMCOREDE').off('change').on('change', async function (e) {
								var ths = $(this);
								var tr = ths.closest('tr');
								//tableCargoSueldoCSMCOREDE.row(tr).data().monto = this.value;
								var __jsonRow = tableCargoSueldoCSMCOREDE.row(tr).data();
								var jsonParam = {
									CodigoDeCargo: __jsonRow.cod_cargo,
									Monto: UIEmpty(this.value, '0'),
									ShowToast: true,
								};
								$.extend(true, jsonParam, ___load.Filtro());
								await UIAjax.runFunctionAsync('ajax/personal', 'PostRegistroPersonalDatosDfltCargos', jsonParam);
							});
							UILoadingDefault('hide');
							//$('#btnGuardarCSMCOREDE').off('click').on('click', async function (e) {
							//	var jsonParam = {
							//		//RegistroPersonalDatosDfltCargos: [],
							//		CodigoDeCargo: '',
							//		Monto: '',
							//	};
							//	$.extend(true, jsonParam, ___load.Filtro());								
							//	$.each(tableCargoSueldoCSMCOREDE.rows().data(), function (i, v) {
							//		jsonParam.RegistroPersonalDatosDfltCargos.push({
							//			cod_cargo: v.cod_cargo,
							//			monto: v.monto
							//		});
							//	});
							//	if (jsonParam.RegistroPersonalDatosDfltCargos.length > 0) {
							//		await UILoadingDefault('show');
							//		await UIAjax.runFunctionAsync('ajax/personal', 'PostRegistroPersonalDatosDfltCargosMasivo', jsonParam);
							//		UILoadingDefault('hide');
							//	}
							//	else {
							//		MsgBox("No hay registros para guardar.", "info", false);
							//	}
							//});
						},
						info: false,
						paging: false,
						scrollY: "300px",
						scrollX: true,
						scrollCollapse: true,
						columns: [
							{ data: null, defaultContent: '', className: 'text-center' },
							{ data: 'nombre_cargo' },
							{
								data: 'monto', render: function (data, type, row, meta) {
									return `<input type="text" class="form-control text-right txtMontoTCSCSMCOREDE" value="${data}" />`;
								}, className: 'text-right w-160px'
							},
						]
					});
					//UIDataTable.CreateEnumerator(tableCargoSueldoCSMCOREDE, 0, '{__}');
					UIDataTable.CreateEnumerator(tableCargoSueldoCSMCOREDE, 0, "<b>{__}</b>");
					//$('#tableCargoSueldoCSMCOREDE thead').off('keyup').on('keyup', ".column_search", function () {
					//	tableCargoSueldoCSMCOREDE.column($(this).parent().index()).search(this.value).draw();
					//});
				});
			},
			Categoria: async function (jsonFiltro) {
				await UILoadingDefault('show');
				await UIAjax.invokeMethodAsync('ajax/personal', 'GetAllRegistroPersonalDatosDfltCategoriaByFiltro', jsonFiltro).then(function (r) {
					var tableCategoriaCAMCOREDE = $('#tableCategoriaCAMCOREDE').DataTable({
						data: r,
						initComplete: function (settings, json) {
							UIAutoNumeric.Init('input.txtMontoTCACAMCOREDE', { vMax: '99999.99' });
							$('input.txtMontoTCACAMCOREDE').off('change').on('change', async function (e) {
								var ths = $(this);
								var tr = ths.closest('tr');
								var __jsonRow = tableCategoriaCAMCOREDE.row(tr).data();
								var jsonParam = {
									CodigoCategoria: __jsonRow.cod_categoria,
									Monto: UIEmpty(this.value, '0'),
									ShowToast: true,
								};
								$.extend(true, jsonParam, ___load.Filtro());
								await UIAjax.runFunctionAsync('ajax/personal', 'PostRegistroPersonalDatosDfltCategoria', jsonParam);
							});
							UILoadingDefault('hide');
						},
						searching: false,
						info: false,
						paging: false,
						columns: [
							{ data: null, defaultContent: '' },
							{ data: 'nombre_categoria' },
							{
								data: 'monto', render: function (data, type, row, meta) {
									return `<input type="text" class="form-control text-right txtMontoTCACAMCOREDE" value="${data}" />`;
								}, className: 'text-right w-160px'
							},
						]
					});
				});
			}
		}
		$('#ulTabPrincipalMCOREDE a').off('shown.bs.tab').on('shown.bs.tab', async function () {
			await ___load.DELEGATE();
		});
		$('select.selChangeMCOREDE').off('change').on('change', async function (e) {
			await ___load.DELEGATE();
		});
		await ___load.DELEGATE();
		$('#modalConfiguracionRegistroDefecto').modal('show');
	});

	$('#btnItemFichaDePersonal').on('click', function (e) {
		e.preventDefault();
		var inputChecked = $('input[name="chkTableColaboradores"]:checked');
		var Row = inputChecked.closest("tr");
		if (Row.length == 1) {
			$.ajax({
			});
			var jsonRow = tableColaboradores.row(Row).data();
			var jsonParam = {
				CodigoDePersonal: jsonRow.personal,
			};
			UIModal.Rpte('Personal', 'ShowFichaDePersonal', jsonParam);
		}
		else {
			MsgBox("Debe elegir un colaborador primero.", "warning", true);
		}
	});

	$('#btnItemActualizacionDatosColaborador').on('click', function (e) {
		e.preventDefault();

		ReloadAutoservicioWeb();

	});

	$('#btnItemClaveAutoservicioColaborador').on('click', function (e) {
		e.preventDefault();
		var inputChecked = $('input[name="chkTableColaboradores"]:checked');
		var Row = inputChecked.closest("tr");
		if (Row.length == 1) {
			var jsonRow = tableColaboradores.row(Row).data();
			if (!isEmpty(jsonRow)) {
				$('#frmClaveAutoservicioColaborador input[type="password"]').val('');
				$('#chkSolicitarCambioDeClave').prop('checked', false);
				$('#modalClaveAutoservicioColaborador').modal('show');
			}
		}
		else {
			MsgBox("Debe elegir un colaborador primero.", "warning", true);
		}
	});

	$('#tableColaboradorADC').on('click', 'tbody tr', function () {
		var ths = $(this);
		$('#tableColaboradorADC tbody tr').removeClass('selected');
		ths.addClass('selected');

		var jsonRow = tableColaboradorADC.row(ths).data();
		if (!isEmpty(jsonRow)) {
			var selInfo = $('#selectInformacionADC');
			selInfo.empty();
			var jsonParam = {
				CodigoDePersonal: jsonRow.personal,
				Tipo: "PE",
				CodigoDeEntidad: jsonRow.entidad
			};

			$.ajax({
				type: "GET",
				url: portal + "/ajax/personal/GetPersonalAutoservicioSelector",
				data: jsonParam,
				dataType: "json",
				contentType: "application/json; charset=utf-8",
				//async: false,
				success: function success(r) {
					if (r.success) {
						var htmlInfo = '';
						$.each(r.data, function (i, v) {
							htmlInfo += `<option value="${v.descripcion}">${v.descripcion}</option>`
						});
						selInfo.append(htmlInfo);
						selInfo.trigger('change');
					}
				},
				error: function error(data) {
					MsgBox("error");
				}
			});


		}

	});

	$('#selectOperacionADC').on('change', function () {
		var ths = $(this);
		$('select.selDynamicAutoservicioWeb').val(ths.val()).change();
	});

	$('#selectInformacionADC').on('change', function () {
		var ths = $(this);
		$('#selectOperacionADC').val('');
		var Rows = $('#tableColaboradorADC tbody tr.selected');
		var jsonRow = tableColaboradorADC.row(Rows).data();
		if (!isEmpty(jsonRow)) {
			var jsonParam = {
				Tipo: "PE",
				CodigoDeEntidad: jsonRow.entidad,
				CodigoDePersonal: jsonRow.personal
			};
			var tblDMADC = $('#tableDatosModificadosADC');
			var tblLADC = $('#tableListaADC');

			if ($.fn.dataTable.isDataTable(tblDMADC)) {
				tblDMADC.DataTable().clear().destroy();
			}
			else {
				//tblDMADC.empty();
			}

			if ($.fn.dataTable.isDataTable(tblLADC)) {
				tblLADC.DataTable().clear().destroy();
				tblLADC.empty();
			}
			else {
				tblLADC.empty();
			}

			var jsonDataTable = {
				bDestroy: true,
				language: {
					url: '/js/language.js'
				},
				order: false,
				ordering: false,
				searching: false,
				paging: false,
				info: false,
				responsive: false,
				//columnDefs: [
				//    { className: "th-sky", targets: "_all" },
				//]
				headerCallback: function (thead, data, start, end, display) {
					//$(thead).find('th').eq(0).html('Displaying ' + (end - start) + ' records');
					$('th', thead).addClass('th-sky');
				}
			};

			switch (ths.val()) {

				case "Familiares":
					$.extend(true, jsonDataTable, {
						ajax: portal + "/ajax/personal/GetPersonalAutoservicioListaDatosFamiliares/?" + $.param(jsonParam),
						initComplete: function (settings, json) { },
						columns: [
							{ data: "estado", title: "Estado" },
							{ data: "item", title: "Item" },
							{ data: "nombre", title: "Nombre" },
							{ data: "parentesco", title: "Parentesco" },
						],
					});
					tableListaADC = tblLADC.DataTable(jsonDataTable);
					break;
				case "Otros Datos":
					$.extend(true, jsonDataTable, {
						ajax: portal + "/ajax/personal/GetPersonalAutoservicioListaOtrosDatos/?" + $.param(jsonParam),
						initComplete: function (settings, json) { },
						columns: [
							{ data: "item", title: "Item" },
							{ data: "descripcion", title: "Pregunta" },
						],
					});
					tableListaADC = tblLADC.DataTable(jsonDataTable);
					break;
				case "Formación":
					$.extend(true, jsonDataTable, {
						ajax: portal + "/ajax/personal/GetPersonalAutoservicioListaFormacion/?" + $.param(jsonParam),
						initComplete: function (settings, json) { },
						columns: [
							{ data: "estado", title: "Estado" },
							{ data: "item", title: "Item" },
							{ data: "nivel_academico", title: "Nivel Académico" },
							{ data: "titulo", title: "Título" },
							{ data: "desde", title: "Desde" },
							{ data: "hasta", title: "Hasta" },
						],
					});
					tableListaADC = tblLADC.DataTable(jsonDataTable);
					break;
				case "Documentos Escaneados":
					$.extend(true, jsonDataTable, {
						ajax: portal + "/ajax/personal/GetPersonalAutoservicioListaDocumentosEscaneados/?" + $.param(jsonParam),
						initComplete: function (settings, json) { },
						columns: [
							{ data: "estado", title: "Estado" },
							{ data: "item", title: "Item" },
							{ data: "descripcion", title: "Descripción" },
							{ data: "flg_tipo_archivo", title: "Tipo" },
						],
					});
					tableListaADC = tblLADC.DataTable(jsonDataTable);
					break;
				case "Datos Personales": case "Datos Generales": case "Datos Laborales":
					var url = "";
					if (ths.val() == "Datos Personales") {
						url = portal + "/ajax/personal/GetPersonalAutoservicioDatosPersonales/?" + $.param(jsonParam);
					}
					else if (ths.val() == "Datos Generales") {
						url = portal + "/ajax/personal/GetPersonalAutoservicioDatosGenerales/?" + $.param(jsonParam);
					}
					else if (ths.val() == "Datos Laborales") {
						url = portal + "/ajax/personal/GetPersonalAutoservicioDatosLaborales/?" + $.param(jsonParam);
					}
					ReloadAutoservicioWebDatosModificadosADC(url);
					break;
			}
		}

	});

	$('#tableDatosModificadosADC').on('change', 'select.selDynamicAutoservicioWeb', function (e) {
		e.preventDefault();
		e.stopPropagation();
		var ths = $(this);
		var value = ths.val();
		var cell = ths.closest('td');
		var row = ths.closest('tr');
		//var jsonIndex = tableDatosModificadosADC.cell($(cell)).index();
		//var jsonRow = tableDatosModificadosADC.row(row).data();
		tableDatosModificadosADC.row(row).data().action = value;
	});

	$('#tableListaADC').on('click', 'tbody tr', function (e) {
		e.preventDefault();
		e.stopPropagation();
		$('#selectOperacionADC').val('N');
		$('#tableListaADC tbody tr').removeClass('selected');

		var ths = $(this);
		ths.addClass('selected');
		var Rows = $('#tableColaboradorADC tbody tr.selected');
		var jsonRowCO = tableColaboradorADC.row(Rows).data();
		var jsonRow = tableListaADC.row(ths).data();
		if (!isEmpty(jsonRowCO) && !isEmpty(jsonRow)) {
			var jsonParam = {
				Tipo: "PE",
				CodigoDeEntidad: jsonRowCO.entidad,
				CodigoDePersonal: jsonRowCO.personal,
				Numero: jsonRow.item,
				Codigo: jsonRow.item
			};
			var valSelect = $('#selectInformacionADC').val();
			var url = "";
			switch (valSelect) {
				case "Familiares":
					url = portal + "/ajax/personal/GetPersonalAutoservicioDatosFamiliares/?" + $.param(jsonParam);
					ReloadAutoservicioWebDatosModificadosADC(url);
					break;
				case "Otros Datos":
					url = portal + "/ajax/personal/GetPersonalAutoservicioOtrosDatos/?" + $.param(jsonParam);
					ReloadAutoservicioWebDatosModificadosADC(url);
					break;
				case "Formación":
					url = portal + "/ajax/personal/GetPersonalAutoservicioFormacion/?" + $.param(jsonParam);
					ReloadAutoservicioWebDatosModificadosADC(url);
					break;
				case "Documentos Escaneados":
					url = portal + "/ajax/personal/GetPersonalAutoservicioDocumentosEscaneados/?" + $.param(jsonParam);
					ReloadAutoservicioWebDatosModificadosADC(url);
					break;
			}
		}
	});

	$('#modalActualizacionDatosColaborador').on('click', '#btnGuardarAutoservicioWeb', function () {
		var textSel = $('#selectInformacionADC').val();
		var RowsCO = $('#tableColaboradorADC tbody tr.selected');
		var jsonRowCO = tableColaboradorADC.row(RowsCO).data();

		if (!isEmpty(jsonRowCO)) {
			var jParam = {};
			jParam.cod_personal = jsonRowCO.personal;
			jParam.cod_tipo_entidad = "PE";
			jParam.cod_entidad = jsonRowCO.entidad;
			var url = "";
			switch (textSel) {
				case "Datos Personales":
					url = `${portal}/ajax/personal/PostPersonalAutoservicioDatosPersonales`;
					break;
				case "Datos Generales":
					url = `${portal}/ajax/personal/PostPersonalAutoservicioDatosGenerales`;
					break;
				case "Datos Laborales":
					url = `${portal}/ajax/personal/PostPersonalAutoservicioDatosLaborales`;
					break;
				case "Familiares":
					var Rows = $('#tableListaADC tbody tr.selected');
					var jsonRow = tableListaADC.row(Rows).data();
					if (!isEmpty(jsonRow)) {
						url = `${portal}/ajax/personal/PostPersonalAutoservicioDatosFamiliares`;
						jParam.num_familiar = jsonRow.item;
					}
					else {
						MsgBox("Debe elegir un registro y las acciones.", "warning", true);
					}
					break;
				case "Otros Datos":
					var Rows = $('#tableListaADC tbody tr.selected');
					var jsonRow = tableListaADC.row(Rows).data();
					if (!isEmpty(jsonRow)) {
						url = `${portal}/ajax/personal/PostPersonalAutoservicioOtrosDatos`;
						jParam.codigo = jsonRow.item;
					}
					else {
						MsgBox("Debe elegir un registro y las acciones.", "warning", true);
					}
					break;
				case "Formación":
					var Rows = $('#tableListaADC tbody tr.selected');
					var jsonRow = tableListaADC.row(Rows).data();
					if (!isEmpty(jsonRow)) {
						url = `${portal}/ajax/personal/PostPersonalAutoservicioFormacion`;
						jParam.item = jsonRow.item;
					}
					else {
						MsgBox("Debe elegir un registro y las acciones.", "warning", true);
					}
					break;
				case "Documentos Escaneados":
					var Rows = $('#tableListaADC tbody tr.selected');
					var jsonRow = tableListaADC.row(Rows).data();
					if (!isEmpty(jsonRow)) {
						url = `${portal}/ajax/personal/PostPersonalAutoservicioDocumentosEscaneados`;
						jParam.item = jsonRow.item;
					}
					else {
						MsgBox("Debe elegir un registro y las acciones.", "warning", true);
					}
					break;
				default:
					MsgBox("Opción " + textSel + " en desarrollo.", "warning", true);
					break;

			}
			if (url != "") {
				var jsonDM = tableDatosModificadosADC.rows().data();
				$.each(jsonDM, function (i, k) {
					jParam[k.param] = k.cod_Destino;
					jParam[k.param + "_M"] = "M";
					jParam[k.param + "_P"] = k.action;
				});
				$.ajax({
					type: "POST",
					url: url,
					data: jParam,
					dataType: "json",
					//contentType: "application/json; charset=utf-8",
					success: function (r) {
						if (r.respuesta == 1) {
							ReloadAutoservicioWeb();
							//Rows.trigger('click');
							MsgBox("success");
						}
						else {
							MsgBox(r.mensaje, "warning", false);
						}
					},
					error: function (r) {
						MsgBox(r.mensaje, "error", false);
						console.log(r);
					}
				});
			}
			else {
				//MsgBox("Opción " + textSel + " en desarrollo.", "warning", true);
			}
		}
		else {
			MsgBox("No hay registros para guardar.", "warning", true);
		}

	});


	$('#btnAceptarCAC').on('click', async function () {
		var jsonForm = $('#frmClaveAutoservicioColaborador').serializeObject();
		if (!isEmpty(jsonForm)) {
			if (isEmpty(jsonForm.Password)) {
				MsgBox("La contraseña no puede ser vacía.", "warning", true);
			}
			else if (jsonForm.Password != jsonForm.RepeatPassword) {
				MsgBox("Las contraseñas no coinciden.", "warning", true);
			}
			else {
				var inputChecked = $('#tableColaboradores input[name="chkTableColaboradores"]:checked');
				var Row = inputChecked.closest("tr");
				if (Row.length == 1) {
					var jsonRow = tableColaboradores.row(Row).data();
					if (!isEmpty(jsonRow)) {
						jsonForm.NumeroDeDocumento = $.trim(jsonRow.numero_doc);
						await UILoadingDefault("show");
						await UIAjax.runFunctionAsync('ajax/personal', 'PostPersonalClave', jsonForm, function () {
							$('#modalClaveAutoservicioColaborador').modal('hide');
						});
						UILoadingDefault("hide");
					}
				}
				else {
					MsgBox("Debe elegir un colaborador primero.", "warning", true);
				}
			}
		}
	});

	//RELOJES
	$('#btnGuardarRJ').on('click', function () {

	});

});

function FormacionReloadInstitucionEducativa(Filtro) {
	var container = $('#selInstEducativa_Formacion');
	container.empty();
	$.ajax({
		type: "GET",
		url: portal + "/ajax/personal/GetInstedusunat",
		data: { Filtro: Filtro },
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		async: false,
		success: function success(data) {
			//console.log(data);
			var html = "";
			$.each(data, function (index, value) {
				html = "<option value='" + value.codigo + "'>" + value.nombre + "</option>";
				container.append(html);
			});
			container.prop('selectedIndex', 0);
			container.trigger('change');
		},
		error: function error(response) {
			console.log(response);
		}
	});
}

function ReloadAutoservicioWeb() {
	var jsonParam = {
		CodigoDeTipoPlanilla: $('#selectTipoPlanilla').val(),
		CodigoDeProyecto: $('#selectProyecto').val(),
		CodigoDeUN: $('#selectUnidadesNegocio').val(),
		CodigoDeCC: $('#selectCentrosCosto').val(),
		CodigoDeArea: $('#selectArea').val(),
		CodigoDeLocalidad: $('#selectLocalidad').val(),
		Estado: $('#selectEstado').val(),
		CodigoMes: ""
	};

	tableColaboradorADC = $('#tableColaboradorADC').DataTable({
		ajax: portal + "/ajax/personal/GetPersonalAutoservicio/?" + $.param(jsonParam),
		initComplete: function (settings, json) {
			$('#selectOperacionADC').val('N');
			if (json.recordsTotal > 0) {
				$('#tableColaboradorADC_filter').append('<button id="btnGuardarAutoservicioWeb" type="button" class="float-left btn btn-outline-primary">Guardar</button>');
				var firstRow = $('#tableColaboradorADC tbody tr:first-child');
				//firstRow.addClass('selected');
				firstRow.trigger('click');
			}
			else {
				var tblDMADC = $('#tableDatosModificadosADC');
				var tblLADC = $('#tableListaADC');

				if ($.fn.dataTable.isDataTable(tblDMADC)) {
					tblDMADC.DataTable().clear().destroy();
				}
				else {
					//tblDMADC.empty();
				}

				if ($.fn.dataTable.isDataTable(tblLADC)) {
					tblLADC.DataTable().clear().destroy();
					tblLADC.empty();
				}
				else {
					tblLADC.empty();
				}
				$('#selectInformacionADC').empty();
			}

			$('#modalActualizacionDatosColaborador').modal('show');
		},
		bDestroy: true,
		dom: 'Bfrtip',
		buttons: [
			//'copyHtml5',
			//'excelHtml5',
			//'csvHtml5',
			//'pdfHtml5'
		],
		language: {
			url: '/js/language.js'
		},
		order: false,
		ordering: false,
		searching: true,
		paging: false,
		info: false,
		responsive: false,
		columns: [
			{ data: "row" },
			{ data: "tipo_doc" },
			{ data: "numero_doc" },
			{ data: "trabajador" }
		],
		//createdRow: function (row, data, dataIndex) {
		//    //$(row).find('td:eq(2)').attr('data-validate', '1');
		//    $(row).addClass('unread');
		//    //$(row).attr('onclick', "GetDetallePersonal('" + data.entidad + "','" + data.personal + "')");
		//    //$(row).on('click', function (e) {
		//    //    GetDetallePersonal(row.entidad, row.personal);
		//    //});
		//    //rRow.child('<input type="text" value="ABC" class="form-control form-control-sm" />').show();
		//}
	});
}
function ReloadAutoservicioWebDatosModificadosADC(url) {
	tableDatosModificadosADC = $('#tableDatosModificadosADC').DataTable({
		ajax: url,
		initComplete: function (settings, json) {
		},
		bDestroy: true,
		language: {
			url: '/js/language.js'
		},
		order: false,
		ordering: false,
		searching: false,
		paging: false,
		info: false,
		responsive: false,
		columns: [
			{ data: "row" },
			{ data: "descripcion" },
			{ data: "origen" },
			{ data: "operacion" },
			{ data: "destino", className:'text-wrap' },
			{
				data: "action", render: function (data, type, row) {
					return `<select class="w-100 selDynamicAutoservicioWeb">
															<option value="N"></option>
															<option value="A">Aplicar</option>
															<option value="D">Descartar</option>
														</select>`;
				}, class: "minw-120px"
			},
		],
	});
}

var Load = {
	Filtro() {
		let CodigoDeProyecto = $('#selectProyecto').val();
		let CodigoDeCC = $('#selectCentrosCosto').val();
		let CodigoDeUN = $('#selectUnidadesNegocio').val();
		let CodigoDeArea = $('#selectArea').val();
		let CodigoDeLocalidad = $('#selectLocalidad').val();
		let CodigoDeTipoPlanilla = $('#selectTipoPlanilla').val();
		let Estado = $('#selectEstado').val();
		let Filtro = '';
		let CodigoDeEntidad = '';
		var jsonRow = null;
		if (!UIEmpty(tableColaboradores)) {
			let _jsonRow = tableColaboradores.row('.clicked').data();
			console.log(_jsonRow);
			if (!UIEmpty(_jsonRow)) {
				jsonRow = _jsonRow;
				CodigoDeEntidad = _jsonRow.entidad;
			}
		}
		g_proyecto = CodigoDeProyecto;
		g_ccosto = CodigoDeCC;
		g_unegocio = CodigoDeUN;
		g_area = CodigoDeArea;
		g_localidad = CodigoDeLocalidad;
		g_tipoplanilla = CodigoDeTipoPlanilla;
		g_estadotrab = Estado;
		g_filtro = Filtro;
		return {
			CodigoDeProyecto: CodigoDeProyecto,
			CodigoDeCC: CodigoDeCC,
			CodigoDeUN: CodigoDeUN,
			CodigoDeArea: CodigoDeArea,
			CodigoDeLocalidad: CodigoDeLocalidad,
			CodigoDeTipoPlanilla: CodigoDeTipoPlanilla,
			Estado: Estado,
			Filtro: Filtro,
			CodigoDeEntidad: CodigoDeEntidad,
			CodigoDePersonal: g_codPersonal,
			jsonRow: jsonRow,
		};
	},
	VariacionClasificadores: {
		Ready: async function (jsonRow) {
			$('#addVariacionClasificadoresCVARCLA').collapse('hide');
			var jsonFiltro = Load.Filtro();
			await UIAjax.invokeMethodAsync('ajax/personal', 'GetPersonalVariacionClasificadores', jsonFiltro).then(function (r) {
				tableVariacionClasificadoresCVARCLA = $('#tableVariacionClasificadoresCVARCLA').DataTable({
					data: r.data,
					initComplete: function (settings, json) {
						UIDataTable.SelectRows(this, false, false, Load.VariacionClasificadores.Add);
						UIDataTable.AJAXDelete(this, '#btnEliminarCVARCLA', 'item', 'ajax/personal', 'DeletePersonalVariacionClasificadores', {
							CodigoDePersonal: 'cod_personal',
							Item: 'item',
						}, Load.VariacionClasificadores.Ready);
					},
					columns: [
						{ data: "item" },
						{ data: "nombre_proyecto" },
						{ data: "nombre_centro_costo" },
						{ data: "nombre_unidad_negocio" },
						{ data: "nombre_localidad" },
						{ data: "nombre_area" },
						{ data: "nombre_cargo" },
						{ data: "fecha_variacion" },
						{ data: "observacion" },
						{ data: "fecha_registro" },
					],
					createdRow: function (row, data, dataIndex) {
						let tr = $(row);
						$('select.selectLocalidad', tr).val(data.cod_localidad);
						$('select.selectSigno', tr).val(data.zona_signo);
						$('select.selectFormato', tr).val(data.formato);
					}
				});
			});
		},
		Add: async function (jsonRow, ths) {
			var jsonFiltro = Load.Filtro();
			$('#addVariacionClasificadoresCVARCLA').collapse('show');
			if (jsonRow.isNew) {
				tableVariacionClasificadoresCVARCLA.$('tr').removeClass('selected');
				await UIAjax.invokeMethodAsync('Personal', 'GetOrganizacionLegajo', { codPersonal: jsonFiltro.CodigoDePersonal }).then(function (r) {
					var jsonObj = r[0];
					jsonRow.cod_proyecto = $.trim(jsonObj.cod_proyecto);
					jsonRow.cod_centro_costo = $.trim(jsonObj.cod_centro_costo);
					jsonRow.cod_unidad_negocio = $.trim(jsonObj.cod_unidad_negocio);
					jsonRow.cod_localidad = $.trim(jsonObj.cod_localidad);
					jsonRow.cod_area = $.trim(jsonObj.cod_area);
					jsonRow.cod_cargo = $.trim(jsonObj.cod_cargo);
				});
			}
			$('#selectProyectoCVARCLAR').val(jsonRow.cod_proyecto);
			$('#selectCCCVARCLAR').val(jsonRow.cod_centro_costo);
			$('#selectUNCVARCLAR').val(jsonRow.cod_unidad_negocio);
			$('#selectLocalidadCVARCLAR').val(jsonRow.cod_localidad);
			$('#selectAreaCVARCLAR').val(jsonRow.cod_area);
			$('#selectCargoCVARCLAR').val(jsonRow.cod_cargo);

			$('#txtDesdeCVARCLAR').val(jsonRow.fecha_variacion);
			$('#txtObservacionCVARCLAR').val(jsonRow.observacion);

			$('#btnGuardarCVARCLA').off('click').on('click', function () {
				var jsonParam = {
					CodigoDePersonal: jsonFiltro.CodigoDePersonal,
					Item: jsonRow.isNew ? null : jsonRow.item,
					CodigoDeProyecto: $('#selectProyectoCVARCLAR').val(),
					CodigoDeCC: $('#selectCCCVARCLAR').val(),
					CodigoDeUN: $('#selectUNCVARCLAR').val(),
					CodigoDeLocalidad: $('#selectLocalidadCVARCLAR').val(),
					CodigoDeArea: $('#selectAreaCVARCLAR').val(),
					CodigoDeCargo: $('#selectCargoCVARCLAR').val(),
					FechaDeVariacion: $('#txtDesdeCVARCLAR').val(),
					Observacion: $('#txtObservacionCVARCLAR').val(),
				};
				if (UIEmpty(jsonParam.FechaDeVariacion)) {
					MsgBox("La fecha desde no puede ser vacío.", "warning", false);
					return;
				}
				if (!UIConvert.isDate(jsonParam.FechaDeVariacion)) {
					MsgBox("La fecha desde es un valor incorrecto.", "warning", false);
					return;
				}
				UIAjax.runFunctionAsync('ajax/personal', 'PostPersonalVariacionClasificadores', jsonParam, function () {
					Load.VariacionClasificadores.Ready();
					$('#addVariacionClasificadoresCVARCLA').collapse('hide');
				});
			});
		}
	},
}

async function GetColaboradores() {

	//GetFiltros();
	var jsonParam = Load.Filtro();
	//containerColaboradores.maskContainer("Cargando ....");
	await UILoadingDefault("show");
	//UIDataTable.setEmpty('#tableColaboradores');
	await UIAjax.invokeMethodAsync('ajax/personal', 'GetColaboradoresLegajo', jsonParam).then(function (r) {
		tableColaboradores = $('#tableColaboradores').DataTable({
			data: r.data,
			initComplete: function (settings, json) {
				//containerColaboradores.unmaskContainer();
				UILoadingDefault("hide");
			},
			//searching: true,
			//bDestroy:true,
			paging: false,
			info: false,
			responsive: false,
			columnDefs: [
				{ targets: [2], className: 'text-left' },
				{ targets: '_all', className: 'text-center' },
			],
			columns: [
				{
					data: null, render: function (data, type, row) {
						return UIFunctions.Checkbox.HTML2(`customCheck${row.personal}`, false, "chkTableColaboradores");
					}
				},
				{
					data: "personal", render: function (data, type, row) {
						if (row.hasPhoto) {
							return `<img class="rounded-circle" style="width:40px;" src="${portal}/Noticias/ShowImagePersonal?codPersonal=${row.personal}" alt="activity-user">`;
						}
						else {
							return `<img class="rounded-circle" style="width:40px;" src="/images/custom/imageDefault.jpg" alt="activity-user">`;
						}
					}
				},
				{
					data: "trabajador", render: function (data, type, row) {
						return `<h6 class="mb-1">${row.trabajador}</h6><p class="m-0">${row.cargo}</p>`;
					}
				},
				{
					data: "tipo_doc", render: function (data, type, row) {
						return `<h6 class="text-muted"><i class="fas fa-circle text-c-green f-10 m-r-15"></i>${row.tipo_doc}  ${row.numero_doc}</h6>`;
					}
				},
				{
					data: "estado", render: function (data, type, row) {
						var html = `<a href="#!" class="label theme-^^ text-white f-12">${row.desc_estado}</a>`;
						if (row.estado == 'C') {
							html = html.replace('^^', 'bg2');
						}
						else {
							html = html.replace('^^', 'bg');
						}
						return html;
					}
				}
			],
			createdRow: function (row, data, dataIndex) {
				$(row).addClass('unread');
			}
		});
		UILoadingDefault("hide");
	});
}

var GetDetallePersonal = function GetDetallePersonal(pCodEntidad, pCodPersonal) {
	//$('#tabGenerales').trigger('click');
	$('#tabGenerales a').trigger('click');
	$('#tabGenerales a').trigger('shown.bs.tab');
	return;
	ClearInput('Bi');
	ClearInput('Me');
	g_accion_bienes = 'N';
	g_accion_merito = 'N';
	g_codPersonal = pCodPersonal;
	g_codEntidad = pCodEntidad;
	GetFiltros();
	//GetDatosGenerales(pCodEntidad, pCodPersonal);
	//GetDatosPersonales(pCodEntidad, "AD");
	//GetDatosLaborales(pCodPersonal);
	//GetHorario(pCodPersonal);
	//GetRelojes(pCodPersonal);
	//GetBienes_Admin(pCodPersonal);
	//GetOtrosDatos(pCodPersonal);
	//GetMeritosDemeritos(pCodPersonal);
	//GetOrganizacion(pCodPersonal);
	//GetPersonalDistribucionCC(pCodPersonal, g_proyecto);
	//GetPersonalDistribucionLocalidad(pCodPersonal);
	//GetPersonalDistribucionUnidadDeNegocio();
	//GetPersonalHistorial(pCodEntidad);
	//GetFamiliares(pCodEntidad);
	//GetRetenciones(pCodPersonal);
	//GetDocsEscaneados(pCodPersonal);
	//GetVariacionSueldo(pCodPersonal);
	//GetReloj(pCodPersonal);
	//GetPersonalcontrol(pCodPersonal);
	//GetFormacion(pCodPersonal);
	$('#codPersonal').val(g_codPersonal);
	$('#modalPersonal').modal('show');
	validaInicializaDefault();
};