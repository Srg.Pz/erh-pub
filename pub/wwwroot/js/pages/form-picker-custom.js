'use strict';
$(document).ready(function() {
    $('#date').bootstrapMaterialDatePicker({
        weekStart: 1,
        format: 'DD/MM/YYYY',
        cancelText: 'Anular',
        nowText: 'Hoy',
        time: false,
        nowButton: true,
        switchOnClick: true
    });

    $('.datePickerCustom').bootstrapMaterialDatePicker({
        weekStart: 1,
        format: 'DD/MM/YYYY',
        cancelText: 'Anular',
        nowText: 'Hoy',
        time: false,
        nowButton: true,
        switchOnClick: true
    });

    $('#dateStart').bootstrapMaterialDatePicker({
        weekStart: 1,
        lang: 'es',
        format: 'DD/MM/YYYY',
        cancelText: 'Anular',
        nowText: 'Hoy',
        time: false,
        nowButton: true,
        switchOnClick: true
    }).on('change', function (e, date) {
        $('#dateEnd').bootstrapMaterialDatePicker('setMinDate', date);
    });

    $('#dateEnd').bootstrapMaterialDatePicker({
        weekStart: 1,
        lang: 'es',
        format: 'DD/MM/YYYY',
        cancelText: 'Anular',
        nowText: 'Hoy',
        time: false,
        nowButton: true,
        switchOnClick: true
    }).on('change', function (e, date) {
        $('#dateStart').bootstrapMaterialDatePicker('setMaxDate', date);
    });

    $('.timePickerCustom').bootstrapMaterialDatePicker({
        weekStart: 1,
        lang: 'es',
        format: 'HH:mm',
        cancelText: 'Anular',
        nowText: 'Hoy',
        date: false,
        nowButton: true,
        switchOnClick: true
    });

    $('#date-format').bootstrapMaterialDatePicker({
        format: 'dddd DD MMMM YYYY - HH:mm'
    });

    $('#date-fr').bootstrapMaterialDatePicker({
        format: 'DD/MM/YYYY HH:mm',
        lang: 'fr',
        weekStart: 1,
        cancelText: 'ANNULER'
    });

    $('#min-date').bootstrapMaterialDatePicker({
        format: 'DD/MM/YYYY HH:mm',
        minDate: new Date()
    });

    $('#date-end').bootstrapMaterialDatePicker({
        weekStart: 0,
        format: 'dddd DD MMMM YYYY - HH:mm'
    });
    $('#date-start').bootstrapMaterialDatePicker({
        weekStart: 0,
        format: 'dddd DD MMMM YYYY - HH:mm'
    }).on('change', function(e, date) {
        $('#date-end').bootstrapMaterialDatePicker('setMinDate', date);
    });

    $('.demo').each(function() {
        $(this).minicolors({
            control: $(this).attr('data-control') || 'hue',
            defaultValue: $(this).attr('data-defaultValue') || '',
            format: $(this).attr('data-format') || 'hex',
            keywords: $(this).attr('data-keywords') || '',
            inline: $(this).attr('data-inline') === 'true',
            letterCase: $(this).attr('data-letterCase') || 'lowercase',
            opacity: $(this).attr('data-opacity'),
            position: $(this).attr('data-position') || 'bottom',
            swatches: $(this).attr('data-swatches') ? $(this).attr('data-swatches').split('|') : [],
            change: function(value, opacity) {
                if (!value) return;
                if (opacity) value += ', ' + opacity;
                if (typeof console === 'object') {
                    var example = '';
                }
            },
            theme: 'bootstrap'
        });
    });
});
