'use strict';
$(document).ready(function() {
    // $("#smartwizard").on("stepContent", function(e, anchorObject, stepIndex, stepDirection) {
    //     return "Here is the content for step " + stepIndex;
    // });
    
    $("#smartwizard").on("leaveStep", function(e, anchorObject, stepNumber, stepDirection) {
        if(stepDirection === "forward") {
            const buttons = $(`#step-${stepNumber + 1}`).find(".btnGuardarEquivalencia");
            let status = true;
            $.each(buttons, function(index, button) {
                if($(button).data('status') !== 'success'){
                    MsgBox("Aún no ha validado las equivalencias.", "warning")
                    status = false;
                }
            })
            return status;
        }
    });

    $("#smartwizard").on("showStep", async function(e, anchorObject, stepIndex, stepDirection) {
        if(stepIndex == 4) {
          $('#containerVistaPrevia').empty();
          try {
            UILoadingDefault('show')
            const _ruta = $('#txtRutaBoletasExternas').val()
            if(_ruta == "") {
              MsgBox("No se ha ingresado la ruta de las boletas del sistema externo.", "warning")
              return
            }
            let DesdeEmpresa = $("#txtDesdeEmpresa").val() - 1
            let DesdePeriodoAnio = $("#txtDesdePeriodoAnio").val() - 1
            let DesdePeriodoMes = $("#txtDesdePeriodoMes").val() - 1
            let DesdeProceso = $("#txtDesdeProceso").val() - 1
            let DesdeDni = $("#txtDesdeDni").val() - 1

            let HastaEmpresa = $("#txtHastaEmpresa").val() - 1
            let HastaPeriodoAnio = $("#txtHastaPeriodoAnio").val() - 1
            let HastaPeriodoMes = $("#txtHastaPeriodoMes").val() - 1
            let HastaProceso = $("#txtHastaProceso").val() - 1
            let HastaDni = $("#txtHastaDni").val() - 1

            const jsonParam = {
              rutaOrigen: _ruta,  
              defaultEmpresa: $('#flgValorDefectoEmpresas').prop('checked'),  
              inicioEmpresa: DesdeEmpresa,  
              finEmpresa: HastaEmpresa, 
              inicioAnio: DesdePeriodoAnio, 
              finAnio: HastaPeriodoAnio, 
              inicioMes: DesdePeriodoMes, 
              finMes: HastaPeriodoMes, 
              inicioProceso: DesdeProceso, 
              finProceso: HastaProceso, 
              inicioDni: DesdeDni, 
              finDni: HastaDni
            }

            let r = await UIAjax.invokeMethodAsync('ajax/planillas', 'InfoVistaPrevia', jsonParam)
            if(r.respuesta == 1) {
                let data = r.table
                const empresas = Array.from(new Set(data.map(x => x.nombre_empresa)))
                let html = '';
                $.each(empresas, function(index, empresa){
                  const dataEmpresa = data.filter(x => x.nombre_empresa === empresa)
                  const periodos = Array.from(new Set(dataEmpresa.map(x => x.periodo)))
                    html += `<div id="accordion">
                    <div class="card">
                      <div class="card-header" id="heading${index}">
                        <h5 class="mb-0">
                          <button class="btn btn-link" data-toggle="collapse" data-target="#collapse${index}" aria-expanded="true" aria-controls="collapse${index}">
                            ${empresa}
                          </button>
                        </h5>
                      </div>
                  
                      <div id="collapse${index}" class="collapse" aria-labelledby="heading${index}" data-parent="#accordion">
                        <div class="card-body pt-0" style="background: #f4f7fa;">
                          <div class="row">`
                          
                            periodos.forEach((periodo, indexPeriodo) => {
                              const dataPeriodo = dataEmpresa.filter(x => x.periodo === periodo)
                              const nroTrabajadores = Array.from(new Set(dataPeriodo.map(x => x.dni)))
                              html += 
                              `<div class="col-md-4 col-12 px-1">
                              <div class="card Active-visitor mb-0  my-3">
                                <div class="card-block text-center">
                                  <h5 class="mb-0">Periodo</h5>
                                  <h2 class="f-w-300 mt-1">${periodo}</h2>
                                  <div class="progress my-3">
                                      <div class="progress-bar progress-c-theme" role="progressbar" style="width: 100%;height:7px;" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                                  </div>
                                  <div class="row card-active">
                                      <div class="col-6 collapsed" data-toggle="collapse" data-target="#collapseBoletas${index}-${indexPeriodo}" 
                                        aria-expanded="false" aria-controls="collapseBoletas${index}-${indexPeriodo}" onclick="fnRemoveShow('#collapseTrabajadores${index}-${indexPeriodo}')">
                                          <h4 class="m-0">${dataPeriodo.length}</h4>
                                          <span class="text-muted">Boletas</span>
                                      </div>
                                      <div class="col-6 collapsed" data-toggle="collapse" data-target="#collapseTrabajadores${index}-${indexPeriodo}"  onclick="fnRemoveShow('#collapseBoletas${index}-${indexPeriodo}')"
                                      aria-expanded="false" aria-controls="collapseTrabajadores${index}-${indexPeriodo}">
                                          <h4 class="m-0">${nroTrabajadores.length}</h4>
                                          <span class="text-muted">Trabajadores</span>
                                      </div>
                                  </div>
                                  <hr>
                                  <div class="row">
                                    <div class="collapse w-100" id="collapseBoletas${index}-${indexPeriodo}">
                                      <div style="max-height: 15rem; overflow-y: auto; overflow-x: hidden;">`
                                      const dataProceso = Array.from(new Set(dataPeriodo.map(x => x.nombre_clase_proceso)))
                                      dataProceso.forEach((proceso, indexProceso) => {
                                        const arrProceso = dataPeriodo.filter(x => x.nombre_clase_proceso === proceso)
                                        html += `<div class="row justify-content-center">
                                                  <span class="col-7 text-left font-weight-bold">${proceso}</span>
                                                  <span class="col-3 font-weight-bold">${arrProceso.length}</span>
                                                </div>`
                                      })
                                      html += `</div>
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="collapse w-100" id="collapseTrabajadores${index}-${indexPeriodo}">
                                      <ol class="mb-0">`
                                      const dataPersonal = Array.from(new Set(dataPeriodo.map(x => x.nombre_personal)))
                                      dataPersonal.forEach((personal, indexPersonal) => {
                                        html += `<li class="mb-0">${personal}</li>`
                                      })
                                      html += `</ol>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              </div>`
                            })
                        html += `</div></div>
                      </div>
                      </div>
                    </div>`
                })
                $('#containerVistaPrevia').append(html);
                $("#smartwizard").find('.sw-toolbar-bottom').append('<button class="btn btn-secondary m-0 btnFinalizar" onclick="fnFinalizar()">Finalizar</button>')
            } else if(r.respuesta == -2) {
              let html = `<div class="row">
              <div class="col-12">
                <p class="font-weight-bold mt-4" style="color: red;">Regrese a corregir los errores antes de finalizar.</p>
                <ol>`
              r.mensaje.split('|').forEach((item) => {
                html += `<li>${item}</li>`
              })
              html += `</ol></div></div>`
              $('#containerVistaPrevia').append(html);

            } else {
              MsgBox(r.mensaje, "warning")
            }
          } catch(e) {
            MsgBox(e.message, 'warning');
          } finally {
            UILoadingDefault('hide')
          }
        } else {
          $("#smartwizard").find('.btnFinalizar').remove()
        }
    });

    $('#smartwizard').smartWizard({
        theme: 'default',//'arrows',
        transitionEffect: 'fade',
        autoAdjustHeight: false,
        useURLhash: false,
        showStepURLhash: false,
        lang: {
            next: 'Siguiente',
            previous: 'Anterior'
        }
    });
    $('#smartwizard .sw-toolbar').appendTo($('#smartwizard .sw-container'));
});
