﻿var tableCliente;
$(document).ready(async function () {
	UIDatePicker.Init('#txtFechaDeInscripcionMCLTD');
	UIAutoNumeric.Init('#txtNumeroDeTrabajadoresMCLTD', { vMin: '0', vMax: '999999' });
	UIAutoNumeric.Init('#txtTasaRiesgoMCLTE', { vMax: '999.99' });
	UIFNCustom.Ubigeo('#selectDepartamentoMCLTD', '#selectProvinciaMCLTD', '#selectDistritoMCLTD');

	$('#ulNavMRPTVA').on('shown.bs.tab', async function (e) {
		var jsonRow = tableCliente.row('.selected').data();
		if (!UIEmpty(jsonRow)) {
			Load.Modal.DELEGATE(jsonRow);
		}
	});
	$('#btnNuevoCliente').on('click', function (e) {
		$('#tableCliente tbody tr').removeClass('selected');
		$('#aLinkDatosMCL').trigger('click');
		$('#aLinkEstablecimientosMCL').addClass('d-none');
		Load.Modal.Ready({ isNew: true });
	});

	

	await Load.Ready();
});
var Load = {
	Ready: async function () {
		UIDataTable.setEmpty('#tableCliente');
		await UIAjax.invokeMethodAsync('ajax/tablassoporte', 'GetAllClientesByEmpresa').then(function (r) {
			tableCliente = $('#tableCliente').DataTable({
				data: r.data,
				initComplete: function (settings, json) {
					UIDataTable.SelectRows(this, false, false, Load.Modal.Ready);
				},
				columns: [
					{ data: 'cod_cliente' },
					{ data: 'ruc', className: 'maxw-120px' },
					{ data: 'cliente' },
					{ data: 'siglas' },
					{ data: 'representante' },
				]
			});
		});
	},
	Modal: {
		Ready: async function (jsonRow) {
			await Load.Modal.DELEGATE(jsonRow);
			$('#modalCliente').modal('show');
		},
		DELEGATE: async function (jsonRow) {
			var tabActive = $('#ulNavMRPTVA a.active');
			var actionExecute = '';
			switch (tabActive.prop('id')) {
				case 'aLinkDatosMCL':
					actionExecute = 'Load.Modal.Datos';
					break;
				case 'aLinkEstablecimientosMCL':
					actionExecute = 'Load.Modal.Establecimientos';
					break;
			}
			//if (jsonRow.isNew) {
			//	$('#aLinkDatosMCL').trigger('click');
			//	$('#aLinkEstablecimientosMCL').addClass('d-none');
			//}
			//else {
			//	$('#aLinkEstablecimientosMCL').removeClass('d-none');
			//}

			if (!UIEmpty(actionExecute)) {
				await UIFunctions.invokeJSAsync(actionExecute, jsonRow);
			}
		},
		Datos: async function (jsonValue) {
			await UILoadingDefault("show");
			var jsonEntidad = {};

			if (jsonValue.isNew) {
				$('#h4TitleMCLTD').html('NUEVO CLIENTE');
				jsonValue.cod_cliente = '-1';
			}
			else {
				$('#h4TitleMCLTD').html(`CLIENTE: ${jsonValue.cod_cliente}`);
				$('#aLinkEstablecimientosMCL').removeClass('d-none');
				var jsonParam = {
					CodigoDeEntidad: jsonValue.cod_entidad,
					CodigoDeTipoEntidad: 'CL',
				};
				await UIAjax.invokeMethodAsync('ajax/tablassoporte', 'GetSingleEntidadesByEntidadTipoEntidad', jsonParam).then(function (r) {
					jsonEntidad = r[0];
				});
			}

			$('#txtCodigoMCLTD').val(jsonValue.cod_cliente);
			$('#txtRUCMCLTD').val(jsonValue.ruc);
			$('#txtClienteMCLTD').val(jsonValue.cliente);
			$('#txtSiglasMCLTD').val(jsonValue.siglas);
			$('#txtFechaDeInscripcionMCLTD').val(jsonValue.fecha_inscripcion);
			$('#selectActividadMCLTD').val(jsonValue.cod_actividad);
			$('#txtNumeroDeTrabajadoresMCLTD').val(jsonValue.nro_trabajadores);
			$('#selectDocumentoRepresentanteMCLTD').val(jsonValue.tipo_docid_rep);
			$('#txtNumeroDocumentoRepresentanteMCLTD').val(jsonValue.cod_docid_rep);
			$('#txtRepresentanteMCLTD').val(jsonValue.representante);
			$('#selectViaMCLTD').val(jsonEntidad.cod_via);
			$('#txtDireccionMCLTD').val(jsonEntidad.direccion);
			$('#selectZonaMCLTD').val(jsonEntidad.cod_zona);
			$('#txtZonaMCLTD').val(jsonEntidad.zona);
			$('#txtReferenciaMCLTD').val(jsonEntidad.referencia);
			if (!UIEmpty(jsonEntidad.cod_ubigeo) && jsonEntidad.cod_ubigeo != "-1") {
				$('#selectDepartamentoMCLTD').val(jsonEntidad.cod_departamento).change();
				$('#selectProvinciaMCLTD').val(jsonEntidad.cod_provincia).change();
				$('#selectDistritoMCLTD').val(jsonEntidad.cod_ubigeo).change();
			}
			else {
				$('#selectDepartamentoMCLTD').val('-1');
			}
			$('#txtTelefonosMCLTD').val(jsonEntidad.telefonos);
			$('#txtTelefono2MCLTD').val(jsonEntidad.telefono2);
			$('#txtTelefono3MCLTD').val(jsonEntidad.telefono3);
			$('#txtFaxMCLTD').val(jsonEntidad.fax);
			$('#txtEmailMCLTD').val(jsonEntidad.email);
			$('#txtWWWlMCLTD').val(jsonEntidad.www);

			$('#btnConsultarSunatMCLTD').off('click').on('click', async function (e) {
				var RUC = document.getElementById('txtRUCMCLTD').value;
				if (UIRUC.Validar(RUC)) {
					await UILoadingDefault('show');
					try {
						await UIRUC.Consultar(RUC).then(function (jsonRUC) {
							if (jsonRUC.success) {
								$('#txtClienteMCLTD').val(jsonRUC.nombre);
								$('#txtDireccionMCLTD').val(jsonRUC.direccion);
							}
							else {
								MsgBox(jsonRUC.mensaje, "warning", false);
							}
						});
					} catch (e) {
						MsgBox("El servicio de SUNAT dejó de funcionar.", "info", false);
					}
					UILoadingDefault('hide');
				}
				else {
					MsgBox('El R.U.C ingresado es incorrecto', 'warning', false);
				}
			});

			$('#btnGuardarMCLTD').off('click').on('click', async function (e) {
				let jsonParam = {
					Accion: null,
					CodigoDeEntidad: jsonValue.cod_entidad,
					CodigoDeCliente: jsonValue.cod_cliente,
					NombreDeCliente: $('#txtClienteMCLTD').val(),
					Ruc: $('#txtRUCMCLTD').val(),
					Siglas: $('#txtSiglasMCLTD').val(),
					FechaInscripcion: $('#txtFechaDeInscripcionMCLTD').val(),
					Representante: $('#txtRepresentanteMCLTD').val(),
					CodigoTipoDocumentoRepresentante: $('#selectDocumentoRepresentanteMCLTD').val(),
					CodigoDocumentoRepresentante: $('#txtNumeroDocumentoRepresentanteMCLTD').val(),
					CodigoCuenta: $('#selectCuentaContableMCLTD').val(),
					NumeroTrabajadores: $('#txtNumeroDeTrabajadoresMCLTD').val(),
					CodigoActividad: $('#selectActividadMCLTD').val(),
					CodigoDeVia: $('#selectViaMCLTD').val(),
					Direccion: $('#txtDireccionMCLTD').val(),
					Numero: $('#txtNumeroMCLTD').val(),
					Interior: $('#txtInteriorMCLTD').val(),
					CodigoDeZona: $('#selectZonaMCLTD').val(),
					Zona: $('#txtZonaMCLTD').val(),
					Referencia: $('#txtReferenciaMCLTD').val(),
					CodigoDeUbigeo: $('#selectDistritoMCLTD').val(),
					Telefonos: $('#txtTelefonosMCLTD').val(),
					Telefono2: $('#txtTelefono2MCLTD').val(),
					Telefono3: $('#txtTelefono3MCLTD').val(),
					Fax: $('#txtFaxMCLTD').val(),
					Email: $('#txtEmailMCLTD').val(),
					WWW: $('#txtWWWlMCLTD').val(),
				};
				if (jsonValue.isNew) {
					jsonParam.Accion = 'N';
				}
				else {
					jsonParam.Accion = 'M';
				}
				if (UIEmpty(jsonParam.NombreDeCliente)) {
					MsgBox("El 'Nombre del Cliente' no puede ser vacío.", "warning", false);
					return;
				}
				if (UIEmpty(jsonParam.Ruc)) {
					MsgBox("El 'R.U.C' no puede ser vacío.", "warning", false);
					return;
				}
				await UIAjax.runFunctionAsync('ajax/tablassoporte', 'PostClienteDatos', jsonParam, async function () {
					await Load.Ready();
					$('#modalCliente').modal('hide');
				});
			});
			$('#modalCliente').modal('show');
			UILoadingDefault('hide');
		},
		Establecimientos: async function (jsonValue) {
			await UILoadingDefault("show");
			UIDataTable.setEmpty('#tableEstablecimientoMCLTE');
			$('#txtCodigoMCLTE').val(null);
			$('#txtNombreMCLTE').val(null);
			$('#selectTipoMCLTE').val(null);
			$('#txtTasaRiesgoMCLTE').val(null);
			UICheckbox.setValue('#chkCentroRiesgoMCLTE', '0');
			var jsonParam = {
				CodigoDeCliente: jsonValue.cod_cliente,
			};
			await UIAjax.invokeMethodAsync('ajax/tablassoporte', 'GetAllClientesEstablecimientosByCliente', jsonParam).then(function (r) {
				UILoadingDefault("hide");
				var fnEstablecimientosDetalle = function (jsonEstablecimiento) {
					$('#txtCodigoMCLTE').val(jsonEstablecimiento.codigo);
					$('#txtNombreMCLTE').val(jsonEstablecimiento.nombre);
					$('#selectTipoMCLTE').val(jsonEstablecimiento.tipo);
					$('#txtTasaRiesgoMCLTE').val(jsonEstablecimiento.tasa_riesgo);
					UICheckbox.setValue('#chkCentroRiesgoMCLTE', jsonEstablecimiento.flg_riesgo);
					$('#btnGuardarMCLTE').off('click').on('click', async function (e) {
						let jsonParamEstablecimiento = {
							Accion: null,
							CodigoDeCliente: jsonEstablecimiento.cod_cliente,
							Codigo: jsonEstablecimiento.codigo,
							Nombre: $('#txtNombreMCLTE').val(),
							FlgRiesgo: UICheckbox.getValue('#chkCentroRiesgoMCLTE'),
							TasaRiesgo: $('#txtTasaRiesgoMCLTE').val(),
							Tipo: $('#selectTipoMCLTE').val(),
						};
						if (jsonEstablecimiento.isNew) {
							jsonParamEstablecimiento.Accion = 'N';
						}
						else {
							jsonParamEstablecimiento.Accion = 'M';
						}
						if (UIEmpty(jsonParamEstablecimiento.Nombre)) {
							UIMensaje.NOMBRE();
							return;
						}
						await UIAjax.runFunctionAsync('ajax/tablassoporte', 'PostClientesEstablecimientos', jsonParamEstablecimiento, async function () {
							await Load.Modal.Establecimientos(jsonValue);
							$('#btnNuevoMCLTE').trigger('click');
						});
					});
				};
				tableEstablecimientoMCLTE = $('#tableEstablecimientoMCLTE').DataTable({
					data: r.data,
					initComplete: function (settings, json) {
						UIDataTable.SelectRows(this, false, false, fnEstablecimientosDetalle);
						$('#btnNuevoMCLTE').off('click').on('click', async function (e) {
							$('#tableEstablecimientoMCLTE tbody tr').removeClass('selected');
							fnEstablecimientosDetalle({ isNew: true, cod_cliente: jsonValue.cod_cliente });
						});
					},
					columns: [
						{ data: 'codigo' },
						{ data: 'nombre' },
						{ data: 'tipo' },
						UIDataTable.CreateColumnCHK('flg_riesgo', 'w-100px'),
						{ data: 'tasa_riesgo' },
					]
				});
			});
		}
	},
}