if (typeof Load != undefined) {
    if (typeof Load.Asistencia != undefined) {
        //$.ajax(portal + '/ajax/core/GetLocalidadesUsuario').done(function (data) {
        //    console.log(data);
        //});
        Load.Asistencia.RelojMarcacion = {
            selectLocalidadHTML: '',
            selSignoHTML: '',
            selFormatoHTML: '',
            Ready: async function () {
                Load.Asistencia.RelojMarcacion.selectLocalidadHTML = '';
                Load.Asistencia.RelojMarcacion.selSignoHTML = '';
                Load.Asistencia.RelojMarcacion.selFormatoHTML = '';
                Load.ShowCard('#cardRelojMarcacion');
                UIDataTable.setEmpty('#tableRelojMarcacion');
                await UIAjax.invokeMethodAsync('ajax/tablassoporte', 'GetAllRelojMarcacion').then(async function (r) {
                    var _columnCHK = function (_data) {
                        return {
                            data: _data, render: function (data, type, row, meta) {
                                return UIFunctions.Checkbox.HTML2(null, data, null, true);
                            }, className: 'text-center align-middle'
                        };
                    };

                    await UIFunctions.setSelect('ajax/core', 'GetLocalidadesUsuario', {}, false, null, 'codigo', 'codigo_nombre').then(function (rl) {
                        Load.Asistencia.RelojMarcacion.selectLocalidadHTML += rl;
                    });

                    Load.Asistencia.RelojMarcacion.selSignoHTML += '<option value="+">+</option>';
                    Load.Asistencia.RelojMarcacion.selSignoHTML += '<option value="-">-</option>';

                    Load.Asistencia.RelojMarcacion.selFormatoHTML += '<option value="TXT">Texto</option>';
                    Load.Asistencia.RelojMarcacion.selFormatoHTML += '<option value="DBF">Dbase</option>';
                    Load.Asistencia.RelojMarcacion.selFormatoHTML += '<option value="XLS">Excel</option>';

                    var selectLocalidadHTML = '<select class="form-control selectLocalidad" disabled>';
                    selectLocalidadHTML += Load.Asistencia.RelojMarcacion.selectLocalidadHTML;
                    selectLocalidadHTML += "</select>";
                    var selSignoHTML = '<select class="form-control selectSigno" disabled>';
                    selSignoHTML += Load.Asistencia.RelojMarcacion.selSignoHTML;
                    selSignoHTML += "</select>";
                    var selFormatoHTML = '<select class="form-control selectFormato" disabled>';
                    selFormatoHTML += Load.Asistencia.RelojMarcacion.selFormatoHTML;
                    selFormatoHTML += "</select>";

                    tableRelojMarcacion = $('#tableRelojMarcacion').DataTable({
                        data: r.data,
                        initComplete: function (settings, json) {
                            UIDataTable.SelectRows(this, false, false, Load.Asistencia.RelojMarcacion.Modal);
                            UIDataTable.AJAXDelete(this, '#btnEliminarRelojMarcacion', 'cod_reloj', 'ajax/tablassoporte', 'DeleteRelojMarcacion', {
                                CodigoDeReloj: 'cod_reloj',
                            }, Load.Asistencia.RelojMarcacion.Ready);
                        },
                        columnDefs: [
                            { targets: [1], className: 'text-left' },
                            { targets: '_all', className: 'text-center' },
                        ],
                        columns: [
                            { data: "cod_reloj", className: 'text-center' },
                            { data: "descripcion" },
                            {
                                data: "cod_localidad", render: function (data, type, row, meta) {
                                    return selectLocalidadHTML;
                                }
                            },
                            _columnCHK("flg_comedor"),
                            {
                                data: "zona_signo", render: function (data, type, row, meta) {
                                    return selSignoHTML;
                                }
                            },
                            { data: "zona_hora_texto" },
                            {
                                data: "formato", render: function (data, type, row, meta) {
                                    return selFormatoHTML;
                                }
                            },
                            { data: "fotocheck" },
                            { data: "reloj" },
                            { data: "cod_anual" },
                            { data: "mes" },
                            { data: "dia" },
                            { data: "hora" },
                            { data: "minuto" },
                            { data: "segundo" },
                            { data: "tipo_dia" },
                            { data: "directorio" },
                            { data: "archivo" },
                        ],
                        createdRow: function (row, data, dataIndex) {
                            let tr = $(row);
                            $('select.selectLocalidad', tr).val(data.cod_localidad);
                            $('select.selectSigno', tr).val(data.zona_signo);
                            $('select.selectFormato', tr).val(data.formato);
                        }
                    });
                });
            },
            Modal: async function (jsonValue) {
                await UILoadingDefault("show");
                if (jsonValue.isNew) {
                    $('#h4TitleMREMA').html('NUEVO RELOJ DE MARCACIÓN');
                    $('#txtCodigoMREMA').removeAttr('disabled');
                    jsonValue.cod_localidad = '-1';
                }
                else {
                    $('#h4TitleMREMA').html(`RELOJ DE MARCACIÓN: ${jsonValue.cod_reloj}`);
                    $('#txtCodigoMREMA').attr('disabled', 'disabled');
                }
                var fnBoolean = function (key) { return jsonValue[key] == "S" ? true : false; };
                await UIFunctions.setSelect('ajax/core', 'GetLocalidadesUsuario', null, false, '#selectLocalidadMREMA', null, 'codigo_nombre');
                $('#selectSignoMREMA').html(Load.Asistencia.RelojMarcacion.selSignoHTML);
                $('#selectFormatoMREMA').html(Load.Asistencia.RelojMarcacion.selFormatoHTML);

                //$('#selectLocalidadMREMA').html(Load.Asistencia.RelojMarcacion.selectLocalidadHTML);
                $('#txtCodigoMREMA').val(jsonValue.cod_reloj);
                $('#txtDescripcionMREMA').val(jsonValue.descripcion);
                $('#selectLocalidadMREMA').val(jsonValue.cod_localidad);
                $('#chkComedorMREMA').prop('checked', fnBoolean("flg_comedor"));

                $('#selectSignoMREMA').val(jsonValue.zona_signo);
                $('#txtZonaHorariaMREMA').val(UIEmpty(jsonValue.zona_hora_texto, '00:00'));
                $('#selectFormatoMREMA').val(jsonValue.formato);
                $('#txtFotocheckMREMA').val(jsonValue.fotocheck);
                $('#txtRelojMREMA').val(jsonValue.reloj);
                $('#txtCodigoAnualMREMA').val(jsonValue.cod_anual);
                $('#txtMesMREMA').val(jsonValue.mes);
                $('#txtDiaMREMA').val(jsonValue.dia);
                $('#txtHoraMREMA').val(jsonValue.hora);
                $('#txtMinutoMREMA').val(jsonValue.minuto);
                $('#txtSegundoMREMA').val(jsonValue.segundo);
                $('#txtTipoDiaMREMA').val(jsonValue.tipo_dia);

                $('#txtDirectorioMREMA').val(jsonValue.directorio);
                $('#txtArchivoMREMA').val(jsonValue.archivo);


                $('#btnGuardarMREMA').off('click').on('click', function (e) {
                    let colorRG = $('#txtColoresMTIEX').minicolors('rgbObject');
                    let jsonParam = {
                        Accion: null,
                        CodigoDeReloj: null,
                        Descripcion: $('#txtDescripcionMREMA').val(),
                        Formato: $('#selectFormatoMREMA').val(),
                        CodigoDeFotocheck: $('#txtFotocheckMREMA').val(),
                        Reloj: $('#txtRelojMREMA').val(),
                        CodigoAnual: $('#txtCodigoAnualMREMA').val(),
                        CodigoMes: $('#txtMesMREMA').val(),
                        Dias: $('#txtDiaMREMA').val(),
                        Hora: $('#txtHoraMREMA').val(),
                        Minuto: $('#txtMinutoMREMA').val(),
                        Segundo: $('#txtSegundoMREMA').val(),
                        Directorio: $('#txtDirectorioMREMA').val(),
                        NombreDeArchivo: $('#txtArchivoMREMA').val(),
                        Tipo: $('#txtTipoDiaMREMA').val(),
                        GPSLatitud: null,
                        GPSLongitud: null,
                        GPSAltitud: null,
                        GPSReferencia: null,
                        GPSAccuracy: null,
                        CodigoDeLocalidad: $('#selectLocalidadMREMA').val(),
                        Estado: 'S',
                        GPSRangoMetros: '50',
                        FlgComedor: UIFunctions.getValue.HTML('#chkComedorMREMA'),
                        ZonaSigno: $('#selectSignoMREMA').val(),
                        ZonaHora: $('#txtZonaHorariaMREMA').val(),

                    };
                    if (jsonValue.isNew) {
                        jsonParam.Accion = 'N';
                        jsonParam.CodigoDeReloj = $('#txtCodigoMREMA').val();
                    }
                    else {
                        jsonParam.Accion = 'M';
                        jsonParam.CodigoDeReloj = jsonValue.cod_reloj;
                    }
                    if (UIEmpty(jsonParam.CodigoDeReloj)) {
                        UIMensaje.CODIGO();
                        return;
                    }
                    if (UIEmpty(jsonParam.Descripcion)) {
                        UIMensaje.DESCRIPCION();
                        return;
                    }
                    if (UIEmpty(jsonParam.CodigoDeLocalidad)) {
                        MsgBox("El código de localidad no puede ser vacío.", "warning", false);
                        return;
                    }
                    //return;
                    UIAjax.runFunctionAsync('ajax/tablassoporte', 'PostRelojMarcacion', jsonParam, function () {
                        Load.Asistencia.RelojMarcacion.Ready();
                        $('#modalRelojMarcacion').modal('hide');
                    });
                });
                $('#modalRelojMarcacion').modal('show');
                UILoadingDefault();
            }
        };
        Load.Asistencia.TipoExcepcion = {
            Ready: async function () {
                Load.ShowCard('#cardTipoExcepciones');
                UIDataTable.setEmpty('#tableTipoExcepcion');
                await UIAjax.invokeMethodAsync('ajax/tablassoporte', 'GetAllTipoExcepciones',).then(function (r) {
                    var _columnCHK = function (_data) {
                        return {
                            data: _data, render: function (data, type, row, meta) {
                                return UIFunctions.Checkbox.HTML2(null, data, null, true);
                            }, className: 'text-center align-middle'
                        };
                    };
                    tableTipoExcepcion = $('#tableTipoExcepcion').DataTable({
                        data: r.data,
                        initComplete: function (settings, json) {
                            var api = this.api();
                            var trs = api.$('tr');
                            trs.on('click', function (e) {
                                let ths = $(this);
                                trs.removeClass('selected');
                                ths.addClass('selected');
                                var jsonRow = Object.assign({}, api.row(this).data());
                                jsonRow.Title = "Excepción: " + jsonRow.cod_excepcion;
                                Load.Asistencia.TipoExcepcion.Modal(jsonRow);
                            });
                            $('#btnEliminarTipoExcepcion').off('click').on('click', function (e) {
                                var jsonRow = api.row(api.$('tr.selected')).data();
                                if (UIEmpty(jsonRow)) {
                                    MsgBox('Debe seleccionar un tipo de excepción primero.', 'warning', true);
                                    return;
                                }
                                MsgBox(`Se eliminará el código: ${jsonRow.cod_excepcion}.`, 'question', false, true, async function () {
                                    var jsonParam = {
                                        CodigoDeExcepcion: jsonRow.cod_excepcion,
                                    };
                                    await UIAjax.invokeFunctionAsync('ajax/tablassoporte', 'DeleteTipoExcepciones', jsonParam).then(function (r) {
                                        if (r.respuesta > 0) {
                                            Load.Asistencia.TipoExcepcion.Ready();
                                            MsgBox('success');
                                        }
                                        else {
                                            if (r.mensaje.includes("Existen trabajadores")) {
                                                driver.highlight({
                                                    element: '#btnColaboradoresTipoExcepcion',
                                                    popover: {
                                                        title: 'Excepción: ' + jsonRow.cod_excepcion,
                                                        description: 'Existen trabajadores registrados en esta excepción.',
                                                        // position can be left, left-center, left-bottom, top,
                                                        // top-center, top-right, right, right-center, right-bottom,
                                                        // bottom, bottom-center, bottom-right, mid-center
                                                        position: 'left',
                                                    },
                                                    closeBtnText: 'Cerrar',
                                                });
                                            }
                                            else {
                                                MsgBox(r.mensaje, "warning", false);
                                            }
                                            //console.log(r.mensaje);
                                        }
                                    });
                                    //await UIAjax.runFunctionAsync('ajax/tablassoporte', 'DeleteTipoExcepciones', jsonParam, Load.Asistencia.TipoExcepcion.Ready, function () {
                                    //    const driver = new Driver();
                                    //    driver.highlight({
                                    //        element: '#btnColaboradoresTipoExcepcion',
                                    //        popover: {
                                    //            title: 'Excepción: ' + jsonRow.cod_excepcion,
                                    //            description: 'Lista de trabajadores registrados en esta excepción.',
                                    //            // position can be left, left-center, left-bottom, top,
                                    //            // top-center, top-right, right, right-center, right-bottom,
                                    //            // bottom, bottom-center, bottom-right, mid-center
                                    //            position: 'left',
                                    //        }
                                    //    });
                                    //});
                                });
                            });
                            $('#btnColaboradoresTipoExcepcion').off('click').on('click', function () {
                                if (driver.isActivated) {
                                    driver.reset();
                                }
                                let jsonRow = api.row('.selected').data();
                                if (UIEmpty(jsonRow)) {
                                    MsgBox('Debe seleccionar un registro primero.', 'warning', true);
                                    return;
                                }
                                let jsonParam = {
                                    CodigoDeExcepcion: jsonRow.cod_excepcion,
                                };
                                let optionsDT = {
                                    TituloDeModal: 'Trabajadores registrados en la excepción: ' + jsonRow.cod_excepcion,
                                    //Debug: true,
                                    info: true,
                                    paging: true,
                                    columns: [
                                        { data: 'cod_anual', title: 'CÓDIGO ANUAL', className: 'text-center' },
                                        { data: 'nombre_empresa', title: 'EMPRESA' },
                                        { data: 'nombre_trabajador', title: 'TRABAJADOR' },
                                        { data: 'programado', title: 'PROGRAMADO', className: 'text-center' },
                                        { data: 'gozado', title: 'GOZADO', className: 'text-center' },
                                        { data: 'pagado', title: 'PAGADO', className: 'text-center' },
                                    ]
                                };
                                UIModal.DataTable('ajax/tablassoporte', 'GetTipoExcepcionesTrabajadores', jsonParam, optionsDT);
                            });
                        },
                        iDisplayLength: 13,
                        dom: 'Bfrtip',
                        buttons: [],
                        columns: [
                            { data: "cod_excepcion", className: 'text-center' },
                            { data: "nombre" },
                            { data: "cadena3" },
                            { data: "tooltip" },
                            _columnCHK("flg_envia_mail"),
                            _columnCHK("flg_con_goce_haber"),
                            {
                                data: "flg_formato", render: function (data, type, row, meta) {
                                    return 'Formato ' + data.replace("0", "");
                                }
                            },
                            { data: "flg_memorandum" },
                            { data: "valor_asumido" },
                            { data: "unidades_minimo" },
                            { data: "flg_unidad_texto" },
                            { data: "tipo_excepcion_texto" },
                            _columnCHK("flg_tardanza"),
                            _columnCHK("flg_hhee"),
                            _columnCHK("flg_permiso"),
                            _columnCHK("flg_compensacion"),
                            _columnCHK("flg_automatico"),
                            { data: "flg_pagadopor_texto" },
                            { data: "cod_excepcion_sim" },
                            { data: "flg_subsidio" },
                            { data: null, defaultContent: '' },//Color
                            { data: "red" },
                            { data: "green" },
                            { data: "blue" },
                            _columnCHK("flg_rrhh"),
                            _columnCHK("flg_jefe"),
                            _columnCHK("flg_jefe2"),
                            _columnCHK("flg_jefe3"),
                            _columnCHK("flg_jefe4"),
                            _columnCHK("flg_trabajador"),
                            _columnCHK("flg_asit_social"),
                            { data: "cadena1" },
                            _columnCHK("flg_horario_registrado"),//Usa Horas Reg. en Exc.
                            { data: "cadena2" },//Código Interface
                            { data: "cadena4" },//AFP
                        ],
                        createdRow: function (row, data, dataIndex) {
                            let tr = $(row);
                            let tdColor = $('td:nth-child(20)', tr);
                            let colorRGB = `rgb(${data.red}, ${data.green}, ${data.blue})`;
                            tdColor.css('background-color', colorRGB);
                        }
                    });
                });
            },
            Modal: async function (jsonValue) {
                UILoadingDefault();
                let jsonParam = {
                    CodigoDeExcepcion: jsonValue.cod_excepcion,
                };
                if (jsonValue.isNew) {
                    jsonValue.red = 0;
                    jsonValue.green = 0;
                    jsonValue.blue = 0;
                    jsonValue.flg_formato = "01";
                    jsonValue.flg_unidad = "D";
                    jsonValue.tipo_excepcion = "A";
                    jsonValue.flg_pagadopor = "E";
                    jsonValue.flg_con_goce_haber = "S";
                    $('#h4TitleMTIEX').html('NUEVA EXCEPCIÓN');
                    $('#txtCodigoExcepcionMTIEX').removeAttr('disabled');
                }
                else {
                    $('#h4TitleMTIEX').html(jsonValue.Title);
                    $('#txtCodigoExcepcionMTIEX').attr('disabled', 'disabled');
                }

                var colorRGB = `rgb(${jsonValue.red}, ${jsonValue.green}, ${jsonValue.blue})`;
                var fnBoolean = function (key) { return jsonValue[key] == "S" ? true : false; };
                $('#txtCodigoExcepcionMTIEX').val(jsonValue.cod_excepcion);
                $('#txtExcepcionMTIEX').val(jsonValue.nombre);
                $('#txtAbreviaturaMTIEX').val(jsonValue.cadena3);
                $('#txtMensajeAyudaMTIEX').val(jsonValue.tooltip);
                $('#selectFormatoPapeletaMTIEX').val(jsonValue.flg_formato);
                $('#selectFormatoMemorandumMTIEX').val(jsonValue.flg_memorandum);
                $('#txtMaxMTIEX').val(jsonValue.valor_asumido);
                $('#txtMinMTIEX').val(jsonValue.unidades_minimo);
                $('#selectUnidadMTIEX').val(jsonValue.flg_unidad);
                $('#selectTipoExcepcionMTIEX').val(jsonValue.tipo_excepcion);
                $('#selectAsumidoporMTIEX').val(jsonValue.flg_pagadopor);

                await UIFunctions._setPrivateSelect('ajax/tablassoporte', 'GetTipoExcepcionesExcepcionSimilar', null, false, '#selectExcepcionSimilarMTIEX');
                $('#selectExcepcionSimilarMTIEX').val(UIEmpty(jsonValue.cod_excepcion_sim, '--'));
                await UIFunctions._setPrivateSelect('ajax/tablassoporte', 'GetTipoExcepcionesAFP', null, false, '#selectAFPMTIEX');
                $('#selectAFPMTIEX').val(UIEmpty(jsonValue.cadena4, '--'));

                $('#txtColoresMTIEX').minicolors({ format: 'rgb', theme: 'bootstrap', }).minicolors('value', colorRGB);
                $('#txtCodigoMTIEX').val(jsonValue.cadena1);
                $('#txtCodigoInterfaceMTIEX').val(jsonValue.cadena2);

                $('#chkEMailMTIEX').prop('checked', fnBoolean("flg_envia_mail"));
                $('#chkGoceHaberMTIEX').prop('checked', fnBoolean("flg_con_goce_haber"));
                $('#chkTardanzaMTIEX').prop('checked', fnBoolean("flg_tardanza"));
                $('#chkHHEEMTIEX').prop('checked', fnBoolean("flg_hhee"));
                $('#chkPermisosMTIEX').prop('checked', fnBoolean("flg_permiso"));
                $('#chkCompensacionMTIEX').prop('checked', fnBoolean("flg_compensacion"));
                $('#chkRRHHMTIEX').prop('checked', fnBoolean("flg_rrhh"));
                $('#chkJefe1MTIEX').prop('checked', fnBoolean("flg_jefe"));
                $('#chkJefe2MTIEX').prop('checked', fnBoolean("flg_jefe2"));
                $('#chkJefe3MTIEX').prop('checked', fnBoolean("flg_jefe3"));
                $('#chkJefe4MTIEX').prop('checked', fnBoolean("flg_jefe4"));
                $('#chkTrabajadorMTIEX').prop('checked', fnBoolean("flg_trabajador"));
                $('#chkAsitMTIEX').prop('checked', fnBoolean("flg_asit_social"));
                $('#chkHorarioRegistradoMTIEX').prop('checked', fnBoolean("flg_horario_registrado"));
                $('#chkAutomaticoMTIEX').prop('checked', fnBoolean("flg_automatico"));

                await UIAjax.invokeMethodAsync('ajax/tablassoporte', 'GetTipoExcepcionesTipoPlanilla', jsonParam).then(function (r) {
                    tableTipoExcepcionPlanilla = $('#tableTipoExcepcionPlanilla').DataTable({
                        data: r.data,
                        initComplete: function (settings, json) {
                            var api = this.api();
                            var trs = api.$('tr');
                            $('#btnGuardarMTIEX').off('click').on('click', function (e) {
                                let colorRG = $('#txtColoresMTIEX').minicolors('rgbObject');
                                let jsonParam = {
                                    Accion: null,
                                    CodigoDeExcepcion: null,
                                    Nombre: $('#txtExcepcionMTIEX').val(),
                                    Tooltip: $('#txtMensajeAyudaMTIEX').val(),
                                    FlgPagadoPor: $('#selectAsumidoporMTIEX').val(),
                                    FlgUnidad: $('#selectUnidadMTIEX').val(),
                                    FlgVacaciones: 'N',
                                    CodigoDeTipoExcepcion: $('#selectTipoExcepcionMTIEX').val(),
                                    CodigoDeTipoPago: 'P',
                                    MontoMaximo: $('#txtMaxMTIEX').val(),
                                    Cadena1: $('#txtCodigoMTIEX').val(),
                                    Cadena2: $('#txtCodigoInterfaceMTIEX').val(),
                                    Cadena3: $('#txtAbreviaturaMTIEX').val(),
                                    Cadena4: $('#selectAFPMTIEX').val(),
                                    Red: colorRG.r,
                                    Green: colorRG.g,
                                    Blue: colorRG.b,
                                    FlgSubsidio: 'N',
                                    MontoMinimo: $('#txtMinMTIEX').val(),
                                    FlgRRHH: UIFunctions.getValue.HTML('#chkRRHHMTIEX'),
                                    FlgJefe: UIFunctions.getValue.HTML('#chkJefe1MTIEX'),
                                    FlgJefe2: UIFunctions.getValue.HTML('#chkJefe2MTIEX'),
                                    FlgJefe3: UIFunctions.getValue.HTML('#chkJefe3MTIEX'),
                                    FlgJefe4: UIFunctions.getValue.HTML('#chkJefe4MTIEX'),
                                    FlgAsistentaSocial: UIFunctions.getValue.HTML('#chkAsitMTIEX'),
                                    FlgTardanza: UIFunctions.getValue.HTML('#chkTardanzaMTIEX'),
                                    FlgHHEE: UIFunctions.getValue.HTML('#chkHHEEMTIEX'),
                                    FlgCompensacion: UIFunctions.getValue.HTML('#chkCompensacionMTIEX'),
                                    FlgPermiso: UIFunctions.getValue.HTML('#chkPermisosMTIEX'),
                                    FlgFormato: $('#selectFormatoPapeletaMTIEX').val(),
                                    CodigoAlterno: $('#selectExcepcionSimilarMTIEX').val(),
                                    FlgTrabajador: UIFunctions.getValue.HTML('#chkTrabajadorMTIEX'),
                                    FlgGoceHaber: UIFunctions.getValue.HTML('#chkGoceHaberMTIEX'),
                                    FlgMemorandum: $('#selectFormatoMemorandumMTIEX').val(),
                                    FlgEnviaEmail: UIFunctions.getValue.HTML('#chkEMailMTIEX'),
                                    FlgHorarioRegistrado: UIFunctions.getValue.HTML('#chkHorarioRegistradoMTIEX'),
                                    FlgAutomatico: UIFunctions.getValue.HTML('#chkAutomaticoMTIEX'),
                                    Json: [],
                                };
                                if (jsonValue.isNew) {
                                    jsonParam.Accion = 'N';
                                    jsonParam.CodigoDeExcepcion = $('#txtCodigoExcepcionMTIEX').val();
                                }
                                else {
                                    jsonParam.Accion = 'M';
                                    jsonParam.CodigoDeExcepcion = jsonValue.cod_excepcion;
                                }
                                if (UIEmpty(jsonParam.CodigoDeExcepcion)) {
                                    MsgBox('El código no puede ser vacío.', 'warning', true);
                                    return;
                                }
                                if (UIEmpty(jsonParam.Nombre)) {
                                    MsgBox('El nombre no puede ser vacío.', 'warning', true);
                                    return;
                                }
                                $.each(trs, function (i, v) {
                                    if ($('input[type="checkbox"]', v).is(':checked')) {
                                        let jsonRow = api.row(v).data();
                                        jsonParam.Json.push({
                                            CodigoDeExcepcion: jsonParam.CodigoDeExcepcion,
                                            CodigoDeTipoPlanilla: jsonRow.cod_tipo_planilla,
                                            MontoMaximo: $('input.txtMontoMaximo', v).val(),
                                            MontoMinimo: $('input.txtMontoMinimo', v).val(),
                                        });
                                    }
                                });
                                UIAjax.runFunctionAsync('ajax/tablassoporte', 'PostTipoExcepciones', jsonParam, function () {
                                    Load.Asistencia.TipoExcepcion.Ready();
                                    $('#modalTipoExcepciones').modal('hide');
                                });
                            });
                            $('#modalTipoExcepciones').modal('show');
                            UILoadingDefault();
                        },
                        searching: false,
                        info: false,
                        paging: false,
                        dom: 'Bfrtip',
                        buttons: [],
                        columns: [
                            {
                                data: "c_check", render: function (data, type, row, meta) {
                                    return UIFunctions.Checkbox.HTML2(`chkPlanilla${row.cod_tipo_planilla}MTIEX`, data, null);
                                }, className: 'text-center align-middle'
                            },
                            { data: "cod_tipo_planilla" },
                            { data: "nombre" },
                            {
                                data: "valor_asumido", render: function (data, type, row, meta) {
                                    return `<input type="number" value="${data}" hideButtons class="form-control text-center txtMontoMaximo" />`;
                                }, className: 'w-100px text-center'
                            },
                            {
                                data: "unidades_minimo", render: function (data, type, row, meta) {
                                    return `<input type="number" value="${data}" hideButtons class="form-control text-center txtMontoMinimo" />`;
                                }, className: 'w-100px text-center'
                            },
                        ],
                    });
                });
            }
        };
        Load.Asistencia.FormatoMemorandum = {
            Ready: async function () {
                Load.ShowCard('#cardFormatoMemorandum');
                UIDataTable.setEmpty('#tableFormatoMemorandum');
                await UIAjax.invokeMethodAsync('ajax/tablassoporte', 'GetAllFormatoMemorandum',).then(function (r) {
                    tableFormatoMemorandum = $('#tableFormatoMemorandum').DataTable({
                        data: r.data,
                        initComplete: function (settings, json) {
                            UIDataTable.SelectRows(this, false, false, Load.Asistencia.FormatoMemorandum.Modal);
                            UIDataTable.AJAXDelete(this, '#btnEliminarFormatoMemorandum', 'cod_reporte', 'ajax/tablassoporte', 'DeleteFormatoMemorandum', {
                                CodigoDeReporte: 'cod_reporte',
                            }, Load.Asistencia.FormatoMemorandum.Ready);
                        },
                        columns: [
                            { data: "id_aplicacion_texto", className: 'text-center' },
                            { data: "cod_reporte" },
                            { data: "nombre" },
                        ],
                        createdRow: function (row, data, dataIndex) {
                            //let tr = $(row);
                            //let tdColor = $('td:nth-child(20)', tr);
                            //let colorRGB = `rgb(${data.red}, ${data.green}, ${data.blue})`;
                            //tdColor.css('background-color', colorRGB);
                        }
                    });
                });
            },
            Modal: async function (jsonValue) {
                await UILoadingDefault("show");
                if (jsonValue.isNew) {
                    $('#h4TitleMFOME').html('NUEVO FORMATO MEMORÁNDUM');
                    $('#txtCodigoMFOME').removeAttr('disabled');
                }
                else {
                    $('#h4TitleMFOME').html('FORMATO MEMORÁNDUM');
                    $('#txtCodigoMFOME').attr('disabled', 'disabled');
                }

                var fnBoolean = function (key) { return jsonValue[key] == "S" ? true : false; };
                $('#txtCodigoMFOME').val(jsonValue.cod_reporte);
                $('#txtNombreMFOME').val(jsonValue.nombre);

                $('#btnGuardarMFOME').off('click').on('click', function (e) {
                    let jsonParam = {
                        Accion: null,
                        CodigoDeReporte: null,
                        Nombre: $('#txtNombreMFOME').val(),
                    };
                    if (jsonValue.isNew) {
                        jsonParam.Accion = 'N';
                        jsonParam.CodigoDeReporte = $('#txtCodigoMFOME').val();
                    }
                    else {
                        jsonParam.Accion = 'M';
                        jsonParam.CodigoDeReporte = jsonValue.cod_reporte;
                    }
                    if (UIEmpty(jsonParam.CodigoDeReporte)) {
                        UIMensaje.CODIGO();
                        return;
                    }
                    if (UIEmpty(jsonParam.Nombre)) {
                        UIMensaje.NOMBRE();
                        return;
                    }
                    UIAjax.runFunctionAsync('ajax/tablassoporte', 'PostFormatoMemorandum', jsonParam, function () {
                        Load.Asistencia.FormatoMemorandum.Ready();
                        $('#modalFormatoMemorandum').modal('hide');
                    });
                });
                $('#modalFormatoMemorandum').modal('show');
                UILoadingDefault();
            }
        };
        let jsonParam = {
            Flg: '',
        };
    }
    else {
        console.warn("No se encontró la propiedad 'Asistencia' en la variable 'Load'");
    }
}
else {
    console.warn("No se encontró la variable 'Load'");
}