if (typeof Load != undefined) {
    if (typeof Load.Personal != undefined) {
        Load.Personal.AlcanceCargo = {
            Ready: async function () {
                Load.ShowCard('#cardAlcanceCargo');
                await UIAjax.invokeMethodAsync('ajax/tablassoporte', 'GetAllAlcanceCargo', {}).then(function (r) {
                    tableAlcanceCargo = $('#tableAlcanceCargo').DataTable({
                        data: r.data,
                        initComplete: function (settings, json) {
                            UIDataTable.SelectRows(this, false, false, Load.Personal.AlcanceCargo.Modal);
                            UIDataTable.AJAXDelete(this, '#btnEliminarAlcanceCargo', 'cod_alcance', 'ajax/tablassoporte', 'DeleteAlcanceCargo', {
                                CodigoAlcance: 'cod_alcance',
                            }, Load.Personal.AlcanceCargo.Ready);
                        },
                        dom: 'Bfrtip',
                        buttons: [],
                        columnDefs: [
                            { targets: [1], className: 'text-left' },
                            { targets: '_all', className: 'text-center' },
                        ],
                        columns: [
                            { data: "cod_alcance" },
                            { data: "descripcion" },
                            {
                                data: "flg_firma", render: function (data, type, row, meta) {
                                    return UIFunctions.Checkbox.HTML2(null, data, null, true);
                                }, className: 'text-center align-middle'
                            },
                            { data: "monto1" },
                            { data: "monto2" },
                            { data: "monto3" },
                            { data: "monto4" },
                            { data: "monto5" },
                            { data: "cadena1" },
                            { data: "cadena2" },
                            { data: "cadena3" },
                            { data: "cadena4" },
                            { data: "cadena5" },
                            { data: "fecha1_texto" },
                            { data: "fecha2_texto" },
                            { data: "fecha3_texto" },
                            { data: "fecha4_texto" },
                            { data: "fecha5_texto" },
                        ],
                    });
                });
            },
            Modal: async function (jsonValue) {
                await UILoadingDefault("show");
                if (jsonValue.isNew) {
                    $('#h4TitleMALCA').html('NUEVO ALCANCE DEL CARGO');
                    $('#txtCodigoMALCA').removeAttr('disabled');
                }
                else {
                    $('#h4TitleMALCA').html(`ALCANDE DEL CARGO: ${jsonValue.cod_alcance}`);
                    $('#txtCodigoMALCA').attr('disabled', 'disabled');
                }

                var fnBoolean = function (key) { return jsonValue[key] == "S" ? true : false; };
                UIDatePicker.Init('input.txtFechaMALCA');
                UIAutoNumeric.Init('input.txtMontoMALCA');

                $('#txtCodigoMALCA').val(jsonValue.cod_alcance);
                $('#txtDescripcionMALCA').val(jsonValue.descripcion);
                $('#chkFirmaMALCA').prop('checked', fnBoolean("flg_firma"));

                $('#txtMonto1MALCA').autoNumeric('set', UIFloat2(jsonValue.monto1));
                $('#txtMonto2MALCA').autoNumeric('set', UIFloat2(jsonValue.monto2));
                $('#txtMonto3MALCA').autoNumeric('set', UIFloat2(jsonValue.monto3));
                $('#txtMonto4MALCA').autoNumeric('set', UIFloat2(jsonValue.monto4));
                $('#txtMonto5MALCA').autoNumeric('set', UIFloat2(jsonValue.monto5));
                $('#txtCadena1MALCA').val(jsonValue.cadena1);
                $('#txtCadena2MALCA').val(jsonValue.cadena2);
                $('#txtCadena3MALCA').val(jsonValue.cadena3);
                $('#txtCadena4MALCA').val(jsonValue.cadena4);
                $('#txtCadena5MALCA').val(jsonValue.cadena5);
                $('#txtFecha1MALCA').datepicker('setDate', jsonValue.fecha1_texto);
                $('#txtFecha2MALCA').datepicker('setDate', jsonValue.fecha2_texto);
                $('#txtFecha3MALCA').datepicker('setDate', jsonValue.fecha3_texto);
                $('#txtFecha4MALCA').datepicker('setDate', jsonValue.fecha4_texto);
                $('#txtFecha5MALCA').datepicker('setDate', jsonValue.fecha5_texto);

                $('#btnGuardarMALCA').off('click').on('click', function (e) {
                    var jsonParam = {
                        Accion: null,
                        CodigoAlcance: null,
                        Descripcion: $('#txtDescripcionMALCA').val(),
                        FlgFirma: UIFunctions.getValue.HTML('#chkFirmaMALCA'),
                        Monto1: $('#txtMonto1MALCA').val(),
                        Monto2: $('#txtMonto2MALCA').val(),
                        Monto3: $('#txtMonto3MALCA').val(),
                        Monto4: $('#txtMonto4MALCA').val(),
                        Monto5: $('#txtMonto5MALCA').val(),
                        Cadena1: $('#txtCadena1MALCA').val(),
                        Cadena2: $('#txtCadena2MALCA').val(),
                        Cadena3: $('#txtCadena3MALCA').val(),
                        Cadena4: $('#txtCadena4MALCA').val(),
                        Cadena5: $('#txtCadena5MALCA').val(),
                        Fecha1: $('#txtFecha1MALCA').val(),
                        Fecha2: $('#txtFecha2MALCA').val(),
                        Fecha3: $('#txtFecha3MALCA').val(),
                        Fecha4: $('#txtFecha4MALCA').val(),
                        Fecha5: $('#txtFecha5MALCA').val(),
                    };
                    if (jsonValue.isNew) {
                        jsonParam.Accion = 'N';
                        jsonParam.CodigoAlcance = $('#txtCodigoMALCA').val();
                    }
                    else {
                        jsonParam.Accion = 'M';
                        jsonParam.CodigoAlcance = jsonValue.cod_alcance;
                    }
                    if (UIEmpty(jsonParam.CodigoAlcance)) {
                        MsgBox('El código no puede ser vacío.', 'warning', true);
                        return;
                    }
                    if (UIEmpty(jsonParam.Descripcion)) {
                        MsgBox('La descripción no puede ser vacía.', 'warning', true);
                        return;
                    }
                    UIAjax.runFunctionAsync('ajax/tablassoporte', 'PostAlcanceCargo', jsonParam, function () {
                        Load.Personal.AlcanceCargo.Ready();
                        $('#modalAlcanceCargo').modal('hide');
                    });
                });

                $('#modalAlcanceCargo').modal('show');
                UILoadingDefault();
            }
        };
        Load.Personal.Cargos = {
            Ready: async function () {
                Load.ShowCard('#cardCargos');
                await UIAjax.invokeMethodAsync('ajax/tablassoporte', 'GetAllCargo', {}).then(function (r) {
                    tableCargoPECA = $('#tableCargoPECA').DataTable({
                        data: r.data,
                        initComplete: function (settings, json) {
                            var api = this.api();
                            var trs = api.$('tr');
                            trs.on('click', function (e) {
                                let ths = $(this);
                                trs.removeClass('selected');
                                ths.addClass('selected');
                                var jsonRow = Object.assign({}, api.row(this).data());
                                jsonRow.Title = `CARGO: ${jsonRow.cod_cargo}`;
                                Load.Personal.Cargos.Modal(jsonRow);
                            });
                            $('#btnEliminarCargo').off('click').on('click', function (e) {
                                var jsonRow = api.row(api.$('tr.selected')).data();
                                if (UIEmpty(jsonRow)) {
                                    MsgBox('Debe seleccionar un cargo primero.', 'warning', true);
                                    return;
                                }
                                MsgBox(`Se eliminará el código: ${jsonRow.cod_cargo}.`, 'question', false, true, async function () {
                                    var jsonParam = {
                                        CodigoDeCargo: jsonRow.cod_cargo,
                                    };
                                    await UIAjax.runFunctionAsync('ajax/tablassoporte', 'DeleteCargo', jsonParam, function () {
                                        Load.Personal.Cargos.Ready();
                                    });
                                });
                            });

                            var divFilter = $('#tableCargoPECA_filter');
                            var divButtons = $('#tableCargoPECA_wrapper div.dt-buttons');
                            divFilter.css('float', 'left');
                            divButtons.css('float', 'right');
                            //$('div.divPropagar').html('');
                        },
                        //dom: '<"divPropagar">Bfrtip',
                        dom: 'Bfrtip',
                        //buttons: ['csv'],
                        buttons: [],
                        columns: [
                            //{ data: null, defaultContent: '' },
                            { data: "cod_cargo", className: 'text-center' },
                            { data: "nombre" },
                            { data: "causa_objetiva", className: 'text-center' },
                            { data: "cadena1", className: 'text-center' },
                            { data: "cadena2", className: 'text-center' },
                            { data: "cod_alterno", className: 'text-center' },
                            { data: "funciones", className: 'text-center' },
                            { data: "funciones_sec", className: 'text-center' },
                            //{
                            //    data: "neto", render: function (data, type, row, meta) {
                            //        return UIFloat(data);
                            //    }, className: 'text-right'
                            //},
                        ],
                    });
                });
            },
            Modal: async function (jsonValue) {
                $('#txtCodigoMPECA').val(jsonValue.cod_cargo);
                $('#txtNombreMPECA').val(jsonValue.nombre);
                $('#txtCausaObjetivaMPECA').val(jsonValue.causa_objetiva);
                $('#txtCadena1MPECA').val(jsonValue.cadena1);
                $('#txtCadena2MPECA').val(jsonValue.cadena2);
                $('#txtCodigoTRegistroMPECA').val(jsonValue.cod_alterno);
                $('#txtFuncionesPrincipalesMPECA').val(jsonValue.funciones);
                $('#txtFuncionesSecundariasMPECA').val(jsonValue.funciones_sec);
                if (jsonValue.isNew) {
                    $('#h4TitleMPECA').html('NUEVO CARGO');
                    $('#txtCodigoMPECA').removeAttr('disabled');
                }
                else {
                    $('#h4TitleMPECA').html(jsonValue.Title);
                    $('#txtCodigoMPECA').attr('disabled', 'disabled');
                }
                $('#btnGuardarMPECA').off('click').on('click', function (e) {
                    var jsonParam = {
                        Accion: null,
                        CodigoDeCargo: null,
                        Nombre: $('#txtNombreMPECA').val(),
                        Causa: $('#txtCausaObjetivaMPECA').val(),
                        Cadena1: $('#txtCadena1MPECA').val(),
                        Cadena2: $('#txtCadena2MPECA').val(),
                        CodigoAlterno: $('#txtCodigoTRegistroMPECA').val(),
                        FuncionesPrincipales: $('#txtFuncionesPrincipalesMPECA').val(),
                        FuncionesSecundarias: $('#txtFuncionesSecundariasMPECA').val(),
                    };
                    if (jsonValue.isNew) {
                        jsonParam.Accion = 'N';
                        jsonParam.CodigoDeCargo = $('#txtCodigoMPECA').val();
                    }
                    else {
                        jsonParam.Accion = 'M';
                        jsonParam.CodigoDeCargo = jsonValue.cod_cargo;
                    }
                    if (UIEmpty(jsonParam.CodigoDeCargo)) {
                        MsgBox('El código no puede ser vacío.', 'warning', true);
                        return;
                    }
                    if (UIEmpty(jsonParam.Nombre)) {
                        MsgBox('El nombre no puede ser vacío.', 'warning', true);
                        return;
                    }
                    UIAjax.runFunctionAsync('ajax/tablassoporte', 'PostCargo', jsonParam, function () {
                        Load.Personal.Cargos.Ready();
                        $('#modalPECA').modal('hide');
                    });
                });
                $('#modalPECA').modal('show');
            }
        };
        Load.Personal.Categorias = {
            Ready: async function () {
                Load.ShowCard('#cardCategorias');
                await UIAjax.invokeMethodAsync('ajax/tablassoporte', 'GetAllCategorias', {}).then(function (r) {
                    tableCategorias = $('#tableCategorias').DataTable({
                        data: r.data,
                        initComplete: function (settings, json) {
                            UIDataTable.SelectRows(this, false, false, Load.Personal.Categorias.Modal);
                            UIDataTable.AJAXDelete(this, '#btnEliminarCategorias', 'cod_categoria', 'ajax/tablassoporte', 'DeleteCategorias', {
                                CodigoCategoria: 'cod_categoria',
                            }, Load.Personal.Categorias.Ready);
                        },
                        dom: 'Bfrtip',
                        buttons: [],
                        columnDefs: [
                            { targets: [1], className: 'text-left' },
                            { targets: '_all', className: 'text-center' },
                        ],
                        columns: [
                            { data: "cod_categoria" },
                            { data: "nombre" },                            
                            { data: "monto" },
                            { data: "monto_minimo" },
                            { data: "monto_maximo" },
                            { data: "tipo_texto" },
                            { data: "bono1" },
                            { data: "bono2" },
                            { data: "bono3" },
                            { data: "bono4" },
                            { data: "bono5" },
                            {
                                data: "flg_activo", render: function (data, type, row, meta) {
                                    return UIFunctions.Checkbox.HTML2(null, data, null, true);
                                }, className: 'text-center align-middle'
                            },
                        ],
                    });
                });
            },
            Modal: async function (jsonValue) {
                UILoadingDefault();
                if (jsonValue.isNew) {
                    jsonValue.tipo = 'S';
                    $('#h4TitleMCATE').html('NUEVA CATEGORÍA');
                    $('#txtCodigoMCATE').removeAttr('disabled');
                }
                else {
                    $('#h4TitleMCATE').html(`CATEGORÍA: ${jsonValue.cod_categoria}`);
                    $('#txtCodigoMCATE').attr('disabled', 'disabled');
                }

                var fnBoolean = function (key) { return jsonValue[key] == "S" ? true : false; };
                UIAutoNumeric.Init('input.txtMontoMCATE, input.txtBonoMCATE');

                $('#txtCodigoMCATE').val(jsonValue.cod_categoria);
                $('#txtNombreMCATE').val(jsonValue.nombre);                

                $('#txtMontoMCATE').autoNumeric('set', UIFloat2(jsonValue.monto));
                $('#txtMontoMinimoMCATE').autoNumeric('set', UIFloat2(jsonValue.monto_minimo));
                $('#txtMontoMaximoMCATE').autoNumeric('set', UIFloat2(jsonValue.monto_maximo));
                $('#selectTipoMCATE').val(jsonValue.tipo);
                $('#txtBono1MCATE').autoNumeric('set', UIFloat2(jsonValue.bono1));
                $('#txtBono2MCATE').autoNumeric('set', UIFloat2(jsonValue.bono2));
                $('#txtBono3MCATE').autoNumeric('set', UIFloat2(jsonValue.bono3));
                $('#txtBono4MCATE').autoNumeric('set', UIFloat2(jsonValue.bono4));
                $('#txtBono5MCATE').autoNumeric('set', UIFloat2(jsonValue.bono5));
                $('#chkActivoMCATE').prop('checked', fnBoolean("flg_activo"));

                $('#btnGuardarMCATE').off('click').on('click', function (e) {
                    var jsonParam = {
                        Accion: null,
                        CodigoCategoria: null,
                        Nombre: $('#txtNombreMCATE').val(),
                        Monto: $('#txtMontoMCATE').val(),
                        MontoMaximo: $('#txtMontoMaximoMCATE').val(),
                        MontoMinimo: $('#txtMontoMinimoMCATE').val(),
                        Tipo: $('#selectTipoMCATE').val(),
                        Bono1: $('#txtBono1MCATE').val(),
                        Bono2: $('#txtBono2MCATE').val(),
                        Bono3: $('#txtBono3MCATE').val(),
                        Bono4: $('#txtBono4MCATE').val(),
                        Bono5: $('#txtBono5MCATE').val(),
                        FlgActivo: UIFunctions.getValue.HTML('#chkActivoMCATE'),
                    };
                    if (jsonValue.isNew) {
                        jsonParam.Accion = 'N';
                        jsonParam.CodigoCategoria = $('#txtCodigoMCATE').val();
                    }
                    else {
                        jsonParam.Accion = 'M';
                        jsonParam.CodigoCategoria = jsonValue.cod_categoria;
                    }
                    if (UIEmpty(jsonParam.CodigoCategoria)) {
                        UIMensaje.CODIGO();
                        return;
                    }
                    if (UIEmpty(jsonParam.Nombre)) {
                        UIMensaje.NOMBRE();
                        return;
                    }
                    UIAjax.runFunctionAsync('ajax/tablassoporte', 'PostCategorias', jsonParam, function () {
                        Load.Personal.Categorias.Ready();
                        $('#modalCategorias').modal('hide');
                    });
                });

                $('#modalCategorias').modal('show');
                UILoadingDefault();
            }
        };
        Load.Personal.ControlDocumentos = {
            Ready: async function () {
                Load.ShowCard('#cardControlDocumentos');
                await UIAjax.invokeMethodAsync('ajax/tablassoporte', 'GetAllControlDocumentos', {}).then(function (r) {
                    tableControlDocumentos = $('#tableControlDocumentos').DataTable({
                        data: r.data,
                        initComplete: function (settings, json) {
                            UIDataTable.SelectRows(this, false, false, Load.Personal.ControlDocumentos.Modal);
                            UIDataTable.AJAXDelete(this, '#btnEliminarControlDocumentos', 'cod_control', 'ajax/tablassoporte', 'DeleteControlDocumentos', {
                                CodigoControl: 'cod_control',
                            }, Load.Personal.ControlDocumentos.Ready);
                        },
                        dom: 'Bfrtip',
                        buttons: [],
                        columnDefs: [
                            { targets: [1], className: 'text-left' },
                            { targets: '_all', className: 'text-center' },
                        ],
                        columns: [
                            { data: "cod_control" },
                            { data: "nombre" },
                            {
                                data: "flg_obligatorio", render: function (data, type, row, meta) {
                                    return UIFunctions.Checkbox.HTML2(null, data, null, true);
                                }, className: 'text-center align-middle'
                            },
                        ],
                    });
                });
            },
            Modal: async function (jsonValue) {
                UILoadingDefault();
                if (jsonValue.isNew) {
                    $('#h4TitleMCODO').html('NUEVO CONTROL DE DOCUMENTO');
                    $('#txtCodigoMCODO').removeAttr('disabled');
                }
                else {
                    $('#h4TitleMCODO').html(`CONTROL DE DOCUMENTO: ${jsonValue.cod_control}`);
                    $('#txtCodigoMCODO').attr('disabled', 'disabled');
                }

                var fnBoolean = function (key) { return jsonValue[key] == "S" ? true : false; };

                $('#txtCodigoMCODO').val(jsonValue.cod_control);
                $('#txtNombreMCODO').val(jsonValue.nombre);
                $('#chkObligatorioMCODO').prop('checked', fnBoolean("flg_obligatorio"));

                $('#btnGuardarMCODO').off('click').on('click', function (e) {
                    var jsonParam = {
                        Accion: null,
                        CodigoControl: null,
                        Nombre: $('#txtNombreMCODO').val(),
                        FlgObligatorio: UIFunctions.getValue.HTML('#chkObligatorioMCODO'),
                    };
                    if (jsonValue.isNew) {
                        jsonParam.Accion = 'N';
                        jsonParam.CodigoControl = $('#txtCodigoMCODO').val();
                    }
                    else {
                        jsonParam.Accion = 'M';
                        jsonParam.CodigoControl = jsonValue.cod_control;
                    }
                    if (UIEmpty(jsonParam.CodigoControl)) {
                        UIMensaje.CODIGO();
                        return;
                    }
                    if (UIEmpty(jsonParam.Nombre)) {
                        UIMensaje.NOMBRE();
                        return;
                    }
                    UIAjax.runFunctionAsync('ajax/tablassoporte', 'PostControlDocumentos', jsonParam, function () {
                        Load.Personal.ControlDocumentos.Ready();
                        $('#modalControlDocumentos').modal('hide');
                    });
                });

                $('#modalControlDocumentos').modal('show');
                UILoadingDefault();
            }
        };

        Load.Personal.Actividades = {
            Ready: async function () {
                Load.ShowCard('#cardActividades');
                await UIAjax.invokeMethodAsync('ajax/tablassoporte', 'GetAllActividades', {}).then(function (r) {
                    tableActividades = $('#tableActividades').DataTable({
                        data: r.data,
                        initComplete: function (settings, json) {
                            UIDataTable.SelectRows(this, false, false, Load.Personal.Actividades.Modal);
                            UIDataTable.AJAXDelete(this, '#btnEliminarActividades', 'cod_act', 'ajax/tablassoporte', 'DeleteActividades', {
                                CodigoActividad: 'cod_act',
                            }, Load.Personal.Actividades.Ready);
                        },
                        dom: 'Bfrtip',
                        buttons: [],
                        columnDefs: [
                            { targets: [1], className: 'text-left' },
                            { targets: '_all', className: 'text-center' },
                        ],
                        columns: [
                            { data: "cod_act" },
                            { data: "descripcion" },
                        ],
                    });
                });
            },
            Modal: async function (jsonValue) {
                UILoadingDefault();
                if (jsonValue.isNew) {
                    $('#h4TitleMACTI').html('NUEVA ACTIVIDAD');
                    $('#txtCodigoMACTI').removeAttr('disabled');
                }
                else {
                    $('#h4TitleMACTI').html(`ACTIVIDAD: ${jsonValue.cod_act}`);
                    $('#txtCodigoMACTI').attr('disabled', 'disabled');
                }

                var fnBoolean = function (key) { return jsonValue[key] == "S" ? true : false; };

                $('#txtCodigoMACTI').val(jsonValue.cod_act);
                $('#txtDescripcionMACTI').val(jsonValue.descripcion);

                $('#btnGuardarMACTI').off('click').on('click', function (e) {
                    var jsonParam = {
                        Accion: null,
                        CodigoActividad: null,
                        Descripcion: $('#txtDescripcionMACTI').val(),
                    };
                    if (jsonValue.isNew) {
                        jsonParam.Accion = 'N';
                        jsonParam.CodigoActividad = $('#txtCodigoMACTI').val();
                    }
                    else {
                        jsonParam.Accion = 'M';
                        jsonParam.CodigoActividad = jsonValue.cod_act;
                    }
                    if (UIEmpty(jsonParam.CodigoActividad)) {
                        UIMensaje.CODIGO();
                        return;
                    }
                    if (UIEmpty(jsonParam.Descripcion)) {
                        UIMensaje.DESCRIPCION();
                        return;
                    }
                    UIAjax.runFunctionAsync('ajax/tablassoporte', 'PostActividades', jsonParam, function () {
                        Load.Personal.Actividades.Ready();
                        $('#modalActividades').modal('hide');
                    });
                });

                $('#modalActividades').modal('show');
                UILoadingDefault();
            }
        };
        Load.Personal.EstructuraEducativa = {
            Ready: async function () {
                Load.ShowCard('#cardEstructuraEducativa');
            },
        };
        Load.Personal.ModalidadFormativa = {
            Ready: async function () {
                Load.ShowCard('#cardModalidadFormativa');
                await UIAjax.invokeMethodAsync('ajax/tablassoporte', 'GetAllModalidadFormativa', {}).then(function (r) {
                    tableModalidadFormativa = $('#tableModalidadFormativa').DataTable({
                        data: r.data,
                        initComplete: function (settings, json) {
                            UIDataTable.SelectRows(this, false, false, Load.Personal.ModalidadFormativa.Modal);
                            UIDataTable.AJAXDelete(this, '#btnEliminarModalidadFormativa', 'cod_tipo_modalidad', 'ajax/tablassoporte', 'DeleteModalidadFormativa', {
                                CodigoTipoModalidad: 'cod_tipo_modalidad',
                            }, Load.Personal.ModalidadFormativa.Ready);
                        },
                        dom: 'Bfrtip',
                        buttons: [],
                        columnDefs: [
                            { targets: [1], className: 'text-left' },
                            { targets: '_all', className: 'text-center' },
                        ],
                        columns: [
                            { data: "cod_tipo_modalidad" },
                            { data: "modalidad_formativa" },
                            { data: "cadena" },
                        ],
                    });
                });
            },
            Modal: async function (jsonValue) {
                UILoadingDefault();
                if (jsonValue.isNew) {
                    $('#h4TitleMMOFO').html('NUEVA ACTIVIDAD');
                    $('#txtCodigoMMOFO').removeAttr('disabled');
                }
                else {
                    $('#h4TitleMMOFO').html(`ACTIVIDAD: ${jsonValue.cod_tipo_modalidad}`);
                    $('#txtCodigoMMOFO').attr('disabled', 'disabled');
                }

                var fnBoolean = function (key) { return jsonValue[key] == "S" ? true : false; };

                $('#txtCodigoMMOFO').val(jsonValue.cod_tipo_modalidad);
                $('#txtModalidadMMOFO').val(jsonValue.modalidad_formativa);
                $('#txtCadenaMMOFO').val(jsonValue.cadena);

                $('#btnGuardarMMOFO').off('click').on('click', function (e) {
                    var jsonParam = {
                        Accion: null,
                        CodigoTipoModalidad: null,
                        ModalidadFormativa: $('#txtModalidadMMOFO').val(),
                        Cadena: $('#txtCadenaMMOFO').val(),
                    };
                    if (jsonValue.isNew) {
                        jsonParam.Accion = 'N';
                        jsonParam.CodigoTipoModalidad = $('#txtCodigoMMOFO').val();
                    }
                    else {
                        jsonParam.Accion = 'M';
                        jsonParam.CodigoTipoModalidad = jsonValue.cod_tipo_modalidad;
                    }
                    if (UIEmpty(jsonParam.CodigoTipoModalidad)) {
                        UIMensaje.CODIGO();
                        return;
                    }
                    if (UIEmpty(jsonParam.ModalidadFormativa)) {
                        MsgBox("La Modalidad no puede ser vacía.", "warning", false);
                        return;
                    }
                    UIAjax.runFunctionAsync('ajax/tablassoporte', 'PostModalidadFormativa', jsonParam, function () {
                        Load.Personal.ModalidadFormativa.Ready();
                        $('#modalModalidadFormativa').modal('hide');
                    });
                });

                $('#modalModalidadFormativa').modal('show');
                UILoadingDefault();
            }
        };
        Load.Personal.TipoTrabajador = {
            Ready: async function () {
                Load.ShowCard('#cardTipoTrabajador');
                await UIAjax.invokeMethodAsync('ajax/tablassoporte', 'GetAllTipoTrabajador', {}).then(function (r) {
                    tableTipoTrabajador = $('#tableTipoTrabajador').DataTable({
                        data: r.data,
                        initComplete: function (settings, json) {
                            UIDataTable.SelectRows(this, false, false, Load.Personal.TipoTrabajador.Modal);
                            UIDataTable.AJAXDelete(this, '#btnEliminarTipoTrabajador', 'cod_tipo_trabajador', 'ajax/tablassoporte', 'DeleteTipoTrabajador', {
                                CodigoTipoTrabajador: 'cod_tipo_trabajador',
                            }, Load.Personal.TipoTrabajador.Ready);
                        },
                        columnDefs: [
                            { targets: [1], className: 'text-left' },
                            { targets: '_all', className: 'text-center' },
                        ],
                        columns: [
                            { data: "cod_tipo_trabajador" },
                            { data: "nombre" },
                            { data: "monto1" },
                            { data: "monto2" },
                            { data: "monto3" },
                            { data: "monto4" },
                            { data: "monto5" },
                            { data: "cadena1" },
                            { data: "cadena2" },
                            { data: "cadena3" },
                            { data: "cadena4" },
                            { data: "cadena5" },
                            { data: "fecha1_texto" },
                            { data: "fecha2_texto" },
                            { data: "fecha3_texto" },
                            { data: "fecha4_texto" },
                            { data: "fecha5_texto" },
                        ],
                    });
                });
            },
            Modal: async function (jsonValue) {
                await UILoadingDefault("show");
                if (jsonValue.isNew) {
                    $('#h4TitleMTITR').html('NUEVO TIPO TRABAJADOR');
                    $('#txtCodigoMTITR').removeAttr('disabled');
                }
                else {
                    $('#h4TitleMTITR').html(`TIPO TRABAJADOR: ${jsonValue.cod_tipo_trabajador}`);
                    $('#txtCodigoMTITR').attr('disabled', 'disabled');
                }

                var fnBoolean = function (key) { return jsonValue[key] == "S" ? true : false; };
                UIDatePicker.Init('input.txtFechaMTITR');
                UIAutoNumeric.Init('input.txtMontoMTITR');

                $('#txtCodigoMTITR').val(jsonValue.cod_tipo_trabajador);
                $('#txtNombreMTITR').val(jsonValue.nombre);

                $('#txtMonto1MTITR').autoNumeric('set', UIFloat2(jsonValue.monto1));
                $('#txtMonto2MTITR').autoNumeric('set', UIFloat2(jsonValue.monto2));
                $('#txtMonto3MTITR').autoNumeric('set', UIFloat2(jsonValue.monto3));
                $('#txtMonto4MTITR').autoNumeric('set', UIFloat2(jsonValue.monto4));
                $('#txtMonto5MTITR').autoNumeric('set', UIFloat2(jsonValue.monto5));
                $('#txtCadena1MTITR').val(jsonValue.cadena1);
                $('#txtCadena2MTITR').val(jsonValue.cadena2);
                $('#txtCadena3MTITR').val(jsonValue.cadena3);
                $('#txtCadena4MTITR').val(jsonValue.cadena4);
                $('#txtCadena5MTITR').val(jsonValue.cadena5);
                $('#txtFecha1MTITR').datepicker('setDate', jsonValue.fecha1_texto);
                $('#txtFecha2MTITR').datepicker('setDate', jsonValue.fecha2_texto);
                $('#txtFecha3MTITR').datepicker('setDate', jsonValue.fecha3_texto);
                $('#txtFecha4MTITR').datepicker('setDate', jsonValue.fecha4_texto);
                $('#txtFecha5MTITR').datepicker('setDate', jsonValue.fecha5_texto);

                $('#btnGuardarMTITR').off('click').on('click', function (e) {
                    var jsonParam = {
                        Accion: null,
                        CodigoTipoTrabajador: null,
                        Nombre: $('#txtNombreMTITR').val(),
                        Monto1: $('#txtMonto1MTITR').val(),
                        Monto2: $('#txtMonto2MTITR').val(),
                        Monto3: $('#txtMonto3MTITR').val(),
                        Monto4: $('#txtMonto4MTITR').val(),
                        Monto5: $('#txtMonto5MTITR').val(),
                        Cadena1: $('#txtCadena1MTITR').val(),
                        Cadena2: $('#txtCadena2MTITR').val(),
                        Cadena3: $('#txtCadena3MTITR').val(),
                        Cadena4: $('#txtCadena4MTITR').val(),
                        Cadena5: $('#txtCadena5MTITR').val(),
                        Fecha1: $('#txtFecha1MTITR').val(),
                        Fecha2: $('#txtFecha2MTITR').val(),
                        Fecha3: $('#txtFecha3MTITR').val(),
                        Fecha4: $('#txtFecha4MTITR').val(),
                        Fecha5: $('#txtFecha5MTITR').val(),
                    };
                    if (jsonValue.isNew) {
                        jsonParam.Accion = 'N';
                        jsonParam.CodigoTipoTrabajador = $('#txtCodigoMTITR').val();
                    }
                    else {
                        jsonParam.Accion = 'M';
                        jsonParam.CodigoTipoTrabajador = jsonValue.cod_tipo_trabajador;
                    }
                    if (UIEmpty(jsonParam.CodigoTipoTrabajador)) {
                        MsgBox('El código no puede ser vacío.', 'warning', true);
                        return;
                    }
                    if (UIEmpty(jsonParam.Nombre)) {
                        MsgBox('El nombre no puede ser vacía.', 'warning', true);
                        return;
                    }
                    UIAjax.runFunctionAsync('ajax/tablassoporte', 'PostTipoTrabajador', jsonParam, function () {
                        Load.Personal.TipoTrabajador.Ready();
                        $('#modalTipoTrabajador').modal('hide');
                    });
                });

                $('#modalTipoTrabajador').modal('show');
                UILoadingDefault();
            }
        };
        Load.Personal.TipoDocumento = {
            Ready: async function () {
                Load.ShowCard('#cardTipoDocumento');
                await UIAjax.invokeMethodAsync('ajax/tablassoporte', 'GetAllTipoDocumento', {}).then(function (r) {
                    tableTipoDocumento = $('#tableTipoDocumento').DataTable({
                        data: r.data,
                        initComplete: function (settings, json) {
                            UIDataTable.SelectRows(this, false, false, Load.Personal.TipoDocumento.Modal);
                            UIDataTable.AJAXDelete(this, '#btnEliminarTipoDocumento', 'cod_docid', 'ajax/tablassoporte', 'DeleteTipoDocumento', {
                                CodigoTipoDocumento: 'cod_docid',
                            }, Load.Personal.TipoDocumento.Ready);
                        },
                        columnDefs: [
                            { targets: [1], className: 'text-left' },
                            { targets: '_all', className: 'text-center' },
                        ],
                        columns: [
                            { data: "cod_docid" },
                            { data: "nombre" },
                            { data: "monto1" },
                            { data: "monto2" },
                            { data: "monto3" },
                            { data: "monto4" },
                            { data: "monto5" },
                            { data: "cadena1" },
                            { data: "cadena2" },
                            { data: "cadena3" },
                            { data: "cadena4" },
                            { data: "cadena5" },
                            { data: "fecha1_texto" },
                            { data: "fecha2_texto" },
                            { data: "fecha3_texto" },
                            { data: "fecha4_texto" },
                            { data: "fecha5_texto" },
                        ],
                    });
                });
            },
            Modal: async function (jsonValue) {
                await UILoadingDefault("show");
                if (jsonValue.isNew) {
                    $('#h4TitleMTIDO').html('NUEVO TIPO TRABAJADOR');
                    $('#txtCodigoMTIDO').removeAttr('disabled');
                }
                else {
                    $('#h4TitleMTIDO').html(`TIPO TRABAJADOR: ${jsonValue.cod_docid}`);
                    $('#txtCodigoMTIDO').attr('disabled', 'disabled');
                }

                var fnBoolean = function (key) { return jsonValue[key] == "S" ? true : false; };
                UIDatePicker.Init('input.txtFechaMTIDO');
                UIAutoNumeric.Init('input.txtMontoMTIDO');

                $('#txtCodigoMTIDO').val(jsonValue.cod_docid);
                $('#txtNombreMTIDO').val(jsonValue.nombre);

                $('#txtMonto1MTIDO').autoNumeric('set', UIFloat2(jsonValue.monto1));
                $('#txtMonto2MTIDO').autoNumeric('set', UIFloat2(jsonValue.monto2));
                $('#txtMonto3MTIDO').autoNumeric('set', UIFloat2(jsonValue.monto3));
                $('#txtMonto4MTIDO').autoNumeric('set', UIFloat2(jsonValue.monto4));
                $('#txtMonto5MTIDO').autoNumeric('set', UIFloat2(jsonValue.monto5));
                $('#txtCadena1MTIDO').val(jsonValue.cadena1);
                $('#txtCadena2MTIDO').val(jsonValue.cadena2);
                $('#txtCadena3MTIDO').val(jsonValue.cadena3);
                $('#txtCadena4MTIDO').val(jsonValue.cadena4);
                $('#txtCadena5MTIDO').val(jsonValue.cadena5);
                $('#txtFecha1MTIDO').datepicker('setDate', jsonValue.fecha1_texto);
                $('#txtFecha2MTIDO').datepicker('setDate', jsonValue.fecha2_texto);
                $('#txtFecha3MTIDO').datepicker('setDate', jsonValue.fecha3_texto);
                $('#txtFecha4MTIDO').datepicker('setDate', jsonValue.fecha4_texto);
                $('#txtFecha5MTIDO').datepicker('setDate', jsonValue.fecha5_texto);

                $('#btnGuardarMTIDO').off('click').on('click', function (e) {
                    var jsonParam = {
                        Accion: null,
                        CodigoTipoDocumento: null,
                        Nombre: $('#txtNombreMTIDO').val(),
                        Monto1: $('#txtMonto1MTIDO').val(),
                        Monto2: $('#txtMonto2MTIDO').val(),
                        Monto3: $('#txtMonto3MTIDO').val(),
                        Monto4: $('#txtMonto4MTIDO').val(),
                        Monto5: $('#txtMonto5MTIDO').val(),
                        Cadena1: $('#txtCadena1MTIDO').val(),
                        Cadena2: $('#txtCadena2MTIDO').val(),
                        Cadena3: $('#txtCadena3MTIDO').val(),
                        Cadena4: $('#txtCadena4MTIDO').val(),
                        Cadena5: $('#txtCadena5MTIDO').val(),
                        Fecha1: $('#txtFecha1MTIDO').val(),
                        Fecha2: $('#txtFecha2MTIDO').val(),
                        Fecha3: $('#txtFecha3MTIDO').val(),
                        Fecha4: $('#txtFecha4MTIDO').val(),
                        Fecha5: $('#txtFecha5MTIDO').val(),
                    };
                    if (jsonValue.isNew) {
                        jsonParam.Accion = 'N';
                        jsonParam.CodigoTipoDocumento = $('#txtCodigoMTIDO').val();
                    }
                    else {
                        jsonParam.Accion = 'M';
                        jsonParam.CodigoTipoDocumento = jsonValue.cod_docid;
                    }
                    if (UIEmpty(jsonParam.CodigoTipoDocumento)) {
                        UIMensaje.CODIGO();
                        return;
                    }
                    if (UIEmpty(jsonParam.Nombre)) {
                        UIMensaje.NOMBRE();
                        return;
                    }
                    UIAjax.runFunctionAsync('ajax/tablassoporte', 'PostTipoDocumento', jsonParam, function () {
                        Load.Personal.TipoDocumento.Ready();
                        $('#modalTipoDocumento').modal('hide');
                    });
                });
                $('#modalTipoDocumento').modal('show');
                UILoadingDefault();
            }
        };
        Load.Personal.TipoBrevete = {
            Ready: async function () {
                Load.ShowCard('#cardTipoBrevete');
                await UIAjax.invokeMethodAsync('ajax/tablassoporte', 'GetAllTipoBrevete', {}).then(function (r) {
                    tableTipoBrevete = $('#tableTipoBrevete').DataTable({
                        data: r.data,
                        initComplete: function (settings, json) {
                            UIDataTable.SelectRows(this, false, false, Load.Personal.TipoBrevete.Modal);
                            UIDataTable.AJAXDelete(this, '#btnEliminarTipoBrevete', 'cod_brevete', 'ajax/tablassoporte', 'DeleteTipoBrevete', {
                                CodigoTipoBrevete: 'cod_brevete',
                            }, Load.Personal.TipoBrevete.Ready);
                        },
                        columnDefs: [
                            { targets: [1], className: 'text-left' },
                            { targets: '_all', className: 'text-center' },
                        ],
                        columns: [
                            { data: "cod_brevete" },
                            { data: "nombre" },
                            { data: "monto1" },
                            { data: "monto2" },
                            { data: "monto3" },
                            { data: "monto4" },
                            { data: "monto5" },
                            { data: "cadena1" },
                            { data: "cadena2" },
                            { data: "cadena3" },
                            { data: "cadena4" },
                            { data: "cadena5" },
                            { data: "fecha1_texto" },
                            { data: "fecha2_texto" },
                            { data: "fecha3_texto" },
                            { data: "fecha4_texto" },
                            { data: "fecha5_texto" },
                        ],
                    });
                });
            },
            Modal: async function (jsonValue) {
                await UILoadingDefault("show");
                if (jsonValue.isNew) {
                    $('#h4TitleMTIBR').html('NUEVO TIPO BREVE');
                    $('#txtCodigoMTIBR').removeAttr('disabled');
                }
                else {
                    $('#h4TitleMTIBR').html(`TIPO BREVE: ${jsonValue.cod_brevete}`);
                    $('#txtCodigoMTIBR').attr('disabled', 'disabled');
                }

                var fnBoolean = function (key) { return jsonValue[key] == "S" ? true : false; };
                UIDatePicker.Init('input.txtFechaMTIBR');
                UIAutoNumeric.Init('input.txtMontoMTIBR');

                $('#txtCodigoMTIBR').val(jsonValue.cod_brevete);
                $('#txtNombreMTIBR').val(jsonValue.nombre);

                $('#txtMonto1MTIBR').autoNumeric('set', UIFloat2(jsonValue.monto1));
                $('#txtMonto2MTIBR').autoNumeric('set', UIFloat2(jsonValue.monto2));
                $('#txtMonto3MTIBR').autoNumeric('set', UIFloat2(jsonValue.monto3));
                $('#txtMonto4MTIBR').autoNumeric('set', UIFloat2(jsonValue.monto4));
                $('#txtMonto5MTIBR').autoNumeric('set', UIFloat2(jsonValue.monto5));
                $('#txtCadena1MTIBR').val(jsonValue.cadena1);
                $('#txtCadena2MTIBR').val(jsonValue.cadena2);
                $('#txtCadena3MTIBR').val(jsonValue.cadena3);
                $('#txtCadena4MTIBR').val(jsonValue.cadena4);
                $('#txtCadena5MTIBR').val(jsonValue.cadena5);
                $('#txtFecha1MTIBR').datepicker('setDate', jsonValue.fecha1_texto);
                $('#txtFecha2MTIBR').datepicker('setDate', jsonValue.fecha2_texto);
                $('#txtFecha3MTIBR').datepicker('setDate', jsonValue.fecha3_texto);
                $('#txtFecha4MTIBR').datepicker('setDate', jsonValue.fecha4_texto);
                $('#txtFecha5MTIBR').datepicker('setDate', jsonValue.fecha5_texto);

                $('#btnGuardarMTIBR').off('click').on('click', function (e) {
                    var jsonParam = {
                        Accion: null,
                        CodigoTipoBrevete: null,
                        Nombre: $('#txtNombreMTIBR').val(),
                        Monto1: $('#txtMonto1MTIBR').val(),
                        Monto2: $('#txtMonto2MTIBR').val(),
                        Monto3: $('#txtMonto3MTIBR').val(),
                        Monto4: $('#txtMonto4MTIBR').val(),
                        Monto5: $('#txtMonto5MTIBR').val(),
                        Cadena1: $('#txtCadena1MTIBR').val(),
                        Cadena2: $('#txtCadena2MTIBR').val(),
                        Cadena3: $('#txtCadena3MTIBR').val(),
                        Cadena4: $('#txtCadena4MTIBR').val(),
                        Cadena5: $('#txtCadena5MTIBR').val(),
                        Fecha1: $('#txtFecha1MTIBR').val(),
                        Fecha2: $('#txtFecha2MTIBR').val(),
                        Fecha3: $('#txtFecha3MTIBR').val(),
                        Fecha4: $('#txtFecha4MTIBR').val(),
                        Fecha5: $('#txtFecha5MTIBR').val(),
                    };
                    if (jsonValue.isNew) {
                        jsonParam.Accion = 'N';
                        jsonParam.CodigoTipoBrevete = $('#txtCodigoMTIBR').val();
                    }
                    else {
                        jsonParam.Accion = 'M';
                        jsonParam.CodigoTipoBrevete = jsonValue.cod_brevete;
                    }
                    if (UIEmpty(jsonParam.CodigoTipoBrevete)) {
                        MsgBox('El código no puede ser vacío.', 'warning', true);
                        return;
                    }
                    if (UIEmpty(jsonParam.Nombre)) {
                        MsgBox('El nombre no puede ser vacío.', 'warning', true);
                        return;
                    }
                    UIAjax.runFunctionAsync('ajax/tablassoporte', 'PostTipoBrevete', jsonParam, function () {
                        Load.Personal.TipoBrevete.Ready();
                        $('#modalTipoBrevete').modal('hide');
                    });
                });

                $('#modalTipoBrevete').modal('show');
                UILoadingDefault();
            }
        };
        Load.Personal.EstadoLaboral = {
            Ready: async function () {
                Load.ShowCard('#cardEstadoLaboral');
                await UIAjax.invokeMethodAsync('ajax/tablassoporte', 'GetAllEstadoLaboral', {}).then(function (r) {
                    tableEstadoLaboral = $('#tableEstadoLaboral').DataTable({
                        data: r.data,
                        initComplete: function (settings, json) {
                            UIDataTable.SelectRows(this, false, false, Load.Personal.EstadoLaboral.Modal);
                            UIDataTable.AJAXDelete(this, '#btnEliminarEstadoLaboral', 'cod_estado_laboral', 'ajax/tablassoporte', 'DeleteEstadoLaboral', {
                                CodigoEstadoLaboral: 'cod_estado_laboral',
                            }, Load.Personal.EstadoLaboral.Ready);
                        },
                        columnDefs: [
                            { targets: [1], className: 'text-left' },
                            { targets: '_all', className: 'text-center' },
                        ],
                        columns: [
                            { data: "cod_estado_laboral" },
                            { data: "nombre" },
                            { data: "monto1" },
                            { data: "monto2" },
                            { data: "monto3" },
                            { data: "monto4" },
                            { data: "monto5" },
                            { data: "cadena1" },
                            { data: "cadena2" },
                            { data: "cadena3" },
                            { data: "cadena4" },
                            { data: "cadena5" },
                            { data: "fecha1_texto" },
                            { data: "fecha2_texto" },
                            { data: "fecha3_texto" },
                            { data: "fecha4_texto" },
                            { data: "fecha5_texto" },
                        ],
                    });
                });
            },
            Modal: async function (jsonValue) {
                await UILoadingDefault("show");
                if (jsonValue.isNew) {
                    $('#h4TitleMESLA').html('NUEVO ESTADO LABORAL');
                    $('#txtCodigoMESLA').removeAttr('disabled');
                }
                else {
                    $('#h4TitleMESLA').html(`ESTADO LABORAL: ${jsonValue.cod_estado_laboral}`);
                    $('#txtCodigoMESLA').attr('disabled', 'disabled');
                }

                var fnBoolean = function (key) { return jsonValue[key] == "S" ? true : false; };
                UIDatePicker.Init('input.txtFechaMESLA');
                UIAutoNumeric.Init('input.txtMontoMESLA');

                $('#txtCodigoMESLA').val(jsonValue.cod_estado_laboral);
                $('#txtNombreMESLA').val(jsonValue.nombre);

                $('#txtMonto1MESLA').autoNumeric('set', UIFloat2(jsonValue.monto1));
                $('#txtMonto2MESLA').autoNumeric('set', UIFloat2(jsonValue.monto2));
                $('#txtMonto3MESLA').autoNumeric('set', UIFloat2(jsonValue.monto3));
                $('#txtMonto4MESLA').autoNumeric('set', UIFloat2(jsonValue.monto4));
                $('#txtMonto5MESLA').autoNumeric('set', UIFloat2(jsonValue.monto5));
                $('#txtCadena1MESLA').val(jsonValue.cadena1);
                $('#txtCadena2MESLA').val(jsonValue.cadena2);
                $('#txtCadena3MESLA').val(jsonValue.cadena3);
                $('#txtCadena4MESLA').val(jsonValue.cadena4);
                $('#txtCadena5MESLA').val(jsonValue.cadena5);
                $('#txtFecha1MESLA').datepicker('setDate', jsonValue.fecha1_texto);
                $('#txtFecha2MESLA').datepicker('setDate', jsonValue.fecha2_texto);
                $('#txtFecha3MESLA').datepicker('setDate', jsonValue.fecha3_texto);
                $('#txtFecha4MESLA').datepicker('setDate', jsonValue.fecha4_texto);
                $('#txtFecha5MESLA').datepicker('setDate', jsonValue.fecha5_texto);

                $('#btnGuardarMESLA').off('click').on('click', function (e) {
                    var jsonParam = {
                        Accion: null,
                        CodigoEstadoLaboral: null,
                        Nombre: $('#txtNombreMESLA').val(),
                        Monto1: $('#txtMonto1MESLA').val(),
                        Monto2: $('#txtMonto2MESLA').val(),
                        Monto3: $('#txtMonto3MESLA').val(),
                        Monto4: $('#txtMonto4MESLA').val(),
                        Monto5: $('#txtMonto5MESLA').val(),
                        Cadena1: $('#txtCadena1MESLA').val(),
                        Cadena2: $('#txtCadena2MESLA').val(),
                        Cadena3: $('#txtCadena3MESLA').val(),
                        Cadena4: $('#txtCadena4MESLA').val(),
                        Cadena5: $('#txtCadena5MESLA').val(),
                        Fecha1: $('#txtFecha1MESLA').val(),
                        Fecha2: $('#txtFecha2MESLA').val(),
                        Fecha3: $('#txtFecha3MESLA').val(),
                        Fecha4: $('#txtFecha4MESLA').val(),
                        Fecha5: $('#txtFecha5MESLA').val(),
                    };
                    if (jsonValue.isNew) {
                        jsonParam.Accion = 'N';
                        jsonParam.CodigoEstadoLaboral = $('#txtCodigoMESLA').val();
                    }
                    else {
                        jsonParam.Accion = 'M';
                        jsonParam.CodigoEstadoLaboral = jsonValue.cod_estado_laboral;
                    }
                    if (UIEmpty(jsonParam.CodigoEstadoLaboral)) {
                        UIMensaje.CODIGO();
                        return;
                    }
                    if (UIEmpty(jsonParam.Nombre)) {
                        UIMensaje.NOMBRE();
                        return;
                    }
                    UIAjax.runFunctionAsync('ajax/tablassoporte', 'PostEstadoLaboral', jsonParam, function () {
                        Load.Personal.EstadoLaboral.Ready();
                        $('#modalEstadoLaboral').modal('hide');
                    });
                });

                $('#modalEstadoLaboral').modal('show');
                UILoadingDefault();
            }
        };
        Load.Personal.ParentescoFamiliar = {
            Ready: async function () {
                Load.ShowCard('#cardParentescoFamiliar');
                await UIAjax.invokeMethodAsync('ajax/tablassoporte', 'GetAllParentescoFamiliar', {}).then(function (r) {
                    tableParentescoFamiliar = $('#tableParentescoFamiliar').DataTable({
                        data: r.data,
                        initComplete: function (settings, json) {
                            UIDataTable.SelectRows(this, false, false, Load.Personal.ParentescoFamiliar.Modal);
                            UIDataTable.AJAXDelete(this, '#btnEliminarParentescoFamiliar', 'cod_parentesco', 'ajax/tablassoporte', 'DeleteParentescoFamiliar', {
                                CodigoParentescoFamiliar: 'cod_parentesco',
                            }, Load.Personal.ParentescoFamiliar.Ready);
                        },
                        columnDefs: [
                            { targets: [1], className: 'text-left' },
                            { targets: '_all', className: 'text-center' },
                        ],
                        columns: [
                            { data: "cod_parentesco" },
                            { data: "nombre" },
                            { data: "monto1" },
                            { data: "monto2" },
                            { data: "monto3" },
                            { data: "monto4" },
                            { data: "monto5" },
                            { data: "cadena1" },
                            { data: "cadena2" },
                            { data: "cadena3" },
                            { data: "cadena4" },
                            { data: "cadena5" },
                            { data: "fecha1_texto" },
                            { data: "fecha2_texto" },
                            { data: "fecha3_texto" },
                            { data: "fecha4_texto" },
                            { data: "fecha5_texto" },
                        ],
                    });
                });
            },
            Modal: async function (jsonValue) {
                await UILoadingDefault("show");
                if (jsonValue.isNew) {
                    $('#h4TitleMPAFA').html('NUEVO PARENTESCO FAMILIAR');
                    $('#txtCodigoMPAFA').removeAttr('disabled');
                }
                else {
                    $('#h4TitleMPAFA').html(`PARENTESCO FAMILIAR: ${jsonValue.cod_parentesco}`);
                    $('#txtCodigoMPAFA').attr('disabled', 'disabled');
                }

                var fnBoolean = function (key) { return jsonValue[key] == "S" ? true : false; };
                UIDatePicker.Init('input.txtFechaMPAFA');
                UIAutoNumeric.Init('input.txtMontoMPAFA');

                $('#txtCodigoMPAFA').val(jsonValue.cod_parentesco);
                $('#txtNombreMPAFA').val(jsonValue.nombre);

                $('#txtMonto1MPAFA').autoNumeric('set', UIFloat2(jsonValue.monto1));
                $('#txtMonto2MPAFA').autoNumeric('set', UIFloat2(jsonValue.monto2));
                $('#txtMonto3MPAFA').autoNumeric('set', UIFloat2(jsonValue.monto3));
                $('#txtMonto4MPAFA').autoNumeric('set', UIFloat2(jsonValue.monto4));
                $('#txtMonto5MPAFA').autoNumeric('set', UIFloat2(jsonValue.monto5));
                $('#txtCadena1MPAFA').val(jsonValue.cadena1);
                $('#txtCadena2MPAFA').val(jsonValue.cadena2);
                $('#txtCadena3MPAFA').val(jsonValue.cadena3);
                $('#txtCadena4MPAFA').val(jsonValue.cadena4);
                $('#txtCadena5MPAFA').val(jsonValue.cadena5);
                $('#txtFecha1MPAFA').datepicker('setDate', jsonValue.fecha1_texto);
                $('#txtFecha2MPAFA').datepicker('setDate', jsonValue.fecha2_texto);
                $('#txtFecha3MPAFA').datepicker('setDate', jsonValue.fecha3_texto);
                $('#txtFecha4MPAFA').datepicker('setDate', jsonValue.fecha4_texto);
                $('#txtFecha5MPAFA').datepicker('setDate', jsonValue.fecha5_texto);

                $('#btnGuardarMPAFA').off('click').on('click', function (e) {
                    var jsonParam = {
                        Accion: null,
                        CodigoParentescoFamiliar: null,
                        Nombre: $('#txtNombreMPAFA').val(),
                        Monto1: $('#txtMonto1MPAFA').val(),
                        Monto2: $('#txtMonto2MPAFA').val(),
                        Monto3: $('#txtMonto3MPAFA').val(),
                        Monto4: $('#txtMonto4MPAFA').val(),
                        Monto5: $('#txtMonto5MPAFA').val(),
                        Cadena1: $('#txtCadena1MPAFA').val(),
                        Cadena2: $('#txtCadena2MPAFA').val(),
                        Cadena3: $('#txtCadena3MPAFA').val(),
                        Cadena4: $('#txtCadena4MPAFA').val(),
                        Cadena5: $('#txtCadena5MPAFA').val(),
                        Fecha1: $('#txtFecha1MPAFA').val(),
                        Fecha2: $('#txtFecha2MPAFA').val(),
                        Fecha3: $('#txtFecha3MPAFA').val(),
                        Fecha4: $('#txtFecha4MPAFA').val(),
                        Fecha5: $('#txtFecha5MPAFA').val(),
                    };
                    if (jsonValue.isNew) {
                        jsonParam.Accion = 'N';
                        jsonParam.CodigoParentescoFamiliar = $('#txtCodigoMPAFA').val();
                    }
                    else {
                        jsonParam.Accion = 'M';
                        jsonParam.CodigoParentescoFamiliar = jsonValue.cod_parentesco;
                    }
                    if (UIEmpty(jsonParam.CodigoParentescoFamiliar)) {
                        UIMensaje.CODIGO();
                        return;
                    }
                    if (UIEmpty(jsonParam.Nombre)) {
                        UIMensaje.NOMBRE();
                        return;
                    }
                    UIAjax.runFunctionAsync('ajax/tablassoporte', 'PostParentescoFamiliar', jsonParam, function () {
                        Load.Personal.ParentescoFamiliar.Ready();
                        $('#modalParentescoFamiliar').modal('hide');
                    });
                });

                $('#modalParentescoFamiliar').modal('show');
                UILoadingDefault();
            }
        };
        Load.Personal.ProfesionOcupacion = {
            Ready: async function () {
                Load.ShowCard('#cardProfesionOcupacion');
                await UIAjax.invokeMethodAsync('ajax/tablassoporte', 'GetAllProfesionOcupacion', {}).then(function (r) {
                    tableProfesionOcupacion = $('#tableProfesionOcupacion').DataTable({
                        data: r.data,
                        initComplete: function (settings, json) {
                            UIDataTable.SelectRows(this, false, false, Load.Personal.ProfesionOcupacion.Modal);
                            UIDataTable.AJAXDelete(this, '#btnEliminarProfesionOcupacion', 'cod_profesion', 'ajax/tablassoporte', 'DeleteProfesionOcupacion', {
                                CodigoProfesionOcupacion: 'cod_profesion',
                            }, Load.Personal.ProfesionOcupacion.Ready);
                        },
                        columnDefs: [
                            { targets: [1], className: 'text-left' },
                            { targets: '_all', className: 'text-center' },
                        ],
                        columns: [
                            { data: "cod_profesion" },
                            { data: "nombre" },
                            { data: "monto1" },
                            { data: "monto2" },
                            { data: "monto3" },
                            { data: "monto4" },
                            { data: "monto5" },
                            { data: "cadena1" },
                            { data: "cadena2" },
                            { data: "cadena3" },
                            { data: "cadena4" },
                            { data: "cadena5" },
                            { data: "fecha1_texto" },
                            { data: "fecha2_texto" },
                            { data: "fecha3_texto" },
                            { data: "fecha4_texto" },
                            { data: "fecha5_texto" },
                        ],
                    });
                });
            },
            Modal: async function (jsonValue) {
                await UILoadingDefault("show");
                if (jsonValue.isNew) {
                    $('#h4TitleMPROC').html('NUEVA PROFESIÓN U OCUPACIÓN');
                    $('#txtCodigoMPROC').removeAttr('disabled');
                }
                else {
                    $('#h4TitleMPROC').html(`PROFESIÓN U OCUPACIÓN: ${jsonValue.cod_profesion}`);
                    $('#txtCodigoMPROC').attr('disabled', 'disabled');
                }

                var fnBoolean = function (key) { return jsonValue[key] == "S" ? true : false; };
                UIDatePicker.Init('input.txtFechaMPROC');
                UIAutoNumeric.Init('input.txtMontoMPROC');

                $('#txtCodigoMPROC').val(jsonValue.cod_profesion);
                $('#txtNombreMPROC').val(jsonValue.nombre);

                $('#txtMonto1MPROC').autoNumeric('set', UIFloat2(jsonValue.monto1));
                $('#txtMonto2MPROC').autoNumeric('set', UIFloat2(jsonValue.monto2));
                $('#txtMonto3MPROC').autoNumeric('set', UIFloat2(jsonValue.monto3));
                $('#txtMonto4MPROC').autoNumeric('set', UIFloat2(jsonValue.monto4));
                $('#txtMonto5MPROC').autoNumeric('set', UIFloat2(jsonValue.monto5));
                $('#txtCadena1MPROC').val(jsonValue.cadena1);
                $('#txtCadena2MPROC').val(jsonValue.cadena2);
                $('#txtCadena3MPROC').val(jsonValue.cadena3);
                $('#txtCadena4MPROC').val(jsonValue.cadena4);
                $('#txtCadena5MPROC').val(jsonValue.cadena5);
                $('#txtFecha1MPROC').datepicker('setDate', jsonValue.fecha1_texto);
                $('#txtFecha2MPROC').datepicker('setDate', jsonValue.fecha2_texto);
                $('#txtFecha3MPROC').datepicker('setDate', jsonValue.fecha3_texto);
                $('#txtFecha4MPROC').datepicker('setDate', jsonValue.fecha4_texto);
                $('#txtFecha5MPROC').datepicker('setDate', jsonValue.fecha5_texto);

                $('#btnGuardarMPROC').off('click').on('click', function (e) {
                    var jsonParam = {
                        Accion: null,
                        CodigoProfesionOcupacion: null,
                        Nombre: $('#txtNombreMPROC').val(),
                        Monto1: $('#txtMonto1MPROC').val(),
                        Monto2: $('#txtMonto2MPROC').val(),
                        Monto3: $('#txtMonto3MPROC').val(),
                        Monto4: $('#txtMonto4MPROC').val(),
                        Monto5: $('#txtMonto5MPROC').val(),
                        Cadena1: $('#txtCadena1MPROC').val(),
                        Cadena2: $('#txtCadena2MPROC').val(),
                        Cadena3: $('#txtCadena3MPROC').val(),
                        Cadena4: $('#txtCadena4MPROC').val(),
                        Cadena5: $('#txtCadena5MPROC').val(),
                        Fecha1: $('#txtFecha1MPROC').val(),
                        Fecha2: $('#txtFecha2MPROC').val(),
                        Fecha3: $('#txtFecha3MPROC').val(),
                        Fecha4: $('#txtFecha4MPROC').val(),
                        Fecha5: $('#txtFecha5MPROC').val(),
                    };
                    if (jsonValue.isNew) {
                        jsonParam.Accion = 'N';
                        jsonParam.CodigoProfesionOcupacion = $('#txtCodigoMPROC').val();
                    }
                    else {
                        jsonParam.Accion = 'M';
                        jsonParam.CodigoProfesionOcupacion = jsonValue.cod_profesion;
                    }
                    if (UIEmpty(jsonParam.CodigoProfesionOcupacion)) {
                        UIMensaje.CODIGO();
                        return;
                    }
                    if (UIEmpty(jsonParam.Nombre)) {
                        UIMensaje.NOMBRE();
                        return;
                    }
                    UIAjax.runFunctionAsync('ajax/tablassoporte', 'PostProfesionOcupacion', jsonParam, function () {
                        Load.Personal.ProfesionOcupacion.Ready();
                        $('#modalProfesionOcupacion').modal('hide');
                    });
                });

                $('#modalProfesionOcupacion').modal('show');
                UILoadingDefault();
            }
        };
        Load.Personal.EstadoCivil = {
            Ready: async function () {
                Load.ShowCard('#cardEstadoCivil');
                await UIAjax.invokeMethodAsync('ajax/tablassoporte', 'GetAllEstadoCivil', {}).then(function (r) {
                    tableEstadoCivil = $('#tableEstadoCivil').DataTable({
                        data: r.data,
                        initComplete: function (settings, json) {
                            UIDataTable.SelectRows(this, false, false, Load.Personal.EstadoCivil.Modal);
                            UIDataTable.AJAXDelete(this, '#btnEliminarEstadoCivil', 'cod_estado_civil', 'ajax/tablassoporte', 'DeleteEstadoCivil', {
                                CodigoEstadoCivil: 'cod_estado_civil',
                            }, Load.Personal.EstadoCivil.Ready);
                        },
                        columnDefs: [
                            { targets: [1], className: 'text-left' },
                            { targets: '_all', className: 'text-center' },
                        ],
                        columns: [
                            { data: "cod_estado_civil" },
                            { data: "nombre" },
                            { data: "monto1" },
                            { data: "monto2" },
                            { data: "monto3" },
                            { data: "monto4" },
                            { data: "monto5" },
                            { data: "cadena1" },
                            { data: "cadena2" },
                            { data: "cadena3" },
                            { data: "cadena4" },
                            { data: "cadena5" },
                            { data: "fecha1_texto" },
                            { data: "fecha2_texto" },
                            { data: "fecha3_texto" },
                            { data: "fecha4_texto" },
                            { data: "fecha5_texto" },
                        ],
                    });
                });
            },
            Modal: async function (jsonValue) {
                await UILoadingDefault("show");
                if (jsonValue.isNew) {
                    $('#h4TitleMESCI').html('NUEVO ESTADO CIVIL');
                    $('#txtCodigoMESCI').removeAttr('disabled');
                }
                else {
                    $('#h4TitleMESCI').html(`ESTADO CIVIL: ${jsonValue.cod_estado_civil}`);
                    $('#txtCodigoMESCI').attr('disabled', 'disabled');
                }

                var fnBoolean = function (key) { return jsonValue[key] == "S" ? true : false; };
                UIDatePicker.Init('input.txtFechaMESCI');
                UIAutoNumeric.Init('input.txtMontoMESCI');

                $('#txtCodigoMESCI').val(jsonValue.cod_estado_civil);
                $('#txtNombreMESCI').val(jsonValue.nombre);

                $('#txtMonto1MESCI').autoNumeric('set', UIFloat2(jsonValue.monto1));
                $('#txtMonto2MESCI').autoNumeric('set', UIFloat2(jsonValue.monto2));
                $('#txtMonto3MESCI').autoNumeric('set', UIFloat2(jsonValue.monto3));
                $('#txtMonto4MESCI').autoNumeric('set', UIFloat2(jsonValue.monto4));
                $('#txtMonto5MESCI').autoNumeric('set', UIFloat2(jsonValue.monto5));
                $('#txtCadena1MESCI').val(jsonValue.cadena1);
                $('#txtCadena2MESCI').val(jsonValue.cadena2);
                $('#txtCadena3MESCI').val(jsonValue.cadena3);
                $('#txtCadena4MESCI').val(jsonValue.cadena4);
                $('#txtCadena5MESCI').val(jsonValue.cadena5);
                $('#txtFecha1MESCI').datepicker('setDate', jsonValue.fecha1_texto);
                $('#txtFecha2MESCI').datepicker('setDate', jsonValue.fecha2_texto);
                $('#txtFecha3MESCI').datepicker('setDate', jsonValue.fecha3_texto);
                $('#txtFecha4MESCI').datepicker('setDate', jsonValue.fecha4_texto);
                $('#txtFecha5MESCI').datepicker('setDate', jsonValue.fecha5_texto);

                $('#btnGuardarMESCI').off('click').on('click', function (e) {
                    var jsonParam = {
                        Accion: null,
                        CodigoEstadoCivil: null,
                        Nombre: $('#txtNombreMESCI').val(),
                        Monto1: $('#txtMonto1MESCI').val(),
                        Monto2: $('#txtMonto2MESCI').val(),
                        Monto3: $('#txtMonto3MESCI').val(),
                        Monto4: $('#txtMonto4MESCI').val(),
                        Monto5: $('#txtMonto5MESCI').val(),
                        Cadena1: $('#txtCadena1MESCI').val(),
                        Cadena2: $('#txtCadena2MESCI').val(),
                        Cadena3: $('#txtCadena3MESCI').val(),
                        Cadena4: $('#txtCadena4MESCI').val(),
                        Cadena5: $('#txtCadena5MESCI').val(),
                        Fecha1: $('#txtFecha1MESCI').val(),
                        Fecha2: $('#txtFecha2MESCI').val(),
                        Fecha3: $('#txtFecha3MESCI').val(),
                        Fecha4: $('#txtFecha4MESCI').val(),
                        Fecha5: $('#txtFecha5MESCI').val(),
                    };
                    if (jsonValue.isNew) {
                        jsonParam.Accion = 'N';
                        jsonParam.CodigoEstadoCivil = $('#txtCodigoMESCI').val();
                    }
                    else {
                        jsonParam.Accion = 'M';
                        jsonParam.CodigoEstadoCivil = jsonValue.cod_estado_civil;
                    }
                    if (UIEmpty(jsonParam.CodigoEstadoCivil)) {
                        UIMensaje.CODIGO();
                        return;
                    }
                    if (UIEmpty(jsonParam.Nombre)) {
                        UIMensaje.NOMBRE();
                        return;
                    }
                    UIAjax.runFunctionAsync('ajax/tablassoporte', 'PostEstadoCivil', jsonParam, function () {
                        Load.Personal.EstadoCivil.Ready();
                        $('#modalEstadoCivil').modal('hide');
                    });
                });

                $('#modalEstadoCivil').modal('show');
                UILoadingDefault();
            }
        };
        Load.Personal.MotivoCeseFamiliar = {
            Ready: async function () {
                Load.ShowCard('#cardMotivoCeseFamiliar');
                await UIAjax.invokeMethodAsync('ajax/tablassoporte', 'GetAllMotivoCeseFamiliar', {}).then(function (r) {
                    tableMotivoCeseFamiliar = $('#tableMotivoCeseFamiliar').DataTable({
                        data: r.data,
                        initComplete: function (settings, json) {
                            UIDataTable.SelectRows(this, false, false, Load.Personal.MotivoCeseFamiliar.Modal);
                            UIDataTable.AJAXDelete(this, '#btnEliminarMotivoCeseFamiliar', 'cod_motivo', 'ajax/tablassoporte', 'DeleteMotivoCeseFamiliar', {
                                CodigoDeMotivo: 'cod_motivo',
                            }, Load.Personal.MotivoCeseFamiliar.Ready);
                        },
                        columnDefs: [
                            { targets: [1], className: 'text-left' },
                            { targets: '_all', className: 'text-center' },
                        ],
                        columns: [
                            { data: "cod_motivo" },
                            { data: "descripcion" },
                            { data: "cadena1" },
                        ],
                    });
                });
            },
            Modal: async function (jsonValue) {
                await UILoadingDefault("show");
                if (jsonValue.isNew) {
                    $('#h4TitleMMOCEFA').html('NUEVO MOTIVO CESE FAMILIAR');
                    $('#txtCodigoMMOCEFA').removeAttr('disabled');
                }
                else {
                    $('#h4TitleMMOCEFA').html(`MOTIVO CESE FAMILIAR: ${jsonValue.cod_motivo}`);
                    $('#txtCodigoMMOCEFA').attr('disabled', 'disabled');
                }

                $('#txtCodigoMMOCEFA').val(jsonValue.cod_motivo);
                $('#txtDescripcionMMOCEFA').val(jsonValue.descripcion);
                $('#txtCadena1MMOCEFA').val(jsonValue.cadena1);

                $('#btnGuardarMMOCEFA').off('click').on('click', function (e) {
                    var jsonParam = {
                        Accion: null,
                        CodigoDeMotivo: null,
                        Descripcion: $('#txtDescripcionMMOCEFA').val(),
                        Cadena1: $('#txtCadena1MMOCEFA').val(),
                    };
                    if (jsonValue.isNew) {
                        jsonParam.Accion = 'N';
                        jsonParam.CodigoDeMotivo = $('#txtCodigoMMOCEFA').val();
                    }
                    else {
                        jsonParam.Accion = 'M';
                        jsonParam.CodigoDeMotivo = jsonValue.cod_motivo;
                    }
                    if (UIEmpty(jsonParam.CodigoDeMotivo)) {
                        UIMensaje.CODIGO();
                        return;
                    }
                    if (UIEmpty(jsonParam.Descripcion)) {
                        UIMensaje.DESCRIPCION();
                        return;
                    }
                    UIAjax.runFunctionAsync('ajax/tablassoporte', 'PostMotivoCeseFamiliar', jsonParam, function () {
                        Load.Personal.MotivoCeseFamiliar.Ready();
                        $('#modalMotivoCeseFamiliar').modal('hide');
                    });
                });

                $('#modalMotivoCeseFamiliar').modal('show');
                UILoadingDefault();
            }
        };
        Load.Personal.NivelAcademico = {
            Ready: async function () {
                Load.ShowCard('#cardNivelAcademico');
                await UIAjax.invokeMethodAsync('ajax/tablassoporte', 'GetAllNivelAcademico', {}).then(function (r) {
                    tableNivelAcademico = $('#tableNivelAcademico').DataTable({
                        data: r.data,
                        initComplete: function (settings, json) {
                            UIDataTable.SelectRows(this, false, false, Load.Personal.NivelAcademico.Modal);
                            UIDataTable.AJAXDelete(this, '#btnEliminarNivelAcademico', 'cod_nivel_academico', 'ajax/tablassoporte', 'DeleteNivelAcademico', {
                                CodigoNivelAcademico: 'cod_nivel_academico',
                            }, Load.Personal.NivelAcademico.Ready);
                        },
                        columnDefs: [
                            { targets: [1], className: 'text-left' },
                            { targets: '_all', className: 'text-center' },
                        ],
                        columns: [
                            { data: "cod_nivel_academico" },
                            { data: "nombre" },
                            { data: "monto1" },
                            { data: "monto2" },
                            { data: "monto3" },
                            { data: "monto4" },
                            { data: "monto5" },
                            { data: "cadena1" },
                            { data: "cadena2" },
                            { data: "cadena3" },
                            { data: "cadena4" },
                            { data: "cadena5" },
                            { data: "fecha1_texto" },
                            { data: "fecha2_texto" },
                            { data: "fecha3_texto" },
                            { data: "fecha4_texto" },
                            { data: "fecha5_texto" },
                        ],
                    });
                });
            },
            Modal: async function (jsonValue) {
                await UILoadingDefault("show");
                if (jsonValue.isNew) {
                    $('#h4TitleMNIAC').html('NUEVO NIVEL ACADÉMICO');
                    $('#txtCodigoMNIAC').removeAttr('disabled');
                }
                else {
                    $('#h4TitleMNIAC').html(`NIVEL ACADÉMICO: ${jsonValue.cod_nivel_academico}`);
                    $('#txtCodigoMNIAC').attr('disabled', 'disabled');
                }

                var fnBoolean = function (key) { return jsonValue[key] == "S" ? true : false; };
                UIDatePicker.Init('input.txtFechaMNIAC');
                UIAutoNumeric.Init('input.txtMontoMNIAC');

                $('#txtCodigoMNIAC').val(jsonValue.cod_nivel_academico);
                $('#txtNombreMNIAC').val(jsonValue.nombre);

                $('#txtMonto1MNIAC').autoNumeric('set', UIFloat2(jsonValue.monto1));
                $('#txtMonto2MNIAC').autoNumeric('set', UIFloat2(jsonValue.monto2));
                $('#txtMonto3MNIAC').autoNumeric('set', UIFloat2(jsonValue.monto3));
                $('#txtMonto4MNIAC').autoNumeric('set', UIFloat2(jsonValue.monto4));
                $('#txtMonto5MNIAC').autoNumeric('set', UIFloat2(jsonValue.monto5));
                $('#txtCadena1MNIAC').val(jsonValue.cadena1);
                $('#txtCadena2MNIAC').val(jsonValue.cadena2);
                $('#txtCadena3MNIAC').val(jsonValue.cadena3);
                $('#txtCadena4MNIAC').val(jsonValue.cadena4);
                $('#txtCadena5MNIAC').val(jsonValue.cadena5);
                $('#txtFecha1MNIAC').datepicker('setDate', jsonValue.fecha1_texto);
                $('#txtFecha2MNIAC').datepicker('setDate', jsonValue.fecha2_texto);
                $('#txtFecha3MNIAC').datepicker('setDate', jsonValue.fecha3_texto);
                $('#txtFecha4MNIAC').datepicker('setDate', jsonValue.fecha4_texto);
                $('#txtFecha5MNIAC').datepicker('setDate', jsonValue.fecha5_texto);

                $('#btnGuardarMNIAC').off('click').on('click', function (e) {
                    var jsonParam = {
                        Accion: null,
                        CodigoNivelAcademico: null,
                        Nombre: $('#txtNombreMNIAC').val(),
                        Monto1: $('#txtMonto1MNIAC').val(),
                        Monto2: $('#txtMonto2MNIAC').val(),
                        Monto3: $('#txtMonto3MNIAC').val(),
                        Monto4: $('#txtMonto4MNIAC').val(),
                        Monto5: $('#txtMonto5MNIAC').val(),
                        Cadena1: $('#txtCadena1MNIAC').val(),
                        Cadena2: $('#txtCadena2MNIAC').val(),
                        Cadena3: $('#txtCadena3MNIAC').val(),
                        Cadena4: $('#txtCadena4MNIAC').val(),
                        Cadena5: $('#txtCadena5MNIAC').val(),
                        Fecha1: $('#txtFecha1MNIAC').val(),
                        Fecha2: $('#txtFecha2MNIAC').val(),
                        Fecha3: $('#txtFecha3MNIAC').val(),
                        Fecha4: $('#txtFecha4MNIAC').val(),
                        Fecha5: $('#txtFecha5MNIAC').val(),
                    };
                    if (jsonValue.isNew) {
                        jsonParam.Accion = 'N';
                        jsonParam.CodigoNivelAcademico = $('#txtCodigoMNIAC').val();
                    }
                    else {
                        jsonParam.Accion = 'M';
                        jsonParam.CodigoNivelAcademico = jsonValue.cod_nivel_academico;
                    }
                    if (UIEmpty(jsonParam.CodigoNivelAcademico)) {
                        UIMensaje.CODIGO();
                        return;
                    }
                    if (UIEmpty(jsonParam.Nombre)) {
                        UIMensaje.NOMBRE();
                        return;
                    }
                    UIAjax.runFunctionAsync('ajax/tablassoporte', 'PostNivelAcademico', jsonParam, function () {
                        Load.Personal.NivelAcademico.Ready();
                        $('#modalNivelAcademico').modal('hide');
                    });
                });

                $('#modalNivelAcademico').modal('show');
                UILoadingDefault();
            }
        };
    }
    else {
        console.warn("No se encontró la propiedad 'Personal' en la variable 'Load'");
    }
}
else {
    console.warn("No se encontró la variable 'Load'");
}