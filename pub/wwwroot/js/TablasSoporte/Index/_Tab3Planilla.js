if (typeof Load != undefined) {
    if (typeof Load.Planilla != undefined) {
        Load.Planilla.MotivoIngresoCese = {
            Ready: async function () {
                Load.ShowCard('#cardMotivoIngresoCese');
                await UIAjax.invokeMethodAsync('ajax/tablassoporte', 'GetAllMotivoIngresoCese', {}).then(function (r) {
                    tableMotivoIngresoCese = $('#tableMotivoIngresoCese').DataTable({
                        data: r.data,
                        initComplete: function (settings, json) {
                            UIDataTable.SelectRows(this, false, false, Load.Planilla.MotivoIngresoCese.Modal);
                            UIDataTable.AJAXDelete(this, '#btnEliminarMotivoIngresoCese', 'cod_motivo', 'ajax/tablassoporte', 'DeleteMotivoIngresoCese', {
                                CodigoDeMotivo: 'cod_motivo',
                            }, Load.Planilla.MotivoIngresoCese.Ready);
                        },
                        dom: 'Bfrtip',
                        buttons: [],
                        columnDefs: [
                            { targets: [1], className: 'text-left' },
                            { targets: '_all', className: 'text-center' },
                        ],
                        columns: [
                            { data: "cod_motivo" },
                            { data: "descripcion" },
                            { data: "flg_motivo_texto" },
                            { data: "codigo_sunat" },
                            { data: "monto1" },
                            { data: "monto2" },
                            { data: "monto3" },
                            { data: "monto4" },
                            { data: "monto5" },
                            { data: "cadena1" },
                            { data: "cadena2" },
                            { data: "cadena3" },
                            { data: "cadena4" },
                            { data: "cadena5" },
                            { data: "fecha1_texto" },
                            { data: "fecha2_texto" },
                            { data: "fecha3_texto" },
                            { data: "fecha4_texto" },
                            { data: "fecha5_texto" },
                        ],
                    });
                });
            },
            Modal: async function (jsonValue) {
                UILoadingDefault();
                if (jsonValue.isNew) {
                    $('#h4TitleMMOINCE').html('NUEVO MOTIVO INGRESO/CESE');
                    $('#txtCodigoMMOINCE').removeAttr('disabled');
                    jsonValue.flg_motivo = "I";
                }
                else {
                    $('#h4TitleMMOINCE').html(`MOTIVO INGRESO/CESE: ${jsonValue.cod_motivo}`);
                    $('#txtCodigoMMOINCE').attr('disabled', 'disabled');
                }
                $("input.txtFechaMMOINCE").datepicker({ language: 'es' });

                $('#txtCodigoMMOINCE').val(jsonValue.cod_motivo);
                $('#txtDescripcionMMOINCE').val(jsonValue.descripcion);
                $('#selectMotivoMMOINCE').val(jsonValue.flg_motivo);
                $('#txtSunatMMOINCE').val(jsonValue.codigo_sunat);

                $('#txtMonto1MMOINCE').val(jsonValue.monto1);
                $('#txtMonto2MMOINCE').val(jsonValue.monto2);
                $('#txtMonto3MMOINCE').val(jsonValue.monto3);
                $('#txtMonto4MMOINCE').val(jsonValue.monto4);
                $('#txtMonto5MMOINCE').val(jsonValue.monto5);
                $('#txtCadena1MMOINCE').val(jsonValue.cadena1);
                $('#txtCadena2MMOINCE').val(jsonValue.cadena2);
                $('#txtCadena3MMOINCE').val(jsonValue.cadena3);
                $('#txtCadena4MMOINCE').val(jsonValue.cadena4);
                $('#txtCadena5MMOINCE').val(jsonValue.cadena5);
                $('#txtFecha1MMOINCE').datepicker('setDate', jsonValue.fecha1_texto);
                $('#txtFecha2MMOINCE').datepicker('setDate', jsonValue.fecha2_texto);
                $('#txtFecha3MMOINCE').datepicker('setDate', jsonValue.fecha3_texto);
                $('#txtFecha4MMOINCE').datepicker('setDate', jsonValue.fecha4_texto);
                $('#txtFecha5MMOINCE').datepicker('setDate', jsonValue.fecha5_texto);

                $('#btnGuardarMMOINCE').off('click').on('click', function (e) {
                    var jsonParam = {
                        Accion: null,
                        CodigoDeMotivo: null,
                        Descripcion: $('#txtDescripcionMMOINCE').val(),
                        FlgMotivo: $('#selectMotivoMMOINCE').val(),
                        CodigoDeSunat: $('#txtSunatMMOINCE').val(),
                        Monto1: $('#txtMonto1MMOINCE').val(),
                        Monto2: $('#txtMonto2MMOINCE').val(),
                        Monto3: $('#txtMonto3MMOINCE').val(),
                        Monto4: $('#txtMonto4MMOINCE').val(),
                        Monto5: $('#txtMonto5MMOINCE').val(),
                        Cadena1: $('#txtCadena1MMOINCE').val(),
                        Cadena2: $('#txtCadena2MMOINCE').val(),
                        Cadena3: $('#txtCadena3MMOINCE').val(),
                        Cadena4: $('#txtCadena4MMOINCE').val(),
                        Cadena5: $('#txtCadena5MMOINCE').val(),
                        Fecha1: $('#txtFecha1MMOINCE').val(),
                        Fecha2: $('#txtFecha2MMOINCE').val(),
                        Fecha3: $('#txtFecha3MMOINCE').val(),
                        Fecha4: $('#txtFecha4MMOINCE').val(),
                        Fecha5: $('#txtFecha5MMOINCE').val(),
                    };
                    if (jsonValue.isNew) {
                        jsonParam.Accion = 'N';
                        jsonParam.CodigoDeMotivo = $('#txtCodigoMMOINCE').val();
                    }
                    else {
                        jsonParam.Accion = 'M';
                        jsonParam.CodigoDeMotivo = jsonValue.cod_motivo;
                    }
                    if (UIEmpty(jsonParam.CodigoDeMotivo)) {
                        MsgBox('El código no puede ser vacío.', 'warning', true);
                        return;
                    }
                    if (UIEmpty(jsonParam.Descripcion)) {
                        MsgBox('La descripción no puede ser vacía.', 'warning', true);
                        return;
                    }
                    UIAjax.runFunctionAsync('ajax/tablassoporte', 'PostMotivoIngresoCese', jsonParam, function () {
                        Load.Planilla.MotivoIngresoCese.Ready();
                        $('#modalMotivoIngresoCese').modal('hide');
                    });
                });

                $('#modalMotivoIngresoCese').modal('show');
                UILoadingDefault();
            }
        };
        Load.Planilla.ModeloContrato = {
            Ready: async function () {
                Load.ShowCard('#cardModeloContrato');
                await UIAjax.invokeMethodAsync('ajax/tablassoporte', 'GetAllModeloContrato', {}).then(function (r) {
                    tableModeloContrato = $('#tableModeloContrato').DataTable({
                        data: r.data,
                        initComplete: function (settings, json) {
                            UIDataTable.SelectRows(this, false, false, Load.Planilla.ModeloContrato.Modal);
                            UIDataTable.AJAXDelete(this, '#btnEliminarModeloContrato', 'nombre', 'ajax/tablassoporte', 'DeleteModeloContrato', {
                                IdDeAplicacion: 'id_aplicacion',
                                CodigoDeReporte: 'cod_reporte',
                            }, Load.Planilla.ModeloContrato.Ready);
                        },
                        dom: 'Bfrtip',
                        buttons: [],
                        columns: [
                            { data: "id_aplicacion_texto" },
                            { data: "cod_reporte" },
                            { data: "nombre" },
                        ],
                    });
                });
            },
            Modal: async function (jsonValue) {
                UILoadingDefault();
                if (jsonValue.isNew) {
                    jsonValue.id_aplicacion = "CT";
                    $('#h4TitleMMOCO').html('NUEVO MODELO DE CONTRATO');
                    $('#txtCodigoMMOCO').removeAttr('disabled');
                }
                else {
                    $('#h4TitleMMOCO').html(`MODELO DE CONTRATO: ${jsonValue.id_aplicacion} - ${jsonValue.cod_reporte}`);
                    $('#txtCodigoMMOCO').attr('disabled', 'disabled');
                }

                await UIFunctions._setPrivateSelect('ajax/tablassoporte', 'GetModeloContratoReporte', null, false, '#selectReporteMMOCO');

                $('#txtCodigoMMOCO').val(jsonValue.cod_reporte);
                $('#selectReporteMMOCO').val(jsonValue.id_aplicacion);
                $('#txtNombreMMOCO').val(jsonValue.nombre);

                $('#btnGuardarMMOCO').off('click').on('click', function (e) {
                    //Tener cuidado, es muy confuso esta parte.
                    var jsonParam = {
                        Accion: null,
                        IdDeAplicacion: $('#selectReporteMMOCO').val(),
                        IdDeAplicacionAnterior: null,
                        Codigo: $('#txtCodigoMMOCO').val(),
                        CodigoAnterior: null,
                        Nombre: $('#txtNombreMMOCO').val(),
                    };
                    if (jsonValue.isNew) {
                        jsonParam.Accion = 'N';
                    }
                    else {
                        jsonParam.Accion = 'M';
                        jsonParam.IdDeAplicacionAnterior = jsonValue.id_aplicacion;
                        jsonParam.CodigoAnterior = jsonValue.cod_reporte;
                    }
                    if (UIEmpty(jsonParam.Codigo)) {
                        MsgBox('El código no puede ser vacío.', 'warning', true);
                        return;
                    }
                    if (UIEmpty(jsonParam.Nombre)) {
                        MsgBox('El nombre no puede ser vacío.', 'warning', true);
                        return;
                    }
                    UIAjax.runFunctionAsync('ajax/tablassoporte', 'PostModeloContrato', jsonParam, function () {
                        Load.Planilla.ModeloContrato.Ready();
                        $('#modalModeloContrato').modal('hide');
                    });
                });

                $('#modalModeloContrato').modal('show');
                UILoadingDefault();
            }
        };
        Load.Planilla.PlanCuenta = {
            Ready: async function () {
                Load.ShowCard('#cardPlanCuenta');
                await UIAjax.invokeMethodAsync('ajax/tablassoporte', 'GetAllPlanCuentas', {}).then(function (r) {
                    var _columnCHK = function (_data) {
                        return {
                            data: _data, render: function (data, type, row, meta) {
                                return UIFunctions.Checkbox.HTML2(null, data, null, true);
                            }, className: 'text-center align-middle'
                        };
                    };
                    tablePlanCuenta = $('#tablePlanCuenta').DataTable({
                        data: r.data,
                        initComplete: function (settings, json) {
                            UIDataTable.SelectRows(this, false, false, Load.Planilla.PlanCuenta.Modal);
                            UIDataTable.AJAXDelete(this, '#btnEliminarPlanCuenta', 'cod_cuenta', 'ajax/tablassoporte', 'DeletePlanCuentas', {
                                CodigoCuenta: 'cod_cuenta',
                            }, Load.Planilla.PlanCuenta.Ready);
                        },
                        columns: [
                            { data: "cod_cuenta" },
                            { data: "descripcion" },
                            _columnCHK("cod_auxiliar"),
                            _columnCHK("flg_proyecto"),
                            _columnCHK("flg_centro_costo"),
                            _columnCHK("flg_unidad_negocio"),
                            _columnCHK("flg_localidad"),
                            _columnCHK("flg_area"),
                            _columnCHK("flg_afp"),
                            { data: "cadena1" },
                            { data: "cadena2" },
                            { data: "cadena3" },
                            { data: "cadena4" },
                            { data: "cadena5" },
                            { data: "cadena6" },
                            { data: "cadena7" },
                            { data: "cadena8" },
                            { data: "cadena9" },
                            { data: "cadena10" },
                            { data: "cadena11" },
                            { data: "cadena12" },
                            { data: "cadena13" },
                            { data: "cadena14" },
                            { data: "cadena15" },
                            { data: "cadena16" },
                            { data: "cadena17" },
                            { data: "cadena18" },
                            { data: "cadena19" },
                            { data: "cadena20" },
                            { data: "cod_cuenta_puente" },
                            { data: "cod_cuenta_destino" },
                        ],
                    });
                });
            },
            Modal: async function (jsonValue) {
                await UILoadingDefault("show");
                if (jsonValue.isNew) {
                    $('#h4TitleMPLCU').html('NUEVO PLAN DE CUENTA');
                    $('#txtCodigoMPLCU').removeAttr('disabled');
                }
                else {
                    $('#h4TitleMPLCU').html(`PLAN DE CUENTA: ${jsonValue.cod_cuenta}`);
                    $('#txtCodigoMPLCU').attr('disabled', 'disabled');
                }
                var fnBoolean = function (key) { return jsonValue[key] == "S" ? true : false; };

                $('#txtCodigoMPLCU').val(jsonValue.cod_cuenta);
                $('#txtDescripcionMPLCU').val(jsonValue.descripcion);

                $('#chkAuxiliarMPLCU').prop('checked', fnBoolean("cod_auxiliar"));
                $('#chkProyectoMPLCU').prop('checked', fnBoolean("flg_proyecto"));
                $('#chkCCMPLCU').prop('checked', fnBoolean("flg_centro_costo"));
                $('#chkUNMPLCU').prop('checked', fnBoolean("flg_unidad_negocio"));
                $('#chkLocalidadMPLCU').prop('checked', fnBoolean("flg_localidad"));
                $('#chkAreaMPLCU').prop('checked', fnBoolean("flg_area"));
                $('#chkAFPMPLCU').prop('checked', fnBoolean("flg_afp"));

                $('#txtCadena1MPLCU').val(jsonValue.cadena1);
                $('#txtCadena2MPLCU').val(jsonValue.cadena2);
                $('#txtCadena3MPLCU').val(jsonValue.cadena3);
                $('#txtCadena4MPLCU').val(jsonValue.cadena4);
                $('#txtCadena5MPLCU').val(jsonValue.cadena5);
                $('#txtCadena6MPLCU').val(jsonValue.cadena6);
                $('#txtCadena7MPLCU').val(jsonValue.cadena7);
                $('#txtCadena8MPLCU').val(jsonValue.cadena8);
                $('#txtCadena9MPLCU').val(jsonValue.cadena9);
                $('#txtCadena10MPLCU').val(jsonValue.cadena10);
                $('#txtCadena11MPLCU').val(jsonValue.cadena11);
                $('#txtCadena12MPLCU').val(jsonValue.cadena12);
                $('#txtCadena13MPLCU').val(jsonValue.cadena13);
                $('#txtCadena14MPLCU').val(jsonValue.cadena14);
                $('#txtCadena15MPLCU').val(jsonValue.cadena15);
                $('#txtCadena16MPLCU').val(jsonValue.cadena16);
                $('#txtCadena17MPLCU').val(jsonValue.cadena17);
                $('#txtCadena18MPLCU').val(jsonValue.cadena18);
                $('#txtCadena19MPLCU').val(jsonValue.cadena19);
                $('#txtCadena20MPLCU').val(jsonValue.cadena20);
                $('#txtCodigoCuentaPuenteMPLCU').val(jsonValue.cod_cuenta_puente);
                $('#txtCodigoCuentaDestinoMPLCU').val(jsonValue.cod_cuenta_destino);
                $('#btnGuardarMPLCU').off('click').on('click', function (e) {
                    var jsonParam = {
                        Accion: null,
                        PersonalCuentas: [
                            {
                                cod_cuenta: $('#txtCodigoMPLCU').val(),
                                descripcion: $('#txtDescripcionMPLCU').val(),
                                cod_auxiliar: UIFunctions.getValue.HTML('#chkAuxiliarMPLCU'),
                                flg_proyecto: UIFunctions.getValue.HTML('#chkProyectoMPLCU'),
                                flg_unidad_negocio: UIFunctions.getValue.HTML('#chkUNMPLCU'),
                                flg_centro_costo: UIFunctions.getValue.HTML('#chkCCMPLCU'),
                                flg_area: UIFunctions.getValue.HTML('#chkAreaMPLCU'),
                                cod_cuenta_puente: $('#txtCodigoCuentaPuenteMPLCU').val(),
                                cod_cuenta_destino: $('#txtCodigoCuentaDestinoMPLCU').val(),
                                flg_localidad: UIFunctions.getValue.HTML('#chkLocalidadMPLCU'),
                                cadena1: $('#txtCadena1MPLCU').val(),
                                cadena2: $('#txtCadena2MPLCU').val(),
                                cadena3: $('#txtCadena3MPLCU').val(),
                                cadena4: $('#txtCadena4MPLCU').val(),
                                flg_afp: UIFunctions.getValue.HTML('#chkAFPMPLCU'),
                                cadena5: $('#txtCadena5MPLCU').val(),
                                cadena6: $('#txtCadena6MPLCU').val(),
                                cadena7: $('#txtCadena7MPLCU').val(),
                                cadena8: $('#txtCadena8MPLCU').val(),
                                cadena9: $('#txtCadena9MPLCU').val(),
                                cadena10: $('#txtCadena10MPLCU').val(),
                                cadena11: $('#txtCadena11MPLCU').val(),
                                cadena12: $('#txtCadena12MPLCU').val(),
                                cadena13: $('#txtCadena13MPLCU').val(),
                                cadena14: $('#txtCadena14MPLCU').val(),
                                cadena15: $('#txtCadena15MPLCU').val(),
                                cadena16: $('#txtCadena16MPLCU').val(),
                                cadena17: $('#txtCadena17MPLCU').val(),
                                cadena18: $('#txtCadena18MPLCU').val(),
                                cadena19: $('#txtCadena19MPLCU').val(),
                                cadena20: $('#txtCadena20MPLCU').val(),
                            }
                        ]
                    };
                    if (jsonValue.isNew) {
                        jsonParam.Accion = 'N';
                    }
                    else {
                        jsonParam.Accion = 'M';
                        jsonParam.PersonalCuentas[0].cod_cuenta = jsonValue.cod_cuenta;
                    }
                    if (UIEmpty(jsonParam.PersonalCuentas[0].cod_cuenta)) {
                        UIMensaje.CODIGO();
                        return;
                    }
                    if (UIEmpty(jsonParam.PersonalCuentas[0].descripcion)) {
                        UIMensaje.DESCRIPCION();
                        return;
                    }
                    if (UIEmpty(jsonParam.PersonalCuentas[0].cod_cuenta_puente)) {
                        MsgBox("El código cuenta puente no puede ser vacío.", "warning", false);
                        return;
                    }
                    if (UIEmpty(jsonParam.PersonalCuentas[0].cod_cuenta_destino)) {
                        MsgBox("El código cuenta destino no puede ser vacío.", "warning", false);
                        return;
                    }
                    UIAjax.runFunctionAsync('ajax/tablassoporte', 'PostPlanCuentas', jsonParam, function () {
                        Load.Planilla.PlanCuenta.Ready();
                        $('#modalPlanCuenta').modal('hide');
                    });
                });

                $('#modalPlanCuenta').modal('show');
                UILoadingDefault();
            }
        };
        Load.Planilla.TipoContrato = {
            Ready: async function () {
                Load.ShowCard('#cardTipoContrato');
                await UIAjax.invokeMethodAsync('ajax/tablassoporte', 'GetAllTipoContrato', {}).then(function (r) {
                    tableTipoContrato = $('#tableTipoContrato').DataTable({
                        data: r.data,
                        initComplete: function (settings, json) {
                            UIDataTable.SelectRows(this, false, false, Load.Planilla.TipoContrato.Modal);
                            UIDataTable.AJAXDelete(this, '#btnEliminarTipoContrato', 'cod_tipo_contrato', 'ajax/tablassoporte', 'DeleteTipoContrato', {
                                CodigoTipoContrato: 'cod_tipo_contrato',
                            }, Load.Planilla.TipoContrato.Ready);
                        },
                        columnDefs: [
                            { targets: [1], className: 'text-left' },
                            { targets: '_all', className: 'text-center' },
                        ],
                        columns: [
                            { data: "cod_tipo_contrato" },
                            { data: "descripcion" },
                            { data: "nombre_reporte" },
                            { data: "nombre_modalidad_contrato" },
                            { data: "monto1" },
                            { data: "monto2" },
                            { data: "monto3" },
                            { data: "monto4" },
                            { data: "monto5" },
                            { data: "cadena1" },
                            { data: "cadena2" },
                            { data: "cadena3" },
                            { data: "cadena4" },
                            { data: "cadena5" },
                            { data: "fecha1_texto" },
                            { data: "fecha2_texto" },
                            { data: "fecha3_texto" },
                            { data: "fecha4_texto" },
                            { data: "fecha5_texto" },
                        ],
                    });
                });
            },
            Modal: async function (jsonValue) {
                await UILoadingDefault("show");
                if (jsonValue.isNew) {
                    $('#h4TitleMTICO').html('NUEVO TIPO CONTRATO');
                    $('#txtCodigoMTICO').removeAttr('disabled');
                }
                else {
                    $('#h4TitleMTICO').html(`TIPO CONTRATO: ${jsonValue.cod_tipo_contrato}`);
                    $('#txtCodigoMTICO').attr('disabled', 'disabled');
                }

                var fnBoolean = function (key) { return jsonValue[key] == "S" ? true : false; };
                UIDatePicker.Init('input.txtFechaMTICO');
                UIAutoNumeric.Init('input.txtMontoMTICO');
                await UIFunctions.setSelect('ajax/tablassoporte', 'GetReporteConfigurableByIdAplicacion', { IdDeAplicacion: 'CT' }, false, '#selectReporteMTICO', 'cod_reporte', 'nombre');
                await UIFunctions.setSelect('ajax/tablassoporte', 'GetAllModalidadContrato', null, false, '#selectModalidadMTICO', 'cod_modalidad_contrato', 'nombre');

                $('#txtCodigoMTICO').val(jsonValue.cod_tipo_contrato);
                $('#txtDescripcionMTICO').val(jsonValue.descripcion);

                $('#selectReporteMTICO').val(jsonValue.cod_reporte);
                $('#selectModalidadMTICO').val(jsonValue.cod_modalidad_contrato);

                $('#txtMonto1MTICO').autoNumeric('set', UIFloat2(jsonValue.monto1));
                $('#txtMonto2MTICO').autoNumeric('set', UIFloat2(jsonValue.monto2));
                $('#txtMonto3MTICO').autoNumeric('set', UIFloat2(jsonValue.monto3));
                $('#txtMonto4MTICO').autoNumeric('set', UIFloat2(jsonValue.monto4));
                $('#txtMonto5MTICO').autoNumeric('set', UIFloat2(jsonValue.monto5));
                $('#txtCadena1MTICO').val(jsonValue.cadena1);
                $('#txtCadena2MTICO').val(jsonValue.cadena2);
                $('#txtCadena3MTICO').val(jsonValue.cadena3);
                $('#txtCadena4MTICO').val(jsonValue.cadena4);
                $('#txtCadena5MTICO').val(jsonValue.cadena5);
                $('#txtFecha1MTICO').datepicker('setDate', jsonValue.fecha1_texto);
                $('#txtFecha2MTICO').datepicker('setDate', jsonValue.fecha2_texto);
                $('#txtFecha3MTICO').datepicker('setDate', jsonValue.fecha3_texto);
                $('#txtFecha4MTICO').datepicker('setDate', jsonValue.fecha4_texto);
                $('#txtFecha5MTICO').datepicker('setDate', jsonValue.fecha5_texto);

                $('#btnGuardarMTICO').off('click').on('click', function (e) {
                    var jsonParam = {
                        Accion: null,
                        CodigoTipoContrato: null,
                        Descripcion: $('#txtDescripcionMTICO').val(),
                        CodigoDeReporte: $('#selectReporteMTICO').val(),
                        Monto1: $('#txtMonto1MTICO').val(),
                        Monto2: $('#txtMonto2MTICO').val(),
                        Monto3: $('#txtMonto3MTICO').val(),
                        Monto4: $('#txtMonto4MTICO').val(),
                        Monto5: $('#txtMonto5MTICO').val(),
                        Cadena1: $('#txtCadena1MTICO').val(),
                        Cadena2: $('#txtCadena2MTICO').val(),
                        Cadena3: $('#txtCadena3MTICO').val(),
                        Cadena4: $('#txtCadena4MTICO').val(),
                        Cadena5: $('#txtCadena5MTICO').val(),
                        Fecha1: $('#txtFecha1MTICO').val(),
                        Fecha2: $('#txtFecha2MTICO').val(),
                        Fecha3: $('#txtFecha3MTICO').val(),
                        Fecha4: $('#txtFecha4MTICO').val(),
                        Fecha5: $('#txtFecha5MTICO').val(),
                        CodigoModalidadContrato: $('#selectModalidadMTICO').val(),
                    };
                    if (jsonValue.isNew) {
                        jsonParam.Accion = 'N';
                        jsonParam.CodigoTipoContrato = $('#txtCodigoMTICO').val();
                    }
                    else {
                        jsonParam.Accion = 'M';
                        jsonParam.CodigoTipoContrato = jsonValue.cod_tipo_contrato;
                    }
                    if (UIEmpty(jsonParam.CodigoTipoContrato)) {
                        UIMensaje.CODIGO();
                        return;
                    }
                    if (UIEmpty(jsonParam.Descripcion)) {
                        UIMensaje.DESCRIPCION();
                        return;
                    }
                    UIAjax.runFunctionAsync('ajax/tablassoporte', 'PostTipoContrato', jsonParam, function () {
                        Load.Planilla.TipoContrato.Ready();
                        $('#modalTipoContrato').modal('hide');
                    });
                });

                $('#modalTipoContrato').modal('show');
                UILoadingDefault();
            }
        };
        Load.Planilla.ConceptosSimilares = {
            Ready: async function () {
                Load.ShowCard('#cardConceptosSimilares');
                await UIAjax.invokeMethodAsync('ajax/tablassoporte', 'GetAllConceptosSimilares', {}).then(function (r) {
                    tableConceptosSimilares = $('#tableConceptosSimilares').DataTable({
                        data: r.data,
                        initComplete: function (settings, json) {
                            UIDataTable.SelectRows(this, false, false, Load.Planilla.ConceptosSimilares.Modal);
                            UIDataTable.AJAXDelete(this, '#btnEliminarConceptosSimilares', 'cod_simil', 'ajax/tablassoporte', 'DeleteConceptosSimilares', {
                                CodigoSimilar: 'cod_simil',
                            }, Load.Planilla.ConceptosSimilares.Ready);
                        },
                        columnDefs: [
                            { targets: [1], className: 'text-left' },
                            { targets: '_all', className: 'text-center' },
                        ],
                        columns: [
                            { data: "cod_simil" },
                            { data: "nombre" },
                            UIDataTable.CreateColumnCHK('flg_equivalente'),
                            UIDataTable.CreateColumnCHK('flg_dias_trabajados'),
                            UIDataTable.CreateColumnCHK('flg_grupo_principal'),
                            { data: "porcen_minimo" },
                            { data: "nombre_concepto1" },
                            { data: "nombre_concepto2" },
                        ],
                    });
                });
            },
            Modal: async function (jsonValue) {
                await UILoadingDefault("show");
                if (jsonValue.isNew) {
                    $('#h4TitleMCOSI').html('NUEVO CONCEPTO SIMILAR');
                    $('#txtCodigoMCOSI').removeAttr('disabled');
                }
                else {
                    $('#h4TitleMCOSI').html(`CONCEPTO SIMILAR: ${jsonValue.cod_simil}`);
                    $('#txtCodigoMCOSI').attr('disabled', 'disabled');
                }

                var fnBoolean = function (key) { return jsonValue[key] == "S" ? true : false; };
                UIAutoNumeric.Init('input.txtMontoMCOSI', { vMax: '100.00' });
                var jsonParamTC = {
                    source_direction: "A",
                    source: [{ codigo: '--1', nombre: '' }],
                };
                await UIFunctions.setSelect('ajax/tablassoporte', 'GetAllTipoConcepto', jsonParamTC, false, '#selectTipoConcepto1MCOSI, #selectTipoConcepto2MCOSI', 'cod_tipo_concepto');

                $('#txtCodigoMCOSI').val(jsonValue.cod_simil);
                $('#txtNombreMCOSI').val(jsonValue.nombre);

                $('#chkEquivalenteMCOSI').prop('checked', fnBoolean("flg_equivalente"));
                $('#chkDiasTrabajadosMCOSI').prop('checked', fnBoolean("flg_dias_trabajados"));
                $('#chkGrupoPrincipalMCOSI').prop('checked', fnBoolean("flg_grupo_principal"));

                $('#txtPorcentajeMinimoMCOSI').autoNumeric('set', UIFloat2(jsonValue.porcen_minimo));

                $('#selectTipoConcepto1MCOSI').val(UIValue(jsonValue.tipo_concepto1, '--1'));
                $('#selectTipoConcepto2MCOSI').val(UIValue(jsonValue.tipo_concepto2, '--1'));

                $('#btnGuardarMCOSI').off('click').on('click', function (e) {
                    var jsonParam = {
                        Accion: null,
                        CodigoSimilar: null,
                        Nombre: $('#txtNombreMCOSI').val(),
                        FlgEquivalente: UIFunctions.getValue.HTML('#chkEquivalenteMCOSI'),
                        FlgDiasTrabajados: UIFunctions.getValue.HTML('#chkDiasTrabajadosMCOSI'),
                        FlgGrupoPrincipal: UIFunctions.getValue.HTML('#chkGrupoPrincipalMCOSI'),
                        PorcetajeMinimo: $('#txtPorcentajeMinimoMCOSI').val(),
                        TipoConcepto1: $('#selectTipoConcepto1MCOSI').val(),
                        TipoConcepto2: $('#selectTipoConcepto2MCOSI').val(),
                    };
                    if (jsonValue.isNew) {
                        jsonParam.Accion = 'N';
                        jsonParam.CodigoSimilar = $('#txtCodigoMCOSI').val();
                    }
                    else {
                        jsonParam.Accion = 'M';
                        jsonParam.CodigoSimilar = jsonValue.cod_simil;
                    }
                    if (UIEmpty(jsonParam.CodigoSimilar)) {
                        UIMensaje.CODIGO();
                        return;
                    }
                    if (UIEmpty(jsonParam.Nombre)) {
                        UIMensaje.NOMBRE();
                        return;
                    }
                    UIAjax.runFunctionAsync('ajax/tablassoporte', 'PostConceptosSimilares', jsonParam, function () {
                        Load.Planilla.ConceptosSimilares.Ready();
                        $('#modalConceptosSimilares').modal('hide');
                    });
                });

                $('#modalConceptosSimilares').modal('show');
                UILoadingDefault();
            }
        };        
        Load.Planilla.ConceptosRTPS = {
            Ready: async function () {
                Load.ShowCard('#cardConceptosRTPS');
                await UIAjax.invokeMethodAsync('ajax/tablassoporte', 'GetAllConceptosRTPS', {}).then(function (r) {
                    tableConceptosRTPS = $('#tableConceptosRTPS').DataTable({
                        data: r.data,
                        initComplete: function (settings, json) {
                            UIDataTable.SelectRows(this, false, false, Load.Planilla.ConceptosRTPS.Modal);
                            UIDataTable.AJAXDelete(this, '#btnEliminarConceptosRTPS', 'valor', 'ajax/tablassoporte', 'DeleteConceptosRTPS', {
                                Codigo: 'valor',
                            }, Load.Planilla.ConceptosRTPS.Ready);
                        },
                        dom: 'Bfrtip',
                        buttons: [],
                        columnDefs: [
                            { targets: [1], className: 'text-left' },
                            { targets: '_all', className: 'text-center' },
                        ],
                        columns: [
                            { data: "valor" },
                            { data: "valor_nombre" },
                        ],
                    });
                });
            },
            Modal: async function (jsonValue) {
                await UILoadingDefault("show");
                if (jsonValue.isNew) {
                    $('#h4TitleMCORTPS').html('NUEVO CONCEPTO RTPS');
                    $('#txtCodigoMCORTPS').removeAttr('disabled');
                }
                else {
                    $('#h4TitleMCORTPS').html(`CONCEPTO RTPS: ${jsonValue.valor}`);
                    $('#txtCodigoMCORTPS').attr('disabled', 'disabled');
                }

                var fnBoolean = function (key) { return jsonValue[key] == "S" ? true : false; };

                $('#txtCodigoMCORTPS').val(jsonValue.valor);
                $('#txtNombreMCORTPS').val(jsonValue.valor_nombre);

                $('#btnGuardarMCORTPS').off('click').on('click', function (e) {
                    var jsonParam = {
                        Accion: null,
                        Codigo: null,
                        Nombre: $('#txtNombreMCORTPS').val(),
                    };
                    if (jsonValue.isNew) {
                        jsonParam.Accion = 'N';
                        jsonParam.Codigo = $('#txtCodigoMCORTPS').val();
                    }
                    else {
                        jsonParam.Accion = 'M';
                        jsonParam.Codigo = jsonValue.valor;
                    }
                    if (UIEmpty(jsonParam.Codigo)) {
                        UIMensaje.CODIGO();
                        return;
                    }
                    if (UIEmpty(jsonParam.Nombre)) {
                        UIMensaje.NOMBRE();
                        return;
                    }
                    UIAjax.runFunctionAsync('ajax/tablassoporte', 'PostConceptosRTPS', jsonParam, function () {
                        Load.Planilla.ConceptosRTPS.Ready();
                        $('#modalConceptosRTPS').modal('hide');
                    });
                });

                $('#modalConceptosRTPS').modal('show');
                UILoadingDefault();
            }
        };
        Load.Planilla.ModalidadContrato = {
            Ready: async function () {
                Load.ShowCard('#cardModalidadContrato');
                await UIAjax.invokeMethodAsync('ajax/tablassoporte', 'GetAllModalidadContrato', {}).then(function (r) {
                    tableModalidadContrato = $('#tableModalidadContrato').DataTable({
                        data: r.data,
                        initComplete: function (settings, json) {
                            UIDataTable.SelectRows(this, false, false, Load.Planilla.ModalidadContrato.Modal);
                            UIDataTable.AJAXDelete(this, '#btnEliminarModalidadContrato', 'cod_modalidad_contrato', 'ajax/tablassoporte', 'DeleteModalidadContrato', {
                                CodigoModalidadContrato: 'cod_modalidad_contrato',
                            }, Load.Planilla.ModalidadContrato.Ready);
                        },
                        columnDefs: [
                            { targets: [1], className: 'text-left' },
                            { targets: '_all', className: 'text-center' },
                        ],
                        columns: [
                            { data: "cod_modalidad_contrato" },
                            { data: "nombre" },
                            { data: "tope_anos" },
                            { data: "tope_meses" },
                            { data: "tope_dias" },
                        ],
                    });
                });
            },
            Modal: async function (jsonValue) {
                await UILoadingDefault("show");
                if (jsonValue.isNew) {
                    $('#h4TitleMMODACO').html('NUEVA MODALIDAD DE CONTRATO');
                    $('#txtCodigoMMODACO').removeAttr('disabled');
                }
                else {
                    $('#h4TitleMMODACO').html(`MODALIDAD DE CONTRATO: ${jsonValue.cod_modalidad_contrato}`);
                    $('#txtCodigoMMODACO').attr('disabled', 'disabled');
                }
                UIAutoNumeric.Init('input.txtTopeMMODACO', { vMin: '0', vMax: '100', mDec: '0' });
                var fnBoolean = function (key) { return jsonValue[key] == "S" ? true : false; };

                $('#txtCodigoMMODACO').val(jsonValue.cod_modalidad_contrato);
                $('#txtNombreMMODACO').val(jsonValue.nombre);

                $('#txtTopeAnosMMODACO').val(jsonValue.tope_anos);
                $('#txtTopeMesesMMODACO').val(jsonValue.tope_meses);
                $('#txtTopeDiasMMODACO').val(jsonValue.tope_dias);

                $('#btnGuardarMMODACO').off('click').on('click', function (e) {
                    var jsonParam = {
                        Accion: null,
                        CodigoModalidadContrato: null,
                        Nombre: $('#txtNombreMMODACO').val(),
                        TopeAnos: $('#txtTopeAnosMMODACO').val(),
                        TopeMeses: $('#txtTopeMesesMMODACO').val(),
                        TopeDias: $('#txtTopeDiasMMODACO').val(),
                    };
                    if (jsonValue.isNew) {
                        jsonParam.Accion = 'N';
                        jsonParam.CodigoModalidadContrato = $('#txtCodigoMMODACO').val();
                    }
                    else {
                        jsonParam.Accion = 'M';
                        jsonParam.CodigoModalidadContrato = jsonValue.cod_modalidad_contrato;
                    }
                    if (UIEmpty(jsonParam.CodigoModalidadContrato)) {
                        UIMensaje.CODIGO();
                        return;
                    }
                    if (UIEmpty(jsonParam.Nombre)) {
                        UIMensaje.NOMBRE();
                        return;
                    }
                    UIAjax.runFunctionAsync('ajax/tablassoporte', 'PostModalidadContrato', jsonParam, function () {
                        Load.Planilla.ModalidadContrato.Ready();
                        $('#modalModalidadContrato').modal('hide');
                    });
                });

                $('#modalModalidadContrato').modal('show');
                UILoadingDefault();
            }
        };
        Load.Planilla.CuentasEquivalencias = {
            Ready: async function () {
                Load.ShowCard('#cardCuentasEquivalencias');
                await UIAjax.invokeMethodAsync('ajax/tablassoporte', 'GetAllCuentasEquivalencias', {}).then(function (r) {
                    tableCuentasEquivalencias = $('#tableCuentasEquivalencias').DataTable({
                        data: r.data,
                        initComplete: function (settings, json) {
                            UIDataTable.SelectRows(this, false, false, Load.Planilla.CuentasEquivalencias.Modal);
                            UIDataTable.AJAXDelete(this, '#btnEliminarCuentasEquivalencias', 'cuenta', 'ajax/tablassoporte', 'DeleteCuentasEquivalencias', {
                                CodigoCuenta: 'cuenta',
                            }, Load.Planilla.CuentasEquivalencias.Ready);
                        },
                        columnDefs: [
                            { targets: [1], className: 'text-left' },
                            { targets: '_all', className: 'text-center' },
                        ],
                        columns: [
                            { data: "cuenta" },
                            { data: "equivalencia" },
                            { data: "descripcion" },
                        ],
                    });
                });
            },
            Modal: async function (jsonValue) {
                await UILoadingDefault("show");
                if (jsonValue.isNew) {
                    $('#h4TitleMCUEQ').html('NUEVA QUIVALENCIA PLAN DE CUENTAS');
                    $('#txtCodigoMCUEQ').removeAttr('disabled');
                }
                else {
                    $('#h4TitleMCUEQ').html(`QUIVALENCIA PLAN DE CUENTAS: ${jsonValue.cuenta}`);
                    $('#txtCodigoMCUEQ').attr('disabled', 'disabled');
                }
                var fnBoolean = function (key) { return jsonValue[key] == "S" ? true : false; };

                $('#txtCodigoMCUEQ').val(jsonValue.cuenta);
                $('#txtEquivalenciaMCUEQ').val(jsonValue.equivalencia);
                $('#txtDescripcionMCUEQ').val(jsonValue.descripcion);

                $('#btnGuardarMCUEQ').off('click').on('click', function (e) {
                    var jsonParam = {
                        Accion: null,
                        CodigoCuenta: null,
                        Equivalencia: $('#txtEquivalenciaMCUEQ').val(),
                        Descripcion: $('#txtDescripcionMCUEQ').val(),
                    };
                    if (jsonValue.isNew) {
                        jsonParam.Accion = 'N';
                        jsonParam.CodigoCuenta = $('#txtCodigoMCUEQ').val();
                    }
                    else {
                        jsonParam.Accion = 'M';
                        jsonParam.CodigoCuenta = jsonValue.cuenta;
                    }
                    if (UIEmpty(jsonParam.CodigoCuenta)) {
                        UIMensaje.CODIGO();
                        return;
                    }
                    UIAjax.runFunctionAsync('ajax/tablassoporte', 'PostCuentasEquivalencias', jsonParam, function () {
                        Load.Planilla.CuentasEquivalencias.Ready();
                        $('#modalCuentasEquivalencias').modal('hide');
                    });
                });

                $('#modalCuentasEquivalencias').modal('show');
                UILoadingDefault();
            }
        };
    }
    else {
        console.warn("No se encontró la propiedad 'Planilla' en la variable 'Load'");
    }
}
else {
    console.warn("No se encontró la variable 'Load'");
}