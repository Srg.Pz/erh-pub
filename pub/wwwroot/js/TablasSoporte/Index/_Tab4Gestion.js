if (typeof Load != undefined) {
    if (typeof Load.Gestion != undefined) {
        Load.Gestion.TipoDatoMedico = {
            Ready: async function () {
                Load.ShowCard('#cardTipoDatoMedico');
                await UIAjax.invokeMethodAsync('ajax/tablassoporte', 'GetAllTipoDatoMedico', {}).then(function (r) {
                    tableTipoDatoMedico = $('#tableTipoDatoMedico').DataTable({
                        data: r.data,
                        initComplete: function (settings, json) {
                            UIDataTable.SelectRows(this, false, false, Load.Gestion.TipoDatoMedico.Modal);
                            UIDataTable.AJAXDelete(this, '#btnEliminarTipoDatoMedico', 'cod_tipo_dato_medico', 'ajax/tablassoporte', 'DeleteTipoDatoMedico', {
                                CodigoDeTipo: 'cod_tipo_dato_medico',
                            }, Load.Gestion.TipoDatoMedico.Ready);
                        },
                        dom: 'Bfrtip',
                        buttons: [],
                        columns: [
                            { data: "cod_tipo_dato_medico" },
                            { data: "descripcion" },
                            { data: "abreviatura" },
                        ],
                    });
                });
            },
            Modal: async function (jsonValue) {
                await UILoadingDefault("show");
                if (jsonValue.isNew) {
                    $('#h4TitleMTIDAME').html('NUEVO TIPO DATO MÉDICO');
                    $('#txtCodigoMTIDAME').removeAttr('disabled');
                }
                else {
                    $('#h4TitleMTIDAME').html(`TIPO DATO MÉDICO: ${jsonValue.cod_tipo_dato_medico}`);
                    $('#txtCodigoMTIDAME').attr('disabled', 'disabled');
                }

                $('#txtCodigoMTIDAME').val(jsonValue.cod_tipo_dato_medico);
                $('#txtDescripcionMTIDAME').val(jsonValue.descripcion);
                $('#txtAbreviaturaMTIDAME').val(jsonValue.abreviatura);

                $('#btnGuardarMTIDAME').off('click').on('click', function (e) {
                    var jsonParam = {
                        Accion: null,
                        CodigoDeTipo: null,
                        Descripcion: $('#txtDescripcionMTIDAME').val(),
                        Abreviatura: $('#txtAbreviaturaMTIDAME').val(),
                    };
                    if (jsonValue.isNew) {
                        jsonParam.Accion = 'N';
                        jsonParam.CodigoDeTipo = $('#txtCodigoMTIDAME').val();
                    }
                    else {
                        jsonParam.Accion = 'M';
                        jsonParam.CodigoDeTipo = jsonValue.cod_tipo_dato_medico;
                    }
                    if (UIEmpty(jsonParam.CodigoDeTipo)) {
                        MsgBox('El código no puede ser vacío.', 'warning', true);
                        return;
                    }
                    if (UIEmpty(jsonParam.Descripcion)) {
                        MsgBox('La descripcion no puede ser vacía.', 'warning', true);
                        return;
                    }
                    UIAjax.runFunctionAsync('ajax/tablassoporte', 'PostTipoDatoMedico', jsonParam, function () {
                        Load.Gestion.TipoDatoMedico.Ready();
                        $('#modalTipoDatoMedico').modal('hide');
                    });
                });

                $('#modalTipoDatoMedico').modal('show');
                UILoadingDefault();
            }
        };
        Load.Gestion.SubTipoDatoMedico = {
            Ready: async function () {
                Load.ShowCard('#cardSubTipoDatoMedico');
                await UIAjax.invokeMethodAsync('ajax/tablassoporte', 'GetAllSubTipoDatoMedico', {}).then(function (r) {
                    tableSubTipoDatoMedico = $('#tableSubTipoDatoMedico').DataTable({
                        data: r.data,
                        initComplete: function (settings, json) {
                            UIDataTable.SelectRows(this, false, false, Load.Gestion.SubTipoDatoMedico.Modal);
                            UIDataTable.AJAXDelete(this, '#btnEliminarSubTipoDatoMedico', 'cod_subtipo_dato_medico', 'ajax/tablassoporte', 'DeleteSubTipoDatoMedico', {
                                CodigoDeSubTipo: 'cod_subtipo_dato_medico',
                            }, Load.Gestion.SubTipoDatoMedico.Ready);
                        },
                        dom: 'Bfrtip',
                        buttons: [],
                        columns: [
                            { data: "cod_subtipo_dato_medico" },
                            { data: "cod_tipo_dato_medico" },
                            { data: "descripcion" },
                            { data: "abreviatura" },
                        ],
                    });
                });
            },
            Modal: async function (jsonValue) {
                UILoadingDefault();
                if (jsonValue.isNew) {
                    $('#h4TitleMSUTIDAME').html('NUEVO SUBTIPO DATO MÉDICO');
                    $('#txtCodigoMSUTIDAME').removeAttr('disabled');
                }
                else {
                    $('#h4TitleMSUTIDAME').html(`SUBTIPO DATO MÉDICO: ${jsonValue.cod_subtipo_dato_medico}`);
                    $('#txtCodigoMSUTIDAME').attr('disabled', 'disabled');
                }

                $('#txtCodigoMSUTIDAME').val(jsonValue.cod_subtipo_dato_medico);
                await UIFunctions._setPrivateSelect('ajax/tablassoporte', 'GetAllTipoDatoMedico', null, false, '#selectTipoDatoMedicoMSUTIDAME', 'cod_tipo_dato_medico', 'descripcion');

                //'ajax/tablassoporte', 'GetAllSubTipoDatoMedico', {}
                $('#selectTipoDatoMedicoMSUTIDAME').val(jsonValue.cod_tipo_dato_medico);
                $('#txtDescripcionMSUTIDAME').val(jsonValue.descripcion);
                $('#txtAbreviaturaMSUTIDAME').val(jsonValue.abreviatura);

                $('#btnGuardarMSUTIDAME').off('click').on('click', function (e) {
                    var jsonParam = {
                        Accion: null,
                        CodigoDeSubTipo: null,
                        CodigoDeTipo: $('#selectTipoDatoMedicoMSUTIDAME').val(),
                        Descripcion: $('#txtDescripcionMSUTIDAME').val(),
                        Abreviatura: $('#txtAbreviaturaMSUTIDAME').val(),
                    };
                    if (jsonValue.isNew) {
                        jsonParam.Accion = 'N';
                        jsonParam.CodigoDeSubTipo = $('#txtCodigoMSUTIDAME').val();
                    }
                    else {
                        jsonParam.Accion = 'M';
                        jsonParam.CodigoDeSubTipo = jsonValue.cod_subtipo_dato_medico;
                    }
                    if (UIEmpty(jsonParam.CodigoDeSubTipo)) {
                        MsgBox('El código no puede ser vacío.', 'warning', true);
                        return;
                    }
                    if (UIEmpty(jsonParam.CodigoDeTipo)) {
                        MsgBox('El tipo de dato médico no puede ser vacío.', 'warning', true);
                        return;
                    }
                    if (UIEmpty(jsonParam.Descripcion)) {
                        MsgBox('La descripcion no puede ser vacía.', 'warning', true);
                        return;
                    }
                    UIAjax.runFunctionAsync('ajax/tablassoporte', 'PostSubTipoDatoMedico', jsonParam, function () {
                        Load.Gestion.SubTipoDatoMedico.Ready();
                        $('#modalSubTipoDatoMedico').modal('hide');
                    });
                });

                $('#modalSubTipoDatoMedico').modal('show');
                UILoadingDefault();
            }
        };
        Load.Gestion.TipoEvento = {
            Ready: async function () {
                Load.ShowCard('#cardTipoEvento');
                await UIAjax.invokeMethodAsync('ajax/tablassoporte', 'GetAllTipoEvento', {}).then(function (r) {
                    tableTipoEvento = $('#tableTipoEvento').DataTable({
                        data: r.data,
                        initComplete: function (settings, json) {
                            UIDataTable.SelectRows(this, false, false, Load.Gestion.TipoEvento.Modal);
                            UIDataTable.AJAXDelete(this, '#btnEliminarTipoEvento', 'cod_tipo_evento', 'ajax/tablassoporte', 'DeleteTipoEvento', {
                                CodigoTipoEvento: 'cod_tipo_evento',
                            }, Load.Gestion.TipoEvento.Ready);
                        },
                        columns: [
                            { data: "cod_tipo_evento" },
                            { data: "descripcion" },
                        ],
                    });
                });
            },
            Modal: async function (jsonValue) {
                UILoadingDefault();
                if (jsonValue.isNew) {
                    $('#h4TitleMTIEV').html('NUEVO TIPO EVENTO');
                    $('#txtCodigoMTIEV').removeAttr('disabled');
                }
                else {
                    $('#h4TitleMTIEV').html(`TIPO EVENTO: ${jsonValue.cod_tipo_evento}`);
                    $('#txtCodigoMTIEV').attr('disabled', 'disabled');
                }

                $('#txtCodigoMTIEV').val(jsonValue.cod_tipo_evento);
                $('#txtDescripcionMTIEV').val(jsonValue.descripcion);

                $('#btnGuardarMTIEV').off('click').on('click', function (e) {
                    var jsonParam = {
                        Accion: null,
                        CodigoTipoEvento: null,
                        Descripcion: $('#txtDescripcionMTIEV').val(),
                    };
                    if (jsonValue.isNew) {
                        jsonParam.Accion = 'N';
                        jsonParam.CodigoTipoEvento = $('#txtCodigoMTIEV').val();
                    }
                    else {
                        jsonParam.Accion = 'M';
                        jsonParam.CodigoTipoEvento = jsonValue.cod_tipo_evento;
                    }
                    if (UIEmpty(jsonParam.CodigoTipoEvento)) {
                        UIMensaje.CODIGO();
                        return;
                    }
                    if (UIEmpty(jsonParam.Descripcion)) {
                        UIMensaje.DESCRIPCION();
                        return;
                    }
                    UIAjax.runFunctionAsync('ajax/tablassoporte', 'PostTipoEvento', jsonParam, function () {
                        Load.Gestion.TipoEvento.Ready();
                        $('#modalTipoEvento').modal('hide');
                    });
                });

                $('#modalTipoEvento').modal('show');
                UILoadingDefault();
            }
        };
        Load.Gestion.ClaseDocumento = {
            Ready: async function () {
                Load.ShowCard('#cardClaseDocumento');
                await UIAjax.invokeMethodAsync('ajax/tablassoporte', 'GetAllClaseDocumento', {}).then(function (r) {
                    tableClaseDocumento = $('#tableClaseDocumento').DataTable({
                        data: r.data,
                        initComplete: function (settings, json) {
                            UIDataTable.SelectRows(this, false, false, Load.Gestion.ClaseDocumento.Modal);
                            UIDataTable.AJAXDelete(this, '#btnEliminarClaseDocumento', 'cod_clase_documento', 'ajax/tablassoporte', 'DeleteClaseDocumento', {
                                CodigoClaseDocumento: 'cod_clase_documento',
                            }, Load.Gestion.ClaseDocumento.Ready);
                        },
                        columns: [
                            { data: "cod_clase_documento" },
                            { data: "descripcion" },
                        ],
                    });
                });
            },
            Modal: async function (jsonValue) {
                UILoadingDefault();
                if (jsonValue.isNew) {
                    $('#h4TitleMCLDO').html('NUEVO CLASE DOCUMENTO');
                    $('#txtCodigoMCLDO').removeAttr('disabled');
                }
                else {
                    $('#h4TitleMCLDO').html(`CLASE DOCUMENTO: ${jsonValue.cod_clase_documento}`);
                    $('#txtCodigoMCLDO').attr('disabled', 'disabled');
                }

                $('#txtCodigoMCLDO').val(jsonValue.cod_clase_documento);
                $('#txtDescripcionMCLDO').val(jsonValue.descripcion);

                $('#btnGuardarMCLDO').off('click').on('click', function (e) {
                    var jsonParam = {
                        Accion: null,
                        CodigoClaseDocumento: null,
                        Descripcion: $('#txtDescripcionMCLDO').val(),
                    };
                    if (jsonValue.isNew) {
                        jsonParam.Accion = 'N';
                        jsonParam.CodigoClaseDocumento = $('#txtCodigoMCLDO').val();
                    }
                    else {
                        jsonParam.Accion = 'M';
                        jsonParam.CodigoClaseDocumento = jsonValue.cod_clase_documento;
                    }
                    if (UIEmpty(jsonParam.CodigoClaseDocumento)) {
                        UIMensaje.CODIGO();
                        return;
                    }
                    if (UIEmpty(jsonParam.Descripcion)) {
                        UIMensaje.DESCRIPCION();
                        return;
                    }
                    UIAjax.runFunctionAsync('ajax/tablassoporte', 'PostClaseDocumento', jsonParam, function () {
                        Load.Gestion.ClaseDocumento.Ready();
                        $('#modalClaseDocumento').modal('hide');
                    });
                });

                $('#modalClaseDocumento').modal('show');
                UILoadingDefault();
            }
        };
        Load.Gestion.Particularidad = {
            Ready: async function () {
                Load.ShowCard('#cardParticularidad');
                await UIAjax.invokeMethodAsync('ajax/tablassoporte', 'GetAllParticularidad', {}).then(function (r) {
                    tableParticularidad = $('#tableParticularidad').DataTable({
                        data: r.data,
                        initComplete: function (settings, json) {
                            UIDataTable.SelectRows(this, false, false, Load.Gestion.Particularidad.Modal);
                            UIDataTable.AJAXDelete(this, '#btnEliminarParticularidad', 'cod_particularidad', 'ajax/tablassoporte', 'DeleteParticularidad', {
                                CodigoParticularidad: 'cod_particularidad',
                            }, Load.Gestion.Particularidad.Ready);
                        },
                        columns: [
                            { data: "cod_particularidad" },
                            { data: "descripcion" },
                        ],
                    });
                });
            },
            Modal: async function (jsonValue) {
                UIMensaje.DESARROLLO();
                return;
                await UILoadingDefault("show");
                if (jsonValue.isNew) {
                    $('#h4TitleMPARTI').html('NUEVA PARTICULARIDAD');
                    $('#txtCodigoMPARTI').removeAttr('disabled');
                }
                else {
                    $('#h4TitleMPARTI').html(`PARTICULARIDAD: ${jsonValue.cod_particularidad}`);
                    $('#txtCodigoMPARTI').attr('disabled', 'disabled');
                }

                $('#txtCodigoMPARTI').val(jsonValue.cod_particularidad);
                $('#txtDescripcionMPARTI').val(jsonValue.descripcion);

                $('#btnGuardarMPARTI').off('click').on('click', function (e) {
                    var jsonParam = {
                        Accion: null,
                        CodigoParticularidad: null,
                        Descripcion: $('#txtDescripcionMPARTI').val(),
                    };
                    if (jsonValue.isNew) {
                        jsonParam.Accion = 'N';
                        jsonParam.CodigoParticularidad = $('#txtCodigoMPARTI').val();
                    }
                    else {
                        jsonParam.Accion = 'M';
                        jsonParam.CodigoParticularidad = jsonValue.cod_particularidad;
                    }
                    if (UIEmpty(jsonParam.CodigoParticularidad)) {
                        UIMensaje.CODIGO();
                        return;
                    }
                    if (UIEmpty(jsonParam.Descripcion)) {
                        UIMensaje.DESCRIPCION();
                        return;
                    }
                    UIAjax.runFunctionAsync('ajax/tablassoporte', 'PostParticularidad', jsonParam, function () {
                        Load.Gestion.Particularidad.Ready();
                        $('#modalParticularidad').modal('hide');
                    });
                });

                $('#modalParticularidad').modal('show');
                UILoadingDefault();
            }
        };
        Load.Gestion.TipoAsociacion = {
            Ready: async function () {
                Load.ShowCard('#cardTipoAsociacion');
                await UIAjax.invokeMethodAsync('ajax/tablassoporte', 'GetAllTipoAsociacion', {}).then(function (r) {
                    tableTipoAsociacion = $('#tableTipoAsociacion').DataTable({
                        data: r.data,
                        initComplete: function (settings, json) {
                            UIDataTable.SelectRows(this, false, false, Load.Gestion.TipoAsociacion.Modal);
                            UIDataTable.AJAXDelete(this, '#btnEliminarTipoAsociacion', 'cod_tipo_asociacion', 'ajax/tablassoporte', 'DeleteTipoAsociacion', {
                                CodigoTipoAsociacion: 'cod_tipo_asociacion',
                            }, Load.Gestion.TipoAsociacion.Ready);
                        },
                        columns: [
                            { data: "cod_tipo_asociacion" },
                            { data: "descripcion" },
                        ],
                    });
                });
            },
            Modal: async function (jsonValue) {
                await UILoadingDefault("show");
                if (jsonValue.isNew) {
                    $('#h4TitleMTIAS').html('NUEVO TIPO ASOCIACIÓN');
                    $('#txtCodigoMTIAS').removeAttr('disabled');
                }
                else {
                    $('#h4TitleMTIAS').html(`TIPO ASOCIACIÓN: ${jsonValue.cod_tipo_asociacion}`);
                    $('#txtCodigoMTIAS').attr('disabled', 'disabled');
                }

                $('#txtCodigoMTIAS').val(jsonValue.cod_tipo_asociacion);
                $('#txtDescripcionMTIAS').val(jsonValue.descripcion);

                $('#btnGuardarMTIAS').off('click').on('click', function (e) {
                    var jsonParam = {
                        Accion: null,
                        CodigoTipoAsociacion: null,
                        Descripcion: $('#txtDescripcionMTIAS').val(),
                    };
                    if (jsonValue.isNew) {
                        jsonParam.Accion = 'N';
                        jsonParam.CodigoTipoAsociacion = $('#txtCodigoMTIAS').val();
                    }
                    else {
                        jsonParam.Accion = 'M';
                        jsonParam.CodigoTipoAsociacion = jsonValue.cod_tipo_asociacion;
                    }
                    if (UIEmpty(jsonParam.CodigoTipoAsociacion)) {
                        UIMensaje.CODIGO();
                        return;
                    }
                    if (UIEmpty(jsonParam.Descripcion)) {
                        UIMensaje.DESCRIPCION();
                        return;
                    }
                    UIAjax.runFunctionAsync('ajax/tablassoporte', 'PostTipoAsociacion', jsonParam, function () {
                        Load.Gestion.TipoAsociacion.Ready();
                        $('#modalTipoAsociacion').modal('hide');
                    });
                });

                $('#modalTipoAsociacion').modal('show');
                UILoadingDefault();
            }
        };
        Load.Gestion.TipoPublicacion = {
            Ready: async function () {
                Load.ShowCard('#cardTipoPublicacion');
                await UIAjax.invokeMethodAsync('ajax/tablassoporte', 'GetAllTipoPublicacion', {}).then(function (r) {
                    tableTipoPublicacion = $('#tableTipoPublicacion').DataTable({
                        data: r.data,
                        initComplete: function (settings, json) {
                            UIDataTable.SelectRows(this, false, false, Load.Gestion.TipoPublicacion.Modal);
                            UIDataTable.AJAXDelete(this, '#btnEliminarTipoPublicacion', 'cod_tipo_publicacion', 'ajax/tablassoporte', 'DeleteTipoPublicacion', {
                                CodigoTipoPublicacion: 'cod_tipo_publicacion',
                            }, Load.Gestion.TipoPublicacion.Ready);
                        },
                        columns: [
                            { data: "cod_tipo_publicacion" },
                            { data: "descripcion" },
                        ],
                    });
                });
            },
            Modal: async function (jsonValue) {
                await UILoadingDefault("show");
                if (jsonValue.isNew) {
                    $('#h4TitleMTIPU').html('NUEVO TIPO ASOCIACIÓN');
                    $('#txtCodigoMTIPU').removeAttr('disabled');
                }
                else {
                    $('#h4TitleMTIPU').html(`TIPO ASOCIACIÓN: ${jsonValue.cod_tipo_publicacion}`);
                    $('#txtCodigoMTIPU').attr('disabled', 'disabled');
                }

                $('#txtCodigoMTIPU').val(jsonValue.cod_tipo_publicacion);
                $('#txtDescripcionMTIPU').val(jsonValue.descripcion);

                $('#btnGuardarMTIPU').off('click').on('click', function (e) {
                    var jsonParam = {
                        Accion: null,
                        CodigoTipoPublicacion: null,
                        Descripcion: $('#txtDescripcionMTIPU').val(),
                    };
                    if (jsonValue.isNew) {
                        jsonParam.Accion = 'N';
                        jsonParam.CodigoTipoPublicacion = $('#txtCodigoMTIPU').val();
                    }
                    else {
                        jsonParam.Accion = 'M';
                        jsonParam.CodigoTipoPublicacion = jsonValue.cod_tipo_publicacion;
                    }
                    if (UIEmpty(jsonParam.CodigoTipoPublicacion)) {
                        UIMensaje.CODIGO();
                        return;
                    }
                    if (UIEmpty(jsonParam.Descripcion)) {
                        UIMensaje.DESCRIPCION();
                        return;
                    }
                    UIAjax.runFunctionAsync('ajax/tablassoporte', 'PostTipoPublicacion', jsonParam, function () {
                        Load.Gestion.TipoPublicacion.Ready();
                        $('#modalTipoPublicacion').modal('hide');
                    });
                });
                $('#modalTipoPublicacion').modal('show');
                UILoadingDefault();
            }
        };
        Load.Gestion.Denominacion = {
            Ready: async function () {
                Load.ShowCard('#cardDenominacion');
                await UIAjax.invokeMethodAsync('ajax/tablassoporte', 'GetAllDenominacion', {}).then(function (r) {
                    tableDenominacion = $('#tableDenominacion').DataTable({
                        data: r.data,
                        initComplete: function (settings, json) {
                            UIDataTable.SelectRows(this, false, false, Load.Gestion.Denominacion.Modal);
                            UIDataTable.AJAXDelete(this, '#btnEliminarDenominacion', 'cod_denominacion', 'ajax/tablassoporte', 'DeleteDenominacion', {
                                CodigoDenominacion: 'cod_denominacion',
                            }, Load.Gestion.Denominacion.Ready);
                        },
                        columns: [
                            { data: "cod_denominacion" },
                            { data: "descripcion" },
                        ],
                    });
                });
            },
            Modal: async function (jsonValue) {
                await UILoadingDefault("show");
                if (jsonValue.isNew) {
                    $('#h4TitleMDENO').html('NUEVO DENOMINACIÓN');
                    $('#txtCodigoMDENO').removeAttr('disabled');
                }
                else {
                    $('#h4TitleMDENO').html(`DENOMINACIÓN: ${jsonValue.cod_denominacion}`);
                    $('#txtCodigoMDENO').attr('disabled', 'disabled');
                }

                $('#txtCodigoMDENO').val(jsonValue.cod_denominacion);
                $('#txtDescripcionMDENO').val(jsonValue.descripcion);

                $('#btnGuardarMDENO').off('click').on('click', function (e) {
                    var jsonParam = {
                        Accion: null,
                        CodigoDenominacion: null,
                        Descripcion: $('#txtDescripcionMDENO').val(),
                    };
                    if (jsonValue.isNew) {
                        jsonParam.Accion = 'N';
                        jsonParam.CodigoDenominacion = $('#txtCodigoMDENO').val();
                    }
                    else {
                        jsonParam.Accion = 'M';
                        jsonParam.CodigoDenominacion = jsonValue.cod_denominacion;
                    }
                    if (UIEmpty(jsonParam.CodigoDenominacion)) {
                        UIMensaje.CODIGO();
                        return;
                    }
                    if (UIEmpty(jsonParam.Descripcion)) {
                        UIMensaje.DESCRIPCION();
                        return;
                    }
                    UIAjax.runFunctionAsync('ajax/tablassoporte', 'PostDenominacion', jsonParam, function () {
                        Load.Gestion.Denominacion.Ready();
                        $('#modalDenominacion').modal('hide');
                    });
                });
                $('#modalDenominacion').modal('show');
                UILoadingDefault();
            }
        };
        Load.Gestion.TipoNota = {
            Ready: async function () {
                Load.ShowCard('#cardTipoNota');
                await UIAjax.invokeMethodAsync('ajax/tablassoporte', 'GetAllTipoNota', {}).then(function (r) {
                    tableTipoNota = $('#tableTipoNota').DataTable({
                        data: r.data,
                        initComplete: function (settings, json) {
                            UIDataTable.SelectRows(this, false, false, Load.Gestion.TipoNota.Modal);
                            UIDataTable.AJAXDelete(this, '#btnEliminarTipoNota', 'cod_tipo_nota', 'ajax/tablassoporte', 'DeleteTipoNota', {
                                CodigoTipoNota: 'cod_tipo_nota',
                            }, Load.Gestion.TipoNota.Ready);
                        },
                        columns: [
                            { data: "cod_tipo_nota" },
                            { data: "descripcion" },
                            { data: "abrv" },
                            { data: "tipo_dec" },
                        ],
                    });
                });
            },
            Modal: async function (jsonValue) {
                await UILoadingDefault("show");
                if (jsonValue.isNew) {
                    $('#h4TitleMTINO').html('NUEVO TIPO NOTA');
                    $('#txtCodigoMTINO').removeAttr('disabled');
                }
                else {
                    $('#h4TitleMTINO').html(`TIPO NOTA: ${jsonValue.cod_tipo_nota}`);
                    $('#txtCodigoMTINO').attr('disabled', 'disabled');
                }

                UIAutoNumeric.Init('input.txtValorMTINO', { vMin: '0.0000', });

                $('#txtCodigoMTINO').val(jsonValue.cod_tipo_nota);
                $('#txtDescripcionMTINO').val(jsonValue.descripcion);
                $('#txtObservacionMTINO').val(jsonValue.abrv);
                $('#txtValorMTINO').autoNumeric('set', UIFloat2(jsonValue.tipo_dec, 4));
                //$('#txtValorMTINO').val(jsonValue.tipo_desc);

                $('#btnGuardarMTINO').off('click').on('click', function (e) {
                    var jsonParam = {
                        Accion: null,
                        CodigoTipoNota: null,
                        Descripcion: $('#txtDescripcionMTINO').val(),
                        Observacion: $('#txtObservacionMTINO').val(),
                        Valor: $('#txtValorMTINO').val(),
                    };
                    if (jsonValue.isNew) {
                        jsonParam.Accion = 'N';
                        jsonParam.CodigoTipoNota = $('#txtCodigoMTINO').val();
                    }
                    else {
                        jsonParam.Accion = 'M';
                        jsonParam.CodigoTipoNota = jsonValue.cod_tipo_nota;
                    }
                    if (UIEmpty(jsonParam.CodigoTipoNota)) {
                        UIMensaje.CODIGO();
                        return;
                    }
                    if (UIEmpty(jsonParam.Descripcion)) {
                        UIMensaje.DESCRIPCION();
                        return;
                    }
                    UIAjax.runFunctionAsync('ajax/tablassoporte', 'PostTipoNota', jsonParam, function () {
                        Load.Gestion.TipoNota.Ready();
                        $('#modalTipoNota').modal('hide');
                    });
                });
                $('#modalTipoNota').modal('show');
                UILoadingDefault();
            }
        };
        Load.Gestion.MetodoDireccion = {
            Ready: async function () {
                Load.ShowCard('#cardMetodoDireccion');
                await UIAjax.invokeMethodAsync('ajax/tablassoporte', 'GetAllMetodoDireccion', {}).then(function (r) {
                    tableMetodoDireccion = $('#tableMetodoDireccion').DataTable({
                        data: r.data,
                        initComplete: function (settings, json) {
                            UIDataTable.SelectRows(this, false, false, Load.Gestion.MetodoDireccion.Modal);
                            UIDataTable.AJAXDelete(this, '#btnEliminarMetodoDireccion', 'cod_met_direc', 'ajax/tablassoporte', 'DeleteMetodoDireccion', {
                                Codigo: 'cod_met_direc',
                            }, Load.Gestion.MetodoDireccion.Ready);
                        },
                        columns: [
                            { data: "cod_met_direc" },
                            { data: "cod_alterno" },
                            { data: "descripcion" },
                        ],
                    });
                });
            },
            Modal: async function (jsonValue) {
                await UILoadingDefault("show");
                if (jsonValue.isNew) {
                    $('#h4TitleMMEDI').html('NUEVO MÉTODO DIRECCIÓN');
                    $('#txtCodigoMMEDI').removeAttr('disabled');
                }
                else {
                    $('#h4TitleMMEDI').html(`MÉTODO DIRECCIÓN: ${jsonValue.cod_met_direc}`);
                    $('#txtCodigoMMEDI').attr('disabled', 'disabled');
                }

                $('#txtCodigoMMEDI').val(jsonValue.cod_met_direc);
                $('#txtDescripcionMMEDI').val(jsonValue.descripcion);
                $('#txtCodigoAlternoMMEDI').val(jsonValue.cod_alterno);

                $('#btnGuardarMMEDI').off('click').on('click', function (e) {
                    var jsonParam = {
                        Accion: null,
                        Codigo: null,
                        Descripcion: $('#txtDescripcionMMEDI').val(),
                        CodigoAlterno: $('#txtCodigoAlternoMMEDI').val(),
                    };
                    if (jsonValue.isNew) {
                        jsonParam.Accion = 'N';
                        jsonParam.Codigo = $('#txtCodigoMMEDI').val();
                    }
                    else {
                        jsonParam.Accion = 'M';
                        jsonParam.Codigo = jsonValue.cod_met_direc;
                    }
                    if (UIEmpty(jsonParam.Codigo)) {
                        UIMensaje.CODIGO();
                        return;
                    }
                    if (UIEmpty(jsonParam.Descripcion)) {
                        UIMensaje.DESCRIPCION();
                        return;
                    }
                    UIAjax.runFunctionAsync('ajax/tablassoporte', 'PostMetodoDireccion', jsonParam, function () {
                        Load.Gestion.MetodoDireccion.Ready();
                        $('#modalMetodoDireccion').modal('hide');
                    });
                });
                $('#modalMetodoDireccion').modal('show');
                UILoadingDefault();
            }
        };
        Load.Gestion.MedioTransporte = {
            Ready: async function () {
                Load.ShowCard('#cardMedioTransporte');
                await UIAjax.invokeMethodAsync('ajax/tablassoporte', 'GetAllMedioTransporte', {}).then(function (r) {
                    tableMedioTransporte = $('#tableMedioTransporte').DataTable({
                        data: r.data,
                        initComplete: function (settings, json) {
                            UIDataTable.SelectRows(this, false, false, Load.Gestion.MedioTransporte.Modal);
                            UIDataTable.AJAXDelete(this, '#btnEliminarMedioTransporte', 'cod_medio_trans', 'ajax/tablassoporte', 'DeleteMedioTransporte', {
                                Codigo: 'cod_medio_trans',
                            }, Load.Gestion.MedioTransporte.Ready);
                        },
                        columns: [
                            { data: "cod_medio_trans" },
                            { data: "descripcion" },
                        ],
                    });
                });
            },
            Modal: async function (jsonValue) {
                await UILoadingDefault("show");
                if (jsonValue.isNew) {
                    $('#h4TitleMMETRA').html('NUEVO MÉTODO DIRECCIÓN');
                    $('#txtCodigoMMETRA').removeAttr('disabled');
                }
                else {
                    $('#h4TitleMMETRA').html(`MÉTODO DIRECCIÓN: ${jsonValue.cod_medio_trans}`);
                    $('#txtCodigoMMETRA').attr('disabled', 'disabled');
                }

                $('#txtCodigoMMETRA').val(jsonValue.cod_medio_trans);
                $('#txtDescripcionMMETRA').val(jsonValue.descripcion);
                $('#txtCodigoAlternoMMETRA').val(jsonValue.cod_alterno);

                $('#btnGuardarMMETRA').off('click').on('click', function (e) {
                    var jsonParam = {
                        Accion: null,
                        Codigo: null,
                        Descripcion: $('#txtDescripcionMMETRA').val(),
                        CodigoAlterno: $('#txtCodigoAlternoMMETRA').val(),
                    };
                    if (jsonValue.isNew) {
                        jsonParam.Accion = 'N';
                        jsonParam.Codigo = $('#txtCodigoMMETRA').val();
                    }
                    else {
                        jsonParam.Accion = 'M';
                        jsonParam.Codigo = jsonValue.cod_medio_trans;
                    }
                    if (UIEmpty(jsonParam.Codigo)) {
                        UIMensaje.CODIGO();
                        return;
                    }
                    if (UIEmpty(jsonParam.Descripcion)) {
                        UIMensaje.DESCRIPCION();
                        return;
                    }
                    UIAjax.runFunctionAsync('ajax/tablassoporte', 'PostMedioTransporte', jsonParam, function () {
                        Load.Gestion.MedioTransporte.Ready();
                        $('#modalMedioTransporte').modal('hide');
                    });
                });
                $('#modalMedioTransporte').modal('show');
                UILoadingDefault();
            }
        };
        Load.Gestion.TipoTraslado = {
            Ready: async function () {
                Load.ShowCard('#cardTipoTraslado');
                await UIAjax.invokeMethodAsync('ajax/tablassoporte', 'GetAllTipoTraslado', {}).then(function (r) {
                    tableTipoTraslado = $('#tableTipoTraslado').DataTable({
                        data: r.data,
                        initComplete: function (settings, json) {
                            UIDataTable.SelectRows(this, false, false, Load.Gestion.TipoTraslado.Modal);
                            UIDataTable.AJAXDelete(this, '#btnEliminarTipoTraslado', 'cod_tipo_traslado', 'ajax/tablassoporte', 'DeleteTipoTraslado', {
                                Codigo: 'cod_tipo_traslado',
                            }, Load.Gestion.TipoTraslado.Ready);
                        },
                        columns: [
                            { data: "cod_tipo_traslado" },
                            { data: "descripcion" },
                        ],
                    });
                });
            },
            Modal: async function (jsonValue) {
                await UILoadingDefault("show");
                if (jsonValue.isNew) {
                    $('#h4TitleMTITRA').html('NUEVO MÉTODO DIRECCIÓN');
                    $('#txtCodigoMTITRA').removeAttr('disabled');
                }
                else {
                    $('#h4TitleMTITRA').html(`MÉTODO DIRECCIÓN: ${jsonValue.cod_tipo_traslado}`);
                    $('#txtCodigoMTITRA').attr('disabled', 'disabled');
                }

                $('#txtCodigoMTITRA').val(jsonValue.cod_tipo_traslado);
                $('#txtDescripcionMTITRA').val(jsonValue.descripcion);
                $('#txtCodigoAlternoMTITRA').val(jsonValue.cod_alterno);

                $('#btnGuardarMTITRA').off('click').on('click', function (e) {
                    var jsonParam = {
                        Accion: null,
                        Codigo: null,
                        Descripcion: $('#txtDescripcionMTITRA').val(),
                        CodigoAlterno: $('#txtCodigoAlternoMTITRA').val(),
                    };
                    if (jsonValue.isNew) {
                        jsonParam.Accion = 'N';
                        jsonParam.Codigo = $('#txtCodigoMTITRA').val();
                    }
                    else {
                        jsonParam.Accion = 'M';
                        jsonParam.Codigo = jsonValue.cod_tipo_traslado;
                    }
                    if (UIEmpty(jsonParam.Codigo)) {
                        UIMensaje.CODIGO();
                        return;
                    }
                    if (UIEmpty(jsonParam.Descripcion)) {
                        UIMensaje.DESCRIPCION();
                        return;
                    }
                    UIAjax.runFunctionAsync('ajax/tablassoporte', 'PostTipoTraslado', jsonParam, function () {
                        Load.Gestion.TipoTraslado.Ready();
                        $('#modalTipoTraslado').modal('hide');
                    });
                });
                $('#modalTipoTraslado').modal('show');
                UILoadingDefault();
            }
        };
        Load.Gestion.ConceptoGasto = {
            Ready: async function () {
                Load.ShowCard('#cardConceptoGasto');
                $('#h5ConceptoGasto').html(optionCard.html());
                var jsonParam = {
                    TipoGasto: selectCard.val(),
                };
                await UIAjax.invokeMethodAsync('ajax/tablassoporte', 'GetAllConceptoGasto', jsonParam).then(function (r) {
                    tableConceptoGasto = $('#tableConceptoGasto').DataTable({
                        data: r.data,
                        initComplete: function (settings, json) {
                            UIDataTable.SelectRows(this, false, false, Load.Gestion.ConceptoGasto.Modal);
                            UIDataTable.AJAXDelete(this, '#btnEliminarConceptoGasto', 'cod_cpto', 'ajax/tablassoporte', 'DeleteConceptoGasto', {
                                TipoGasto: 'tipo_gasto',
                                CodigoConcepto: 'cod_cpto',
                            }, Load.Gestion.ConceptoGasto.Ready);
                        },
                        columns: [
                            { data: "cod_cpto" },
                            { data: "descripcion" },
                        ],
                    });
                });
            },
            Modal: async function (jsonValue) {
                await UILoadingDefault("show");
                var title_name = optionCard.html().toUpperCase();
                if (jsonValue.isNew) {
                    $('#h4TitleMCOGA').html('NUEVO ' + title_name);
                    $('#txtCodigoMCOGA').removeAttr('disabled');
                }
                else {
                    $('#h4TitleMCOGA').html(`${title_name}: ${jsonValue.cod_cpto}`);
                    $('#txtCodigoMCOGA').attr('disabled', 'disabled');
                }

                $('#txtCodigoMCOGA').val(jsonValue.cod_cpto);
                $('#txtDescripcionMCOGA').val(jsonValue.descripcion);

                $('#btnGuardarMCOGA').off('click').on('click', function (e) {
                    var jsonParam = {
                        Accion: null,
                        CodigoConcepto: null,
                        TipoGasto: selectCard.val(),
                        Descripcion: $('#txtDescripcionMCOGA').val(),
                    };
                    if (jsonValue.isNew) {
                        jsonParam.Accion = 'N';
                        jsonParam.CodigoConcepto = $('#txtCodigoMCOGA').val();
                    }
                    else {
                        jsonParam.Accion = 'M';
                        jsonParam.CodigoConcepto = jsonValue.cod_cpto;
                    }
                    if (UIEmpty(jsonParam.CodigoConcepto)) {
                        UIMensaje.CODIGO();
                        return;
                    }
                    if (UIEmpty(jsonParam.Descripcion)) {
                        UIMensaje.DESCRIPCION();
                        return;
                    }
                    UIAjax.runFunctionAsync('ajax/tablassoporte', 'PostConceptoGasto', jsonParam, function () {
                        Load.Gestion.ConceptoGasto.Ready();
                        $('#modalConceptoGasto').modal('hide');
                    });
                });
                $('#modalConceptoGasto').modal('show');
                UILoadingDefault();
            }
        };
        Load.Gestion.ClaseFecha = {
            Ready: async function () {
                Load.ShowCard('#cardClaseFecha');
                await UIAjax.invokeMethodAsync('ajax/tablassoporte', 'GetAllClaseFecha', {}).then(function (r) {
                    tableClaseFecha = $('#tableClaseFecha').DataTable({
                        data: r.data,
                        initComplete: function (settings, json) {
                            UIDataTable.SelectRows(this, false, false, Load.Gestion.ClaseFecha.Modal);
                            UIDataTable.AJAXDelete(this, '#btnEliminarClaseFecha', 'cod_clase_fecha', 'ajax/tablassoporte', 'DeleteClaseFecha', {
                                Codigo: 'cod_clase_fecha',
                            }, Load.Gestion.ClaseFecha.Ready);
                        },
                        columns: [
                            { data: "cod_clase_fecha" },
                            { data: "descripcion" },
                        ],
                    });
                });
            },
            Modal: async function (jsonValue) {
                await UILoadingDefault("show");
                if (jsonValue.isNew) {
                    $('#h4TitleMCLFE').html('NUEVO CLASE FECHA');
                    $('#txtCodigoMCLFE').removeAttr('disabled');
                }
                else {
                    $('#h4TitleMCLFE').html(`CLASE FECHA: ${jsonValue.cod_clase_fecha}`);
                    $('#txtCodigoMCLFE').attr('disabled', 'disabled');
                }

                $('#txtCodigoMCLFE').val(jsonValue.cod_clase_fecha);
                $('#txtDescripcionMCLFE').val(jsonValue.descripcion);

                $('#btnGuardarMCLFE').off('click').on('click', function (e) {
                    var jsonParam = {
                        Accion: null,
                        Codigo: null,
                        Descripcion: $('#txtDescripcionMCLFE').val(),
                    };
                    if (jsonValue.isNew) {
                        jsonParam.Accion = 'N';
                        jsonParam.Codigo = $('#txtCodigoMCLFE').val();
                    }
                    else {
                        jsonParam.Accion = 'M';
                        jsonParam.Codigo = jsonValue.cod_clase_fecha;
                    }
                    if (UIEmpty(jsonParam.Codigo)) {
                        UIMensaje.CODIGO();
                        return;
                    }
                    if (UIEmpty(jsonParam.Descripcion)) {
                        UIMensaje.DESCRIPCION();
                        return;
                    }
                    UIAjax.runFunctionAsync('ajax/tablassoporte', 'PostClaseFecha', jsonParam, function () {
                        Load.Gestion.ClaseFecha.Ready();
                        $('#modalClaseFecha').modal('hide');
                    });
                });
                $('#modalClaseFecha').modal('show');
                UILoadingDefault();
            }
        };
        Load.Gestion.DocumentoEscaneado = {
            Ready: async function () {
                Load.ShowCard('#cardDocumentoEscaneado');
                await UIAjax.invokeMethodAsync('ajax/tablassoporte', 'GetAllDocumentoEscaneado', {}).then(function (r) {
                    tableDocumentoEscaneado = $('#tableDocumentoEscaneado').DataTable({
                        data: r.data,
                        initComplete: function (settings, json) {
                            UIDataTable.SelectRows(this, false, false, Load.Gestion.DocumentoEscaneado.Modal);
                            UIDataTable.AJAXDelete(this, '#btnEliminarDocumentoEscaneado', 'cod_tipo_documento', 'ajax/tablassoporte', 'DeleteDocumentoEscaneado', {
                                Codigo: 'cod_tipo_documento',
                            }, Load.Gestion.DocumentoEscaneado.Ready);
                        },
                        columns: [
                            { data: "cod_tipo_documento" },
                            { data: "tipo_documento" },
                        ],
                    });
                });
            },
            Modal: async function (jsonValue) {
                await UILoadingDefault("show");
                if (jsonValue.isNew) {
                    $('#h4TitleMDOES').html('NUEVO CLASE FECHA');
                    $('#txtCodigoMDOES').removeAttr('disabled');
                }
                else {
                    $('#h4TitleMDOES').html(`CLASE FECHA: ${jsonValue.cod_tipo_documento}`);
                    $('#txtCodigoMDOES').attr('disabled', 'disabled');
                }

                $('#txtCodigoMDOES').val(jsonValue.cod_tipo_documento);
                $('#txtDescripcionMDOES').val(jsonValue.tipo_documento);

                $('#btnGuardarMDOES').off('click').on('click', function (e) {
                    var jsonParam = {
                        Accion: null,
                        Codigo: null,
                        Descripcion: $('#txtDescripcionMDOES').val(),
                    };
                    if (jsonValue.isNew) {
                        jsonParam.Accion = 'N';
                        jsonParam.Codigo = $('#txtCodigoMDOES').val();
                    }
                    else {
                        jsonParam.Accion = 'M';
                        jsonParam.Codigo = jsonValue.cod_tipo_documento;
                    }
                    if (UIEmpty(jsonParam.Codigo)) {
                        UIMensaje.CODIGO();
                        return;
                    }
                    if (UIEmpty(jsonParam.Descripcion)) {
                        UIMensaje.DESCRIPCION();
                        return;
                    }
                    UIAjax.runFunctionAsync('ajax/tablassoporte', 'PostDocumentoEscaneado', jsonParam, function () {
                        Load.Gestion.DocumentoEscaneado.Ready();
                        $('#modalDocumentoEscaneado').modal('hide');
                    });
                });
                $('#modalDocumentoEscaneado').modal('show');
                UILoadingDefault();
            }
        };
    }
    else {
        console.warn("No se encontró la propiedad 'Gestion' en la variable 'Load'");
    }
}
else {
    console.warn("No se encontró la variable 'Load'");
}