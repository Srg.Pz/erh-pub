$(document).ready(function () {
	Load.Departamento.Ready();
});
var Load = {
	Departamento: {
		Ready: async function () {
			UIDataTable.setEmpty('#tableDepartamento');
			await UIAjax.invokeMethodAsync('ajax', 'GetAllTypeDepartment').then(function (r) {
				tableDepartamento = $('#tableDepartamento').DataTable({
					data: r,
					initComplete: function (settings, json) {
						UIDataTable.SelectRows(this, false, false, Load.Provincia.Ready);
					},
					paging: false,
					info:false,
					columns: [
						{ data: 'codigo', className:'text-center' },
						{ data: 'nombre' },
					]
				});
			});

		}
	},
	Provincia: {
		Ready: async function (jsonRow) {
			var jsonParam = {
				IdDepartment: jsonRow.codigo,
			};
			UIDataTable.setEmpty('#tableProvincia, #tableDistrito');
			await UIAjax.invokeMethodAsync('ajax', 'GetTypeProvince', jsonParam).then(function (r) {
				tableProvincia = $('#tableProvincia').DataTable({
					data: r,
					initComplete: function (settings, json) {
						UIDataTable.SelectRows(this, false, false, Load.Distrito.Ready, null, true);
					},
					paging: false,
					info:false,
					columns: [
						{ data: 'codigo', className:'text-center' },
						{ data: 'nombre' },
					]
				});
			});
		}
	},
	Distrito: {
		Ready: async function (jsonRow) {
			var jsonParam = {
				IdProvince: jsonRow.codigo,
			};
			UIDataTable.setEmpty('#tableDistrito');
			await UIAjax.invokeMethodAsync('ajax', 'GetTypedistrict', jsonParam).then(function (r) {
				tableDistrito = $('#tableDistrito').DataTable({
					data: r,
					initComplete: function (settings, json) {
						//UIDataTable.SelectRows(this, false, false, Load.Provincia.Ready);
					},
					paging: false,
					info:false,
					columns: [
						{ data: 'codigo', className:'text-center' },
						{ data: 'nombre' },
					]
				});
			});
		}
	}
}