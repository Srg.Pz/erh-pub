﻿if ($.fn.dataTable != undefined) {
    if ($.fn.dataTable.Api != undefined) {
        $.fn.dataTable.Api.register('column().title()', function () {
            var colheader = this.header();
            return $(colheader).text().trim();
        });
    }
}