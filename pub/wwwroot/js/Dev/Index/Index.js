﻿window.onload = async function () {

	await UILoadingDefaultConfigure();
	await UIMensaje.DEVELOPER().then(function (r) {
		if (r == true) {
			UIHTMLReporte.TryGetJS('ajax/core', 'Get');
		}
		else {
			document.body.removeAttribute('class');
			$('body').addClass('container');
			document.body.innerHTML = '<div class="row align-items-center" style="height: 100vh;"><div class="col-auto mx-auto" style="color: red;font-size: 6rem;font-weight: bold;">[SIN ACCESO]</div></div>';
		}
	});
}