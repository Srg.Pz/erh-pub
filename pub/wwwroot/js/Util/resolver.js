﻿function ValidarEntero_Copiar_Pegar(e) {

    if ($.inArray(e.keyCode, [17, 46, 8, 9, 27, 13, 110, 190]) !== -1 ||
        ((e.keyCode === 'C'.charCodeAt() || e.keyCode === 'V'.charCodeAt()) && (e.ctrlKey === true || e.metaKey === true)) ||
        (e.keyCode >= 35 && e.keyCode <= 40)) {
        return;
    }
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
}



function ValidarFormatoHoraMin(CtrId, codeKey) {

    if (codeKey !== 8 && codeKey !== 46) {
        var txtVal = $("#" + CtrId).val();
        if (txtVal !== undefined && txtVal !== isNaN) {
            if (txtVal.length === 2) {
                var ValProc = txtVal.slice(0, 2);
                if (parseInt(ValProc) > 23) {
                    $("#" + CtrId).val(23);
                }
            }

            if (txtVal.length === 5) {
                var ValProc5 = txtVal.slice(3, 5);
                if (parseInt(ValProc5) > 59) {
                    ValProc5 = txtVal.slice(0, 2);
                    $("#" + CtrId).val(ValProc5 + ":" + 59);
                }

                var ValProcBT = txtVal.slice(0, 2);
                if (parseInt(ValProcBT) > 23) {
                    $("#" + CtrId).val("23:" + txtVal.slice(3, 5));
                }
            }
        }
    }
}


function ValidarFormatoDate(CtrId, codeKey) {
    
    if (codeKey !== 8 && codeKey !== 46) {
        var txtVal = $("#" + CtrId).val();
        if (txtVal !== undefined && txtVal !== isNaN) {

            if (txtVal.length == 2) {
                var ValProc = txtVal.slice(0, 2);
                if (parseInt(ValProc) > 31) {
                    $("#" + CtrId).val(30);
                }
            }

            if (txtVal.length == 5) {
                var ValProc5 = txtVal.slice(3, 5);
                if (parseInt(ValProc5) > 12) {
                    ValProc5 = txtVal.slice(0, 2);
                    $("#" + CtrId).val(ValProc5 + "/" + 12);
                }

                var ValProcBT = txtVal.slice(0, 2);
                if (parseInt(ValProcBT) > 31) {
                    $("#" + CtrId).val("30/" + txtVal.slice(3, 5));
                }
            }
            
            if (txtVal.length == 10)
            {
                 //01/02/2020
                var ValProc10 = txtVal.slice(6, 10);
                if (parseInt(ValProc10) > 2050) {
                    ValProc10 = txtVal.substring(0, 6);
                    $("#" + CtrId).val(ValProc10 + "2020");
                }

                var Dia = parseInt(txtVal.substring(0, 1));
                var Mes = parseInt(txtVal.substring(3, 4));

                //01/02/2020
                if (Dia > 31) {
                    var valD = "01/" + txtVal.substring(2, 9);
                    $("#" + CtrId).val(valD);
                }

                if (Mes > 12) {
                    var valM = txtVal.substring(0, 1) + "/12/" + txtVal.substring(6, 9);
                    $("#" + CtrId).val(valM);
                }
            }

            //if (txtVal.length == 10) {

            //    var ValProc10 = txtVal.slice(6, 10);
            //    if (parseInt(ValProc10) > 2025) {
            //        ValProc10 = txtVal.slice(0, 2);
            //        $("#" + CtrId).val(ValProc10 + "/" + 12);
            //    }

            //    var ValProc5Bt = txtVal.slice(3, 5);
            //    if (parseInt(ValProc5Bt) > 12) {
            //        ValProc5Bt = txtVal.slice(0, 2);
            //        $("#" + CtrId).val(ValProc5Bt + "/" + 12);
            //    }

            //    var ValProcPR1 = txtVal.slice(0, 2);
            //    if (parseInt(ValProcPR1) > 31) {
            //        $("#" + CtrId).val("30/" + txtVal.slice(3, 5));
            //    }
            //}
        }
    }
}

function ValidarFormatoHoraMinGuardar(Crtl) {
    var Status = true;

    if ($("#" + Crtl).val().length >= 1 && $("#" + Crtl).val().length < 5) {
        MsgBox("Formato en horas incorrecto, sírvase corregir.", "warning", true);
        $("#" + Crtl).addClass("validation_empty").focus();
        Status = false;
    }
    return Status;
}
