﻿let TypeAjax = {
    G='GET',
    P='POST'
};

const __Ajax = (pType = TypeAjax.G , pUrl, pParam, pAsync = true) => {
    $.ajax({
        type: pType,
        url: pUrl,
        data: JSON.stringify(pParam),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: pAsync,
        success: function (data) {
            return data;
        },
        error: function (response) {
            return response;
        }
    });
};