﻿//import { f } from "../plugins/pdfmake/js/pdfmake";

var __Ajax = function __Ajax() {
	var pType = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'GET';
	var pUrl = arguments.length > 1 ? arguments[1] : undefined;
	var pParam = arguments.length > 2 ? arguments[2] : undefined;
	var pAsync = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : false;
	var rpta = null;
	$.ajax({
		type: pType,
		url: pUrl,
		data: pParam,
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		async: pAsync,
		success: function success(data) {
			rpta = data;
		},
		error: function error(response) {
			rpta = response;
		}
	});
	return rpta;
};

var GetInconsistencias = function GetInconsistencias(jsonParam) {
	$.getJSON(portal + '/ajax/personal/GetInconsistenciasPersonal', jsonParam, function (data) {
		bodyInconsistencias('containerInconsistencias', data);
	});
};

var bodyInconsistencias = function bodyInconsistencias(idContainer, obj) {
	//var obj = JSON.parse(data.d);
	var container = $("#".concat(idContainer));
	container.empty();
	var i = 0;
	var html = "";
	var incidencia_old = '';
	var incidencia_new = '';
	var countIncon = obj.length;
	$.each(obj, function (index, value) {
		i++;

		if (i === 1) {
			incidencia_old = value.cod_incon;
			incidencia_new = value.cod_incon;
			html = "<div style=\"margin-bottom: 2px;\">\n                        <div class=\"bg-primary rounded\">\n                            <a data-toggle=\"collapse\" href=\"#collapse".concat(i, "\">\n                                <h3 class=\"mb-0 p-2\" style=\"color: white; font-size: 16px;\">").concat(value.inconsistencia, "</h3>\n                            </a>\n                        </div>\n                    <div id=\"collapse").concat(i, "\" class=\"panel-collapse collapse\">\n                        <ul style=\"margin: 10px;\">\n                            <li>").concat(value.nombre, "</li>");
		} else if (i > 1) {
			incidencia_new = value.cod_incon;

			if (incidencia_new === incidencia_old) {
				html += "<li>".concat(value.nombre, "</li>");
			} else {
				html += "</ul>\n                            </div>\n                        </div>";

				if (i < countIncon) {
					html += "<div style=\"margin-bottom: 2px;\">\n                                    <div class=\"bg-primary rounded\">\n                                        <a data-toggle=\"collapse\" href=\"#collapse".concat(i, "\">\n                                            <h3 class=\"mb-0 p-2\" style=\"color: white; font-size: 16px;\">").concat(value.inconsistencia, "</h3>\n                                        </a>\n                                    </div>\n                                <div id=\"collapse").concat(i, "\" class=\"panel-collapse collapse\">\n                                    <ul style=\"margin: 10px;\">\n                                        <li>").concat(value.nombre, "</li>");
				}
			}
		}

		if (countIncon === i) {
			html += "</ul>\n                            </div>\n                        </div>";
		}

		incidencia_old = incidencia_new;
	});
	container.append(html);
};

var ClearInput = function ClearInput(p_opcion) {
	switch (p_opcion) {
		case 'Fa':
			$('#dvFamiliar_edit input').each(function (a) {
				$(this).val('');
			});
			$('#dvFamiliar_edit select').each(function (a) {
				$(this).prop('selectedIndex', 0);
			});
			$('#selDepartamento_Familiar').change();
			g_accion_familiar = '';
			g_num_familiar = 0;
			break;

		case 'Me':
			$('#dvMeritosDemeritos_edit input, #dvMeritosDemeritos_edit textarea').val('');
			$('#dvMeritosDemeritos_edit select').val(''); //.prop('selectedIndex', 0);

			g_accion_merito = 'N';
			g_item_merito = 0;
			break;

		case 'Bi':
			$('#dvBienes_edit input, #dvBienes_edit textarea').val('');
			$('#dvBienes_edit select').prop('selectedIndex', 0);
			$('#txtCostoMn_Bienes').val(0);
			$('#txtCostoMe_Bienes').val(0);
			$('#txtCostoReposMn').val(0);
			$('#txtCostoReposMe').val(0);
			g_accion_bienes = '';
			g_num_bien = '';
			break;

		case 'Fo':
			$('#dvFormacion_edit input').each(function (a) {
				var ths = $(this);
				var id = ths.prop('id');
				if (id != "rbRegPub_Formacion" && id != "rbRegPri_Formacion") {
					ths.val('');
				}
			});
			$('#dvFormacion_edit select').each(function (a) {
				$(this).prop('selectedIndex', 0);
			});
			g_accion_formacion = '';
			g_num_formacion = 0;
			break;

		case 'Re':
			$('#containerRetenciones input:text').each(function (a) {
				$(this).val('');
			});
			$('#containerRetenciones select').each(function (a) {
				$(this).prop('selectedIndex', 0);
			});
			//$('#selectClaseProceso_Retencion').multipleSelect('setSelects', []);
			$('#selectClaseProceso_Retencion').val('');
			break;

		default:
			break;
	}
};

var fnEnabledText = function fnEnabledText(p_this, idTxt) {
	if ($(p_this).prop('checked')) {
		$('#' + idTxt).attr('disabled', false);
	} else {
		$('#' + idTxt).attr('disabled', true);
		$('#' + idTxt).val('0');
	}
};

var convertDateFormat = function convertDateFormat(string) {
	if (string == null) {
		string = '0000-00-00';
	}

	string = string.substring(0, 10);
	var info = string.split('-');
	return info[2] + '/' + info[1] + '/' + info[0];
};

var convertDateFormat2 = function convertDateFormat2(string) {
	//if (string == null) {
	//    string = '0000-00-00'
	//}
	string = string.substring(0, 10);
	var info = string.split('/');
	return info[1] + '/' + info[0] + '/' + info[2];
};

var toInt = function toInt(p) {
	if (p === "" || p === null || p === undefined) {
		p = 0;
	}

	return p;
};

var isEmpty = function isEmpty(p, v) {
	var result = false;

	if (p === "" || p === null || p === undefined) {
		result = true;
	}

	if (v != null) {
		result = v;
	}

	return result;
};

var GetDatosGenerales = function GetDatosGenerales(p_codEntidad, p_codPersonal) {
	//var jsonParam = {
	//    codEntidad: p_codEntidad,
	//    codPersonal: p_codPersonal
	//};
	//containerPage.maskContainer("Cargando ....");
	$.ajax({
		type: "GET",
		url: portal + "/AdministracionPersonal/GetDatosGeneralesLegajo?codEntidad=" + p_codEntidad + "&codPersonal=" + p_codPersonal/*.concat(p_codEntidad, "&codPersonal=").concat(p_codPersonal)*/,
		//data: JSON.stringify(jsonParam),
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		success: async function success(data) {
			var obj = data;
			var cab = obj.table;
			var det = obj.table1;

			var objDetalle = det[0];
			$('#txtApePat').val(cab[0].apellido_P.trim());
			$('#txtApeMat').val(cab[0].apellido_M.trim());
			$('#txtNombres').val(cab[0].nombres.trim());
			$('#txtCodPersonal').val(cab[0].cod_personal);
			$('#txtRuc_data').val(cab[0].ruc.trim());
			if (!UIEmpty(objDetalle)) {
				$('#lblNombreCompleto').text(objDetalle.nombre);
				$('#selVia').val(objDetalle.cod_via);
				$('#txtViaDesc').val(objDetalle.tregistro_nombre_via);
				$('#txtNro').val(parseInt(objDetalle.numero));
				$('#txtInt_data').val(objDetalle.interior);
				$('#txtDpto').val(objDetalle.tregistro_departamento);
				$('#txtManzana').val(objDetalle.tregistro_manzana);
				$('#txtLote').val(objDetalle.tregistro_lote);
				$('#txtKilometro').val(objDetalle.tregistro_kilometro);
				$('#txtBlock').val(objDetalle.tregistro_block);
				$('#txtEtapa').val(objDetalle.tregistro_etapa);
				var val_depa = objDetalle.departamento;
				var val_prov = objDetalle.provincia;
				$('#selZona').val(objDetalle.cod_zona);
				$('#txtZonaDesc').val(objDetalle.zona);
				$('#lblDireccionCompleta').text(objDetalle.direccion);
				//var jsonDet = objDetalle;
				//console.log(jsonDet);
				objDetalle.cod_ubigeo = $.trim(objDetalle.cod_ubigeo);
				if (objDetalle.cod_ubigeo != null && objDetalle.cod_ubigeo.trim() != '') {
					$('#selDepartamento').val(objDetalle.departamento);
					await UIFunctions.setProvincia('#selProvincia', objDetalle.departamento, "codigo");
					$('#selProvincia').val(objDetalle.provincia);
					await UIFunctions.setDistrito('#selDistrito', objDetalle.provincia, "codigo");
					$('#selDistrito').val(objDetalle.cod_ubigeo);
				} else {
					$('#selDepartamento').val("x");
					$('#selProvincia').val("x");
					$('#selDistrito').val("x");
				}

				$('#txtReferencia').val(objDetalle.referencia);
				$('#txtPreFijo').val(objDetalle.tregistro_cod_telefono);
				$('#txtTelCasa').val(objDetalle.tregistro_telefono);
				$('#txtTelParticular').val(objDetalle.telefono2);
				$('#txtTelLaboral').val(objDetalle.telefono3);
				$('#txtTelAnexo').val(objDetalle.fax);
				$('#txtEmail').val(objDetalle.email);
				$('#txtEmailPer').val(objDetalle.email_personal);
				$('#txtWeb').val(objDetalle.www);
				$('#txtAlias').val(objDetalle.alias);

				//Familiares Extre
				$('#selVia_Familiar').val(objDetalle.cod_via);
				$('#txtDescVia_Familiar').val(objDetalle.tregistro_nombre_via);
			}
			else {
				$('input.txtLegajoDatosGeneralesDetalle').val(null);
				$('#selVia, #selZona').val('-1');
				$('#selDepartamento').val("x");
				$('#selProvincia').val("x");
				$('#selDistrito').val("x");
				//MsgBox("");
			}
			//containerPage.unmaskContainer();
		},
		error: function error(response) { }
	});
};

var GetDatosPersonales = async function GetDatosPersonales(p_codEntidad, p_Perfil) {
	let jsonParam = {
		codEntidad: p_codEntidad,
	};
	await UIAjax.invokeMethodAsync('AdministracionPersonal', 'GetDatosPersonalesLegajo', jsonParam).then(async function (data) {
		var obj = data;
		var jsonObj = obj[0];
		if (p_Perfil == "AD") {
			$('#txtFechaNac').val(jsonObj.fecha_nacimiento);
			$('#txtNroHijos').val(jsonObj.numero_hijos);
			$('#selEstadoCivil').val(jsonObj.cod_estado_civil);
			if (jsonObj.cod_ubigeo_nac != null && jsonObj.cod_ubigeo_nac.trim != "") {
				$('#selDepartamento_Personal').val(jsonObj.departamento);
				await UIFunctions.setProvincia('#selProvincia_Personal', jsonObj.departamento, 'codigo');
				$('#selProvincia_Personal').val(jsonObj.provincia);
				await UIFunctions.setDistrito('#selDistrito_Personal', jsonObj.provincia, 'codigo');
				$('#selDistrito_Personal').val(jsonObj.cod_ubigeo_nac);
			} else {
				$('#selDepartamento_Personal').val("x");
				$('#selProvincia_Personal').val("x");
				$('#selDistrito_Personal').val("x");
			}

			$('#selTipoDocIdent').val(jsonObj.cod_docid);
			$('#txtNroDocIdent').val(jsonObj.numero_docid);
			$('#txtCsse').val(jsonObj.csse);
			$('#selAFP').val(jsonObj.cod_afp);
			$('#txtCussp').val(jsonObj.cuispp);
			$('#txtFechaAfiliacion').val(jsonObj.fecha_afiliacion);
			$('#selNacionalidad').val(jsonObj.cod_pais);
			$('#selNivelAcademico').val(jsonObj.cod_nivel_academico);
			if (jsonObj.tipo_comision_afp == 1) {
				$('#rbRemuneracion').prop('checked', true);
			} else {
				if (jsonObj.tipo_comision_afp == 2) {
					$('#rbMixta').prop('checked', true);
				} else {
					$('#rbRemuneracion').prop('checked', false);
					$('#rbMixta').prop('checked', false);
				}
			}
			if (jsonObj.flg_discapacidad == 'S') $('#chkDiscapacitado').prop('checked', true); else $('#chkDiscapacitado').prop('checked', false);
			if (jsonObj.flg_domicilio == 'S') {
				$('#chkDomiciliado').prop('checked', true).change();
			}
			else {
				$('#chkDomiciliado').prop('checked', false).change();
			}
			if (jsonObj.flg_madre_trabajadora == 'S') $('#chkMadreRespFamiliar').prop('checked', true); else $('#chkMadreRespFamiliar').prop('checked', false);
		}

		$('#selTipoBrevete').val(jsonObj.cod_brevete);
		$('#txtNroBrevete').val(jsonObj.numero_brevete);

		$('#selProfesion').val(jsonObj.cod_profesion);
		$('#txtTalla').val(jsonObj.talla);
		$('#txtOjoIzq').val(jsonObj.ojo_iz);
		$('#txtOjoDer').val(jsonObj.ojo_de);
		$('#txtColorPiel').val(jsonObj.piel);
		$('#txtOidosIzq').val(jsonObj.oido_iz);
		$('#txtOidosDer').val(jsonObj.oido_de);
		$('#txtPeso').val(jsonObj.peso);
		$('#txtOjos').val(jsonObj.ojos);
		$('#selTallaPolo').val(jsonObj.polo);
		$('#selTallaCamisa').val(jsonObj.camisa);
		$('#txtPelo').val(jsonObj.pelo);
		$('#selTallaPantalon').val(jsonObj.pantalon);
		$('#selTallaCasaca').val(jsonObj.casaca);
		$('#txtAlergias').val(jsonObj.nomedicamentos);
		$('#selTallaZapato').val(jsonObj.zapato);
		$('#selTallaZapatilla').val(jsonObj.zapatilla);
		$('#selGrupoSanguineo').val(jsonObj.grupo_sanguineo);

		if (jsonObj.sexo == 1) {
			$('#rbMasculino').prop('checked', true);
		} else if (jsonObj.sexo == 2) {
			$('#rbFemenino').prop('checked', true);
		}

		var imagen = portal + "/Noticias/ShowImagePersonal?codPersonal=" + g_codPersonal;
		$('#imgPersonal').attr('src', imagen);
	});
};

var GetDatosLaborales = function GetDatosLaborales(p_codPersonal) {
	var jsonData = {
		codPersonal: p_codPersonal
	};
	$.ajax({
		type: "GET",
		url: portal + "/AdministracionPersonal/GetDatosLaboralesLegajo?codPersonal=".concat(p_codPersonal),
		data: jsonData,
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		success: function success(data) {
			var obj = data.table;

			//debugger
			var jsonObj = obj[0];

			$('#txtCodTrabajador').val(obj[0].cod_trabajador);
			$('#txtCodAuxCont').val(obj[0].cod_auxiliar_cont);
			$('#txtUsuarioAD').val(obj[0].usuario_ad);
			$('#selTipoContrato').val(obj[0].cod_tipo_contrato);
			$('#selCondicionLaboral').val(obj[0].tipo_trabajador);
			$('#selAsistencia').val(obj[0].flg_reloj);
			$('#txtFotocheck').val(obj[0].fotocheck);
			$('#selTipoAsist').val(obj[0].flg_tipo_asist);
			$('#txtHorasSem').val(obj[0].hrs_asist);
			$('#lblDiaCorte').text(obj[0].desc_dia_corte);
			$('#txtEstado').val(obj[0].desc_estado);
			$('#txtEstado').data('cod', obj[0].estado);
			$('#txtIngreso').val(obj[0].fecha_ingreso.includes('/1900') ? '' : obj[0].fecha_ingreso);
			$('#selMotivoIngreso').val(obj[0].cod_motivo_ingreso);
			$('#txtFecIngreso2').val((obj[0].fecha_ingreso_aux != null) ? (obj[0].fecha_ingreso_aux == '01/01/1900' ? '' : obj[0].fecha_ingreso_aux) : '');
			$('#txtCese').val((obj[0].fecha_cese != null) ? (obj[0].fecha_cese == '01/01/1900' ? '' : obj[0].fecha_cese) : '');
			$('#txtLiqui').val((obj[0].fecha_liquidacion != null) ? (obj[0].fecha_liquidacion == '01/01/1900' ? '' : obj[0].fecha_liquidacion) : '');
			$('#txtCodMotivoCese').attr('data-cod_motivo_cese', jsonObj.cod_motivo_cese);
			$('#txtCodMotivoCese').val(obj[0].cese_desc);
			$('#txtMotivoCese').val(jsonObj.motivo_cese);

			UICheckbox.setValue('#chkRealizaMarcacionTDALA', jsonObj.flg_marcacion_web);

			if (obj[0].afiliacion_EPS == '1') {
				$('#chkAfiliacionEPS').prop('checked', true);
				$('#selAfiliacionEPS, #txtCodEPS, #txtFechaAfilEPS').attr('disabled', false);
			} else {
				$('#chkAfiliacionEPS').prop('checked', false);
				$('#selAfiliacionEPS, #txtCodEPS, #txtFechaAfilEPS').attr('disabled', true);
			}

			$('#selAfiliacionEPS').val(obj[0].cod_eps);
			$('#txtCodEPS').val(obj[0].cod_afiliado_eps);

			$('#txtFechaAfilEPS').val((obj[0].fecha_Afiliacion_EPS != null) ? (obj[0].fecha_Afiliacion_EPS == '01/01/1900' ? '' : obj[0].fecha_Afiliacion_EPS) : '');
			if (obj[0].flg_SCTR_Pension == 'S') {
				$('#chkSctrPension').prop('checked', true).change();
				$('#txtSctrDesde,#txtSctrHasta').prop('checked', true);
			} else {
				$('#chkSctrPension').prop('checked', false).change();
				$('#txtSctrDesde,#txtSctrHasta').prop('checked', false);
			}
			$('#selSctrPension').val(obj[0].sctR_Pension);
			$('#selSctrPensionRiesgo').val(obj[0].flg_SCTR_Salud_riesgo);
			if (obj[0].flg_SCTR_Salud == 'S') {
				$('#chkSctrSalud').prop('checked', true).change();
			}
			else {
				$('#chkSctrSalud').prop('checked', false).change();
			}
			$('#selSctrSalud').val(obj[0].sctR_Salud);
			$('#selSctrSaludRiesgo').val(obj[0].flg_SCTR_Pension_riesgo);

			$('#txtSctrDesde').val((obj[0].fecha_sctr_inicio != null) ? (obj[0].fecha_sctr_inicio == '01/01/1900' ? '' : obj[0].fecha_sctr_inicio) : '');
			$('#txtSctrHasta').val((obj[0].fecha_sctr_fin != null) ? (obj[0].fecha_sctr_fin == '01/01/1900' ? '' : obj[0].fecha_sctr_fin) : '');

			if (obj[0].flg_senati == 'S') $('#chkSenati').prop('checked', true); else $('#chkSenati').prop('checked', false);
			$('#selEstadoLaboral').val(obj[0].cod_Estado_Laboral);
			$('#selTipoTrabajador').val(obj[0].cod_Tipo_Trabajador);
			if (obj[0].flg_vida_ley == 'S') {
				$('#chkVidaLey').prop('checked', true);
				$('#selVidaLey, #txtCodVidaLey, #txtTasaVidaLey').attr('disabled', false);
			} else {
				$('#chkVidaLey').prop('checked', false);
				$('#selVidaLey, #txtCodVidaLey, #txtTasaVidaLey').attr('disabled', true);
			}

			$('#selVidaLey').val(obj[0].cod_vida_ley);
			$('#txtCodVidaLey').val(obj[0].cod_vida_ley_aseg);
			$('#txtTasaVidaLey').val(obj[0].tasa_vida_ley);
			$('#selTipoRemunVariable').val(obj[0].flg_tipo_rem_var);
			$('#selModFormativa').val(obj[0].cod_tipo_mod_formativa);
			$('#selSeguroPract').val(obj[0].flg_seguro_practicante);
			if (obj[0].flg_jornada_maxima == 'S') $('#chkJornadaMaxima').prop('checked', true); else $('#chkJornadaMaxima').prop('checked', false);
			if (obj[0].flg_regimen_alternativo == 'S') $('#chkRegimenAlternativo').prop('checked', true); else $('#chkRegimenAlternativo').prop('checked', false);
			if (obj[0].flg_horario_nocturno == 'S') $('#chkHoraNoct').prop('checked', true); else $('#chkHoraNoct').prop('checked', false);
			if (obj[0].flg_HH_Auto == 'S') $('#chkHoraExt').prop('checked', true); else $('#chkHoraExt').prop('checked', false);
			if (obj[0].flg_sindicato == 'S') $('#chkSindicalizado').prop('checked', true); else $('#chkSindicalizado').prop('checked', false);
			if (obj[0].flg_Alimentos == '1') $('#chkPrestAlimentaria').prop('checked', true); else $('#chkPrestAlimentaria').prop('checked', false);
			if (obj[0].flg_Movilidad == '1') $('#chkAsigMovilidad').prop('checked', true); else $('#chkAsigMovilidad').prop('checked', false);
			if (obj[0].essaluD_VIDA == '1') $('#chkESSALUD').prop('checked', true); else $('#chkESSALUD').prop('checked', false);
			if (obj[0].flg_asignacion_familiar == 'S') $('#chkAsignacionFamiliar').prop('checked', true); else $('#chkAsignacionFamiliar').prop('checked', false);
			if (obj[0].flg_exoneracion_ret_cuarta == 'S') $('#chkExoneracionRet').prop('checked', true); else $('#chkExoneracionRet').prop('checked', false);

			if (obj[0].flg_miembro_comite_sst == 'S') {
				$('#chkMiembroComite').prop('checked', true);
				$('#txtDelAsig, #txtAlAsig').attr('disabled', false);
			} else {
				$('#chkMiembroComite').prop('checked', false);
				$('#txtDelAsig, #txtAlAsig').attr('disabled', true);
			}

			$('#txtDelAsig').val(UIValue(obj[0].miembro_comite_sst_desde).includes('/1900') ? '' : obj[0].miembro_comite_sst_desde);
			$('#txtAlAsig').val(UIValue(obj[0].miembro_comite_sst_hasta).includes('/1900') ? '' : obj[0].miembro_comite_sst_hasta);

			if (obj[0].flg_Adelanto == 'S') {
				$('#chkAdelanto').prop('checked', true);
				$('#txtMontoAdelanto, #txtPorcentajeAdelanto').attr('disabled', false);
			} else {
				$('#chkAdelanto').prop('checked', false);
				$('#txtMontoAdelanto, #txtPorcentajeAdelanto').attr('disabled', true);
			}
			$('#txtMontoAdelanto').val(obj[0].monto_adelanto);
			$('#txtPorcentajeAdelanto').val(obj[0].porcen_Adelanto);
			if (obj[0].flg_pago_cuenta == 'S') $('#chkPagoCuenta').prop('checked', true); else $('#chkPagoCuenta').prop('checked', false);
			if (obj[0].flg_cheque == 'S') $('#chkPagoCheque').prop('checked', true); else $('#chkPagoCheque').prop('checked', false);
			if (obj[0].flg_Boleta_ReciboH == 'B') $('#chkConceptoRecibo').prop('checked', true); else $('#chkConceptoRecibo').prop('checked', false);
			if (obj[0].flg_liquidacion_semanal == 'S') $('#chkLiquiBoleta').prop('checked', true); else $('#chkLiquiBoleta').prop('checked', false);
			$('#selMonedaBasico').val(obj[0].cod_moneda_basico);
			$('#txtBasico').val(String(obj[0].monto));
			$('#txtBono').val(obj[0].monto_bono);
			$('#selPeriodoBasico').val(obj[0].tipo_Monto_Periodo);

			if (obj[0].flg_cta_interbancario_pago == 'S') {
				$('#chkBancoPago').prop('checked', true);
				$('#contBancoOrigenPago').removeClass('d-none');
				$('#lblBancoPago').text('Interbancario Pago:');
			} else {
				$('#chkBancoPago').prop('checked', false);
				$('#contBancoOrigenPago').addClass('d-none');
				$('#lblBancoPago').text('Banco Pago:');
			}

			$('#selBancoPago').val(obj[0].cod_banco_pago);
			$('#selMonedaPago').val(obj[0].cod_moneda_pago);
			$('#selTipoCuentaPago').val(obj[0].cod_tipo_cuenta_pago);
			$('#txtNroCuenta1').val(obj[0].num_cuenta_pago);
			$('#selBancoOrigenPago').val(obj[0].cod_banco_origen_pago);

			if (obj[0].flg_cta_interbancario_cts == 'S') {
				$('#chkBancoCTS').prop('checked', true);
				$('#contBancoOrigenCTS').removeClass('d-none');
				$('#lblBancoCTS').text('Interbancario Pago:');
			} else {
				$('#chkBancoCTS').prop('checked', false);
				$('#contBancoOrigenCTS').addClass('d-none');
				$('#lblBancoCTS').text('Banco Pago:');
			}

			$('#selBancoCTS').val(obj[0].cod_banco_cts);
			$('#selMonedaCTS').val(obj[0].cod_moneda_cts);
			$('#selTipoCuentaCTS').val(obj[0].cod_tipo_cuenta_cts);
			$('#txtNroCuentaCTS').val(obj[0].num_cuenta_cts);
			$('#selBancoOrigenCTS').val(obj[0].cod_banco_origen_cts);
		},
		error: function error(response) { }
	});
};

var GetProvincias = function GetProvincias(ddl, p_cod_depa) {
	$.ajax({
		type: "GET",
		url: portal + '/Ajax/getTypeProvince?IdDepartment=' + p_cod_depa,
		data: JSON.stringify({}),
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		async: false,
		success: function success(data) {
			var obj = data;
			var container = $("#" + ddl);
			container.empty();
			$.each(obj, function (index, value) {
				var html = "<option value=\"".concat(value.codigo, "\">").concat(value.nombre, "</option>");
				container.append(html);
			});
			container.change();
		},
		error: function error(response) { }
	});
};

var GetDistritos = function GetDistritos(ddl, p_cod_prov) {
	var jsonData = {
		IdProvince: p_cod_prov
	};
	$.ajax({
		type: "GET",
		url: portal + '/Ajax/getTypeDistrict?IdProvince=' + p_cod_prov,
		data: JSON.stringify({}),
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		async: false,
		success: function success(data) {
			var obj = data;
			dtPeriodos = obj;
			var container = $("#" + ddl);
			$("#" + ddl).empty();
			$.each(obj, function (index, value) {
				var html = "<option value=\"".concat(value.codigo.trim(), "\">").concat(value.nombre, "</option>");
				container.append(html);
			});
		},
		error: function error(response) { }
	});
};

var GetHorario = function GetHorario(p_codPersonal) {
	//var jsonData = {
	//    cod_personal: p_codPersonal
	//};
	$.ajax({
		type: "GET",
		url: portal + "/AdministracionPersonal/GetHorarioPersonal?codPersonal=" + p_codPersonal/*.concat(p_codPersonal)*/,
		//data: JSON.stringify(jsonData),
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		success: function success(obj) {
			$('#tbodyHorarios tr input[type=radio]:checked').prop('checked', false);
			if (obj.length === 0) return;
			CheckHorario(1, obj[0].d1);
			CheckHorario(2, obj[0].d2);
			CheckHorario(3, obj[0].d3);
			CheckHorario(4, obj[0].d4);
			CheckHorario(5, obj[0].d5);
			CheckHorario(6, obj[0].d6);
			CheckHorario(7, obj[0].d7);
		},
		error: function error(response) { }
	});
};

var CheckHorario = function CheckHorario(p_dia, p_cod_turno) {
	$('#tbodyHorarios tr').each(function () {
		if ($(this).data('cod') == p_cod_turno) {
			switch (p_dia) {
				case 1:
					var chk = $(this).find('#chkDomingo');
					$(chk).prop('checked', true);
					break;

				case 2:
					var chk = $(this).find('#chkLunes');
					$(chk).prop('checked', true);
					break;

				case 3:
					var chk = $(this).find('#chkMartes');
					$(chk).prop('checked', true);
					break;

				case 4:
					var chk = $(this).find('#chkMiercoles');
					$(chk).prop('checked', true);
					break;

				case 5:
					var chk = $(this).find('#chkJueves');
					$(chk).prop('checked', true);
					break;

				case 6:
					var chk = $(this).find('#chkViernes');
					$(chk).prop('checked', true);
					break;

				case 7:
					var chk = $(this).find('#chkSabado');
					$(chk).prop('checked', true);
					break;

				default:
					break;
			}
		}
	});
};

var GetHorariosUsuario = function GetHorariosUsuario(pPerfil) {
	//var jsonData = {
	//    perfil: pPerfil
	//};
	$.ajax({
		type: "GET",
		url: portal + '/AdministracionPersonal/GetHorariosUsuario/?perfil=' + pPerfil,
		//data: jsonData,
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		success: function success(data) {
			var obj = data;
			var container = $("#tbodyHorarios");
			$("#tbodyHorarios").empty();
			$.each(obj, function (index, value) {
				var html = "<tr data-cod=\"".concat(value.cod_turno, "\">\n                                        <td class=\"text-center\">").concat(value.cod_turno, "</td>\n                                        <td class=\"text-center\">").concat(value.nombre, "</td>\n                                        <td class=\"text-center\"><input id=\"chkLunes\" type=\"radio\" name=\"gpLunes\" /></td>\n                                        <td class=\"text-center\"><input id=\"chkMartes\" type=\"radio\" name=\"gpMartes\" /></td>\n                                        <td class=\"text-center\"><input id=\"chkMiercoles\" type=\"radio\" name=\"gpMiercoles\" /></td>\n                                        <td class=\"text-center\"><input id=\"chkJueves\" type=\"radio\" name=\"gpJueves\" /></td>\n                                        <td class=\"text-center\"><input id=\"chkViernes\" type=\"radio\" name=\"gpViernes\" /></td>\n                                        <td class=\"text-center\"><input id=\"chkSabado\" type=\"radio\" name=\"gpSabado\" /></td>\n                                        <td class=\"text-center\"><input id=\"chkDomingo\" type=\"radio\" name=\"gpDomingo\" /></td>\n                                    </tr>");
				container.append(html);
			});
		},
		error: function error(response) { }
	});
};

var GetBienes = function GetBienes(pCodPersonal) {
	//var jsonData = {
	//    cod_personal: pCodPersonal
	//};
	$.ajax({
		type: "GET",
		url: portal + "/AdministracionPersonal/GetBienesPersonal?codPersonal=" + pCodPersonal/*.concat(pCodPersonal)*/,
		//data: JSON.stringify(jsonData),
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		success: function success(data) {
			var obj = data;
			var container = $("#tbodyBienes");
			container.empty();
			$.each(obj, function (index, value) {
				var html = "<tr onclick=\"GetDetalleBienes(this);\" data-item=\"".concat(value.item, "\">\n                                        <td class=\"text-center\">").concat(value.item, "</td>\n                                        <td class=\"text-center\">").concat(value.nombre, "</td>\n                                        <td class=\"text-center\">").concat(value.desc_estado, "</td>\n                                  </tr>");
				container.append(html);
			});
		},
		error: function error(response) { }
	});
};

var GetDetalleBienes = function GetDetalleBienes(p_tr) {
	g_accion_bienes = 'E';
	g_num_bien = $(p_tr).data('item');
	$('#txtItem_Bienes').prop('disabled', true);
	$("#tbodyBienes tr").removeClass('selected');
	$(p_tr).addClass('selected');
	var jsonData = {
		cod_personal: g_codPersonal,
		numero: $(p_tr).data('item')
	};
	$.ajax({
		type: "GET",
		url: portal + "/AdministracionPersonal/GetDetalleBienesPersonal?codPersonal=".concat(g_codPersonal, "&numero=").concat(g_num_bien),
		data: JSON.stringify(jsonData),
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		success: function success(data) {
			var obj = data;

			if (obj[0].estado == 'E') {
				$('#btnViewConstancia').text('Acta de Entrega');
				$('#btnViewConstancia').attr('on-state', obj[0].Estado);
				$('#btnViewConstancia').attr('on-item', obj[0].item); //if ($('#btnViewConstancia').hasClass('d-none'))
				//    $('#btnViewConstancia').removeClass('d-none')
			} else if (obj[0].estado == 'D') {
				$('#btnViewConstancia').text('Acta de Entrega');
				$('#btnViewConstancia').attr('on-state', obj[0].Estado);
				$('#btnViewConstancia').attr('on-item', obj[0].item); //if ($('#btnViewConstancia').hasClass('d-none'))
				//    $('#btnViewConstancia').removeClass('d-none')
			} else {//if (!$('#btnViewConstancia').hasClass('d-none'))
				//    $('#btnViewConstancia').addClass('d-none')
			}

			$('#txtItem_Bienes').val(obj[0].item);
			$('#txtNombre_Bienes').val(obj[0].nombre);
			$('#selEstado_Bienes').val(obj[0].estado);
			$('#txtFechaEntrega_Bienes').val(obj[0].fecha_entrega);
			$('#txtFechaDevolucion_Bienes').val(obj[0].fecha_devolucion);
			$('#txtCostoMn_Bienes').val(obj[0].costo_mn);
			$('#txtCostoMe_Bienes').val(obj[0].costo_me);
			$('#selCodDocEntrega_Bienes').val(obj[0].cod_documento);
			$('#txtFechaDocEntrega_Bienes').val(obj[0].fecha_documento);
			$('#txtNroDocEntrega_Bienes').val(obj[0].numero_documento);
			$('#txtCostoReposMn').val(obj[0].costo_reposicion_mn);
			$('#txtCostoReposMe').val(obj[0].costo_reposicion_me);
			$('#txtDescripcion_Bienes').val(obj[0].descripcion);
		},
		error: function error(response) { }
	});
};

var GetOtrosDatos = function GetOtrosDatos(pCodPersonal) {
	//var jsonData = {
	//    cod_personal: pCodPersonal
	//};
	$.ajax({
		type: "GET",
		url: portal + "/AdministracionPersonal/GetOtrosDatosPersonal?codPersonal=" + pCodPersonal/*.concat(g_codPersonal)*/,
		//data: JSON.stringify(jsonData),
		cache: false,
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		success: function success(data) {
			var obj = data;
			var container = $("#tbodyOtrosDatos");
			container.empty();
			$.each(obj, function (index, value) {
				var html = "<tr onclick=\"selectOtroDato(this);\" data-numero=\"".concat(value.codigo, "\">\n<td class=\"text-center\">").concat(value.descripcion, "</td>\n<td class=\"text-center\" style=\"min-width: 400px;\"><input type=\"text\" class=\"form-control\" maxlength=\"20\" value=\"").concat(value.valor, "\" /></td>\n                                  </tr>");
				container.append(html);
			});
		},
		error: function error(response) { }
	});
};

var GetMeritosDemeritos = function GetMeritosDemeritos(pCodPersonal) {
	//var jsonData = {
	//    cod_personal: pCodPersonal
	//};

	tableMeritosDemeritos = $('#tableMeritosDemeritos').DataTable({
		ajax: portal + "/AdministracionPersonal/GetMeritosDemeritosPersonal?codPersonal=" + pCodPersonal,
		initComplete: function (settings, json) {

		},
		bDestroy: true,
		"language": {
			"url": '/js/language.js'
		},
		searching: false,
		paging: false,
		columns: [
			{ "data": "item" },
			{ "data": "desc_tipo" },
			{ "data": "descripcion" },
			{ "data": "fecha" },
			{
				"data": null, render: function () {
					return '<i class="fas fa-eye text-primary cursor-pointer"></i></td>';
				}
			}
		],
		info: false
	});
	return;
	//$.ajax({
	//    type: "GET",
	//    url: portal + "/AdministracionPersonal/GetMeritosDemeritosPersonal?codPersonal=" + pCodPersonal/*.concat(pCodPersonal)*/,
	//    //data: JSON.stringify(jsonData),
	//    contentType: "application/json; charset=utf-8",
	//    dataType: "json",
	//    cache: false,
	//    success: function success(data) {
	//        //var obj = data;


	//        dataMeritosDemeritos = data;


	//        var container = $("#tbodyMeritosDemeritos");
	//        container.empty();
	//        $.each(dataMeritosDemeritos, function (index, value) {
	//            //var html = "<tr onclick=\"GetDetalleMeritosDemerito(this, ".concat(value.item, ");\" "data-item=\"").concat(value.item, " \">\n                                        <td class=\"text-center\">").concat(value.item, "</td>\n                                        <td class=\"text-center\">").concat(value.desc_tipo, "</td>\n                                        <td class=\"text-center\">").concat(value.descripcion, "</td>\n                                        <td class=\"text-center\">").concat(value.fecha, "</td>\n                                  </tr>");
	//            //onclick = "GetDetalleMeritosDemerito(this, ' + value.item + ');"
	//            var html = '<tr  data-item="' + value.item + '">' +
	//                '<td onclick = "GetDetalleMeritosDemerito(this, ' + value.item + ');" class="text-center">' + value.item + '</td>'+
	//                '<td onclick = "GetDetalleMeritosDemerito(this, ' + value.item + ');" class="text-center">' + value.desc_tipo + '</td>'+
	//                '<td onclick = "GetDetalleMeritosDemerito(this, ' + value.item + ');" class="text-center">' + value.descripcion + '</td>'+
	//                '<td onclick = "GetDetalleMeritosDemerito(this, ' + value.item + ');" class="text-center">' + value.fecha + '</td>'+
	//                '<td onclick = "GetDetalleMeritosDemerito(this, ' + value.item + ');" class="text-center td-ver"><i class="fas fa-eye"></i></td>'+
	//                '</tr>';
	//            $("#tbodyMeritosDemeritos").append(html);
	//        });
	//    },
	//    error: function error(response) { }
	//});
};

var GetDetalleMeritosDemerito = function GetDetalleMeritosDemerito(ptr, pItem) {
	g_accion_merito = 'E';
	g_item_merito = pItem;
	$("#tbodyMeritosDemeritos tr").removeClass('selected');
	$(ptr).addClass('selected');
	var row = dataMeritosDemeritos.find(function (r) {
		return r.item == pItem;
	});

	if (!(row === undefined)) {
		$('#txtItem_MeritosDemeritos').val(row.item);
		$('#selTipo_MeritosDemeritos').val(row.tipo);
		$('#txtFecha_MeritosDemeritos').val(row.fecha);
		$('#selReportado_MeritosDemeritos').val(row.cod_entidad);
		$('#txtDescripcion_MeritosDemeritos').val(row.descripcion);
	}
};

var GetOrganizacion = function GetOrganizacion(p_codPersonal) {
	var jsonData = {
		codPersonal: p_codPersonal
	};
	$.ajax({
		type: "GET",
		url: portal + '/Personal/GetOrganizacionLegajo',
		data: jsonData,
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		success: function success(data) {
			var obj = data;
			var jsonObj = obj[0];
			$('#txtCategoria_Org').val(obj[0].cod_categoria);
			$('#selCategoria_Org').val(obj[0].cod_categoria);
			$('#txtCargo_Org').val(obj[0].cod_cargo);
			$('#selCargo_Org').val(obj[0].cod_cargo);
			$('#txtAlcanceCargo_Org').val(obj[0].cod_alcance);
			$('#selAlcanceCargo_Org').val(obj[0].cod_alcance);
			$('#txtTipoPlanilla_Org').val(obj[0].cod_tipo_planilla);
			$('#selTipoPlanilla_Org').val(obj[0].cod_tipo_planilla);
			$('#txtProyecto_Org').val(obj[0].cod_proyecto);
			$('#selProyecto_Org').val(AllTrim(obj[0].cod_proyecto));
			$('#txtCC_Org').val(obj[0].cod_centro_costo);
			$('#selCC_Org').val(obj[0].cod_centro_costo);
			$('#txtLocalidad_Org').val(obj[0].cod_localidad);
			$('#selLocalidad_Org').val(obj[0].cod_localidad);
			$('#txtUN_Org').val(obj[0].cod_unidad_negocio);
			$('#selUN_Org').val(obj[0].cod_unidad_negocio);
			$('#txtArea_Org').val(obj[0].cod_area);
			$('#selArea_Org').val(obj[0].cod_area);
			$('#txtJefeDirecto_Org').val(obj[0].cod_jefe_directo);
			$('#selJefeDirecto_Org').val(obj[0].cod_jefe_directo);
			$('#txtCliente_Org').val(obj[0].cod_cliente);
			$('#selCliente_Org').val(obj[0].cod_cliente.trim());
			GetEstablecimientos('selEstablecimiento_Org', obj[0].cod_cliente);
			$('#txtEstablecimiento_Org').val(obj[0].cod_establecimiento);
			$('#selEstablecimiento_Org').val(obj[0].cod_establecimiento != null ? obj[0].cod_establecimiento.trim() : "");
			$('#lblGrupo1').html(obj[0].nombre_grupo1);
			$('#txtGrupo1').val(obj[0].cod_grupo1);
			$('#selGrupo1').val(obj[0].cod_grupo1);
			$('#lblGrupo2').html(obj[0].nombre_grupo2);
			$('#txtGrupo2').val(obj[0].cod_grupo2);
			$('#selGrupo2').val(obj[0].cod_grupo2);
			$('#lblGrupo3').html(obj[0].nombre_grupo3);
			$('#txtGrupo3').val(obj[0].cod_grupo3);
			$('#selGrupo3').val(obj[0].cod_grupo3);
			$('#lblGrupo4').html(obj[0].nombre_grupo4);
			$('#txtGrupo4').val(obj[0].cod_grupo4);
			$('#selGrupo4').val(obj[0].cod_grupo4);
			$('#lblGrupo5').html(obj[0].nombre_grupo5);
			$('#txtGrupo5').val(obj[0].cod_grupo5);
			$('#selGrupo5').val(obj[0].cod_grupo5);
			$('#lblGrupo6').html(obj[0].nombre_grupo6);
			$('#txtGrupo6').val(obj[0].cod_grupo6);
			$('#selGrupo6').val(obj[0].cod_grupo6);
			$('#lblGrupo7').html(obj[0].nombre_grupo7);
			$('#txtGrupo7').val(obj[0].cod_grupo7);
			$('#selGrupo7').val(obj[0].cod_grupo7);
			$('#lblGrupo8').html(obj[0].nombre_grupo8);
			$('#txtGrupo8').val(obj[0].cod_grupo8);
			$('#selGrupo8').val(obj[0].cod_grupo8);
			$('#lblGrupo9').html(obj[0].nombre_grupo9);
			$('#txtGrupo9').val(obj[0].cod_grupo9);
			$('#selGrupo9').val(obj[0].cod_grupo9);
			//console.log(jsonObj);
			//console.log(obj[0].cod_centro_costo);
			//console.log($('#selCC_Org'));
			//if (obj[0].cod_centro_costo == '-1') {
			//    console.log('sí es -1');
			//}
			//else {
			//    console.log('es', obj[0].cod_centro_costo);
			//}
		},
		error: function error(response) { }
	});
};

var GetPersonalDistribucionCC = function GetPersonalDistribucionCC(p_codPersonal, p_codProyecto) {
	var jsonData = {
		codPersonal: p_codPersonal,
		codProyecto: p_codProyecto
	};
	$.ajax({
		type: "GET",
		url: portal + '/Personal/GetDistribucionCCxAsiento',
		data: jsonData,
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		success: function success(data) {
			var obj = data;
			var container = $("#tbodyDistCC");
			container.empty();
			var i = 1;
			$.each(obj, function (index, value) {
				var NameTxt, Chk = '',
					ReadOnly = '';
				NameTxt = "txtCCPorcen".concat(i);

				if (value.porcentaje != 0) {
					Chk = 'checked';
					ReadOnly = '';
				} else {
					Chk = '';
					ReadOnly = 'disabled';
				}
				var html =
					'<tr data-numero="' + value.cod_centro_costo + '">' +
					'<td class="text-center">' + value.descripcion + '</td>' +
					'<td class="text-center">' +
					'<input type="checkbox" onchange="fnEnabledText(this, \'' + NameTxt + '\')" ' + Chk + ' />' +
					'</td>' +
					'<td class="text-center">' +
					'<input type="number" hideButtons id="' + NameTxt + '" style="max-width: 90px;" class="form-control" ' + ReadOnly + ' value="' + value.porcentaje + '" maxlength="3" />' +
					'</td>' +
					'</tr>';
				container.append(html);
				i++;
			});
			ActionsReactionsLoadRegex();
		},
		error: function error(response) { }
	});
};

var GetPersonalDistribucionLocalidad = function GetPersonalDistribucionLocalidad(p_codPersonal) {
	var jsonData = {
		codPersonal: p_codPersonal
	};
	$.ajax({
		type: "GET",
		url: portal + '/Personal/GetDistribucionLocalidadxAsiento',
		data: jsonData,
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		success: function success(data) {
			var obj = data;
			var container = $("#tbodyDistLocalidad");
			container.empty();
			var i = 1;
			$.each(obj, function (index, value) {
				var NameTxt,
					Chk = '',
					ReadOnly = '';
				NameTxt = "txtLOPorcen".concat(i);

				if (value.porcentaje != 0) {
					Chk = 'checked';
					ReadOnly = '';
				} else {
					Chk = '';
					ReadOnly = 'disabled';
				}
				var html =
					'<tr data-numero="' + value.cod_localidad + '">' +
					'<td class="text-center">' + value.descripcion + '</td>' +
					'<td class="text-center">' +
					'<input type="checkbox" onchange="fnEnabledText(this, \'' + NameTxt + '\')" ' + Chk + ' />' +
					'</td>' +
					'<td class="text-center">' +
					'<input type="number" hideButtons id="' + NameTxt + '" style="max-width: 90px;" class="form-control" ' + ReadOnly + ' value="' + value.porcentaje + '" maxlength="3" />' +
					'</td>' +
					'</tr>';
				container.append(html);
				i++;
			});
			ActionsReactionsLoadRegex();
		},
		error: function error(response) { }
	});
};

var GetPersonalDistribucionUnidadDeNegocio = function GetPersonalDistribucionUnidadDeNegocio() {
	var jsonData = {
		cod_personal: g_codPersonal
	};
	$.ajax({
		type: "GET",
		url: portal + "/Personal/GetDistribucionUnidadDeNegocioxAsiento",
		data: jsonData,
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		success: function success(data) {
			var obj = data;
			var container = $("#tbodyDistUnidadDeNegocio");
			container.empty();
			var i = 1;
			$.each(obj, function (index, value) {
				var NameTxt,
					Chk = '',
					ReadOnly = '';
				NameTxt = "txtUNPorcen".concat(i);

				if (value.porcentaje != 0) {
					Chk = 'checked';
					ReadOnly = '';
				} else {
					Chk = '';
					ReadOnly = 'disabled';
				}

				var html =
					'<tr data-numero="' + value.cod_unidad_negocio + '">' +
					'<td class="text-center">' + value.descripcion + '</td>' +
					'<td class="text-center">' +
					'<input type="checkbox" onchange="fnEnabledText(this, \'' + NameTxt + '\')" ' + Chk + ' />' +
					'</td>' +
					'<td class="text-center">' +
					'<input type="number" hideButtons id="' + NameTxt + '" style="max-width: 90px;" class="form-control" ' + ReadOnly + ' value="' + value.porcentaje + '" maxlength="3" />' +
					'</td>' +
					'</tr>';
				container.append(html);
				i++;
			});
			ActionsReactionsLoadRegex();
		},
		error: function error(response) { }
	});
};

var GetFamiliares = function GetFamiliares(p_codEntidad) {
	var jsonData = {
		codEntidad: p_codEntidad
	};
	$.ajax({
		type: "GET",
		url: portal + '/Personal/GetFamiliaresLegajo',
		cache: false,
		data: jsonData,
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		success: function success(data) {
			var obj = data;
			var container = $("#tbodyFamiliares");
			container.empty();
			$.each(obj, function (index, value) {
				var html = "<tr onclick=\"GetDetalleFamiliar(this);\" data-numero=\"".concat(value.num_familiar, "\">\n                                <td class=\"text-center\">").concat(value.num_familiar, "</td>\n                                <td class=\"text-center\">").concat(value.nombre, "</td>\n                                <td class=\"text-center\">").concat(value.parentesco, "</td>\n                            </tr>");
				container.append(html);
			});
		},
		error: function error(response) { }
	});
	$('#selParentesco_Familiar').trigger('change');
};

var GetDetalleFamiliar = function GetDetalleFamiliar(p_tr) {
	g_num_familiar = parseInt($(p_tr).data('numero'));
	$("#tbodyFamiliares tr").removeClass('selected');
	$(p_tr).addClass('selected');
	$('#txtNumero_Familiar').prop('disabled', true);
	var jsonData = {
		codEntidad: g_codEntidad,
		numero: $(p_tr).data('numero')
	};
	$.ajax({
		type: "GET",
		url: portal + '/Personal/GetDetalleFamiliarLegajo',
		data: jsonData,
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		async: false,
		success: function success(data) {
			var obj = data;
			var jSingle = obj[0];
			//console.log(jSingle);
			$('#txtNumero_Familiar').val(jSingle.num_familiar);
			$('#selParentesco_Familiar').val(jSingle.cod_parentesco).trigger('change');
			if (jSingle.cod_parentesco == '02') {
				$('#pnEstudiosSup').removeClass('hidden');
				if (jSingle.flg_estudios_superiores == 'S') $('#chkEstudiosSup').prop('checked', true); else $('#chkEstudiosSup').prop('checked', false);
			} else {
				$('#pnEstudiosSup').addClass('hidden');
				$('#chkEstudiosSup').prop('checked', false);
			}

			$('#txtApellidoP_Familiar').prop('value', jSingle.apellido_P);
			$('#txtApellidoM_Familiar').val(jSingle.apellido_M);
			$('#txtNombres_Familiar').val(jSingle.nombres);
			$('#txtFechaAlta_Familiar').val(jSingle.fecha_alta);
			$('#txtFechaNac_Familiar').val(jSingle.fecha_nacimiento);
			if (jSingle.flg_genero == 'M') $('#rbM_Familiar').prop('checked', true); else $('#rbF_Familiar').prop('checked', true);
			$('#selEstado_Familiar').val(jSingle.estado).change();
			$('#selDocid_Familiar').val(jSingle.cod_docid);
			$('#txtNroDocId_Familiar').val(jSingle.numero_docid);
			$('#selMotivoBaja_Familiar').val(jSingle.cod_motivo_cese);
			$('#txtFechaBaja_Familiar').val(jSingle.fecha_baja);
			$('#sel_Nacionalidad_Familiar').val(jSingle.nacionalidad);
			var ubigeo = jSingle.cod_ubigeo;

			if (ubigeo !== null && ubigeo.trim() !== '') {
				$('#selDepartamento_Familiar').val(jSingle.departamento).change();
				//GetProvincias('selProvincia_Familiar', jSingle.departamento);
				$('#selProvincia_Familiar').val(jSingle.provincia).change();
				//GetDistritos('selDistrito_Familiar', jSingle.provincia);
				$('#selDistrito_Familiar').val(jSingle.cod_ubigeo);
			}

			$('#txtNroRIHME').val(jSingle.numero_docinc);
			if (jSingle.flg_domicilio == 'S') $('#chkDomiciliado_Familiar').prop('checked', true); else $('#chkDomiciliado_Familiar').prop('checked', false);
			$('#selVia_Familiar').val(jSingle.cod_via);
			$('#txtDescVia_Familiar').val(jSingle.direccion);
			$('#txtTel_Familiar').val(jSingle.telefonos);
			$('#txtNro_Familiar').val(jSingle.numero);
			$('#txtInterior_Familiar').val(jSingle.interior);
			$('#selZona_Familiar').val(jSingle.cod_zona);
			$('#txtDescZona').val(jSingle.zona);
			$('#txtReferencia_Familiar').val(jSingle.referencia);
			if (jSingle.flg_EPS == 'S') $('#chkAfilEPS_Familiar').prop('checked', true); else $('#chkAfilEPS_Familiar').prop('checked', false);
			$('#txtFechaAfilEPS_Familiar').val(jSingle.fecha_afi_EPS);
			GetVinculoFamiliar($('#selParentesco_Familiar'));
			$('#selVinculoFamiliar').val(jSingle.cod_vinculo);

			var vinculo = jSingle.cod_vinculo;

			if (vinculo !== null && vinculo.trim() !== '') {
				GetVinculoSustentoFamiliar('selVinculoSustentoFam', jSingle.cod_vinculo);
			}

			$('#selVinculoSustentoFam').val(jSingle.cod_suntenta_vinculo);
			$('#txtNroDocVinculo').val(jSingle.numero_docpaternidad);
		},
		error: function error(response) { }
	});
};

var selectOtroDato = function selectOtroDato(p_this) {
	g_cod_OtroDato = parseInt($(p_this).data('numero'));
	$("#tbodyOtrosDatos tr").removeClass('selected');
	$(p_this).addClass('selected');
};

var GetEstablecimientos = function GetEstablecimientos(ddl, p_codCliente) {
	var container = $("#" + ddl);
	container.empty();
	if (!UIEmpty(p_codCliente)) {
		var jsonData = {
			codCliente: p_codCliente
		};
		UIAjax.invokeMethodAsync('Personal', 'GetEstablecimientoCliente', jsonData).then(function (data) {
			$.each(data, function (index, value) {
				var html = "<option value=\"".concat(value.codigo, "\">").concat(value.nombre, "</option>");
				container.append(html);
			});
		});
	}
};

var GetPersonalHistorial = function GetPersonalHistorial(p_codEntidad) {
	var jsonData = {
		codEntidad: p_codEntidad
	};
	$.ajax({
		type: "GET",
		url: portal + '/Personal/GetHistorialLegajo',
		data: jsonData,
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		success: function success(data) {
			var obj = data;
			var container = $("#tbodyHistorial");
			container.empty();
			var i = 1,
				periodo_ant,
				periodo_act,
				color1 = '#CECBE7',
				color2 = '#FFCB84',
				mostrarColor;
			$.each(obj, function (index, value) {
				var text_periodocidad,
					text_neto,
					text_pagado,
					text_vacio = '',
					text_si = 'Si',
					text_no = 'No',
					text_sctr_pension,
					text_sctr_salud;
				if (value.periodocidad_basico == 'M') text_periodocidad = 'Mensual'; else if (value.periodocidad_basico == 'H') text_periodocidad = 'Hora'; else if (value.periodocidad_basico == 'D') text_periodocidad = 'Dia'; else if (value.periodocidad_basico == 'Q') text_periodocidad = 'Quincenal';
				var simbolo = value.simbolo_mon_basico;
				text_neto = value.remuneracion - value.aporte_trab - value.descuentos;
				if (simbolo.substring(0, 2) == 'S/') text_pagado = text_neto * value.tipo_cambio; else text_pagado = text_neto;
				if (value.sctR_Pension == '0') text_sctr_pension = 'Ninguno'; else if (value.sctR_Pension == '1') text_sctr_pension = 'ONP'; else if (value.sctR_Pension == '2') text_sctr_pension = 'Privado';
				if (value.sctR_Salud == '0') text_sctr_salud = 'Ninguno'; else if (value.sctR_Salud == '1') text_sctr_salud = 'ESSALUD'; else if (value.sctR_Salud == '2') text_sctr_salud = 'EPS';

				if (i == 1) {
					periodo_ant = value.periodo;
					periodo_act = value.periodo;
					mostrarColor = color1;
				} else {
					periodo_act = value.periodo;
				}

				if (periodo_act == periodo_ant) {
					mostrarColor = mostrarColor;
				} else {
					mostrarColor = mostrarColor == color1 ? color2 : color1;
				}

				var html = "<tr style=\"background-color: ".concat(mostrarColor, ";\" data-cod=\"").concat(value.Cod_turno, "\">\n                                        <td class=\"text-center\">").concat(value.periodo, "</td>\n                                        <td class=\"text-center\">").concat(value.numero, "</td>\n                                        <td class=\"text-center\">").concat(value.cod_empresa, "</td>\n                                        <td class=\"text-center\">").concat(value.empresa, "</td>\n                                        <td class=\"text-center\">").concat(value.proceso, "</td>\n                                        <td class=\"text-center\">").concat(value.clase_Proceso, "</td>\n                                        <td class=\"text-center\">").concat(value.simbolo_mon_basico, "</td>\n                                        <td class=\"text-center\">").concat(value.basico.toFixed(2), "</td>\n                                        <td class=\"text-center\">").concat(text_periodocidad, "</td>\n                                        <td class=\"text-center\">").concat(value.remuneracion.toFixed(2), "</td>\n                                        <td class=\"text-center\">").concat(value.aporte_trab.toFixed(2), "</td>\n                                        <td class=\"text-center\">").concat(value.descuentos.toFixed(2), "</td>\n                                        <td class=\"text-center\">").concat(text_neto.toFixed(2), "</td>\n                                        <td class=\"text-center\">").concat(value.tipo_cambio.toFixed(4), "</td>\n                                        <td class=\"text-center\">").concat(text_pagado.toFixed(2), "</td>\n                                        <td class=\"text-center\">").concat(value.cod_trabajador, "</td>\n                                        <td class=\"text-center\">").concat(value.categoria, "</td>\n                                        <td class=\"text-center\">").concat(value.cargo, "</td>\n                                        <td class=\"text-center\">").concat(value.cod_tipo_planilla, "</td>\n                                        <td class=\"text-center\">").concat(value.tipo_planilla, "</td>\n                                        <td class=\"text-center\">").concat(value.cod_proyecto, "</td>\n                                        <td class=\"text-center\">").concat(value.proyecto, "</td>\n                                        <td class=\"text-center\">").concat(value.cod_centro_costo, "</td>\n                                        <td class=\"text-center\">").concat(value.centro_costo, "</td>\n                                        <td class=\"text-center\">").concat(value.cod_unidad_negocio, "</td>\n                                        <td class=\"text-center\">").concat(value.unidad_negocio, "</td>\n                                        <td class=\"text-center\">").concat(value.cod_area, "</td>\n                                        <td class=\"text-center\">").concat(value.area, "</td>\n                                        <td class=\"text-center\">").concat(value.cod_localidad, "</td>\n                                        <td class=\"text-center\">").concat(value.localidad, "</td>\n                                        <td class=\"text-center\">").concat(value.simbolo_mon_pago, "</td>\n                                        <td class=\"text-center\">").concat(value.nom_banco_Pago, "</td>\n                                        <td class=\"text-center\">").concat(value.num_cuenta_pago, "</td>\n                                        <td class=\"text-center\">").concat(value.simbolo_mon_cts, "</td>\n                                        <td class=\"text-center\">").concat(value.nom_banco_cts, "</td>\n                                        <td class=\"text-center\">").concat(value.num_cuenta_cts, "</td>\n\n                                        <td class=\"text-center\">").concat(value.pago_en_cta, "</td>\n\n                                        <td class=\"text-center\">").concat(convertDateFormat(value.fecha_ingreso), "</td>\n                                        <td class=\"text-center\">").concat(value.motivo_ingreso, "</td>\n                                        <td class=\"text-center\">").concat(convertDateFormat(value.fecha_cese), "</td>\n                                        <td class=\"text-center\">").concat(value.motivo_cese == null ? text_vacio : value.motivo_cese, "</td>\n                                        <td class=\"text-center\">").concat(value.tipo_trabajador, "</td>\n                                        <td class=\"text-center\">").concat(value.estado_laboral, "</td>\n                                        <td class=\"text-center\">").concat(value.afp, "</td>\n                                        <td class=\"text-center\">").concat(convertDateFormat(value.fecha_Afiliacion), "</td>\n                                        <td class=\"text-center\">").concat(value.tipo_contrato, "</td>\n\n                                        <td class=\"text-center\">").concat(value.Adelanto == 'N' ? text_no : text_si, "</td>\n\n                                        <td class=\"text-center\">").concat(value.porc_Adelanto, "</td>\n                                        <td class=\"text-center\">").concat(value.ESSALUD_Vida == '0' ? text_no : text_si, "</td>\n                                        <td class=\"text-center\">").concat(text_sctr_pension, "</td>\n                                        <td class=\"text-center\">").concat(text_sctr_salud, "</td>\n                                        <td class=\"text-center\">").concat(value.afiliacion_EPS == '0' ? text_no : text_si, "</td>\n                                        <td class=\"text-center\">").concat(value.aseg_Eps == null ? text_vacio : value.Aseg_Eps, "</td>\n                                        <td class=\"text-center\">").concat(value.sindicato == 'N' ? text_no : text_si, "</td>\n                                        <td class=\"text-center\">").concat(value.regimen_alternativo == 'N' ? text_no : text_si, "</td>\n                                        <td class=\"text-center\">").concat(value.jornada_maxima == 'N' ? text_no : text_si, "</td>\n                                        <td class=\"text-center\">").concat(value.horario_nocturno == 'N' ? text_no : text_si, "</td>\n                                        <td class=\"text-center\">").concat(value.vida_ley == 'N' ? text_no : text_si, "</td>\n                                        <td class=\"text-center\">").concat(value.aseg_vida_ley == null ? text_vacio : value.Aseg_vida_ley, "</td>\n                                        <td class=\"text-center\">").concat(value.tasa_vida_ley.toFixed(2), "</td>\n                                        <td class=\"text-center\">").concat(value.reloj == 'N' ? text_no : text_si, "</td>\n                                        <td class=\"text-center\">").concat(value.fotocheck, "</td>\n                                        <td class=\"text-center\">").concat(convertDateFormat(value.fecha_calculo), "</td>\n                                        <td class=\"text-center\">").concat(value.usuario == null ? text_vacio : value.usuario, "</td>\n                                    </tr>");
				container.append(html);
				periodo_ant = periodo_act;
				i++;
			});
		},
		error: function error(response) { }
	});
};

var GetVinculoFamiliar = function GetVinculoFamiliar(select) {
	var cod_parentesco = $(select).val();
	var jsonData = {
		cod_parentesco: cod_parentesco
	};
	$.ajax({
		type: "GET",
		url: portal + '/Ajax/GetFamilyBond',
		data: jsonData,
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		async: false,
		success: function success(data) {
			var container = $('#selVinculoFamiliar');
			$('#selVinculoSustentoFam').empty();
			container.empty();
			if (data.length > 0) {
				var html = '<option>Seleccione una opción</option>"';
				container.append(html);
				$.each(data, function (index, value) {
					html = '<option value="' + value.codigo + '">' + value.nombre + '</option>"';
					container.append(html);
				});
			}
		},
		error: function error(response) { }
		//selVinculoFamiliar
	});
};

var GetVinculoSustentoFamiliar = function GetVinculoSustentoFamiliar(ddl, p_codVinculo) {
	var jsonData = {
		cod_vinculo: p_codVinculo
	};
	$.ajax({
		type: "GET",
		url: portal + '/Ajax/GetFamilyBondSupport',
		data: jsonData,
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		async: false,
		success: function success(data) {
			var obj = data;
			var container = $("#" + ddl);
			container.empty();
			var html = '<option>Seleccione una opción</option>"';
			container.append(html);
			$.each(obj, function (index, value) {
				html = "<option value=\"".concat(value.codigo, "\">").concat(value.nombre, "</option>");
				container.append(html);
			});
		},
		error: function error(response) { }
	});
};

var GetDetalleRetencion = function GetDetalleRetencion(row) {
	$('#tbodyRetenciones tr').removeClass('selected');
	$(row).addClass('selected');
	var jsonData = {
		cod_personal: g_codPersonal,
		num_retencion: $(row).data('numero')
	};
	$.ajax({
		type: "GET",
		url: portal + '/Personal/GetDetalleRetencionesLegajo',
		data: jsonData,
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		success: function success(data) {
			var objE = data[0];
			$('#txtNro_Retencion').val(objE.num_retencion);
			$('#selConcepto_Retencion').val(objE.nroConcepto);
			$('#selTipo_Retencion').val(objE.flg_tipo_retencion);
			$('#selEstado_Retencion').val(objE.estado);
			$('#txtDesde_Retencion').val(objE.desde);
			$('#txtHasta_Retencion').val(objE.hasta);
			$('#txtBeneficiario_Retencion').val(objE.beneficiario);
			$('#txtNroDocId_Retencion').val(objE.doc_beneficiario);
			$('#txtValorPorcen_Retencion').val(objE.valor_porcen);
			$('#txtMonto_Retencion').val(objE.monto);
			var proceso = objE.cod_clase_proceso;

			if (proceso != null && proceso != '') {
				//$('#selectClaseProceso_Retencion').multipleSelect('setSelects', proceso.split(','));
				procesoArray = proceso.split(',');
				$("#selectClaseProceso_Retencion").val(procesoArray);
			} else {
				//$('#selectClaseProceso_Retencion').multipleSelect('setSelects', []);
				$("#selectClaseProceso_Retencion").val('');
			}

			$('#rbRemuBruta').prop('checked', false);
			$('#rbRemuNeta').prop('checked', false);
			$('#rbCuenta_Retencion').prop('checked', false);
			$('#rbEfectivo_Retencion').prop('checked', false);

			if (objE.flg_dscto_rem == 'B') {
				$('#rbRemuBruta').prop('checked', true);
			} else if (objE.flg_dscto_rem == 'N') {
				$('#rbRemuNeta').prop('checked', true);
			}

			if (objE.flg_pago_cuenta == 'S') {
				$('#rbCuenta_Retencion').prop('checked', true);
			} else if (objE.flg_pago_cuenta == 'N') {
				$('#rbEfectivo_Retencion').prop('checked', true);
			}

			$('#txtDistritoJudicial_Retencion').val(objE.distrito_judicial);
			$('#txtJuzgado_Retencion').val(objE.juzgado);
			$('#txtExpediente_Retencion').val(objE.expediente);
			$('#txtSecretario_Retencion').val(objE.secretario);
			$('#selBanco_Retencion').val(objE.cod_banco);
			$('#txtNroCuenta_Retencion').val(objE.num_cuenta);
		},
		error: function error(response) { }
	});
};

var GetRetenciones = function GetRetenciones(p_codPersonal) {
	var jsonData = {
		codPersonal: p_codPersonal
	};
	$.ajax({
		type: "GET",
		url: portal + '/Personal/GetRetencionesLegajo',
		cache: false,
		data: jsonData,
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		success: function success(data) {
			var obj = data;
			var container = $("#tbodyRetenciones");
			container.empty();
			$.each(obj, function (index, value) {
				var html = "<tr onclick=\"GetDetalleRetencion(this);\" data-numero=\"".concat(value.num_retencion, "\">\n                                <td class=\"text-center\">").concat(value.num_retencion, "</td>\n                                <td class=\"text-center\">").concat(value.tipo_retencion, "</td>\n                                <td class=\"text-center\">").concat(value.beneficiario, "</td>\n                            </tr>");
				container.append(html);
			});
		},
		error: function error(response) { }
	});
};

var PostRetencionPersonal = function PostRetencionPersonal() {
	//Comprobación
	var MsgError = '';
	var Input = $('#txtDesde_Retencion').val().trim();

	if (Input == "") {
		MsgError = 'Falta fecha desde en retención.';
	}

	Input = $('#txtHasta_Retencion').val().trim();

	if (Input == "" && $('#selTipo_Retencion').val() != '1') {
		MsgError = 'Falta fecha hasta en retención.';
	}

	Input = $('#txtValorPorcen_Retencion').val().trim();

	if (Input == "") {
		MsgError = 'Falta valor porcentaje.';
	}

	Input = $('#txtMonto_Retencion').val().trim();

	if (Input == "") {
		MsgError = 'Falta monto en retención.';
	}

	if (MsgError != "") {
		MsgBox(MsgError, "warning", false);
		return;
	}

	var accion = 'N'; //N=Nuevo G=Guardar(Este último"G" no es necesario, If N)    

	var num_retencion = $("#txtNro_Retencion").val();

	if (num_retencion != "") {
		accion = 'G';
	}

	var objE = {
		accion: accion,
		cod_personal: g_codPersonal,
		num_retencion: $("#txtNro_Retencion").val(),
		Beneficiario: $('#txtBeneficiario_Retencion').val(),
		Estado: $('#selEstado_Retencion').val(),
		Valor_porcen: $('#txtValorPorcen_Retencion').val(),
		monto: $('#txtMonto_Retencion').val(),
		Distrito_judicial: $('#txtDistritoJudicial_Retencion').val(),
		Juzgado: $('#txtJuzgado_Retencion').val(),
		Expediente: $('#txtExpediente_Retencion').val(),
		Secretario: $('#txtSecretario_Retencion').val(),
		Flg_dscto_rem: $('#rbRemuBruta').prop('checked') ? 'B' : 'N',
		cod_banco: $('#selBanco_Retencion').val(),
		num_cuenta: $('#txtNroCuenta_Retencion').val(),
		Flg_pago_cuenta: $('#rbCuenta_Retencion').prop('checked') ? 'S' : 'N',
		//List_clase_proceso: $('#selectClaseProceso_Retencion').multipleSelect('getSelects'),
		List_clase_proceso: $('#selectClaseProceso_Retencion').val(),
		desde: $('#txtDesde_Retencion').val(),
		hasta: $('#txtHasta_Retencion').val(),
		Flg_tipo_retencion: $('#selTipo_Retencion').val(),
		NroConcepto: $('#selConcepto_Retencion').val(),
		doc_beneficiario: $('#txtNroDocId_Retencion').val()
	};
	UIAjax.runFunctionAsync('Personal', 'PostRetencionPersonal', objE, function () {
		GetRetenciones(g_codPersonal);
		ClearInput('Re');
	}, function () {
		rpta = -1;
	});
};

var DeleteRetencion = async function DeleteRetencion(tr) {
	MsgBox('question', async function () {
		await UILoadingDefault('show');
		var Row = tr; //return;
		var num_retencion = $(Row[0]).data('numero');
		var jsonData = {
			cod_personal: g_codPersonal,
			num_retencion: num_retencion
		};
		await UIAjax.runFunctionAsync('Personal', 'DeleteRetencionPersonal', jsonData, function () {
			ClearInput('Re');
			$(Row).remove();
			g_num_bien = '';
			rpta = 1;
		}, function () {
			rpta = -1;
		});
		UILoadingDefault('hide');
	});
};

var getListClaseProceso = function getListClaseProceso() {
	var jsonData = {
		cod_proceso: 'P'
	};
	$.ajax({
		type: "GET",
		url: portal + '/Personal/GetListClaseProceso',
		data: jsonData,
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		async: false,
		success: function success(data) {
			var obj = data;
			var container = $("#selectClaseProceso_Retencion");
			container.empty();
			$.each(obj, function (index, value) {
				var html = "<option value=\"".concat(value.codigo, "\">").concat(value.nombre, "</option>");
				container.append(html);
			});
		},
		error: function error(response) { }
	});
};

var GetDocsEscaneados = function GetDocsEscaneados(p_codPersonal) {
	var jsonData = {
		codPersonal: p_codPersonal
	};
	$.ajax({
		type: "GET",
		url: portal + '/Personal/GetDocsEscaneadosLegajo',
		cache: false,
		data: jsonData,
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		success: function success(data) {
			var obj = data;
			var container = $("#tbodyDocsEscaneados");
			container.empty();
			$.each(obj, function (index, value) {
				//var html = "<tr data-item=\"".concat(value.item, "\" data-tipo=\"").concat(value.flg_tipo_archivo, "\">\n                                        <td onclick=\"GetDetalleDocEscaneado(this);\" class=\"text-center\">").concat(value.item, "</td>\n                                        <td onclick=\"GetDetalleDocEscaneado(this);\" class=\"text-center\">").concat(value.descripcion, "</td>\n                                        <td onclick=\"GetDetalleDocEscaneado(this);\" class=\"text-center\">").concat(value.tipo_archivo, "</td>\n                                        <td onclick=\"GetDetalleDocEscaneado(this);\" class=\"text-center td-ver\"><i class=\"fas fa-eye\"></i></td>\n                                  </tr>");
				var html = '<tr data-item="' + value.item + '" data-tipo="' + value.flg_tipo_archivo + '">'
					+ '<td onclick="GetDetalleDocEscaneado(this);" class="text-center">' + value.item + '</td>'
					+ '<td onclick="GetDetalleDocEscaneado(this);" class="text-center">' + value.descripcion + '</td>'
					+ '<td onclick="GetDetalleDocEscaneado(this);" class="text-center">' + value.tipo_archivo + '</td>'
					+ '<td onclick="GetDetalleDocEscaneado(this);" class="text-center td-ver"><i class="fas fa-eye"></i></td>'
					+ '</tr>';
				container.append(html);
			});
		},
		error: function error(response) { }
	});
};

var GetVariacionSueldo = function (p_codPersonal) {
	var jsonParam = {
		CodigoDePersonal: p_codPersonal,
		CodigoDeTipoPlanilla: '01',
		CodigoDeProceso: 'P',
		CodigoClaseDeProceso: 'B',
		Estado: 'C',
	};
	UIAjax.invokeMethodAsync('ajax/personal', 'GetVariacionSueldoPersonal', jsonParam).then(function (r) {
		tableVariacionSueldoVS = $('#tableVariacionSueldoVS').DataTable({
			data: r.data,
			initComplete: function (settings, json) {
				var api = this.api();
				api.$('tr').on('click', function (e) {
					let ths = $(this);
					api.$('tr.selected').removeClass('selected');
					ths.addClass('selected');
					let jsonRow = tableVariacionSueldoVS.row(ths).data();
					if (!UIEmpty(jsonRow)) {
						$('#txtFeachaDesdeVS').datepicker('setDate', jsonRow.fecha_desde);
						$('#txtMontoVS').val(jsonRow.monto);
						$('#txtObservacionesVS').val(jsonRow.observaciones);
						$("#divCollapseVS").collapse('show');
					}
				});
				$('#btnAgregarVS').off('click').on('click', function (e) {
					api.$('tr.selected').removeClass('selected');
					$('#txtFeachaDesdeVS').datepicker('setDate', new Date());
					$('#txtMontoVS').val('');
					$('#txtObservacionesVS').val('');
					$("#divCollapseVS").collapse('show');
				});
				$('#btnEliminarVS').off('click').on('click', function (e) {
					let jsonRow = tableVariacionSueldoVS.row(api.$('tr.selected')).data();
					if (UIEmpty(jsonRow)) {
						MsgBox('Debe seleccionar un item primero.', 'warning', true);
						return;
					}
					MsgBox('question', function () {
						let jsonParam = {
							CodigoDePersonal: jsonRow.cod_personal,
							Item: jsonRow.item,
						};
						UIAjax.runFunctionAsync('ajax/personal', 'DeleteVariacionSueldoPersonal', jsonParam, function () {
							$("#divCollapseVS").collapse('hide');
							GetVariacionSueldo(p_codPersonal);
						});
					});
				});
				$('#btnGuardarVS').off('click').on('click', function (e) {
					let jsonRow = tableVariacionSueldoVS.row(api.$('tr.selected')).data();
					let jsonParam = {
						CodigoDePersonal: p_codPersonal,
						Item: UIEmpty(jsonRow) ? null : jsonRow.item,
						//Fecha: $('#txtFeachaDesdeVS').datepicker('getDate'),
						Fecha: $('#txtFeachaDesdeVS').val(),
						Monto: $('#txtMontoVS').val(),
						Mensaje: $('#txtObservacionesVS').val(),
					};
					UIAjax.runFunctionAsync('ajax/personal', 'PostVariacionSueldoPersonal', jsonParam, function () {
						$("#divCollapseVS").collapse('hide');
						GetVariacionSueldo(p_codPersonal);
					});
				});
				$('#btnCancelarVS').off('click').on('click', function (e) {
					$("#divCollapseVS").collapse('hide');
				});
			},
			columnDefs: [
				{ className: "dt-body-center", targets: [0, 1, 2, 4] },
				{ className: "dt-head-center", targets: [0, 1, 2, 4] },
			],
			lengthChange: false,
			columns: [
				{ data: "item" },
				{ data: "fecha_desde" },
				{ data: "monto" },
				{ data: "observaciones" },
				{ data: "cod_usuario_autoriza" },
			],
			createdRow: function (row, data, index) {
				$(row).addClass('unread');
			}
		});
	});
};

var GetReloj = function (p_codPersonal) {
	var jsonParam = {
		CodigoDePersonal: p_codPersonal,
	};
	UIAjax.invokeMethodAsync('ajax/personal', 'GetRelojPersonal', jsonParam).then(function (r) {
		tableRelojRJ = $('#tableRelojRJ').DataTable({
			data: r.data,
			initComplete: function (settings, json) {
				var api = this.api();
				api.$('tr').on('click', function (e) {
					e.preventDefault();
					var ths = $(this);
					var taget = $(e.target);
					api.$('tr.selected').removeClass('selected');
					ths.addClass('selected');
					if (taget.hasClass('custom-control-label')) {
						var jsonRow = api.row(ths).data();
						var input = $(taget.prop('control'));
						var valor = input.is(':checked');
						var nvalor = !valor;
						var td = input.parents('td');
						input.prop('checked', nvalor);
						jsonRow.estado = nvalor ? 'S' : 'N';
					}
					else {

					}
				});
				$('#btnGuardarRJ').off('click').on('click', function (e) {
					//let jsonRows = api.row(api.$('tr.selected')).data();
					let jsonRows = api.rows().data();
					let jsonParam = {
						CodigoDePersonal: p_codPersonal,
						PersonalRelojes: []
					};
					$.each(jsonRows, function (i, v) {
						jsonParam.PersonalRelojes.push({
							cod_reloj: v.cod_reloj,
							fecha_inicio: v.fecha_inicio,
							fecha_fin: v.fecha_fin,
							estado: v.estado,
						});
					});
					UIAjax.runFunctionAsync('ajax/personal', 'PostRelojPersonal', jsonParam, function () {
						GetReloj(p_codPersonal);
					});
				});
			},
			columnDefs: [
				{ className: "dt-body-center", targets: [0, 1, 2, 4] },
				{ className: "dt-head-center", targets: [0, 1, 2, 4] },
			],
			lengthChange: false,
			columns: [
				{
					data: 'estado', render: function (data, type, row, meta) {
						return UIFunctions.Checkbox.HTML(`chkReloj${row.cod_personal}_${row.cod_reloj}`, data == 'S' ? true : false, '');
					}
				},
				{ data: "cod_reloj" },
				{ data: "localidad" },
				{
					data: "fecha_inicio", render: function (data, type, row, meta) {
						return `<input type="text" readonly="readonly" class="form-control form-control-sm DatePicker" />`;
					}, className: "w-180px"
				},
				{
					data: "fecha_fin", render: function (data, type, row, meta) {
						return `<input type="text" readonly="readonly" class="form-control form-control-sm DatePicker" />`;
					}, className: "w-180px"
				},
			],
			createdRow: function (row, data, index) {
				$(row).addClass('unread');
				var tr = $(row);
				var Inputs = $('.DatePicker', tr);
				Inputs.datepicker("destroy");
				Inputs.datepicker({
					format: "dd/mm/yyyy",
					autoclose: true,
					language: 'es'
				});
				var input0 = $(Inputs[0]);
				var input1 = $(Inputs[1]);
				input0.datepicker("setDate", data.fecha_inicio);
				input1.datepicker("setDate", data.fecha_fin);
				input0.on('changeDate', function (e) {
					let ths = $(this);
					data.fecha_inicio = ths.val();
				});
				input1.on('changeDate', function (e) {
					let ths = $(this);
					data.fecha_fin = ths.val();
				});
			}
		});
	});
};


var GetPersonalcontrol = function GetPersonalcontrol(p_codPersonal) {
	//var jsonData = {
	//    codPersonal: g_codPersonal
	//};

	$.ajax({
		type: "GET",
		url: portal + "/Personal/GetPersonalcontrol?codPersonal=" + p_codPersonal,
		//data: jsonData,
		cache: false,
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		success: function success(data) {
			var obj = data;
			var container = $("#tbodyControl");
			container.empty();
			$.each(obj, function (index, value) {
				var html = "<tr onclick=\"EditRowPersonalcontrol(this);\" data-codigo=\"".concat(value.cod_control, "\" data-json='").concat(JSON.stringify(value), "'>\n                                        <td class=\"text-center\">").concat(value.nombre, "</td>\n                                        <td class=\"text-center\">").concat(value.fecha_emision, "</td>\n                                        <td class=\"text-center\">").concat(value.fecha_caducidad, "</td>\n                                  </tr>");
				container.append(html);
			});
		},
		error: function error(response) {
			MsgBox("error");
		}
	});
};

var EliminarDocEscan = async function EliminarDocEscan(Row) {
	MsgBox('question', async function () {
		var jsonParam = {
			cod_personal: g_codPersonal,
			item: Row.data('item')
		};
		await UILoadingDefault('show');
		//Lo hicieron mal, no debía ser GET pero ni modo
		await UIAjax.invokeMethodAsync('Personal', 'DeletePersonaldocumentos', jsonParam).then(function (data) {
			if (data.respuesta == 1) {
				$('#tbodyDocsEscaneados tr.selected').remove();
				MsgBox("success");
			} else {
				MsgBox("warning");
			}
		});
		UILoadingDefault('hide');
	});
};

var PostPersonalcontrol = function PostPersonalcontrol() {
	var codControl = $('#selControles').val();
	var fechaEmision = $('#txtFechaEmision').val();
	var fechaCaducidad = $('#txtFechaCaducidad').val();
	var jsonData = {
		codPersonal: g_codPersonal,
		codControl: codControl,
		fechaEmision: fechaEmision,
		fechaCaducidad: fechaCaducidad
	};
	$.ajax({
		type: "GET",
		url: portal + '/Personal/PostPersonalcontrol',
		data: jsonData,
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		success: function success(data) {
			if (data.respuesta == 1) {
				$("#addControl").collapse('hide');
				GetPersonalcontrol(g_codPersonal);
				MsgBox("success");
			} else {
				MsgBox("warning");
			}
		},
		error: function error(response) {
			MsgBox("error");
		}
	});
};

async function GuardarDocEscan() {
	//let Formulario = $("#frmAgregarDocumentos").serialize();
	////console.log(Formulario);
	//var File = $("#fileDocEscan")[0].files[0];
	var flg_tipo_archivo = $('#selTipoArchivo').val();
	var descripcionInput = $('#txtDescripcion_Doc').val();
	var input = document.getElementById('fileDocEscan');
	var files = input.files;
	//var formData = new FormData();
	//formData.append("codPersonal", g_codPersonal);
	//formData.append("descripcion", descripcionInput);
	//formData.append("fileDoc", files[0]);
	//formData.append("flgTipoArchivo", flg_tipo_archivo);    
	//$.ajax({
	//    url: GetUrl("Personal/PostPersonaldocumentos"),
	//    data: formData,
	//    processData: false,
	//    contentType: false,
	//    type: "POST",
	//    success: function success(data) {
	//        if (data.respuesta == 1) {
	//            GetDocsEscaneados(g_codPersonal);
	//            MsgBox("success");
	//        } else {
	//            MsgBox(data.mensaje, "warning", false);
	//        }
	//    },
	//    error: function error(response) {
	//        MsgBox("error");
	//    }
	//});
	var jsonParam = {
		codPersonal: g_codPersonal,
		descripcion: descripcionInput,
		fileDoc: files[0],
		flgTipoArchivo: flg_tipo_archivo,
	};
	await UILoadingDefault("show");
	await UIAjax.runFormAsync('Personal', 'PostPersonaldocumentos', jsonParam, function () {
		GetDocsEscaneados(g_codPersonal);
	});
	UILoadingDefault("hide");
};

var GetDetalleDocEscaneado = function GetDetalleDocEscaneado(ths) {
	$("#tbodyDocsEscaneados tr").each(function (a) {
		$(this).removeClass('selected');
	});
	var Row = $(ths).parent();
	Row.addClass('selected');

	if (!$(ths).hasClass('td-ver')) {
		return;
	}

	var jsonData = {
		codPersonal: g_codPersonal,
		item: Row.data('item'),
		tipo: Row.data('tipo')
	};
	var html;
	var url = GetUrl("Personal/GetItemPersonaldocumentos") + '?codPersonal=' + jsonData.codPersonal + '&item=' + jsonData.item + '&tipo=' + jsonData.tipo;
	var container = $('#divVerDocumento');
	var modaldialog = '#modaldialogVerDocumento';
	container.empty();

	if (jsonData.tipo == 1) {
		var _jsonParam = {
			codPersonal: jsonData.codPersonal,
			item: jsonData.item,
			tipo: jsonData.tipo,
		};
		UIAjax.invokeBlobAsync('Personal', 'GetItemPersonaldocumentos', _jsonParam, true).then(function (r) {
			//console.log(r);
			html = '<img class="w-100" src="' + r + '" >';
			container.append(html);
		});
		//html = '<img class="w-100" src="' + url + '" >';
	} else if (jsonData.tipo == 2 || jsonData.tipo == 3) {
		html = '<div class="text-center">' + '<a href="' + url + '">Descargar el documento</a>' + '</div>';
		container.append(html);
	}

	$('#modalVerDocumento').modal('show');
};

var DeletePersonalcontrol = function DeletePersonalcontrol(tr) {
	//var Row = $(tr);
	//Row.addClass('selected');
	//var codControl = $(Row[0]).data('codigo');
	//var jsonParam = {
	//    codPersonal: g_codPersonal,
	//    codControl: codControl
	//};
	//$.post(portal + '/Personal/DeletePersonalcontrol', jsonParam).done(function (data) {
	//    if (data.respuesta == 1) {
	//        $(Row).remove();
	//        MsgBox("success");
	//    } else {
	//        MsgBox("warning");
	//    }
	//}).fail(function (data) {
	//    MsgBox("error");
	//});
	//$("#addControl").collapse('hide');
	MsgBox('question', async function () {
		var Row = $(tr);
		Row.addClass('selected');
		var codControl = $(Row[0]).data('codigo');
		var jsonParam = {
			codPersonal: g_codPersonal,
			codControl: codControl,
		};
		await UILoadingDefault('show');
		//Lo hicieron mal, no debía ser GET pero ni modo
		await UIAjax.runFunctionAsync('Personal', 'DeletePersonalcontrol', jsonParam, function () {
			$(Row).remove();
		});
		$("#addControl").collapse('hide');
		UILoadingDefault('hide');
	});
};

var EditRowPersonalcontrol = function EditRowPersonalcontrol(ths) {
	$('#tbodyControl tr').each(function (a) {
		$(this).removeClass('selected');
	});
	var Row = $(ths);
	Row.addClass('selected');
	var jsonData = $(Row[0]).data('json');
	$('#selControles').val(jsonData.cod_control);
	$('#txtFechaEmision').val(jsonData.fecha_emision);
	$('#txtFechaCaducidad').val(jsonData.fecha_caducidad); //$('#selControles').prop('disabled', '');

	$('#selControles').attr('disabled', 'disabled');
	$("#addControl").collapse('show'); //toggle
};

var GetFormacion = function GetFormacion(p_codPersonal) {
	//var jsonData = {
	//    codPersonal: p_codPersonal
	//};

	$.ajax({
		type: "GET",
		url: portal + '/Personal/GetFormacionLegajo?codPersonal=' + p_codPersonal,
		//data: jsonData,
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		success: function success(data) {
			var obj = data;
			var container = $("#tbodyFormacion");
			container.empty();
			$.each(obj, function (index, value) {
				var html = "<tr onclick=\"GetDetalleFormacion(this);\" data-item=\"".concat(value.item, "\">\n                                <td class=\"text-center\">").concat(value.item, "</td>\n                                <td class=\"text-center\">").concat(value.titulo, "</td>\n                                <td class=\"text-center\">").concat(value.nivel_academico, "</td>\n                                <td class=\"text-center\">").concat(value.desde, "</td>\n                                <td class=\"text-center\">").concat(value.hasta, "</td>\n                            </tr>");
				container.append(html);
			});
		},
		error: function error(response) { }
	});
	$('#selTipoInst_Formacion').trigger('change');
	//$('#selInstEducativa_Formacion').trigger('change');
	//GetCarreras('selCarrera_Formacion', container.val());
};

var GetDetalleFormacion = function GetDetalleFormacion(p_tr) {
	g_accion_formacion = 'E';
	g_num_formacion = parseInt($(p_tr).data('item'));
	$("#tbodyFormacion tr").removeClass('selected');
	$(p_tr).addClass('selected');
	var jsonData = {
		codPersonal: g_codPersonal,
		item: g_num_formacion
	};
	$.ajax({
		type: "GET",
		url: portal + '/Personal/GetDetalleFormacionLegajo',
		data: jsonData,
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		async: false,
		success: function success(data) {
			var obj = data;
			var jSingle = data[0];
			$('#selNivelAca_Formacion').val(jSingle.cod_nivel_academico);
			$('#txtTitulo_Formacion').val(jSingle.titulo);
			$('#selPais_Formacion').val(jSingle.cod_pais);
			(jSingle.cod_inst_edu == '1') ? $('#rbRegPub_Formacion').prop('checked', true).change() : $('#rbRegPri_Formacion').prop('checked', true).change();
			$('#selClase_Formacion').val(jSingle.cod_clase_inst_edu).change();
			$('#selTipoInst_Formacion').val(jSingle.cod_tipo_instit).change();
			////GetInstEdu("selInstEducativa_Formacion", obj[0].cod_inst_edu, obj[0].cod_tipo_instit);
			//GetInstEdu("selInstEducativa_Formacion", obj[0].cod_tipo_instit);
			$('#selInstEducativa_Formacion').val(jSingle.cod_inst_sunat).change();
			//GetCarreras('selCarrera_Formacion', obj[0].cod_inst_sunat);
			$('#txtAnio_Formacion').val(jSingle.duracion_anos);
			$('#txtMeses_Formacion').val(jSingle.duracion_Meses);
			$('#txtDias_Formacion').val(jSingle.duracion_dias);
			$('#txtAnioEgreso_Formacion').val(jSingle.ano_egreso);
			$('#txtDesde_Formacion').val(jSingle.desde);
			$('#txtHasta_Formacion').val(jSingle.hasta);
			$('#selCarrera_Formacion').val(jSingle.cod_Carrera);
			$('#txtRegimen_Formacion').val(jSingle.regimen);
			$('#txtTipoInst_Formacion').val(jSingle.tipo_Instit);
			$('#txtInst_Formacion').val(jSingle.institucion);
			$('#txtNroColeg_Formacion').val(jSingle.numero_colegiatura);
		},
		error: function error(response) {
			MsgBox("error");
		}
	});
};

var GetInstEdu = function GetInstEdu(ddl,/* p_regimen,*/ p_cod_tipo_inst) {
	var jsonData = {
		/*regime: p_regimen,*/
		cod_inst_edu: p_cod_tipo_inst
	};
	$.ajax({
		type: "GET",
		url: portal + '/Ajax/GetEducationalInstitution',
		data: jsonData,
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		async: false,
		success: function success(data) {
			var obj = data;
			var container = $("#" + ddl);
			container.empty();
			$.each(obj, function (index, value) {
				container.append("<option value=\"".concat(value.codigo, "\">").concat(value.nombre, "</option>"));
			});
			//container.prop('selectedIndex', 0);            
		},
		error: function error(response) { }
	});
};

var GetCarreras = function GetCarreras(ddl, p_cod_inst) {
	var jsonData = {
		idInstitute: p_cod_inst
	};
	$.ajax({
		type: "GET",
		url: portal + '/Ajax/GetCareer',
		data: jsonData,
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		async: false,
		success: function success(data) {
			var obj = data;
			var container = $("#" + ddl);
			container.empty();
			$.each(obj, function (index, value) {
				container.append("<option value=\"".concat(value.codigo, "\">").concat(value.nombre, "</option>"));
			});
		},
		error: function error(response) { }
	});
};

var EliminarOtroDato = function EliminarOtroDato(pCodigo) {
	var obj = {
		codigo: pCodigo
	};
	$.post(portal + '/AdministracionPersonal/DeleteOtroDatoLegajo', obj).done(function (data) {
		if (data.respuesta == 1) {
			GetOtrosDatos(g_codPersonal);
			$('#addOtrosDatos').collapse('hide');
			MsgBox("success");
		} else {
			MsgBox("warning");
		}
	}).fail(function (data) {
		MsgBox("error");
	});
};

var GrabarRptaOtrosDatos = function GrabarRptaOtrosDatos(pCodigo, pValor) {
	var rpta = -1;
	var obj = {
		codPersonal: g_codPersonal,
		codigo: pCodigo,
		valor: pValor
	};
	$.post(portal + '/AdministracionPersonal/PostRptaOtrosDatosLegajo', obj).done(function (data) {
		if (data.respuesta == 1) {
			GetOtrosDatos(g_codPersonal);
			$('#addOtrosDatos').collapse('hide');
			MsgBox("success");
			rpta = 1;
		} else {
			MsgBox("warning");
			rpta = -1;
		}
	}).fail(function (data) {
		MsgBox("error");
		rpta = -1;
	});
	return rpta;
};

var GrabarOtroDato = function GrabarOtroDato(pDescripcion, pAbrv, pTipoDato) {
	var obj = {
		descripcion: pDescripcion,
		abrv: pAbrv,
		tipoDato: pTipoDato
	};
	$.post(portal + '/AdministracionPersonal/PostOtroDatoLegajo', obj).done(function (data) {
		if (data.respuesta == 1) {
			GetOtrosDatos(g_codPersonal);
			$('#addOtrosDatos').collapse('hide');
			rpta = 1;
			MsgBox("success");
		} else {
			MsgBox("warning");
			rpta = -1;
		}
	}).fail(function (data) {
		MsgBox("error");
		rpta = -1;
	});
};

var GrabarRelojPersonal = function GrabarRelojPersonal(pCodReloj, pIni, pFin, pEstado) {
	var rpta = -1;
	var obj = {
		codPersonal: g_codPersonal,
		codReloj: pCodReloj,
		fechaIni: pIni,
		fechaFin: pFin,
		estado: pEstado
	};
	$.post(portal + '/AdministracionPersonal/PostAsignaRelojLegajo', obj).done(function (data) {
		if (data.respuesta == 1) {
			MsgBox("success");
			rpta = 1;
		} else {
			MsgBox("warning");
			rpta = -1;
		}
	}).fail(function (data) {
		MsgBox("error");
		rpta = -1;
	});
	return rpta;
};

var EliminarBienes = function EliminarBienes(pItem) {
	MsgBox('question', async function () {
		var jsonParam = {
			codPersonal: g_codPersonal,
			item: pItem
		};
		await UILoadingDefault('show');
		await UIAjax.invokeFunctionAsync('AdministracionPersonal', 'DeleteBienesLegajo', jsonParam).then(function (data) {
			if (data.respuesta == 1) {
				MsgBox("success");
				GetBienes(g_codPersonal);
				ClearInput('Bi');
				g_num_bien = '';
				rpta = 1;
			} else {
				MsgBox("warning");
				rpta = -1;
			}
		});
		UILoadingDefault('hide');
	});
};

var GrabarBienes = function GrabarBienes() {
	var obj = {
		accion: g_accion_bienes,
		codPersonal: g_codPersonal,
		item: g_num_bien,
		nombre: $('#txtNombre_Bienes').val(),
		estado: $('#selEstado_Bienes').val(),
		fechaEntrega: $('#txtFechaEntrega_Bienes').val() == '00/00/0000' ? '' : $('#txtFechaEntrega_Bienes').val(),
		fechaDevolucion: $('#txtFechaDevolucion_Bienes').val() == '00/00/0000' ? '' : $('#txtFechaDevolucion_Bienes').val(),
		costoMN: $('#txtCostoMn_Bienes').val(),
		costoME: $('#txtCostoMe_Bienes').val(),
		codDoc: $('#selCodDocEntrega_Bienes').val(),
		fechaDoc: $('#txtFechaDocEntrega_Bienes').val(),
		numeroDoc: $('#txtNroDocEntrega_Bienes').val(),
		costoRepoMN: $('#txtCostoReposMn').val(),
		costoRepoME: $('#txtCostoReposMe').val(),
		descripcion: $('#txtDescripcion_Bienes').val()
	};
	$.post(portal + '/AdministracionPersonal/PostBienesLegajo', obj).done(function (data) {
		if (data.respuesta == 1) {
			MsgBox("success");
			ClearInput('Bi');
			GetBienes(g_codPersonal);
			rpta = 1;
		} else {
			MsgBox("warning");
			rpta = -1;
		}
	}).fail(function (data) {
		MsgBox("error");
		rpta = -1;
	});
};

var EliminarMeritoDemerito = function EliminarMeritoDemerito(pItem) {
	MsgBox('question', async function () {
		var jsonParam = {
			codPersonal: g_codPersonal,
			item: pItem
		};
		await UILoadingDefault('show');
		await UIAjax.invokeFunctionAsync('AdministracionPersonal', 'DeleteMeritoDemeritoLegajo', jsonParam).then(function (data) {
			if (data.respuesta == 1) {
				MsgBox("success");
				GetMeritosDemeritos(g_codPersonal);
				ClearInput('Me');
				g_item_merito = 0;
				rpta = 1;
			} else {
				MsgBox("warning");
				rpta = -1;
			}
		});
		UILoadingDefault('hide');
	});
};

var GrabarMeritosDemeritos = function GrabarMeritosDemeritos() {
	var input = document.getElementById('file');
	var files = input.files;
	var formData = new FormData();
	formData.append("accion", g_accion_merito);
	formData.append("codPersonal", g_codPersonal);
	formData.append("item", g_item_merito);
	formData.append("tipo", $('#selTipo_MeritosDemeritos').val());
	formData.append("codEntidad", $('#selReportado_MeritosDemeritos').val());
	formData.append("fecha", $('#txtFecha_MeritosDemeritos').val());
	formData.append("descripcion", $('#txtDescripcion_MeritosDemeritos').val());
	formData.append("document", files[0]);
	//formData.append("existeArchivo", $('#hdExisteArchivo_MeritosDemeritos').val());
	formData.append("existeArchivo", $('#lblFileMeritosFaltas').text() == 'Actualizar' ? 'S' : 'N');

	$.ajax({
		url: portal + '/AdministracionPersonal/PostMeritoDemeritoLegajo',
		data: formData,
		processData: false,
		contentType: false,
		type: "POST",
		success: function success(data) {
			if (data.respuesta == 1) {
				MsgBox("success");
				ClearInput('Me');
				GetMeritosDemeritos(g_codPersonal);
				rpta = 1;
			} else {
				MsgBox("warning");
				rpta = -1;
			}
		}
	});
};

var GrabarDatosGenerales = function GrabarDatosGenerales() {
	var saveObj = {
		codPersonal: g_codPersonal,
		codEntidad: g_codEntidad,
		apellidoP: $('#txtApePat').val(),
		apellidoM: $('#txtApeMat').val(),
		nombres: $('#txtNombres').val(),
		ruc: $('#txtRuc_data').val(),
		codVia: $('#selVia').val(),
		descVia: $('#txtViaDesc').val(),
		numero: $('#txtNro').val(),
		interior: $('#txtInt_data').val(),
		dpto: $('#txtDpto').val(),
		manzana: $('#txtManzana').val(),
		lote: $('#txtLote').val(),
		kilometro: $('#txtKilometro').val(),
		block: $('#txtBlock').val(),
		etapa: $('#txtEtapa').val(),
		codZona: $('#selZona').val(),
		descZona: $('#txtZonaDesc').val(),
		codUbigeo: $('#selDistrito').val(),
		referencia: $('#txtReferencia').val(),
		telPrefijo: $('#txtPreFijo').val(),
		telTelefono: $('#txtTelCasa').val(),
		telParticular: $('#txtTelParticular').val(),
		telLaboral: $('#txtTelLaboral').val(),
		telAnexo: $('#txtTelAnexo').val(),
		email: $('#txtEmail').val(),
		emailPersonal: $('#txtEmailPer').val(),
		web: $('#txtWeb').val(),
		alias: $('#txtAlias').val()
	};
	$.post(portal + '/AdministracionPersonal/PostDatosGeneralesLegajo', saveObj).done(function (data) {
		if (data.respuesta == 1) {
			//$('#modalEncuesta').modal('hide');
			MsgBox("success");
		} else {
			MsgBox("warning");
		}
	}).fail(function (data) {
		MsgBox("error");
	});
};

var GrabarDatosPersonalesJefe = function GrabarDatosPersonalesJefe() {
	var v_sexo;
	if ($('#rbMasculino').prop('checked')) v_sexo = 1; else if ($('#rbFemenino').prop('checked')) v_sexo = 2; else {
		MsgBox("Seleccione el sexo.", "warning", true);
		return;
	}
	var obj = {
		codEntidad: g_codEntidad,
		codBrevete: $('#selTipoBrevete').val(),
		nroBrevete: $('#txtNroBrevete').val(),
		sexo: v_sexo,
		talla: $('#txtTalla').val(),
		colorPiel: $('#txtColorPiel').val(),
		peso: $('#txtPeso').val(),
		ojos: $('#txtOjos').val(),
		pelo: $('#txtPelo').val(),
		alergias: $('#txtAlergias').val(),
		ojoIz: $('#txtOjoIzq').val(),
		ojoDe: $('#txtOjoDer').val(),
		oidoIz: $('#txtOidosIzq').val(),
		oidoDe: $('#txtOidosDer').val(),
		polo: $('#selTallaPolo').val(),
		camisa: $('#selTallaCamisa').val(),
		pantalon: $('#selPantalon').val(),
		casaca: $('#selCasaca').val(),
		zapato: $('#selTallaZapato').val(),
		zapatilla: $('#selTallaZapatilla').val(),
		tipoSangre: $('#selGrupoSanguineo').val()
	};
	$.post(portal + '/AdministracionPersonal/PostDatosPersonalesLegajo', obj).done(function (data) {
		if (data.respuesta == 1) {
			//$('#modalEncuesta').modal('hide');
			MsgBox("success");
		} else {
			MsgBox("warning");
		}
	}).fail(function (data) {
		MsgBox("error");
	});
};

var GrabarOrganizacion = function GrabarOrganizacion() {
	var arrayDistLocalidad = [];
	var arrayDistCC = [];
	var arrayDistUN = [];
	var k = 0;
	var totalDistCC = 0;
	$('#tbodyDistCC tr').each(function (e) {
		//if (($(this).find('td input[type=checkbox]')).prop('checked')) {
		var Row = $(this);
		var monto = $(Row.find('td input[type=number]')).val();
		if (monto.length == 0) monto = 0;
		if (!isEmpty(monto)) {
			arrayDistCC.push({});
			arrayDistCC[k].item = k + 1;
			arrayDistCC[k].valor = monto;
			arrayDistCC[k].codigo = $(this).data('numero');
			totalDistCC += parseFloat(monto);
			k++; //}
		}
	});

	if (totalDistCC > 100 && k > 0) {
		MsgBox("El porcentaje total en Centro de Costo debe ser 100.", "warning", true);
		return;
	}

	var totalDistLocalidad = 0;
	k = 0;
	$('#tbodyDistLocalidad tr').each(function () {
		var monto = $($(this).find('td input[type=number]')).val();
		if (monto.length == 0) monto = 0;
		if (!isEmpty(monto)) {
			arrayDistLocalidad.push({});
			arrayDistLocalidad[k].item = k + 1;
			arrayDistLocalidad[k].valor = $($(this).find('td input[type=number]')).val();
			arrayDistLocalidad[k].codigo = $(this).data('numero');
			totalDistLocalidad += parseFloat(monto);
			k++;
		}
	});

	if (totalDistLocalidad > 100 && k > 0) {
		MsgBox("El porcentaje total en Localidad debe ser 100.", "warning", true);
		return;
	}

	var totalDistUnidadDeNegocio = 0;
	k = 0;
	$('#tbodyDistUnidadDeNegocio tr').each(function () {
		var monto = $($(this).find('td input[type=number]')).val();
		if (monto.length == 0) monto = 0;
		if (!isEmpty(monto)) {
			arrayDistUN.push({});
			arrayDistUN[k].item = k + 1;
			arrayDistUN[k].valor = $($(this).find('td input[type=number]')).val();
			arrayDistUN[k].codigo = $(this).data('numero');
			totalDistUnidadDeNegocio += parseFloat(monto);
			k++;
		}
	});

	if (totalDistUnidadDeNegocio > 100 && k > 0) {
		MsgBox("El porcentaje total en Unidad de Negocio debe ser 100.", "warning", true);
		return;
	}

	var obj = {
		codPersonal: g_codPersonal,
		categoria: $('#selCategoria_Org').val(),
		cargo: $('#selCargo_Org').val(),
		alcanceCargo: $('#selAlcanceCargo_Org').val(),
		planilla: $('#selTipoPlanilla_Org').val(),
		proyecto: $('#selProyecto_Org').val(),
		cc: $('#selCC_Org').val(),
		localidad: $('#selLocalidad_Org').val(),
		un: $('#selUN_Org').val(),
		area: $('#selArea_Org').val(),
		jefeDirecto: $('#selJefeDirecto_Org').val(),
		cliente: $('#selCliente_Org').val(),
		establecimiento: $('#selEstablecimiento_Org').val(),
		grupo1: $('#selGrupo1').val(),
		grupo2: $('#selGrupo2').val(),
		grupo3: $('#selGrupo3').val(),
		grupo4: $('#selGrupo4').val(),
		grupo5: $('#selGrupo5').val(),
		grupo6: $('#selGrupo6').val(),
		grupo7: $('#selGrupo7').val(),
		grupo8: $('#selGrupo8').val(),
		grupo9: $('#selGrupo9').val(),
		listCC: arrayDistCC,
		listLocalidad: arrayDistLocalidad,
		listUN: arrayDistUN
	};
	$.post(portal + '/Personal/PostOrganizacionLegajo', obj).done(function (data) {
		if (data.respuesta == 1) {
			MsgBox("success");
			GetOrganizacion(g_codPersonal);
			GetPersonalDistribucionCC(g_codPersonal, $('#selProyecto_Org').val());
			GetPersonalDistribucionLocalidad(g_codPersonal);
			GetPersonalDistribucionUnidadDeNegocio();
		} else {
			MsgBox("warning");
		}
	}).fail(function (data) {
		MsgBox("error");
	});
};

var GrabarHorarios = async function GrabarHorarios(p_d1, p_d2, p_d3, p_d4, p_d5, p_d6, p_d7) {
	var obj = {
		codPersonal: g_codPersonal,
		d1: p_d1,
		d2: p_d2,
		d3: p_d3,
		d4: p_d4,
		d5: p_d5,
		d6: p_d6,
		d7: p_d7
	};
	await UIAjax.runFunctionAsync('AdministracionPersonal', 'PostHorariosLegajo', obj);
};

var GrabarDatosLaborales = async function GrabarDatosLaborales() {
	var v_flg_afiliacion_eps, v_flg_sctr_pension, v_flg_sctr_salud, v_flg_senati, v_flg_vida_ley, v_flg_jornada_maxima, v_flg_regimen_alternativo, v_flg_horario_nocturno, v_flg_HH_Auto, v_flg_sindicato, v_flg_alimentos, v_flg_movilidad, v_ESSALUD_VIDA, v_flg_asignacion_familiar, v_flg_exoneracion_ret_cuarta, v_flg_miembro_comite_sst, v_flg_Adelanto, v_flg_pago_cuenta, v_flg_cheque, v_flg_liquidacion_semanal, v_flg_cta_interbancario_pago, v_flg_cta_interbancario_cts;
	if ($('#chkAfiliacionEPS').prop('checked')) v_flg_afiliacion_eps = '1'; else v_flg_afiliacion_eps = '0';
	if ($('#chkSctrPension').prop('checked')) v_flg_sctr_pension = 'S'; else v_flg_sctr_pension = 'N';
	if ($('#chkSctrSalud').prop('checked')) v_flg_sctr_salud = 'S'; else v_flg_sctr_salud = 'N';
	if ($('#chkSenati').prop('checked')) v_flg_senati = 'S'; else v_flg_senati = 'N';
	if ($('#chkVidaLey').prop('checked')) v_flg_vida_ley = 'S'; else v_flg_vida_ley = 'N';
	if ($('#chkJornadaMaxima').prop('checked')) v_flg_jornada_maxima = 'S'; else v_flg_jornada_maxima = 'N';
	if ($('#chkRegimenAlternativo').prop('checked')) v_flg_regimen_alternativo = 'S'; else v_flg_regimen_alternativo = 'N';
	if ($('#chkHoraNoct').prop('checked')) v_flg_horario_nocturno = 'S'; else v_flg_horario_nocturno = 'N';
	if ($('#chkAfiliacionEPS').prop('checked')) v_flg_HH_Auto = 'S'; else v_flg_HH_Auto = 'N';
	if ($('#chkSindicalizado').prop('checked')) v_flg_sindicato = 'S'; else v_flg_sindicato = 'N';
	if ($('#chkPrestAlimentaria').prop('checked')) v_flg_alimentos = '1'; else v_flg_alimentos = '0';
	if ($('#chkAsigMovilidad').prop('checked')) v_flg_movilidad = '1'; else v_flg_movilidad = '0';
	if ($('#chkESSALUD').prop('checked')) v_ESSALUD_VIDA = '1'; else v_ESSALUD_VIDA = '0';
	if ($('#chkAsignacionFamiliar').prop('checked')) v_flg_asignacion_familiar = 'S'; else v_flg_asignacion_familiar = 'N';
	if ($('#chkExoneracionRet').prop('checked')) v_flg_exoneracion_ret_cuarta = 'S'; else v_flg_exoneracion_ret_cuarta = 'N';
	if ($('#chkMiembroComite').prop('checked')) v_flg_miembro_comite_sst = 'S'; else v_flg_miembro_comite_sst = 'N';
	if ($('#chkAdelanto').prop('checked')) v_flg_Adelanto = 'S'; else v_flg_Adelanto = 'N';
	if ($('#chkPagoCuenta').prop('checked')) v_flg_pago_cuenta = 'S'; else v_flg_pago_cuenta = 'N';
	if ($('#chkPagoCheque').prop('checked')) v_flg_cheque = 'S'; else v_flg_cheque = 'N';
	if ($('#chkLiquiBoleta').prop('checked')) v_flg_liquidacion_semanal = 'S'; else v_flg_liquidacion_semanal = 'N';
	if ($('#chkBancoPago').prop('checked')) v_flg_cta_interbancario_pago = 'S'; else v_flg_cta_interbancario_pago = 'N';
	if ($('#chkBancoCTS').prop('checked')) v_flg_cta_interbancario_cts = 'S'; else v_flg_cta_interbancario_cts = 'N';

	if ($('#chkAdelanto').prop('checked')) {
		var montoAdelanto = $('#txtMontoAdelanto').val() == '' ? 0 : parseFloat($('#txtMontoAdelanto').val());
		var porcentajeAdelanto = $('#txtPorcentajeAdelanto').val() == '' ? 0 : parseFloat($('#txtPorcentajeAdelanto').val());
		if (montoAdelanto > 0 && porcentajeAdelanto > 0) {
			MsgBox("Solo debe de ingresar en el adelanto monto o porcentaje, no ambos.", "warning", true);
			return;
		}
	}


	var obj = {
		codPersonal: g_codPersonal,
		codAuxCont: $('#txtCodAuxCont').val(),
		usuarioAD: $('#txtUsuarioAD').val(),
		codTipoContrato: $('#selTipoContrato').val(),
		tipoTrabajador: $('#selCondicionLaboral').val(),
		flgReloj: $('#selAsistencia').val(),
		fotocheck: $('#txtFotocheck').val(),
		flgTipoAsist: $('#selTipoAsist').val(),
		horasSem: $('#txtHorasSem').val(),
		diaCorte: 1,
		//$('#lblDiaCorte').text(),
		estado: $('#txtEstado').val(),
		fechaIngreso: $('#txtIngreso').val(),
		codMotivoIngreso: $('#selMotivoIngreso').val(),
		fechaIngreso2: $('#txtFecIngreso2').val(),
		fechaCese: $('#txtCese').val(),
		fechaLiquidacion: $('#txtLiqui').val(),
		//codMotivoCese: $('#txtCodMotivoCese').val(),
		codMotivoCese: $('#txtCodMotivoCese').attr('data-cod_motivo_cese'),
		motivoCese: $('#txtMotivoCese').val(),
		flgAfiliacionEPS: v_flg_afiliacion_eps,
		codEps: $('#selAfiliacionEPS').val(),
		codAfiliadoEPS: $('#txtCodEPS').val(),
		fechaAfiliado: $('#txtFechaAfilEPS').val(),
		flgSCTRPension: v_flg_sctr_pension,
		SCTRPension: $('#selSctrPension').val() == '' ? '0' : $('#selSctrPension').val(),
		flgSCTRPensionRiesgo: $('#selSctrPensionRiesgo').val(),
		flgSCTRSalud: v_flg_sctr_salud,
		SCTRSalud: $('#selSctrSalud').val() == '' ? '0' : $('#selSctrSalud').val(),
		flgSCTRSaludRiesgo: $('#selSctrSaludRiesgo').val(),
		fechaSCTRInicio: $('#txtSctrDesde').val(),
		fechaSCTRFin: $('#txtSctrHasta').val(),
		flgSenati: v_flg_senati,
		codEstadoLaboral: $('#selEstadoLaboral').val(),
		codTipoTrabajador: $('#selTipoTrabajador').val(),
		flgVidaLey: v_flg_vida_ley,
		codVidaLey: $('#selVidaLey').val(),
		codVidaLeyAse: $('#txtCodVidaLey').val(),
		tasaVidaLey: $('#txtTasaVidaLey').val(),
		flgTipoRemu: $('#selTipoRemunVariable').val(),
		codTipoModFormativa: $('#selModFormativa').val(),
		flgSeguroPract: $('#selSeguroPract').val(),
		flgJornadaMaxima: v_flg_jornada_maxima,
		flgRegimenAlternativo: v_flg_regimen_alternativo,
		flgHorarioNocturno: v_flg_horario_nocturno,
		flgHHAuto: v_flg_HH_Auto,
		flgSindicato: v_flg_sindicato,
		flgAlimentos: v_flg_alimentos,
		flgMovilidad: v_flg_movilidad,
		ESSALUD_VIDA: v_ESSALUD_VIDA,
		flgAsignacionFamiliar: v_flg_asignacion_familiar,
		flgExoneracionRentaCuarta: v_flg_exoneracion_ret_cuarta,
		flgMiembroComiteSST: v_flg_miembro_comite_sst,
		miembtoComiteSSTDesde: $('#txtDelAsig').val(),
		miembroComiteSSTHasta: $('#txtAlAsig').val(),
		flgAdelanto: v_flg_Adelanto,
		montoAdelanto: $('#txtMontoAdelanto').val().replace(',', ''),
		porcenAdelanto: isEmpty($('#txtPorcentajeAdelanto').val()) ? '0' : $('#txtPorcentajeAdelanto').val().replace(',', ''),
		flgPagoCuenta: v_flg_pago_cuenta,
		flgCheque: v_flg_cheque,
		flgLiquidacionSemanal: v_flg_liquidacion_semanal,
		codMonedaBasico: $('#selMonedaBasico').val(),
		monto: $('#txtBasico').val().replace(',', ''),
		montoBono: $('#txtBono').val().replace(',', ''),
		tipoMontoPeriodo: $('#selPeriodoBasico').val(),
		flgCtaInterbancarioPago: v_flg_cta_interbancario_pago,
		codBancoPago: $('#selBancoPago').val(),
		codMonedaPago: $('#selMonedaPago').val(),
		codTipoCuentaPago: $('#selTipoCuentaPago').val(),
		nroCuentaPago: $('#txtNroCuenta1').val(),
		codBancoOrigenPago: $('#selBancoOrigenPago').val(),
		flgCtaInterbancarioCTS: v_flg_cta_interbancario_cts,
		codBancoCTS: $('#selBancoCTS').val(),
		codMonedaCTS: $('#selMonedaCTS').val(),
		codTipoCuentaCTS: $('#selTipoCuentaCTS').val(),
		nroCuentaCTS: $('#txtNroCuentaCTS').val(),
		codBancoOrigenCTS: $('#selBancoOrigenCTS').val(),
		FlgMarcacionWEB: UICheckbox.getValue('#chkRealizaMarcacionTDALA'),
	};
	await UILoadingDefault('show');
	await UIAjax.runFunctionAsync('Personal', 'PostDatosLaboralesLegajo', obj);
	UILoadingDefault('hide');
};

var GetListPlanillasAbiertas = function GetListPlanillasAbiertas(p_personal) {
	var rpta = -1;
	var obj = {
		codPersonal: p_personal
	};
	$.ajax({
		type: "GET",
		url: portal + '/Personal/GetPlanillasCerradas',
		data: obj,
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		async: false,
		success: function success(data) {
			var obj = data;

			if (obj.length == 1) {
				var texto = 'Periodo de cálculo abierto. No se debe hacer modificaciones puesto que el trabajador tiene la planilla abierta y podría generar inconsistencias en el histórico del periodo: \n' + obj[0].Proceso + '-' + obj[0].Clase + '-' + obj[0].anual + '-' + obj[0].mes + '-' + obj[0].numero + '. ¿Desea continuar de todas maneras?';
				if (!confirm(texto)) rpta = -1; else rpta = 1;
			} else if (obj.length > 1) {
				g_objPlanillasAbiertas = obj;
				rpta = 2;
			} else {
				rpta = 1;
			}
		},
		error: function error(response) {
			rpta = -1;
		}
	});
	return rpta;
};

var GrabarFamilares = function GrabarFamilares(accion, numFamiliar) {
	var obj = {
		accion: accion,
		codTipoEntidad: "PE",
		codEntidad: g_codEntidad,
		numFamiliar: numFamiliar,
		codParentesco: $('#selParentesco_Familiar').val(),
		codVinculo: $('#selVinculoFamiliar').val(),
		codSustentoVinculo: $('#selVinculoSustentoFam').val(),
		nroDocPaternidad: $('#txtNroDocVinculo').val(),
		apellidoP: $('#txtApellidoP_Familiar').val(),
		apellidoM: $('#txtApellidoM_Familiar').val(),
		nombres: $('#txtNombres_Familiar').val(),
		fechaAlta: $('#txtFechaAlta_Familiar').val() == '00/00/0000' ? '' : $('#txtFechaAlta_Familiar').val(),
		fechaNac: $('#txtFechaNac_Familiar').val() == '00/00/0000' ? '' : $('#txtFechaNac_Familiar').val(),
		sexo: $('#rbM_Familiar').prop('checked') ? 'M' : 'F',
		estado: $('#selEstado_Familiar').val(),
		codDocIdent: $('#selDocid_Familiar').val(),
		nroDocIdent: $('#txtNroDocId_Familiar').val(),
		codMotivoCese: $('#selMotivoBaja_Familiar').val(),
		fechaBaja: $('#txtFechaBaja_Familiar').val() == '00/00/0000' ? '' : $('#txtFechaBaja_Familiar').val(),
		codNacionalidad: $('#sel_Nacionalidad_Familiar').val(),
		nroRIHME: $('#txtNroRIHME').val(),
		flgDomiciliado: $('#chkDomiciliado_Familiar').prop('checked') ? 'S' : 'N',
		codUbigeo: $('#selDistrito_Familiar').val(),
		codVia: $('#selVia_Familiar').val(),
		direccion: $('#txtDescVia_Familiar').val(),
		numero: $('#txtNro_Familiar').val(),
		interior: $('#txtInterior_Familiar').val(),
		telefonos: $('#txtTel_Familiar').val(),
		codZona: $('#selZona_Familiar').val(),
		zona: $('#txtDescZona').val(),
		referencia: $('#txtReferencia_Familiar').val(),
		flgEps: $('#chkAfilEPS_Familiar').prop('checked') ? 'S' : 'N',
		fechaEps: $('#txtFechaAfilEPS_Familiar').val() == '00/00/0000' ? '' : $('#txtFechaAfilEPS_Familiar').val(),
		flgEstudioSuperior: $('#chkEstudiosSup').prop('checked') ? 'S' : 'N'
	};

	$.post(portal + '/Personal/PostFamiliaresLegajo', obj).done(function (data) {
		if (data.respuesta == 1) {
			ClearInput('Fa');
			GetFamiliares(g_codPersonal);
			MsgBox("success");
			rpta = 1;
		} else {
			MsgBox(data.mensaje, "warning", false);
			rpta = -1;
		}
	}).fail(function (data) {
		MsgBox("error");
		rpta = -1;
	});

};

var GrabarFormacion = function GrabarFormacion(accion, item) {
	var obj = {
		accion: accion,
		codPersonal: g_codPersonal,
		item: parseInt(item),
		codNivelAcademico: $('#selNivelAca_Formacion').val(),
		titulo: $('#txtTitulo_Formacion').val(),
		codPais: parseInt($('#selPais_Formacion').val()),
		codInstEdu: parseInt($('#rbRegPub_Formacion').prop('checked') ? 1 : 2),
		codClaseInstEdu: parseInt($('#selClase_Formacion').val()),
		codTipoInst: parseInt($('#selTipoInst_Formacion').val()),
		codInstSunat: parseInt(isEmpty($('#selInstEducativa_Formacion').val()) ? '0' : $('#selInstEducativa_Formacion').val()),
		anios: parseInt(isEmpty($('#txtAnio_Formacion').val()) ? 0 : $('#txtAnio_Formacion').val()),
		meses: parseInt(isEmpty($('#txtMeses_Formacion').val()) ? 0 : $('#txtMeses_Formacion').val()),
		dias: parseInt(isEmpty($('#txtDias_Formacion').val()) ? 0 : $('#txtDias_Formacion').val()),
		anioEgreso: parseInt(isEmpty($('#txtAnioEgreso_Formacion').val()) ? 0 : $('#txtAnioEgreso_Formacion').val()),
		fechaDesde: $('#txtDesde_Formacion').val() == '00/00/0000' ? '' : $('#txtDesde_Formacion').val(),
		fechaHasta: $('#txtHasta_Formacion').val() == '00/00/0000' ? '' : $('#txtHasta_Formacion').val(),
		codCarrera: $('#selCarrera_Formacion').val(),
		nroColegiatura: $('#txtNroColeg_Formacion').val()
	};
	$.post(portal + '/Personal/PostFormacionLegajo', obj).done(function (data) {
		if (data.respuesta == 1) {
			ClearInput('Fo');
			GetFormacion(g_codPersonal);
			MsgBox("success");
		} else {
			MsgBox("warning");
		}
	}).fail(function (data) {
		MsgBox("error");
	});
};

var EliminarFormacion = function EliminarFormacion(p_item) {
	var jsonParam = {
		codPersonal: g_codPersonal,
		item: p_item
	};
	MsgBox('question', async function () {
		await UILoadingDefault('show');
		await UIAjax.runFunctionAsync('Personal', 'DeletePersonalFormacion', jsonParam, function () {
			GetFormacion(g_codPersonal);
		});
		UILoadingDefault('hide');
	});
};

var EliminarFamiliar = async function EliminarFamiliar(p_item) {
	var jsonParam = {
		codEntidad: g_codEntidad,
		item: p_item
	};
	MsgBox('question', async function () {
		await UILoadingDefault('show');
		await UIAjax.runFunctionAsync('Personal', 'DeleteFamiliarLegajo', jsonParam, function () {
			ClearInput('Fa');
			GetFamiliares(g_codPersonal);
		});
		UILoadingDefault('hide');
	});
};

var GetListResponse = function GetListResponse(data, id) {
	var container = $(id);
	container.empty();

	if (id === '#tbodyError') {
		$.each(data, function (index, value) {
			var html = "<tr>\n                        <td class=\"text-center\">".concat(value.tipo, "</td>\n                        <td class=\"text-center\">").concat(value.fila, "</td>\n                        <td class=\"text-center\">").concat(value.texto, "</td>\n                        <td class=\"text-center\">").concat(value.dato, "</td>\n                    </tr>");
			container.append(html);
		});
	} else {
		$.each(data, function (index, value) {
			var html = "<tr>\n                        <td class=\"text-center\">".concat(value.cod_tipo_doc, "</td>\n                        <td class=\"text-center\">").concat(value.nro_doc, "</td>\n                        <td class=\"text-center\">").concat(value.ape_pat, "</td>\n                        <td class=\"text-center\">").concat(value.ape_mat, "</td>\n                        <td class=\"text-center\">").concat(value.nombres, "</td>\n                        <td class=\"text-center\">").concat(value.mensaje, "</td>\n                    </tr>");
			container.append(html);
		});
	}
}; //function GetUrl(url) {
//    return portal + "/" + url;
//};


var Buscar_new = function Buscar_new() {
	var limpiar = false;

	if ($('#txtNroDoc_consulta').val() == '') {
		MsgBox("Ingrese número de documento.", "warning", true);
		return;
	}

	var jsonData = {
		codTipoEntidad: 'PE',
		codDocid: $('#selTipoDoc_consulta').val(),
		numeroDocid: $('#txtNroDoc_consulta').val()
	};
	$.ajax({
		type: "GET",
		url: GetUrl("Personal/GetDatosGeneralesLegajo"),
		data: jsonData,
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		success: function success(data) {
			var obj = data; //g_reingreso = obj.flg_reingreso;
			if (obj.rpta < 0) {
				MsgBox(data.mensaje, "warning", false);
				return;

			}
			else if (obj.rpta == 0) {
				$('#selDepartamentoNac_new').val(obj.depa_per);
				GetProvincias('selProvinciaNac_new', obj.depa_per);
				$('#selProvinciaNac_new').val(obj.prov_per);
				GetDistritos('selDistritoNac_new', obj.prov_per);
				$('#txtCodTrab_new').val(obj.cod_trabajador);
				$('#selDepartamento_new').val(obj.depa_gen);
				GetProvincias('selProvincia_new', obj.depa_gen);
				$('#selProvincia_new').val(obj.prov_gen);
				GetDistritos('selDistrito_new', obj.prov_gen);
				MsgBox(data.mensaje, "warning", false);
			}
			else {
				//limpiar controles
				$('#selDepartamentoNac_new').val('14');
				GetProvincias('selProvinciaNac_new', '14');
				$('#selProvinciaNac_new').val('1401');
				GetDistritos('selDistritoNac_new', '1401');
				$('#selDepartamento_new').val('14');
				GetProvincias('selProvincia_new', '14');
				$('#selProvincia_new').val('1401');
				GetDistritos('selDistrito_new', '1401');
				$("#tbPer input[type=text], #tbLab input[type=text]").val('');
				MsgBox(data.mensaje, "warning", false);
			}
		},
		error: function error(response) {
			console.log(response);
		}
	});
};

var validarFechaAfiliacion = function validarFechaAfiliacion(fecha) {
	var fechaMax = convertDateFormat2('01/02/2013');
	var fechaM = new Date(fechaMax)
	edad = Math.floor(((fechaM - fecha) / (1000 * 60 * 60 * 24) / 365));
	if (fecha >= fechaM) {
		return true;
	}
	else {
		return false;
	}
};

var ValidaDatosVacios = function ValidaDatosVacios() {
	var resultado = "";

	if ($('#txtNumDoc_new').val() != '') {
		resultado = 'N';
	}

	if ($('#txtRuc_new').val() != '') {
		resultado = 'N';
	}

	if ($('#txtNumBrevete_new').val() != '') {
		resultado = 'N';
	}

	if ($('#txtApellidoP_new').val() != '') {
		resultado = 'N';
	}

	if ($('#txtApellidoM_new').val() != '') {
		resultado = 'N';
	}

	if ($('#txtNombres_new').val() != '') {
		resultado = 'N';
	}

	if ($('#txtCUSPP_new').val() != '') {
		resultado = 'N';
	}

	if ($('#txtVia_new').val() != '') {
		resultado = 'N';
	}

	if ($('#txtNro_new').val() != '') {
		resultado = 'N';
	}

	if ($('#txtInterior_new').val() != '') {
		resultado = 'N';
	}

	if ($('#txtZona_new').val() != '') {
		resultado = 'N';
	}

	return resultado;
};

var GrabarPersonal = function GrabarPersonal() {
	var sexo = "";
	if ($('#rb_Masculino_new').prop('checked')) sexo = "1"; else if ($('#rb_Femenino_new').prop('checked')) sexo = "2"; else {
		MsgBox("Seleccione el sexo del trabajador.", "warning", true);
		return;
	}
	var tipoComision = "";
	if ($('#selAFP_new').val() != null) {
		if ($("#selAFP_new option:selected").text().indexOf('AFP') > -1) {
			if ($('#rbComisionRemu_new').prop('checked')) tipoComision = "1"; else if ($('#rbComisionMixta_new').prop('checked')) tipoComision = "2"; else {
				MsgBox("Seleccione el tipo de comisión AFP.", "warning", true);
				return;
			}
		}
	}

	var pagoCuenta = "";
	if ($('#rbPagoCuenta_new').prop('checked')) pagoCuenta = "S"; else if ($('#rbPagoCheque_new').prop('checked')) pagoCuenta = "N"; else {
		MsgBox("Seleccione el tipo de pago.", "warning", true);
		return;
	}
	var boletaRecibo = "";
	if ($('#rbPagoBoleta_new').prop('checked')) boletaRecibo = "B"; else if ($('#rbPagoRecibo_new').prop('checked')) boletaRecibo = "R"; else {
		MsgBox("Seleccione boleta o recibo.", "warning", true);
		return;
	}
	var fechaNac = convertDateFormat2($('#txtFechaNac_new').val());
	var fechaNace = new Date(fechaNac);
	var fechaActual = new Date();
	var mes = fechaActual.getMonth();
	var dia = fechaActual.getDate();
	var año = fechaActual.getFullYear();
	fechaActual.setDate(dia);
	fechaActual.setMonth(mes);
	fechaActual.setFullYear(año);
	edad = Math.floor((fechaActual - fechaNace) / (1000 * 60 * 60 * 24) / 365);

	if (edad < 18) {
		MsgBox("No se puede ingresar a un menor de edad, el trabajador tiene " + edad + " años de edad.", "warning", false);
		return;
	}
	if ($('#chkAdelanto_new').prop('checked')) {
		var montoAdelanto = $('#txtMontoAdelanto_new').val() == '' ? 0 : parseFloat($('#txtMontoAdelanto_new').val());
		var porcentajeAdelanto = $('#txtPorcenAdelanto_new').val() == '' ? 0 : parseFloat($('#txtPorcenAdelanto_new').val());
		if (montoAdelanto > 0 && porcentajeAdelanto > 0) {
			MsgBox("Solo debe de ingresar en el adelanto monto o porcentaje, no ambos.", "warning", false);
			return;
		}
	}
	var jsonData = {
		codTipoPlanilla: $('#selTipoPlanilla_new').val(),
		codLocalidad: $('#selLocalidad_new').val(),
		codProyecto: $('#selProyecto_new').val(),
		codUnidadNegocio: $('#selUN_new').val(),
		codCentroCosto: $('#selCC_new').val(),
		codArea: $('#selArea_new').val(),
		codTipoDoc: $('#selTipoDoc_new').val(),
		nroDoc: $('#txtNumDoc_new').val(),
		ruc: $('#txtRuc_new').val(),
		codBrevete: $('#selBrevete_new').val(),
		nroBrevete: $('#txtNumBrevete_new').val(),
		apellidoPat: $('#txtApellidoP_new').val(),
		apellidoMat: $('#txtApellidoM_new').val(),
		nombres: $('#txtNombres_new').val(),
		fechaNacimiento: $('#txtFechaNac_new').val(),
		codUbigeoNac: $('#selDistritoNac_new').val(),
		codNacionalidad: $('#selPais_new').val(),
		codEstadoCivil: $('#selEstadoCivil_new').val(),
		nroHijos: $('#txtNroHijos_new').val() == '' ? 0 : $('#txtNroHijos_new').val(),
		codSexo: sexo,
		codAFP: $('#selAFP_new').val(),
		cuspp: $('#txtCUSPP_new').val(),
		fechaAfiliacion: $('#txtFechaAfil_new').val(),
		csse: $('#txtCSSE_new').val(),
		tipoComisionAFP: tipoComision,
		codNivelAcademico: $('#selNivelAcademico_new').val(),
		codVia: $('#selVia_new').val(),
		nombreVia: $('#txtVia_new').val(),
		nroVia: $('#txtNro_new').val(),
		interiorVia: $('#txtInterior_new').val(),
		codZona: $('#selZona_new').val(),
		nombreZona: $('#txtZona_new').val(),
		codUbigeo: $('#selDepartamento_new').val() != '-1' ? $('#selDistrito_new').val() : null,
		referencia: $('#txtReferencia_new').val(),
		emailLaboral: $('#txtEmailLab_new').val(),
		codTelefono: $('#txtCodTelefono').val(),
		telefono: $('#telefono1').val(),
		telefono2: $('#telefono2').val(),
		telefono3: $('#telefono3').val(),
		//selTipoPlanilla_new: $('#txtFax').val(),
		emailPersonal: $('#txtEmailPer_new').val(),
		codCategoria: $('#selCategoria_new').val(),
		codTrabajador: $('#txtCodTrab_new').val(),
		fotocheck: $('#txtFotocheck_new').val(),
		codCargo: $('#selCargo_new').val(),
		codTipoTrabajador: $('#selTipoTrab_new').val(),
		tipoTrabajador: $('#selClase_new').val(),
		codAlcance: $('#selAlcanceCargo_new').val(),
		flgAfiliacionEPS: $('#chkAfilEPS_new').prop('checked') ? 'S' : 'N',
		codEPS: $('#selAfilEPS_new').val(),
		flgSCTRPension: $('#chkSCTRPension_new').prop('checked') ? 'S' : 'N',
		SCTRPension: $('#selSCTRPension_new').val(),
		flgSCTRSalud: $('#chkSCTRSalud_new').prop('checked') ? 'S' : 'N',
		SCTRSalud: $('#selSCTRSalud_new').val(),
		flgAsignacionFamiliar: $('#chkAsigFam_new').prop('checked') ? 'S' : 'N',
		flgEssaludVida: $('#chkEsSaludVida_new').prop('checked') ? 'S' : 'N',
		flgSenati: $('#chkSenati_new').prop('checked') ? 'S' : 'N',
		codMonedaBasico: $('#selMonedaBasico_new').val(),
		montoBasico: $('#txtMontoBasico_new').val() == '' ? 0 : $('#txtMontoBasico_new').val().replace(',', ''),
		tipoMontoPeriodo: $('#selRegistroLaboral_new').val(),
		flgVidaLey: $('#chkVidaLey_new').prop('checked') ? 'S' : 'N',
		codVidaLey: $('#selSeguro_new').val(),
		tasaVidaLey: $('#txtTasa_new').val() == '' ? 0 : $('#txtTasa_new').val().replace(',', ''),
		//$('#txtTasa_new').val(),
		codHorario: $('#selHorarioLuVi_new').val(),
		fechaIngreso: $('#txtFechaIngreso_new').val(),
		codMotivoIngreso: $('#selMotivoIngreso_new').val(),
		codTipoContrato: $('#selTipoContrato_new').val(),
		flgAdelanto: $('#chkAdelanto_new').prop('checked') ? 'S' : 'N',
		montoAdelanto: $('#txtMontoAdelanto_new').val() == '' ? 0 : $('#txtMontoAdelanto_new').val(),
		//$('#txtMontoAdelanto_new').val(),
		porcentajeAdelanto: $('#txtPorcenAdelanto_new').val() == '' ? 0 : $('#txtPorcenAdelanto_new').val(),
		//$('#txtPorcenAdelanto_new').val(),
		flgPagoCuenta: pagoCuenta,
		flgCtaInterbancarioPago: $('#chkPagoInter_new').prop('checked') ? 'S' : 'N',
		codBancoPago: $('#selBancoPago_new').val(),
		codMonedaPago: $('#selMonedaPago_new').val(),
		nroCuentaPago: $('#txtNumCuentaPago_new').val(),
		codBancoOrigenPago: $('#selBancoOrigenPago_new').val(),
		flgBoletaReciboH: boletaRecibo,
		flgCtaInterbancarioCTS: $('#chkCTSInter_new').prop('checked') ? 'S' : 'N',
		codBancoCTS: $('#selBancoCTS_new').val(),
		codMonedaCTS: $('#selMonedaCTS_new').val(),
		nroCuentaCTS: $('#txtNumCuentaCTS_new').val(),
		codBancoOrigenCTS: $('#selBancoOrigenCTS_new').val(),
		finContrato: $('#txtFinContrato').val(),
		inicioPeriodoPruebas: $('#txtInicioPeriodoPrueba').val(),
		finPeriodoPruebas: $('#txtFinPeriodoPrueba').val(),
		grupoSanguineo: $('#selGrupoSanguineo_new').val(),
		polo: $('#selTallaPolo_new').val(),
		camisa: $('#selTallaCamisa_new').val(),
		alergias: $('#txtAlergias_new').val(),
		pantalon: $('#selPantalon_new').val(),
		casaca: $('#selCasaca_new').val(),
		zapato: $('#selTallaZapato_new').val(),
		zapatilla: $('#selTallaZapatilla_new').val(),
		flgReingreso: g_reingreso,
		flgDomiciliado: UICheckbox.getValue('#chkDomiciliado_new'),
	};
	$.post(portal + '/Personal/PostNewPersonal', jsonData).done(function (data) {

		if (data.respuesta > 0) {
			MsgBox("success");
			g_reingreso = 'N';
			GetColaboradores();
			$('#modalNuevoPersonal').modal('hide');
		} else {
			if (data.table1.length > 0) { //if (data.table1.length <= 0) {
				MsgBox(data.table1[0].mensaje, "warning", false);
			} else {
				var container = $("#tbodyErrores");
				container.empty();
				var html = '';
				$.each(data.table1, function (index, value) {
					//html += "<li class=\"list-group-item list-group-item-danger\">"
					//    + "${value.sec} .- ${value.mensaje} - ${value.dato_importado}"
					//    + "</li>";
				});
				new PNotify({
					title: 'Errores',
					text: '<ul class="list-group list-unstyled">' + html + '</ul>',
					type: 'error'
				});
			}
		}
	}).fail(function (data) {
		MsgBox("error");
	});
};

var GetDetalleBanco = function GetDetalleBanco(row) {
	var data = $(row).data('json');
	$('#selBancoPago').val(data.cod_Banco);
	$('#selMonedaPago').val(data.cod_Moneda);
	$('#selTipoCuentaPago').val(data.tipo_cuenta);
	$('#txtNroCuenta1').val(data.numero_cuenta);
	$('#modalHistorialBancos').modal('hide');
};

function JsonToConcat(jsonParam) {
	//var result = jQuery.param(jsonParam);
	//return result;

	//var result = new URLSearchParams(jsonParam).toString();
	//return result;

	var result = "";
	$.each(jsonParam, function (name, value) {
		result += name + '=' + AllTrim(value) + '&';
	});
	result += '|!';
	result = result.replace('&|!', '');
	return result;
};

function AllTrim(cadena) {
	var result = String(cadena).replace(/^\s+/g, '');
	result = result.replace(/\s+$/g, '');
	result = $.trim(result);
	return result;
};

function getDate(frmt) {
	if (isEmpty(frmt)) {
		frmt = 'yyyy-mm-dd';
	}
	var now = new Date();
	var result = shortDate(now, frmt);
	return result;
};

function addDate(date, type, add) {
	//var result = new Date(Number(date));
	var result = new Date(date);
	switch (type) {
		case 'd':
			//var copy = new Date(Number(date));
			//copy.setDate(date.getDate() + add);
			//result = copy;
			result.setDate(date.getDate() + add);
			break;
		case 'm':
			//var copy = new Date(Number(date));
			//copy.setMonth(date.getMonth() + add);
			//result = copy;
			result.setMonth(date.getMonth() + add);
			break;
		case 'y':
			result.setFullYear(date.getFullYear() + add);
			break;
		default:
			result = date
			break;
	}
	return result;
};

function shortDate(date, frmt) {
	if (isEmpty(frmt)) {
		frmt = 'yyyy-mm-dd';
	}
	var now = new Date(date);
	var month = (now.getMonth() + 1);
	var day = now.getDate();
	if (month < 10) { month = "0" + month; }
	if (day < 10) { day = "0" + day; }
	var result = "";
	switch (frmt) {
		case 'dd/mm/yyyy':
			//result = `${day}/${month}/${now.getFullYear()}`;
			result = day + '/' + month + '/' + now.getFullYear();
			break;
		case 'dd-mm-yyyy':
			//result = `${day}-${month}-${now.getFullYear()}`;
			result = day + '-' + month + '-' + now.getFullYear();
			break;
		case 'yyyy/mm/dd':
			//result = `${now.getFullYear()}/${month}/${day}`
			result = now.getFullYear() + '/' + month + '/' + day;
			break;
		case 'yyyy-mm-dd':
			//result = `${now.getFullYear()}-${month}-${day}`
			result = now.getFullYear() + '-' + month + '-' + day;
			break;
		//case 'mm/dd/yyyy':
		//    result = month + '/' + day + '/' +now.getFullYear();
		//    break;
		//case 'mm-dd-yyyy':
		//    result = month + '-' + day + '-' + now.getFullYear();
		//    break;
	}
	//result = `${day}/${month}/${now.getFullYear()}`;
	//result = now.getFullYear() + '-' + month + '-' + day;
	return result;
};


//GetFirstDayMonth
//*******************************************************************************************************

//*******************************************************************************************************
//GetFirstDayMonth
//Fecha: 2020-02-20
//Autor: Franco Isla
//Descripcion: Recupera el Primer día del mes
function GetFirstDayMonth(month) {
	var now = new Date();
	if (isEmpty(month) || isNaN(month)) {
		month = now.getMonth();
		alert("cae dat");
	}
	var prevMonthLastDate = new Date(now.getFullYear(), month, 0);
	var prevMonthFirstDate = new Date(now.getFullYear() - (month > 0 ? 0 : 1), (month - 1 + 12) % 12, 1);

	var formatDateComponent = function (dateComponent) {
		return (dateComponent < 10 ? '0' : '') + dateComponent;
	};

	var formatDate = function (date) {
		return formatDateComponent(date.getMonth() + 1) + '/' + formatDateComponent(date.getDate()) + '/' + date.getFullYear();
	};

	return formatDate(prevMonthFirstDate);
}

//Date.prototype.addDays = function (days) {
//    //var date = new Date(this.valueOf());
//    //date.setDate(date.getDate() + days);
//    //return date;
//    var date = new Date(this.valueOf());
//    //date.setMonth()
//    date.setDate(date.getDate() + days);
//    //this.setDate(date.getDate());
//    return date;
//}


//Date.prototype.addMonths = function (months) {
//    var date = new Date(this.valueOf());
//    date.setMonth(date.getMonth() + months);
//    return date;
//    //this.setMonth(this.getMonth() + months);
//    //return this;
//}
//Date.prototype.addYears = function (years) {
//    var date = new Date(this.valueOf());
//    date.setFullYear(date.getFullYear() + years);
//    return date;
//    //this.setFullYear(this.getFullYear() + years);
//    //return this;
//}

//Date.isLeapYear = function (year) {
//    return (((year % 4 === 0) && (year % 100 !== 0)) || (year % 400 === 0));
//};

//Date.getDaysInMonth = function (year, month) {
//    return [31, (Date.isLeapYear(year) ? 29 : 28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][month];
//};

//Date.prototype.isLeapYear = function () {
//    return Date.isLeapYear(this.getFullYear());
//};

//Date.prototype.getDaysInMonth = function () {
//    return Date.getDaysInMonth(this.getFullYear(), this.getMonth());
//};

//Date.prototype.addDays = function (value) {
//    var n = this.getDate();
//    this.setDate(1);
//    //this.setMonth(this.getMonth() + value);
//    this.setDate(Math.min(n, this.getDaysInMonth()) + value);
//    return this;
//};

//Date.prototype.addMonths = function (value) {
//    var n = this.getDate();
//    this.setDate(1);
//    this.setMonth(this.getMonth() + value);
//    this.setDate(Math.min(n, this.getDaysInMonth()));
//    return this;
//};

//Date.prototype.shortDate = function (frmt = 'yyyy-mm-dd') {
//    //var result = shortDate(this, frmt);
//    return shortDate(this.valueOf(), frmt);
//}

var GetUrl = function GetUrl(url) {
	let _portal = portal;
	if (portal.substring(portal.length - 1) == '/') _portal = portal.slice(0, portal.length - 1)
	if (url.substring(0, 1) == '/') url = url.slice(1, url.length)

	return _portal + "/" + url;
};

//*******************************************************************************************************
//parseIsoDate
//Fecha: 2020-02-07
//Autor: Chris Isla
//Descripcion: esta funcion convierte de una "cadena fecha": 2020-01-01T00:00:00 a un objeto Date
function parseIsoDate(s) {
	var isoDateRegex = /^(\d\d\d\d)-(\d\d)-(\d\d)T(\d\d):(\d\d):(\d\d)(\.\d\d?\d?)?([\+-]\d\d:\d\d|Z)?$/;

	var m = isoDateRegex.exec(s);

	if (m == null)
		return null;

	// Is this UTC, offset, or undefined? Treat undefined as UTC.
	if (m.length == 7 ||                // Just the y-m-dTh:m:s, no ms, no tz offset - assume UTC
		(m.length > 7 && (
			!m[7] ||                    // Array came back length 9 with undefined for 7 and 8
			m[7].charAt(0) != '.' ||    // ms portion, no tz offset, or no ms portion, Z
			!m[8] ||                    // ms portion, no tz offset
			m[8] == 'Z'))) {            // ms portion and Z
		// JavaScript's weirdo date handling expects just the months to be 0-based, as in 0-11, not 1-12 - the rest are as you expect in dates.
		//var d = new Date(Date.UTC(m[1], m[2]-1, m[3], m[4], m[5], m[6]));
		var d = new Date(m[1], m[2] - 1, m[3], m[4], m[5], m[6]);
	}
	else {
		// local
		var d = new Date(m[1], m[2] - 1, m[3], m[4], m[5], m[6]);
	}

	return d;
}
//parseIsoDate
//*******************************************************************************************************

//*******************************************************************************************************
//formatDateDMY
//Fecha: 2020-02-07
//Autor: Chris Isla
//Descripcion: formatea un objeto Date en una cadena DD/MM/YYYY
function formatDateDMY(date) {

	if (date == null)
		return "";

	var dia = "" + date.getDate();
	var mes = "" + (date.getMonth() + 1);

	if (dia.length == 1)
		dia = "0" + dia;

	if (mes.length == 1)
		mes = "0" + mes;

	return dia + "/" + mes + "/" + date.getFullYear();
}
//formatDateDMY
//*******************************************************************************************************


//*******************************************************************************************************
//Definiciones & Constantes
//Fecha: 2020-02-11
//Autor: Chris Isla

var Dias = ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"];
var Meses = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];

//Definiciones & Constantes
//*******************************************************************************************************






//*******************************************************************************************************
//formatDateDMY
//Fecha: 2020-02-11
//Autor: Chris Isla
//Descripcion: zeroPad(5, 2); --> "05"
function zeroPad(num, places) {
	var zero = places - num.toString().length + 1;
	return Array(+(zero > 0 && zero)).join("0") + num;
}
//formatDateDMY
//*******************************************************************************************************


function NumericToString(num, dec) {
	num = toInt(num);
	return num.toFixed(dec);
}

function CheckString(str) {
	if (isEmpty(str) == true)
		return "";
	return str;
}


function isAlphaNumeric(str) {
	var code, i, len;

	for (i = 0, len = str.length; i < len; i++) {
		code = str.charCodeAt(i);
		if (!(code > 47 && code < 58) && // numeric (0-9)
			!(code > 64 && code < 91) && // upper alpha (A-Z)
			!(code > 96 && code < 123)) { // lower alpha (a-z)
			return false;
		}
	}
	return true;
};

function isNumeric(str) {
	var code, i, len;

	for (i = 0, len = str.length; i < len; i++) {
		code = str.charCodeAt(i);
		if (!((code > 47 && code < 58) || code == '.')) // numeric (0-9)
		{
			return false;
		}
	}
	return true;
};


//*******************************************************************************************************
//formatNumber
//Fecha: 2020-02-24
//Autor: Chris Isla
//Descripcion: formatNumber(5000.3) ==> 5,000.30
function formatNumber(num) {
	if (num == null)
		return "";

	return num.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
}
//formatNumber
//*******************************************************************************************************


var savePeriodo = function savePeriodo(periodo) {
	localStorage.setItem("periodo", periodo);
};
var getPeriodo = function getPeriodo() {
	return localStorage.getItem("periodo");
};
var removePeriodo = function removePeriodo() {
	localStorage.removeItem("periodo");
};

var saveMinPeriodo = function saveMinPeriodo(periodo) {
	localStorage.setItem("min_periodo", periodo);
};
var getMinPeriodo = function getMinPeriodo() {
	return localStorage.getItem("min_periodo");
};
var removeMinPeriodo = function removeMinPeriodo() {
	localStorage.removeItem("min_periodo");
};

var clearLocales = function clearLocales() {
	localStorage.clear();
};

var dateAddDays = function dateAddDays(d, a) {
	a = parseInt(a);
	_n = new Date(d.setDate(d.getDate() + a)).toLocaleDateString();
	return _n;
};

var dateAddMonths = function dateAddMonths(d, a) {
	a = parseInt(a);
	//return formatDateDMY(new Date(d.setMonth(d.getMonth() + a)));
	_n = new Date(d.setMonth(d.getMonth() + a));//.toLocaleDateString();
	_2 = new Date(_n.setDate(_n.getDate() - 1)).toLocaleDateString();
	return _2;
};

var transformDateSql = function transformDateSql(s) {
	_s = s.split('/');
	return _s[2] + '' + zeroPad(_s[1], 2) + '' + zeroPad(_s[0], 2);
}

var setValToField = function setValToField(t, o, d, v) {
	_d = $('#' + d).val();
	_p = $('#' + o).val().split("/");
	_o = new Date(_p[1] + '-' + _p[0] + '-' + _p[2]);
	switch (_d) {
		case '01':
			$('#' + t).val(dateAddDays(new Date(_o), parseInt(v)));
			break;
		case '02':
			$('#' + t).val(dateAddMonths(new Date(_o), parseInt(v)));
			break;
	}
};

var NumberToFormat = function NumberToFormat(Number, Digits) {
	var result;
	if (isEmpty(Digits)) {
		Digits = 2;
	}
	result = Number.toLocaleString('es-PE', { minimumIntegerDigits: Digits, useGrouping: false });
	return result;
};

var ActionsReactionsLoadRegex = function ActionsReactionsLoadRegex() {
	$("input[regexp]").each(function () {
		// por cada elemento encontrado setea un listener del keypress
		$(this).keypress(function (event) {
			var p_option = $(this).attr("regexp");
			switch (p_option) {
				case 'Numeric':
					patron = "^[0-9]+$";
					break;
				case 'Alfanumeric':
					patron = "^[a-zA-Z0-9]+$";
					break;
				case 'FullTextAcentuado':
					patron = "^[a-zA-ZáéíóúàèìòùÀÈÌÒÙÁÉÍÓÚñÑüÜ/\\s]+$";
					break;
				case 'email':
					patron = "";
					break;
				default:
					break;
			}

			//var exclude = "31|27"; /*|Escape*/           


			if (event.which <= 31)
				return 0;
			// extrae la cadena que define la expresión regular y creo un objeto RegExp
			//var regexp = new RegExp("^" + $(this).attr("regexp") + "$", "g");
			var regexp = new RegExp(patron, "g");
			// evalua si el contenido del campo se ajusta al patrón REGEXP
			if (!regexp.test($(this).val() + String.fromCharCode(event.which)))
				event.preventDefault();
		});
	});

};

var ValidateTipoDocPersonal = function ValidateTipoDocPersonal(idSelectTipo, idInputDocumento, tipo) {
	var p_option = $('#' + idSelectTipo).val();
	var maxLength = 0;
	var minLength = 0;
	var patron = '';

	switch (p_option) {
		case '01':
			maxLength = 8;
			minLength = 8;
			patron = "Numeric";
			break;
		case '02':
			maxLength = 15;
			minLength = 1;
			patron = "Alfanumeric";
			break;
		case '03':
			maxLength = 15;
			minLength = 1;
			patron = "Alfanumeric";
			break;

		case '04':
			maxLength = 12;
			minLength = 1;
			patron = "Alfanumeric";
			break;
		case '05':
			maxLength = 15;
			minLength = 1;
			patron = "Alfanumeric";
			break;
		case '07':
			maxLength = 12;
			minLength = 1;
			patron = "Alfanumeric";
			break;
		case '08':
			maxLength = 15;
			minLength = 1;
			patron = "Alfanumeric";
			break;

		case '10':
			maxLength = 15;
			minLength = 1;
			patron = "Alfanumeric";
			break;
		case '13':
			maxLength = 8;
			minLength = 8;
			patron = "numeric";
			break;
		default:
			break;
	}
	if (maxLength > 0) {
		//AsignaAtributoValida(idInputDocumento, maxLength, minLength, tipo, patron);
		if (tipo == 1 || tipo == 3) {
			$('#' + idInputDocumento).attr('maxlength', maxLength);
			if (tipo == 1) {
				$('#' + idInputDocumento).val("").focus();
			} else {
				$('#' + idInputDocumento).val($('#' + idInputDocumento).val().trim()).focus();
			}
			$('#' + idInputDocumento).attr('regexp', patron);
		} else {
			if (tipo == 2) {
				var valorInput = $('#' + idInputDocumento).val().trim().length;
				if (valorInput < minLength) {
					MsgBox('Ingreso minimo ' + minLength + ' digitos.', "warning", true);
					return 1;
				}
			}
		}
	}

}

var AsignaAtributoValida = function AsignaAtributoValida(idInputDocumento, maxLength, minLength, tipo, patronValida) {
	if (tipo == 1) {
		$('#' + idInputDocumento).attr('maxlength', maxLength).focus();
		$('#' + idInputDocumento).attr('regexp', patronValida);
	} else {
		if (tipo == 2) {
			var valorInput = $('#' + idInputDocumento).val().trim().length;
			if (valorInput < minLength) {
				MsgBox('Ingreso minimo ' + minLength + ' digitos.', "warning", true);
				return 1;
			}
		}
	}

}

var ValidateTipoDato = function ValidateTipoDato(idSelectTipo, idInputDocumento, keyCode) {
	var p_option = $('#' + idSelectTipo).val();
	var valueDocumento = $('#' + idInputDocumento).val();

}

var ActionsReactionsLoadRegex = function ActionsReactionsLoadRegex() {

	$("input[regexp]").each(function () {
		// por cada elemento encontrado setea un listener del keypress
		$(this).on('keypress', function (event) {

			p_option = $(this).attr('regexp');
			patron = "";
			switch (p_option) {
				case 'Numeric':
					patron = "^[0-9]+$";
					break;
				case 'Alfanumeric':
					patron = "^[a-zA-Z0-9]+$";
					break;
				case 'FullTextAcentuado':
					patron = "^[a-zA-ZáéíóúàèìòùÀÈÌÒÙÁÉÍÓÚñÑüÜ/\\s]+$";
					break;
				case 'Numeric2':
					patron = "^[1-9][0-9]*$"; //Numeros sin 0 a la izquierda Ejemplo 01
					break;
				case 'CuentaB':
					patron = "^[0-9\-]+$";
					break;
				default:
					break;
			}
			if (event.which <= 31)
				return 0;

			// extrae la cadena que define la expresión regular y creo un objeto RegExp
			//var regexp = new RegExp("^" + $(this).attr("regexp") + "$", "g");
			var regexp = new RegExp(patron, "g");
			// evalua si el contenido del campo se ajusta al patrón REGEXP
			if (!regexp.test($(this).val() + String.fromCharCode(event.which)))
				event.preventDefault();

		});
	});

}


var ValidateFechaEdad = function ValidateFechaEdad(InputFecha) {
	var fechaNac = convertDateFormat2($('#' + InputFecha).val());
	var fechaNace = new Date(fechaNac);
	var fechaActual = new Date();
	var mes = fechaActual.getMonth();
	var dia = fechaActual.getDate();
	var año = fechaActual.getFullYear();
	fechaActual.setDate(dia);
	fechaActual.setMonth(mes);
	fechaActual.setFullYear(año);
	edad = Math.floor((fechaActual - fechaNace) / (1000 * 60 * 60 * 24) / 365);


	return edad;
}

var ShowToast = function ShowToast(jsonParam) {

	var jsonDefault = {
		title: 'Buen trabajo!',
		message: 'Acción realizada con éxito',
		type: "success",
		timer: 2500
	};

	$.extend(true, jsonDefault, jsonParam);

	var title = jsonDefault.title;

	var message = jsonDefault.message;

	var type = jsonDefault.type;

	var timer = jsonDefault.timer;

	var divNotification = $('#divNotification');
	var icon = '<i class="fal fa-check-circle f-58 text-success"></i>';
	switch (type) {
		case 'warning': case 'info':
			icon = '<i class="fal fa-exclamation-circle f-58 text-' + type + '"></i>';
			break;
		case 'danger':
			icon = '<i class="fal fa-ban f-58 text-danger"></i>';
			break;
	}
	var timeId = new Date().getTime();
	var html = '<div id = "toast-' + timeId + '" class="customToast rounded alert-' + type + '" role="alert" aria-live="assertive" data-animation="true" data-delay="' + timer + '" aria-atomic="true" >' +
		'<div class="container-fluid py-2 us-n">' +
		'<div class="row">' +
		'<div class="col-md-4 d-flex justify-content-center">' +
		icon +
		'</div>' +
		'<div class="col-md-8 pl-0 m-auto">' +
		'<div class="row">' +
		'<div class="col-auto">' +
		'<span class="h5">' + title + '</span>' +
		'</div>' +
		'</div>' +
		'<div class="row">' +
		'<div class="col-12">' +
		'<span>' + message + '</span>' +
		'</div>' +
		'</div>' +
		'</div>' +
		'</div>' +
		'</div>' +
		'</div >';
	divNotification.append(html);
	//var containerBody = $('body');
	//containerBody.append(html);
	$('#toast-' + timeId).toast('show');
	setInterval(function () {
		$('#toast-' + timeId).remove();
	}, timer);
	//$('#toast-123').toast('show');
};

function cleanRows(name, ths) {
	$('#' + name + ' tr').removeClass('selected');
	ths.addClass('selected');
};

//<div class="toast" role="alert" aria-live="assertive" aria-atomic="true">
//    <div class="toast-header">
//        <img src="..." class="rounded mr-2" alt="...">
//            <strong class="mr-auto">Bootstrap</strong>
//            <small class="text-muted">just now</small>
//            <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
//                <span aria-hidden="true">&times;</span>
//            </button>
//      </div>
//        <div class="toast-body">
//            See? Just like this.
//      </div>
//    </div>

function CheckInteger(e) {
	var keyCode = (e.keyCode ? e.keyCode : e.which);
	if (keyCode > 47 && keyCode < 58) {
		return true;
	}

	return false;
}


function isNumberKey(evt, obj) {

	var charCode = (evt.which) ? evt.which : event.keyCode
	var value = obj.value;
	var dotcontains = value.indexOf(".") != -1;
	if (dotcontains)
		if (charCode == 46) return false;
	if (charCode == 46) return true;
	if (charCode > 31 && (charCode < 48 || charCode > 57))
		return false;
	return true;
}