﻿/**
 * Tables & Datables
 * Asistencia Diaria
 **/

var AsistenciaDiariaColaboradores;
var TableAsistenciaDiariaColaboradorPeriodo = $('#AsistenciaDiariaColaboradorPeriodo');
var AsistenciaDiariaColaboradorPeriodo;
var TableAsistenciaDiariaColaboradorTotales = $('#AsistenciaDiariaColaboradorTotales');
var AsistenciaDiariaColaboradorTotales;
var TableAsistenciaDiariaColaboradorMarcas = $('#AsistenciaDiariaColaboradorMarcas');
var AsistenciaDiariaColaboradorMarcas;

var tableColaboradoresFecha;

var tableColaboradorTotalesFecha;
var tableColaboradorTotalesRefrigerio;

var tableColadoradorGMMM;

var tableColadoradorIM;
var tableProcesoIM;
/**
 * Vars
 * Asistencia Diaria
 **/

var jsonParamDefault = {
	AsiDesde: '',
	AsiHasta: '',
	CodTipoPlanilla: $('#AsistenciaDiariaSelectTipoPlanilla').val(),
	CodProyecto: $('#AsistenciaDiariaSelectProyecto').val(),
	CodCentroCosto: $('#AsistenciaDiariaSelectCentroCosto').val(),
	CodLocalidad: $('#AsistenciaDiariaSelectLocalidad').val(),
	CodUnidadNegocio: $('#AsistenciaDiariaSelectUnidadNegocio').val(),
	CodArea: $('#AsistenciaDiariaSelectArea').val(),
	Estado: '%',
	CodAnual: getPeriodo(),
	FlgSoloAutorizador: MenuAcceso,
	FlgClaseProceso: 'B',
	FlgProceso: 'P'
};

var selectPeriodo = $('#selectPeriodo');

$("html, body").animate({ scrollTop: 0 }, 600);

$(document).ready(function () {

	UIMask.Time('#txtHoraAM, #txtHoraGMMM');

	AsistenciaDiariaColaboradores = $('#AsistenciaDiariaColaboradores').DataTable({
		searching: false,
		paging: false,
		info: false,
		fixedHeader: true
	});

	AsistenciaDiariaColaboradorPeriodo = TableAsistenciaDiariaColaboradorPeriodo.DataTable({
		searching: false,
		paging: false,
		info: false,
		fixedHeader: true
	});
	AsistenciaDiariaColaboradorTotales = TableAsistenciaDiariaColaboradorTotales.DataTable({
		searching: false,
		paging: false,
		info: false,
		fixedHeader: true
	});
	AsistenciaDiariaColaboradorMarcas = TableAsistenciaDiariaColaboradorMarcas.DataTable({
		searching: false,
		paging: false,
		info: false,
		fixedHeader: true
	});

	AsistenciaDiariaLoadPeriodos();

	$('#AsistenciaDiariaSelectTipoPlanilla').on('change', function (e) {
		e.preventDefault();
		AsistenciaDiariaLoadPeriodos();
	})

	//$(document).on('change', `select.selChangeAD`, LoadAsistenciaDiariaColaboradores);

	$(document).on('change', `#AsistenciaDiariaSelectProyecto, #AsistenciaDiariaSelectCentroCosto, #AsistenciaDiariaSelectUnidadNegocio,
				#AsistenciaDiariaSelectArea, #AsistenciaDiariaSelectLocalidad, #AsistenciaDiariaSelectLocalidad`, function (e) {
		LoadAsistenciaDiariaColaboradores();
	});

	AsistenciaDiariaColaboradores.on('click', 'tbody tr', function (e) {
		e.preventDefault();
		var ths = $(this);
		AsistenciaDiariaColaboradores.rows().$('tr.selected').removeClass('selected');
		ths.addClass('selected');
		var jsonRow = AsistenciaDiariaColaboradores.row(ths).data();
		jsonParamDefault.CodPersonal = jsonRow.cod_personal;
		LoadAsistenciaDiariaColaboradorPeriodo();
		LoadAsistenciaDiariaColaboradorTotales();
		$("html, body").animate({
			scrollTop: $('#rowAsistenciaDiariaColaboradorPeriodo').offset().top
		}, 600, "linear", function () { });

	});
	AsistenciaDiariaColaboradorTotales.on('click', 'tbody tr', function (e) {
		e.preventDefault();
		//console.log('yep');
		var ths = $(this);
		AsistenciaDiariaColaboradorTotales.rows().$('tr').removeClass('selected');
		ths.addClass('selected');
		var row = AsistenciaDiariaColaboradorTotales.row(ths).data();
		_f = new Date(row.fecha);
		jsonParamDefault.MarcasFecha = _f.getFullYear() + '' + zeroPad(_f.getMonth() + 1, 2) + '' + zeroPad(_f.getDate(), 2);
		LoadAsistenciaDiariaColaboradorMarcas();
		LoadAsistenciaDiariaColaboradorSobretiempo();
	});

	$('#tableColaboradorTotalesRefrigerio').on('click', 'tbody tr', function (e) {
		e.preventDefault();
		var ths = $(this);
		tableColaboradorTotalesRefrigerio.rows().$('tr').removeClass('selected');
		var row = tableColaboradorTotalesRefrigerio.row(ths).data();
		ths.addClass('selected');
		_f = new Date(row.fecha);
		jsonParamDefault.MarcasFecha = _f.getFullYear() + '' + zeroPad(_f.getMonth() + 1, 2) + '' + zeroPad(_f.getDate(), 2);
		LoadAsistenciaDiariaColaboradorMarcas();
		LoadAsistenciaDiariaColaboradorSobretiempo();
	});

	$('#tableColaboradorTotalesFecha').on('click', 'tbody tr', function (e) {
		e.preventDefault();
		var ths = $(this);
		tableColaboradorTotalesFecha.rows().$('tr').removeClass('selected');
		var row = tableColaboradorTotalesFecha.row(ths).data();
		ths.addClass('selected');
		_f = new Date(row.fecha);
		jsonParamDefault.MarcasFecha = _f.getFullYear() + '' + zeroPad(_f.getMonth() + 1, 2) + '' + zeroPad(_f.getDate(), 2);
		jsonParamDefault.CodPersonal = row.cod_personal;
		LoadAsistenciaDiariaColaboradorMarcas();
		LoadAsistenciaDiariaColaboradorSobretiempo();
	});

	$('#btnItemEditarProgramacionHorario').on('click', async function () {
		var jsonAll = GetData.Json.All();
		if (UIEmpty(jsonAll.Periodo)) {
			MsgBox('Debe elegir un periodo primero.', 'warning', true);
			return;
		}
		if (UIEmpty(jsonAll.Colaborador)) {
			MsgBox('Debe elegir un registro primero.', 'warning', true);
			return;
		}
		if (UIEmpty(jsonAll.Totales)) {
			MsgBox('Debe elegir un registro primero.', 'warning', true);
			return;
		}
		var jsonColaborador = {};
		var jsonTotales = {};
		if (jsonAll.Filtro.ModoDeVentana == 'T') {
			jsonColaborador = jsonAll.Colaborador;
			jsonTotales = jsonAll.Totales;
		}
		else if (jsonAll.Filtro.ModoDeVentana == 'F') {
			jsonColaborador = jsonAll.Totales;
			jsonTotales = jsonAll.Colaborador;
		}

		$('#h4titleMEDPRHO').html(`Edición de horario <span class="badge badge-warning f-18">${jsonTotales.fecha_ddmmyyyy}</span>`);

		var _f = new Date(jsonAll.Totales.fecha);
		var jsonParam = {
			CodigoDePersonal: jsonColaborador.cod_personal,
			//CodigoDePersonal: jsonAll.Colaborador.cod_personal || jsonAll.Totales.cod_personal,
			Fecha: zeroPad(_f.getDate(), 2) + '/' + zeroPad(_f.getMonth() + 1, 2) + '/' + _f.getFullYear(),
		};
		UIMask.Time('input.txtHoraMEDPRHO');
		var fnSetHorarioEspecial = function () {
			$('#txtCodigoMEDPRHO').val('-2');
			$('#selectHorarioMEDPRHO').val('-2');
		}
		$('#chkNocturnoMEDPRHO').on('change', fnSetHorarioEspecial);
		$('input.txtHoraMEDPRHO').on('change', function (e) {
			fnSetHorarioEspecial();
			var mm = new moment(this.value, 'HH:mm');
			if (mm._isValid) {
				switch (this.id) {
					case 'txthiMEDPRHO':
						//Valor en Horario de Ingreso y calculamos el horario mínimo de ingreso: 6 horas antes
						var himin_temp = mm.add(-4, 'hours');
						var himin = himin_temp.add(1, 'minutes').format('HH:mm');
						$('#txthiminMEDPRHO').val(himin);
						break;
					case 'txthsMEDPRHO':
						//Valor en Horario de Salida y calculamos el horario máximo de salida: 6 horas después
						var hsmax_temp = mm.add(4, 'hours');
						var hsmax = hsmax_temp.add(-1, 'minutes').format('HH:mm');
						$('#txthsmaxMEDPRHO').val(hsmax);
						break;
				}
				//cuando se hace cambio en hi o hs ponerlo en tb pero no invesamente
				if (this.id == 'txthiMEDPRHO' || this.id == 'txthsMEDPRHO') {
					var hi = moment($('#txthiMEDPRHO').val(), 'HH:mm');
					var hs = moment($('#txthsMEDPRHO').val(), 'HH:mm');
					var v_tb = $('#txttbMEDPRHO').val();
					var th = moment.utc(hs.diff(hi));
					var rf = th.format('HH:mm');
					if (v_tb != '00:00') {
						var tb = moment(v_tb, 'HH:mm');
						var _th = moment(th.format('HH:mm'), 'HH:mm');
						var a_b = _th.diff(tb);
						rf = moment.utc(a_b).format('HH:mm');
					}
					$('#txtthMEDPRHO').val(rf);
				}
				//cuando se hace cambio en bs o bi ponerlo en tb pero no invesamente
				if (this.id == 'txtbsMEDPRHO' || this.id == 'txtbiMEDPRHO') {
					//No olvidarse que el bi es mayor
					var bs = moment($('#txtbsMEDPRHO').val(), 'HH:mm');
					var bi = moment($('#txtbiMEDPRHO').val(), 'HH:mm');
					var tb = moment.utc(bi.diff(bs));
					var rf = tb.format('HH:mm');
					$('#txttbMEDPRHO').val(rf);
					$('#txthsMEDPRHO').trigger('change');
				}
				//cuando se hace cambio en tb, recalcular
				if (this.id == 'txttbMEDPRHO') {
					$('#txthsMEDPRHO').trigger('change');
				}


			}
		});

		var fnSetInformation = function (jsonResult) {
			$('#txtCodigoMEDPRHO').val(jsonResult.cod_turno);
			$('#txtTrabajadorMEDPRHO').val(jsonResult.trabajador);
			$('#selectHorarioMEDPRHO').val(jsonResult.cod_turno);
			UICheckbox.setValue('#chkNocturnoMEDPRHO', jsonResult.flg_nocturno);
			$('#txthiminMEDPRHO').val(jsonResult.phimin || jsonResult.himin);
			$('#txthei3MEDPRHO').val(jsonResult.phei3 || jsonResult.hei3);
			$('#txthei2MEDPRHO').val(jsonResult.phei2 || jsonResult.hei2);
			$('#txthei1MEDPRHO').val(jsonResult.phei1 || jsonResult.hei1);
			$('#txthiMEDPRHO').val(jsonResult.phi || jsonResult.hi);
			$('#txtbsMEDPRHO').val(jsonResult.pbs || jsonResult.bs);
			$('#txtbiMEDPRHO').val(jsonResult.pbi || jsonResult.bi);
			$('#txthsMEDPRHO').val(jsonResult.phs || jsonResult.hs);
			$('#txthes1MEDPRHO').val(jsonResult.phes1 || jsonResult.hes1);
			$('#txthes2MEDPRHO').val(jsonResult.phes2 || jsonResult.hes2);
			$('#txthes3MEDPRHO').val(jsonResult.phes3 || jsonResult.hes3);
			$('#txthsmaxMEDPRHO').val(jsonResult.phsmax || jsonResult.hsmax);
			$('#txtthMEDPRHO').val(jsonResult.pth || jsonResult.th);
			$('#txttbMEDPRHO').val(jsonResult.ptb || jsonResult.tb);
			$('#txtthiMEDPRHO').val(jsonResult.thi);
			$('#txtthsMEDPRHO').val(jsonResult.ths);
			$('#txtthtMEDPRHO').val(jsonResult.tht);
			$('#txtthsiiMEDPRHO').val(jsonResult.thsii);
			$('#txtthsisMEDPRHO').val(jsonResult.thsis);
		};
		$('#selectHorarioMEDPRHO').off('change').on('change', async function (e) {
			var jsonResult = {
				cod_turno: this.value,
				trabajador: jsonAll.Colaborador.trabajador,
			};
			await UIAjax.invokeMethodAsync('controlasistencia', 'GetLoadDataDetalleHorario', { CodTurno: this.value }).then(function (r) {
				if (r.length > 0) {
					$.extend(true, jsonResult, r[0]);
				}
			});
			fnSetInformation(jsonResult);
		});
		await UIAjax.invokeMethodAsync('ajax/controlasistencia', 'GetSingleAsistenciaByCodPersonalFecha', jsonParam).then(function (r) {
			if (r.length > 0) {
				var jsonResult = r[0];
				fnSetInformation(jsonResult);
			}
		});

		$('#btnGuardarMEDPRHO').off('click').on('click', async function (e) {
			var jsonParam = {
				Asistencia: [],
			};
			jsonParam.Asistencia.push({
				cod_personal: jsonColaborador.cod_personal,
				fecha: jsonTotales.fecha_ddmmyyyy,
				cod_turno: $('#selectHorarioMEDPRHO').val(),
				flg_nocturno: UICheckbox.getValue('#chkNocturnoMEDPRHO'),
				phimin: $('#txthiminMEDPRHO').val(),
				phei3: $('#txthei3MEDPRHO').val(),
				phei2: $('#txthei2MEDPRHO').val(),
				phei1: $('#txthei1MEDPRHO').val(),
				phi: $('#txthiMEDPRHO').val(),
				pbs: $('#txtbsMEDPRHO').val(),
				pbi: $('#txtbiMEDPRHO').val(),
				phs: $('#txthsMEDPRHO').val(),
				phes1: $('#txthes1MEDPRHO').val(),
				phes2: $('#txthes2MEDPRHO').val(),
				phes3: $('#txthes3MEDPRHO').val(),
				phsmax: $('#txthsmaxMEDPRHO').val(),
				pth: $('#txtthMEDPRHO').val(),
				ptb: $('#txttbMEDPRHO').val(),
				thi: $('#txtthiMEDPRHO').val(),
				ths: $('#txtthsMEDPRHO').val(),
				tht: $('#txtthtMEDPRHO').val(),
				thsii: $('#txtthsiiMEDPRHO').val(),
				thsis: $('#txtthsisMEDPRHO').val(),
			});
			await UIAjax.runFunctionAsync('ajax/controlasistencia', 'PostAsistenciaPersonal', jsonParam, function () {
				if (jsonAll.Filtro.ModoDeVentana == 'T') {
					$('#AsistenciaDiariaColaboradores tbody tr.selected').trigger('click');
				}
				else if (jsonAll.Filtro.ModoDeVentana == 'F') {
					$('#tableColaboradoresFecha tbody tr.selected').trigger('click');
				}
				$('#modalEditarProgramacionHorario').modal('hide');
			});
		});
		$('#modalEditarProgramacionHorario').modal('show');
	});

	$('#btnItemExcepcionDeAsistencia').on('click', function () {
		//$('#modalRegistroDiarioExcepciones').modal('show');
		//return
		//var jsonParam = {
		//    CodigoDeTipoPlanilla: '01',
		//    CodigoAnual: '2020',
		//    CodigoDeExcepcion: '16',
		//    CodigoDePersonal: '000001',
		//    Numero: '0001'
		//};

		//var urlShowReport = `${portal}/ControlAsistencia/ShowRptPapeletaDeSalida/?${$.param(jsonParam)}`;
		//console.log(urlShowReport);
		//var options = {
		//    id: "myID",
		//    height: "80vh",
		//    //width: "500px",
		//    pdfOpenParams: {
		//        //view: 'FitV',
		//        page: '1',
		//        //pagemode: 'thumbs',
		//        //search: 'lorem ipsum'
		//    },
		//    //fallbackLink: "<p>This is a <a href='[url]'>fallback link</a></p>",
		//    forcePDFJS: true, //default false
		//};
		//PDFObject.embed(urlShowReport, "#showDoc", options);
		//$('#modalRpte').modal('show');
		//return;

		var jsonAll = GetData.Json.All();
		if (UIEmpty(jsonAll.Periodo)) {
			MsgBox('Debe elegir un periodo primero.', 'warning', true);
			return;
		}
		if (UIEmpty(jsonAll.Colaborador)) {
			MsgBox('Debe elegir un registro primero.', 'warning', true);
			return;
		}
		if (UIEmpty(jsonAll.Totales)) {
			MsgBox('Debe elegir un registro primero.', 'warning', true);
			return;
		}
		var jsonParam = {
			flg_rrhh: 'S'
		};
		UIAjax.invokeMethodAsync('ajax/controlasistencia', 'GetTipoExcepcionesRRHH', jsonParam).then(function (r) {
			var html = '';
			$.each(r.data, function (i, v) {
				//console.log(v);
				html += `<option value="${v.cod_Excepcion}" data-json='${JSON.stringify(v)}'>${v.cod_Excepcion} | ${v.flg_unidad} | ${v.nombre}</option>`;
			});
			var selExcepcionRDE = $('#selectExcepcionRDE');
			selExcepcionRDE.empty();
			selExcepcionRDE.append(html);
			selExcepcionRDE.val('01').trigger('change');

			var jsonColaborador = {};
			if (jsonAll.Filtro.ModoDeVentana == 'T') {
				jsonColaborador = jsonAll.Colaborador;
			}
			else if (jsonAll.Filtro.ModoDeVentana == 'F') {
				jsonColaborador = jsonAll.Totales;
			}

			$('#chkImprimirPapeletaRDE').prop('checked', false);
			$('#txtNumeroRDE').val('');
			$('#txtMotivoRDE').val('');
			$('#h4TitleMREDIEX').html(`Registro diario de Excepciones <span class="badge badge-warning f-18">${jsonColaborador.trabajador}</span >`);
			$('#txtFileExcepcion').val(null).trigger('change');
			$('#modalRegistroDiarioExcepciones').modal('show');
		});
	});

	UIEvent.FileChange('#btnAgregarSustentoMEX', '#txtFileExcepcion', function (e) {
		var ths = $(this);
		$('#btnAgregarSustentoMEX').html(`<i class="fas fa-file-upload"></i>AGREGAR SUSTENTO`);
		if (!UIEmpty(ths.val())) {
			$('#btnAgregarSustentoMEX').html(`<i class="fas fa-file-upload text-danger"></i>SUSTENTO AGREGADO`);
		}
	});

	$('#modalRegistroDiarioExcepciones').on('change', '#selectExcepcionRDE', function (e) {
		var ths = $(this);
		var json = JSON.parse($('option:selected', ths).attr('data-json'));

		var jsonTotales = GetData.Json.Totales();

		Load.modalRDE.selectExcepcion(json.cod_Excepcion);

		if (json.cod_Excepcion == '01') {
			//$('#selectTipoRDE').prop('selectedIndex', 0);
			$('#selectTipoRDE').val('-1');
			$('#chkPagoAdelantado').prop('checked', false);

			$('#rowMotivoRDE').removeClass('d-none');
		}
		else {
			$('#rowMotivoRDE').addClass('d-none');
		}

		$('#txtFechaRDE').val(jsonTotales.fecha_ddmmyyyy);
		//$('#txtFechaRDE').val('04/03/2020');

		if (json.flg_unidad == 'D') {
			$("#txtInicioRDE, #txtFinRDE, #txtNumeroDeDiasHorasRDE").unbind("change");
			$('#txtNumeroDeDiasHorasRDE').unmask();
			$('#txtInicioRDE, #txtFinRDE').mask("__/__/____", { placeholder: "__/__/____" });

			//$('#txtInicioRDE, #txtFinRDE').val(jsonTotales.fecha_ddmmyyyy);
			//$('#txtInicioRDE, #txtFinRDE').val('04/03/2020');

			$("#txtInicioFinRDE").datepicker("destroy");
			$("#txtInicioFinRDE").datepicker({
				//constrainInput: false,
				format: "dd/mm/yyyy",
				autoclose: true,
				language: 'es',
				//weekStart: 2,
			});

			//$("#txtInicioRDE").datepicker("destroy");
			//$("#txtInicioRDE").datepicker({
			//    //constrainInput: false,
			//    format: "dd/mm/yyyy",
			//    autoclose: true,
			//    language: 'es',
			//    weekStart: 1,
			//});

			$("#txtInicioRDE").datepicker("setDate", jsonTotales.fecha_ddmmyyyy);
			$("#txtFinRDE").datepicker("setDate", jsonTotales.fecha_ddmmyyyy);
			//$('#txtInicioRDE, #txtFinRDE')

			$('#lblNumeroDeDiasHorasRDE').text('# Días:');
			$('#txtNumeroDeDiasHorasRDE').val('1').trigger('change');
		}
		else {
			$("#txtInicioFinRDE").datepicker("destroy");
			$("#txtInicioRDE").datepicker("destroy");
			$("#txtFinRDE").datepicker("destroy");

			var now = new moment();
			$("#txtInicioRDE, #txtFinRDE").val(now.format("HH:mm"));

			$('#txtInicioRDE, #txtFinRDE, #txtNumeroDeDiasHorasRDE').mask("00:00", { placeholder: "00:00" });

			$('#lblNumeroDeDiasHorasRDE').text('# Horas:');
			$('#txtNumeroDeDiasHorasRDE').val('00:00');

			UIHH_mm.Change('#txtInicioRDE, #txtFinRDE, #txtNumeroDeDiasHorasRDE');
		}

	});
	$('#modalRegistroDiarioExcepciones').on('change', '#txtNumeroDeDiasHorasRDE', function (e) {
		e.stopPropagation();
		var ths = $(this);
		var valor = ths.val();

		var jsonOption = JSON.parse($('#selectExcepcionRDE option:selected').attr('data-json'));

		if (jsonOption.flg_unidad == 'D') {
			//console.log(valor);
			if (valor < 1) {
				ths.val(1);
				valor = 1;
			}

			valor = valor - 1;

			var fecha = $("#txtInicioRDE").datepicker("getDate");
			var n_fecha = moment(fecha, "DD-MM-YYYY").add(valor, 'days');
			var str_fecha = n_fecha.format('DD/MM/YYYY');
			//$('#txtFinRDE').datepicker('update', str_fecha);
			$('#txtFinRDE').datepicker('setDate', str_fecha);
		}
		else {
			var hora_ini = $("#txtInicioRDE").val();
			var hhmm = moment(hora_ini, 'HH:mm');
			var hhmm_add = moment(valor, 'HH:mm');
			var n_hhmm = hhmm.add(moment.duration(valor));
			//var n_hhmm = hhmm.add(hhmm_add.hours(), 'hours').add(hhmm_add.minutes(), 'minutes');
			var str_hhmm = n_hhmm.format('HH:mm');
			$('#txtFinRDE').val(str_hhmm);
		}

		//console.log(jsonOption);
	});
	$('#modalRegistroDiarioExcepciones').on('change', '#txtInicioRDE, #txtFinRDE', function (e) {
		e.stopPropagation();
		var jsonOption = JSON.parse($('#selectExcepcionRDE option:selected').attr('data-json'));
		if (jsonOption.flg_unidad == 'D') {
			var fecha_ini = $("#txtInicioRDE").datepicker("getDate");
			var fecha_fin = $("#txtFinRDE").datepicker("getDate");
			var ddmmyyyy_ini = moment(fecha_ini);
			var ddmmyyyy_fin = moment(fecha_fin);
			ddmmyyyy_fin.add(1, 'days');
			var diffDays = ddmmyyyy_fin.diff(ddmmyyyy_ini, 'days');
			$('#txtNumeroDeDiasHorasRDE').val(diffDays);
		}
		else {
			var hora_ini = $("#txtInicioRDE").val();
			var hora_fin = $("#txtFinRDE").val();
			var hhmm = moment(hora_fin, 'HH:mm');
			var n_hhmm = hhmm.add(-moment.duration(hora_ini));
			var str_hhmm = n_hhmm.format('HH:mm');
			$('#txtNumeroDeDiasHorasRDE').val(str_hhmm);
		}
	});
	$('#modalRegistroDiarioExcepciones').on('click', '#btnAceptarRDE', async function (e) {
		var jsonOption = JSON.parse($('#selectExcepcionRDE option:selected').attr('data-json'));

		var jsonAll = GetData.Json.All();
		var jsonPeriodo = jsonAll.Periodo;
		var jsonColaborador = jsonAll.Colaborador;
		var jsonTotales = jsonAll.Totales;
		var CodigoDePersonal = '';
		var Fecha = '';
		var valor = $('#selectModoDeVentana').val();
		if (valor == "T") {
			CodigoDePersonal = jsonColaborador.cod_entidad;
			Fecha = jsonTotales.fecha_ddmmyyyy;
		}
		else {
			CodigoDePersonal = jsonTotales.cod_personal;
			Fecha = jsonTotales.fecha_ddmmyyyy;
		}

		var files = $('#txtFileExcepcion').prop("files");


		var jsonParam = {
			CodigoAnual: jsonPeriodo.cod_anual,
			CodigoAnualAfecto: $('#selectPeriodoAfectoRDE').val(),
			Mensaje: $('#txtMotivoRDE').val(),
			CodigoDeTipoPlanilla: $('#AsistenciaDiariaSelectTipoPlanilla').val(),
			FlgProceso: 'P',
			FlgClaseProceso: 'B',
			CodigoDePersonal: CodigoDePersonal,
			CodigoDeExcepcion: jsonOption.cod_Excepcion,
			Fecha: Fecha,
			FlgUnidad: jsonOption.flg_unidad,
			FlgAdelanto: 'N',
			File: files[0],
		};

		var tiempo_ini = $('#txtInicioRDE').val();
		var tiempo_fin = $('#txtFinRDE').val();
		var tiempo = $('#txtNumeroDeDiasHorasRDE').val();

		if (jsonOption.flg_unidad == 'D') {
			jsonParam.FechaDeInicio = tiempo_ini;
			jsonParam.FechaDeFin = tiempo_fin;
			jsonParam.Unidades = tiempo;
		}
		else {
			jsonParam.Hora1 = tiempo_ini;
			jsonParam.Hora2 = tiempo_fin;
			jsonParam.Hora = tiempo;
		}

		if (jsonOption.cod_Excepcion == '01') {
			jsonParam.Tipo = $('#selectTipoRDE').val();
			if ($('#chkPagoAdelantado').is(':checked')) {
				jsonParam.FlgAdelanto = 'S';
			}
		}
		//console.log(jsonParam);
		////if (jsonParam.File != null) {
		////	//jsonParam.File.type = "application/pdf"
		////	//jsonParam.File.type = "image/jpeg"
		////	//jsonParam.File.type = "image/png"
		////	//console.log('asdasd');
		////}
		//return;
		await UIAjax.invokeFormAsync('ajax/controlasistencia', 'PostExcepciones', jsonParam).then(function (r) {
			if (r.respuesta > 0) {
				$('#txtNumeroRDE').val(r.valor1);
				$('#modalRegistroDiarioExcepciones').modal('hide');
				LoadAsistenciaDiariaColaboradorTotales();
				MsgBox('success');
				if ($('#chkImprimirPapeletaRDE').is(':checked')) {
					jsonParam.Numero = r.valor1;
					UIModal.Rpte('ControlAsistencia', 'ShowRptPapeletaDeSalida', jsonParam);
				}
			}
			else {
				MsgBox(r.mensaje, "warning", false);
			}
		});

	});

	$('#AsistenciaDiariaPeriodos').on('click', 'tbody tr', function () {
		AsistenciaDiariaPeriodos.rows().$('tr.selected').removeClass('selected');
		var ths = $(this);
		ths.addClass('selected');
	});

	$('#AsistenciaDiariaColaboradorMarcas').on('click', 'tbody tr', function () {
		AsistenciaDiariaColaboradorMarcas.rows().$('tr.selected').removeClass('selected');
		var ths = $(this);
		ths.addClass('selected');
	});

	$('#tableColaboradoresFecha').on('click', 'tbody tr', function (e) {
		var ths = $(this);
		tableColaboradoresFecha.rows().$('tr').removeClass('selected');
		ths.addClass('selected');
		//console.log(ths);
		LoadAsistenciaDiariaColaboradorTotales();
		$("html, body").animate({
			scrollTop: $('#rowAsistenciaDiariaColaboradorPeriodo').offset().top
		}, 600, "linear", function () {
		});
	});

	$('#modalAgregarMarca').on('click', '#btnAceptarAM', function () {
		var modal = $('#modalAgregarMarca');

		var trTotales;
		var jsonTotales;
		var valor = $('#selectModoDeVentana').val();
		if (valor == "T") {
			var chk = $('#chkRefrigerio').is(':checked');
			if (chk) {
				trTotales = $('#tableColaboradorTotalesRefrigerio tbody tr.selected');
				jsonTotales = tableColaboradorTotalesRefrigerio.row(trTotales).data();
			}
			else {
				trTotales = $('#AsistenciaDiariaColaboradorTotales tbody tr.selected');
				jsonTotales = AsistenciaDiariaColaboradorTotales.row(trTotales).data();
			}
		}
		else {
			trTotales = $('#tableColaboradorTotalesFecha tbody tr.selected');
			if (!UIEmpty(tableColaboradorTotalesFecha)) {
				jsonTotales = tableColaboradorTotalesFecha.row(trTotales).data();
			}
		}

		var jsonParam = {
			FechaDeInicio: jsonTotales.fecha_ddmmyyyy,
			Mensaje: $('#txtDescripcionAM').val(),
			Hora1: $('#txtHoraAM').val(),
			CodigoDeReloj: '01',
			CodigoDePersonal: jsonTotales.cod_personal,
		};
		switch (modal.attr('data-action')) {
			case '0':
				break;
			case '1':
				var trMarcas = AsistenciaDiariaColaboradorMarcas.rows().$('tr.selected');
				var jsonMarcas = AsistenciaDiariaColaboradorMarcas.row(trMarcas).data();
				jsonParam.Hora2 = jsonMarcas.marca_HHmm;
				break;
			default:
		}
		//var himin = $('#si-phimin').html();
		//var hsmax = $('#si-phsmax').html();

		//var m_himin = moment(himin, 'HH:mm');
		//var m_hsmax = moment(hsmax, 'HH:mm');
		//var nowTime = moment(jsonParam.Hora1, 'HH:mm');
		//console.log(nowTime.isBetween(m_himin, m_hsmax) || nowTime.isSame(m_himin) || nowTime.isSame(m_hsmax)); // true

		UIAjax.runFunctionAsync('ajax/controlasistencia', 'PostMarcas', jsonParam);
		//$('#AsistenciaDiariaColaboradorTotales tbody tr.selected').trigger('click');
		trTotales.trigger('click');
		//LoadAsistenciaDiariaColaboradorMarcas();
		//AsistenciaDiariaColaboradorMarcas
		$('#modalAgregarMarca').modal('hide');

	});

	$('#chkRefrigerio').on('change', function (e) {
		e.preventDefault();
		var ths = $(this);
		var Rows;
		var valor = $('#selectModoDeVentana').val();
		if (valor == "T") {
			Rows = AsistenciaDiariaColaboradores.rows().$('tr.selected');
		}
		else {
			Rows = tableColaboradoresFecha.rows().$('tr.selected');
		}
		if (Rows.length != 1) {
			ths.prop('checked', !ths.is(':checked'));
			MsgBox('Debe seleccionar a un colaborador primero.', 'warning', false);
			return false;
		}
		LoadAsistenciaDiariaColaboradorTotales();
	});

	$('#selectModoDeVentana').on('change', function (e) {
		var ths = $(this);
		if (ths.val() != 'T') {
			$('#chkInconsistencia').prop('checked', false);
			$('#selectInconsistencia').val('%');
		}
		LoadAsistenciaDiariaColaboradores();
	});

	$('#btnItemImprimir').on('click', function (e) {
		var jsonAll = GetData.Json.All();
		var jsonColaborador = jsonAll.Colaborador;
		if (!UIEmpty(jsonColaborador)) {
			var jsonFiltro = jsonAll.Filtro;
			var jsonPeriodo = jsonAll.Periodo;
			var jsonParam = Object.assign({}, jsonFiltro);
			var urlShowReport;
			if (jsonFiltro.ModoDeVentana == 'T') {
				jsonParam.FechaDeInicio = jsonPeriodo.asi_desde;
				jsonParam.FechaDeFin = jsonPeriodo.asi_hasta;
				jsonParam.CodigoDePersonal = jsonColaborador.cod_personal;
				//urlShowReport = `${portal}/ControlAsistencia/ShowRptAsistenciaDiariaTF1/?${$.param(jsonParam)}`;
				UIModal.Rpte('ControlAsistencia', 'ShowRptAsistenciaDiariaTF1', jsonParam);
			}
			else {
				jsonParam.CodigoDeCargo = 'T';
				jsonParam.Estado = '%';
				jsonParam.Fecha = jsonColaborador.fecha_ddmmyyyy;
				//urlShowReport = `${portal}/ControlAsistencia/ShowRptAsistenciaDiariaFF1/?${$.param(jsonParam)}`;
				UIModal.Rpte('ControlAsistencia', 'ShowRptAsistenciaDiariaFF1', jsonParam);
			}
		}
		else {
			MsgBox('Debe seleccionar un registro primero.', 'warning', false);
			return;
		}
	});

	$('#btnItemImportarMarcaciones').on('click', function (e) {
		$('#btnVolverIM').trigger('click');
		Load.modalIM.Ready(function () {
			$('#modalImportarMarcaciones').modal('show');
		});
	});

	$('#btnPlantillaIM').on('click', function (e) {
		var Link = document.createElement("a");
		Link.setAttribute("href", portal + "/ajax/controlasistencia/GetPlantillaMarcacionCSV/?");
		document.body.appendChild(Link);
		Link.click();
		document.body.removeChild(Link);
	});
	$('#btnCargarArchivoIM').on('click', function (e) {
		if (UIEmpty(tableColadoradorIM)) {
			$("#fileCargarArchivoIM").trigger('click');
			return;
		}
		MsgBox(`Desea eliminar información previamente.`, 'question', false, false, function () {
			$("#fileCargarArchivoIM").trigger('click');
		});
	});
	$('#fileCargarArchivoIM').on('click', function (e) {
		var ths = $(this);
		ths.val(null);
	});
	$('#fileCargarArchivoIM').on('change', function (e) {
		var ths = $(this);
		e.preventDefault();
		var files = ths.prop("files");
		var formData = new FormData();
		formData.append("File", files[0]);
		$.ajax({
			type: "POST",
			url: portal + "/ajax/controlasistencia/GetColaboradorPlantillaMarcacion",
			data: formData,
			contentType: false,
			processData: false,
			success: function (r) {
				if (r.respuesta > 0) {
					MsgBox('success');
					tableColadoradorIM = $('#tableColadoradorIM').DataTable({
						//ajax: portal + '/ajax/core/GetFechas/?' + $.param(jsonParamDefault),
						data: r.json.data,
						initComplete: function (settings, json) {
						},
						bDestroy: true,
						//order: false,
						//ordering: false,
						//searching: true,
						//paging: true,
						//info: true,
						lengthChange: false,
						columns: [
							{ data: "cod_fotocheck", className: "text-center" },
							{ data: "trabajador" },
							{ data: "fecha", className: "text-center" },
							{ data: "reloj", className: "text-center" },
						],
					});
				}
				else {
					MsgBox(r.mensaje, 'warning', false);
				}
				console.log(r);
			},
			error: function (r) {
				console.log(r);
				MsgBox('warning');
			}
		});
		//$('#fileCargarArchivoIM').parse({
		//    config: {
		//        //delimiter: ",", //auto
		//        delimiter: "auto",
		//        quotes: true,
		//        skipEmptyLines: true,
		//        //dynamicTyping: true,
		//        complete: function (results, file) {
		//            //var allRows = data.split(/\r?\n|\r/);
		//            //console.log(results);
		//            var table = "<table class='table'>";
		//            var data = results.data;
		//            for (i = 0; i < data.length; i++) {
		//                table += "<tr>";
		//                var row = data[i];
		//                //var cells = row.join(",").split(",");
		//                var cells = $.splitAttrString(row.join(","));
		//                for (j = 0; j < cells.length; j++) {
		//                    table += "<td>";
		//                    table += cells[j];
		//                    table += "</th>";
		//                }
		//                table += "</tr>";
		//            }
		//            table += "</table>";
		//            $("#colTable").html(table);
		//        },
		//    },
		//    before: function (file, inputElem) {
		//        //console.log("Parsing file...", file);
		//    },
		//    error: function (err, file) {
		//        //console.log("ERROR:", err, file);
		//    },
		//    complete: function (a,b) {
		//        //console.log("Done with all files");
		//    }
		//});
	});
	$('#btnImportarIM').on('click', function (e) {
		var ths = $(this);
		if (UIEmpty(tableColadoradorIM)) {
			MsgBox('Debe cargar un archivo CSV primero.', 'warning', true);
			return;
		}
		var jsonParam = {
			Json: [],
		};
		var jsonRows = tableColadoradorIM.rows().data();
		$.each(jsonRows, function (i, v) {
			jsonParam.Json.push({
				CodigoDeFotocheck: v.cod_fotocheck,
				Fecha: v.fecha,
				CodigoDeReloj: v.reloj,
			});
		});
		MsgBox(`Se iniciará con la importación masiva.`, 'question', false, false, function () {
			UIAjax.runFunctionAsync('ajax/controlasistencia', 'PostProcesoPlantillaMarcacion', jsonParam, function () {
				Load.modalIM.Ready();
			});
		});
	});

	$("#smartwizard").on("leaveStep", function (e, anchorObject, currentStepIndex, nextStepIndex, stepDirection) {
		var ths = $(this);
	});
	$("#smartwizard").on("showStep", function (e, anchorObject, stepIndex, stepDirection) {
		var ths = $(this);
		var indexDisable;
		switch (stepIndex) {
			case 0:
				indexDisable = 1;
				break;
			case 1:
				indexDisable = 0;
				break;
		}
		ths.smartWizard("stepState", [indexDisable], "disable");
	});

	$('#btnBorrarProcesoIM, #btnConsultarDataProcesoIM').on('click', function (e) {
		var ths = $(this);
		var Rows = tableProcesoIM.rows().$('tr.selected');
		var jsonRow = tableProcesoIM.row(Rows).data();
		if (UIEmpty(jsonRow)) {
			MsgBox('Debe seleccionar un proceso primero', 'warning', true);
			return;
		}
		var jsonParam = {
			Numero: jsonRow.numero_importacion,
		};
		var id = ths.prop('id');
		switch (id) {
			case 'btnBorrarProcesoIM':
				MsgBox(`Eliminando proceso: ${jsonParam.Numero}`, 'question', false, true, function () {
					UIAjax.runFunctionAsync('ajax/controlasistencia', 'DeleteProcesoPlantillaMarcacion', jsonParam, function () {
						Load.modalIM.Ready();
					});
				});
				break;
			case 'btnConsultarDataProcesoIM':
				var _chk = function (id, check) {
					var checked = check ? 'checked' : '';
					return `<div class="checkbox checkbox-primary d-inline mr-0 p-0" without-label>
										<input id="${id}" type="checkbox" ${checked} >
										<label for="${id}" class="cr m-0"></label>
									</div>`
				}
				tableDataProcesadaIM = $('#tableDataProcesadaIM').DataTable({
					ajax: portal + '/ajax/controlasistencia/GetProcesoDetallePlantillaMarcacion/?' + $.param(jsonParam),
					initComplete: function (settings, json) {
						tableDataProcesadaIM.rows().$('input[id^="chkCargado"]').click(false);
						$("#smartwizard").smartWizard("stepState", [1], "enable");
						$('#smartwizard').smartWizard("next");
					},
					lengthChange: false,
					//ordering: false,
					//searching: true,
					//paging: true,
					//info: true,
					columns: [
						{ data: "item", className: "text-center" },
						{ data: "fotocheck", className: "text-center" },
						{ data: "trabajador" },
						{ data: "fecha_hora", className: "text-center" },
						{ data: "nombre_empresa" },
						{
							data: "cargado", render: function (data, type, row, meta) {
								return _chk(`chkCargado${row.item}`, data);
							}, className: "text-center"
						},
					],
				});
				break;
		}
	});

	$('#btnVolverIM').on('click', function (e) {
		$("#smartwizard").smartWizard("stepState", [0], "enable");
		$('#smartwizard').smartWizard("prev");
	});

	$('#btnItemGenerarMarcasManualesMasivas').on('click', function (e) {
		Load.modalGMMM.Ready(jsonParamDefault, function () {
			var txtFecha = $('#txtFechaGMMM');
			txtFecha.datepicker("destroy");
			txtFecha.datepicker({
				format: "dd/mm/yyyy",
				autoclose: true,
				language: 'es',
				//startDate: '-3d',
				//weekStart: 2,
			});
			txtFecha.datepicker("setDate", new Date);
			$('#txtHoraGMMM').val('00:00');
			$('#txtNotaGMMM').val('');
			$('#modalGenerarMarcaManualMasiva').modal('show');
		});
	});

	$('select.selClasificadorGMMM').on('change', function () {
		var jsonParam = Load.modalGMMM.Filtro();
		var jsonParametro = Object.assign({}, jsonParamDefault);
		jsonParametro.CodProyecto = jsonParam.CodigoDeProyecto;
		jsonParametro.CodCentroCosto = jsonParam.CodigoDeCC;
		jsonParametro.CodUnidadNegocio = jsonParam.CodigoDeUN;
		jsonParametro.CodArea = jsonParam.CodigoDeArea;
		jsonParametro.CodLocalidad = jsonParam.CodigoDeLocalidad;
		jsonParametro.CodTipoPlanilla = jsonParam.CodigoDeTipoPlanilla;
		jsonParametro.Estado = jsonParam.Estado;
		Load.modalGMMM.Ready(jsonParametro);
	});

	$('#btnGuardarGMMM').on('click', function () {
		var Rows = tableColadoradorGMMM.rows().$('tr.selected');
		if (Rows.length == 0) {
			MsgBox('Debe seleccionar un colaborador como mínimo.', 'warning', false);
			return;
		}
		var jsonParam = Load.modalGMMM.Filtro();
		var APersonal = [];
		$.each(Rows, function (i, v) {
			var jsonRow = tableColadoradorGMMM.row(v).data();
			APersonal.push(jsonRow.cod_personal);
		});
		jsonParam.CodigoDePersonal = APersonal.join(',');
		jsonParam.CodigoDeReloj = '00';
		UIAjax.runFunctionAsync('ajax/controlasistencia', 'PostMarcasMasiva', jsonParam, function () {
			$('#modalGenerarMarcaManualMasiva').modal('hide');
		});
	});

	$('#btnItemRealizarCalculo').on('click', async function () {
		var jsonFiltro = GetData.Json.Filtro();
		if (jsonFiltro.ModoDeVentana == 'T') {
			var jsonPeriodo = GetData.Json.Periodo();
			var jsonColaborador = GetData.Json.Colaborador();
			if (!UIEmpty(jsonColaborador)) {
				if (jsonPeriodo.estado == 'C') {
					MsgBox("El periodo se encuentra cerrado.", "warning", false);
					return;
				}
				MsgBox("Este proceso puede demorar cosiderablemente.", "question", false, false, async function () {
					var jsonParam = {
						CodigoDePersonal: jsonColaborador.cod_personal,
						FechaDeInicio: jsonPeriodo.asi_desde,
						FechaDeFin: jsonPeriodo.asi_hasta,
					};
					await UILoadingDefault("show");
					await UIAjax.runFunctionAsync('ajax/controlasistencia', 'PostCalificaAsistManualTrab', jsonParam);
					UILoadingDefault("hide");
				});
			}
			else {
				UIMensaje.COLABORADOR();
			}
		}
		else {
			MsgBox("Sólo se puede hacer cálculo en el modo de ventana Trabajador.", "warning", false);
		}
	});

});

async function AsistenciaDiariaLoadPeriodos(jsonParams) {

	var jsonParamsPre = {
		CodTipoPlanilla: $('#AsistenciaDiariaSelectTipoPlanilla').val(),
		Order: 'periodo',
		OrderType: 'DESC'
	};
	$.extend(true, jsonParamDefault, jsonParamsPre, jsonParams);
	await UIAjax.invokeMethodAsync('ajax/controlasistencia', 'GetPeriodos', jsonParamDefault).then(function (r) {
		selectPeriodo.empty();
		var html = '';
		$.each(r.data, function (i, v) {
			html += `<option value='${JSON.stringify(v)}'>${v.desde_hasta}</option>`;
		});
		selectPeriodo.append(html);
		selectPeriodo.on('change', function (e) {
			UIDataTable.setEmpty('#AsistenciaDiariaColaboradorTotales, #AsistenciaDiariaColaboradorPeriodo, #AsistenciaDiariaColaboradorMarcas');
			var ths = $(this);
			var json = JSON.parse(ths.val());
			jsonParamDefault.Periodo = json.periodo;
			jsonParamDefault.Mes = json.mes;
			jsonParamDefault.Numero = json.numero;
			jsonParamDefault.AsiDesde = transformDateSql(json.asi_desde);
			jsonParamDefault.AsiHasta = transformDateSql(json.asi_hasta);
			LoadAsistenciaDiariaColaboradores();
			$('#btnItemRealizarCalculo').addClass('d-none');
			$('#btnItemEditarProgramacionHorario').addClass('d-none');
			$('#btnItemExcepcionDeAsistencia').addClass('d-none');
			if (json.estado == "A") {
				$('#btnItemRealizarCalculo').removeClass('d-none');
				$('#btnItemEditarProgramacionHorario').removeClass('d-none');
				$('#btnItemExcepcionDeAsistencia').removeClass('d-none');
			}
		});
		var valor = selectPeriodo.val();
		if (!UIEmpty(valor)) {
			selectPeriodo.trigger('change');
		}
	});
}

function LoadAsistenciaDiariaColaboradorPeriodo() {
	//console.log(jsonParamDefault);
	UIDataTable.setEmpty('#AsistenciaDiariaColaboradorPeriodo');
	if (!UIEmpty(jsonParamDefault.CodPersonal)) {
		AsistenciaDiariaColaboradorPeriodo = $('#AsistenciaDiariaColaboradorPeriodo').DataTable({
			ajax: portal + '/ajax/controlasistencia/AsistenciaDiariaColaboradorPeriodo/?' + $.param(jsonParamDefault),
			initComplete: function (settings, json) {

			},
			select: {
				style: 'api'
			},
			searching: false,
			paging: false,
			info: false,
			columns: [
				{ data: "nombre" },
				{ data: "th" },
				{ data: "htar" },
				{ data: "the1" },
				{ data: "the2" },
				{ data: "the3" },
				{ data: "the7" },
				{ data: "the8" },
				{
					data: null, "defaultContent": "&nbsp;", "render": function (data, type, row, meta) {
						_d = row.the1 + row.the1 + row.the3;
						return _d.toFixed(2);
					}
				},
				{ data: "the4" },
				{ data: "the5" },
				{ data: "the6" },
				{
					data: null, "defaultContent": "&nbsp;", "render": function (data, type, row, meta) {
						_d = row.the5 + row.the6;
						return _d.toFixed(2);
					}
				},
				{ data: "hai" },
				{ data: "fi" },
				{ data: "fm" },
				{ data: "si" },
				{ data: "lab" },
			],
		});
	}
};
function LoadAsistenciaDiariaColaboradorTotales() {
	$('#rowColaboradorTotales').addClass('d-none');
	$('#rowColaboradorTotalesRefrigerio').addClass('d-none');
	$('#rowColaboradorTotalesFecha').addClass('d-none');

	var valor = $('#selectModoDeVentana').val();
	if (valor == "T") {
		var chk = $('#chkRefrigerio').is(':checked');
		if (chk) {
			$('#rowColaboradorTotalesRefrigerio').removeClass('d-none');
			tableColaboradorTotalesRefrigerio = $('#tableColaboradorTotalesRefrigerio').DataTable({
				ajax: portal + '/ajax/controlasistencia/AsistenciaDiariaColaboradorTotales2/?' + $.param(jsonParamDefault),
				initComplete: function (settings, json) {

				},
				select: {
					style: 'single'
				},
				searching: false,
				paging: false,
				columns: [
					{ data: "dia_text" },
					{ data: "fecha_ddmm" },
					{
						data: "flg_ok", "render": function (data, type, row, meta) {
							if (data == "1") {
								return '<span class="badge badge-dark">&nbsp;</span>';
							} else {
								return "&nbsp;";
							}
						}
					},
					{
						data: null, render: function (data, type, row, meta) {
							return '<span class="badge badge-warning">&nbsp;</span>';
						}
					},
					{
						data: "flg_ta", "render": function (data, type, row, meta) {
							if (data == "1") {
								return '<span class="badge badge-sky">&nbsp;</span>';
							} else {
								return "&nbsp;";
							}
						}
					},
					{
						data: "flg_fi", "render": function (data, type, row, meta) {
							if (data == "1") {
								return '<span class="badge badge-success">&nbsp;</span>';
							} else {
								return "&nbsp;";
							}
						}
					},
					{
						data: "flg_ai", "render": function (data, type, row, meta) {
							if (data == "1") {
								return '<span class="badge badge-fucsia">&nbsp;</span>';
							} else {
								return "&nbsp;";
							}
						}
					},
					{
						data: "flg_si", "render": function (data, type, row, meta) {
							if (data == "1") {
								return '<span class="badge badge-warning">&nbsp;</span>';
							} else {
								return "&nbsp;";
							}
						}
					},
					{ data: "excepcion" },//Excepciones
					{ data: "himax" },
					{ data: "hsmax" },
					{ data: "th" },
					{ data: "tb" },
					{ data: null, "defaultContent": "&nbsp;" },//hrsa
					{ data: null, "defaultContent": "&nbsp;" },//hrrd
					{ data: null, "defaultContent": "&nbsp;" },//hrsi
					{ data: "cod_turno" },
					{ data: "phi" },
					{ data: "phs" },
					{ data: "hpi" },
					{ data: "hps" },
					{ data: "bs" },
					{ data: "bi" },
					{ data: "ptb" },
					{ data: "fecha_ingreso" },
					{ data: "fecha_cese" },
					{ data: "hi" },//Hmin
					{ data: "hs" },//Hmax
					{ data: "htarj" },
					{ data: "th2" },
					{ data: "th3" },
					{ data: "th4" },
				],
				info: false
			});
		}
		else {
			$('#rowColaboradorTotales').removeClass('d-none');
			AsistenciaDiariaColaboradorTotales = $('#AsistenciaDiariaColaboradorTotales').DataTable({
				ajax: portal + '/ajax/controlasistencia/AsistenciaDiariaColaboradorTotales/?' + $.param(jsonParamDefault),
				initComplete: function (settings, json) {
				},
				select: {
					style: 'single'
				},
				searching: false,
				paging: false,
				//order: [[ 2, "asc" ]],
				columns: [
					{ "data": null, "defaultContent": "&nbsp;" },
					{
						"data": "fecha", "render": function (data, type, row, meta) {
							_f = new Date(data);
							return zeroPad(_f.getDate(), 2) + '/' + zeroPad(_f.getMonth() + 1, 2);
						}
					},
					{
						"data": "flg_ok", "render": function (data, type, row, meta) {
							if (data == "1") {
								return '<span class="badge badge-dark">&nbsp;</span>';
							} else {
								return "&nbsp;";
							}
						}
					},
					{
						"data": "flg_fm", "render": function (data, type, row, meta) {
							if (data == "1") {
								return '<span class="badge badge-danger">&nbsp;</span>';
							} else {
								return "&nbsp;";
							}
						}
					},
					{
						"data": "flg_ta", "render": function (data, type, row, meta) {
							if (data == "1") {
								return '<span class="badge badge-sky">&nbsp;</span>';
							} else {
								return "&nbsp;";
							}
						}
					},
					{
						"data": "flg_fi", "render": function (data, type, row, meta) {
							if (data == "1") {
								return '<span class="badge badge-success">&nbsp;</span>';
							} else {
								return "&nbsp;";
							}
						}
					},
					{
						"data": "flg_ai", "render": function (data, type, row, meta) {
							if (row.flg_ai == "1") {
								return '<span class="badge badge-fucsia">&nbsp;</span>';
							} else {
								return "&nbsp;";
							}
						}
					},
					{
						"data": "flg_si", "render": function (data, type, row, meta) {
							if (data == "1") {
								return '<span class="badge badge-warning">&nbsp;</span>';
							} else {
								return "&nbsp;";
							}
						}
					},
					{ "data": "excepcion" },//Excepciones
					{
						"data": "hi", "render": function (data, type, row, meta) {
							if (data) {
								_f = new Date(data);
								return zeroPad(_f.getHours(), 2) + ':' + zeroPad(_f.getMinutes(), 2);
							} else {
								return "&nbsp;";
							}
						}
					},
					{
						"data": "hs", "render": function (data, type, row, meta) {
							if (data) {
								_f = new Date(data);
								return zeroPad(_f.getHours(), 2) + ':' + zeroPad(_f.getMinutes(), 2);
							} else {
								return "&nbsp;";
							}
						}
					},
					{
						"data": "th", "render": function (data, type, row, meta) {
							if (data) {
								_f = new Date(data);
								return zeroPad(_f.getHours(), 2) + ':' + zeroPad(_f.getMinutes(), 2);
							} else {
								return "&nbsp;";
							}
						}
					},
					{
						"data": "tb", "render": function (data, type, row, meta) {
							if (data) {
								_f = new Date(data);
								return zeroPad(_f.getHours(), 2) + ':' + zeroPad(_f.getMinutes(), 2);
							} else {
								return "&nbsp;";
							}
						}
					},
					{
						"data": "htar", "render": function (data, type, row, meta) {
							if (data) {
								_f = new Date(data);
								return zeroPad(_f.getHours(), 2) + ':' + zeroPad(_f.getMinutes(), 2);
							} else {
								return "&nbsp;";
							}
						}
					},
					{
						"data": "the1", "render": function (data, type, row, meta) {
							if (data) {
								_f = new Date(data);
								return zeroPad(_f.getHours(), 2) + ':' + zeroPad(_f.getMinutes(), 2);
							} else {
								return "&nbsp;";
							}
						}
					},
					{
						"data": "the2", "render": function (data, type, row, meta) {
							if (data) {
								_f = new Date(data);
								return zeroPad(_f.getHours(), 2) + ':' + zeroPad(_f.getMinutes(), 2);
							} else {
								return "&nbsp;";
							}
						}
					},
					{
						"data": "the3", "render": function (data, type, row, meta) {
							if (data) {
								_f = new Date(data);
								return zeroPad(_f.getHours(), 2) + ':' + zeroPad(_f.getMinutes(), 2);
							} else {
								return "&nbsp;";
							}
						}
					},
					{
						"data": "the7", "render": function (data, type, row, meta) {
							if (data) {
								_f = new Date(data);
								return zeroPad(_f.getHours(), 2) + ':' + zeroPad(_f.getMinutes(), 2);
							} else {
								return "&nbsp;";
							}
						}
					},
					{
						"data": "the8", "render": function (data, type, row, meta) {
							if (data) {
								_f = new Date(data);
								return zeroPad(_f.getHours(), 2) + ':' + zeroPad(_f.getMinutes(), 2);
							} else {
								return "&nbsp;";
							}
						}
					},
					{ "data": null, "defaultContent": "&nbsp;" },//hfer
					{ "data": null, "defaultContent": "&nbsp;" },//honcf
					{
						"data": "hai", "render": function (data, type, row, meta) {
							if (data) {
								_f = new Date(data);
								return '<span class="badge badge-primary">' + zeroPad(_f.getHours(), 2) + ':' + zeroPad(_f.getMinutes(), 2) + '</span>';
							} else {
								return "&nbsp;";
							}
						}
					},
					{
						"data": "hsii", "render": function (data, type, row, meta) {
							if (data) {
								_f = new Date(data);
								return '<span class="badge badge-warning">' + zeroPad(_f.getHours(), 2) + ':' + zeroPad(_f.getMinutes(), 2) + '</span>';
							} else {
								return "&nbsp;";
							}
						}
					},
					{
						"data": "hsis", "render": function (data, type, row, meta) {
							if (data) {
								_f = new Date(data);
								return '<span class="badge badge-warning">' + zeroPad(_f.getHours(), 2) + ':' + zeroPad(_f.getMinutes(), 2) + '</span>';
							} else {
								return "&nbsp;";
							}
						}
					},
					{
						"data": "hsi", "render": function (data, type, row, meta) {
							if (data) {
								_f = new Date(data);
								return zeroPad(_f.getHours(), 2) + ':' + zeroPad(_f.getMinutes(), 2);
							} else {
								return "&nbsp;";
							}
						}
					},
					{ "data": null, "defaultContent": "&nbsp;" },//hnoc t2
					{
						"data": "thec", "render": function (data, type, row, meta) {
							if (data) {
								_f = new Date(data);
								return zeroPad(_f.getHours(), 2) + ':' + zeroPad(_f.getMinutes(), 2);
							} else {
								return "&nbsp;";
							}
						}
					},
					{
						"data": "cod_turno", "render": function (data, type, row, meta) {
							return zeroPad(data, 2);
						}
					},
					{
						"data": "phi", "render": function (data, type, row, meta) {
							if (data) {
								_f = new Date(data);
								return zeroPad(_f.getHours(), 2) + ':' + zeroPad(_f.getMinutes(), 2);
							} else {
								return "&nbsp;";
							}
						}
					},
					{
						"data": "phs", "render": function (data, type, row, meta) {
							if (data) {
								_f = new Date(data);
								return zeroPad(_f.getHours(), 2) + ':' + zeroPad(_f.getMinutes(), 2);
							} else {
								return "&nbsp;";
							}
						}
					},
					{
						"data": "hei1", "render": function (data, type, row, meta) {
							if (data) {
								_f = new Date(data);
								return zeroPad(_f.getHours(), 2) + ':' + zeroPad(_f.getMinutes(), 2);
							} else {
								return "&nbsp;";
							}
						}
					},
					{
						"data": "hei2", "render": function (data, type, row, meta) {
							if (data) {
								_f = new Date(data);
								return zeroPad(_f.getHours(), 2) + ':' + zeroPad(_f.getMinutes(), 2);
							} else {
								return "&nbsp;";
							}
						}
					},
					{
						"data": "hei3", "render": function (data, type, row, meta) {
							if (data) {
								_f = new Date(data);
								return zeroPad(_f.getHours(), 2) + ':' + zeroPad(_f.getMinutes(), 2);
							} else {
								return "&nbsp;";
							}
						}
					},
					{
						"data": "hes1", "render": function (data, type, row, meta) {
							if (data) {
								_f = new Date(data);
								return zeroPad(_f.getHours(), 2) + ':' + zeroPad(_f.getMinutes(), 2);
							} else {
								return "&nbsp;";
							}
						}
					},
					{
						"data": "hes2", "render": function (data, type, row, meta) {
							if (data) {
								_f = new Date(data);
								return zeroPad(_f.getHours(), 2) + ':' + zeroPad(_f.getMinutes(), 2);
							} else {
								return "&nbsp;";
							}
						}
					},
					{
						"data": "hes3", "render": function (data, type, row, meta) {
							if (data) {
								_f = new Date(data);
								return zeroPad(_f.getHours(), 2) + ':' + zeroPad(_f.getMinutes(), 2);
							} else {
								return "&nbsp;";
							}
						}
					},
					{
						"data": "hpi", "render": function (data, type, row, meta) {
							if (data) {
								_f = new Date(data);
								return zeroPad(_f.getHours(), 2) + ':' + zeroPad(_f.getMinutes(), 2);
							} else {
								return "&nbsp;";
							}
						}
					},
					{
						"data": "hps", "render": function (data, type, row, meta) {
							if (data) {
								_f = new Date(data);
								return zeroPad(_f.getHours(), 2) + ':' + zeroPad(_f.getMinutes(), 2);
							} else {
								return "&nbsp;";
							}
						}
					},
					{
						"data": "bs", "render": function (data, type, row, meta) {
							if (data) {
								_f = new Date(data);
								return zeroPad(_f.getHours(), 2) + ':' + zeroPad(_f.getMinutes(), 2);
							} else {
								return "&nbsp;";
							}
						}
					},
					{
						"data": "bi", "render": function (data, type, row, meta) {
							if (data) {
								_f = new Date(data);
								return zeroPad(_f.getHours(), 2) + ':' + zeroPad(_f.getMinutes(), 2);
							} else {
								return "&nbsp;";
							}
						}
					},
					{
						"data": "ptb", "render": function (data, type, row, meta) {
							if (data) {
								_f = new Date(data);
								return zeroPad(_f.getHours(), 2) + ':' + zeroPad(_f.getMinutes(), 2);
							} else {
								return "&nbsp;";
							}
						}
					},
					{
						"data": "fecha_ingreso", "render": function (data, type, row, meta) {
							if (data) {
								_f = new Date(data);
								return zeroPad(_f.getDate(), 2) + '/' + zeroPad(_f.getMonth() + 1, 2) + '/' + _f.getFullYear();
							} else {
								return "&nbsp";
							}
						}
					},
					{
						"data": "fecha_cese", "render": function (data, type, row, meta) {
							if (data) {
								_fi = new Date(data);
								return zeroPad(_f.getDate(), 2) + '/' + zeroPad(_f.getMonth() + 1, 2) + '/' + _f.getFullYear();
							} else {
								return "&nbsp";
							}
						}
					},
					{
						"data": "himax", "render": function (data, type, row, meta) {
							if (data) {
								_f = new Date(data);
								return zeroPad(_f.getHours(), 2) + ':' + zeroPad(_f.getMinutes(), 2);
							} else {
								return "&nbsp;";
							}
						}
					},
					{
						"data": "hsmax", "render": function (data, type, row, meta) {
							if (data) {
								_f = new Date(data);
								return zeroPad(_f.getHours(), 2) + ':' + zeroPad(_f.getMinutes(), 2);
							} else {
								return "&nbsp;";
							}
						}
					},
					{
						"data": "htarj", "render": function (data, type, row, meta) {
							if (data) {
								_f = new Date(data);
								return zeroPad(_f.getHours(), 2) + ':' + zeroPad(_f.getMinutes(), 2);
							} else {
								return "&nbsp;";
							}
						}
					},
					{
						"data": "th2", "render": function (data, type, row, meta) {
							if (data) {
								_f = new Date(data);
								return zeroPad(_f.getHours(), 2) + ':' + zeroPad(_f.getMinutes(), 2);
							} else {
								return "&nbsp;";
							}
						}
					},
					{
						"data": "th3", "render": function (data, type, row, meta) {
							if (data) {
								_f = new Date(data);
								return zeroPad(_f.getHours(), 2) + ':' + zeroPad(_f.getMinutes(), 2);
							} else {
								return "&nbsp;";
							}
						}
					},
					{
						"data": "th4", "render": function (data, type, row, meta) {
							if (data) {
								_f = new Date(data);
								return zeroPad(_f.getHours(), 2) + ':' + zeroPad(_f.getMinutes(), 2);
							} else {
								return "&nbsp;";
							}
						}
					},
				],
				info: false
			});
		}
	}
	else {
		$('#rowColaboradorTotalesFecha').removeClass('d-none');
		var tr = tableColaboradoresFecha.rows().$('tr.selected');
		var jsonRow = tableColaboradoresFecha.row(tr).data();
		var jsonParam = {
			Fecha: jsonRow.fecha_ddmmyyyy,
			FechaDeInicio: jsonParamDefault.AsiDesde,
			FechaDeFin: jsonParamDefault.AsiHasta,
			CodigoDeTipoPlanilla: jsonParamDefault.CodTipoPlanilla,
			CodigoDeProyecto: jsonParamDefault.CodProyecto,
			CodigoDeCC: jsonParamDefault.CodCentroCosto,
			CodigoDeLocalidad: jsonParamDefault.CodLocalidad,
			CodigoDeUN: jsonParamDefault.CodUnidadNegocio,
			CodigoDeArea: jsonParamDefault.CodArea,
			Estado: jsonParamDefault.Estado,
		}

		UIAjax.invokeMethodAsync('ajax/controlasistencia', 'GetAsistenciaDiariaFechaPersona', jsonParam).then(function (r) {
			var APersonal = [];
			$.each(r.data, function (i, v) {
				APersonal.push(v.cod_personal);
			});
			jsonParamDefault.CodPersonal = APersonal.join(',');
			LoadAsistenciaDiariaColaboradorPeriodo();
			UIDataTable.setEmpty('#tableColaboradorTotalesFecha');
			if (APersonal.length > 0) {
				tableColaboradorTotalesFecha = $('#tableColaboradorTotalesFecha').DataTable({
					//ajax: portal + '/ajax/controlasistencia/GetAsistenciaDiariaFechaPersona/?' + $.param(jsonParam),
					data: r.data,
					initComplete: function (settings, json) {
						//var APersonal = [];
						//$.each(json.data, function (i, v) {
						//    APersonal.push(v.cod_personal);
						//});
						//jsonParamDefault.CodPersonal = APersonal.join(',');
						//LoadAsistenciaDiariaColaboradorPeriodo();
					},
					//searching: false,
					paging: false,
					info: false,
					columns: [
						{ data: "trabajador" },
						{ data: "num_doc" },
						{ data: "flg_reloj" },
						{ data: "flg_ok_texto" },
						{ data: "flg_fm_texto" },
						{ data: "flg_ta_texto" },
						{ data: "flg_fi_texto" },
						{ data: "flg_ai_texto" },
						{ data: "flg_si_texto" },
						{ data: "excepcion" },
						{ data: "hi" },
						{ data: "hs" },
						{ data: "th" },
						{ data: "htar", className: 'td-htar' },
						{ data: "hai" },
						{ data: "the1" },
						{ data: "the2" },
						{ data: null, "defaultContent": "&nbsp;" },//Hnoc
						{ data: null, "defaultContent": "&nbsp;" },//Href
						{ data: null, "defaultContent": "&nbsp;" },//HnocF
						{ data: "hsii", className: 'td-hsii' },
						{ data: "hsis", className: 'td-hsis' },
						{ data: "hsi", className: 'td-hsi' },
						{ data: "thec" },
						{ data: "cod_turno" },
						{ data: "phi" },
						{ data: "phs" },
						{ data: "hei1" },
						{ data: "hei2" },
						{ data: "hei3" },
						{ data: "hes1" },
						{ data: "hes2" },
						{ data: "hes3" },
						{ data: "hpi" },
						{ data: "hps" },
						{ data: "bs" },
						{ data: "bi" },
						{ data: "ptb" },
						{ data: "htarj" },
						{ data: "har" },
						{ data: "th2" },
						{ data: "th3" },
						{ data: "th4" },
						{ data: "himax" },
						{ data: "hsmax" },
						{ data: "hina" },
						{ data: "fecha_ingreso" },
						{ data: "fecha_cese" },
					],
					rowCallback: function (row, data) {
						if (!UIEmpty(data.htar)) {
							//$(row).css({ 'background-color': 'red', 'color': 'white' });
							$('td.td-htar', row).addClass('badge-sky');
						}
						if (!UIEmpty(data.hsii)) {
							$('td.td-hsii', row).addClass('badge-warning');
						}
						if (!UIEmpty(data.hsis)) {
							$('td.td-hsis', row).addClass('badge-warning');
						}
						if (!UIEmpty(data.hsi)) {
							$('td.td-hsi', row).addClass('badge-warning');
						}
					}
				});
			}
		});

	}
};
function LoadAsistenciaDiariaColaboradorMarcas(jsonParams) {
	var valor = $('#selectModoDeVentana').val();
	AsistenciaDiariaColaboradorMarcas = $('#AsistenciaDiariaColaboradorMarcas').DataTable({
		ajax: portal + '/ajax/controlasistencia/AsistenciaDiariaColaboradorMarcas/?' + $.param(jsonParamDefault),
		initComplete: function (settings, json) {
			var _FN_jsonRow = function (tr) {
				var jsonRow = AsistenciaDiariaColaboradorMarcas.row(tr).data();
				var response = {
					bool: !UIEmpty(jsonRow),
					data: jsonRow
				};
				return response;
			};
			if (AccesoRegistra == "True") {
				$.contextMenu({
					selector: '#AsistenciaDiariaColaboradorMarcas tbody tr',
					animation: {
						duration: 50,
						show: 'slideDown',
						hide: 'slideUp'
					},
					events: {
						show: function (options) {
							this.addClass('currently-showing-menu');
							var tr = options.$trigger;
							AsistenciaDiariaColaboradorMarcas.rows().$('tr.selected').removeClass('selected');
							tr.addClass('selected');
							var jsonTotales;
							var jsonPeriodo = GetData.Json.Periodo();
							//var valor = $('#selectModoDeVentana').val();
							if (valor == "T") {
								var chk = $('#chkRefrigerio').is(':checked');
								if (chk) {
									var Rows = $('#tableColaboradorTotalesRefrigerio tbody tr.selected');
									jsonTotales = tableColaboradorTotalesRefrigerio.row(Rows).data();
								}
								else {
									var Rows = $('#AsistenciaDiariaColaboradorTotales tbody tr.selected');
									jsonTotales = AsistenciaDiariaColaboradorTotales.row(Rows).data();
								}
							}
							else {
								var Rows = $('#tableColaboradorTotalesFecha tbody tr.selected');
								if (!UIEmpty(tableColaboradorTotalesFecha)) {
									jsonTotales = tableColaboradorTotalesFecha.row(Rows).data();
								}
							}
							return !UIEmpty(jsonTotales) && (jsonPeriodo.estado == 'A');
						},
					},
					callback: function (key, opt, rootMenu, originalEvent) {
						var response = _FN_jsonRow(opt.$trigger);
						var modal = $('#modalAgregarMarca');
						var _setModal = function (title, hora, descripcion) {
							$('#spanTitleAM').text(title);
							$('#txtHoraAM').val(hora).focus();
							$('#txtDescripcionAM').val(descripcion).focus();
							modal.modal('show');
						};
						switch (key) {
							case 'añadir':
								_setModal('Agregar marca manual', '00:00', '');
								modal.attr('data-action', '0');
								break;
							case 'actualizar':
								var jsonRow = response.data;
								_setModal('Actualizar marca manual', jsonRow.marca_HHmm, jsonRow.mensaje_marca);
								modal.attr('data-action', '1');
								break;
							case 'eliminar':
								var jsonRow = response.data;
								var jsonParam = {
									FechaDeInicio: jsonRow.marca_ddmmyyyy,
									Hora: jsonRow.marca_HHmm,
									CodigoDePersonal: jsonRow.cod_personal,
								};
								MsgBox('question', function () {
									UIAjax.runFunctionAsync('ajax/controlasistencia', 'DeleteMarcas', jsonParam, function () {
										//var valor = $('#selectModoDeVentana').val();
										if (valor == "T") {
											var chk = $('#chkRefrigerio').is(':checked');
											if (chk) {
												$('#tableColaboradorTotalesRefrigerio tbody tr.selected').trigger('click');
											}
											else {
												$('#AsistenciaDiariaColaboradorTotales tbody tr.selected').trigger('click');
											}
										}
										else {
											$('#tableColaboradorTotalesFecha tbody tr.selected').trigger('click');
										}
									});
								});
								break;
						}
					},
					items: {
						añadir: { name: "Añadir", isHtmlName: true, icon: "fas fa-plus" },
						separator: {
							type: "cm_separator",
							visible: function (key, opt) { return _FN_jsonRow(opt.$trigger).bool },
						},
						actualizar: {
							name: "Actualizar", icon: "fad fa-pencil",
							disabled: function (key, opt) {
								return false;
							},
							visible: function (key, opt) {
								var jsonResult = _FN_jsonRow(opt.$trigger);
								if (jsonResult.bool) {
									return (jsonResult.data.flg_Origen == 'M');
								}
								return jsonResult.bool;
							},
						},
						eliminar: {
							name: "Eliminar", icon: "fas fa-minus",
							visible: function (key, opt) { return _FN_jsonRow(opt.$trigger).bool },
						},
					},
				});
			}
		},
		select: {
			style: 'single'
		},
		searching: false,
		paging: false,
		info: false,
		columns: [
			{ data: "cod_fotocheck" },
			{ data: "marca" },
			{ data: "flg_Origen" },
			{ data: "reloj" },
			{
				data: "flg_estado", render: function (data, type, row, meta) {
					var texto = '';
					switch (data) {
						case 'A':
							texto = 'Anulado';
							break;
						case 'V':
							texto = 'Vigente';
							break;

					}
					return texto;
				}
			},
			{ data: "hora" },
			{ data: "mensaje_marca" },
			{ data: "lectura" },
			{ data: "archivo" },
			{ data: "directorio" },
			{ data: "cod_reloj" }
		],
		rowCallback: function (row, data) {
			if (data.flg_estado == 'A') {
				$(row).css({ 'background-color': 'red', 'color': 'white' });
			}
		}
	});

}
async function LoadAsistenciaDiariaColaboradores(jsonParams) {
	jsonParamsExtra = {
		CodTipoPlanilla: $('#AsistenciaDiariaSelectTipoPlanilla').val(),
		CodProyecto: $('#AsistenciaDiariaSelectProyecto').val(),
		CodCentroCosto: $('#AsistenciaDiariaSelectCentroCosto').val(),
		CodLocalidad: $('#AsistenciaDiariaSelectLocalidad').val(),
		CodUnidadNegocio: $('#AsistenciaDiariaSelectUnidadNegocio').val(),
		CodArea: $('#AsistenciaDiariaSelectArea').val()
	}
	$.extend(true, jsonParamDefault, jsonParamsExtra, jsonParams);
	jsonParamDefault.FechaDeInicio = jsonParamDefault.AsiDesde;
	jsonParamDefault.FechaDeFin = jsonParamDefault.AsiHasta;
	$('#rowColaboradoresTrabajador').addClass('d-none');
	$('#rowColaboradoresFecha').addClass('d-none');

	var valor = $('#selectModoDeVentana').val();
	if (valor == "T") {
		$('#rowColaboradoresTrabajador').removeClass('d-none');
		jsonParamDefault.CodPersonal = '';
		await UIAjax.invokeMethodAsync('ajax/controlasistencia', 'GetColaboradores', jsonParamDefault).then(function (r) {
			AsistenciaDiariaColaboradores = $('#AsistenciaDiariaColaboradores').DataTable({
				data: r.data,
				initComplete: function (settings, json) {
					//this.api().columns().every(function () {
					//    var column = this;
					//    var select = $('<select><option value=""></option></select>')
					//        .appendTo($(column.footer()).empty())
					//        .on('change', function () {
					//            var val = $.fn.dataTable.util.escapeRegex(
					//                $(this).val()
					//            );

					//            column
					//                .search(val ? '^' + val + '$' : '', true, false)
					//                .draw();
					//        });

					//    column.data().unique().sort().each(function (d, j) {
					//        select.append('<option value="' + d + '">' + d + '</option>')
					//    });
					//});
					var api = this.api();
					var columnsEach = [3, 4, 5];
					$('.filterhead', api.table().header()).each(function (i) {
						//console.log(i);
						var column = api.column(i);
						if (columnsEach.includes(i)) {
							var select = $('<select class="form-control form-control-sm pr-0"><option value=""></option></select>')
								.appendTo($(this).empty())
								.on('change', function () {
									var val = $.fn.dataTable.util.escapeRegex(
										$(this).val()
									);
									//column.search(val ? '^' + val + '$' : '', true, false).draw();
									column.search(val).draw();
								});

							column.data().unique().sort().each(function (d, j) {
								select.append('<option value="' + d + '">' + d + '</option>');
							});
						}
						if (i == 19) {
							$('#chkInconsistencia').on('change', function (e) {
								var ths = $(this);
								if (ths.is(':checked')) {
									var val = $('#selectInconsistencia').val();
									column.search(val).draw();
								}
								else {
									column.search('').draw();
								}
							});
							$('#selectInconsistencia').on('change', function (e) {
								var ths = $(this);
								//var val = $.fn.dataTable.util.escapeRegex(
								//    $(this).val()
								//);
								if ($('#chkInconsistencia').is(':checked')) {
									var val = ths.val();
									//console.log(val);
									//column.search(val ? '^' + val + '$' : '', true, false).draw();
									column.search(val).draw();
								}
							});
						}
					});

					//this.api().columns().every(function () {
					//    var column = this;
					//    var select = $('<select><option value=""></option></select>')
					//        .appendTo($(column.footer()).empty())
					//        .on('change', function () {
					//            var val = $.fn.dataTable.util.escapeRegex(
					//                $(this).val()
					//            );

					//            column
					//                .search(val ? '^' + val + '$' : '', true, false)
					//                .draw();
					//        });

					//    column.data().unique().sort().each(function (d, j) {
					//        select.append('<option value="' + d + '">' + d + '</option>')
					//    });
					//});
				},
				ordering: false,
				searching: true,
				paging: true,
				info: true,
				lengthChange: false,
				columns: [
					{
						data: "flg_OK", render: function (data, type, row, meta) {
							textOK = '<span class="badge badge-pill badge-dark">OK</span>';
							textTA = '';
							textFI = '';
							textAI = '';
							textSI = '';
							if (data >= 0x0001) {
								textOK = '<span class="badge badge-pill badge-danger">' + data + '</span>';
							}
							if (row.flg_TA >= 0x0001) {
								textTA = ' <span class="badge badge-pill badge-sky">' + row.flg_TA + '</span>';
							}
							if (row.flg_FI >= 0x0001) {
								textFI = ' <span class="badge badge-pill badge-success">' + row.flg_FI + '</span>';
							}
							if (row.flg_AI >= 0x0001) {
								textAI = ' <span class="badge badge-pill badge-fucsia">' + row.flg_AI + '</span>';
							}
							if (row.flg_SI >= 0x0001) {
								textSI = ' <span class="badge badge-pill badge-warning">' + row.flg_SI + '</span>';
							}
							return textOK + textTA + textFI + textAI + textSI;
						}
					},
					{ data: "cod_trabajador" },
					{ data: "trabajador" },
					{ data: "flg_reloj", className: "text-center" },
					{ data: "flg_hh_auto", className: "text-center" },
					{ data: "flg_horario_nocturno", className: "text-center" },
					{ data: "fotocheck" },
					{ data: "nombre_documento" },
					{ data: "numero_docid" },
					{ data: "nombre_cargo" },
					{ data: "estado" },
					{ data: "fecha_ingreso" },
					{ data: "fecha_cese" },
					{ data: "nombre_proyecto" },
					{ data: "descripcion_unidad_negocio" },
					{ data: "descripcion_centro_costo" },
					{ data: "descripcion_area" },
					{ data: "descripcion_localidad" },
					{ data: "cod_personal" },
					{ data: "flg_include", className: "d-none" },
				],
			});
		});
	}
	else {
		$('#rowColaboradoresFecha').removeClass('d-none');
		tableColaboradoresFecha = $('#tableColaboradoresFecha').DataTable({
			ajax: portal + '/ajax/core/GetFechas/?' + $.param(jsonParamDefault),
			initComplete: function (settings, json) { },
			order: false,
			ordering: false,
			searching: true,
			paging: true,
			info: true,
			lengthChange: false,
			columns: [
				{ data: "dia_texto", className: "text-center" },
				{ data: "fecha_ddmmyyyy", className: "text-center" },
				{ data: "fecha_larga" },
			],
		});
	}
}
function LoadAsistenciaDiariaColaboradorSobretiempo() {
	//return;
	UIAjax.invokeMethodAsync('ajax/controlasistencia', 'AsistenciaDiariaColaboradorSobretiempo', jsonParamDefault).then(function (r) {
		var row = r.data[0];
		$('#si-phimin').html(row.phimin);
		$('#si-phei1').html(row.phei1);
		$('#si-phei2').html(row.phei2);
		$('#si-phei3').html(row.phei3);
		$('#si-phi').html(row.phi);
		$('#si-pbs').html(row.pbs);
		$('#si-pbi').html(row.pbi);
		$('#si-phs').html(row.phs);
		$('#si-phes1').html(row.phes1);
		$('#si-phes2').html(row.phes2);
		$('#si-phes3').html(row.phes3);
		$('#si-ptb').html(row.ptb);
		$('#si-pth').html(row.pth);
		$('#si-phsmax').html(row.phsmax);
	});
}

var Load = {
	modalRDE: {
		selectExcepcion: function (CodigoDeExcepcion) {
			var jsonParam = {
				CodigoDeExcepcion: CodigoDeExcepcion,
			};
			var valor = $('#selectModoDeVentana').val();
			if (valor == "T") {
				var jsonColaborador = GetData.Json.Colaborador();
				jsonParam.CodigoDePersonal = jsonColaborador.cod_entidad;
			}
			else {
				var jsonColaborador = GetData.Json.Totales();
				jsonParam.CodigoDePersonal = jsonColaborador.cod_personal;
			}
			UIAjax.invokeMethodAsync('ajax/controlasistencia', 'GetVExcepcionesResumen', jsonParam).then(function (r) {
				var html = '';
				$.each(r.data, function (i, v) {
					html += `<option value="${v.cod_anual}" data-json='${JSON.stringify(v)}'>${v.anual_afecto}</option>`;
				});
				var selectPeriodoAfectoRDE = $('#selectPeriodoAfectoRDE');
				selectPeriodoAfectoRDE.empty();
				selectPeriodoAfectoRDE.append(html);
				var año = UIYear();
				if (CodigoDeExcepcion == '01') {
					selectPeriodoAfectoRDE.val(año - 1);
					selectPeriodoAfectoRDE.removeAttr('disabled');
				}
				else {
					selectPeriodoAfectoRDE.val(año);
					selectPeriodoAfectoRDE.attr('disabled', 'disabled');
				}

			});

		}
	},
	modalGMMM: {
		Filtro: function () {
			var jsonPeriodo = GetData.Json.Periodo();
			return {
				CodigoDeProyecto: $('#selectProyectoGMMM').val(),
				CodigoDeCC: $('#selectCCGMMM').val(),
				CodigoDeUN: $('#selectUNGMMM').val(),
				CodigoDeArea: $('#selectAreaGMMM').val(),
				CodigoDeLocalidad: $('#selectLocalidadGMMM').val(),
				CodigoDeTipoPlanilla: $('#selectTipoPlanillaGMMM').val(),
				Estado: $('#selectEstadoGMMM').val(),
				Fecha: $('#txtFechaGMMM').val(),
				Hora: $('#txtHoraGMMM').val(),
				Mensaje: $('#txtNotaGMMM').val(),
				CodigoAnual: jsonPeriodo.cod_anual,
				FechaDeInicio: jsonPeriodo.asi_desde,
				FechaDeFin: jsonPeriodo.asi_hasta,
				Numero: jsonPeriodo.numero,
				Periodo: jsonPeriodo.periodo,
				//jsonStrPeriodo: $('#selectPeriodo').val(),
				ModoDeVenta: $('#selectModoDeVentana').val(),
				chkRefrigerio: $('#chkRefrigerio').is(':checked'),
				chkInconsistencia: $('#chkInconsistencia').is(':checked'),
				Inconsistencia: $('#selectInconsistencia').val(),
			}
		},
		Ready: function (jsonParam, fTrue) {
			UIAjax.invokeMethodAsync('ajax/controlasistencia', 'GetColaboradores', jsonParam).then(function (r) {
				var chk = function (id) {
					return `<div class="checkbox checkbox-primary d-inline mr-0" without-label>
									<input id="${id}" type="checkbox">
									<label for="${id}" class="cr mb-0"></label>
								</div>`
				};
				tableColadoradorGMMM = $('#tableColadoradorGMMM').DataTable({
					//ajax: portal + '/ajax/controlasistencia/GetColaboradores/?' + $.param(jsonParamDefault),
					data: r.data,
					initComplete: function (settings, json) {
						var api = this.api();
						$(this).off('click').on('click', 'tbody tr', function (e) {
							var ths = $(this);
							ths.toggleClass('selected');
						});
						var data = api.data();
						if (data.length > 0) {
							$('#tableColadoradorGMMM_filter').append('<button id="btnTodosColaboradores" type="button" class="float-left btn btn-outline-secondary">Todos</button>');
							var btn = $('#btnTodosColaboradores');
							btn.on('click', function (e) {
								var ths = $(this);
								var boolChk = ths.toggleClass('btn-secondary btn-outline-secondary').hasClass('btn-secondary');
								//tableColadoradorGMMM.rows().every(function (rowIdx, tableLoop, rowLoop) {
								//    var node = this.node();
								//    $(node).toggleClass('selected', boolChk);
								//});
								api.rows().$('tr').toggleClass('selected', boolChk);
								//api.table().rows().$('tr').toggleClass('selected', boolChk);
								//tableColadoradorGMMM.rows().$('tr').toggleClass('selected', boolChk);
							});
						}
						if (!UIEmpty(fTrue)) {
							fTrue();
						}
					},
					//pageLength: false,
					dom: '<"divFilterColaboradores">Bfrtip',
					buttons: [],
					ordering: false,
					//searching: true,
					//paging: true,
					//info: true,
					columns: [
						//{
						//    data: 'row', render: function (data, type, row, meta) {
						//        return chk(`chkTrabajador${data}`);
						//    }, className: "text-center"
						//},
						{ data: 'row', className: "text-center" },
						{ data: "cod_trabajador", className: "text-center" },
						{ data: "trabajador" },
						{ data: "fotocheck", className: "text-center" },
						{ data: "estado_texto", className: "text-center" },
					],
				});
			});
		}
	},
	modalIM: {
		Ready: function (fTrue) {
			if (!UIEmpty(tableColadoradorIM)) {
				tableColadoradorIM.clear().destroy();
				tableColadoradorIM = null;
			}
			tableProcesoIM = $('#tableProcesoIM').DataTable({
				ajax: portal + '/ajax/controlasistencia/GetProcesoPlantillaMarcacion/?',
				initComplete: function (settings, json) {
					if (!UIEmpty(fTrue)) {
						fTrue();
					}
					var api = this.api();
					this.off('click').on('click', 'tbody tr', function (e) {
						var ths = $(this);
						api.rows().$('tr.selected').removeClass('selected');
						ths.addClass('selected');
					});
					//console.log(this);
					//console.log(api);
				},
				bDestroy: true,
				//order: false,
				//ordering: false,
				searching: false,
				paging: false,
				info: false,
				lengthChange: false,
				columns: [
					{ data: "numero_importacion", className: "text-center" },
					{ data: "fecha_minima" },
					{ data: "fecha_maxima" },
				],
			});
		}
	}
}

var GetData = {
	Json: {
		Filtro: function () {
			return {
				CodigoDeProyecto: $('#AsistenciaDiariaSelectProyecto').val(),
				CodigoDeCC: $('#AsistenciaDiariaSelectCentroCosto').val(),
				CodigoDeUN: $('#AsistenciaDiariaSelectUnidadNegocio').val(),
				CodigoDeArea: $('#AsistenciaDiariaSelectArea').val(),
				CodigoDeLocalidad: $('#AsistenciaDiariaSelectLocalidad').val(),
				CodigoDeTipoPlanilla: $('#AsistenciaDiariaSelectTipoPlanilla').val(),
				ModoDeVentana: $('#selectModoDeVentana').val(),
				chkRefrigerio: $('#chkRefrigerio').is(':checked'),
				chkInconsistencia: $('#chkInconsistencia').is(':checked'),
				Inconsistencia: $('#selectInconsistencia').val(),
			}
		},
		Periodo: function () {
			var jsonPeriodo = JSON.parse(selectPeriodo.val());
			return jsonPeriodo;
		},
		Colaborador: function () {
			//var trColaborador = AsistenciaDiariaColaboradores.rows().$('tr.selected');
			//var jsonColaborador = AsistenciaDiariaColaboradores.row(trColaborador).data();
			var trColaborador;
			var jsonColaborador = {};
			var valor = $('#selectModoDeVentana').val();
			if (valor == "T") {
				trColaborador = $('#AsistenciaDiariaColaboradores tbody tr.selected');
				jsonColaborador = AsistenciaDiariaColaboradores.row(trColaborador).data();
			}
			else {
				trColaborador = $('#tableColaboradoresFecha tbody tr.selected');
				if (!UIEmpty(tableColaboradoresFecha)) {
					jsonColaborador = tableColaboradoresFecha.row(trColaborador).data();
				}
			}
			return jsonColaborador;
		},
		Totales: function () {
			var chk = $('#chkRefrigerio').is(':checked');
			var trTotales;
			var jsonTotales = {};
			var valor = $('#selectModoDeVentana').val();
			if (valor == "T") {
				var chk = $('#chkRefrigerio').is(':checked');
				if (chk) {
					trTotales = $('#tableColaboradorTotalesRefrigerio tbody tr.selected');
					jsonTotales = tableColaboradorTotalesRefrigerio.row(trTotales).data();
				}
				else {
					trTotales = $('#AsistenciaDiariaColaboradorTotales tbody tr.selected');
					jsonTotales = AsistenciaDiariaColaboradorTotales.row(trTotales).data();
				}
			}
			else {
				trTotales = $('#tableColaboradorTotalesFecha tbody tr.selected');
				if (!UIEmpty(tableColaboradorTotalesFecha)) {
					jsonTotales = tableColaboradorTotalesFecha.row(trTotales).data();
				}
			}
			return jsonTotales;
		},
		All: function () {
			var jsonFiltro = GetData.Json.Filtro();
			var jsonPeriodo = JSON.parse(selectPeriodo.val());
			var jsonColaborador = GetData.Json.Colaborador();
			var jsonTotales = GetData.Json.Totales();

			var jsonAll = {
				Filtro: jsonFiltro,
				Periodo: jsonPeriodo,
				Colaborador: jsonColaborador,
				Totales: jsonTotales,
			}

			return jsonAll;
		}
	},
	TRJson: {
		Totales: function () {
			var trTotales;
			var jsonTotales;
			var valor = $('#selectModoDeVentana').val();
			if (valor == "T") {
				var chk = $('#chkRefrigerio').is(':checked');
				if (chk) {
					trTotales = $('#tableColaboradorTotalesRefrigerio tbody tr.selected');
					jsonTotales = tableColaboradorTotalesRefrigerio.row(trTotales).data();
				}
				else {
					trTotales = $('#AsistenciaDiariaColaboradorTotales tbody tr.selected');
					jsonTotales = AsistenciaDiariaColaboradorTotales.row(trTotales).data();
				}
			}
			else {
				trTotales = $('#tableColaboradorTotalesFecha tbody tr.selected');
				if (!UIEmpty(tableColaboradorTotalesFecha)) {
					jsonTotales = tableColaboradorTotalesFecha.row(trTotales).data();
				}
			}
			if (!UIEmpty(jsonTotales)) {
				return { tr: trTotales, json: jsonTotales };
			} else {
				return null;
			}
		}
	}
}