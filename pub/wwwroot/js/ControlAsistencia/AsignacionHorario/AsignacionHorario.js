﻿var tableCodigoHorarioMIH;
var tableRegistroHorasMIH;
$(document).ready(function () {
	$('#btnMdImportacionDeHorarios').on('click', function () {
		$('#modalImportacionDeHorarios').modal('show');
	});
	$('input[name="gprbMIH"]').on('change', function () {
		var ths = $(this);
		var row1 = $('#rowCodigoHorarioMIH').addClass('d-none');
		var row2 = $('#rowRegistroHorasMIH').addClass('d-none');
		switch (ths.prop('id')) {
			case 'rbPorCodigoHorarioMIH':
				row1.removeClass('d-none');
				break;
			case 'rbPorRegistroHoraMIH':
				row2.removeClass('d-none');
				break;
		}
	});
	$('#btnPlantillaMIH').on('click', function (e) {
		var jsonFiltro = Load.MIH.Filtro();
		if (jsonFiltro.rbPorCodigo) {
			//UISave.URL(portal + "/ajax/controlasistencia/GetPlantillaIHCodigoHorarioCSV/?");
			UISave.URL(portal + "/ajax/controlasistencia/GetPlantillaIHCodigoHorarioXLSX/?");
		}
		else {
			//UISave.URL(portal + "/ajax/controlasistencia/GetPlantillaIHRegistroHorasCSV/?");
			UISave.URL(portal + "/ajax/controlasistencia/GetPlantillaIHRegistroHorasXLSX/?");
		}
	});
	$('#btnCargarArchivoMIH').on('click', function (e) {
		var jsonFiltro = Load.MIH.Filtro();
		if (UIEmpty(jsonFiltro.table)) {
			$("#fileCargarArchivoMIH").trigger('click');
			return;
		}
		MsgBox(`Desea eliminar información previamente.`, 'question', false, false, function () {
			$("#fileCargarArchivoMIH").trigger('click');
		});
	});
	$('#fileCargarArchivoMIH').on('click', function (e) {
		var ths = $(this);
		ths.val(null);
	});
	$('#fileCargarArchivoMIH').on('change', function (e) {
		var ths = $(this);
		e.preventDefault();
		var jsonFiltro = Load.MIH.Filtro();
		var files = ths.prop("files");
		var jsonParam = {
			File: files[0]
		};
		//app_config.debugger.state = true;
		if (jsonFiltro.rbPorCodigo) {
			UIAjax.invokeFormAsync('ajax/controlasistencia', 'GetColaboradorPlantillaIHCodigoHorario', jsonParam).then(function (r) {
				if (r.respuesta > 0) {
					MsgBox('success');
					tableCodigoHorarioMIH = $('#tableCodigoHorarioMIH').DataTable({
						data: r.json.data,
						initComplete: function (settings, json) {
						},
						bDestroy: true,
						lengthChange: false,
						columns: [
							{ data: "cod_trabajador", className: "text-center" },
							{ data: "numero_docid" },
							{ data: "trabajador" },
							{ data: "fecha", className: "text-center" },
							{ data: "cod_turno", className: "text-center" },
							{ data: "nombre_turno", className: "text-center" },
							{ data: "error_texto", className: "text-center" },
						],
						rowCallback: function (row, data) {
							if (data.error) {
								var tr = $(row);
								tr.addClass('table-danger text-dark text-bold');
							}
						}
					});
				}
				else {
					Load.MIH.Ready();
					MsgBox(r.mensaje, 'warning', false);
				}
			});
		}
		else {
			UIAjax.invokeFormAsync('ajax/controlasistencia', 'GetColaboradorPlantillaIHRegistroHoras', jsonParam).then(function (r) {
				if (r.respuesta > 0) {
					MsgBox('success');
					tableRegistroHorasMIH = $('#tableRegistroHorasMIH').DataTable({
						data: r.json.data,
						initComplete: function (settings, json) {
						},
						bDestroy: true,
						lengthChange: false,
						columns: [
							{ data: "cod_trabajador", className: "text-center" },
							{ data: "numero_docid", className: "text-center" },
							{ data: "trabajador" },
							{ data: "fecha", className: "text-center" },
							{ data: "phimin", className: "text-center" },
							{ data: "phi", className: "text-center" },
							{ data: "pbs", className: "text-center" },
							{ data: "pbi", className: "text-center" },
							{ data: "phs", className: "text-center" },
							{ data: "phsmax", className: "text-center" },
							{ data: "error_texto", className: "text-center" },
						],
						rowCallback: function (row, data) {
							if (data.error) {
								var tr = $(row);
								tr.addClass('table-danger text-dark text-bold');
							}
						}
					});
				}
				else {
					Load.MIH.Ready();
					MsgBox(r.mensaje, 'warning', false);
				}
			});
		}
	});
	$('#btnImportarMIH').on('click', function (e) {
		var ths = $(this);
		var jsonFiltro = Load.MIH.Filtro();
		if (UIEmpty(jsonFiltro.table)) {
			MsgBox('Debe cargar un archivo CSV primero.', 'warning', true);
			return;
		}
		var jsonParam = {
			Json: [],
			Asistencia: [],
		};
		var jsonRows = jsonFiltro.table.rows().data();
		if (jsonFiltro.rbPorCodigo) {
			MsgBox(`Se iniciará con la importación masiva.`, 'question', false, false, function () {
				$.each(jsonRows, function (i, v) {
					jsonParam.Json.push({
						CodigoDeTrabajador: v.cod_trabajador,
						NumeroDeDocumento: v.numero_docid,
						Fecha: v.fecha,
						CodigoDeTurno: v.cod_turno,
						Error: v.error,
						ErrorTexto: v.error_texto
					});
				});
				UIAjax.runFunctionAsync('ajax/controlasistencia', 'PostProcesoPlantillaIHCodigoHorario', jsonParam, function () {
					Load.MIH.Ready();
				});
			});
		}
		else {
			MsgBox(`Se iniciará con la importación masiva.`, 'question', false, false, function () {
				$.each(jsonRows, function (i, v) {
					jsonParam.Asistencia.push({
						cod_trabajador: v.cod_trabajador,
						numero_docid: v.numero_docid,
						fecha: v.fecha,
						phimin: v.phimin,
						phi: v.phi,
						pbs: v.pbs,
						pbi: v.pbi,
						phs: v.phs,
						phsmax: v.phsmax,
					});
				});
				UIAjax.runFunctionAsync('ajax/controlasistencia', 'PostProcesoPlantillaIHRegistroHoras', jsonParam, function () {
					Load.MIH.Ready();
				});
			});
		}
	});
});
if (typeof Load != undefined) {
	Load.MIH = {
		Filtro: function () {
			var rbPorCodigo = ($('input[name="gprbMIH"]:checked').prop('id') == 'rbPorCodigoHorarioMIH');
			var files = $('#fileCargarArchivoIM').prop("files");
			var file = UIEmpty(files) ? null : files[0];
			return {
				rbPorCodigo: rbPorCodigo,
				rbPorRegistro: !rbPorCodigo,
				file: file,
				table: rbPorCodigo ? tableCodigoHorarioMIH : tableRegistroHorasMIH,
			};
		},
		Ready: function () {
			if (!UIEmpty(tableCodigoHorarioMIH)) {
				tableCodigoHorarioMIH.clear().destroy();
				tableCodigoHorarioMIH = null;
			}
			if (!UIEmpty(tableRegistroHorasMIH)) {
				tableRegistroHorasMIH.clear().destroy();
				tableRegistroHorasMIH = null;
			}
		}
	}
}
else {
	console.warn("FALTA CREAR LA VARIABLE 'Load'");
}